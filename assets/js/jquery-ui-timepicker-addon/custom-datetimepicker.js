/* 
 * Used Page
 * insertCouponPage.php
 * updateCouponPage.php 
*/
$(function(){
var opt={	dateFormat: 'yy-mm-dd',
			showSecond: true,
			timeFormat: 'HH:mm:ss',
			showOn: "both",
			buttonImage: "/learninghouse/assets/img/calendar-icon.png"
		};
$('#start_datetimepicker').datetimepicker(opt);	
$('#end_datetimepicker').datetimepicker(opt);	
});