/*----------------------------Tin Can Api------------------------------*/
jQuery(document).ready(function($){
	//使用者發問問題時，初始化時間
    $('.tin-can-ask').on("click",function(){
        var initTime = new Date();
    });
     $("#tin-can-submit-form").click(function() {
     	library_name = $("#startDiscuss").find( "select[name='library_id'] option:selected" ).text();
        library_id = $("#startDiscuss").find( "select[name='library_id']" ).val();
    	forum_topic = $("#startDiscuss").find( "input[name='forum_topic']" ).val();
    	forum_content = $("#startDiscuss").find( "textarea[name='forum_content']" ).val();
    	tinCanApiAsk(library_id,library_name,forum_topic,forum_content);
    });
});

function tinCanApiAsk(library_id,library_name,forum_topic,forum_content){
    var parentId = base_url+"forum/course/category/"+library_id;
    var accessType = base_url+"forum/topic";
    var agent = new ADL.XAPIStatement.Agent(email,name);
    var contentVerb = ADL.verbs.asked;
    var authority = {"account":{"homePage":"http://140.92.88.88:80/mLRSTest","name":'2AEB66648CC123B8566D'},"name":"learninghouse","objectType":"Agent"};
    var activityId = base_url+"forum/forumContent/";
    var accessCourseTag = library_name;//問題分類
    var activity = new ADL.XAPIStatement.Activity(activityId,forum_topic,forum_content,accessType,'');
    var context = new ADL.XAPIStatement.Context(
        '',
        {"objectType":"Agent","mbox":email,"name":name},
        '',{
            "parent":[{
                "id":parentId,"objectType":"Activity",
                "definition":{
                    "name":{"en-US":accessCourseTag},
                    "description":{"en-US":accessCourseTag}
                }
            }]
           },
        '','','','','');
    var leaveTime = new Date();
    var lastTime = leaveTime-initTime;
    var result = new ADL.XAPIStatement.Result('',true,'','','',{"http://www.learninghouse.com.tw/learninghouse/forum/forumContent/time":lastTime});
    var stmt = new ADL.XAPIStatement(agent,contentVerb,activity,context,result,authority);
    stmt.generateId();
    var json_stmt = JSON.stringify(stmt);
    jsSendTincan(json_stmt);
    //console.log(JSON.stringify(stmt));
    // ADL.XAPIWrapper.changeConfig({
        // 'endpoint': 'https://140.92.88.88/mLRSTest/TCAPI/',
            // 'user': '2AEB66648CC123B8566D',
        // 'password': 'CACA3D203627B194301B'
        // });
    // ADL.XAPIWrapper.sendStatement(stmt);
}

function tinCanApiVote(forum_topic_id,forum_content_id,forum_topic,forum_content,alias,vote){
  	var parentId = base_url+"forum/forumContent/"+forum_topic_id;
    var accessType = base_url+"topic";
    var agent = new ADL.XAPIStatement.Agent(email,name);
    if(vote=='1'){
     	var contentVerb = ADL.verbs.up_voted;
    }else{
      	var contentVerb = ADL.verbs.down_voted;
    }
    var authority = {"account":{"homePage":"http://140.92.88.88:80/mLRSTest","name":'2AEB66648CC123B8566D'},"name":"learninghouse","objectType":"Agent"};
    var activityId = base_url+"forum/forumContent/"+forum_content_id;
    var activity = new ADL.XAPIStatement.Activity(activityId,forum_content,forum_content,accessType);
    activity.addExtensions({"http://localhost/learninghouse/author":alias});
    var context = new ADL.XAPIStatement.Context(
        '',
        {"objectType":"Agent","mbox":email,"name":name},
        '',{
            "parent":[{
                "id":parentId,"objectType":"Activity",
                "definition":{
                    "name":{"en-US":forum_topic},
                    "description":{"en-US":forum_topic}
                }
            }]
           },
        '','','','','');
    var result = new ADL.XAPIStatement.Result('','','',vote,'','');
    var stmt = new ADL.XAPIStatement(agent,contentVerb,activity,context,result,authority);
    stmt.generateId();
    var json_stmt = JSON.stringify(stmt);
    jsSendTincan(json_stmt);
    //console.log(JSON.stringify(stmt));
    // ADL.XAPIWrapper.changeConfig({
        // 'endpoint': 'https://140.92.88.88/mLRSTest/TCAPI/',
            // 'user': '2AEB66648CC123B8566D',
        // 'password': 'CACA3D203627B194301B'
        // });
    // ADL.XAPIWrapper.sendStatement(stmt);
}
function tinCanApiBestAnswer(forum_topic_id,forum_content_id,alias,forum_topic,forum_content){
  	var parentId = base_url+"forum/forumContent/"+forum_topic_id;
    var accessType = "http://activitystrea.ms/schema/1.0/question";
    var agent = new ADL.XAPIStatement.Agent(email,name);
    var contentVerb = ADL.verbs.gave;
    var authority = {"account":{"homePage":"http://140.92.88.88:80/mLRSTest","name":'2AEB66648CC123B8566D'},"name":"learninghouse","objectType":"Agent"};
    var activityId = base_url+"forum/forumContent/"+forum_content_id;
    var activity = new ADL.XAPIStatement.Activity(activityId,forum_content,forum_content,accessType);
    var context = new ADL.XAPIStatement.Context(
        '',
        {"objectType":"Agent","mbox":email,"name":name},
        '',{
            "parent":[{
                "id":parentId,"objectType":"Activity",
                "definition":{
                    "name":{"en-US":forum_topic},
                    "description":{"en-US":forum_topic}
                }
            }]
           },
        '','','','','');
    var result = new ADL.XAPIStatement.Result('',true,'','Wow!you got a best answer','',{"http://id.tincanapi.com/extension/feedback":alias});
    var stmt = new ADL.XAPIStatement(agent,contentVerb,activity,context,result,authority);
    stmt.generateId();
    var json_stmt = JSON.stringify(stmt);
    jsSendTincan(json_stmt);
    //console.log(JSON.stringify(stmt));
    // ADL.XAPIWrapper.changeConfig({
        // 'endpoint': 'https://140.92.88.88/mLRSTest/TCAPI/',
            // 'user': '2AEB66648CC123B8566D',
        // 'password': 'CACA3D203627B194301B'
        // });
    // ADL.XAPIWrapper.sendStatement(stmt);
}

function tinCanApiLeaveAns(forum_topic_id,forum_topic,forum_content,alias){
  	var parentId = base_url+"forum/forumContent/"+forum_topic_id;
    var accessType = base_url+"topic";
    var agent = new ADL.XAPIStatement.Agent(email,name);
    var contentVerb = ADL.verbs.commented;
    var authority = {"account":{"homePage":"http://140.92.88.88:80/mLRSTest","name":'2AEB66648CC123B8566D'},"name":"learninghouse","objectType":"Agent"};
    var activityId = base_url+"forum/forumContent/";
    var activity = new ADL.XAPIStatement.Activity(activityId,'Re:'+forum_topic,forum_topic,accessType);
    var context = new ADL.XAPIStatement.Context(
        '',
        {"objectType":"Agent","mbox":email,"name":name},
        '',{
            "parent":[{
                "id":parentId,"objectType":"Activity",
                "definition":{
                    "name":{"en-US":forum_topic},
                    "description":{"en-US":forum_topic},
                    "type":accessType,
                    "extensions":{
                    	"http://localhost/learninghouse/author":alias
					}
                }
            }]
           },
        '','','','','');
    var leaveTime = new Date();
    var lastTime = leaveTime-initTime;
    var result = new ADL.XAPIStatement.Result('','','',forum_content,'',{"http://www.learninghouse.com.tw/learninghouse/forum/forumContent/time":lastTime});
    var stmt = new ADL.XAPIStatement(agent,contentVerb,activity,context,result,authority);
    stmt.generateId();
    var json_stmt = JSON.stringify(stmt);
    jsSendTincan(json_stmt);
    //console.log(JSON.stringify(stmt));
    // ADL.XAPIWrapper.changeConfig({
        // 'endpoint': 'https://140.92.88.88/mLRSTest/TCAPI/',
            // 'user': '2AEB66648CC123B8566D',
        // 'password': 'CACA3D203627B194301B'
        // });
    // ADL.XAPIWrapper.sendStatement(stmt);
}

function tinCanApiLeaveComment(forum_topic_id,forum_topic,forum_content_id,forum_content,forum_comment,alias){
  	var parentId = base_url+"forum/forumContent/"+forum_topic_id+"/forumContent/"+forum_content_id;
    var accessType = base_url+"topic";
    var agent = new ADL.XAPIStatement.Agent(email,name);
    var contentVerb = ADL.verbs.commented;
    var authority = {"account":{"homePage":"http://140.92.88.88:80/mLRSTest","name":'2AEB66648CC123B8566D'},"name":"learninghouse","objectType":"Agent"};
    var activityId = base_url+"forum/forumContent/forumContent/";
    var activity = new ADL.XAPIStatement.Activity(activityId,'Re:'+forum_topic,'Re:'+forum_content,accessType);
    var context = new ADL.XAPIStatement.Context(
        '',
        {"objectType":"Agent","mbox":email,"name":name},
        '',{
            "parent":[{
                "id":parentId,"objectType":"Activity",
                "definition":{
                    "name":{"en-US":forum_topic},
                    "description":{"en-US":forum_content},
                    "type":accessType,
                    "extensions":{
                    	"http://localhost/learninghouse/author":alias
					}
                }
            }]
           },
        '','','','','');
    var leaveTime = new Date();
    var lastTime = leaveTime-initTime;
    var result = new ADL.XAPIStatement.Result('','','',forum_comment,'',{"http://www.learninghouse.com.tw/learninghouse/forum/forumContent/forumComent/time":lastTime});
    var stmt = new ADL.XAPIStatement(agent,contentVerb,activity,context,result,authority);
    stmt.generateId();
    var json_stmt = JSON.stringify(stmt);
    jsSendTincan(json_stmt);
    //console.log(JSON.stringify(stmt));
    // ADL.XAPIWrapper.changeConfig({
        // 'endpoint': 'https://140.92.88.88/mLRSTest/TCAPI/',
            // 'user': '2AEB66648CC123B8566D',
        // 'password': 'CACA3D203627B194301B'
        // });
    // ADL.XAPIWrapper.sendStatement(stmt);
}
if(json_stmt){
	//console.log(json_stmt);
	$.ajax({
			url: base_url + "backend/insertStmt",
			type:"POST",
			async:false,
			data: {stmt:json_stmt}
	});
	//alert(base_url);
};

function jsSendTincan(json_stmt){
	$.ajax({
			url: base_url + "backend/insertStmt",
			type:"POST",
			async:false,
			data: {stmt:json_stmt}
	});
}
