/* ---------------- 首頁的 header nav ----------------- */
jQuery(document).ready(function($){
	if((document.URL==base_url+"home")||(document.URL==base_url)){
		$("header").addClass("nav-home-translucence");
		$(document).scroll(function(){
			var commentsPos = $(".container").offset().top;
			if(commentsPos!=0){
				$("header").removeClass("nav-home-translucence");
			}else{
				$("header").addClass("nav-home-translucence");
			}
		});
	}
});

$(document).ready(function() {
    if(typeof message != 'undefined'){
        $.growlUI(message);
    }
});


/* ---------------- Sidebar Collapse ----------------- */

jQuery(document).ready(function($){
	$( ".sidebar .navbar-toggle" ).click(function() {
		if($(".sidebar a").is('.navbar-toggle.collapsed')){
			jQuery('.sidebar .navbar-toggle i').attr('class','fa fa-eye-slash');
		}else{
			jQuery('.sidebar .navbar-toggle i').attr('class','fa fa-eye');
		}
	});
});

/* ------------------ 註冊 ------------------- */
jQuery(document).ready(function($){
    var credit_card = $( ".credit-card" );
    var coupon = $( ".coupon" );
    $("#paid1").on('click',function () {
          credit_card.hide();
          coupon.show();
          $( "#credit-card" ).val('');
    });
    $("#paid2").on('click',function () {
          credit_card.show();
          coupon.hide();
          $( "#coupon" ).val('');
    });
    var basic_data = $(".basic-data");
    var add_member_desc = $(".add-member-desc");
    $("#add-member").on('click',function () {
          basic_data.show();
          add_member_desc.hide();
          $('#registrationForm')[0].reset();
    });
    $("#cancel-add-member").on('click',function () {
          basic_data.hide();
          add_member_desc.show();
          /*------取消時順便清除表單的資料------*/
          $('#registrationForm')[0].reset();
    });
});

/*----------------註冊表單驗證--------------*/
jQuery(document).ready(function($){
  $("#registrationForm").validate({
      highlight: function(element, errorClass, validClass) {
        $(element).parent().addClass(errorClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parent().removeClass(errorClass);
       },
    errorElement: "span",
    errorClass: "has-error",
    errorPlacement: function(error, element) {
        if($(element).attr('name')=="agreement"){
          $(error).addClass("label label-danger").insertAfter('#agree');
        }else{
          $(error).addClass("label label-danger").insertAfter($(element).parent());
        }
        },
    rules: {
      inputName: {
        required: true,
        minlength: 2,
        maxlength:15,
        regex:"^[ A-Za-z0-9_\u4e00-\u9fa5]+$"
      },
      inputEmail: {
        required: true,
        email: true,
        checkEmail: true,
        maxlength:50
      },
      email: {
        required: true,
        email: true,
        maxlength:50
      },
      inputPassword: {
        required: true,
        minlength: 5,
        maxlength:15
      },
      checkPassword: {
        required: true,
        minlength: 5,
        maxlength:15,
        equalTo: "#inputPassword"
      },
      coupon: {
        required: true,
        minlength:19,
        maxlength:19,
        checkCoupon: true
      },
          agreement: "required"
    },
    messages: {
      inputName: {
        required: "請輸入使用者帳號",
        minlength: "使用者帳號至少需要二個字",
        maxlength: "帳號最多十五個字",
        regex:"帳號請輸入中英數字、空白或底線(_)"
      },
      inputEmail: {
        required:"請輸入E-mail",
        email:"請輸入正確的E-mail格式",
        checkEmail: "E-mail已經被註冊了!",
        maxlength: "Email最多五十個字"
      },
      email: {
        required:"請輸入E-mail",
        email:"請輸入正確的E-mail格式",
        maxlength: "Email最多五十個字"
      },
      inputPassword: {
        required: "請填寫密碼",
        minlength: "密碼至少需要五個字",
        maxlength: "密碼最多十五個字"
      },
      checkPassword: {
        required: "請再確認密碼",
        minlength: "密碼至少需要五個字",
        maxlength: "密碼最多十五個字",
        equalTo: "密碼不正確"
      },
      coupon:{
          required: "請輸入優惠卷條碼",
          minlength: "請輸入十九個字元",
          maxlength: "請輸入十九個字元",
          checkCoupon: "請輸入有效的優惠卷條碼!"
      },
        agreement: "請同意我們的使用者條款方能註冊"
     }
  });
  $.validator.addMethod(
     "checkEmail",
     function(value,element){
        var result=$.ajax({
              url:"../user/checkEmail",
              type:"POST",
              async:false,
              data: "email="+value
            }).responseText;
    if(result=="0") return true //can be used
    else return false;
   },"E-mail已經被註冊了!")
  $.validator.addMethod(
       "checkCoupon",
       function(value,element){
        var result=$.ajax({
              url:"../user/checkCoupon",
              type:"POST",
              async:false,
              data: "coupon="+value
            }).responseText;
    if(result=="1") return true //can be used
    else return false;
    });

  $.validator.addMethod(
      "regex",
      function(value, element, regexp) {
          var re = new RegExp(regexp);
          return this.optional(element) || re.test(value);
    }
  );
});
/*----------------------登入驗證----------------------------*/
jQuery(document).ready(function($){
  $("#loginForm").validate({
      highlight: function(element, errorClass, validClass) {
        $(element).parent().addClass(errorClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parent().removeClass(errorClass);
       },
    errorElement: "span",
    errorClass: "has-error",
    errorPlacement: function(error, element) {
         $(error).addClass("label label-danger").insertAfter($(element).parent());
        },
    rules: {
          inputEmail: {
            required: true,
                email: true,
            maxlength:50
      },
          inputPassword: {
            required: true,
            minlength: 5,
            maxlength:15
      }
    },
    messages: {
      inputEmail: {
        required:"請輸入E-mail",
        email:"請輸入正確的E-mail格式",
        maxlength: "Email最多五十個字"
      },
      inputPassword: {
        required: "請輸入密碼",
        minlength: "密碼至少需要五個字",
        maxlength: "密碼最多十五個字"
      }
     }
  });
});

jQuery(document).ready(function($){
$("#login_reset").on('click',function () {
          /*------清除登入資料------*/
          $('#loginForm')[0].reset();
    });
});

/*----------------------click sidebar----------------------------*/
jQuery(document).ready(function($){
var url = document.URL;
$('.sidebar a[href="'+url+'"]').parent().addClass('active');
});

/*----------------------dashboard sidebar----------------------------*/
    jQuery(document).ready(function($){
        var    award_video_modal=$('#award-video-modal .modal-body').html();
        $('li>a[href="#knowledge"]').parent().removeClass('active');
        $('li>a[href="#technique"]').parent().removeClass('active');
        $('li>a[href="#activity"]').parent().removeClass('active');
        $('li>a[href="#award_video"]').parent().removeClass('active');
        $target=location.href.replace('http://','').split('/');
        $pos=$target[$target.length-1].lastIndexOf('#');
        $label=$target[$target.length-1].substr($pos+1);
        if($label=='dashboard'){
            $('li>a[href="#knowledge"]').parent().addClass('active');
            $('#knowledge').removeClass('hide');
        }
        $('#knowledge').parent().removeClass('active');
        $('#technique').parent().removeClass('active');
        $('#activity').parent().removeClass('active');
        $('#award_video').parent().removeClass('active');
        if($label=='knowledge' || $label=='technique' || $label=='activity'|| $label=='award_video'){
            $('li>a[href=#'+$label+']').parent().addClass('active');
            $('#'+$label).removeClass('hide');
        }
        $('li>a[href="#knowledge"]').click(function(){
            $('#knowledge').removeClass('hide');
            $('#technique').addClass('hide');
            $('#activity').addClass('hide');
            $('#award_video').addClass('hide');
            $('li>a[href="#knowledge"]').parent().addClass('active');
            $('li>a[href="#technique"]').parent().removeClass('active');
            $('li>a[href="#activity"]').parent().removeClass('active');
            $('li>a[href="#award_video"]').parent().removeClass('active');
        });
        $('li>a[href="#technique"]').click(function(){
            $('#knowledge').addClass('hide');
            $('#technique').removeClass('hide');
            $('#activity').addClass('hide');
            $('#award_video').addClass('hide');
            $('li>a[href="#knowledge"]').parent().removeClass('active');
            $('li>a[href="#technique"]').parent().addClass('active');
            $('li>a[href="#activity"]').parent().removeClass('active');
            $('li>a[href="#award_video"]').parent().removeClass('active');
        });
        $('li>a[href="#activity"]').click(function(){
            $('#knowledge').addClass('hide');
            $('#technique').addClass('hide');
            $('#activity').removeClass('hide');
            $('#award_video').addClass('hide');
            $('li>a[href="#knowledge"]').parent().removeClass('active');
            $('li>a[href="#technique"]').parent().removeClass('active');
            $('li>a[href="#activity"]').parent().addClass('active');
            $('li>a[href="#award_video"]').parent().removeClass('active');
        });
        $('li>a[href="#award_video"]').click(function(){
            $('#knowledge').addClass('hide');
            $('#technique').addClass('hide');
            $('#activity').addClass('hide');
            $('#award_video').removeClass('hide');
            $('li>a[href="#knowledge"]').parent().removeClass('active');
            $('li>a[href="#technique"]').parent().removeClass('active');
            $('li>a[href="#activity"]').parent().removeClass('active');
            $('li>a[href="#award_video"]').parent().addClass('active');
        });
        $('.dashboard_challengeBtn').click(function(){
            var content_order     = $.parseJSON($(this).attr('data-button')).content_order;
            var library=$.parseJSON($(this).attr('data-button')).library;
            var course=$.parseJSON($(this).attr('data-button')).course;
            var chapter=$.parseJSON($(this).attr('data-button')).chapter;
            var chapter_content=$.parseJSON($(this).attr('data-button')).chapter_content;
            var content_type=$.parseJSON($(this).attr('data-button')).content_type;
            if(content_type=='1'||content_type=='2'){
                location.href=base_url+'library/'+library+'/'+course+'/'+chapter+'/'+chapter_content;
            }else if(content_type=='0'){
                $.ajax({
                    url: base_url + 'field/set_flashdata_recordPosition',
                    type:'POST',
                    data:{current_content_order:content_order,currentChapter:chapter,isQuizDone:'0'},
                    complete:function(){
                        location.href=base_url+'library/'+library+'/'+course+'/'+chapter;
                    }
                });
            }
        });
        $(".award_video_challengeBtn").click(function(){
            var chapter=$.parseJSON($(this).attr('data-button')).chapter;
            var content=$.parseJSON($(this).attr('data-button')).content;
            var chapter_content_desc=$.parseJSON($(this).attr('data-button')).chapter_content_desc;
            var    chapter_content_url=$.parseJSON($(this).attr('data-button')).chapter_content_url;
            var    subtitle=$.parseJSON($(this).attr('data-button')).subtitle;
            var    email=$.parseJSON($(this).attr('data-button')).email;
            var    preview=$.parseJSON($(this).attr('data-button')).preview;
            resetFlowplayer('#award-video-modal',award_video_modal);
            $('h4.modal-title').html(chapter_content_desc);
            $('#award-video-modal').modal();
            $('#award-video-modal').on('hidden.bs.modal', function (e) {
                $('.overlay_player').flowplayer().stop();
            })
            resetFlowplayerClass('.overlay_player');
            embedSubtitle(chapter,subtitle,'.overlay_player');
            initFlowplayer(chapter,content,chapter_content_url,subtitle,email,preview,".overlay_player");
            if(isFromMobile!=true){
                initSpeedWidget();
            }
        });
        });

/*----------------------測驗題＿前一題為複選題的狀況----------------------*/
var isQuizDone;
var my_array = [];
$(document).on('click','.multiple-choice-submit',function(event){
    $('div.answers').block({message: null});
    $('.multiple-choice-submit').attr('disabled', true);
    var multiple_choice_arr = new Array();
    var question_id = $('input:checkbox').attr('data-question-id');
    $('input:checkbox:checked[name="answers"]').each(function(i) {
        var options_id = $(this).attr('data-options-id');
        var answer = this.value;
        multiple_choice_arr.push({options_id : options_id,answer : answer});
    });
    function sortByProperty(property) {
            'use strict';
            return function (a, b) {
            var sortStatus = 0;
            if (a[property] < b[property]) {
                sortStatus = -1;
            } else if (a[property] > b[property]) {
                sortStatus = 1;
            }
            return sortStatus;
        };
    }
    multiple_choice_arr.sort(sortByProperty('options_id'));
    var arr = new Array();
    for (var i = 0; i < multiple_choice_arr.length; i++) {
        arr.push(multiple_choice_arr[i]["answer"]);
    }
    var answer = arr.join(',');
    my_array.push({question_id:question_id,answer:$.md5(answer)});
    var result=$.ajax({
          url: base_url + "field/checkAnswer",
          type:"POST",
          async:false,
          data: {question_id: question_id, answer: $.md5(answer)}
        }).responseText;
    $('#feedback').empty();
    if(result=="true"){
        $('#feedback').append("<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>繼續</button>很好! 這是正確答案。</div>");
        var correct = +($('.correct strong').text())+1;
        $('.correct strong').empty().text(correct);
        $('.correct').parent().addClass('active');
    }else{
        $('#feedback').append("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>繼續</button>差一點! 不是這個答案</div>");
        var incorrect = +($('.incorrect strong').text())+1;
        $('.incorrect strong').empty().text(incorrect);
        $('.incorrect').parent().addClass('active');
    }
    $('.close').on("click",function(){
        $('div.answers').unblock();
        //取得下一題題目ID
        for ( var i = 0; i < quizNo.length; i = i + 1 ) {
            if(quizNo[ i ]==question_id){
                //取得下一題的ID
                var next_question_id = quizNo[ i + 1];
                if(next_question_id){
                    $.ajax({
                          url: base_url + "field/getQuestion",
                          type:"POST",
                          async:false,
                          data: {chapter_content_url: chapter_content_url,question_id: next_question_id},
                          dataType:"json",
                          success:quizCallback
                      });
                    //當前題數 ex:"1" of 5
                    $('.current-quizNo').text(i+2);
                }else{
                    //alert('最後一題');
                    //尚未實作把答案寫入資料庫(送資料到控制器時檢查是否有登入再做送入的動作)登入時把測驗的session清掉(未登入的清掉，已登入就從db拉)
                    //check總答對數目，result = 答對數，需大於pass_conditions
                    var result=$.ajax({
                          url: base_url + "field/checkAnswers",
                          type:"POST",
                          async:false,
                          data: {my_array: my_array}
                      }).responseText;
                      if(result>=pass_conditions){
                          $.ajax({
                        url: base_url + 'dashboard/recrodContent',
                        type:'POST',
                        data:{    chapter_content_url:chapter_content_url},
                        complete:function(){
                            $.ajax({
                                url: base_url + 'field/set_flashdata_isQuizDone',
                                type:'POST',
                                data:{    isQuizDone:'1',next_content_order:next_url}
                            });
                            //location.href ='http://'+url.attr('host')+arr.join("/");
                            //修改使用者進行關卡的完成狀態
                            var chpater_complete_flag = $.ajax({
                                url: base_url+ "field/updateUserChapterStatus",
                                type:'POST',
                                async:false,
                                data:{chapter_content_url:chapter_content_url}
                            }).responseText; // End of ajax call
                            //修改使用者進行課程的完成狀態
                            $.ajax({
                                url: base_url+ "field/updateUserCourseStatus",
                                type:'POST',
                                data:{chapter_content_url:chapter_content_url}
                                }); // End of ajax call
                            if (chpater_complete_flag.indexOf("affectedRows") >= 0){
                                $('#quiz-finish-modal').modal({backdrop: 'static',keyboard: false});
                            }else{
                                $('#quiz-completed-modal').modal({backdrop: 'static',keyboard: false});
                            }
                        }//complete
                        });// End of ajax call
                        $.ajax({
                            url: base_url + 'home/insertPoint',
                            type:'POST',
                            data:{chapter_content_url:chapter_content_url,completed:'completed_quiz'}
                        });
                      }else{
                          $('#quiz-failed-modal').modal({backdrop: 'static',keyboard: false});
                          //location.href ='http://'+url.attr('host')+arr.join("/");
                      }
                }
            }
        }

    })
})
/*----------------------測驗題＿前一題為單選題的狀況----------------------------*/
var my_array = [];
$(document).on('click', '.answer', function(event){
    $('div.answers').block({message: null});
    var href = $(this).attr('href');
    var question_id = $.url(href).param('question_id');
    var answer = $.url(href).param('answer');
    //var chapter_content_url = $(this).attr('data-content-url');
    my_array.push({question_id:question_id,answer:answer});
    url = $.url(href);
    var result=$.ajax({
          url: base_url + "field/checkAnswer",
          type:"POST",
          async:false,
          data: {question_id: question_id, answer: answer}
        }).responseText;
    $('#feedback').empty();
    if(result=="true"){
        $('#feedback').append("<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>繼續</button>很好! 這是正確答案。</div>");
        var correct = +($('.correct strong').text())+1;
        $('.correct strong').empty().text(correct);
        $('.correct').parent().addClass('active');
    }else{
        $('#feedback').append("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>繼續</button>差一點! 不是這個答案</div>");
        var incorrect = +($('.incorrect strong').text())+1;
        $('.incorrect strong').empty().text(incorrect);
        $('.incorrect').parent().addClass('active');
    }
    $('.close').on("click",function(){
        $('div.answers').unblock();
        //取得下一題題目ID
        for ( var i = 0; i < quizNo.length; i = i + 1 ) {
            if(quizNo[ i ]==question_id){
                //取得下一題的ID
                var next_question_id = quizNo[ i + 1];
                if(next_question_id){
                    $.ajax({
                          url: base_url + "field/getQuestion",
                          type:"POST",
                          async:false,
                          data: {chapter_content_url: chapter_content_url,question_id: next_question_id},
                          dataType:"json",
                          success:quizCallback
                      });
                    //當前題數 ex:"1" of 5
                    $('.current-quizNo').text(i+2);
                }else{
                    //alert('最後一題');
                    //尚未實作把答案寫入資料庫(送資料到控制器時檢查是否有登入再做送入的動作)登入時把測驗的session清掉(未登入的清掉，已登入就從db拉)
                    //check總答對數目，result = 答對數，需大於pass_conditions
                    var result=$.ajax({
                          url: base_url + "field/checkAnswers",
                          type:"POST",
                          async:false,
                          data: {my_array: my_array}
                      }).responseText;
                      if(result>=pass_conditions){
                        $.ajax({
                            url: base_url + 'field/set_flashdata_isQuizDone',
                            type:'POST',
                            data:{    isQuizDone:'1',next_content_order:next_url}
                        });
                          $.ajax({
                        url: base_url + 'dashboard/recrodContent',
                        type:'POST',
                        data:{    chapter_content_url:chapter_content_url},
                        complete:function(){
                            //location.href ='http://'+url.attr('host')+arr.join("/");
                            //修改使用者進行關卡的完成狀態
                            var chpater_complete_flag = $.ajax({
                                url: base_url+ "field/updateUserChapterStatus",
                                type:'POST',
                                async:false,
                                data:{chapter_content_url:chapter_content_url}
                            }).responseText; // End of ajax call
                            //修改使用者進行課程的完成狀態
                            $.ajax({
                                url: base_url+ "field/updateUserCourseStatus",
                                type:'POST',
                                data:{chapter_content_url:chapter_content_url}
                                }); // End of ajax call
                            if (chpater_complete_flag.indexOf("affectedRows") >= 0){
                                $('#quiz-finish-modal').modal({backdrop: 'static',keyboard: false});
                            }else{
                                $('#quiz-completed-modal').modal({backdrop: 'static',keyboard: false});
                            }
                        }//complete
                        });// End of ajax call
                        $.ajax({
                            url: base_url + 'home/insertPoint',
                            type:'POST',
                            data:{chapter_content_url:chapter_content_url,completed:'completed_quiz'}
                        });
                      }else{
                          $('#quiz-failed-modal').modal({backdrop: 'static',keyboard: false});
                          //location.href ='http://'+url.attr('host')+arr.join("/");
                      }
                }
            }
        }

    })
});
/*----------------------測驗題＿前一題為填充題的狀況----------------------*/
var my_array = [];
$(document).on('click','.fill-in-blank-quiz',function(event){
    $('div.answers').block({message: null});
    $('.fill-in-blank-quiz').attr('disabled', true);
    var fill_in_block_arr = new Array();
    var question_id = $(this).attr('data-question-id');
    $("input.text").each(function(index, elm){
        if(elm.name !='forum_topic'){
            fill_in_block_arr.push($(elm).val().trim());
        }
    });
    answer = fill_in_block_arr.join(',');
    my_array.push({question_id:question_id,answer:$.md5(answer)});
    var result=$.ajax({
          url: base_url + "field/checkAnswer",
          type:"POST",
          async:false,
          data: {question_id: question_id, answer: $.md5(answer)}
        }).responseText;
    $('#feedback').empty();
    if(result=="true"){
        $('#feedback').append("<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>繼續</button>很好! 這是正確答案。</div>");
        var correct = +($('.correct strong').text())+1;
        $('.correct strong').empty().text(correct);
        $('.correct').parent().addClass('active');
    }else{
        $('#feedback').append("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>繼續</button>差一點! 不是這個答案</div>");
        var incorrect = +($('.incorrect strong').text())+1;
        $('.incorrect strong').empty().text(incorrect);
        $('.incorrect').parent().addClass('active');
    }
    $('.close').on("click",function(){
        $('div.answers').unblock();
        //取得下一題題目ID
        for ( var i = 0; i < quizNo.length; i = i + 1 ) {
            if(quizNo[ i ]==question_id){
                //取得下一題的ID
                var next_question_id = quizNo[ i + 1];
                if(next_question_id){
                    $.ajax({
                          url: base_url + "field/getQuestion",
                          type:"POST",
                          async:false,
                          data: {chapter_content_url: chapter_content_url,question_id: next_question_id},
                          dataType:"json",
                          success:quizCallback
                      });
                    //當前題數 ex:"1" of 5
                    $('.current-quizNo').text(i+2);
                }else{
                    //alert('最後一題');
                    //尚未實作把答案寫入資料庫(送資料到控制器時檢查是否有登入再做送入的動作)登入時把測驗的session清掉(未登入的清掉，已登入就從db拉)
                    //check總答對數目，result = 答對數，需大於pass_conditions
                    var result=$.ajax({
                          url: base_url + "field/checkAnswers",
                          type:"POST",
                          async:false,
                          data: {my_array: my_array}
                      }).responseText;
                      if(result>=pass_conditions){
                        $.ajax({
                            url: base_url + 'field/set_flashdata_isQuizDone',
                            type:'POST',
                            data:{    isQuizDone:'1',next_content_order:next_url}
                        });
                          $.ajax({
                        url: base_url + 'dashboard/recrodContent',
                        type:'POST',
                        data:{    chapter_content_url:chapter_content_url},
                        complete:function(){
                            //location.href ='http://'+url.attr('host')+arr.join("/");
                            //修改使用者進行關卡的完成狀態
                            var chpater_complete_flag = $.ajax({
                                url: base_url+ "field/updateUserChapterStatus",
                                type:'POST',
                                async:false,
                                data:{chapter_content_url:chapter_content_url}
                            }).responseText; // End of ajax call
                            //修改使用者進行課程的完成狀態
                            $.ajax({
                                url: base_url+ "field/updateUserCourseStatus",
                                type:'POST',
                                data:{chapter_content_url:chapter_content_url}
                                }); // End of ajax call
                            if (chpater_complete_flag.indexOf("affectedRows") >= 0){
                                $('#quiz-finish-modal').modal({backdrop: 'static',keyboard: false});
                            }else{
                                $('#quiz-completed-modal').modal({backdrop: 'static',keyboard: false});
                            }
                        }//complete
                        });// End of ajax call
                        $.ajax({
                            url: base_url + 'home/insertPoint',
                            type:'POST',
                            data:{chapter_content_url:chapter_content_url,completed:'completed_quiz'}
                        });
                      }else{
                          $('#quiz-failed-modal').modal({backdrop: 'static',keyboard: false});
                          //location.href ='http://'+url.attr('host')+arr.join("/");
                      }
                }
            }
        }

    })
})
/*----------------------登入完回到原本瀏覽的該頁面----------------------------*/
$(".login").on("click",function(){
	login();
})
function login(){
	// Get the src of the url
    var src = $(location).attr('href');
    //Send Ajax request to setUrlSession.php, with src set as session in the POST data
    $.ajax({
        url: base_url + "field/setUrlSession",
          type:"POST",
          async:false,
          data: {src: src}
          })
}
/*----------------------jwplayer----------------------------*/
    // function initJwplayer(chapter,content,chapter_content_url){
        // $('.overlay').hide();
        // jwplayer('mediaplayer').setup({
            // file:"http://learninghousemediafiles.s3.hicloud.net.tw/"+chapter+"/"+content,
            // width: "800",
            // height: "450",
            // aspectratio: "16:9",
            // autostart:true
        // });
        // var sent=0;
        // //onTime Event
        // jwplayer("mediaplayer").onTime(function(event) {
            // // event.position contains playback position in seconds so to detect we're after
            // // 10 min point we check if position is larger than 10 seconds.
            // if (sent!=1 && event.position >= 10) {
                // sent=1;
                // $.ajax({
                    // url: base_url+ "dashboard/recrodContent",
                    // type:'POST',
                    // data:{chapter_content_url:chapter_content_url}
                // }); // End of ajax call
            // }
        // });
        // //onComplete Event
        // jwplayer('mediaplayer').onComplete(function() {
            // $('.overlay').css('height'     ,jwplayer('mediaplayer').getHeight()+'px');
            // $('.overlay').css('width'     ,jwplayer('mediaplayer').getWidth()+'px');
            // $('#mediaplayer').hide();
            // $('.overlay').show();
        // });
    // }
    // if ( $("#mediaplayer").length > 0 ) {
        // initJwplayer(chapter,content,chapter_content_url);
    // }
/*----------------------Video.js----------------------------*/
    // // Once the video is ready
        // var width =800;
        // var height=450;
        // var myPlayer ;    // Store the video object

    // function initVideoJs(chapter,content,chapter_content_url){
        // $('.overlay').hide();
        // myPlayer.src("http://learninghousemediafiles.s3.hicloud.net.tw/"+chapter+"/"+content);
        // _V_("example_video_1").ready(function(){
            // var aspectRatio = 9/16; // Make up an aspect ratio
            // function resizeVideoJS(){
                // // Get the parent element's actual width
                // width = document.getElementById(myPlayer.id).parentElement.offsetWidth*0.7;
                // height= width * aspectRatio;
                // // Set width to fill parent element, Set height
                // myPlayer.width(width).height(height);
            // }

            // resizeVideoJS(); // Initialize the function
            // window.onresize = resizeVideoJS; // Call the function on resize
        // });
    // }
    // if ( $("#example_video_1").length > 0 ) {
        // myPlayer = _V_("example_video_1");
        // initVideoJs(chapter,content,chapter_content_url);
        // var sent=0;
        // myPlayer.addEvent('timeupdate',function(){
            // if(sent!=1 &&this.currentTime()>=10){
                // sent=1;
                // $.ajax({
                    // url: base_url+ "dashboard/recrodContent",
                    // type:'POST',
                    // data:{chapter_content_url:chapter_content_url}
                // }); // End of ajax call
            // }
        // });
        // myPlayer.addEvent('play',function(){
            // $('.overlay').hide();
            // $('#example_video_1').show();
        // });
        // myPlayer.addEvent('ended',function(){
            // $('.overlay').width(width);
            // $('.overlay').height(height);
            // $('.overlay').show();
            // $('.example_video_1').hide();
        // });
    // }
/*----------------------flowplayer----------------------------*/
    var width=800;
    var height=450;
    var adaptiveRatio=9/16;
    var currentMovie;
    var player_content=$('#player-content').html();
    var isFromMobile=(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent));
    var isFromApple=(/iPhone|iPad|iPod/i.test(navigator.userAgent));
    function initFlowplayer(chapter,content,chapter_content_url,subtitle,email,preview,active_flag,$player){
        // run script after document is ready
        if($($player).length>0){
            // flowplayer.conf.adaptiveRatio = true;
            flowplayer.conf.embed = false;
            // install flowplayer to an element with CSS class "player"
            $($player).flowplayer({
                adaptiveRatio:true,
                native_fullscreen:true,
                splash:true,
                swf: base_url +"assets/css/flowplayer/flowplayer.swf",
                ratio: adaptiveRatio,
                playlist: [[{mp4:"http://learninghousemediafiles.s3.hicloud.net.tw/"+chapter+"/"+content}]]
            });
            if(isFromMobile!=true){
                $($player).flowplayer().play(0);
            }
            var sent=0;
            var web_cam_flag = 0;
            var time;
            $($player).flowplayer().bind('resume',function(){
                $($player)    .removeClass('hide');
                $('.overlay')    .addClass('hide');
                $('.errorPage')    .addClass('hide');
                if($($player+'>a[href="http://flowplayer.org"]').length > 0){
                    $($player+'>a[href="http://flowplayer.org"]').hide();
                }
            });
            $($player).flowplayer().bind('progress',function(){
                time=$($player).flowplayer().video.time;
                if(time>=10 && sent!=1){
                $.ajax({
                    url: base_url+ "dashboard/recrodContent",
                    type:'POST',
                    data:{chapter_content_url:chapter_content_url}
                }); // End of ajax call
                    sent=1;
                updateUserHits(chapter_content_url);
                }
                // if(time>=5 && web_cam_flag!=1){
                    // $('.player').flowplayer().pause();
                    // $('#webCamModal2').modal('show');
                    // web_cam_flag=1;
                // }
                if(!email && preview==1 || active_flag==0){
                    var limitDuration=30;
                    //調整影片總長度
                    if($($player).flowplayer().video.duration>limitDuration){
                        $($player).flowplayer().video.duration=limitDuration;
                        function secToHMString(targetSec){
                            var d=Number(targetSec);
                            var h=Math.floor(d/3600);
                            var m=Math.floor((d%3600)/60);
                            var s=Math.floor((d%3600)%60);
                            return ((h*60+m)<10?'0':'')+(h*60+m)+':'+(s<10?'0':'')+s;
                        }
                        $('.fp-duration').html(secToHMString(limitDuration));
                    }
                    //若影片長度大於指定秒數，未登入者僅可觀看影片指定秒數
                    if(time>=limitDuration){
                        $($player).flowplayer().stop();
                        resizeFlowplayer($player);
                        $('.overlay')    .removeClass('hide');
                        $($player)    .addClass('hide');
                        $(".buttons")    .addClass('hide');
                        $('.errorPage').addClass('hide');
                    }
                }
            });
            $($player).flowplayer().bind('finish',function(){
                resizeFlowplayer($player);
                $('.overlay')    .removeClass('hide');
                if($player=='.player'){
                    $($player)    .addClass('hide');
                }
                $(".buttons")    .addClass('hide');
                $('.errorPage').addClass('hide');
            });
            $($player).flowplayer().bind('pause',function(){
                if($($player+'>a[href="http://flowplayer.org"]').length > 0){
                    $($player+'>a[href="http://flowplayer.org"]').hide();
                }
            });
            $($player).flowplayer().bind('error',function(){
                resizeFlowplayer($player);
                $('.errorPage').removeClass('hide');
                $($player)    .addClass('hide');
                $(".buttons")    .addClass('hide');
            });
            $('#speedbtn').click(function(){
                $('#speedbtn').toggleClass('active');
                if($('#speedbtn').hasClass('active')){
                    $(".buttons").addClass('hide');
                }else if(!$('#speedbtn').hasClass('active')){
                    $(".buttons").removeClass('hide');
                }
            });
            $('#captionbtn').click(function(){
                $('#captionbtn').toggleClass('active');
                if($('#captionbtn').hasClass('active')){
                    $(".fp-subtitle").addClass('hide');
                    $('.player_background').css('padding','15px');
                    resizeFlowplayer($player);
                }else if(!$('#captionbtn').hasClass('active')){
                    $(".fp-subtitle").removeClass('hide');
                    if(isFromMobile!=true){
                        $('.player_background').css('padding','15px 15px 40px 15px');
                    }
                    resizeFlowplayer($player);
                }
            });
            flowplayer.conf = {
            		speeds: [0.25, 0.5, 1, 1.5, 2], // default
            		splash: true,
            		rtmp: "rtmp://stream.blacktrash.org/cfx/st"
            };
			$(".buttons span").click(function(){
				targetSpeed=flowplayer.conf.speeds[$(this).index('.buttons span')];
				$($player).flowplayer().speed(targetSpeed);
				$(".buttons span").removeClass("active");
				$(this).addClass("active");
			});
        }
    }
    function embedSubtitle(chapter,subtitle,$player){
        if(isFromMobile!=true){
            if(subtitle){
                $(".fp-subtitle").removeClass('hide');
                $('#captionbtn').removeClass('hide');
                $('#captionbtn').removeClass('active');
                $($player).append('<track kind="subtitles" src="'+base_url+'assets/subtitles/'+chapter+'/'+subtitle+'"/>');
                $($player).addClass('has-subtitle');
                $($player).removeClass('no-subtitle');
                $('.player_background').css('padding','15px 15px 40px 15px');
                if($player!='.overlay_player'){
                    resizeFlowplayer($player);
                }
            }else{
                $('#captionbtn').addClass('hide');
                $(".fp-subtitle").addClass('hide');
                $($player).addClass('no-subtitle');
                $($player).removeClass('has-subtitle');
                $('.player_background').css('padding','15px');
                if($player!='.overlay_player'){
                    resizeFlowplayer($player);
                }
            }
        }
    }
    function resetFlowplayerClass($player){
        if(isFromMobile!=true){
            if(!$('#speedbtn').hasClass('active')){
                $('#speedbtn').addClass('active');
            }
            $('#speedbtn').removeClass('hide');
            $(".buttons")    .addClass('hide');
            $($player).addClass('has-speedbtn');
        }else{
            $('#captionbtn').addClass('hide');
            $('#speedbtn').addClass('hide');
            $($player).addClass('no-speedbtn');
        }
        $('#player-content').removeClass('hide');
        $('#overlay').addClass('hide');
        $('.errorPage').addClass('hide');
        $('.buttons span').removeClass('active');
        $('.buttons span').eq(2).addClass('active');
        if(!$('#speedbtn').hasClass('active')){
            $('#speedbtn').addClass('active');
        }
    }
    function resetFlowplayer($target,$content){
        $($target).empty();
        $($target).append($content);
    }
    function initSpeedWidget(){
        flowplayer.conf = {
           speeds: [0.25, 0.5, 1, 1.5, 2], // default
          splash: true,
          rtmp: "rtmp://stream.blacktrash.org/cfx/st"
        };

        flowplayer(function (api, root) {
          var speedbuttons = $(".buttons span", root);

          api.bind("load", function (e, api) {
            // remove speed buttons if playback rate changes are not available
            if (api.engine == "flash" || !flowplayer.support.inlineVideo) {
              $(".buttons", root).remove();
            }

          }).bind("speed", function (e, api, rate) {
            // mark the current speed button as active
            var i;

            speedbuttons.removeClass("active");

            for (i = 0; i < api.conf.speeds.length; i = i + 1) {
              if (api.conf.speeds[i] == rate) {
                speedbuttons.eq(i).addClass("active");
                break;
              }
            }
          });

          // bind speed() call to click on buttons
          speedbuttons.click(function () {
            if (!$(this).hasClass("active")) {
              var buttonindex = speedbuttons.index(this);

              if (flowplayer.support.seekable) {
                api.speed(api.conf.speeds[buttonindex]);
              } else {
                // workaround for iPad and friends
                api.pause(function (e, api) {
                  api.speed(api.conf.speeds[buttonindex], function (e, api) {
                    api.resume();
                  });
                });
              }
            }
          });
        });
    }

    function resizeFlowplayer($player){
        // Get the parent element's actual width
            width = $($player).parent().width();
            height= width * adaptiveRatio;
            // Set width to fill parent element, Set height
            // if($player=='.overlay_player'){
                // $('#award-video-modal').width(width).height(height);
            // }
            $($player).width(width).height(height);
            $(".overlay").width(width).height(height);
            $(".errorPage").width(width).height(height);
    }
    // run script after document is ready
    if ( $(".player").length > 0 ) {
    var currentMission;
    var nextMission;
    var current_content_order;
    var next_content_order;
    var rewatch;
    var nextMission;
    function changePlayerContent(challengeBtnId){
        current_content_order=challengeBtnId;
        currentMission     = $.parseJSON($('#challengeBtn_'+current_content_order).attr('data-button'));
        next_content_order=(parseInt(current_content_order)+1);
        $.ajax({
            url: base_url + 'field/set_flashdata_recordPosition',
            type:'POST',
            data:{current_content_order:current_content_order,currentChapter:chapter,isQuizDone:isQuizDone}
        });
        if(currentMission.content_type==0){
            resetFlowplayerClass('.player');
            embedSubtitle(currentMission.chapter,currentMission.subtitle,'.player');
            initFlowplayer(currentMission.chapter,currentMission.content,currentMission.chapter_content_url,currentMission.subtitle,currentMission.email,currentMission.preview,currentMission.active_flag,'.player');
            bindClickFunction();
            if(isFromMobile!=true){
                initSpeedWidget();
            }
            if(currentMission.subtitle){
                resizeFlowplayer('.player');
            }else{
                resizeFlowplayer('.player');
            }
            $(document).scrollTop(0);
            $('title').html(currentMission.chapter_content_desc+'-Learning House');
            $("#forum_topic").val('[影片]'+currentMission.chapter_content_desc);
        }
        if(next_content_order<=currentMission.total){
            nextMission        = $.parseJSON($('#challengeBtn_'+next_content_order).attr('data-button'));
            $('.nextMission').removeClass('hide');
        }
        if(current_content_order==currentMission.total){
            $('.rewatch').empty();
            $('.rewatch').append('<span class="glyphicon glyphicon-repeat"></span> 再看一次 '+currentMission.chapter_content_desc);
            $('.nextMission').addClass('hide');
        }else if(nextMission.content_type=='0'){
            $('.rewatch').empty();
            $('.rewatch').append('<span class="glyphicon glyphicon-repeat"></span> 再看一次 '+currentMission.chapter_content_desc);
            $('.nextMission').empty();
            $('.nextMission').append('下一個任務: '+nextMission.chapter_content_desc+' <span class="glyphicon glyphicon-arrow-right"></span>');
        }else if(nextMission.content_type=='1'){
            $('.rewatch').empty();
            $('.rewatch').append('<span class="glyphicon glyphicon-repeat"></span> 再看一次 '+currentMission.chapter_content_desc);
            $('.nextMission').empty();
            $('.nextMission').append('下一個任務: 進行測驗 <span class="glyphicon glyphicon-arrow-right"></span>');
        }else if(nextMission.content_type=='2'){
            $('.rewatch').empty();
            $('.rewatch').append('<span class="glyphicon glyphicon-repeat"></span> 再看一次 '+currentMission.chapter_content_desc);
            $('.nextMission').empty();
            $('.nextMission').append('下一個任務: 進行挑戰 <span class="glyphicon glyphicon-arrow-right"></span>');
        }
    }
    function bindClickFunction(){
        $('.rewatch').click(function(){
            resetFlowplayer('#player-content',player_content);
            if(currentMission.content_type==0){
                changePlayerContent(current_content_order);
            }else if(currentMission.content_type==1){
                changePlayerContent(current_content_order);
            }else if(currentMission.content_type==2){
                changePlayerContent(current_content_order);
            }
            currentMovie=current_content_order;
        });
        $('.nextMission').click(function(){
            resetFlowplayer('#player-content',player_content);
            if(nextMission.content_type==0){
                changePlayerContent(next_content_order);
                currentMovie=current_content_order;
            }else if(nextMission.content_type==1){
                location.href=base_url+'library/'+library+'/'+course+'/'+chapter+'/'+nextMission.chapter_content_url;
            }else if(nextMission.content_type==2){
                location.href=base_url+'library/'+library+'/'+course+'/'+chapter+'/'+nextMission.chapter_content_url;
            }
            currentMovie=next_content_order;
        });
        $("#error_replay").click(function () {
            resetFlowplayer('#player-content',player_content);
            changePlayerContent(current_content_order);
        });
    }
    $('a[id^="challengeBtn_"]').click(function(){
        resetFlowplayer('#player-content',player_content);
        changePlayerContent($(this).attr('id').substring(13));
    });
        $("a[id^=transcript_]").click(function(){
            $(".player").flowplayer().seek(this.id.substring(11));
        });
        if($('.player video').length>0){
        }
    }
    jQuery(document).ready(function($){
        if ( $(".player").length > 0 ) {
            $(window).resize(function(){
                if(!$('#captionbtn').hasClass('active')){
                    resizeFlowplayer('.player');
                }else{
                    resizeFlowplayer('.player');
                }
            });
            if(typeof currentMovie != "undefined" && currentMovie>0 && isQuizDone==1 && $('#challengeBtn_'+currentMovie).length>0){
                $('#challengeBtn_'+currentMovie).click();
            }else if(currentMovie>0 && chapter==currentChapter){
                $('#challengeBtn_'+currentMovie).click();
            }else if(chapter!=currentChapter && isQuizDone=="" && hasStarted==""){
                $('#challengeBtn_1').click();
            }else if(isQuizDone==0){
                $.ajax({
                    url: base_url + 'field/set_flashdata_recordPosition',
                    type:'POST',
                    data:{isQuizDone:isQuizDone}
                });
                $('#player-content').addClass('hide');
            }
        }
        //進行測驗時點選離開測驗、或麵包屑的上一層
        if($('.breadcrumb li').length==5){
        $('.returnList,.breadcrumb li:nth-child(4)').click(function(event){
            event.preventDefault();
            $.ajax({
                url: base_url + 'field/set_flashdata_isQuizDone',
                type:'POST',
                data:{    isQuizDone:'0'},
                complete:function(){
                    location.href='.';
                }
            });
        });
        }
    });


/*--------------填充題----------------*/

$(".fill-in-blank-submit").on("click",function(){
    $('#question_content').block({message: null});
    $('.fill-in-blank-submit').attr('disabled', true);
    var fill_in_blank_answer = [];
    var question_id = $("#question_id").val();
    $(".text").each(function(index, elm){
        var fill_val = $(elm).val();
         fill_val = fill_val.replace(/ +(?= )/g,'');
         fill_val = ReplaceSeperator(fill_val);
        fill_in_blank_answer.push(fill_val);
    });
    function ReplaceSeperator(str) {
    var i;
    var result = "";
    var c;
    for (i = 0; i < str.length; i++) {
        c = str.substr(i, 1);
        if (c == "\n")
            result = result + "";
        else if (c != "\r")
            result = result + c;
    }
    return result;
}
    answer = fill_in_blank_answer.join(',');
    var result=$.ajax({
          url: base_url + "field/checkAnswer",
          type:"POST",
          async:false,
          data: {question_id: question_id, answer: $.md5(answer)}
        }).responseText;
    $('#feedback').empty();
    if(result=="true"){
        $('#feedback').append("<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>繼續</button>很好! 這是正確答案。</div>");
    }else{
        $('#feedback').append("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>再試一次</button>差一點! 不是這個答案</div>");
    }
     $('.close').on("click",function(){
        $('#question_content').unblock();
        $('.fill-in-blank-submit').attr('disabled', false);
        if(result=="true"){
        for ( var i = 0; i < quizNo.length; i = i + 1 ) {
            if(quizNo[ i ]==question_id){
                //取得下一題的ID
                var next_question_id = quizNo[ i + 1];
                if(next_question_id){
                    $.ajax({
                          url: base_url + "field/getQuestion",
                          type:"POST",
                          async:false,
                          data: {chapter_content_url: chapter_content_url,question_id: next_question_id},
                          dataType:"json",
                          success:callback
                      });
                    function callback(data){
                        var question_content = data[0]['question_content'];
                        var question_hint = data[0]['question_hint'];
                        $('#question_hint,#question_content').empty();
                        $('#question_hint').text(question_hint);
                        $('#question_content').html(question_content);
                        $("#question_id").val(next_question_id);
                    }
                    //當前題數 ex:"1" of 5
                    $('.current-quizNo').text(i+2);
                }else{
                    $.ajax({
                        url: base_url + 'field/set_flashdata_isQuizDone',
                        type:'POST',
                        data:{    isQuizDone:'1',next_content_order:next_url}
                    });
                    //alert('最後一題');
                          $.ajax({
                        url: base_url + 'dashboard/recrodContent',
                        type:'POST',
                        data:{    chapter_content_url:chapter_content_url},
                        complete:function(){
                            //location.href ='http://'+url.attr('host')+arr.join("/");
                            //修改使用者進行關卡的完成狀態
                            var chpater_complete_flag = $.ajax({
                                url: base_url+ "field/updateUserChapterStatus",
                                type:'POST',
                                async:false,
                                data:{chapter_content_url:chapter_content_url}
                            }).responseText; // End of ajax call
                            //修改使用者進行課程的完成狀態
                            $.ajax({
                                url: base_url+ "field/updateUserCourseStatus",
                                type:'POST',
                                data:{chapter_content_url:chapter_content_url}
                                }); // End of ajax call
                            if (chpater_complete_flag.indexOf("affectedRows") >= 0){
                                $('#quiz-finish-modal').modal({backdrop: 'static',keyboard: false});
                            }else{
                                $('#quiz-completed-modal').modal({backdrop: 'static',keyboard: false});
                            }
                            }
                        });
                        $.ajax({
                            url: base_url + 'home/insertPoint',
                            type:'POST',
                            data:{chapter_content_url:chapter_content_url,completed:'completed_code'}
                        });
                }
            }
        }
        }else{
            //填充題答案選錯答案，則繼續做
        }
        });
})
$('#img').on('change', function() {
    if((this.files[0].size/1024/1024).toFixed(2)>2){
        alert('圖片檔案超過限制大小2mb，請重新上傳！');
        window.location.reload(true);
    }

});
$('#update-img').on('change', function() {
    if((this.files[0].size/1024/1024).toFixed(2)>2){
        alert('圖片檔案超過限制大小2mb，請重新上傳！');
        window.location.reload(true);
    }

});
$('.course_img').on('change', function() {
    if((this.files[0].size/1024/1024).toFixed(2)>2){
        alert('課程圖片檔案超過限制大小2mb，請重新上傳！');
        window.location.reload(true);
    }

});
$('.upload-course-img').on('click', function() {
  var course_id = $(this).attr('data-course-id');
  $(".course_id").val(course_id);
});
/*----------------------edit profile----------------------------*/
jQuery(document).ready(function($){
  $("#editProfileForm").validate({
      highlight: function(element, errorClass, validClass) {
        $(element).parent().addClass(errorClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parent().removeClass(errorClass);
       },
    errorElement: "span",
    errorClass: "has-error",
    errorPlacement: function(error, element) {
        if($(element).attr('name')=="agreement"){
          $(error).addClass("label label-danger").insertAfter('#agree');
        }else{
          $(error).addClass("label label-danger").insertAfter($(element).parent());
        }
        },
    rules: {
        name: {
          required: true,
          minlength: 2,
          maxlength:15,
          regex:"^[ A-Za-z0-9_\u4e00-\u9fa5]+$"
        },
        alias: {
          required: true,
          minlength: 2,
          maxlength:15,
          checkAlias: true,
          regex:"^[ A-Za-z0-9_.\u4e00-\u9fa5]+$"
        },
        contentEmail: {
          required: true,
              email: true,
          maxlength:50,
          checkContentEmail: true
        },
        birthday: {
          required: true,
        },
        mobile: {
          required: true,
          minlength: 8,
          maxlength: 10,
        },  
        experience: {
          required: true,
        },        
      },
    messages: {
     name: {
        required: "請輸入姓名",
        minlength: "姓名至少需要二個字",
        maxlength: "姓名最多十五個字",
        regex:"姓名請輸入中英數字、空白或底線(_)"
     },
     alias: {
        required: "請輸入暱稱",
        minlength: "暱稱至少需要二個字",
        maxlength: "暱稱最多十五個字",
        checkAlias: "暱稱已經被使用了，請再輸入一次!",
        regex:"暱稱請輸入中英數字、空白或底線(_)"
     },
     contentEmail: {
        required:"請輸入E-mail",
        email:"請輸入正確的E-mail格式!",
        maxlength: "Email最多五十個字"
      },
      birthday: {
        required: "請輸入生日",
      },
      mobile: {
        required: "請輸入連絡電話",
        minlength: "連絡電話至少需要8個字",
        maxlength: "連絡電話至多需要10個字",
      },  
      experience: {
        required: "請輸入相關經驗",
      },      
  }
  });
  $.validator.addMethod(
     "checkAlias",
     function(value,element){
        var result=$.ajax({
              url: base_url + "user/checkEmailAlias",
              type:"POST",
              async:false,
              data: "alias="+value
            }).responseText;
    if(result=="0") return true //can be used
    else return false;
   },"暱稱已經被使用了，請再輸入一次!");
   $.validator.addMethod(
     "checkContentEmail",
     function(value,element){
        var result=$.ajax({
              url: base_url + "user/checkContentEmail",
              type:"POST",
              async:false,
              data: "email="+value
            }).responseText;
    if(result=="0") return true //can be used
    else return false;
   },"信箱已經被使用了，請再輸入一次!");
  $.validator.addMethod(
      "regex",
      function(value, element, regexp) {
          var re = new RegExp(regexp);
          return this.optional(element) || re.test(value);
    }
  );
});

/*----------------------首頁推播人數----------------------------*/
if((document.URL==base_url+"home")||(document.URL==base_url)){
(function poll() {
    $.ajax({
        url: base_url+ "home/updateAmount",
        type: "POST",
        success: function(data) {
            //console.log(data[0]['amount']);
            $('#amount').html(data[0]['amount']);
        },
        dataType: "json",
        complete: setTimeout(function() {poll()}, 60000),
        timeout: 2000
    })
})();
}
// 測驗後呼叫下一題的題目
function quizCallback(data){
    var question_content = data[0]['question_content'];
    $('.question_content,.content').empty();
    //第一題的時候，寫入測驗次數
    if($('.current-quizNo').text()=='1'){
    	if(!name){
    		$('#modal-login').modal({backdrop: 'static',keyboard: false});
    	}else{
    		updateUserHits(chapter_content_url);
    	}
    }
    if($('.current-quizNo').text()>='2'){
    	if(!name){
    		login();
    		window.location.href = base_url+ "home/login";
    	}
    }
    if(data[0]['question_type']==0){
        $('.question_content').html(question_content);
        for (var i = 0; i < data.length; i++) {
            $('.content').append('<h4>('+String.fromCharCode(65+i)+') <a href="'+$(location).attr('href')+'?answer='+data[i]['answer']+'&question_id='+data[i]['question_id']+'" class="answer" onclick="return false">'+ data[i]['options_content']+'</a></h4>');
        }
    }else if(data[0]['question_type']==1){
        $('.question_content').html(question_content);
        for (var i = 0; i < data.length; i++) {
            $('.content').append('<h4><div class="checkbox"><label>'+
            '<input type="checkbox" name="answers" data-question-id="'+data[i]['question_id']+'" data-options-id="'+data[i]['options_id']+'" value="'+data[i]['answer']+'">('+String.fromCharCode(65+i)+')' + data[i]['options_content']+'</label></div></h4>');
        }
        $('.content').append('<a href="javascript:void(0);" class="multiple-choice-submit btn btn-success btn-lg" role="button">送出答案</a>');
    }else if(data[0]['question_type']==2){
        $('.question_content').html(question_content);
        for (var i = 0; i < data.length; i++) {
            $('.content').append('<h4>('+data[i]['options_content']+') <a href="'+$(location).attr('href')+'?answer='+data[i]['answer']+'&question_id='+data[i]['question_id']+'" class="answer" onclick="return false">'+ (data[i]['options_content']=='O'?'True':'False') +'</a></h4>');
        }
    }else if(data[0]['question_type']==3){
        $('.question_content').html('　');
        $('.content').html('<h4 class="fill-in-blank">'+question_content+'</h4>');
        $('.content').append('<a href="javascript:void(0);" class="fill-in-blank-quiz btn btn-success btn-lg" data-question-id="'+data[0]['question_id']+'"role="button">送出答案</a>');
    }
}


/*---------------------去掉facebook登入後 url附帶的#_=_-------------------------------*/
 if (window.location.hash && window.location.hash == '#_=_') {
        if (window.history && history.pushState) {
            window.history.pushState("", document.title, window.location.pathname);
        } else {
            // Prevent scrolling by storing the page's current scroll offset
            var scroll = {
                top: document.body.scrollTop,
                left: document.body.scrollLeft
            };
            window.location.hash = '';
            // Restore the scroll offset, should be flicker free
            document.body.scrollTop = scroll.top;
            document.body.scrollLeft = scroll.left;
        }
    }

/*--------------------- Masonry -------------------------------*/
jQuery(document).ready(function($){
    if ( $("#masonry").length > 0 ) {
        var container = document.querySelector('#masonry');
        var msnry = new Masonry( container, {
          // options
          columnWidth: '.item',
          itemSelector: '.item',
          animate: true,
          transitionDuration: '0.5s'
        });
    }
});

/*-------------------------更新影片測驗時做點擊次數--------------------------------*/
function updateUserHits(chapter_content_url){
    $.ajax({
    url: base_url + "user/updateUserHits",
    type:"POST",
    async:false,
    data: {chapter_content_url: chapter_content_url},
    dataType:"json"
    });
}

/*----------------------forgetPassword----------------------------*/
jQuery(document).ready(function($){
  $("#forgetPasswordForm").validate({
      highlight: function(element, errorClass, validClass) {
        $(element).parent().addClass(errorClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parent().removeClass(errorClass);
       },
    errorElement: "span",
    errorClass: "has-error",
    errorPlacement: function(error, element) {
         $(error).addClass("label label-danger").insertAfter($(element).parent());
        },
    rules: {
          inputEmail: {
            required: true,
                email: true,
            maxlength:50,
            checkForgetEmail: true
      }
    },
    messages: {
      inputEmail: {
        required:"請輸入E-mail",
        email:"查無您的email，請重新輸入!",
        maxlength: "Email最多五十個字"
      }
     }
  });
  $.validator.addMethod(
     "checkForgetEmail",
     function(value,element){
        var result=$.ajax({
              url:"../user/checkEmail",
              type:"POST",
              async:false,
              data: "email="+value
            }).responseText;
    if(result=="1") return true //can be used
    else return false;
   },"查無您的email，請重新輸入!");
});

/*----------------------resetPassword----------------------------*/
jQuery(document).ready(function($){
  $("#resetForm").validate({
      highlight: function(element, errorClass, validClass) {
        $(element).parent().addClass(errorClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parent().removeClass(errorClass);
       },
    errorElement: "span",
    errorClass: "has-error",
    errorPlacement: function(error, element) {
        $(error).addClass("label label-danger").insertAfter($(element).parent());
    },
    rules: {
          inputNewPassword: {
        required: true,
        minlength: 5,
        maxlength:15
      },
          checkNewPassword: {
        required: true,
        minlength: 5,
        maxlength:15,
        equalTo: "#inputNewPassword"
      }
    },
    messages: {
      inputNewPassword: {
        required: "請填寫密碼",
        minlength: "密碼至少需要五個字",
        maxlength: "密碼最多十五個字"
      },
      checkNewPassword: {
        required: "請再確認密碼",
        minlength: "密碼至少需要五個字",
        maxlength: "密碼最多十五個字",
        equalTo: "密碼不正確"
      }
     }
  });
});

/*----------------------resendActiveMail----------------------------*/
jQuery(document).ready(function($){
    $( "#resendActiveMailBtn" ).click(function() {
		$.ajax({
			url: base_url + "user/resendActiveMail",
			success:function(data) {
				$('#resendActiveMail').html('寄信完成，請到信箱點選啟動認證連結');
			},
			beforeSend:function(){
				$('#resendActiveMail').html('寄信中....');
			}
		});
    });
});
/*----------------------討論區Pjax----------------------------*/
$(document).on('click', 'a[data-pjax]', function(event) {
	$.pjax.click(event, '#pjax-container',{fragment: '#pjax-container',timeout: 3000});
	//發問時，自動選取Modal的文章主題分類
	$('[name="library_id"] option').attr('selected',false);
	if(($(this).index('a[data-pjax]'))>0){		
		$('[name="library_id"] option').eq($(this).index('a[data-pjax]')-1).attr('selected',true);
	}else{
		$('[name="library_id"] option').eq(0).attr('selected',true);		
	}
})
/*----------------------學院Pjax----------------------------*/
$( "#proficiencyUl li" ).click(function() {
	$("#proficiency").text($(this).text());
})
$( "#allProficiency" ).click(function() {
	$("#proficiency").text("依學程深度分類");
})
$(document).on('click', 'a[college-data-pjax]', function(event) {
	$.pjax.click(event, '#college-pjax-container',{fragment: '#college-pjax-container',timeout: 3000});
	$(document).on('pjax:complete', function(event) {
  		$(".track-card .content-meta").dotdotdot({
  			wrap: 'letter',
  			height: 'null',
  			watch: true
  		});
	})
})
/*----------------------國內外資源Pjax----------------------------*/
$(document).on('click', 'a[resource-data-pjax]', function(event) {
	$.ajax({
		url: base_url + "college/setSearchSession",
		type:"POST",
		async:false,
		data: {search_string: $("#search_string").val()}
	})
	$.pjax.click(event, '#resource-pjax-container',{fragment: '#resource-pjax-container',timeout: 3000});
})
//選到的文字內容顯示在ul上面
$( "#resourceUl li" ).click(function() {
	$("#resource").text($(this).text());
})
/*----------------------相關活動(研討會)Pjax----------------------------*/
$(document).on('click', 'a[seminar-data-pjax]', function(event) {
	$.ajax({
		url: base_url + "college/setSearchSession",
		type:"POST",
		async:false,
		data: {search_string: $("#search_string").val()}
	})
	$.pjax.click(event, '#seminar-pjax-container',{fragment: '#seminar-pjax-container',timeout: 3000});
})
//選到的文字內容顯示在ul上面
$( "#seminarUl li" ).click(function() {
	$("#seminar").text($(this).text());
})

/*----------------------Forum----------------------------*/
$( ".comment-click" ).click(function() {
	var initTime = new Date();//[初始化]回覆問題的時間
    if(name){
		var str = $(this).attr('data-comment');
    	$(".comment-close").hide();
    	$("."+str).show();
    	$('html,body').animate({scrollTop:$("."+str).offset().top-350}, 700);
	}else{
		login();
		//alert login window
		$('#modal-title').text('您必須登入才能回覆');
		$('#modal-login').modal();
	}
});
$( ".comment-cancel" ).click(function() {
    $(".comment-close").hide();
});
//討論區編輯有三種  topic、content、comment
//第一種 content
$( ".edit-content" ).click(function() {
    //使用ajax取得使用者是否有seesion
    var result=$.ajax({
              url: base_url + "user/getUserSession",
              async:false
            }).responseText;
    if(result){
        var content_id = $(this).attr('data-edit-content-id');
        $(".dam"+content_id).hide();
        $(".dat"+content_id).hide();
        $(".vb"+content_id).hide();
        $(".daa"+content_id).hide();
            $(".show-edit"+content_id).show();
    }else{
        alert('編輯文章前請先登入LearningHose:)');
    }
});
//取消編輯content
$( ".cancel-edit-content" ).click(function() {
    var content_id = $(this).attr('data-edit-content-id');
    contentShow(content_id);
});
//更新content
// $('.update-content').on("click",function(){
    // var content_id = $(this).attr('data-edit-content-id');
    // var forum_content = $(".update-forum-content"+content_id).val();
    // $(".dat"+content_id).text(forum_content);
    // //寫入db吧  我想睡了 = =
    // $.ajax({
    // url: base_url + "forum/updateForumContent",
    // type:"POST",
    // async:false,
    // data: {forum_content: forum_content,content_id:content_id},
    // dataType:"json",
    // success: contentShow(content_id)
    // });
// });
function contentShow(content_id){
    $(".dam"+content_id).show();
    $(".dat"+content_id).show();
    $(".vb"+content_id).show();
    $(".daa"+content_id).show();
    $(".show-edit"+content_id).hide();
}
//第二種 comment
$( ".edit-comment" ).click(function() {
    var result=$.ajax({
              url: base_url + "user/getUserSession",
              async:false
            }).responseText;
    if(result){
        var comment_id = $(this).attr('data-edit-comment-id');
        $(".dcm"+comment_id).hide();
        $(".dct"+comment_id).hide();
        $(".da"+comment_id).hide();
        $(".show-edit-comment"+comment_id).show();
    }else{
        alert('編輯文章前請先登入LearningHose:)');
    }
});
//取消編輯comment
$( ".cancel-edit-comment" ).click(function() {
    var comment_id = $(this).attr('data-edit-comment-id');
    commentShow(comment_id);
});
//更新comment
// $('.update-comment').on("click",function(){
    // var comment_id = $(this).attr('data-edit-comment-id');
    // var forum_comment = $(".update-forum-comment"+comment_id).val();
    // $(".dct"+comment_id).text(forum_comment);
    // $.ajax({
    // url: base_url + "forum/updateForumComment",
    // type:"POST",
    // async:false,
    // data: {forum_comment: forum_comment,comment_id:comment_id},
    // dataType:"json",
    // success: commentShow(comment_id),
    // });
// });
function commentShow(comment_id){
    $(".dcm"+comment_id).show();
    $(".dct"+comment_id).show();
    $(".da"+comment_id).show();
    $(".show-edit-comment"+comment_id).hide();
}
//第三種 topic
$( ".edit-topic" ).click(function() {
	var result=$.ajax({
              url: base_url + "user/getUserSession",
              async:false
            }).responseText;
    if(result){
        var content_id = $(this).attr('data-edit-content-id');
    	$(".ft"+content_id).hide();
    	$(".dtm"+content_id).hide();
    	$(".dat"+content_id).hide();
    	$(".da"+content_id).hide();
    	$(".show-edit-content"+content_id).show();
    }else{
        alert('編輯文章前請先登入LearningHose:)');
    }
});
//取消編輯topic
$( ".cancel-edit-topic" ).click(function() {
    var content_id = $(this).attr('data-edit-content-id');
    topicShow(content_id);
});
//更新topic
// $('.update-topic').on("click",function(){
    // var topic_id = $(this).attr('data-edit-topic-id');
    // var forum_topic = $(".update-forum-topic"+topic_id).val();
    // var content_id = $(this).attr('data-edit-content-id');
    // var forum_content = $(".update-forum-content"+content_id).val();
    // $(".ft"+content_id).text(forum_topic);
    // $(".dat"+content_id).text(forum_content);
    // $.ajax({
    // url: base_url + "forum/updateForumTopic",
    // type:"POST",
    // async:false,
    // data: {forum_topic: forum_topic,topic_id:topic_id,forum_content: forum_content,content_id:content_id},
    // dataType:"json",
    // success: topicShow(content_id),
    // });
// });
function topicShow(content_id){
    $(".ft"+content_id).show();
    $(".dtm"+content_id).show();
    $(".dat"+content_id).show();
    $(".da"+content_id).show();
    $(".show-edit-content"+content_id).hide();
}
//看影片時可以使用發問問題(使用ajax)
$( "#ajaxStartDiscuss" ).submit(function( event ) {
    event.preventDefault();
      var $form = $( this );
    library_id = $form.find( "select[name='library_id']" ).val();
    library_name = $form.find( "select[name='library_id'] option:selected" ).text();
    forum_topic = $form.find( "input[name='forum_topic']" ).val();
    forum_content = $form.find( "textarea[name='forum_content']" ).val();
    if(! $form.valid()) return false;
    //取得form裡面的變數，準備送出
    $.ajax({
    url: base_url + "forum/add",
    type:"POST",
    data: {library_id: library_id,forum_topic: forum_topic,forum_content: forum_content},
    success: function (data) {
    	tinCanApiAsk(library_id,library_name,forum_topic,forum_content);
        $('#modal-start').modal('hide');
        alert('已將您的問題新增至討論區！');
        $('.player').flowplayer().resume();
    },
    error: function (data) {
         alert('Ajax request 發生錯誤');
    }
    });

});
//click我要發問的時候，先暫停影片
$( '.sidetab' ).click(function() {
	initTime = new Date();
    if($('#player-content').hasClass('hide')){
        //如果有#player-content 有hide的屬性，就表示沒有影片，因此不需要pause以及resume
    }else{
        $('.player').flowplayer().pause();
    }

});
//取消發問問題時，影片繼續撥放
$( '.cancle-video-discuss' ).click(function() {
    if($('#player-content').hasClass('hide')){
        //如果有#player-content 有hide的屬性，就表示沒有影片，因此不需要pause以及resume
    }else{
        $('.player').flowplayer().resume();
    }

});

$("[name='my-tracks-checkbox']").bootstrapSwitch();
$("[name='my-tracks-checkbox']").on('switch-change', function (e, data) {
    value = data.value;
    if(value){
        $(".tracks").show();
        $(".resume").hide();
    }else{
        $(".tracks").hide();
        $(".resume").show();
    }
});

/*---------------------- Track --------------------------------------------*/

jQuery(document).ready(function($){
    $('.achievement-meta').on("click",function() {
        $(this).siblings("div.achievement-steps").slideToggle();
        var el = $(this).find('i');
        if(el.hasClass('fa-chevron-down')){
            $(el).removeClass('fa-chevron-down').addClass('fa-chevron-up');
        }else if(el.hasClass('fa-chevron-up')){
            $(el).removeClass('fa-chevron-up').addClass('fa-chevron-down');
        }
    });
});

/*---------------------- 拍照模組 ----------------------------*/
/*
$(window).load(function(){
    $('#webCamModal').modal('show');
});
*/
$(".show-markdown-cheatsheet").on('click',function () {
    $(".markdown-cheatsheet").toggle();
});
/*---------------------- doughnut ----------------------------*/

// var doughnutData = [{value: 30,    color:"#F7464A"    },{value : 50,color : "#46BFBD"},{value : 100,color : "#FDB45C"},{value : 40,color : "#949FB1"},{value : 120,color : "#4D5360"}];
if(doughnutData){
	var ctx = document.getElementById("canvas").getContext("2d");
	var myDoughnut = new Chart(ctx).Doughnut(doughnutData);
}
/*---------------------- radar ----------------------------*/
if(radarChartData){
	var ctx = document.getElementById("canvas").getContext("2d");
	var option = {scaleShowLabels : true};
	var myRadar = new Chart(ctx).Radar(radarChartData,option);
}
if(barData){
	var myBar = new Chart(document.getElementById("canvas_bar").getContext("2d")).Bar(barData,{
			responsive : true,
			legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
		});
	var legend = myBar.generateLegend();
	$("#legend").html(legend);
}

/*-------------------------編輯研討會訊息----------------------------------*/
jQuery(document).ready(function($){
    $('.edit-seminar-btn').on("click",function() {
        $(this).closest('.seminar').hide();
        $(this).closest('.seminar').next('.edit-seminar').show();
        $(this).closest('.seminar').next('.edit-seminar').find('.start_time').addClass('start_time');
        $(this).closest('.seminar').next('.edit-seminar').find('.end_time').addClass('end_time');
        $('.start_time').datetimepicker({pickTime: false});
		$('.end_time').datetimepicker({pickTime: false});
        dateTimeLimit();
    });
    $('.cancle-edit-seminar').on("click",function(){
        $(this).closest('.edit-seminar').hide();
        $(this).closest('.edit-seminar').prev('.seminar').show();
        $(this).closest('.edit-seminar').find('.start_time').removeClass('start_time');
        $(this).closest('.edit-seminar').find('.end_time').removeClass('end_time');
    });
    $('.del-seminar').on("click",function(){
        $(this).closest('.seminar').hide();
        //ajax del
        var seminar_id = $(this).attr('data-seminar-id');
        $.ajax({
            url: base_url + "backend/delSeminar",
            type:"POST",
            data: {seminar_id: seminar_id},
            success: function (data) {
                alert('delete！');
            },
            error: function (data) {
                 alert('Ajax request 發生錯誤');
            }
        });
    });
    $('.add-seminar-btn').on("click",function() {
        // $(this).closest('.add-plus-seminar').hide();
        $(this).closest('.add-plus-seminar').next('.add-seminar').toggle();
    });
    $('.cancle-add-seminar').on("click",function(){
        $(this).closest('.add-seminar').hide();
        // $(this).closest('.add-seminar').prev('.add-plus-seminar').show();
    });


    $('.edit-industry-btn').on("click",function() {
        $(this).closest('.industry').hide();
        $(this).closest('.industry').next('.edit-industry').show();
        $(this).closest('.industry').next('.edit-industry').find('.industry_origin_date').addClass('date');
        $('.date').datetimepicker({pickTime: false});
    });
    $('.cancle-edit-industry').on("click",function(){
        $(this).closest('.edit-industry').hide();
        $(this).closest('.edit-industry').prev('.industry').show();
        $(this).closest('.edit-industry').find('.industry_origin_date').removeClass('date');
    });
    $('.del-industry').on("click",function(){
        $(this).closest('.industry').hide();
        //ajax del
        var industry_id = $(this).attr('data-industry-id');
        $.ajax({
            url: base_url + "backend/delIndustry",
            type:"POST",
            data: {industry_id: industry_id},
            success: function (data) {
                alert('delete！');
            },
            error: function (data) {
                 alert('Ajax request 發生錯誤');
            }
        });
    });
    $('.add-industry-btn').on("click",function() {
        //$(this).closest('.add-plus-industry').hide();
        $(this).closest('.add-plus-industry').next('.add-industry').toggle();
    });
    $('.cancle-add-industry').on("click",function(){
        $(this).closest('.add-industry').hide();
        //$(this).closest('.add-industry').prev('.add-plus-industry').show();
    });

    $('.edit-jobs-btn').on("click",function() {
        $(this).closest('.jobs').hide();
        $(this).closest('.jobs').next('.edit-jobs').show();
    });
    $('.cancle-edit-jobs').on("click",function(){
        $(this).closest('.edit-jobs').hide();
        $(this).closest('.edit-jobs').prev('.jobs').show();
    });
    $('.del-jobs').on("click",function(){
        $(this).closest('.jobs').hide();
        //ajax del
        var jobs_id = $(this).attr('data-jobs-id');
        $.ajax({
            url: base_url + "backend/delJobs",
            type:"POST",
            data: {jobs_id: jobs_id},
            success: function (data) {
                alert('delete！');
            },
            error: function (data) {
                 alert('Ajax request 發生錯誤');
            }
        });
    });
    $('.add-jobs-btn').on("click",function() {
        // $(this).closest('.add-plus-jobs').hide();
        $(this).closest('.add-plus-jobs').next('.add-jobs').toggle();
    });
    $('.cancle-add-jobs').on("click",function(){
        $(this).closest('.add-jobs').hide();
        // $(this).closest('.add-jobs').prev('.add-plus-jobs').show();
    });

    $('.edit-resource-btn').on("click",function() {
        $(this).closest('.resource').hide();
        $(this).closest('.resource').next('.edit-resource').show();
        $(this).closest('.resource').next('.edit-resource').find('.resource_origin_date').addClass('date');
        $('.date').datetimepicker({pickTime: false});
    });
    $('.cancle-edit-resource').on("click",function(){
        $(this).closest('.edit-resource').hide();
        $(this).closest('.edit-resource').prev('.resource').show();
        $(this).closest('.edit-resource').find('.resource_origin_date').removeClass('date');
    });
    $('.del-resource').on("click",function(){
        $(this).closest('.resource').hide();
        //ajax del
        var resource_id = $(this).attr('data-resource-id');
        $.ajax({
            url: base_url + "backend/delResource",
            type:"POST",
            data: {resource_id: resource_id},
            success: function (data) {
                alert('delete！');
            },
            error: function (data) {
                 alert('Ajax request 發生錯誤');
            }
        });
    });
    $('.add-resource-btn').on("click",function() {
        // $(this).closest('.add-plus-resource').hide();
        $(this).closest('.add-plus-resource').next('.add-resource').toggle();
    });
    $('.cancle-add-resource').on("click",function(){
        $(this).closest('.add-resource').hide();
        // $(this).closest('.add-resource').prev('.add-plus-resource').show();
    });

});
/*----------------------bootstrap-datetimepicker--------------------------*/
	//for 新增資料內的日期_趨勢應用、國內外資源
	$('.date').datetimepicker({pickTime: false});
	//for 新增資料內的日期_相關活動
	$('.start_time').datetimepicker({pickTime: false});
	$('.end_time').datetimepicker({pickTime: false});
	dateTimeLimit();
	//設定起迄日期的限制
	function dateTimeLimit(){
		$(".start_time").on("dp.change",function (e) {
			$(this).parent('div').next('div').find('.end_time').data("DateTimePicker").setMinDate(e.date);
		});
		$(".end_time").on("dp.change",function (e) {
			$(this).parent('div').prev('div').find('.start_time').data("DateTimePicker").setMaxDate(e.date);
		});	
	}
	
/*------------------------- DotDotDot ----------------------------------*/
/*
$(document).ready(function() {
	$(".track-card .content-meta").dotdotdot({
		//	configuration goes here
		wrap: 'letter',
		height: 'null',
		watch: true
	});
	$(".course-card .content-meta").dotdotdot({
		//	configuration goes here
		wrap: 'letter',
		height: 'null',
		watch: true
	});
});
*/
/*---------------------- editPortfolio ----------------------------*/
switchDiv(".portfolio-sidebar [name='recruiters']","");
////勾選核取方塊時，改變該方塊的value
onCheckboxChecked(".education [name='education_untilNow[]']");
onCheckboxChecked(".work_experience [name='work_untilNow[]']");
//設定載入頁面後，移除按鈕的Class，並註冊移除按鈕事件
resetRemoveButtonClass('.remove_experience'	,'.work_experience'	,"[name='experience_open_checkbox']");
resetRemoveButtonClass('.remove_award'		,'.award'			,"[name='skill_open_checkbox']"		);
resetRemoveButtonClass('.remove_license'	,'.license'			,"[name='skill_open_checkbox']"		);
resetRemoveButtonClass('.remove_portfolio'	,'.portfolio'		,"[name='portfolio_open_checkbox']"	);
resetRemoveButtonClass('.remove_education'	,'.education'		,"[name='education_open_checkbox']"	);
resetRemoveButtonClass('.remove_skill'		,'.skill'			,"[name='education_open_checkbox']"	);
resetRemoveButtonClass('.remove_training'	,'.training'		,"[name='training_open_checkbox']"	);
resetRemoveButtonClass('.remove_career_skill','.career_skill'	,"[name='career_skill_open_checkbox']"	);
resetRemoveButtonClass('.remove_seniority','.seniority'			,"[name='seniority_open_checkbox']"	);
//註冊新增按鈕事件
bindAddColumn('#add_experience'	,'.remove_experience'	,'.work_experience'	,"[name='experience_open_checkbox']",'						<div class="work_experience"><hr /> 							<div class="form-group"> 								<div class="controls form-inline col-sm-12"> 									<label class="col-sm-2 control-label" for="company_name">單位名稱</label> 									<div class="col-sm-4"> 										<input class="form-control" type="text" id="company_name" name="company_name[]" placeholder="請填入單位名稱" value=""> 									</div> 									<label class="col-sm-2 control-label" for="job_title">職務名稱</label> 									<div class="col-sm-4"> 										<input class="form-control" type="text" id="job_title" name="job_title[]" placeholder="請填入職務名稱" value=""> 									</div> 								</div> 							</div> 							<div class="form-group"> 								<div class="controls form-inline col-sm-12"> 									<label class="col-sm-2 control-label" for="experience_start">開始年度</label> 									<div class="col-sm-4"> 				                        <select id="experience_start" name="experience_start[]" class="form-control"> 											<option value="0" selected>請選擇開始年度</option> 				                        </select> 									</div> 									<div class="col-sm-3"> 				                        <select id="experience_start_month" name="experience_start_month[]" class="form-control"> 											<option value="0" selected>請選擇開始月份</option> 				                        </select> 									</div> 								</div> 							</div> 							<div class="form-group"> 								<div class="controls form-inline col-sm-12"> 									<label class="col-sm-2 control-label" for="experience_end">結束年度</label> 									<div class="col-sm-4"> 				                        <select id="experience_end" name="experience_end[]" class="form-control"> 											<option value="0" selected>請選擇結束年度</option> 				                        </select> 									</div> 									<div class="col-sm-3"> 				                        <select id="experience_end_month" name="experience_end_month[]" class="form-control"> 											<option value="0" selected>請選擇結束月份</option> 				                        </select> 									</div> 								</div> 							</div> 							<div class="pull-right"> 								<a class="remove_experience" href="javascript:void(0);"></a> 							</div> 						</div>');
bindAddColumn('#add_award'		,'.remove_award'		,'.award'			,"[name='skill_open_checkbox']"		,'						<div class="form-group form-inline award"> 							<label for="awards" class="col-sm-2 control-label">獲獎記錄</label> 							<div class="col-sm-6"> 								<input class="form-control" type="text" id="awards" name="awards[]" placeholder="請填入獲獎記錄" value=""> 							</div> 							<a class="remove_award" href="javascript:void(0);"></a> 						</div>');
bindAddColumn('#add_license'	,'.remove_license'		,'.license'			,"[name='skill_open_checkbox']"		,'						<div class="form-group license"> 							<label for="licenses" class="col-sm-2 control-label">專業證照</label> 							<div class="col-sm-6"> 								<input class="form-control" type="text" id="licenses" name="licenses[]" placeholder="請填入專業證照" value=""> 							</div> 							<a class="remove_license" href="javascript:void(0);"></a> 						</div>');
bindAddColumn('#add_portfolio'	,'.remove_portfolio'	,'.portfolio'		,"[name='portfolio_open_checkbox']"	,'						<div class="portfolio well"> 							<div class="form-group"> 								<div class="controls form-inline"> 									<label for="portfolio_name" class="col-sm-2 control-label">作品名稱</label> 									<div class="col-sm-4"> 										<input class="form-control" type="text" id="portfolio_name" name="portfolio_name[]" placeholder="請填入作品名稱" value=""> 									</div> 									<label for="portfolio_pic" class="col-sm-2 control-label">作品圖檔</label> 									<div class="col-sm-4"> 										<input type="file" id="portfolio_pic" name="portfolio_pic[]"> 									</div> 								</div> 							</div> 			                <div class="form-group"> 			                    <label for="portfolio_type" class="col-sm-2 control-label">作品類型</label> 			                    <div class="col-sm-6"> 			                        <select id="portfolio_type" name="portfolio_type[]" class="form-control"> 										<option value="0" selected>請選擇作品類型</option> 										<option value="1">圖片</option> 										<option value="2">Youtube影片</option> 										<option value="3">SlideShare</option> 										<option value="4">外連網址</option> 			                        </select> 			                    </div> 			                </div> 					                <div class="form-group"> 					                    <label for="portfolio_skill" class="col-sm-2 control-label">使用技術</label> 					                     <div> 						                     <div class="col-sm-6" style="cursor: pointer;"> 							                    <div class="use_skill_div thumbnail" style="line-height:25px;min-height:34px;min-width:100%;"> 							                    </div> 						                     </div> 					                     </div> 					                    <input name="portfolio_skill[]" type="hidden" value=""/> 					                </div> 							<div class="form-group"> 								<label for="hyperlink" class="col-sm-2 control-label">超連結</label> 								<div class="col-sm-6"> 									<input class="form-control" type="text" id="hyperlink" name="hyperlink[]" placeholder="請填入超連結" value=""> 								</div> 							</div> 							<div class="form-group"> 								<label for="portfolio_desc" class="col-sm-2 control-label">作品敘述</label> 								<div class="col-sm-6"> 									<textarea class="col-sm-10 form-control" id="portfolio_desc" name="portfolio_desc[]" rows="5" placeholder="請填入作品敘述"></textarea> 								</div> 							</div> 							<div class="pull-right"> 								<a class="remove_portfolio" href="javascript:void(0);"></a> 							</div> 						</div>');
bindAddColumn('#add_education'	,'.remove_education'	,'.education'		,"[name='education_open_checkbox']"	,'						<div class="education"><hr /><div class="form-group"> <div class="controls form-inline col-sm-12"> <label for="degree" class="col-sm-2 control-label">學歷</label> <div class="col-sm-4"> <select id="degree" name="degree[]" class="form-control"> <option value="0" selected>請選擇學歷</option> <option value="1">高中職</option> <option value="2">大學</option> <option value="3">碩士</option> <option value="4">博士</option> </select> </div> </div> </div> 							<div class="form-group"> 								<div class="controls form-inline col-sm-12"> 									<label class="col-sm-2 control-label" for="school_name">學校名稱</label> 									<div class="col-sm-4"> 										<input class="form-control" type="text" id="school_name" name="school_name[]" placeholder="請填入學校名稱" value=""> 									</div> 									<label class="col-sm-2 control-label" for="department">科系名稱</label> 									<div class="col-sm-4"> 										<input class="form-control" type="text" id="department" name="department[]" placeholder="請填入科系名稱" value=""> 									</div> 								</div> 							</div> 							<div class="form-group"> 								<div class="controls form-inline col-sm-12"> 									<label class="col-sm-2 control-label" for="education_start">開始</label> 									<div class="col-sm-4"> 				                        <select id="education_start" name="education_start[]" class="form-control"> 											<option value="0" selected>請選擇開始年度</option> 				                        </select> 									</div> 									<div class="col-sm-3"> 				                        <select id="education_start_month" name="education_start_month[]" class="form-control"> 											<option value="0" selected>請選擇開始月份</option> 				                        </select> 									</div> 								</div> 							</div> 							<div class="form-group"> 								<div class="controls form-inline col-sm-12"> 									<label class="col-sm-2 control-label" for="education_end">結束</label> 									<div class="col-sm-4"> 				                        <select id="education_end" name="education_end[]" class="form-control"> 											<option value="0" selected>請選擇結束年度</option> 				                        </select> 									</div> 									<div class="col-sm-3"> 				                        <select id="education_end_month" name="education_end_month[]" class="form-control"> 											<option value="0" selected>請選擇結束月份</option> 				                        </select> 									</div> 									<div class="col-sm-3"> 										<div class="radio"> 										  <label> 										    <input type="radio" name="education_untilNow[]" value="1"> 											畢業 										  </label> 										</div> 										<div class="radio"> 										  <label> 										    <input type="radio" name="education_untilNow[]" value="2"> 											肄業 										  </label> 										</div> 										<div class="pull-right"> 											<a class="remove_education" href="javascript:void(0);"></a> 										</div> 									</div> 								</div> 							</div> 						</div>');
bindAddColumn('#add_skill'		,'.remove_skill'		,'.skill'			,"[name='skill_open_checkbox']"		,'					<div class="skill form-group"> 						<label for="skill_name" class="col-sm-2 control-label">專業技能</label> 						<div class="col-sm-6"> 							<input type="text" class="form-control" id="skill_name" name="skill_name[]" placeholder="請填入專業技能，如Java、JavaScript等" value=""> 						</div> 						<a class="remove_skill" href="javascript:void(0);"></a> 					</div>');
bindAddColumn('#add_training'	,'.remove_training'		,'.training'		,"[name='training_open_checkbox']"	,'						<div class="training"> 							<div class="form-group"> 								<div class="controls form-inline col-sm-12"> 									<label class="col-sm-2 control-label" for="training_name">訓練名稱</label> 									<div class="col-sm-4"> 										<input class="form-control" type="text" id="training_name" name="training_name[]" placeholder="請填入專業訓練名稱" value=""> 									</div> 									<label class="col-sm-2 control-label" for="training_unit">訓練單位</label> 									<div class="col-sm-4"> 										<input class="form-control" type="text" id="training_unit" name="training_unit[]" placeholder="請填入訓練單位名稱" value=""> 									</div> 								</div> 							</div> 							<div class="form-group"> 								<div class="controls form-inline col-sm-12"> 									<label class="col-sm-2 control-label" for="training_start">開始年度</label> 									<div class="col-sm-4"> 				                        <select id="training_start" name="training_start[]" class="form-control"> 											<option value="0" selected>請選擇開始年度</option> 				                        </select> 									</div> 									<div class="col-sm-3"> 				                        <select id="training_start_month" name="training_start_month[]" class="form-control"> 											<option value="0" selected>請選擇開始月份</option> 				                        </select> 									</div> 								</div> 							</div> 							<div class="form-group"> 								<div class="controls form-inline col-sm-12"> 									<label class="col-sm-2 control-label" for="training_end">結束年度</label> 									<div class="col-sm-4"> 				                        <select id="training_end" name="training_end[]" class="form-control"> 											<option value="0" selected>請選擇結束年度</option> 				                        </select> 									</div> 									<div class="col-sm-3"> 				                        <select id="training_end_month" name="training_end_month[]" class="form-control"> 											<option value="0" selected>請選擇結束月份</option> 				                        </select> 									</div> 								</div> 							</div> 							<div class="pull-right"> 								<a class="remove_training" href="javascript:void(0);"></a> 							</div> 						</div>');
bindAddColumn('#add_career_skill','.remove_career_skill','.career_skill'	,"[name='career_skill_open_checkbox']"	,'					<div class="career_skill form-group"> 						<label for="career_skill_name" class="col-sm-2 control-label">專業技能</label> 						<div class="col-sm-6"> 							<input type="text" class="form-control" id="career_skill_name" name="career_skill_name[]" placeholder="請填入專業技能，如網路管理、伺服器維護等" value=""> 						</div> 						<a class="remove_career_skill" href="javascript:void(0);"></a> 					</div>');
bindAddColumn('#add_seniority'	,'.remove_seniority'	,'.seniority'		,"[name='seniority_open_checkbox']"	,'<div class="seniority"> <div class="form-group"> <div class="controls form-inline col-sm-12"> <label class="col-sm-2 control-label" for="seniority_category">職務類別</label> <div class="col-sm-4"> <select id="seniority_category" name="seniority_category[]" class="form-control"> <option value="0" selected>請選擇職務類別</option> <option value="2">APP設計</option> <option value="3">Web開發</option> <option value="4">巨量資料分析</option> </select> </div> <div class="col-sm-3"> <select id="year_of_experience" name="year_of_experience[]" class="form-control"> <option value="-1" selected>無經驗</option> <option value="0">未滿1年</option> <option value="1">1年以上</option> <option value="2">2年以上</option> <option value="3">3年以上</option> <option value="4">4年以上</option> <option value="5">5年以上</option> <option value="6">6年以上</option> <option value="7">7年以上</option> <option value="8">8年以上</option> <option value="9">9年以上</option> <option value="10">10年以上</option> </select> </div> </div> <div class="pull-right"> <a class="remove_seniority" href="javascript:void(0);"></a> </div> </div> </div>');
//重設radioButton的name
resetRadioName('.work_experience');
resetRadioName('.education');
resetRadioName('.portfolio');
resetRadioName('.training');
//註冊作品與成果，選擇使用技術事件
bindSelectSkillDiv();
bindRemoveSkillLabel();
function resetDisabledDiv(checkbox,targetDiv){
if($(checkbox).is(':checked')){
	$(targetDiv).show();
}else{
	$(targetDiv).hide();
}
}
function resetRemoveButtonClass(removeButton,target,checkbox){
if($(target).length==1){
	$(removeButton).removeClass('fa fa-trash-o');
}else if($(target).length>1){
	$(removeButton).addClass('fa fa-trash-o');
	bindRemoveColumn(removeButton,target,checkbox);
}
}
function bindAddColumn(addButton,removeButton,target,checkbox,content){
	if(target=='.seniority'){
		if($(target).length==3){
			$(addButton).addClass('hide');
		}
	}
	$(addButton).click(function(){
      if(target=='.seniority'){
        if($(target).length+1==3){
          $(addButton).addClass('hide');
        }
      }
			$(target+':last').after(content);
			bindRemoveColumn(removeButton,target,checkbox);
			if($(target).length>1){
				$(removeButton).addClass('fa fa-trash-o');
			}
			if(target=='.work_experience'||target=='.education'||target=='.portfolio'||target=='.training'){
				resetRadioName(target);
				createDateOption(target);
			}
			if(target=='.education'){
				validateResumeForm('.education');
			}else if(target=='.work_experience'){
				validateResumeForm('.work_experience');
			}else if(target=='.skill'){
				validateResumeForm('.skill');
			}else if(target=='.portfolio'){
				validateResumeForm('.portfolio');
				bindSelectSkillDiv();
				bindRemoveSkillLabel();
			}
	});
}
function bindRemoveColumn(removeButton,target,checkbox){
$(removeButton).click(function(){
    if(target=='.seniority'){
      if($(target).length+1!=3){
        $('#add_seniority').removeClass('hide');
      }
    }
		if($(target).length>=2){
			$(this).closest(target).remove();
		}
		if($(target).length==1){
			$(removeButton).removeClass('fa fa-trash-o');
		}
		if(target=='.work_experience'||target=='.education'||target=='.portfolio'){
			resetRadioName(target);
		}
		if(target=='.education'){
			validateResumeForm('.education');
		}else if(target=='.work_experience'){
			validateResumeForm('.work_experience');
		}else if(target=='.skill'){
			validateResumeForm('.skill');
		}else if(target=='.portfolio'){
			validateResumeForm('.portfolio');
		}
});
}

function switchDiv(checkbox,targetDiv){
$(checkbox).bootstrapSwitch();
$(checkbox).on('switch-change', function (e, data) {
	$(this).attr("checked", !$(this).attr("checked"));
	value = data.value;
	if(value){
		$(targetDiv).show();
	}else{
		$(targetDiv).hide();
	}
});
}
function onCheckboxChecked(checkbox){
	$(checkbox).change(function(){
		$(this).val(this.checked?'1':'0');
	});
}
function resetRadioName(target){
	if(target=='.work_experience'){			
		for(var i=0;i<$(target).length;i++){
			$(target+" [type='radio']").eq(2*i).attr('name','work_untilNow['+i+']');
			$(target+" [type='radio']").eq(2*i+1).attr('name','work_untilNow['+i+']');
			$(target+" select").eq(4*i).attr('name','experience_start['+i+']');
			$(target+" select").eq(4*i+1).attr('name','experience_start_month['+i+']');
			$(target+" select").eq(4*i+2).attr('name','experience_end['+i+']');
			$(target+" select").eq(4*i+3).attr('name','experience_end_month['+i+']');
			$(target+" [type='text']").eq(2*i).attr('name','company_name['+i+']');
			$(target+" [type='text']").eq(2*i+1).attr('name','job_title['+i+']');
		}
	}else if(target=='.education'){
		for(var i=0;i<$(target).length;i++){
			$(target+" [type='radio']").eq(2*i).attr('name','education_untilNow['+i+']');
			$(target+" [type='radio']").eq(2*i+1).attr('name','education_untilNow['+i+']');		
			$(target+" select").eq(5*i).attr('name','degree['+i+']');
			$(target+" select").eq(5*i+1).attr('name','education_start['+i+']');
			$(target+" select").eq(5*i+2).attr('name','education_start_month['+i+']');
			$(target+" select").eq(5*i+3).attr('name','education_end['+i+']');
			$(target+" select").eq(5*i+4).attr('name','education_end_month['+i+']');
			$(target+" [type='text']").eq(2*i).attr('name','school_name['+i+']');
			$(target+" [type='text']").eq(2*i+1).attr('name','department['+i+']');
		}
	}else if(target=='.portfolio'){
		for(var i=0;i<$(target).length;i++){
			$(target+" [type='file']").eq(i).attr('name','portfolio_pic['+i+']');
		}
	}else if(target=='.training'){			
		for(var i=0;i<$(target).length;i++){
			$(target+" select").eq(4*i).attr('name','training_start['+i+']');
			$(target+" select").eq(4*i+1).attr('name','training_start_month['+i+']');
			$(target+" select").eq(4*i+2).attr('name','training_end['+i+']');
			$(target+" select").eq(4*i+3).attr('name','training_end_month['+i+']');
			$(target+" [type='text']").eq(2*i).attr('name','training_name['+i+']');
			$(target+" [type='text']").eq(2*i+1).attr('name','training_unit['+i+']');
		}
	}
}
function createDateOption(target){
	if(target=='.work_experience'){
		for(var i=2014;i>=1950;i--){
			$(target+" [name='experience_start["+($(target).length-1)+"]'").append('<option value="'+i+'">西元'+i+' 年 / 民國'+(i-1911)+'年</option>');
			$(target+" [name='experience_end["+($(target).length-1)+"]'").append('<option value="'+i+'">西元'+i+' 年 / 民國'+(i-1911)+'年</option>');
		}
		for(var i=1;i<=12;i++){
			$(target+" [name='experience_start_month["+($(target).length-1)+"]'").append('<option value="'+i+'">'+i+' 月</option>');
			$(target+" [name='experience_end_month["+($(target).length-1)+"]'").append('<option value="'+i+'">'+i+' 月</option>');
		}
	}else if(target=='.education'){
		for(var i=2014;i>=1950;i--){
			$(target+" [name='education_start["+($(target).length-1)+"]'").append('<option value="'+i+'">西元'+i+' 年 / 民國'+(i-1911)+'年</option>');
			$(target+" [name='education_end["+($(target).length-1)+"]'").append('<option value="'+i+'">西元'+i+' 年 / 民國'+(i-1911)+'年</option>');
		}
		for(var i=1;i<=12;i++){
			$(target+" [name='education_start_month["+($(target).length-1)+"]'").append('<option value="'+i+'">'+i+' 月</option>');
			$(target+" [name='education_end_month["+($(target).length-1)+"]'").append('<option value="'+i+'">'+i+' 月</option>');
		}
	}else if(target=='.training'){
		for(var i=2014;i>=1950;i--){
			$(target+" [name='training_start["+($(target).length-1)+"]'").append('<option value="'+i+'">西元'+i+' 年 / 民國'+(i-1911)+'年</option>');
			$(target+" [name='training_end["+($(target).length-1)+"]'").append('<option value="'+i+'">西元'+i+' 年 / 民國'+(i-1911)+'年</option>');
		}
		for(var i=1;i<=12;i++){
			$(target+" [name='training_start_month["+($(target).length-1)+"]'").append('<option value="'+i+'">'+i+' 月</option>');
			$(target+" [name='training_end_month["+($(target).length-1)+"]'").append('<option value="'+i+'">'+i+' 月</option>');
		}
	}
}
/*----------------------editPortfolio sidebar----------------------------*/
//進入個人履歷後，先關閉所有分頁
jQuery(document).ready(function($){
	$('li>a[href="#resume-profile"]')	.parent().removeClass('active');
	$('li>a[href="#training"]')			.parent().removeClass('active');
	$('li>a[href="#experience"]')		.parent().removeClass('active');
	$('li>a[href="#skill"]')			.parent().removeClass('active');
	$('li>a[href="#product"]')			.parent().removeClass('active');
	$('li>a[href="#society"]')			.parent().removeClass('active');
	$('li>a[href="#autobiography"]')	.parent().removeClass('active');
	$('li>a[href="#career_skill"]')		.parent().removeClass('active');
	$target=location.href.replace('http://','').split('/');
	$pos=$target[$target.length-1].lastIndexOf('#');
	$label=$target[$target.length-1].substr($pos+1);
	//剛進入個人履歷，未選擇任何分頁時預設顯示基本資料分頁
	if($label=='editPortfolio'){
		$('li>a[href="#resume-profile"]')	.parent().addClass('active');
		$('.profile').removeClass('hide');
	}
	//重新整理頁面時，以網址列的字尾判斷要顯示個人履歷的哪個分頁，並且在特定分頁需驗證使用者填寫的資料
 	if($label=='resume-profile' || $label=='experience' || $label=='skill'|| $label=='product'|| $label=='society'|| $label=='autobiography'|| $label=='award'|| $label=='license'|| $label=='training'|| $label=='career_skill' ||$label=='performance'||$label=='condition'||$label=='analysis'){
		$('li>a[href=#'+$label+']').parent().addClass('active');
		if($label=='resume-profile'){
			$('.profile').removeClass('hide');
			validateResumeForm('.profile');
		}else if($label=='experience'){
			$('.experience_div').removeClass('hide');
			validateResumeForm('.work_experience');
			validateResumeForm('.education');
		}else if($label=='skill'){
			$('.skill_div').removeClass('hide');
			validateResumeForm('.skill');
		}else if($label=='product'){
			$('.portfolio_div').removeClass('hide');
			validateResumeForm('.portfolio');
		}else if($label=='society'){
			$('.society_div').removeClass('hide');
		}else if($label=='autobiography'){
			$('.autobiography_div').removeClass('hide');
		}else if($label=='award'){
			$('.award_div').removeClass('hide');
		}else if($label=='license'){
			$('.license_div').removeClass('hide');
		}else if($label=='training'){
			$('.training_div').removeClass('hide');
		}else if($label=='career_skill'){
			$('.career_skill_div').removeClass('hide');
		}else if($label=='performance'){
			$('.performance_div').removeClass('hide');
		}else if($label=='condition'){
			$('.condition_div').removeClass('hide');
 		}else if($label=='analysis'){
			$('.joeyc_div').removeClass('hide');
 		}
 	}
	//這個map儲存點擊左方sidebar,顯示右方分頁的對應關係
 	//點擊左方sidebar時，在右方顯示對應的分頁
	portfolio_map={};
	portfolio_map['#resume-profile']='.profile';
	portfolio_map['#experience']	='.experience_div';
	portfolio_map['#career_skill']	='.career_skill_div';
	portfolio_map['#skill']			='.skill_div';
	portfolio_map['#award']			='.award_div';
	portfolio_map['#license']		='.license_div';
	portfolio_map['#autobiography']	='.autobiography_div';
	portfolio_map['#product']		='.portfolio_div';
	portfolio_map['#society']		='.society_div';
	portfolio_map['#performance']	='.performance_div';
	portfolio_map['#condition']		='.condition_div';
	portfolio_map['#analysis']		='.joeyc_div';
	$('.portfolio-sidebar li>a').click(function(){
		$('.portfolio-sidebar li>a').parent().removeClass('active');
		$(this).parent().addClass('active');
		$('#editProfileForm .item-container').addClass('hide');
		$(portfolio_map[$(this).attr('href')]).removeClass('hide');
		validateResumeForm('.profile');
		validateResumeForm('.education');
		validateResumeForm('.work_experience');
		validateResumeForm('.skill');
		validateResumeForm('.society');
		validateResumeForm('.portfolio');
	});
	//在切換履歷狀態時，若使用者在分頁當中有填寫錯誤的資料，自動切換至該分頁，若無則切換履歷狀態
	$("[name='recruiters']").change(function(event){
		validateResumeForm('.profile');
		validateResumeForm('.education');
		validateResumeForm('.work_experience');
		validateResumeForm('.skill');
		validateResumeForm('.society');
		validateResumeForm('.portfolio');
		  if($('.profile div.has-error').length>0){
			  $('li>a[href="#resume-profile"]').click();
			  event.preventDefault();
		  }else if($('.education div.has-error').length>0){
			  $('li>a[href="#experience"]').click();
			  event.preventDefault();
		  }else if($('.work_experience div.has-error').length>0){
			  $('li>a[href="#experience"]').click();
			  event.preventDefault();
		  }else if($('.skill div.has-error').length>0){
			  $('li>a[href="#skill"]').click();
			  event.preventDefault();
		  }else if($('.society div.has-error').length>0){
			  $('li>a[href="#society"]').click();
			  event.preventDefault();
		  }else if($('.portfolio div.has-error').length>0){
			  $('li>a[href="#product"]').click();
			  event.preventDefault();
		  }else if($(".profile input[name='name']").val()===''){
			  $(".profile input[name='name']").parent().addClass('has-error');
			  $('li>a[href="#resume-profile"]').click();
			  event.preventDefault();
		  }else{
			  var recruiters=$.parseJSON($(this).attr('data-button')).recruiters;
			  $.ajax({
				  url: base_url + 'user/toggleResumeFlag',
				  type:'POST',
				  data:{recruiters:recruiters}
			  });
			  if($(this).is(':checked')){
				  $(this).attr('data-button','{"recruiters":"0"}');
			  }else{
				  $(this).attr('data-button','{"recruiters":"1"}');
			  }
		  }
	});
	});
//點擊portfolio.php 中的個人作品縮圖時，依據作品類型做不同動作
$('#portfolio-content .portfolio .thumbnail').click(function(){
	var portfolio_name	=$.parseJSON($(this).attr('data-button')).portfolio_name;
	var portfolio_pic	=$.parseJSON($(this).attr('data-button')).portfolio_pic;
	var portfolio_desc	=$.parseJSON($(this).attr('data-button')).portfolio_desc;
	var hyperlink		=$.parseJSON($(this).attr('data-button')).hyperlink;
	var portfolio_type	=$.parseJSON($(this).attr('data-button')).portfolio_type;
	var portfolio_order	=$.parseJSON($(this).attr('data-button')).portfolio_order;
	var alias			=$.parseJSON($(this).attr('data-button')).alias;
	var portfolio_skill	=$.parseJSON($(this).attr('data-button')).portfolio_skill;
	var skill_array		=portfolio_skill.split(',');
	var skill_content='';
	for(var i=0;i<skill_array.length;i++){
		if(skill_array[i]!=''){
			skill_content+=('<span class="label label-default">'+skill_array[i]+'</span> ');
		}
	}
	//圖片
	if(portfolio_type==1){
		$('#portfolio-content #pop-portfolio-modal .modal-header').empty().append('<div>'+portfolio_name+'</div>');
		//有上傳照片，未填使用技能
		if(skill_content=='' && portfolio_pic!='browser.png'){
			$('#portfolio-content #pop-portfolio-modal .modal-body').empty().append('<center><div><img src="'+base_url+'assets/img/user/portfolio/'+alias+'/origin/'+portfolio_pic+'" alt="'+portfolio_name+'" width="500"></div></center>'+'<div>作品敘述</div><hr><div>'+(portfolio_desc==''?'作者目前沒有填寫關於這個作品的敘述。':portfolio_desc)+'</div>');
		//有上傳照片，有填使用技能
		}else if(skill_content!='' && portfolio_pic!='browser.png'){
			$('#portfolio-content #pop-portfolio-modal .modal-body').empty().append('<center><div><img src="'+base_url+'assets/img/user/portfolio/'+alias+'/origin/'+portfolio_pic+'" alt="'+portfolio_name+'" width="500"></div></center>'+'<div>使用技術</div><hr><div>'+skill_content+'</div><hr>'+'<div>作品敘述</div><hr><div>'+(portfolio_desc==''?'作者目前沒有填寫關於這個作品的敘述。':portfolio_desc)+'</div>');
		//未上傳照片，未填使用技能
		}else if(skill_content=='' && portfolio_pic=='browser.png'){
			$('#portfolio-content #pop-portfolio-modal .modal-body').empty().append('<center><div><img src="'+base_url+'assets/img/user/portfolio/'+portfolio_pic+'" alt="'+portfolio_name+'" width="500"></div></center>'+'<div>作品敘述</div><hr><div>'+(portfolio_desc==''?'作者目前沒有填寫關於這個作品的敘述。':portfolio_desc)+'</div>');
		//未上傳照片，有填使用技能
		}else if(skill_content!='' && portfolio_pic=='browser.png'){
			$('#portfolio-content #pop-portfolio-modal .modal-body').empty().append('<center><div><img src="'+base_url+'assets/img/user/portfolio/'+portfolio_pic+'" alt="'+portfolio_name+'" width="500"></div></center>'+'<div>使用技術</div><hr><div>'+skill_content+'</div><hr>'+'<div>作品敘述</div><hr><div>'+(portfolio_desc==''?'作者目前沒有填寫關於這個作品的敘述。':portfolio_desc)+'</div>');
		}
		$('#portfolio-content #pop-portfolio-modal .modal-footer').empty().append('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');			
		$('#pop-portfolio-modal').modal();
	//影片
	}else if(portfolio_type==2){
		$('#portfolio-content #pop-portfolio-modal .modal-header').empty().append('<div>'+portfolio_name+'</div>');
		if(skill_content==''){
			$('#portfolio-content #pop-portfolio-modal .modal-body').empty().append('<center><iframe width="500" height="315" src="'+hyperlink+'" frameborder="0" allowfullscreen></iframe></center>'+'<div>作品敘述</div><hr><div>'+(portfolio_desc==''?'作者目前沒有填寫關於這個作品的敘述。':portfolio_desc)+'</div>');			
		}else{
			$('#portfolio-content #pop-portfolio-modal .modal-body').empty().append('<center><iframe width="500" height="315" src="'+hyperlink+'" frameborder="0" allowfullscreen></iframe></center>'+'<div>使用技術</div><hr><div>'+skill_content+'</div><hr>'+'<div>作品敘述</div><hr><div>'+(portfolio_desc==''?'作者目前沒有填寫關於這個作品的敘述。':portfolio_desc)+'</div>');			
		}
		$('#portfolio-content #pop-portfolio-modal .modal-footer').empty().append('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');			
		$('#pop-portfolio-modal').modal();
	//SlideShare
	}else if(portfolio_type==3){
		$('#portfolio-content #pop-portfolio-modal .modal-header').empty().append('<div>'+portfolio_name+'</div>');
		if(skill_content==''){
			$('#portfolio-content #pop-portfolio-modal .modal-body').empty().append('<center><iframe src="'+hyperlink+'" width="500" height="315" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px 1px 0; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe></center>'+'<div>作品敘述</div><hr><div>'+(portfolio_desc==''?'作者目前沒有填寫關於這個作品的敘述。':portfolio_desc)+'</div>');
		}else{
			$('#portfolio-content #pop-portfolio-modal .modal-body').empty().append('<center><iframe src="'+hyperlink+'" width="500" height="315" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px 1px 0; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe></center>'+'<div>使用技術</div><hr><div>'+skill_content+'</div><hr>'+'<div>作品敘述</div><hr><div>'+(portfolio_desc==''?'作者目前沒有填寫關於這個作品的敘述。':portfolio_desc)+'</div>');
		}
		$('#portfolio-content #pop-portfolio-modal .modal-footer').empty().append('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');			
		$('#pop-portfolio-modal').modal();
	//外部連結
	}else if(portfolio_type==4){
		$('#portfolio-content #pop-portfolio-modal .modal-header').empty().append('<div>'+portfolio_name+'</div>');
		//有上傳照片，未填使用技能
		if(skill_content=='' && portfolio_pic!='browser.png'){
			$('#portfolio-content #pop-portfolio-modal .modal-body').empty().append('<center><div><img src="'+base_url+'assets/img/user/portfolio/'+alias+'/origin/'+portfolio_pic+'" alt="'+portfolio_name+'" width="500"></div></center>'+'<div>作品敘述</div><hr><div>'+(portfolio_desc==''?'作者目前沒有填寫關於這個作品的敘述。':portfolio_desc)+'</div>');
		//有上傳照片，有填使用技能
		}else if(skill_content!='' && portfolio_pic!='browser.png'){
			$('#portfolio-content #pop-portfolio-modal .modal-body').empty().append('<center><div><img src="'+base_url+'assets/img/user/portfolio/'+alias+'/origin/'+portfolio_pic+'" alt="'+portfolio_name+'" width="500"></div></center>'+'<div>使用技術</div><hr><div>'+skill_content+'</div><hr>'+'<div>作品敘述</div><hr><div>'+(portfolio_desc==''?'作者目前沒有填寫關於這個作品的敘述。':portfolio_desc)+'</div>');
		//未上傳照片，未填使用技能
		}else if(skill_content=='' && portfolio_pic=='browser.png'){
			$('#portfolio-content #pop-portfolio-modal .modal-body').empty().append('<center><div><img src="'+base_url+'assets/img/user/portfolio/'+portfolio_pic+'" alt="'+portfolio_name+'" width="500"></div></center>'+'<div>作品敘述</div><hr><div>'+(portfolio_desc==''?'作者目前沒有填寫關於這個作品的敘述。':portfolio_desc)+'</div>');
		//未上傳照片，有填使用技能
		}else if(skill_content!='' && portfolio_pic=='browser.png'){
			$('#portfolio-content #pop-portfolio-modal .modal-body').empty().append('<center><div><img src="'+base_url+'assets/img/user/portfolio/'+portfolio_pic+'" alt="'+portfolio_name+'" width="500"></div></center>'+'<div>使用技術</div><hr><div>'+skill_content+'</div><hr>'+'<div>作品敘述</div><hr><div>'+(portfolio_desc==''?'作者目前沒有填寫關於這個作品的敘述。':portfolio_desc)+'</div>');
		}
		if(hyperlink!=''){
			$('#portfolio-content #pop-portfolio-modal .modal-footer').empty().append('<a href="'+hyperlink+'" class="btn btn-success">前往連結</a><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
		}else{
			$('#portfolio-content #pop-portfolio-modal .modal-footer').empty().append('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');			
		}
		$('#pop-portfolio-modal').modal();
	}
});
//點擊個人作品時，處理iframe影片內容，避免出現重複音軌
$('#pop-portfolio-modal').on('hide.bs.modal', function (e) {
	if($('#pop-portfolio-modal iframe').length>0){
		$youtube_src=$('#pop-portfolio-modal iframe').attr('src');
		$('#pop-portfolio-modal iframe').attr('src','');
		$youtube_src=$('#pop-portfolio-modal iframe').attr('src',$youtube_src);
	}
});
/*---------------------修改密碼---------------------------*/
jQuery(document).ready(function($){
  $("#modifyPassword").validate({
      highlight: function(element, errorClass, validClass) {
        $(element).parent().addClass(errorClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parent().removeClass(errorClass);
       },
    errorElement: "span",
    errorClass: "has-error",
    errorPlacement: function(error, element) {
          $(error).addClass("label label-danger").insertAfter($(element).parent());
        },
    rules: {
    	oldPassword: {
        required: true,
        minlength: 5,
        maxlength:15
      },
        newPassword: {
        required: true,
        minlength: 5,
        maxlength:15
      },
        checkPassword: {
        required: true,
        minlength: 5,
        maxlength:15,
        equalTo: "#newPassword"
      }
    },
    messages: {
    	oldPassword: {
        required: "請輸入舊密碼",
        minlength: "密碼至少需要五個字",
        maxlength: "密碼最多十五個字"
      },
        newPassword: {
        required: "請輸入新密碼",
        minlength: "密碼至少需要五個字",
        maxlength: "密碼最多十五個字"
      },
        checkPassword: {
        required: "請確認新密碼",
        minlength: "密碼至少需要五個字",
        maxlength: "密碼最多十五個字",
        equalTo: "密碼不正確"
      }
     }
  });
});
/*---------------------熱門討論以及最新討論切換-----------------*/
 $('#myTab h3 a').click(function (e) {
   e.preventDefault();
   $(".discussion-flag").removeClass('active');
   $(this).addClass('active');
 });
/*---------------------發問問題--------------------*/
jQuery(document).ready(function($){
  $("#ajaxStartDiscuss , #startDiscuss , #collegeStartDiscuss").validate({
      highlight: function(element, errorClass, validClass) {
        $(element).parent().addClass(errorClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parent().removeClass(errorClass);
       },
    errorElement: "span",
    errorClass: "has-error",
    errorPlacement: function(error, element) {
          $(error).addClass("label label-danger").insertAfter($(element).parent());
        },
    rules: {
    	forum_topic: {
        required: true
      },
        forum_content: {
        required: true
      }
    },
    messages: {
    	forum_topic: {
        required: "請輸入問題標題"
      },
        forum_content: {
        required: "請輸入問題描述"
      }
     }
  });
});
/*---------------------編輯履歷表單驗證---------------------------*/
function validateResumeForm(target){
	if(target=='.profile'){
	    $("#editProfileForm "+target).validate({
	        highlight: function(element, errorClass, validClass) {
	          $(element).parent().addClass(errorClass);
	        },
	        unhighlight: function(element, errorClass, validClass) {
	          $(element).parent().removeClass(errorClass);
	         },
	      errorElement: "span",
	      errorClass: "has-error",
	      errorPlacement: function(error, element) {
	           $(error).addClass("label label-danger").insertAfter($(element).parent());
	          },
	      rules: {
	      	name: {
	          required: true
	        }
	      },
	      messages: {
	      	name: {
	          required:"請輸入姓名"
	        }
	       }
	      });
	}else if(target=='.work_experience'){
			$("#editProfileForm "+target).validate();
			$("[name^=company_name]").each(function(){
				$(this).rules("add", {
					maxlength:50,
					messages: {
						maxlength: "單位名稱最長50個字元"
					}
				});   
			});
			$("[name^=job_title]").each(function(){
				$(this).rules("add", {
					required: true,
					maxlength:50,
					messages: {
						required: "請輸入職務名稱",
						maxlength: "職務名稱最長50個字元"
					}
				});   
			});
	}else if(target=='.education'){
			$("#editProfileForm "+target).validate();
			$("[name^=school_name]").each(function(){
				$(this).rules("add", {
					maxlength:50,
					messages: {
						maxlength:"學校名稱最長50個字元"
					}
				});   
			});
			$("[name^=department]").each(function(){
				$(this).rules("add", {
					maxlength:50,
					messages: {
						maxlength:"科系名稱最長50個字元"
					}
				});   
			});
	}else if(target=='.skill'){
		$("#editProfileForm .skill_div .skill").validate();
		$("[name^=skill_name]").each(function(){
			$(this).rules("add", {
				maxlength:20,
				messages: {
					maxlength: "專業技能名稱最長20個字元"
					
				}
			});   
		});
	}else if(target=='.society'){
		$("#editProfileForm .society_div").validate();
		$(".society_div input").each(function(){
			$(this).rules("add", {
				maxlength:1024,
				messages: {
					maxlength:"社群連結最長1024個字元",
				}
			});
		});
	}else if(target=='.portfolio'){
		if($('.portfolio').length>1){
			$("#editProfileForm .portfolio_div .portfolio").validate();
			$("[name^=portfolio_name]").each(function(){
				$(this).rules("add", {
					maxlength:100,
					messages: {
						maxlength:"作品名稱最長100個字元"
					}
				});   
			});
			$("[name^=hyperlink]").each(function(){
				$(this).rules("add", {
					maxlength: 1024,
					messages: {
						maxlength: "作品連結最長1024個字元"
					}
				});
			});
		}
	}
}
/*----------------------portfolio Nav Bar----------------------------*/
jQuery(document).ready(function($){
	$('#portfolio-content li>a[href="#video-resume"]').click(function(event){
		event.preventDefault();
		$('#portfolio-content li>a[href="#video-resume"]')	.parent().addClass('active');
		$('#portfolio-content li>a[href="#portfolio"]')		.parent().removeClass('active');
		$('#portfolio-content li>a[href="#autobiography"]')	.parent().removeClass('active');
		$('#portfolio-content li>a[href="#performance"]')	.parent().removeClass('active');
		$('#portfolio-content .video-resume')	.removeClass('hide');
		$('#portfolio-content .portfolio')		.addClass('hide');
		$('#portfolio-content .autobiography')	.addClass('hide');
		$('#portfolio-content .performance')	.addClass('hide');
		$('.portfolio-right .portfolio-right-edit').attr('href',base_url+'user/editPortfolio#video-resume');
		$('.portfolio-right .portfolio-right-edit').html('編輯影音履歷');
	});
	$('#portfolio-content li>a[href="#portfolio"]').click(function(event){
		event.preventDefault();
		$('#portfolio-content li>a[href="#video-resume"]')	.parent().removeClass('active');
		$('#portfolio-content li>a[href="#portfolio"]')		.parent().addClass('active');
		$('#portfolio-content li>a[href="#autobiography"]')	.parent().removeClass('active');
		$('#portfolio-content li>a[href="#performance"]')	.parent().removeClass('active');
		$('#portfolio-content .video-resume')	.addClass('hide');
		$('#portfolio-content .portfolio')		.removeClass('hide');
		$('#portfolio-content .autobiography')	.addClass('hide');
		$('#portfolio-content .performance')	.addClass('hide');
		$('.portfolio-right .portfolio-right-edit').attr('href',base_url+'user/editPortfolio#product');
		$('.portfolio-right .portfolio-right-edit').html('編輯作品與成果');
	});
	$('#portfolio-content li>a[href="#autobiography"]').click(function(event){
		event.preventDefault();
		$('#portfolio-content li>a[href="#video-resume"]')	.parent().removeClass('active');
		$('#portfolio-content li>a[href="#portfolio"]')		.parent().removeClass('active');
		$('#portfolio-content li>a[href="#autobiography"]')	.parent().addClass('active');
		$('#portfolio-content li>a[href="#performance"]')	.parent().removeClass('active');
		$('#portfolio-content .video-resume')	.addClass('hide');
		$('#portfolio-content .portfolio')		.addClass('hide');
		$('#portfolio-content .autobiography')	.removeClass('hide');
		$('#portfolio-content .performance')	.addClass('hide');
		$('.portfolio-right .portfolio-right-edit').attr('href',base_url+'user/editPortfolio#autobiography');
		$('.portfolio-right .portfolio-right-edit').html('編輯自傳');
	});
	$('#portfolio-content li>a[href="#performance"]').click(function(event){
		event.preventDefault();
		$('#portfolio-content li>a[href="#video-resume"]')	.parent().removeClass('active');
		$('#portfolio-content li>a[href="#portfolio"]')		.parent().removeClass('active');
		$('#portfolio-content li>a[href="#autobiography"]')	.parent().removeClass('active');
		$('#portfolio-content li>a[href="#performance"]')	.parent().addClass('active');
		$('#portfolio-content .video-resume')	.addClass('hide');
		$('#portfolio-content .portfolio')		.addClass('hide');
		$('#portfolio-content .autobiography')	.addClass('hide');
		$('#portfolio-content .performance')	.removeClass('hide');
		$('.portfolio-right .portfolio-right-edit').attr('href',base_url+'user/editPortfolio#performance');
		$('.portfolio-right .portfolio-right-edit').html('編輯學習表現');
	});
});
/*----------------------若履歷編輯頁有提示錯誤，禁止送出表單，並回到該錯誤的分頁----------------------------*/
// $('#editProfileForm').submit(function(event) {
// 	for(var tab in portfolio_map){
// 		$('li>a[href="'+tab+'"]').click();
// 		$(portfolio_map[tab]).validate().form();
// 	}
// 	  if($('.profile div.has-error').length>0){
// 		  $('li>a[href="#resume-profile"]').click();
// 		  event.preventDefault();
// 	  }else if($('.education div.has-error').length>0){
// 		  $('li>a[href="#experience"]').click();
// 		  event.preventDefault();
// 	  }else if($('.work_experience div.has-error').length>0){
// 		  $('li>a[href="#experience"]').click();
// 		  event.preventDefault();
// 	  }else if($('.skill div.has-error').length>0){
// 		  $('li>a[href="#skill"]').click();
// 		  event.preventDefault();
// 	  }else if($('.society div.has-error').length>0){
// 		  $('li>a[href="#society"]').click();
// 		  event.preventDefault();
// 	  }else if($('.portfolio div.has-error').length>0){
// 		  $('li>a[href="#product"]').click();
// 		  event.preventDefault();
// 	  }
// 	});
/*-------------------------------批次寄信----------------------*/
$("#csv").change(function(e) {
var ext = $("input#csv").val().split(".").pop().toLowerCase();
if($.inArray(ext, ["csv"]) == -1) {
	alert('請上傳副檔名為csv格式的檔案');
	return false;
}
if (e.target.files != undefined) {
	var reader = new FileReader();
	reader.onload = function(e) {
		var col=e.target.result.split("\n");
		var data1=data2=data3=data4="";
		var table="<table class='table'><tr><th>#</th><th>姓名</th><th>電子信箱</th><th>預設密碼</th></tr>";
		var end ="</table>";
		var count=1;
		for( var i=0;i<col.length-1;i++){
			var row=col[i].split(",");
			var index = row[0];
			var	name = row[1];
			var email  = row[2];
			if(email != undefined){
				var subemail = email.slice(0,email.indexOf("@"));
				if(subemail.length = 0){
					subemail ="";
				}else if(subemail.length > 15){
					subemail = email.slice(0,15);
				}else if(subemail.length < 5){
					subemail = "code"+subemail;
				}
				//var data1 = data1+index+",";
				email = email.replace('"', "");
				var data2 = data2+name+",";
				var data3 = data3+email+",";
				var data4 = data4+subemail+",";
				var tabledata = "<tr><td>"+count+"</td><td>"+name+"</td><td>"+email+"</td><td>"+subemail+"</td></tr>";
				var table = table + tabledata;
				count++;
			}
		}
		$("#name").val(data2.slice(0,-1));
		$("#email").val(data3.slice(0,-1));
		$("#password").val(data4.slice(0,-1));
		$("#table").empty();
		$("#table").append(table+end);
	}
	reader.readAsText(e.target.files.item(0));
}
return false;
});
/*----------------more區塊-----------------*/
// if(flag){
	// $('#myTab a[href="#'+flag+'"]').tab('show');
// }
$('.more').on("click",function(){
	// var more = $(this).attr('data-more');
	// var domain_url = $(this).attr('data-url');
	// //有登入的話寫入session，未登入的話跳出登入/註冊視窗
	// if(name){
        // location.href=base_url+'college/more'+more+'/'+domain_url;
	// }else{
		// $('#modal-login').modal();
	// }
	var more_type = $(this).attr('data-more');
	var jobs_category_url = $(this).attr('data-url');
	//有登入的話寫入session，未登入的話跳出登入/註冊視窗
	if(name){
        location.href=base_url+'track/'+more_type+'/'+jobs_category_url;
	}else{
		$('#modal-login').modal();
	}
})
/*-------------------------------履歷編輯頁流程按鈕----------------------*/
jQuery(document).ready(function($){
	$('#editProfileForm>div .next_div button').click(function(){
		if($(this).index('#editProfileForm>div .next_div button')<$('#editProfileForm>div .next_div button').length){
			var current=$(this).parent().parent().parent().parent().index('#editProfileForm>div');
			$('.portfolio-sidebar li a').eq(current+1).click();
			$(document).scrollTop(0);
		}
	});
	$('#editProfileForm>div .prev_div button').click(function(){
		if($(this).index('#editProfileForm>div .prev_div button')<$('#editProfileForm>div .prev_div button').length){
			var current=$(this).parent().parent().parent().parent().index('#editProfileForm>div');
			$('.portfolio-sidebar li a').eq(current-1).click();
			$(document).scrollTop(0);
		}
	});
});
/*-------------------------------履歷編輯頁成果與作品使用技術----------------------*/
var portfolio_skill=$('#select-skill-modal .modal-body').html();
$('#select-skill-modal .modal-body').empty();
var current;
var current_anchor;
function bindSelectSkillDiv(){
	$('.use_skill_div').click(function(){
		skill_array=$('input[name^="portfolio_skill"]').eq($(this).index('.use_skill_div')).val().split(",");
		$('#select-skill-modal .modal-body').empty().append(portfolio_skill);
		for(var key in skill_array){
			$('#select-skill-modal .modal-body button.btn[value="'+skill_array[key]+'"]').remove();
		}
		bindSkillButtonClick();
		current=$(this).index('.use_skill_div');
		$('#select-skill-modal').modal();
	});
}
function bindRemoveSkillLabel(){
	$('.use_skill_div span').click(function(){
		event.stopPropagation();
	});
	$('.use_skill_div a').click(function(){
		event.stopPropagation();
		current		=$(this).parent().index('.use_skill_div');
		current_anchor=$(this).index('.use_skill_div:eq('+current+') a');
		if(current!=-1 && current_anchor!=-1){			
			skill_array=$('input[name^="portfolio_skill"]').eq(current).val().split(",");
			skill_array.splice(current_anchor,1);
			var hiddenValue='';
			for(var i=0;i<skill_array.length;i++){
				if(i+1==skill_array.length){
					hiddenValue+=skill_array[i];				
				}else{
					hiddenValue+=(skill_array[i]+',');
				}
			}
			$('input[name^="portfolio_skill"]').eq(current).val(hiddenValue);
				$('.use_skill_div:eq('+current+') a')	.eq(current_anchor).remove();
				$('.use_skill_div:eq('+current+') span').eq(current_anchor).remove();
			}
	});	
}
function bindSkillButtonClick(){
	$('#select-skill-modal .modal-body button.btn').click(function(){
		$('.use_skill_div').eq(current).append('<span class="label label-default">'+$(this).html()+'</span> <a class="fa fa-times" href="javascript:void(0);"></a> ');
		bindRemoveSkillLabel();
		if($('input[name^="portfolio_skill"]').eq(current).val()==''){
			$('input[name^="portfolio_skill"]').eq(current).val($('input[name^="portfolio_skill"]').eq(current).val()+$(this).val());
		}else{
			$('input[name^="portfolio_skill"]').eq(current).val($('input[name^="portfolio_skill"]').eq(current).val()+','+$(this).val());			
		}
		$(this).addClass('hide');
	});		
}
/*-------------------------紀錄各種資源的流覽次數----------------------*/
jQuery(document).ready(function($){
	 //針對各種資源的紀錄
	 $('.record').on("click",function() {
	 	var type = $(this).attr('data-type');
	 	var id = $(this).attr('data-id');
	 	$.post(base_url + "backend/updateUserClicks",{type:type,id:id});
	 });
	 //針對modal的紀錄
	 $('.modal_record').on("click",function() {
	 	var type = $(this).attr('data-type');
	 	var id = $(this).attr('data-id');
	 	$.post(base_url + "backend/updateModalClicks",{type:type,id:id});
	 });
})
/*----------------------趨勢應用以及國內外資源出處 typeahead.js------------------------*/
var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;
    // an array that will be populated with substring matches
    matches = [];
    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');
    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        // the typeahead jQuery plugin expects suggestions to a
        // JavaScript object, refer to typeahead docs for more info
        matches.push({ value: str });
      }
    });
    cb(matches);
  };
};
jQuery(document).ready(function($){
	if(document.URL==base_url+"backend/listIndustry"){
		$.ajax({
			url: base_url + 'backend/getSource',
			type:'POST',
			async:false,
			data:{type:'industry'},
			success:function(data){
				var jsonData = JSON.parse(data);
				var length = jsonData.length;
				var states=[];
				for (var i = 0; i < length; i++) {
					states.push(jsonData[i]["industry_source"]);
				}
				$('.typeahead').typeahead({
  					hint: true,
  					highlight: true,
  					minLength: 1
  				},
				{
  					name: 'states',
  					displayKey: 'value',
	  				source: substringMatcher(states)
				});
			}	
		})
	}else if(document.URL==base_url+"backend/listResource"){
		$.ajax({
			url: base_url + 'backend/getSource',
			type:'POST',
			async:false,
			data:{type:'resource'},
			success:function(data){
				var jsonData = JSON.parse(data);
				var length = jsonData.length;
				var states=[];
				for (var i = 0; i < length; i++) {
					states.push(jsonData[i]["resource_source"]);
				}
				$('.typeahead').typeahead({
  					hint: true,
  					highlight: true,
  					minLength: 1
  				},
				{
  					name: 'states',
  					displayKey: 'value',
	  				source: substringMatcher(states)
				});
			}	
		})
	}
});
/*----------------------notification isread ajax------------------------*/
$('.notification-dropdown').on('hidden.bs.dropdown', function () {
	if(!$('.notification-dropdown').has('.notifications-count') || !$('span.notifications-count').hasClass('hide')){
		var unread_string='';
		var unread=$('.notification-dropdown input[name^="unread"]');
		for(var i=0;i<unread.length;i++){
			if(i+1==unread.length){			
				unread_string+=$(unread).eq(i).val();			
			}else{
				unread_string+=($(unread).eq(i).val()+',');			
			}
		};
		$.ajax({
			url: base_url + 'notifications/markNotificationReaded',
			type:'POST',
			complete:function(){
				$('span.notifications-count').html('');
			}
		});		
	}
	});
/*----------------------forum vote-best-answer------------------------*/
$(document).on('click','.vote-best-answer',function(event){
	var forum_topic_id = $(this).attr('data-topic-id');
	var forum_content_id = $(this).attr('vote-best-answer');
	var alias = $(".alias"+forum_content_id).text().trim();
	var forum_topic = $(".title"+forum_topic_id).text().trim();
	var forum_content = $(".dat"+forum_content_id).text().trim();
	//tinCanApiBestAnswer(forum_topic_id,forum_content_id,alias,forum_topic,forum_content);
	if($(this).closest('.discussion-answer-main').hasClass('discussion-answer-best')){
		//如果有class，就把他移除，消除topic的solved and content的best_answer
		$.ajax({
    		url: base_url + "forum/removeBestAnswer",
    		type:"POST",
    		async:false,
    		data: {forum_content_id: forum_content_id}
    	});
    	//$(this).closest('.discussion-answer-main').removeClass('discussion-answer-best');
    	location.reload();
	}else{
		//如果沒有class，先移除全部的class，再加上click本身的class，消除content的bestanswer
		$.ajax({
    		url: base_url + "forum/changeBestAnswer",
    		type:"POST",
    		async:false,
    		data: {forum_content_id: forum_content_id}
    	});
		//$('.discussion-answer-main').removeClass('discussion-answer-best');
		//$(this).closest('.discussion-answer-main').addClass('discussion-answer-best')
		location.reload();
	}
});
/*----------------------一小時內不得更改最佳解-----------------------------*/
jQuery(document).ready(function($){
    $('.wait-an-hour').on("click",function() {
    	bootbox.alert("您必須等候一段時間才能更改最佳解答！");
    });
});
/*----------------------forum vote-up and down------------------------*/
$(document).on('click','.vote-up a',function(event){
	if(name){
		var forum = $(this).attr('data-web');//發問那篇
		var forum_topic_id = $(this).attr('data-topic-id');
		var forum_content_id = $(this).attr('data-vote-id');
		var check_vote = checkVote(forum_content_id);
		var forum_topic = $(".title"+forum_topic_id).text().trim();
		var forum_content = $(".dat"+forum_content_id).text().trim();
		var alias = $(".alias"+forum_content_id).text().trim();
		if(check_vote=='0'){
			//尚未投過票，所以投票吧
			$(this).addClass('voted');
			var do_vote = doVote(forum_content_id,'1');
			if(!do_vote){
				login();
				//alert login window
				$('#modal-title').text('您必須登入才能投票');
				$('#modal-login').modal();
			}else{
				//tin-can for vote-up
				if(forum){
					tinCanApiVote(forum_topic_id,forum_content_id,forum,forum_content,alias,'1');
				}else{
					tinCanApiVote(forum_topic_id,forum_content_id,forum_topic,forum_content,alias,'1');
				}
			}
		}else if(check_vote=='1'){
			//已經投過票了，抱歉囉
			$(this).popover('show');
			setTimeout(function() {$('.vote a').popover('destroy');}, 1000);
		}
		var sum = getVote(forum_content_id);
		$(this).children('.vote-count').html(sum);
	}else{
		login();
		//alert login window
		$('#modal-title').text('您必須登入才能投票');
		$('#modal-login').modal();
	}
	
});
$(document).on('click','.vote-down a',function(event){
	if(name){
		var forum = $(this).attr('data-web');//發問那篇
		var forum_topic_id = $(this).attr('data-topic-id');
		var forum_content_id = $(this).attr('data-vote-id');
		var check_vote = checkVote(forum_content_id);
		var forum_topic = $(".title"+forum_topic_id).text().trim();
		var forum_content = $(".dat"+forum_content_id).text().trim();
		var alias = $(".alias"+forum_content_id).text().trim();
		if(check_vote=='0'){
			//尚未投過票，所以投票吧
			$(this).addClass('voted');
			var do_vote = doVote(forum_content_id,'-1');
			if(!do_vote){
				login();
				//alert login window
				$('#modal-title').text('您必須登入才能投票');
				$('#modal-login').modal();
			}else{
				//tin-can for vote-up
				if(forum){
					tinCanApiVote(forum_topic_id,forum_content_id,forum,forum_content,alias,'-1');
				}else{
					tinCanApiVote(forum_topic_id,forum_content_id,forum_topic,forum_content,alias,'-1');
				}
			}
		}else if(check_vote=='1'){
			//已經投過票了，抱歉囉
			$(this).popover('show');
			setTimeout(function() {$('.vote a').popover('destroy');}, 1000);
		}
		var sum = getVote(forum_content_id);
		$(this).parent().prev().find('.vote-count').html(sum);
	}else{
		login();
		//alert login window
		$('#modal-title').text('您必須登入才能投票');
		$('#modal-login').modal();
	}
});
//使用ajax取得vote的總票數
function getVote(forum_content_id){
	var sum = 
		$.ajax({
			url: base_url + "forum/getVote",
			type:"POST",
			async:false,
			data: {forum_content_id:forum_content_id}
		}).responseText;
	return sum;
}
//check使用者是否投過票
function checkVote(forum_content_id){
	var val = 
		$.ajax({
			url: base_url + "forum/checkVote",
			type:"POST",
			async:false,
			data: {forum_content_id:forum_content_id}
		}).responseText;
	return val;
}
//check使用者是否投過票
function doVote(forum_content_id,vote){
	var val =
		$.ajax({
			url: base_url + "forum/doVote",
			type:"POST",
			async:false,
			data: {forum_content_id: forum_content_id,vote: vote}
		}).responseText;
	return val;
}
//modal login
$(document).ready(function(){
	$("#modal-login-submit").click(function(){
		$("#loginForm").submit(function(e){
			var postData = $(this).serializeArray();
			var formURL = $(this).attr("action");
			$.ajax({
				url : formURL,
				type: "POST",
				data : postData,
				success:function(data, textStatus, jqXHR) {
					if(data=='login success'){
						location.reload();
					}else if(data=='login fail'){
						$(".message").html('<div class="alert alert-danger alert-dismissable">'+
  '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+
  '信箱或密碼不正確，請重新輸入'+
'</div>');
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$("#simple-msg").html('<pre><code class="prettyprint">AJAX Request Failed<br/> textStatus='+textStatus+', errorThrown='+errorThrown+'</code></pre>');
				}
			});
	    	e.preventDefault();	//STOP default action
	   	 	//e.unbind();
		});
	//$("#loginForm").submit(); //SUBMIT FORM
	});
});

$(document).ready(function(){
	$('.table-progression-bar a').tooltip();
});
/*----------------------紀錄下載次數------------------------*/
jQuery(document).ready(function($){
	$('.teaching-resources .download').on("click",function() {
		if(name){
			var dl_resource_id = $(this).attr('data-id');
	 		$.post(base_url + "field/updateDownloads",{dl_resource_id:dl_resource_id});
	 		var downloadsUrl = 
				$.ajax({
				url: base_url + "field/getDownloadsUrl",
				type:"POST",
				async:false,
				data: {dl_resource_id:dl_resource_id}
				}).responseText;
	 		location.href=base_url+'doc/resource/'+downloadsUrl;
		}else{
			login();
			//alert login window
			$('#modal-title').text('您必須登入才能下載資源');
			$('#modal-login').modal();
		}
	 });
})
/*----------------------討論區取消訂閱按鈕----------------------------*/
$('#unsubscribeForumTopic').click(function(){
	var forum_topic_id	=$.parseJSON($(this).attr('data-button')).forum_topic_id;
	var status	=$.parseJSON($(this).attr('data-button')).status;
	$.ajax({
		url: base_url + 'notifications/updateSubscriptionStatus',
		type:'POST',
		data:{forum_topic_id:forum_topic_id,status:status},
		success:function(data){
			if(status==1){
				$('#unsubscribeForumTopic').html('取消訂閱');
				$('#unsubscribeForumTopic').attr('data-button','{"forum_topic_id":"'+forum_topic_id+'","status":"0"}');
				$('.subscribeEmailCheckbox').addClass('hide');
			}else{
				$('#unsubscribeForumTopic').html('加入訂閱');				
				$('#unsubscribeForumTopic').attr('data-button','{"forum_topic_id":"'+forum_topic_id+'","status":"1"}');
				$('.subscribeEmailCheckbox').removeClass('hide');
			}
		}
	});		
});
/*----------------------帳號管理修改認證信箱Modal----------------------------*/
jQuery(document).ready(function($){
  $("#modifyEmail").validate({
      highlight: function(element, errorClass, validClass) {
        $(element).parent().addClass(errorClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parent().removeClass(errorClass);
       },
    errorElement: "span",
    errorClass: "has-error",
    errorPlacement: function(error, element) {
         $(error).addClass("label label-danger").insertAfter($(element).parent());
        },
    rules: {
          contentEmail: {
            required: true,
                email: true,
            maxlength:50,
            checkContentEmail: true
      }
    },
    messages: {
    	contentEmail: {
        required:"請輸入E-mail",
        email:"請輸入正確的E-mail格式",
        maxlength: "Email最多五十個字"
      }
     }
  });
  $.validator.addMethod(
		     "checkContentEmail",
		     function(value,element){
		        var result=$.ajax({
		              url: base_url + "user/checkContentEmail",
		              type:"POST",
		              async:false,
		              data: "email="+value
		            }).responseText;
		    if(result=="0") return true //can be used
		    else return false;
		   },"信箱已經被使用了，請再輸入一次!");
});

/*----------------------------coupon的新增編輯刪除------------------------------*/
jQuery(document).ready(function($){
    $('.edit-coupon-btn').on("click",function() {
        $(this).closest('.coupon').hide();
        $(this).closest('.coupon').next('.edit-coupon').show();
        $(this).closest('.coupon').next('.edit-coupon').find('.start_time').addClass('start_time');
        $(this).closest('.coupon').next('.edit-coupon').find('.end_time').addClass('end_time');
        $('.start_time').datetimepicker({pickTime: false});
		$('.end_time').datetimepicker({pickTime: false});
        dateTimeLimit();
    });
    $('.cancle-edit-coupon').on("click",function(){
        $(this).closest('.edit-coupon').hide();
        $(this).closest('.edit-coupon').prev('.coupon').show();
        $(this).closest('.edit-coupon').find('.start_time').removeClass('start_time');
        $(this).closest('.edit-coupon').find('.end_time').removeClass('end_time');
    });
    $('.del-coupon').on("click",function(){
        $(this).closest('.coupon').hide();
        //ajax del
        var coupon_id = $(this).attr('data-coupon-id');
        $.ajax({
            url: base_url + "coupon/delCoupon",
            type:"POST",
            data: {coupon_id: coupon_id},
            success: function (data) {
                alert('delete！');
            },
            error: function (data) {
                 alert('Ajax request 發生錯誤');
            }
        });
    });
    $('.add-coupon-btn').on("click",function() {
        $(this).closest('.add-plus-coupon').next('.add-coupon').toggle();
    });
    $('.cancle-add-coupon').on("click",function(){
        $(this).closest('.add-coupon').hide();
    });
});
/*----------------------coupon 表單驗證----------------------------*/
jQuery(document).ready(function($){
	$("#addCouponForm").validate({
		highlight: function(element, errorClass, validClass) {
			$(element).parent().addClass(errorClass);
		},
      	unhighlight: function(element, errorClass, validClass) {
        	$(element).parent().removeClass(errorClass);
       	},
       	errorElement: "span",
       	errorClass: "has-error",
       	errorPlacement: function(error, element) {
       		$(error).addClass("label label-danger").insertAfter($(element).parent());
        },
        rules: {
        	gen_number: {
        		required: true
      		},
      		price:{
      			required: true
      		},
          	start_time: {
            	required: true
      		},
          	end_time: {
            	required: true
      		},
          	comment: {
            	required: true
      		}
    	},
    	messages: {
    		gen_number: {
    			required:"請輸入數量"
      		},
      		price:{
      			required:"請輸入價格，0表示免費課程"
      		},
      		start_time: {
        		required: "請輸入生效時間"
      		},
      		end_time: {
        		required: "請輸入失效時間"
      		},
      		comment: {
        		required: "請輸入優惠條碼用途或說明"
      		}
     	}
     });
});
/*----------------------------訂閱課程------------------------------*/
jQuery(document).ready(function($){
    $('.subscribe-course').on("click",function() {
    	var course_id = $(this).attr('data-course-id');
    	var course_name = $(this).attr('data-course-name');
		bootbox.dialog({
  			message: "您要訂閱 ["+course_name+"] 此課程嗎？",
  			title: "訂閱課程",
  			buttons: {
    			main: {
      				label: "確定訂閱",
      				className: "btn-primary",
      				callback: function() {
      					$.ajax({
            				url: base_url + "pay/subscribe",
            				type:"POST",
            				data: {course_id:course_id,type:'course'},
            				success: function (data) {
                				location.reload();
            				},
            				error: function (data) {
                 				alert('Ajax request 發生錯誤');
            				}
        				}); 
      				}
    			},
    			cancle: {label: "取消",className: "btn",callback: function(){}}
  			}
		});		
    })
});
/*----------------------------未登入狀態下，點選付費課程------------------------------*/
jQuery(document).ready(function($){
    $('.pay-course').on("click",function() {
    	bootbox.alert("此為付費課程，您必須購買才能瀏覽！");
    })
});
/*----------------------------顯示優惠碼提示欄位------------------------------*/
// jQuery(document).ready(function(){
// 	var i=0;
// 	$('#inputCoupon').keyup(function(e){
// 		$('.coupon-message').empty();
// 		$(this).next(".field-counter").text($(this).attr('maxlength')-$(this).val().length);
// 		$('#coupon_code').val($(this).val());
// 		if($(this).val().length===16 && i==0){
// 			coupon_code = $(this).val();
// 			checkCoupon(coupon_code);
// 			i++;
// 		}
// 	});	
// });
/*----------------------------套用優惠碼------------------------------*/
$(document).ready(function(){
    $('.check-code-btn').on("click",function(){
        var coupon_code = $('#inputCoupon').val();
        $('#coupon_code').val(coupon_code);
       	checkCoupon(coupon_code);
    });
});
/*----------------------------使用優惠碼購買課程------------------------------*/
$(document).ready(function(){
	$("#modal-buy-course-btn").click(function(){
		var result = { };
		//取得表單的值
		$.each($('#buy-course-form').serializeArray(), function() {result[this.name] = this.value;});
		coupon_code = result['coupon_code'];
		course_id = result['course_id'];
		if(!result['coupon_code']){
			$('.coupon-message').text('請輸入優惠券');
			console.log('請輸入優惠券');
		}else{
			coupon_code = delquote(coupon_code);
			$.ajax({
        url: base_url + "coupon/checkCoupon",
        type:"POST",
        data: {coupon_code: coupon_code},
        success: function (data) {
          if(data=='true'){
            var coupon_price = 
						  $.ajax({
                url: base_url + "coupon/getPrice",
                type:"POST",
                async:false,
                data: {coupon_code:coupon_code}
						  }).responseText;
						var course_price = 
						  $.ajax({
                url: base_url + "pay/getPrice",
                type:"POST",
                async:false,
                data: {course_id:course_id}
              }).responseText;
						if(coupon_price < course_price){
							$('.coupon-message').text('您輸入的優惠碼餘額不足');
						}else{
							//pay/subscribe 訂閱[購買]學程
							paid=0;
							$.ajax({
            		url: base_url + "pay/subscribe",
            		type:"POST",
            		data: {course_id:course_id,type:'course',coupon_code:coupon_code},
            		success: function (data) {
                  
                  if(data=="true"){
              			//更新訂單資訊
                    $.ajax({
                      url: base_url + "pay/updateOrder",
        						  type:"POST",
        						  data: {coupon_code:coupon_code,price:course_price,paid:paid,course_id:course_id}
                    });
                    $.ajax({
                      url: base_url + "coupon/usedCoupon",
                      type:"POST",
                      data: {coupon_code:coupon_code,course_id:course_id},
                      success: function (data) {
                        alert("購買成功");
                        if(result['course_src']){
                          window.location = result['course_src'];
                        }else{
                          location.reload();
                        }
                      },
                      error: function (data) {
                        alert('Ajax request 發生錯誤');
                      }
        						}); 
                  }else{
                    alert('購買失敗');
                  }
            		},
                error: function (data) {
                  alert('Ajax request 發生錯誤');
                }
              }); 
						}
          }else{
            $('.coupon-message').text('請輸入有效的優惠碼');
          }
        },
        error: function (data) {
          alert('Ajax request 發生錯誤');
        }
      });
		}
  });
});
/*----------------------------購買學程------------------------------*/
$(document).ready(function(){
	$("#modal-buy-tracks-btn").click(function(){
		var result = { };
		//取得表單的值
		$.each($('#buy-tracks-form').serializeArray(), function() {result[this.name] = this.value;});
		//alert(result['coupon_code']);
		coupon_code = result['coupon_code'];
		tracks_id = result['tracks_id'];
		if(!result['coupon_code']){
			$('.coupon-message').text('請輸入優惠券');
			console.log('請輸入優惠券');
		}else{
			coupon_code = delquote(coupon_code);
			$.ajax({
           		url: base_url + "coupon/checkCoupon",
            	type:"POST",
            	data: {coupon_code: coupon_code},
            	success: function (data) {
            		if(data=='true'){
            			var coupon_price = 
						$.ajax({
							url: base_url + "coupon/getPrice",
							type:"POST",
							async:false,
							data: {coupon_code:coupon_code}
						}).responseText;
						var tracks_price = 
						$.ajax({
							url: base_url + "pay/getTracksPrice",
							type:"POST",
							async:false,
							data: {tracks_id:tracks_id}
						}).responseText;
						if(coupon_price < parseInt(tracks_price)){
							$('.coupon-message').text('您輸入的優惠碼餘額不足');
						}else{
							//pay/subscribeTracks [購買]學程
							paid=0;
							$.ajax({
            				url: base_url + "pay/subscribeTracks",
            				type:"POST",
            				data: {tracks_id:tracks_id,type:'tracks'},
            				success: function (data) {
            					//更新訂單資訊
            					$.ajax({
            						url: base_url + "pay/updateOrder",
            						type:"POST",
            						data: {coupon_code:coupon_code,price:tracks_price,paid:paid,tracks_id:tracks_id}
        						}); 
        						//更新已經使用過的coupon 更新時間 email 還有flag
                				$.ajax({
            						url: base_url + "coupon/usedCoupon",
            						type:"POST",
            						data: {coupon_code:coupon_code},
            						success: function (data) {
            							alert("購買成功");
                						location.reload();
            						},
            						error: function (data) {
                 						alert('Ajax request 發生錯誤');
            						}
        						}); 
            				},
            				error: function (data) {
                 				alert('Ajax request 發生錯誤');
            				}
        				}); 
						}
            		}else{
            			$('.coupon-message').text('請輸入有效的優惠碼');
            		}
            	},
            	error: function (data) {
                	alert('Ajax request 發生錯誤');
            	}
           	});
		}
    });
});


//刪除引號
function delquote(str){return (str=str.replace(/["']{1}/gi,""));} 
//檢查coupon是否可使用
function checkCoupon(coupon_code){
  coupon_code = delquote(coupon_code);
  $.ajax({
    url: base_url + "coupon/checkCoupon",
    type:"POST",
    data: {coupon_code: coupon_code},
    success: function (data) {	//找不到：none 使用過：used 過期：expired 可以使用：true
      if(data=='true'){
        var coupon_price = 
          $.ajax({
            url: base_url + "coupon/getPrice",
            type:"POST",
            async:false,
            data: {coupon_code:coupon_code}
          }).responseText;
					total_price = parseInt($('.total-price').text());
					if(total_price<=coupon_price){
						discount = total_price*-1;
						$('.total-price').text('0');
            $('.payMoney').hide();
            $('.buyRightNow').show();
					}else{
						discount = coupon_price*-1;
						$('.total-price').text(total_price-coupon_price);
					}
					$('.input-coupon-field').hide();
					$('.show-coupon .coupon').text(coupon_code);
					$('.show-coupon .discount').text(discount);
					$('.show-coupon,.showDel').show();
          $('.hideOrigin').hide();

      }else if(data=='used'){
        $('.coupon-message').text('您輸入的優惠券已使用過');
      }else if(data=='expired'){
        $('.coupon-message').text('您輸入的優惠券已過期');
      }else{
        $('.coupon-message').text('請輸入有效的優惠碼');
      }
    },
    error: function (data) {
      alert('Ajax request 發生錯誤');
    }
  });
}
//列表全選按鈕
$('#selectAllVacancyBtn').change(function(event){
	$('.deleteVacancyId').prop('checked', isChecked=$('#selectAllVacancyBtn').is(':checked'));
});
//企業徵才
$('.match_skill_div').click(function(){
	$('#match-skill-modal').modal();
});
$('.match_license_div').click(function(){
	$('#match-license-modal').modal();
});
$('#match-skill-modal').on('shown.bs.modal', function () {
	$('#match-skill-modal #skill_name').focus();
})
$('#match-license-modal').on('shown.bs.modal', function () {
	$('#match-license-modal #license_name').focus();
})
function bindDeleteSkillButton(){
	$('.match_skill_div span').click(function(){
		event.stopPropagation();
	});
	$('.match_skill_div a').click(function(){
		event.stopPropagation();
		var current=$(this).index('.match_skill_div a');
		var skill_array		=$('input[name="match_skill"]').val().split(',');
		skill_array.splice(current,1);
		console.log(skill_array);
		var hiddenValue='';
		for(var i=0;i<skill_array.length;i++){
			if(i+1==skill_array.length){
				hiddenValue+=skill_array[i];				
			}else{
				hiddenValue+=(skill_array[i]+',');
			}
		}
		$('input[name="match_skill"]').val(hiddenValue);
		if(current!=-1){
			$('.match_skill_div span').eq(current).remove();
			$('.match_skill_div a').eq(current).remove();
		}
	});
}
function bindDeleteLicenseButton(){
	$('.match_license_div span').click(function(){
		event.stopPropagation();
	});
	$('.match_license_div a').click(function(){
		event.stopPropagation();
		var current=$(this).index('.match_license_div a');
		var license_array		=$('input[name="match_license"]').val().split(',');
		license_array.splice(current,1);
		console.log(license_array);
		var hiddenValue='';
		for(var i=0;i<license_array.length;i++){
			if(i+1==license_array.length){
				hiddenValue+=license_array[i];				
			}else{
				hiddenValue+=(license_array[i]+',');
			}
		}
		$('input[name="match_license"]').val(hiddenValue);
		if(current!=-1){
			$('.match_license_div span').eq(current).remove();
			$('.match_license_div a').eq(current).remove();
		}
	});
}
$('#match-skill-modal #add_match_skill_btn').click(function(){
	var skill=$('#match-skill-modal #skill_name').val();
	$('.match_skill_div').append('<span class="label label-default">'+skill+'</span> <a class="fa fa-times" href="javascript:void(0);"></a> ');
	if($('input[name="match_skill"]').val()==''){
		$('input[name="match_skill"]').val($('input[name="match_skill"]').val()+skill);		
	}else{
		$('input[name="match_skill"]').val($('input[name="match_skill"]').val()+','+skill);
	}
	$('#match-skill-modal #skill_name').val('');
	$('#match-skill-modal #skill_name').focus();
	bindDeleteSkillButton();
});
$('#match-license-modal #add_match_license_btn').click(function(){
	var license=$('#match-license-modal #license_name').val();
	$('.match_license_div').append('<span class="label label-default">'+license+'</span> <a class="fa fa-times" href="javascript:void(0);"></a> ');
	if($('input[name="match_license"]').val()==''){
		$('input[name="match_license"]').val($('input[name="match_license"]').val()+license);		
	}else{
		$('input[name="match_license"]').val($('input[name="match_license"]').val()+','+license);
	}
	$('#match-license-modal #license_name').val('');
	$('#match-license-modal #license_name').focus();
	bindDeleteLicenseButton();
});
bindDeleteSkillButton();
bindDeleteLicenseButton();
/*----------------------企業徵才驗證----------------------------*/
jQuery(document).ready(function($){
  $("#addVacancyForm,#updateVacancyForm").validate({
      highlight: function(element, errorClass, validClass) {
        $(element).parent().addClass(errorClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parent().removeClass(errorClass);
       },
    errorElement: "span",
    errorClass: "has-error",
    errorPlacement: function(error, element) {
        $(error).addClass("label label-danger").insertAfter($(element).parent());
    },
    rules: {
    	job_name: {
    		required: true
    	},
      	jobs_category_id: {
      		required: true,
      		min: 1
      	},
    	address: {
    		required: true
    	},	
    	job_content: {
    		required: true,
    		maxlength: 4000
    	},
    	degree_limit: {
      		required: true,
      		min: 0
      	},
      	working_experience: {
      		required: true,
      		min: -1
      	},
	  	additional_demand: {
	  		maxlength: 4000
	  	},
      	contact: {
      		required: true,
      	},
      	contact_email: {
      		required: true,
      	},
	  	by_others: {
	  		maxlength:4000
	  	}
    },
    messages: {
    	job_name: {
			required: "請填寫職務名稱"
		},
	  	jobs_category_id: {
	  		required: "請選擇職務類別",
	  		min: "請選擇職務類別"
	  	},
		address: {
			required: "請填寫上班地點"
		},	
		job_content: {
			required: "請填寫工作內容",
			maxlength:"職務工作內容限字數4000字"
		},
		degree_limit: {
	  		required: "請選擇學歷要求",
	  		min: "請選擇學歷要求"
	  	},
	  	working_experience: {
	  		required: "請選擇年資要求",
	  		min: "請選擇年資要求"
	  	},
	  	additional_demand: {
	  		maxlength: "其他求才條件字數限4000字"
	  	},
	  	contact: {
	  		required: "請填寫連絡人資料",
	  	},
	  	contact_email: {
	  		required: "請填寫聯絡人Email",
	  	},
	  	by_others: {
	  		maxlength:"其他應徵方式字數限120字"
	  	}
     }
  });
});

/*----------------------------回答問題 & 回覆問題------------------------------*/
$(document).ready(function(){
	$('#leave-answer').on('focus', function() {
		var initTime = new Date();//初始化回答問題的時間
	});
	$(".leave-answer").click(function(){
		var forum_topic_id = $('#form-leave-answer input[name="forum_topic_id"]').val();
		var forum_topic = $(".title"+forum_topic_id).text().trim();
		var forum_content = $("#leave-answer").val();
		var alias = $(".origin-author").text().trim();
		tinCanApiLeaveAns(forum_topic_id,forum_topic,forum_content,alias);
	});
	
	$(".leave-comment").click(function(){
		var forum_topic_id = $(this).attr('data-topic-id');
		var forum_content_id = $(this).attr('data-content-id');
		var forum_topic = $(".title"+forum_topic_id).text().trim();
		var forum_content = $(".dat"+forum_content_id).text().trim();
		var alias = $(".alias"+forum_content_id).text().trim();
		var forum_comment = $(this).closest("form").find("#leave-comment").val();
		tinCanApiLeaveComment(forum_topic_id,forum_topic,forum_content_id,forum_content,forum_comment,alias);
	});
});	
/*-------------------------討論區 送出答案----------------------------*/
jQuery(document).ready(function($){
	$("#form-leave-answer").validate({
		highlight: function(element, errorClass, validClass) {
			$(element).parent().addClass(errorClass);
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parent().removeClass(errorClass);
		},
		errorElement: "span",
		errorClass: "has-error",
		errorPlacement: function(error, element) {
			$(error).addClass("label label-danger").insertAfter($(element).parent());
        },
		rules: {
			forum_content: {
				required: true
			}		
		},
		messages: {
			forum_content: {
				required:"請輸入要回答的內容"
			}
		}
	});
	$("#form-leave-comment").validate({
		highlight: function(element, errorClass, validClass) {
			$(element).parent().addClass(errorClass);
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parent().removeClass(errorClass);
		},
		errorElement: "span",
		errorClass: "has-error",
		errorPlacement: function(error, element) {
			$(error).addClass("label label-danger").insertAfter($(element).parent());
        },
		rules: {
			forum_comment: {
				required: true
			}		
		},
		messages: {
			forum_comment: {
				required:"請輸入要回覆的內容"
			}
		}
	});
});
//get the question info
/* $.getJSON(
    "http://54.238.52.230:3000/vlAPI/code/10",
    function(data){
        var question = data[0].question;
        var code_bank_id = data[0].code_bank_id;
        var stdin = data[0].stdin;

        var partial_code = $("<div id='editor'></div>").text(data[0].partial_code);
        var langid = $("<input type='hidden' id='langid'>").val("8");
        var code_bank_id = $("<input type='hidden' id='code_bank_id'>").val(data[0].code_bank_id);
        var web_id = $("<input type='hidden' id='web_id'>").val("e62b837b5f18eea33941ffdb4679e3d5");
        var user_id = $("<input type='hidden' id='user_id'>").val("140515");
        $("#virtual-lab-question").append(question);
        $("#virtual-lab-editor").append(partial_code).append(langid).append(code_bank_id).append(web_id).append(user_id);
        $('#virtual-lab-editor').append("<button type='submit' class='btn btn-success' id='compile'>編譯</button>" );
        $("#virtual-lab-result").append("<div class='show-compile-result'></div>");
        var editor = ace.edit("editor");
        $("#editor").css({'font-size':"16px"});
        editor.getSession().setMode("ace/mode/java");
        editor.setTheme("ace/theme/chrome");
        var start_time=null;
        editor.on('input', function() {
          if(!start_time){
            start_time= getFormattedDate();
          }
        });  
    function getFormattedDate() {
      var date = new Date();
      var str = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
      return str;
    }
    $('#compile').on('click', function(e) {
  if(!start_time){
    start_time= getFormattedDate();
  }
  e.preventDefault();
  var editor = ace.edit("editor");
  var langid = $('#langid').val();
  var code_bank_id = $('#code_bank_id').val();
  var codeF = editor.getValue();
  var stdin = $('#stdin').val();
  var web_id = $('#web_id').val();
  var user_id = $('#user_id').val();
  var jsonData = {
    language: langid,
    code: codeF,
    stdin:stdin,
    code_bank_id:code_bank_id,
    user_id:user_id,
    web_id:web_id,
    start_time:start_time
  };
  $.ajax({
    type: 'POST',
    url: 'http://54.238.52.230:3000/vlAPI/receive',
    crossDomain: true,
    data: jsonData,
    dataType: 'json',
    success: function(responseData, textStatus, jqXHR) {
        start_time=null;
        //權限不足
        if(responseData["output"].search("min") > 0){
          $(".show-compile-result").html("請聯絡管理員以取得我們提供給您的服務");
        //系統忙碌
        }else if(responseData["output"].search("Timed") > 0){
          $(".show-compile-result").text("系統忙碌，請稍候再試");
        }else{
        //編譯錯誤
          if(responseData["errors"]){
            var temp = responseData["temp_result"];
            var text_result='';
            for (var i=0;i < temp.length; i++) {
              var text = (i+1)+'.'+temp[i].plain_text+'<br>';
              var text_result = text_result+text;
            };
            $(".show-compile-result").append(text_result);
          }else{
        //編譯正確，檢核點正確
            var temp = responseData["check_item_desc"];
            if(responseData["answerisright"]=='1' && temp.length=='0'){
              $(".show-compile-result").text("完全正確");
            }else{
        //編譯正確，檢核點錯誤
              var check_result='';
              for (var i=0;i < temp.length; i++) {
                var text = (i+1)+'.'+temp[i]+'<br>';
                var check_result = check_result+text;
              };
              $(".show-compile-result").append(check_result);
            }
            $(".show-compile-result").append(responseData["output"]);
          }
        }
        
    },
    error: function (responseData, textStatus, errorThrown) {
        console.log("POST failed.");
    }
});
});
}); */
jQuery(document).ready(function($){
  $("#create_datasets").validate({
      highlight: function(element, errorClass, validClass) {
        $(element).parent().addClass(errorClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parent().removeClass(errorClass);
       },
    errorElement: "span",
    errorClass: "has-error",
    errorPlacement: function(error, element) {
        $(error).addClass("label label-danger").insertAfter($(element).parent());
    },
    rules: {
      datasets_name: {
        required: true,
        lettersonly: true,
        check_datasets_name:true
      }
    },
    messages: {
      datasets_name: {
      required: "請填寫Datasets名稱"
    }
     }
  });
  jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[A-Za-z0-9_-\s]+$/i.test(value);
  }, "僅支援半形英文及數字的檔名");
  $.validator.addMethod(
     "check_datasets_name",
     function(value,element){
        var result=$.ajax({
              url:"../datasets/check_datasets_name",
              type:"POST",
              async:false,
              data: "datasets_name="+value
            }).responseText;
    if(result=="0") return true //can be used
    else return false;
   },"Datasets重複命名，請換一個")
});
// 上傳datasets檔案的modal
$("#uploadData").click(function(){
  $('#myModal').modal({
    keyboard: false
  })
});
$('#myModal').on('hidden.bs.modal', function (e) {
  window.location.reload(true);
})
// 刪除datasets
$("#delete-datasets").on('click',function () {
  var url = window.location.href;
  $.ajax({
    url: url,
    type:"DELETE",
    success: function(result) {
        location.href=base_url+'datasets';
    }
  });
})

//列表全選按鈕
$('#selectAllCompanyBtn').change(function(event){
  $('.deleteCompanyId').prop('checked', isChecked=$('#selectAllCompanyBtn').is(':checked'));
});
/*----------------------企業資料維護驗證----------------------------*/
jQuery(document).ready(function($){
  $("#addComapnyForm,#updateComapnyForm").validate({
      highlight: function(element, errorClass, validClass) {
        $(element).parent().addClass(errorClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).parent().removeClass(errorClass);
       },
    errorElement: "span",
    errorClass: "has-error",
    errorPlacement: function(error, element) {
        $(error).addClass("label label-danger").insertAfter($(element).parent());
    },
    rules: {
      company_name: {
        required: true
      },
      tax_id_number: {
        required: true,
        maxlength: 8
      },
      contact_name: {
        required: true
      },  
      contact_mail: {
        required: true,
      },
      contact_phone: {
        required: true,
      },
    },
    messages: {
      company_name: {
      required: "請填寫企業名稱"
    },
    tax_id_number: {
      required: "請填寫統一編號",
      maxlength: "統一編號限字數8字"
    },
    contact_name: {
      required: "請填寫聯絡人姓名"
    },  
    contact_mail: {
      required: "請填寫聯絡人信箱",
    },
    contact_phone: {
        required: "請填寫聯絡人電話",
    },
     }
  });
});

