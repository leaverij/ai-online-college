﻿1
00:00:01,320 --> 00:00:08,914
[單元3：Struts 2 架構剖析與配置詳解 /Struts 2 內建的常數(constant)]

2
00:00:10,700 --> 00:00:13,234
首先我們看一下常數設定的目的

3
00:00:14,910 --> 00:00:18,891
Struts 2內建的常數我們稱它們為智慧型設定

4
00:00:19,670 --> 00:00:22,331
它可以提供事前預設的設定

5
00:00:22,340 --> 00:00:25,245
讓開發者方便系統開發

6
00:00:26,850 --> 00:00:29,531
這些常數是擺放在什麼位置呢

7
00:00:29,540 --> 00:00:31,325
我們看一下以下的說明

8
00:00:33,020 --> 00:00:36,468
這些常數全部都是以key = value的方式

9
00:00:36,470 --> 00:00:41,725
定義在一個檔案叫做default.properties這檔案裡面

10
00:00:42,700 --> 00:00:45,360
你可以在WEB-INF\lib

11
00:00:45,370 --> 00:00:50,834
底下的struts2-core這個jar檔當中有個套件叫做

12
00:00:50,840 --> 00:00:55,188
org.apache.struts2的套件底下

13
00:00:55,190 --> 00:00:58,548
可以找到檔案叫做default.properties

14
00:00:59,460 --> 00:01:02,445
現在實際把這個檔案打開

15
00:01:02,450 --> 00:01:09,154
我們現在看到的是上一單元所建立好的Struts2_Test10這個專案

16
00:01:09,640 --> 00:01:10,365
打開它

17
00:01:11,860 --> 00:01:16,000
底下的 Java Resources底下的Libraries

18
00:01:16,940 --> 00:01:19,371
再底下的Web App Libraries

19
00:01:21,070 --> 00:01:24,308
事實上這些jar檔是我們在上一單元

20
00:01:25,400 --> 00:01:28,720
程式設計步驟的六個步驟的第一個步驟

21
00:01:28,730 --> 00:01:30,720
所匯入的那些jar檔案

22
00:01:31,430 --> 00:01:37,051
裡面這個叫struts2-core的這個jar打開它

23
00:01:38,650 --> 00:01:43,337
它的第一個套件org.apache.struts2這個套件

24
00:01:44,130 --> 00:01:44,857
打開它

25
00:01:45,970 --> 00:01:48,788
裡面這個檔叫default.properties

26
00:01:49,720 --> 00:01:52,537
這檔案就是剛才所提及的

27
00:01:52,540 --> 00:01:56,354
已經設定好的所有常數都在這裡

28
00:01:56,360 --> 00:02:00,605
全部都是以key = value的方式來儲存的

29
00:02:02,540 --> 00:02:04,000
這邊要提醒的是

30
00:02:04,010 --> 00:02:09,714
Struts 2 內建的常數它全部以key = value的方式來做設定

31
00:02:09,720 --> 00:02:13,142
這個等號的左邊這個是key

32
00:02:14,740 --> 00:02:16,617
等號右邊是這個value

33
00:02:17,630 --> 00:02:18,960
如果你有一天

34
00:02:18,970 --> 00:02:22,720
這些常數的值你要做更改了話呢

35
00:02:22,730 --> 00:02:26,720
你必須以左邊這個key為key來做更改

36
00:02:27,500 --> 00:02:28,982
如何更改呢？

37
00:02:29,570 --> 00:02:32,217
我們待會會做更進一步的說明

38
00:02:33,790 --> 00:02:36,720
前面所提的Struts 2內建常數

39
00:02:36,730 --> 00:02:39,188
你也可以由官方的技術文件

40
00:02:39,190 --> 00:02:43,897
叫做strutsproperties.html的檔案去做讀取

41
00:02:44,520 --> 00:02:50,297
這是除了上面jar檔裡面的讀取方式一個更簡便的方式

42
00:02:51,460 --> 00:02:53,382
現在看一下這個檔案

43
00:02:55,710 --> 00:02:58,171
現在看到的是從Struts官方

44
00:02:58,180 --> 00:03:00,662
下載回來的一個doc的一個zip檔案

45
00:03:02,350 --> 00:03:05,782
解壓縮之後裡面有一個子目錄叫做docs

46
00:03:05,790 --> 00:03:07,782
裡面放的就是官方文件

47
00:03:09,630 --> 00:03:11,542
利用這個機會做一下提醒

48
00:03:12,410 --> 00:03:14,937
因為這些文件我們今常要查閱

49
00:03:14,940 --> 00:03:17,542
所以請自行解壓縮之後

50
00:03:17,550 --> 00:03:21,268
放在一個比較好查閱的一個目錄底下

51
00:03:22,780 --> 00:03:25,211
現在進入docs裡面

52
00:03:32,030 --> 00:03:37,874
現在看到的就是官方文件的strutsproperties.html這個檔案

53
00:03:37,880 --> 00:03:39,565
我們打開它

54
00:03:40,700 --> 00:03:45,074
現在看到的一樣是key = value的一個擺放方式

55
00:03:45,510 --> 00:03:51,474
像這個就是剛才我們在jar檔裡面所看到的裡面那些值

56
00:03:52,460 --> 00:03:53,554
這是一樣的設定

57
00:03:54,330 --> 00:03:56,091
一樣可以從這邊去做查閱

