﻿1
00:00:01,320 --> 00:00:08,914
[單元3：Struts 2 架構剖析與配置詳解 /內建常數的設定與更改方式]

2
00:00:10,530 --> 00:00:13,470
由於內建的常數的數量非常多

3
00:00:13,990 --> 00:00:18,194
我們現在因為時間關係沒辦法一個一個做介紹

4
00:00:19,000 --> 00:00:21,771
目前先以struts.xml檔為例

5
00:00:22,510 --> 00:00:26,411
說明一些比較重要常用的設定如下

6
00:00:26,420 --> 00:00:31,897
至於為什麼用struts.xml檔為例，等會會做說明

7
00:00:32,500 --> 00:00:34,800
我們先看下第一個常用的設定

8
00:00:36,090 --> 00:00:38,777
第一個是設定為是否為開發模式

9
00:00:40,060 --> 00:00:44,594
那你就要把常數的key設定為struts.devMode

10
00:00:44,600 --> 00:00:46,590
把它的值改為true

11
00:00:46,600 --> 00:00:52,057
設為true時對於配置文件以及java文件的修改

12
00:00:52,060 --> 00:00:53,920
會自動載入更新

13
00:00:53,930 --> 00:00:56,022
不用重新啟動伺服器

14
00:00:56,030 --> 00:00:58,148
可以節省你開發的時間

15
00:00:58,150 --> 00:01:01,051
要記得預設值為false

16
00:01:03,790 --> 00:01:06,468
我們再看下一個常用的設定

17
00:01:08,190 --> 00:01:11,440
設定全域範圍的國際化資源檔

18
00:01:11,450 --> 00:01:15,565
檔名叫globalMessage起頭的一個資源檔

19
00:01:15,570 --> 00:01:19,428
設定時自己要將常數的name

20
00:01:19,430 --> 00:01:23,862
取為struts.custom.i18n.resources

21
00:01:23,870 --> 00:01:26,651
值取個你喜歡的名稱

22
00:01:26,660 --> 00:01:29,474
記得這個名稱沒有預設值

23
00:01:29,480 --> 00:01:32,057
只要是你喜歡的名稱都可以

24
00:01:32,060 --> 00:01:35,474
假設把它取名為globalMessage起頭

25
00:01:35,480 --> 00:01:39,520
將來你可以用這個起頭的名稱

26
00:01:40,370 --> 00:01:43,828
搭配瀏覽器語系設定的設定

27
00:01:43,830 --> 00:01:47,851
這是一個語言、國家

28
00:01:47,860 --> 00:01:50,102
檔案叫做.properties

29
00:01:50,830 --> 00:01:53,680
通常我們會先把這個設定先設起來

30
00:01:53,690 --> 00:01:57,440
方便你在寫國際化專案時直接拿來使用

31
00:01:59,800 --> 00:02:02,422
我們再來看看還有哪些專案的設定

32
00:02:03,630 --> 00:02:08,571
不過這些設定如果沒有特殊需要，你只要保持預設值即可

33
00:02:09,190 --> 00:02:13,462
第一個設置Struts 2是否支援動態方法呼叫

34
00:02:14,410 --> 00:02:15,668
預設值是true

35
00:02:16,910 --> 00:02:19,245
不用動它如果沒沒有特殊需要了話

36
00:02:19,250 --> 00:02:21,520
什麼叫做動態方法呼叫呢

37
00:02:21,530 --> 00:02:25,634
我們在這一單元的最後會做詳細的說明

38
00:02:26,220 --> 00:02:29,177
第二個設定是更改action的副檔名

39
00:02:30,460 --> 00:02:31,954
預設值是.action

40
00:02:32,900 --> 00:02:35,634
這是我們在上一單元曾經提過的

41
00:02:36,460 --> 00:02:38,582
你在呼叫你的action類別時

42
00:02:38,590 --> 00:02:41,142
你的呼叫方式是myNamespace

43
00:02:41,150 --> 00:02:44,057
下面的myAction的.actoin

44
00:02:44,060 --> 00:02:46,834
最後的.action就是指這個設定

45
00:02:46,840 --> 00:02:51,074
這是一個可以啟動你的核心控制器的一個副檔名

46
00:02:51,080 --> 00:02:55,702
同樣的沒有特殊需要保持預設即可

47
00:02:57,170 --> 00:03:01,908
第三個設定設定一個字元編碼為UTF-8，這什麼呢？

48
00:03:01,910 --> 00:03:08,571
就是當我的JSP在接收到一個非西歐語系的字串時

49
00:03:09,550 --> 00:03:12,948
當你用的方法接收字串時

50
00:03:12,950 --> 00:03:16,068
通常你都必須調用這個方法叫做

51
00:03:16,070 --> 00:03:20,240
req.setCharacterEncoding這個方法作編碼的處理

52
00:03:20,250 --> 00:03:23,554
設定這邊已經幫你設定好

53
00:03:23,560 --> 00:03:27,211
所以你在寫程式時這行程式碼就不用再撰寫了

54
00:03:27,220 --> 00:03:31,188
對我們程式設計師而言是一個非常方便的設定

55
00:03:32,540 --> 00:03:34,868
這個設定是網頁的風格

56
00:03:36,370 --> 00:03:40,297
你可以設為預設的xhtml即可

57
00:03:40,300 --> 00:03:47,588
它有所謂的xhtml或simple或css_xhtml

58
00:03:47,970 --> 00:03:52,468
預設值就是xhtml保持這個就可以了

59
00:03:52,470 --> 00:03:55,222
至於什麼叫做網頁風格

60
00:03:55,230 --> 00:03:59,885
我們在課程的這個單元會做詳細的說明

61
00:04:00,490 --> 00:04:02,091
最後要提醒的是

62
00:04:02,740 --> 00:04:04,857
這些設定裡面的name

63
00:04:04,860 --> 00:04:07,337
就是所謂的key的值從哪裡來呢

64
00:04:07,340 --> 00:04:10,902
是全部定義在default.properties這檔案裡面

65
00:04:11,540 --> 00:04:13,668
你有需要時必須打開這個檔

66
00:04:14,630 --> 00:04:17,405
才能從這個檔取出所有的key

67
00:04:17,410 --> 00:04:18,948
做你需要的設定

68
00:04:20,890 --> 00:04:25,485
我們現在介紹一下Struts 2的內建常數的更改方式

69
00:04:25,490 --> 00:04:30,582
基本上有三種方式可以更改這些內建的常數

70
00:04:30,590 --> 00:04:34,182
依序是透過web.xml檔案

71
00:04:34,190 --> 00:04:37,782
或struts.properties這個檔案

72
00:04:37,790 --> 00:04:41,222
或struts.xml檔來更改你的預設值

73
00:04:42,270 --> 00:04:46,114
而struts.xml檔案的設定會比較方便

74
00:04:46,770 --> 00:04:48,308
也就是我在剛剛

75
00:04:48,310 --> 00:04:53,234
用這個檔案來作一些常用的常數說明的原因

76
00:04:54,170 --> 00:04:56,571
記得當有重複的設定時

77
00:04:56,580 --> 00:05:02,034
系統會以上述的順序為順序依序往下搜尋

78
00:05:02,510 --> 00:05:04,765
也就是web.xml檔最優先

79
00:05:04,770 --> 00:05:07,794
最後才是內建的default.properties

80
00:05:07,800 --> 00:05:10,480
這個檔案裡面的預設定

81
00:05:11,330 --> 00:05:15,165
特別注意struts.properties跟sturts.xml

82
00:05:15,170 --> 00:05:18,651
必須放在classpath的根目錄裡面

83
00:05:18,660 --> 00:05:22,354
也就是直接放在WEB-INF\classes目錄裡

84
00:05:23,430 --> 00:05:26,811
以下我們會以更改.action副檔名為例

85
00:05:26,820 --> 00:05:29,908
說明如何更改這些常數的預設值

86
00:05:31,410 --> 00:05:35,108
我們先看一下Struts 2內建常數的一個更改示範

87
00:05:35,730 --> 00:05:36,925
要提醒的是

88
00:05:37,570 --> 00:05:41,702
目前這邊是以.action的原生檔名當作示範

89
00:05:41,710 --> 00:05:46,205
至於其他常數的更改都可以依此類推

90
00:05:46,860 --> 00:05:49,760
下面示範三種更改常數的方式

91
00:05:49,770 --> 00:05:53,200
第一個是靠web.xml檔案來做設定

92
00:05:54,170 --> 00:05:57,005
在Struts 2核心控制器就是所謂的

93
00:05:57,010 --> 00:06:00,057
StrutsPrepareAndExecuteFilter

94
00:06:00,060 --> 00:06:02,308
這個濾器裡面的filter標籤裡面

95
00:06:02,310 --> 00:06:06,240
使用init-param來作定義

96
00:06:08,490 --> 00:06:12,937
定義時參數的名稱叫struts.action.extension

97
00:06:12,940 --> 00:06:17,702
跟下面的另外兩個設定的這個key是同一個key

98
00:06:18,980 --> 00:06:20,617
這個key是怎麼來的呢

99
00:06:20,620 --> 00:06:23,714
注意是在我們剛剛所提過的這個檔

100
00:06:25,020 --> 00:06:28,822
default.properties裡面有所有的key跟value

101
00:06:28,830 --> 00:06:31,360
需要時必須直從這個檔案打開

102
00:06:31,370 --> 00:06:34,445
找出所有的key來做你需要的更改

103
00:06:36,530 --> 00:06:40,194
這第一個方式是看web.xml檔案的設定

104
00:06:40,200 --> 00:06:42,994
就是設定在你的濾器裡面

105
00:06:43,870 --> 00:06:48,811
第二個設定是靠struts.properties

106
00:06:48,820 --> 00:06:52,217
key是一樣這個key值是你需要的值

107
00:06:52,750 --> 00:06:56,080
第三個設定方式是struts.xml檔案

108
00:06:56,090 --> 00:07:00,308
常數的名稱一樣是這個key值是你需要的值

109
00:07:01,220 --> 00:07:04,777
記得其它的設定方式都是依此類推

