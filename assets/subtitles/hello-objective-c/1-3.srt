﻿1
00:00:00,000 --> 00:00:09,940
[單元1：Hello Objective-C! / Hello Objective-C!開發實例]

2
00:00:09,940 --> 00:00:13,980
接著就讓我們來實際操作一次

3
00:00:13,980 --> 00:00:17,180
打開Xcode這個應用程式之後

4
00:00:17,180 --> 00:00:19,620
會看到Create a new Xcode project

5
00:00:19,620 --> 00:00:21,090
我們就選這個

6
00:00:22,520 --> 00:00:24,330
選了之後會帶出這個畫面

7
00:00:24,330 --> 00:00:27,490
它會問我們要建立哪一種應用程式

8
00:00:27,490 --> 00:00:32,900
在我們這個練習我們選OS X Application裡頭的

9
00:00:32,900 --> 00:00:36,930
Command Line Tool選了這個按下Next

10
00:00:38,210 --> 00:00:40,330
選完之後它會請我們輸入專案名稱

11
00:00:40,330 --> 00:00:45,020
在我們講義上的第七頁就告訴我們

12
00:00:45,020 --> 00:00:50,060
專案名稱我們目前這個例子就輸入HelloXcode

13
00:00:50,060 --> 00:00:53,560
組織名稱可以寫自己喜歡的名字

14
00:00:53,560 --> 00:00:56,160
Company Identifier用網址倒過來

15
00:00:56,160 --> 00:01:00,090
Type我們目前要做的是Fundation

16
00:01:00,090 --> 00:01:02,890
Fundation Framework的相關語法

17
00:01:02,890 --> 00:01:05,910
然後把ARC自動記憶體管理打勾

18
00:01:05,930 --> 00:01:07,170
按下Next

19
00:01:09,930 --> 00:01:15,280
之後就會請我們到想要儲存的地方儲存

20
00:01:16,000 --> 00:01:19,620
接著就可以我們就可以選擇我們想要儲存的資料夾之下

21
00:01:19,620 --> 00:01:21,620
按下Create

22
00:01:29,730 --> 00:01:33,860
由於這是我們第一次打開Xcode這個應用程式

23
00:01:33,870 --> 00:01:36,680
所以我們稍微簡介一下這整個環境

24
00:01:36,680 --> 00:01:40,680
你所看到左邊這個Navigator會顯示

25
00:01:40,680 --> 00:01:44,310
目前專案資料夾下的各個檔案

26
00:01:44,310 --> 00:01:47,370
中間這個是我們主要編輯區

27
00:01:47,370 --> 00:01:51,710
通常會選擇左邊我們要編輯的程式碼main.m檔

28
00:01:52,880 --> 00:01:59,460
接著我們就會在中間看到main.m檔的詳細內容

29
00:01:59,460 --> 00:02:02,820
右邊這塊算是屬性面板

30
00:02:02,820 --> 00:02:07,350
我們會看到目前我們在中間選特定的選項

31
00:02:07,350 --> 00:02:10,950
右邊會出現相關的可以做設定的資訊

32
00:02:10,950 --> 00:02:16,440
右下角則是在我們畫面上可以增加的原件

33
00:02:16,450 --> 00:02:18,880
會顯示在右下角這個地方

34
00:02:18,880 --> 00:02:23,420
這三個主要的畫面下面還有一區

35
00:02:23,420 --> 00:02:29,710
是顯示我們等下會有一些資料會輸出在這個區塊

36
00:02:29,710 --> 00:02:33,550
整個畫面上你都可以做切換的動作

37
00:02:33,550 --> 00:02:35,840
在右上角這邊進行

38
00:02:35,840 --> 00:02:40,570
如果我們要在主要編輯區一分為二為兩塊

39
00:02:40,570 --> 00:02:43,200
我們可以按這個Show the Assistant editor

40
00:02:43,200 --> 00:02:46,310
它就會將中間的畫面一分為二

41
00:02:46,310 --> 00:02:48,600
我們再回到原本的這個

42
00:02:48,600 --> 00:02:55,480
如果我們暫時的要把三大塊主要顯示區的某個部分隱藏起來

43
00:02:55,480 --> 00:02:58,080
我們可以按右上角的選項

44
00:02:58,080 --> 00:03:00,080
譬如說我把右邊的藏起來

45
00:03:00,080 --> 00:03:03,020
把左邊的藏起來畫面就看起來比較寬

46
00:03:03,020 --> 00:03:05,110
你也可以再把下面藏起來

47
00:03:05,110 --> 00:03:07,370
如果你想要他們再一次的顯示

48
00:03:07,370 --> 00:03:12,310
可以再點選它相關的資料全部都帶出來

49
00:03:12,310 --> 00:03:17,130
這是我們基礎在目前這個畫面上所看到的這些資訊

50
00:03:17,130 --> 00:03:23,600
我們剛才有提到説這個一開始的專案我們不需要打任何的字

51
00:03:23,600 --> 00:03:25,570
我們可以直接按這個執行

52
00:03:25,570 --> 00:03:27,570
在左上角的這個Run

53
00:03:39,220 --> 00:03:44,280
我們應該要正確的可以再下方看到Hello,World!

54
00:03:44,280 --> 00:03:48,000
算我們有完成了這個部份的練習

55
00:03:48,000 --> 00:03:49,880
看到了其中的三個重點

56
00:03:49,880 --> 00:03:55,680
第一個main指的是這個程式的進入點

57
00:03:55,680 --> 00:03:59,440
程式開始進來就會走這裡面

58
00:03:59,450 --> 00:04:02,040
一行一行的描述

59
00:04:02,040 --> 00:04:06,660
其中另外提到的是這個autoreleasepool這塊

60
00:04:06,660 --> 00:04:11,260
夾在這樣的語法中間會執行自動的記憶體管理機制

61
00:04:11,260 --> 00:04:14,570
第三個其實是我們這個例子的重點

62
00:04:14,570 --> 00:04:20,680
透過NSLog輸出資訊到console

63
00:04:20,680 --> 00:04:23,355
NSLog到底是什麼呢
十頁告訴我們的

64
00:04:23,355 --> 00:04:25,422
NSLog到底是什麼呢
我們可以把滑鼠移到這上面來

65
00:04:25,422 --> 00:04:27,422
NSLog到底是什麼呢
然後按下Alt鍵

66
00:04:27,422 --> 00:04:28,977
NSLog到底是什麼呢
這時候會出現問號

67
00:04:28,977 --> 00:04:32,130
NSLog到底是什麼呢
在點擊觸控板或滑鼠左鍵

68
00:04:32,130 --> 00:04:32,244
在點擊觸控板或滑鼠左鍵

69
00:04:34,730 --> 00:04:37,866
會帶出相關的解釋資訊

70
00:04:37,866 --> 00:04:40,955
所以我們可以透過這樣的方式來了解

71
00:04:40,955 --> 00:04:45,622
一個類別或是一個方法名稱實際背後的內容到底是什麼

72
00:04:45,622 --> 00:04:48,466
所以這邊就看描述跟你說

73
00:04:48,466 --> 00:04:54,711
透過NSLog可以把一段錯誤訊息秀到蘋果的系統的機制

74
00:04:54,711 --> 00:04:56,444
就是這個指令區塊

75
00:04:56,444 --> 00:04:59,288
透過這樣的方式來把資料秀出來

76
00:04:59,288 --> 00:05:01,666
目前我們這個例子

77
00:05:01,666 --> 00:05:04,266
事實上我們是輸出最普通的一個字串

78
00:05:04,266 --> 00:05:07,444
這個字串在Objective-C裡面

79
00:05:07,444 --> 00:05:10,866
是透過@開頭來跟C語言作區分

80
00:05:10,866 --> 00:05:14,444
後面帶出雙引號開頭到雙引號結尾

81
00:05:14,444 --> 00:05:16,955
去輸出一段固定的字串

82
00:05:16,955 --> 00:05:20,333
所以這個字串透過NSLog方法

83
00:05:20,333 --> 00:05:23,555
就可以秀在這個畫面上console的地方

84
00:05:23,550 --> 00:05:27,711
例如我們在這邊新增一個整數變數

85
00:05:27,711 --> 00:05:31,622
再將我們整數變數做個簡單的運算

86
00:05:31,622 --> 00:05:38,066
運算完我們希望可以把這個整數變數的結果輸出到畫面上

87
00:05:38,066 --> 00:05:42,288
我們可以在前面如果是固定的內容直接打上去

88
00:05:45,288 --> 00:05:48,333
然後變動的內容用%開頭

89
00:05:48,333 --> 00:05:51,755
在加上指定是什麼的資料型態

90
00:05:51,755 --> 00:05:53,933
接著在後面加上逗號去

91
00:05:53,933 --> 00:05:58,044
把我們要輸出變數內容的名稱寫在這個地方

92
00:05:58,044 --> 00:06:03,666
打完之後一樣按下這個Run來執行

93
00:06:12,622 --> 00:06:15,555
這時候我們就可以看到在右下方

94
00:06:15,555 --> 00:06:19,422
它有把我們原本一般的字串帶出來

95
00:06:19,422 --> 00:06:24,711
並且加上這個sum裡面的值為75也顯示出來

96
00:06:26,222 --> 00:06:31,222
首先它告訴我們希望我們可以宣告一個字串

97
00:06:34,577 --> 00:06:36,355
myname

98
00:06:39,266 --> 00:06:44,155
再將這個字串透過NSLog的方式來做顯示

99
00:06:52,911 --> 00:06:56,022
我們一樣來執行看看

100
00:06:56,844 --> 00:07:00,822
可以順利的把這個字串顯示出來

101
00:07:00,822 --> 00:07:04,866
在講義第十二頁的下方還有一個練習

102
00:07:04,866 --> 00:07:06,866
我們一起來做看看

103
00:07:06,866 --> 00:07:09,777
如果我們想要顯示的是數字

104
00:07:09,770 --> 00:07:18,533
例如是整數，還有一個浮點數

105
00:07:24,377 --> 00:07:29,266
一樣都透過NSLog的方式顯示出來

106
00:07:38,200 --> 00:07:40,777
這次是整數%d

107
00:07:43,733 --> 00:07:46,311
還有浮點數

108
00:07:49,377 --> 00:07:54,222
接著再把我們剛剛有使用到的變數接到後面來

109
00:07:54,222 --> 00:07:57,688
第一個是名字，第二個是年紀

110
00:07:59,844 --> 00:08:02,177
第三個是體重

111
00:08:02,177 --> 00:08:05,466
然後讓我們來執行看看

112
00:08:08,911 --> 00:08:11,222
執行的時候發現了一些問題

113
00:08:11,222 --> 00:08:15,488
在浮點數上面顯示後面多了非常多的零

114
00:08:16,266 --> 00:08:17,977
這時候讓我們來學習

115
00:08:17,977 --> 00:08:22,800
在NSLog中我們如何去控制浮點數後面要有幾位數

116
00:08:22,800 --> 00:08:26,177
如果我們想要後面有兩位數了話

117
00:08:26,170 --> 00:08:32,066
在%與f浮點數中間加上.2

118
00:08:32,066 --> 00:08:35,977
來標示我們浮點數後面只需要顯示兩位數

119
00:08:35,977 --> 00:08:39,000
讓我們再次來執行看下結果

120
00:08:39,644 --> 00:08:44,844
的確得到了78.50這樣的一個顯示

121
00:08:44,844 --> 00:08:48,400
在講義的第十六頁中也有提到

122
00:08:48,400 --> 00:08:52,377
我們要查閱進一步解釋我們剛已經會了

123
00:08:52,377 --> 00:08:57,133
就是由標移過去按下Alt出現問號按下滑鼠左鍵

124
00:08:57,133 --> 00:08:58,711
可以帶出解釋

125
00:08:58,711 --> 00:09:04,666
如果我們想要跳到NSLog這個程式碼的原始位置要怎麼辦呢

126
00:09:04,660 --> 00:09:07,911
一樣是滑鼠移過去但這次按下的是Command

127
00:09:07,910 --> 00:09:11,466
出現一個超連結之後按下滑鼠左鍵

128
00:09:11,466 --> 00:09:14,133
它就會跳到這個地方

129
00:09:14,133 --> 00:09:17,422
這個地方事實上在哪裡呢我們可以偷偷往上看

130
00:09:17,420 --> 00:09:21,000
它在MacOSX10.8裡面的Frameworks

131
00:09:21,000 --> 00:09:23,044
裡面的Fundation.framework

132
00:09:23,040 --> 00:09:27,555
裡面有個檔案NSObjectRuntime.h裡面

133
00:09:27,555 --> 00:09:32,755
在這個地方可以實際的找到NSLog這個方法

134
00:09:32,755 --> 00:09:35,244
要怎麼回到我們原本的程式碼呢

135
00:09:35,244 --> 00:09:37,511
事實上這是一個瀏覽器的概念

136
00:09:37,511 --> 00:09:41,044
所以我們可以在左上角看到一個返回的按鈕

137
00:09:41,044 --> 00:09:45,022
我們可以按這個地方返回到剛才所撰寫的程式碼

138
00:09:45,022 --> 00:09:47,600
這就是單元一的內容

