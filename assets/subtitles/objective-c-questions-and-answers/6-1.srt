﻿1
00:00:00,000 --> 00:00:09,940
[單元6：入門大哉問 /宣告的時候，要不要加*號]

2
00:00:10,400 --> 00:00:13,988
到底在Objective-C的使用過程中好像有些有打星號

3
00:00:13,980 --> 00:00:16,034
有時候就沒有打星號

4
00:00:16,034 --> 00:00:18,022
這原因到底是為什麼

5
00:00:18,370 --> 00:00:20,868
其實Objective-C的所有物件

6
00:00:20,860 --> 00:00:24,491
都是透過指標的方式找到位置進行存取

7
00:00:24,491 --> 00:00:28,514
所以你們看到的幾乎把分之八十以上

8
00:00:29,714 --> 00:00:34,160
在作類別宣告成物件的時候都有打星號

9
00:00:34,582 --> 00:00:37,348
沒有打那些的到底是誰我們來看一下

10
00:00:37,348 --> 00:00:41,131
第一種沒有使用的可能是C語言的原始型別

11
00:00:41,611 --> 00:00:44,777
所以我們看到的在Objective-C中混用C語言

12
00:00:44,777 --> 00:00:48,320
char int float這些東西

13
00:00:48,320 --> 00:00:51,131
你所看到的就沒有打上星號

14
00:00:52,788 --> 00:00:55,371
那所以使用上的情境就像這樣

15
00:00:55,371 --> 00:00:57,668
直接宣告一個int float

16
00:00:57,668 --> 00:01:04,171
那當然你也可以混用C言的這個加加減減都是可以拿來作使用的

17
00:01:06,445 --> 00:01:07,885
另外一種狀況是

18
00:01:08,220 --> 00:01:11,600
Objective-C本身也有一些基本的形別

19
00:01:11,600 --> 00:01:16,320
譬如說Boolean值NSInteger NSUInterger CGFloat

20
00:01:16,320 --> 00:01:19,337
這些也不用打上星號

21
00:01:20,171 --> 00:01:23,565
所以在使用上這個狀況就是比較特殊的

22
00:01:23,560 --> 00:01:24,571
NSRange

23
00:01:24,570 --> 00:01:28,480
很多人都會想說NS Objective-C的語法想説都要打星號

24
00:01:28,480 --> 00:01:29,634
事實上不然

25
00:01:29,988 --> 00:01:32,285
看這個結構本身是什麼

26
00:01:32,708 --> 00:01:38,045
NSRange本身是使用C語言的這個struct這樣的結構

27
00:01:38,045 --> 00:01:43,428
裡面持有這兩個NSUInteger Objective-C的基本型別

28
00:01:43,428 --> 00:01:46,960
所以NSRange不需要打星號

29
00:01:46,960 --> 00:01:50,274
這個是初學者很常遇到的一個問題

30
00:01:52,400 --> 00:01:54,857
我們也可以透過Objective-C 的

31
00:01:54,850 --> 00:01:58,880
NSString這個類別來生成物件來存放

32
00:01:58,880 --> 00:02:02,422
一般的字串透過這樣的方式來做存放

33
00:02:04,857 --> 00:02:10,982
那我們可不可以透過NSNumber生成的物件來持有

34
00:02:11,451 --> 00:02:13,154
基本型別值，可以的

35
00:02:13,154 --> 00:02:14,422
寫法要怎麼寫

36
00:02:14,720 --> 00:02:17,005
NSNumber打星號magicNumber

37
00:02:17,188 --> 00:02:25,188
存下來然後initWithI什麼去持有這些基本的形別

38
00:02:25,851 --> 00:02:29,668
持有之後我們想轉換成一般的

39
00:02:29,668 --> 00:02:32,502
譬如說int一般的float要怎麼做

40
00:02:32,502 --> 00:02:38,708
就是拿這個物件去呼叫方法做轉型

41
00:02:38,708 --> 00:02:46,297
intValue或是什麼floatValue這樣的方式來餵給C語言的型別

42
00:02:46,290 --> 00:02:50,365
那可不可以餵給Objective-C語言的基本型別呢

43
00:02:50,365 --> 00:02:53,485
可以，只是方法名稱不太一樣

44
00:02:53,480 --> 00:02:58,354
這邊叫做integerValue，字會點不一樣

45
00:02:58,354 --> 00:03:00,354
不同的方法名稱

46
00:03:00,350 --> 00:03:03,428
所以可以在從這個物件中撈出來

47
00:03:03,420 --> 00:03:06,217
餵給Objective-C語言的基本型別

