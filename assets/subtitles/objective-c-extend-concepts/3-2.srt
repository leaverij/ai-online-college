﻿1
00:00:00,000 --> 00:00:09,940
[單元3：繼承的概念 / 父類別、子類別，繼承的運作]

2
00:00:10,090 --> 00:00:13,980
接著我們來用一個例子說明繼承的概念

3
00:00:13,980 --> 00:00:17,577
如果我們本來就設計一個矩形的類別

4
00:00:17,577 --> 00:00:21,565
也有兩個iVar和兩個方法

5
00:00:22,297 --> 00:00:25,382
那我們接著又想要做一個正方型的類別

6
00:00:25,382 --> 00:00:29,794
那我們發現這個類別跟矩形類別有蠻多相似的地方

7
00:00:29,794 --> 00:00:34,720
那這個時候我們就可以來做一個正方型的類別

8
00:00:34,720 --> 00:00:37,565
繼承這矩形類別

9
00:00:38,320 --> 00:00:42,308
那麼當你將正方形類別繼承矩形類別的時候

10
00:00:42,914 --> 00:00:49,017
你就可以使用到原本這矩形類別的這些兩個iVar跟這兩個方法

11
00:00:49,394 --> 00:00:53,805
都可以在這正方型類別中做使用

12
00:00:54,480 --> 00:00:56,091
那不只繼承過來之後

13
00:00:56,091 --> 00:00:57,440
另外當然

14
00:00:57,440 --> 00:01:03,862
你還加上你自己在使用其他的iVar跟其他的方法

15
00:01:03,862 --> 00:01:07,417
來共同構成這個正方型的類別

16
00:01:07,410 --> 00:01:09,588
這就是繼承的概念

