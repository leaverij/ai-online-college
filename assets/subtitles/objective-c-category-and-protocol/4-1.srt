﻿1
00:00:00,000 --> 00:00:09,940
[單元4：類目(Category)與協定(Protocol) /類目(Category)]

2
00:00:10,651 --> 00:00:12,514
如果在我們原本這個Fraction

3
00:00:12,514 --> 00:00:16,217
在這個類別你想要增加幾個方法

4
00:00:16,217 --> 00:00:17,805
只是為了要增加幾個方法

5
00:00:18,457 --> 00:00:21,691
那你不需要繼承Fraction再創在另外一個類別

6
00:00:21,691 --> 00:00:24,674
你只需要透過這樣撰寫方式

7
00:00:24,674 --> 00:00:28,571
再寫一次@interface @end

8
00:00:28,571 --> 00:00:30,377
@implement @end

9
00:00:31,234 --> 00:00:36,377
掛上原本的類別名稱後面加個小括號取個名字來對應

10
00:00:37,234 --> 00:00:40,617
接著你就可以增加一些方法實作一些方法

11
00:00:41,645 --> 00:00:44,011
所以在這些程式區段之後

12
00:00:44,011 --> 00:00:47,691
Fraction就增加了這四個方法可以來做使用

13
00:00:49,394 --> 00:00:52,240
好比說NSString是一個很大的類別

14
00:00:53,577 --> 00:00:56,137
那我只是想要增加一個方法

15
00:00:56,137 --> 00:00:57,782
就可以透過這樣的方式

16
00:00:57,782 --> 00:01:02,628
CheckEmpty增加一個isEmpty的方法來做這樣的實作

17
00:01:03,485 --> 00:01:08,114
所以就把方法的宣告加上方法的實作

18
00:01:08,708 --> 00:01:11,988
透過這樣再過了這個程式碼區段之後

19
00:01:12,411 --> 00:01:16,628
你的所有NSString使用之後所產生出的物件

20
00:01:17,177 --> 00:01:20,708
多了一個isEmpty的方法可以來做操作

