﻿1
00:00:01,320 --> 00:00:08,914
[單元1：Struts 2 : 一個全新的MVC框架 /熟悉Struts 2 Framework]

2
00:00:11,160 --> 00:00:17,451
我們現在看到的這幾支程式是剛才所看過的幾支程式

3
00:00:17,850 --> 00:00:22,571
前面這兩支是傳統的Servlet JSP裡面的程式

4
00:00:22,580 --> 00:00:26,720
右邊四支則是Struts專案裡面的

5
00:00:27,340 --> 00:00:29,840
那我們看一下這些JSP的所在位置

6
00:00:33,310 --> 00:00:35,485
我們在Eclipse的開發環境

7
00:00:35,500 --> 00:00:39,074
在你的專案底下會有個目錄叫做WebContent

8
00:00:40,250 --> 00:00:42,320
下面這些就是我們的JSP

9
00:00:43,970 --> 00:00:50,765
控制器Servlet程式是位於Java Resources src底下的位置

10
00:00:50,780 --> 00:00:52,160
這是你的Servlet程式

11
00:00:52,630 --> 00:00:54,731
利用這個機會稍微說明一下

12
00:00:55,190 --> 00:00:57,702
因為我們今天的開發環境是Eclipse

13
00:00:58,590 --> 00:01:03,268
所以它會有資料夾叫做Java Resources src WebContent

14
00:01:03,270 --> 00:01:07,154
如果今天是在實際的伺服器環境底下

15
00:01:07,160 --> 00:01:08,902
並沒有這兩個目錄

16
00:01:09,330 --> 00:01:13,851
所謂WebContent將是你的專案名稱直接下面

17
00:01:13,860 --> 00:01:16,068
就是你的JSP

18
00:01:17,080 --> 00:01:22,674
Java程式的部分在傳統的伺服器裡面

19
00:01:22,680 --> 00:01:25,805
在它的資料夾底下

20
00:01:25,810 --> 00:01:29,645
就是專案名稱底下會有一個子目錄叫WEB-INF

21
00:01:30,260 --> 00:01:32,994
WEB-INF底下會有一個classes的目錄

22
00:01:33,430 --> 00:01:36,628
classes目錄放的就是這些Java程式

23
00:01:36,630 --> 00:01:41,497
所以這個畫面的環境位置是Eclipse開發環境

24
00:01:41,890 --> 00:01:44,834
因此記得伺服器的位置是不太一樣的

25
00:01:44,840 --> 00:01:48,011
但是兩邊都是對的，因為這邊屬於開發環境

26
00:01:49,780 --> 00:01:51,851
那麼Struts的程式碼

27
00:01:51,860 --> 00:01:56,685
JSP addEMP當然也是在WebContent底下

28
00:01:56,690 --> 00:01:59,817
如果以Eclipse開發環境而言是在這裡

29
00:02:00,460 --> 00:02:03,977
那控制器的這Servlet支程式EmpAction

30
00:02:03,980 --> 00:02:07,497
它是在src底下同樣位置

31
00:02:07,500 --> 00:02:12,605
稍微注意就是，因為了你方便作錯誤驗證

32
00:02:12,610 --> 00:02:14,354
所以它有一個xml檔案

33
00:02:14,360 --> 00:02:19,531
目前xml檔案名稱就跟你的Java程式名稱是一樣的

34
00:02:19,540 --> 00:02:23,862
多一個-validation.xml，這驗證的部分

35
00:02:25,050 --> 00:02:30,274
至於幫你表列怎麼進怎麼出的這個設定檔

36
00:02:30,920 --> 00:02:33,931
就這一個，Struts.xml這個檔案

37
00:02:34,510 --> 00:02:39,554
它會位在我們的classes底下，就src底下在這裡

38
00:02:40,530 --> 00:02:43,908
看的到怎麼進怎麼出

39
00:02:43,910 --> 00:02:50,697
我們現在用JSP用Struts的版本執行一下大概的過程

40
00:02:51,620 --> 00:02:53,451
點一下這個新增

41
00:02:56,430 --> 00:03:00,902
那如果這資料沒有填沒有填沒有填，它幫你做錯誤處理

42
00:03:02,360 --> 00:03:03,862
一些要求

43
00:03:04,300 --> 00:03:06,720
那以長度而言，或文字的部分

44
00:03:06,730 --> 00:03:11,337
我輸入一個a，然後給它一個怪符號，我們會把你踢掉

45
00:03:13,640 --> 00:03:16,091
只能是中英文字母、數字和底線

46
00:03:16,550 --> 00:03:20,571
長度必在2-10之間，這是xml檔案設定的部分

47
00:03:20,580 --> 00:03:23,234
那我key in 一個 Peter

48
00:03:24,310 --> 00:03:27,930
那職位也是有類似的錯誤處理，key 一個 manger

49
00:03:30,140 --> 00:03:35,565
現在送出一次，這就是正確的，沒問題

50
00:03:36,130 --> 00:03:43,028
錯誤處理這裡，有錯誤處理薪水、獎金

51
00:03:44,360 --> 00:03:46,182
錯誤處理過的資料

52
00:03:46,190 --> 00:03:49,748
對的正確的會幫你留下來，怎麼做處理

53
00:03:51,560 --> 00:03:56,228
現在輸入一個正確的薪水跟獎金

54
00:03:58,940 --> 00:03:59,851
按送出

55
00:04:00,830 --> 00:04:04,022
這新增成功的資料

56
00:04:04,030 --> 00:04:06,020
這成功的畫面

