﻿1
00:00:01,080 --> 00:00:08,240
[單元2--Android專案管理與App在地化 / Android專案在模擬器上執行]

2
00:00:10,800 --> 00:00:14,240
要讓Android專案到時候變成App

3
00:00:14,400 --> 00:00:17,480
要可以放在模擬器上執行的話

4
00:00:17,560 --> 00:00:20,060
首先當然要先建立模擬器

5
00:00:20,540 --> 00:00:24,960
模擬器建立之後，才能在模擬器上執行專案

6
00:00:25,280 --> 00:00:31,320
那建立模擬器的話，必須先開啟建立模擬器的這個功能

7
00:00:31,320 --> 00:00:34,220
我們現在切換到eclipse

8
00:00:36,020 --> 00:00:41,440
在eclipse當中，工具列上面有一個『Android Virtual Device Manager』

9
00:00:41,740 --> 00:00:46,580
這一個按鈕案下去之後，可以看到可以建立模擬器

10
00:00:46,580 --> 00:00:50,180
我們現在已經建立了一個，那我們在建立一個新的

11
00:00:51,260 --> 00:00:52,640
再建立一個新的

12
00:00:53,240 --> 00:00:55,360
在建立模擬器的話

13
00:00:55,740 --> 00:01:01,080
首先先選擇模擬器，它必須是什麼樣的裝置

14
00:01:01,080 --> 00:01:04,000
那這個什麼裝置其實可以自由的選取

15
00:01:04,260 --> 00:01:06,360
或許你可以選取『Nexus』的機種

16
00:01:07,120 --> 00:01:08,440
稱做Google Phone

17
00:01:08,440 --> 00:01:13,520
Google Phone就是Google設計然後請其他廠商代工的

18
00:01:13,840 --> 00:01:17,120
那我們現在看到坊間比較高階的手機

19
00:01:17,120 --> 00:01:20,720
可能跟Nexus 4比較相近，那我們就選擇『Nexus 4』

20
00:01:21,880 --> 00:01:26,820
『Target』我們現在選擇想要執行的模擬器版本

21
00:01:26,820 --> 00:01:29,140
Android 4.3這個作業系統

22
00:01:29,380 --> 00:01:32,680
這邊會跳出ARM，這個其實就是我們之前

23
00:01:32,680 --> 00:01:39,520
在Android SDK Package的時候，都已經裝好了對應模擬器需要的套件

24
00:01:41,820 --> 00:01:44,280
AVD名稱就是這個模擬器的名稱

25
00:01:44,280 --> 00:01:48,180
我們現在可以給它，譬如說就叫做 『Android』

26
00:01:48,740 --> 00:01:52,760
它是4.3版，那我們就建立Android43

27
00:01:54,000 --> 00:02:00,120
這邊它是Nexus 4，那我們可以給個名稱Nexus…

28
00:02:02,600 --> 00:02:03,280
Nexus4

29
00:02:03,280 --> 00:02:07,140
當然不可以有空白鍵，有空白鍵的話就可以看見下面就會跳錯

30
00:02:07,160 --> 00:02:10,420
並且告訴你的名稱只能是小寫a到z

31
00:02:10,420 --> 00:02:13,540
大寫A到Z、0到9、點和底線

32
00:02:13,540 --> 00:02:16,120
等等這些可以

33
00:02:16,120 --> 00:02:17,360
但是不可以留空白

34
00:02:17,360 --> 00:02:20,360
那我們建立這個名稱可以隨意

35
00:02:20,360 --> 00:02:22,900
隨我們的意思來建立

36
00:02:22,900 --> 00:02:26,640
可以看到下面這個『Keyboard』、『Skin』，那我們就把它勾起來

37
00:02:27,740 --> 00:02:31,760
Front Camera就是模擬前面鏡頭

38
00:02:31,760 --> 00:02:33,860
前鏡頭的話，你可以不要模擬

39
00:02:33,860 --> 00:02:39,080
也可以選擇這種是…假模擬

40
00:02:39,220 --> 00:02:43,780
這種假模擬是說，它不管怎麼樣拍照的話都是

41
00:02:44,640 --> 00:02:47,060
拍出來都是Android的圖示

42
00:02:47,060 --> 00:02:49,900
那你也可以選擇Webcam，如果你是用NoteBook

43
00:02:50,280 --> 00:02:54,840
NoteBook上面的Webcam只要能抓到的它就可以利Webcam來拍照

44
00:02:54,980 --> 00:02:59,540
這個就是真模擬，真的可以拍照，只不過是透過Webcam

45
00:03:00,060 --> 00:03:04,760
那這邊這個Back Camera就是背鏡頭

46
00:03:04,860 --> 00:03:06,820
背後的那個鏡頭也可以設定

47
00:03:06,820 --> 00:03:09,780
那我們現在暫時先選擇『Emulated』

48
00:03:12,760 --> 00:03:17,360
『Memory Options』這邊建議設『512』就可以了

49
00:03:17,360 --> 00:03:19,360
『512MG』

50
00:03:19,720 --> 00:03:22,620
其他的就是可以不需要去更動它

51
00:03:22,620 --> 00:03:24,760
『Internal Storage』它現在設『200』

52
00:03:25,040 --> 00:03:28,340
SD Card這邊建議要設『200MG』

53
00:03:28,340 --> 00:03:34,680
因為之後我們會有範例需要把某一些檔案存在SD Card

54
00:03:34,680 --> 00:03:37,780
所以這邊是最好建立一個200MG

55
00:03:38,440 --> 00:03:42,080
『Emulation Options』這邊建議勾『Snapshot』

56
00:03:42,080 --> 00:03:43,800
因為開啟模擬器

57
00:03:44,220 --> 00:03:48,140
是需要…這開機時間需要比較久，可能3到5分鐘

58
00:03:48,560 --> 00:03:52,040
如果不希望每次開機都花3到5分鐘的話

59
00:03:52,040 --> 00:03:53,300
可以勾Snapshot

60
00:03:53,480 --> 00:03:58,200
它第一次開機會比較久，可是它會把這個過程都存起來

61
00:03:58,460 --> 00:04:02,460
下一次的時候就不需要花這麼久的時間來開機

62
00:04:02,660 --> 00:04:06,540
可以在大約10秒左右就可以開機完成

63
00:04:06,540 --> 00:04:07,920
那我們現在按『OK』

64
00:04:08,360 --> 00:04:11,760
按OK之後，可以看到現在建立了

65
00:04:12,840 --> 00:04:16,320
我們剛剛建立了是這個『Android43_Nexus4』

66
00:04:17,160 --> 00:04:20,600
建好之後，我們當然是可以啟動它，點選它之後

67
00:04:20,780 --> 00:04:21,680
按『start』

68
00:04:22,500 --> 00:04:24,680
start這邊

69
00:04:24,960 --> 00:04:28,120
我們剛剛因為有勾Snapshot功能，所以這邊

70
00:04:28,300 --> 00:04:33,980
這兩個『Lauch from snapshot』跟『Save to snapshot』都可以勾起來

71
00:04:33,980 --> 00:04:36,760
這個是說載入snapshot檔案

72
00:04:37,200 --> 00:04:40,400
第一次的時候，當然我們還沒有存開機的過程

73
00:04:40,400 --> 00:04:43,820
所以無法載入任何snapshot

74
00:04:43,820 --> 00:04:45,160
不過這還是可以勾起來

75
00:04:45,480 --> 00:04:51,160
那Save勾起來就是會所有的開機過程以及之後的操作全都存檔

76
00:04:51,340 --> 00:04:55,140
這樣方便我們一開啟，下一次的開啟的時候，因為有勾Lauch

77
00:04:55,240 --> 00:04:59,940
一開啟的時候，它會Lauch那個舊的snapshot

78
00:04:59,940 --> 00:05:03,500
馬上就可以回到上一次操作的畫面

79
00:05:03,840 --> 00:05:06,520
那這邊『Scalar display to real size』

80
00:05:06,820 --> 00:05:08,260
這邊可以勾起來

81
00:05:09,040 --> 00:05:13,540
這個是模擬真實手機的尺寸，但是用4.7的話會蠻小的

82
00:05:13,540 --> 00:05:19,280
一般這個『Monitor dpi 』設『96』的話，這邊建議設『8』

83
00:05:19,280 --> 00:05:21,660
等會兒模擬器看起來大小會比較剛好

84
00:05:22,700 --> 00:05:27,360
好的，那我們現在可以點選『Lauch』

85
00:05:28,660 --> 00:05:30,840
點之後『Lauch』它就會開啟

86
00:05:31,520 --> 00:05:34,320
Nexus4，我們剛剛選好的

87
00:05:36,360 --> 00:05:39,060
好的，它現在就會開啟

88
00:05:39,460 --> 00:05:42,800
因為我們之前沒有開過這個模擬器

89
00:05:43,160 --> 00:05:46,180
所以它開啟的話，就要花比較久的時間

90
00:05:47,200 --> 00:05:51,560
然後到時候在開機畫面一個始它會show這個Android 幾個字

91
00:05:51,560 --> 00:05:55,420
然後它會一直再閃，那我們必須要等到3~5分鐘

92
00:05:57,680 --> 00:05:59,560
開機完成的畫面

93
00:06:00,260 --> 00:06:02,220
就是長這樣，它是彩色的

94
00:06:02,220 --> 00:06:05,660
一開始會要你按OK，那我們就按一下『OK』

95
00:06:08,140 --> 00:06:11,440
模擬器操作會稍微慢一點，會稍微頓一點

96
00:06:11,840 --> 00:06:14,580
按OK之後，我們就可以看到進入這個畫面

97
00:06:15,000 --> 00:06:18,700
那我們現在建議各位就是可以把…

98
00:06:19,200 --> 00:06:21,160
設定抓到桌面上

99
00:06:21,160 --> 00:06:23,160
按這個應用程式列表之後

100
00:06:24,340 --> 00:06:26,520
我們可以看到這是所有應用程式列表

101
00:06:29,080 --> 00:06:32,120
那第一次進來它也是會要我們按一下『OK』

102
00:06:34,040 --> 00:06:37,120
案下OK之後呢，我們可以看到這邊有『Settings』

103
00:06:37,120 --> 00:06:40,720
Settings…我們現在久按它

104
00:06:41,200 --> 00:06:45,140
久案它之後，它可以移到我們的桌面上

105
00:06:45,140 --> 00:06:46,800
那我們現在把它放下去

106
00:06:46,980 --> 00:06:50,380
因為在開發的時候，會常常用到Settings

107
00:06:50,760 --> 00:06:54,220
那我們可以在現在的時候做一個

108
00:06:55,660 --> 00:06:57,400
休眠狀態設定

109
00:06:58,720 --> 00:07:00,460
我們先做一下這個設定

110
00:07:00,800 --> 00:07:05,040
Display這裡…『Display』這邊點下去之後

111
00:07:05,860 --> 00:07:09,960
因為它預設的休眠狀態是1分鐘它就休眠

112
00:07:09,960 --> 00:07:12,420
那我們現在把它改久一點

113
00:07:12,800 --> 00:07:16,820
因為我們到時後在開發的時候丟應用程式進來

114
00:07:16,820 --> 00:07:19,100
丟App進來的時候，它很快就會睡著

115
00:07:19,100 --> 00:07:21,100
我們不想讓它這麼快就睡著

116
00:07:21,840 --> 00:07:24,860
最長30分鐘，我們把它設好，設好之後

117
00:07:25,020 --> 00:07:29,280
OK，我們其實現在就可以使用這個模擬器了

118
00:07:31,160 --> 00:07:35,500
接下來我們就是想要把我們建立的這個專案

119
00:07:35,680 --> 00:07:40,820
我們建立的這個專案能夠丟到這個模擬器來執行一下

120
00:07:40,820 --> 00:07:42,820
我們點選這個專案

121
00:07:44,420 --> 00:07:46,300
然後按一下『執行』

122
00:07:49,360 --> 00:07:51,340
然後選擇『Android Application』

123
00:07:52,420 --> 00:07:53,620
要等它跑一下

124
00:07:54,100 --> 00:07:58,700
這邊其實有跳出console出來

125
00:07:58,700 --> 00:08:01,180
console的話，我們現在選一下，選Andorid

126
00:08:02,960 --> 00:08:07,600
選Android的話它就會…到時候就會顯示

127
00:08:07,600 --> 00:08:10,800
它現在安裝的情形等等這些資訊

128
00:08:11,540 --> 00:08:15,840
好第一次我們是要去把這一個

129
00:08:16,540 --> 00:08:21,660
Android專案裝到這個模擬器上面發生失敗，那沒關系我們再試一次

130
00:08:22,080 --> 00:08:24,000
我現在把這個移下來

131
00:08:25,960 --> 00:08:28,060
把它移到下面

132
00:08:28,880 --> 00:08:31,140
第一次失敗，我們可以再試一次

133
00:08:31,520 --> 00:08:35,920
那跳這個的時候代表它這個

134
00:08:36,360 --> 00:08:39,360
HelloAndroid這個專案丟到模擬器上面發生失敗

135
00:08:39,580 --> 00:08:43,200
各位可以看到它現在開始安裝

136
00:08:45,200 --> 00:08:49,840
OK，安裝好之後它會跳這個『starting activity』

137
00:08:49,840 --> 00:08:54,480
最主要是看『starting Intent』這樣的字眼

138
00:08:54,740 --> 00:09:00,720
那就代表它其實已經把這個專案安裝在我們的模擬器上面

139
00:09:01,140 --> 00:09:02,840
接下來跳出這個它是說

140
00:09:02,840 --> 00:09:07,260
要不要讓Logcat這個功能去監控

141
00:09:07,460 --> 00:09:10,640
這個模擬器或者是到時候手機上面的資訊

142
00:09:10,640 --> 00:09:12,800
當然是OK，按『OK』

143
00:09:13,440 --> 00:09:15,680
那它這邊就會監控…資訊

144
00:09:15,700 --> 00:09:22,060
那我們現在來看一下模擬器是不是已經執行了我們的專案

145
00:09:22,060 --> 00:09:26,860
對，這邊是我們剛剛當初建立的專案還沒有改

146
00:09:26,860 --> 00:09:28,800
那預設它就是會跳HelloWord

147
00:09:29,400 --> 00:09:33,600
各位可以看到…我們第一個建立的這個專案

148
00:09:33,600 --> 00:09:35,600
已經丟到模擬器上面執行

149
00:09:37,220 --> 00:09:39,300
那我們就說明到此結束

