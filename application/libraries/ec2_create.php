<?php

require_once('vendor/autoload.php');
use Aws\Ec2\Ec2Client;

class Ec2_create {
    function createInstance($chapter_id, $chapter_content_id, $s3_url=null, $lab_duration=60){

        // ec2 connection config
        $ec2Client = Ec2Client::factory(array(
            'credentials' => array(
                'key'=>'AKIAIBIHYFIEDY6L6GKA',
                'secret'=>'HwbwfvawdZG8NepaFEofdAHN/3rCE3TsyeVTBIbt',
            ),
            'version' => '2016-11-15',
            'region' => 'ap-northeast-1', // (e.g., us-east-1, http://docs.aws.amazon.com/general/latest/gr/rande.html)
            'http'    => [
                'verify' => false
            ]
        ));

        // select key pair
        $keyPairName = 'forjupyter';

        // select security group
        $securityGroupName = 'launch-wizard-1';
        
        // select image(AMI)
        $ImageId = 'ami-30d13c4f';

        // select instance type
        $InstanceType = 't2.micro';
        
        // LAB start duration
        $lab_duration = $lab_duration / 60;

        // command
       if($s3_url!==null){        
            $script = "#!/bin/bash\r\nsudo wget --restrict-file-names=nocontrol -P /home/ubuntu/jupyter_work ".$s3_url."\r\nshutdown -H +".$lab_duration;
       }else{
            $script = "#!/bin/bash\r\nshutdown -H +".$lab_duration; 
       }
       $init_script = base64_encode($script);

        // Launch an instance with the key pair and security group
        $result = $ec2Client->runInstances(array(
            'ImageId'        => $ImageId,
            'MinCount'       => 1,
            'MaxCount'       => 1,
            'InstanceType'   => $InstanceType,
            'KeyName'        => $keyPairName,
            'SecurityGroups' => array($securityGroupName),
            'InstanceInitiatedShutdownBehavior' => 'terminate'
            //,'UserData'=> $init_script
        ));

        $result = $result->toArray();
        $pathresult = $this->getkeypath($result, 'InstanceId');
        krsort($pathresult);
        $instanceIds = $result;
        foreach ($pathresult as $value) {
            $instanceIds = $instanceIds[$value];
        }

        $ec2Client->waitUntil('InstanceRunning', array('InstanceIds' => array($instanceIds)));

        // Describe the now-running instance to get the public URL
        $result = $ec2Client->describeInstances(array(
            'InstanceIds' => array($instanceIds)
        ));
        $result = $result->toArray();
        $pathresult = $this->getkeypath($result, 'PublicDnsName');
        krsort($pathresult);
        $PublicDnsName = $result;
        foreach ($pathresult as $value) {
            $PublicDnsName = $PublicDnsName[$value];
        }
        $result = array('id'=>$instanceIds, 'dns'=>$PublicDnsName);
        return $result;
    }

    function getkeypath($arr, $lookup){
        if (array_key_exists($lookup, $arr))
        {
            return array($lookup);
        }
        else
        {
            foreach ($arr as $key => $subarr)
            {
                if (is_array($subarr))
                {
                    $ret = $this->getkeypath($subarr, $lookup);

                    if ($ret)
                    {
                        $ret[] = $key;
                        return $ret;
                    }
                }
            }
        }
        return null;
    }

    function createKeyPair($ec2Client, $keyPairName){
        $result = $ec2Client->createKeyPair(array(
            'KeyName' => $keyPairName
        ));
        $result = $ec2Client->getKeyPair([
            'keyPairName' => $keyPairName, // REQUIRED
        ]);
        // Save the private key
        $saveKeyLocation = getenv('DOCUMENT_ROOT') . "/aws/.ssh/{$keyPairName}.pem";
        file_put_contents($saveKeyLocation, $result['KeyMaterial']);
        // Update the key's permissions so it can be used with SSH
        chmod($saveKeyLocation, 0600);
        return $result;
    }

    function createSecurityGroup($ec2Client, $securityGroupName, $Description){
        $result = $ec2Client->createSecurityGroup(array(
            'GroupName'   => $securityGroupName,
            'Description' => $Description
        ));
        return $result;
    }

    function ManageInstance($instanceId, $action){
        $ec2Client = Ec2Client::factory(array(
            'credentials' => array(
                'key'=>'AKIAIBIHYFIEDY6L6GKA',
                'secret'=>'HwbwfvawdZG8NepaFEofdAHN/3rCE3TsyeVTBIbt',
            ),
            'version' => '2016-11-15',
            'region' => 'ap-northeast-1', // (e.g., us-east-1, http://docs.aws.amazon.com/general/latest/gr/rande.html)
            'http'    => [
                'verify' => false
            ]
        ));
        if ($action === 'START'){
            $result = $ec2Client->startInstances(array(
                'InstanceIds' => array($instanceId)
            ));
            $result = $result->toArray();
            $CurrentStateCode = $result;
            $pathresult = $this->getkeypath($result, 'CurrentState');
            krsort($pathresult);
            foreach ($pathresult as $value) {
                $CurrentStateCode = $CurrentStateCode[$value];
            }
            $CurrentStateCode = $CurrentStateCode['Code'];
            $ec2Client->waitUntil('InstanceRunning', array('InstanceIds' => array($instanceId)));
            if($CurrentStateCode=='0' || $CurrentStateCode=='16'){
                $result = $ec2Client->describeInstances(array(
                    'InstanceIds' => array($instanceId)
                ));
                $result = $result->toArray();
                $pathresult = $this->getkeypath($result, 'PublicDnsName');
                krsort($pathresult);
                $PublicDnsName = $result;
                foreach ($pathresult as $value) {
                    $PublicDnsName = $PublicDnsName[$value];
                }
                $result = array('status'=>'success', 'id'=>$instanceId, 'dns'=>$PublicDnsName);
            }else{
                $result = array('status'=>'failure', 'message'=>'START failure!');
            }
            
        }else if($action === 'STOP'){
            $result = $ec2Client->stopInstances(array(
                'InstanceIds' => array($instanceId)
            ));
            $result = $result->toArray();
            $CurrentStateCode = $result;
            $pathresult = $this->getkeypath($result, 'CurrentState');
            krsort($pathresult);
            foreach ($pathresult as $value) {
                $CurrentStateCode = $CurrentStateCode[$value];
            }
            $CurrentStateCode = $CurrentStateCode['Code'];
            $ec2Client->waitUntil('InstanceRunning', array('InstanceIds' => array($instanceId)));
            if($CurrentStateCode=='64' || $CurrentStateCode=='80'){
                $result = array('status'=>'success', 'message'=>'STOP success!');
            }else{
                $result = array('status'=>'failure', 'message'=>'STOP failure!');
            }
        }
        return $result;
        
        // $result = $result->toArray();
        // $CurrentStateCode = $result;
        // $pathresult = $this->getkeypath($result, 'CurrentState');
        // krsort($pathresult);
        // foreach ($pathresult as $value) {
        //     $CurrentStateCode = $CurrentStateCode[$value];
        // }
        // $CurrentStateCode = $CurrentStateCode['Code'];
        // return $CurrentStateCode;
       
    }
}
