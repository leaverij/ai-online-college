<?php

require_once('vendor/autoload.php');
use Aws\S3\S3Client;

class S3_upload {
    //function uploadS3($pathToFile, $file_name, $course_id, $chapter_id, $chapter_content_id){
    function uploadS3($pathToFile, $fileUUID, $file_name, $course_id, $chapter_id, $file_type){

        // s3 connection config
        $client = S3Client::factory(array(
            'credentials' => array(
                'key'=>'AKIAIBIHYFIEDY6L6GKA',
                'secret'=>'HwbwfvawdZG8NepaFEofdAHN/3rCE3TsyeVTBIbt',
            ),
            'version' => 'latest',
            'region' => 'ap-northeast-1', // (e.g., us-east-1, http://docs.aws.amazon.com/general/latest/gr/rande.html)
            'http'    => [
                'verify' => false
            ]
        ));
        
        // uplaod bucket name
        $bucket = 'jupiter-file';
        
        // folder name
        //$Key = $course_id.'/'.$chapter_id.'/'.$chapter_content_id.'/'.$file_name;
        $Key = 'course-jupyter/'.$course_id.'/'.$chapter_id.'/'.$fileUUID.'/'.$file_name;
        if($file_type=='0'){
            $Key = 'course-video/'.$course_id.'/'.$chapter_id.'/'.$fileUUID.'/'.$file_name;
        }
        // Upload an object by streaming the contents of a file
        // SourceFile should be absolute path to a file on disk
        $result = $client->putObject(array(
            'Bucket'     => $bucket,
            'Key'        => $Key,
            'SourceFile' => $pathToFile,
            'ACL'        => 'public-read'
        ));
        
        // We can poll the object until it is accessible
        $client->waitUntil('ObjectExists', array(
            'Bucket' => $bucket,
            'Key'    => $Key
        ));
        
        // show file url
        //echo $result['ObjectURL'];
        //exit;
        return $result['ObjectURL'];
    }

    function getkeypath($arr, $lookup)
    {
        if (array_key_exists($lookup, $arr))
        {
            return array($lookup);
        }
        else
        {
            foreach ($arr as $key => $subarr)
            {
                if (is_array($subarr))
                {
                    $ret = $this->getkeypath($subarr, $lookup);

                    if ($ret)
                    {
                        $ret[] = $key;
                        return $ret;
                    }
                }
            }
        }
        return null;
    }

    function createKeyPair($ec2Client, $keyPairName){
        $result = $ec2Client->createKeyPair(array(
            'KeyName' => $keyPairName
        ));
        $result = $ec2Client->getKeyPair([
            'keyPairName' => $keyPairName, // REQUIRED
        ]);
        // Save the private key
        $saveKeyLocation = getenv('DOCUMENT_ROOT') . "/aws/.ssh/{$keyPairName}.pem";
        file_put_contents($saveKeyLocation, $result['KeyMaterial']);
        // Update the key's permissions so it can be used with SSH
        chmod($saveKeyLocation, 0600);
        return $result;
    }

    function createSecurityGroup($ec2Client, $securityGroupName, $Description){
        $result = $ec2Client->createSecurityGroup(array(
            'GroupName'   => $securityGroupName,
            'Description' => $Description
        ));
        return $result;
    }
}
