<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form id="addComapnyForm" class="form-horizontal" role="form" method="post" action="<?php echo base_url().'company/insertCompany'?>" enctype="multipart/form-data">
                <div class="item-container job-item">
                    <h3>企業設定</h3>
                    <!-- <span style="color: #999999;">此頁為提供企業端一編輯畫面，新增一筆職缺。主要分為兩部分：1.職務資訊設定。2.求才條件。3.應徵方式</span> -->
                    <div class="well" style="min-height: 280px;">
<!--                         <span class="h4" style="color: #999999;">企業資訊</span>
                        <hr> -->
                        <div class="form-group col-sm-12">
                            <label for="company_name" class="col-sm-3 control-label"><span style="color:red;">＊</span>企業名稱</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="company_name" name="company_name" placeholder="請填入企業名稱" value="">
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="tax_id_number" class="col-sm-3 control-label"><span style="color:red;">＊</span>統一編號</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="tax_id_number" name="tax_id_number" placeholder="請填入統一編號" value="">
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="contact_name" class="col-sm-3 control-label"><span style="color:red;">＊</span>聯絡人姓名</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="聯絡人姓名" value="">
                            </div>
                        </div>                        
                        <div class="form-group col-sm-12">
                            <label for="contact_mail" class="col-sm-3 control-label"><span style="color:red;">＊</span>聯絡人信箱</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="contact_mail" name="contact_mail" placeholder="請填入聯絡人信箱" value="">
                            </div>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="contact_phone" class="col-sm-3 control-label"><span style="color:red;">＊</span>聯絡人電話</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="contact_phone" name="contact_phone" placeholder="請填入聯絡人電話" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls form-inline col-sm-12">
                        <div class="col-sm-offset-4 col-sm-1">
                            <button type="submit" name="previewCompany" value="previewCompany" class="btn btn-primary">預覽</button>
                        </div>
                        <div class="col-sm-offset-1 col-sm-1">
                            <button type="submit" name="insertCompany" value="insertCompany" class="btn btn-success">儲存</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div><!-- /container -->