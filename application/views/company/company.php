<div class="container">
    <div class="row" style="margin-top: 20px;">
        <div class="col-sm-12">         
            <form id="deleteCompanyForm" class="form-horizontal" role="form" method="post" action="<?php echo base_url().'company/deleteCompany'?>" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-xs-12">
                        <?php if(!empty($getCompany)){?>
                        <button type="submit" name="deleteCompany" value="deleteCompany" class="btn btn-default col-md-2 col-xs-12"><i class="fa fa-trash-o"></i> 刪除企業</button>
                        <?php }?>
                        <a class="btn btn-default col-md-offset-8 col-md-2 col-xs-12" href="<?php echo base_url().'company/addCompany';?>"><i class="fa fa-plus"></i> 新增企業</a>
                    </div>
                </div>
                <div class="tab-content">
                    <?php if(!empty($getCompany)){?>
                        <div style="padding-top: 20px;">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th><?php if(!empty($getCompany)){?><input id="selectAllCompanyBtn" name="selectAllCompanyBtn" type="checkbox"><?php }?></th>
                                            <th>企業名稱</th>
                                            <th>統一編號</th>
                                            <th>聯絡人姓名</th>
                                            <th>聯絡人電話</th>
                                            <th>聯絡人信箱</th>
                                            <th>編輯</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <? foreach($getCompany as $key => $row){?>
                                            <tr>
                                                <td><input name="deleteCompanyId[]" class="deleteCompanyId" type="checkbox" value="<?php echo $row->tax_id_number;?>"></td>
                                                <td><?php echo $row->company_name;?></a></td>
                                                <td><?php echo $row->tax_id_number;?></td>
                                                <td><?php echo $row->contact_name;?></td>
                                                <td><?php echo $row->contact_phone;?></td>
                                                <td><?php echo $row->contact_mail;?></td>
                                                <td><a href="<?php echo base_url().'company/editCompany/'.$row->tax_id_number;?>" class="btn btn-default"><div class="fa fa-edit"></div>  編輯企業</a></td>
                                            </tr>
                                        <? }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <? }else{?>
                        <tr>
                            <h4 style="color:#a7a7a7;" class="text-center">您尚未新增任何企業</h4>
                        </tr>
                    <? }?>
                </div>
            </form>
        </div>
    </div>
</div>