<style>
.company-info {
	padding-right: 15px;
	padding-left: 15px;
}
</style>
<section>
	<div class="container">
		<div class="row" style="margin:20px;">
			<div class="col-md-offset-2 col-sm-8" style="margin-top:20px;">
				<div class="thumbnail" style="padding-bottom: 20px;">
					<h4 style="padding-left: 5px;">企業名稱</h4>
					<div class="company-info"><?php echo $getOneCompany['company_name'];?></div>
					<hr>
					<h4 style="padding-left: 5px;">統一編號</h4>
					<div class="company-info"><?php echo $getOneCompany['tax_id_number'];?></div>
					<hr>
					<h4 style="padding-left: 5px;">聯絡人姓名</h4>
					<div class="company-info"><?php echo $getOneCompany['contact_name'];?></div>
					<hr>
					<h4 style="padding-left: 5px;">聯絡人信箱</h4>
					<div class="company-info"><?php echo $getOneCompany['contact_mail'];?></div>
					<hr>
					<h4 style="padding-left: 5px;">聯絡人電話</h4>
					<div class="company-info"><?php echo $getOneCompany['contact_phone'];?></div>
				</div>
			</div>
		</div>
	</div>
</section>