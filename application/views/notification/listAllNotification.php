<style>
	.notification-container ul {padding:0;}
	.notification-container ul li {list-style:none; border-top:1px solid #ededed; padding:15px 0;}
	.notification-container ul li:first-child {border-top:none;}
	.avatar {
		width:60px;
		height:60px;
		overflow:hidden;
		background-position:center;
		background-size:50px auto !important;
		border-radius:50px;
		border:5px solid #eee;}
	.avatar img {width:60px; height:60px; display:none;}
	.forum.notification .avatar {margin:0 auto;}
	.notif-meta {}
	.notif-meta h4 {font-size:1.14em; margin:0;}
	.notif-meta p {margin:0; font-size:0.92em; color:#999; line-height:2em;}
	.notif-meta .time { font-size:0.85em; color:#bbb;}
	.discussion-list .reply-count {padding-top:10px; color:#999; float:right; text-align:center;}
	.discussion-list .reply-count strong {font-size:24px;}
</style>
<?php 
$row=json_decode($getNotificationPagination);
?>
<div class="container">
	<div style="padding-bottom:20px;">
		<h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">通知訊息</h1>
	</div>
</div>
<div class="container">
	<div class="row">
    	<div class="col-sm-8">
            <div class="notification-container">
                <ul>
                    <?php $i=0;?>
                    <?php foreach($row as $key=>$value){?>
                        <li class="forum notification" style="list-style: none;">
                        	<div class="row">
                                <div class="col-xs-8">
                                    <div class="notif-meta">
                                        <p><?php if($row[$key]->notification_type==1){echo '討論區回應';}else if($row[$key]->notification_type==2){echo '新問題';}?></p>
                                        <h4>
                                            <a href="<?php if($row[$key]->notification_type==1|| $row[$key]->notification_type==2){echo base_url().'forum/forumContent/'.$row[$key]->hyperlink;}?>">
                                                <?php echo $row[$key]->notification_title;?>
                                            </a>
                                        </h4>
                                        <p>經由 <strong><?php echo $row[$key]->alias;?></strong> <?php if($row[$key]->notification_type==1){echo '回答';}else if($row[$key]->notification_type==2){echo '提問';}?></p>
                                        <p class="time"><?php echo $this->theme_model->calTimeElapsed($row[$key]->notification_time);?></p>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="avatar" style="background-image: url('<?=base_url();?>assets/img/user/128x128/<?php echo $row[$key]->img;?>')">
                                        <img src="<?=base_url();?>assets/img/user/128x128/<?php echo $row[$key]->img;?>">
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php $i++;?>
                    <?php }?>
                </ul>
                <!-- Pagination -->
                <div style="margin-top:30px;">
                    <ul class="pager">
                      <li class="previous <?php echo ($previous)?'':'disabled'?>"><a href="<?php echo ($previous)?'?page='.$previous:'javascript: void(0)'?>">&larr; 較新</a></li>
                      <li class="next <?php echo ($next)?'':'disabled'?>"><a href="<?php echo ($next)?'?page='.$next:'javascript: void(0)'?>">較舊 &rarr;</a></li>
                    </ul>                
                </div>
            </div> <!-- /notification-container -->
    	</div>
    </div>
</div>