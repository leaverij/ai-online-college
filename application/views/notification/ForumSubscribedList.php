<?php 
	$row=json_decode($getSubscriptionPagination);
?>
<div class="container">
	<div style="padding-bottom:10px; margin:30px 0 20px; border-bottom:1px solid #eeeeee;">
		<h2 style="font-size:1.71em;">討論區主題訂閱列表</h2>
	</div>
	<div class="row">
		<!-- CATEGORY FILTER -->
        <div class="col-sm-3 col-sm-push-9">
		</div>
        <!-- SUBSCRIBED LIST -->
        <style>
			.discussion-list {list-style:none; padding:0;}
			.discussion-list li {overflow:hidden; padding:15px 0; border-top:1px solid #ededed;}
			.discussion-list li:first-child {border-top:none;}
			.avatar {
				width:60px;
				height:60px;
				overflow:hidden;
				background-position:center;
				background-size:50px auto !important;
				border-radius:50px;
				border:5px solid #eee;}
			.avatar img {width:60px; height:60px; display:none;}
            .discussion-list .avatar {
				float:left;
				margin-left:-80px;}
			.discussion-meta {padding-left:80px;}
			.discussion-meta h4 {font-size:1.14em;}
			.discussion-meta p {color:#bcbcbc;}
			.discussion-meta p > strong {color:#999;}
			.discussion-list .reply-count {padding-top:10px; color:#999; float:right; text-align:center;}
			.discussion-list .reply-count strong {font-size:24px;}
        </style>
		<div class="col-sm-9 col-sm-pull-3">
			<?if(!empty($row)){?>
            <ul class="discussion-list">
                    <?foreach($row as $val){?>
            	<li>
                	<div class="col-xs-9">
                        <div class="discussion-meta">
                            <div class="avatar" style="background:url(<?=base_url();?>assets/img/user/128x128/<?php echo $val->img;?>)">
                                <img src="<?=base_url();?>assets/img/user/128x128/<?php echo $val->img;?>"/>
                            </div>
                            <h4>
                                <a href="<?php echo base_url().'forum/forumContent/'.$val->forum_topic_id;?>">
                                	<?php echo $val->forum_topic;?>
                                </a>
                            </h4>
                            <p>
                                <?php echo $val->posted_time;?> ‧ 經由 <strong><?php echo $val->alias;?></strong> 提問
                            </p>
                            <p><span class="label label-default"><?php echo $val->library_name;?></span></p>
                        </div>
                    </div>
                    <div class="col-xs-3">
                    	<div class="reply-count">
                        	<strong><?php echo $val->answers;?></strong>
                            <p>回 應</p>
                        </div>
                    </div>
                </li>
                <?}?>
              </ul>
            <!-- Pagination -->
            <div style="margin-top:30px;">
                <ul class="pager">
                  <li class="previous <?php echo ($previous)?'':'disabled'?>"><a href="<?php echo ($previous)?'?page='.$previous:'javascript: void(0)'?>">&larr; 較新</a></li>
                  <li class="next <?php echo ($next)?'':'disabled'?>"><a href="<?php echo ($next)?'?page='.$next:'javascript: void(0)'?>">較舊 &rarr;</a></li>
                </ul>                
            </div>
            <?}else{?>
        	<h4>討論區暫時沒有任何問題提問:)</h4>
        <?}?>
        </div>
	</div>
</div><!-- /container -->
</div>