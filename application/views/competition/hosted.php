<?php // print_r($list);exit;?>
<div class="container">
    <h3>我舉辦的競賽<a href="<?=base_url()?>competitions/create" class="btn btn-success pull-right"><i class="fa fa-plus" aria-hidden="true"></i> 建立新競賽</a></h3><hr>
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label>發布狀態</label>
                <select class="form-control" id="publish">
                    <option value="all" <?php if($selection['is_publish']==''){echo 'selected';}?>>全部</option>
                    <option value="0" <?php if($selection['is_publish']=='0'){echo 'selected';}?>>未發佈</option>
                    <option value="1" <?php if($selection['is_publish']=='1'){echo 'selected';}?>>已發佈</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>進行狀態</label>
                <select class="form-control" id="processing">
                    <option  value="all" <?php if($selection['processing']=='1'){echo 'selected';}?>>全部</option>
                    <option  value="0" <?php if($selection['processing']=='0'){echo 'selected';}?>>尚未開始</option>
                    <option  value="1" <?php if($selection['processing']=='1'){echo 'selected';}?>>進行中</option>
                    <option  value="2" <?php if($selection['processing']=='2'){echo 'selected';}?>>已結束</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>公開狀態</label>
                <select class="form-control" id="public">
                    <option value="all" <?php if($selection['is_public']=='1'){echo 'selected';}?>>全部</option>
                    <option value="1" <?php if($selection['is_public']=='1'){echo 'selected';}?>>Public</option>
                    <option value="0" <?php if($selection['is_public']=='0'){echo 'selected';}?>>Private</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>競賽類別</label>
                <select class="form-control" id="category">
                    <option value="all" <?php if($selection['competition_category_id']==''){echo 'selected';}?>>全部</option>
                <?php
                    $category_content = json_decode($category);
                    foreach($category_content as $category_val){
                        if($selection['competition_category_id']==$category_val->competition_category_id){
                ?>
                            <option value="<?php echo $category_val->competition_category_id ?>" selected><?php echo $category_val->name ?></option>
                <?php
                        }else{
                ?>
                            <option value="<?php echo $category_val->competition_category_id ?>"><?php echo $category_val->name ?></option>
                <?php           
                        }
                    }
                ?>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>名稱搜尋</label>
                <input id="competitionname" type="text" class="form-control" value="<?php echo $selection['name'];?>"/>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label></label>
                <button id="competitionsubmit" type="submit" class="btn btn-primary form-control">搜尋</button>
            </div>
        </div>
    </div>
    <div class="row">   
        <div class="col-md-12">
            <div class="table-responsive table-striped">
                <table class="table" id="hosted-table">
                    <thead>
                        <th>名稱</th>
                        <th>開始時間</th>
                        <th>結束時間</th>
                        <th>類別</th>
                        <th>發布狀態</th>
                        <th>公開狀態</th>
                    </thead>
                    <tbody>
                        <?php
                            $td_array = '';
                            foreach($list as $competition){
                                $temp = '<tr>';
                                    // 名稱
                                    $temp .= '<td><a href="'.base_url().'competition/edit/'.$competition['competition_id'].'/info">'.$competition['name'].'</a></td>';
                                    // 開始時間
                                    $temp .= '<td>'.$competition['start_time'].'</td>';
                                    // 結束時間
                                    $temp .= '<td>'.$competition['end_time'].'</td>';
                                    // 類別
                                    $temp .= '<td>'.$competition['category_name'].'</td>';
                                    // 發布狀態
                                    if($competition['is_publish']=='1')
                                        $temp .= '<td>發布中</td>';
                                    else
                                        $temp .= '<td>未發布</td>';
                                    // 公開狀態
                                    if($competition['is_public']=='1')
                                        $temp .= '<td>已公開</td>';
                                    else
                                        $temp .= '<td>未公開</td>';
                                $temp .= '</tr>';
                                $td_array .=$temp;
                            }
                            echo $td_array;
                        ?>
                        <!-- <tr>
                            <td><a href="<?=base_url()?>competition/edit/5/info">第一個競賽</a></td>
                            <td>2018/05/11</td>
                            <td>2018/05/14</td>
                            <td>旅遊</td>
                            <td>公開</td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/dataTables.bootstrap.js" type="text/javascript"></script>
<link href="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet">
<script>
    jQuery(document).ready(function($){
        $("#competitionsubmit").click(function () {  
            var publish = $("#publish").val();
            var processing = $("#processing").val();
            var public = $("#public").val();
            var category = $("#category").val();
            var competitionname = $("#competitionname").val();
            let url = new URL("<?=base_url();?>competitions/hosted");
            const params = {
                publish: publish,
                processing: processing,
                public: public,
                category: category,
                name: competitionname
            }
            Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
            window.location.href = url;
        });  

        $('#hosted-table').DataTable({
            "bFilter": false,
            "bLengthChange": false,
            "pageLength": 10
        });
    })
</script>