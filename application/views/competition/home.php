<style>
    .navbar-default{background-color:#c8e0f8;}
    #intro{background-image:url(./assets/img/competition-home-banner.png); background-size:cover; margin-top:-90px;}
    #competition-category{background:#f0f5f8;}
    .section-title {
        color: #666; 
        letter-spacing: 0.1em; 
        text-align: center;
        margin-bottom: 20px;
    }
    .category-card, .competition-card {
        background-color: #fff;
        box-shadow: 0 5px 20px rgba(0,0,0,.06);
        border-radius: 5px;
        padding: 15px;
        margin-bottom: 5px;
        text-align: center !important;
    }
    .card-body {
        padding: 10px;
    }
    .category-title {
        font-weight: bold;
        font-size: 18px;
        cursor: pointer;
        text-decoration: none !important;
        color: black;
    }
    .category-title:hover {
        color: #5ca0db;
    }
    .cause {
        position: relative;
        margin-bottom: 35px;
        padding: 20px;
    }
    .cause figure {
        position: relative;
        margin-bottom: 18px;
        background-color: #f0f0f0;
    }
    .cause figure img {
        display: block;
        height: auto;
        width: 100%;
    }
    .cause .cause-label {
        position: absolute;
        right: 10px;
        bottom: 10px;
        background-color: #fff;
        color: #000;
        font-size: 13px;
        padding: 4px 9px;
        z-index: 99;
        border-radius: 4px;
    }
    .cause .cause-title {
        font-size: 16px;
        margin-bottom: 13px;
        line-height: 1.25;
    }
    .cause .list-inline {
        font-size: 13px;
        font-weight: 400;
        color: #6e6e6e;
        margin-bottom: 12px;
    }
    .list-inline>li{display:inline-block;padding-right:5px;padding-left:5px}
    .cause .progress {
        height: 3px;
        padding: 0;
        margin-bottom: 18px;
        background-color: #d9d9d9;
    }
</style>
<?php
    $topic_content = json_decode($topic);
?>
<section id="intro">
    <div class="container">
        <div class="row">
        	<div class="col-md-12">
            	<div class="intro-content"></div>
            </div>
        </div>
    </div>
</section>
<section id="competition-category">
    <div class="container" style="padding: 30px 0px">
        <h3 class="section-title">擂臺主題</h3>
        <div class="row">
        <?php
            foreach($topic_content as $topic_val){
        ?>
            <div class="col-md-3">
                <div class="category-card">
                    <div class="card-body">
                        <p><img src="<?php echo $topic_val->topic_img?>" style="width: 60%" /></p>
                        <a href="<?=base_url();?>competitions/lists/<?php echo $topic_val->competition_topic_id ?>/10" class="category-title"><?php echo $topic_val->name?>館</a>
                    </div>
                </div>
            </div>       
        <?php
            }
        ?>
        </div>
    </div>
</section>
<section id="competition-summary">
    <div class="container" style="padding: 30px 0px">
        <h3 class="section-title">熱門競賽</h3>
        <div class="row">
            <div class="col-md-4">
                <article class="cause">
                    <figure>
                        <a><img src="./assets/img/eclat.jpg" /></a>
                        <span class="cause-label">傳產</span>
                    </figure>
                    <h2 class="cause-title"><a>紡織訂單資料剖析</a></h2>
                    <div class="cause-info">
                        <ul class="list-inline">
                            <li><strong><i class="fa fa-users" aria-hidden="true"></i></strong> 20</li>
                            <li><strong><i class="fa fa-h-square" aria-hidden="true"></i></strong> 儒鴻企業股份有限公司</li>
                        </ul>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-animate gradient2" role="progressbar" data-width="100" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                            <span class="sr-only">100% Complete</span>
                        </div>
                    </div>
                    <p>
                        <span class="pull-left">2018/02/01</span>
                        <span class="pull-right">2018/02/28</span>
                    </p>
                </article>
            </div>
            <div class="col-md-4">
                <article class="cause">
                    <figure>
                        <a><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSmgkbCNLPOCibrhr4SjP23PKKFMmLcbbP53m6ryPsHzJm3eHGD" /></a>
                        <span class="cause-label">交通</span>
                    </figure>
                    <h2 class="cause-title"><a>航班準時預測</a></h2>
                    <div class="cause-info">
                        <ul class="list-inline">
                            <li><strong><i class="fa fa-users" aria-hidden="true"></i></strong> 20</li>
                            <li><strong><i class="fa fa-h-square" aria-hidden="true"></i></strong> Sabre 先啟資訊</li>
                        </ul>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-animate gradient2" role="progressbar" data-width="100" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                            <span class="sr-only">100% Complete</span>
                        </div>
                    </div>
                    <p>
                        <span class="pull-left">2018/03/01</span>
                        <span class="pull-right">2018/03/31</span>
                    </p>
                </article>
            </div>
            <div class="col-md-4">
                <article class="cause">
                    <figure>
                        <a><img src="./assets/img/jct.jpg" /></a>
                        <span class="cause-label">醫療</span>
                    </figure>
                    <h2 class="cause-title"><a>醫療評鑑分級</a></h2>
                    <div class="cause-info">
                        <ul class="list-inline">
                            <li><strong><i class="fa fa-users" aria-hidden="true"></i></strong> 20</li>
                            <li><strong><i class="fa fa-h-square" aria-hidden="true"></i></strong> 醫策會</li>
                        </ul>
                    </div>
                    <div class="progress">
                        <div class="progress-bar progress-animate gradient2" role="progressbar" data-width="50" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
                            <span class="sr-only">50% Complete</span>
                        </div>
                    </div>
                    <p>
                        <span class="pull-left">2018/05/01</span>
                        <span class="pull-right">2018/05/15</span>
                    </p>
                </article>
            </div>
        </div>
    </div>
</section>