<style type="text/css">
.sort-content {/*padding:0 15px;*/ border-right:1px solid #ddd; text-align:right;}
#sidebar-collapse .navbar-collapse {max-height:100%;}
.form-group h4 {margin:0; padding:0 15px 5px 0; font-size:1.14em; color:#71bec6;}
.form-group h4 > a {color:#71bec6; text-decoration:none;}
.sort-content .nav a {padding:5px 15px; color:#666; position:relative;}
.sort-content .nav a:hover, .sort-content .nav a:focus {background:none; color:#2c3947; font-weight:bold;}
.sort-content .nav a:hover:before {content:''; width:2px; height:30px; background-color:#279ca7; position:absolute; right:-15px; top:0;}
.sort-content .nav li.active a {background:none; color:#2c3947; font-weight:bold;}
.sort-content .nav li.active a:before {content:''; width:2px; height:30px; background-color:#279ca7; position:absolute; right:-15px; top:0;}
.sidebar-toggle {display:none;}
/*
#sidebar-collapse ul li h4 {font-size:16px;}
#sidebar-collapse ul li h4 a:hover, #sidebar-collapse ul li.active a{text-decoration:none; background:#eeeeee; color:#24bbc9;}
#sidebar-collapse ul li a {color:#666; font-size:15px;}
#sidebar-collapse ul li a:hover {color:#24bbc9;}
#sidebar-collapse .library-list li a {color:#888;}
*/
@media screen and (max-width: 768px) {
    .sidebar-toggle-head {display:none;}
    .sidebar-toggle {display:block;}
}
</style>
<div id="sidebar-collapse" class="" style="padding:0;">
    <div class="sort-content">
        <div id="topics-sort" class="form-group">
            <h4 class="sidebar-toggle">
                <a data-toggle="collapse" data-parent="#topics-sort" href="#topics-sort-container">依主題分類</a>
            </h4>
            <h4 class="sidebar-toggle-head">依主題分類</h4>
            <ul id="topics-sort-container" class="nav navbar-collapse collapse">
            <?php 
                $topic_json = json_decode($competition_topic_result); 
                foreach($topic_json as $topic_val){
            ?>
                <li class="topic_url" >
                    <a href="<?=base_url()?>competitions/lists/<?php echo $topic_val->competition_topic_id;?>/10">
                    <?php echo $topic_val->name?> 
                    <span class="badge" style="margin-left:5px;"><?php echo $topic_val->counts?></span></a>
                    </a>
                </li>
            <?php
                }
            ?>
            </ul>
        </div>
    </div>
</div>