<style>
.center {
    text-align:center;
}
.modal {
    position:relative;
    display:block;
    top:auto;
    right:auto;
    bottom:auto;
    left:auto;
    z-index:99;
}
</style>
<div class="container">
    <div style="padding-bottom:20px;">
        <div class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form method="post" action="<?=base_url();?>api/competition/createCompetition">
                        <div class="modal-header">
                            <h4 class="modal-title center">建立新競賽</h4>
                        </div>
                        <div class="modal-body">
                            <div class="message">
                                <? if($this->session->flashdata('alert')):?>
                                    <? $data=$this->session->flashdata('alert'); ?>
                                    <div class="alert <?=$data['type']?> fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <?=$data['msg']?>
                                    </div>
                                <? endif; ?>
                            </div>
                            <div class="form-group">
                                <label>競賽名稱</label>
                                <input type="text" name="name" class="form-control" maxlength="32" required="required">
                            </div>
                            <div class="form-group">
                                <label>競賽副標</label>
                                <input type="text" name="subtitle" class="form-control" maxlength="256" required="required">
                            </div>
                            <div class="form-group">
                                <label>競賽主題</label>
                                <select name="topic" class="form-control" onchange="changeTopic(this)">
                                <?php
                                    $topic_content = json_decode($topic);
                                    foreach($topic_content as $topic_val){
                                ?>
                                    <option value="<?php echo $topic_val->competition_topic_id ?>"><?php echo $topic_val->name ?></option>
                                <?php
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>競賽類別</label>
                                <select id="categorySelector" name="category" class="form-control">
                                <?php
                                    $category_content = json_decode($category);
                                    foreach($category_content as $category_val){
                                ?>
                                    <option value="<?php echo $category_val->competition_category_id ?>"><?php echo $category_val->name ?></option>
                                <?php
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">儲存並開始</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/bootstrap-maxlength.js" type="text/javascript" charset="utf-8"></script>
<script>
$("input[maxlength]").maxlength();
function changeTopic(e){
    $('#categorySelector').empty();
    var ele = $(e);
    var topicId = ele.val();
    $.ajax({
        url : "<?=base_url();?>api/competition/getCompetitionCategoryByTopicId",
        type: "GET",
        data : "topic_id="+topicId,
        success:function(data, textStatus, jqXHR) {
            for(var i=0;i<data.length;i++){
                var categoryData = data[i];
                $('#categorySelector').append('<option value="'+categoryData.competition_category_id+'">'+categoryData.name+'</option>');
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //if fails      
        }
    });
}
</script>