<?php
    $competiton_data = json_decode($competition);
    $competition_overview = json_decode($overview);
?>
<style>
.competition-head {
    position: relative;
    border-radius: 5px 5px 0 0;
    background-color: #c8e0f8;
}
.competition-meta {
    padding: 30px;
}
.competition-bottom {
    background: #eee;
    padding: 10px 10px;
    border-radius: 0 0 5px 5px;
    box-shadow: 0 1px 0 0 #dedede;
    margin-bottom: 20px;
}
.competition-action-container {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    border: none;
}
.competition-action {
    float: left;
}
.competition-action a {
    display: block;
    text-align: center;
    padding: 10px;
    text-decoration: none;
    color: black;
    font-size: 20px;
}
.competition-action a:hover {
    color: #337ab7;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    background-color: #eee;
    border: none;
    border-bottom: 3px solid #23527c;
}
</style>
<div class="container">
    <div class="text-left">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().'competitions/lists/1/10'?>">所有競賽</a></li>
            <li><a href="<?php echo base_url().'competitions/lists/'?><?php echo $competiton_data->competition_topic_id ?>/10"><?php echo $competiton_data->topic_name;?>主題館</a></li>
            <li class="active"><?php echo $competiton_data->name;?></li>
        </ol>
    </div>
</div>
<div class="container">
    <div class="row">
    	<div class="col-sm-12">
            <div class="competition-head">
                <div class="competition-meta">
                    <h1><?php echo $competiton_data->name; ?></h1>
                    <p><?php echo $competiton_data->subtitle;?></p>
                </div>
            </div>
            <div class="competition-bottom">
                <ul class="competition-action-container nav nav-tabs" role="tablist">
                    <li class="competition-action active" role="presentation">
                        <a href="#overview_tab" role="tab" data-toggle="tab">Overview</a>
                    </li>
                    <li class="competition-action" role="presentation">
                        <a href="#data_tab" role="tab" data-toggle="tab">Data</a>
                    </li>
                    <li class="competition-action" role="presentation">
                        <a href="#leaderboard_tab" role="tab" data-toggle="tab">Leaderboard</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="overview_tab">
                        <?php
                            foreach($competition_overview as $overview_detail){
                        ?>
                            <h3><?php echo $overview_detail->title; ?></h3>
                            <p><?php echo $overview_detail->content; ?></p>
                            <hr>
                        <?
                            }
                        ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="data_tab">Coming Soon</div>
                        <div role="tabpanel" class="tab-pane" id="leaderboard_tab">Coming Soon</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>