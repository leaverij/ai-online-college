<style>
.bg-gradient{
  background: #c0dffe;
}
ul#detail-list li {
    cursor: pointer;
}
ul#detail-list li:hover {
    background: #c0dffe;
}
</style>
<?php
    $competition_content = json_decode($competition);
    $competition_details = json_decode($details);
?>
<section class="bg-gradient text-white space-lg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3 id="competition_name_show"><?php echo $competition_content->name ?></h3>
            </div>
        </div>
    </div>
</section>
<div class="container" style="padding-top:30px; margin-bottom: 20px;">
    <div class="row">
        <div class="col-md-3"><?php $this->load->view('competition/competition_sidebar'); ?> </div>
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li><a href="<?=base_url()?>competitions/hosted">所有競賽</a></li>
                <li><?php echo $competition_content->name ?></li>
            </ol>
            <h4>競賽細節</h4><hr>
            <div class="row">
                <div class="col-md-3">
                    <ul id="detail-list" class="list-group">
                    <?php
                        foreach($competition_details as $competition_detail_val){
                    ?>
                        <li 
                            class="list-group-item" 
                            onclick="showCompetitionDetail(this)"
                            data-title="<?php echo $competition_detail_val->title ?>"
                            data-competition-detail-id="<?php echo $competition_detail_val->competition_detail_id ?>"
                            data-publish="<?php echo $competition_detail_val->is_publish ?>">
                            <h5 id="detail-title-show"><?php echo $competition_detail_val->title ?></h5>
                            <div id="edit_detail_block"></div>
                        </li>
                    <?php
                        }
                    ?>
                    </ul>
                    <a class="btn btn-primary form-control" id="newBtn" data-title="" data-competition-id="" data-content="" data-publish="0"><i class="fa fa-plus" aria-hidden="true"></i> 增加細節</a>
                </div>
                <div class="col-md-9">
                    <form id="detail_form">
                        <div class="form-group">
                            <label for="detail-title">細節標題</label>
                            <input type="text" class="form-control" value="<?php echo $competition_details[0]->title ?>" id="detail-title" name="detail_title" maxlength="32" />
                        </div>
                        <div class="form-group">
                            <label for="detail_description">細節說明</label>
                            <div id="detail_description"><?php echo $competition_details[0]->content ?></div>
                        </div>
                        <div class="form-group">
                            <label for="detail_publish">發布狀態</label>
                            <select id="detail_publish" class="form-control">
                                <option value="0" <?php echo ($competition_details[0]->is_publish=='0')?'selected':'' ?>>未發布</option>
                                <option value="1" <?php echo ($competition_details[0]->is_publish=='1')?'selected':'' ?>>已發布</option>
                            </select>
                        </div>
                        <input type="hidden" id="competition_detail_id" name="competition_detail_id" value="<?php echo $competition_details[0]->competition_detail_id ?>" />
                        <div style="text-align:center;">
                            <input type="button" id="saveBtn" class="btn btn-success form-control" value="儲存">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/ckeditor/ckeditor.js" type="text/javascript" charset="utf-8"></script>
<script>
$(document).ready(function(){
    CKEDITOR.replace('detail_description');
    $('#saveBtn').on('click', function(){
        var detailTitle = $('#detail-title').val();
        if(detailTitle && detailTitle.trim().length!=0){
            var detailPublish = $('#detail_publish').val();
            var detailContent = CKEDITOR.instances['detail_description'].getData();
            var detailId = $('#competition_detail_id').val();
            $.ajax({
                type: "POST",
                url: "<?=base_url();?>api/competition/updateCompetitionDetail",
                data: "title="+detailTitle+"&content="+encodeURIComponent(detailContent)+"&is_publish="+detailPublish+"&competition_detail_id="+detailId+"&competition_id=<?php echo $competition_content->competition_id ?>",
                success: function(returnData, textStatus, jqXHR){
                    if(returnData["success"]){
                        alert('儲存成功!');
                        if(detailId){
                            var targetDetailEle = $('ul#detail-list li[data-competition-detail-id="'+detailId+'"]');
                            targetDetailEle.find('#detail-title-show').html(detailTitle);
                            targetDetailEle.attr('data-title', detailTitle);
                            targetDetailEle.attr('data-publish', detailPublish);
                        }else{
                            var lastDetailEle = $('ul#detail-list li').last();
                            lastDetailEle.html(detailTitle);
                            lastDetailEle.find('#detail-title-show').html(detailTitle);
                            lastDetailEle.attr('data-competition-detail-id', returnData['id']);
                            lastDetailEle.attr('data-title', detailTitle);
                            lastDetailEle.attr('data-publish', detailPublish);
                        }
                    }
                },
                error: function(){
                    alert("failure");
                }
            });
        }else{
            alert('細節標題為必填!');
            return;
        }
    });
    $('#newBtn').on('click', function(){
        if($('ul#detail-list li').last().attr('data-competition-detail-id')){
            $('#detail-title').val('Untitled');
            $('#competition_detail_id').val('');
            CKEDITOR.instances['detail_description'].setData('');
            $('#detail-list').append('<li class="list-group-item" data-title="Untitled" data-content="" data-competition-detail-id="" data-publish="" onclick="showCompetitionDetail(this)">Untitled</li>');
            $("#detail_publish").val('0');
        }
    });
});
function showCompetitionDetail(e){
    var targetDetail = $(e);
    $('#detail-title').val(targetDetail.attr('data-title'));
    $('#competition_detail_id').val(targetDetail.attr('data-competition-detail-id'));
    $.ajax({
        type: "GET",
        url: "<?=base_url();?>api/competition/getOneCompetitionDetail",
        data: "competition_detail_id="+targetDetail.attr('data-competition-detail-id')+"&competition_id=<?php echo $competition_content->competition_id ?>",
        success: function(returnData, textStatus, jqXHR){
            CKEDITOR.instances['detail_description'].setData(returnData['content']);
        },
        error: function(){

        }
    });
    $('#detail_publish').val(targetDetail.attr('data-publish'));
}
</script>