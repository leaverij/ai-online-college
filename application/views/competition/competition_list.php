<div class="container" style="padding-top:30px;">
    <div class="row">
        <div class="col-sm-3">
            <?php $this->load->view('competition/list_sidebar'); ?> 
        </div>
        <div class="col-sm-9">
            <div class="row">
                <div class="col-md-12">
                    <form class="search-bar" id="search_form" action="<?php echo $_SERVER['PHP_SELF'];?>" method="GET" role="search" style="margin-bottom:10px;">
	                    <input type="hidden" name="action" value="search"/>
	                    <div class="input-group input-group-lg">
		                    <input  type="text" class="form-control" name="search_string" id="search_string" value="" placeholder="今天想要找什麼競賽呢？"/>
		                    <div class="input-group-btn">
			                    <button type="submit" name="submit" class="btn btn-default search-btn" tabindex="-1">
				                    <span class="glyphicon glyphicon-search" style="color:#999;"></span>
			                    </button>
		                    </div>
	                    </div>
                    </form>
                </div>
            </div>          
            <div class="row">
                <div class="col-md-12"> 
                        <p style="font-weight: bold; font-size: 20px;">
                            <?php 
                                $current_topc_val = json_decode($current_topic);
                                echo $current_topc_val[0]->name;
                            ?>館
                        </p>
                    </div>
                <?php 
                    $competitions = json_decode($competition);            
                    $first = true;
                    foreach($competitions as $val){?>
                    
                    <div class="col-md-12" style="margin-bottom: 20px;"> 
                        <?php 
                            $competition_name = $val -> name; 
                            $url = preg_replace("/[\s-]+/", "_", $competition_name);
                        ?>
                        <a href="<?=base_url()?>competitions/details/<?php echo $val -> competition_id; ?>">
                        <div style="display: inline-block; float: left;">
                            <img style="height: 100px; width: 100px;" src="https://thumbs.dreamstime.com/b/winner-cup-flat-symbol-trophy-icon-109391028.jpg">
                        </div>
                        </a>
                        <div style="display: inline-block; padding-left: 10px; padding-top: 7px;">
                            <a href="<?=base_url()?>competitions/details/<?php echo $val -> competition_id; ?>">
                            <p style="font-weight: bold; font-size: 16px; color: #333"><?php echo $val -> name; ?></p>
                        </a>
                            <p style="color: #333"><?php echo $val -> subtitle; ?></p>
                            <p style="display: inline; color: gray;"><?php echo $val -> topic_name; ?> ·</p> 
                            <p style="display: inline; color: gray;"><?php echo $val -> category_name; ?> ·</p>
                            <p style="display: inline; color: gray;"><?php echo $val -> start_time; ?> </p>
                        </div>
                        <div style="display: inline-block; float: right;">
                            <p style="color: #333"></p>
                        </div>

                        
                    </div>
                <?
                    }
                    if(count($competitions)==0){
                ?>
                    <div class="col-md-12" style="margin-bottom: 20px;">
                        <p>We are ready to launch!</p>
                    </div>
                <?
                    }
                ?>
                      

            </div>
            <div class="row">
                <?php 
                    if(isset($val)){  
                        if (isset($_GET['pageno'])) {
                            $pageno = $_GET['pageno'];
                        } else {
                            $pageno = 1;
                        }
                

                        if (isset($_GET['submit'])||isset($_GET['search_string'])){
                            $searchtext= $_GET['search_string'];
                        }
                            $topic_id = ($val -> competition_topic_id)-1;
                            $offset = ($pageno-1) * $per_page; 
                            $total_rows = $competition_topic_total;
                            $total_pages = ceil($total_rows / $per_page);
                        
                        ?>

                <!-- <div class="col-md-12"> -->
                    <div class="col-md-6">
                        <p style="margin-top: 25px;">目前在第<? echo $pageno; ?>頁，共<? echo $total_pages; ?>頁</p>
                    </div>
                    <div class="col-md-6" style="padding-right: 0px;">
                        <ul class="pagination" style="float: right;">
                            <li>
                                <a href=
                                "<?php if (isset($searchtext)){?> 
                                <?=base_url()?><?php echo"competitions/lists/";echo $val -> competition_topic_id."/10?search_string=".$searchtext;?>
                                <?} else{?>
                                <?=base_url()?><?php echo"competitions/lists/";echo $val -> competition_topic_id."/".$per_page;?>
                                <?}?>">First</a>
                            </li>

                            <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
                                <a href="
                                <?php if($pageno <= 1){ echo '#'; } else {?> 
                                    <? base_url()?>
                                    <?php if (isset($searchtext)){?> 
                                        <?echo ($pageno-1)*$per_page;echo "?pageno=".($pageno - 1)."&search_string=".$searchtext;
                                    } else{?>
                                        <?echo ($pageno-1)*$per_page;echo "?pageno=".($pageno - 1) ?>
                                    <?}?>
                                <?}?> " > Prev</a>

                            </li>
                            <li>
                                <a><? echo $pageno; ?></a>
                            </li>
                            <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
                                <a href="
                                <?php if($pageno >= $total_pages){ echo '#'; } else {?> 
                                    <? base_url()?>
                                    <?php if (isset($searchtext)){?> 
                                        <?echo ($pageno+1)*$per_page;echo "?pageno=".($pageno + 1)."&search_string=".$searchtext;
                                    } else{?>
                                        <? echo ($pageno+1)*$per_page;echo "?pageno=".($pageno + 1) ?>
                                    <?}?>
                                <?}?> " > Next</a>

                            </li>
                            <li>
                                <a href=
                                "<?php if (isset($searchtext)){?> 
                                <?=base_url()?><?php echo"competitions/lists/";echo $val -> competition_topic_id;?>/<?echo ($total_pages)*$per_page;?>?pageno=<?php echo $total_pages; ?>&search_string=<?echo $searchtext; ?>
                                <?} else{?>
                                <?=base_url()?>competitions/lists/<?php echo $val -> competition_topic_id; ?>/<?php echo ($total_pages)*$per_page; ?>?pageno=<?php echo $total_pages; ?>
                                <?}?>">Last</a>
                            </li>
                        </ul> 
                    </div>

                    <?}?>
                <!-- </div> -->
            </div>
        </div>
    </div>
</div>
