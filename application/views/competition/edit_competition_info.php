<style>
.bg-gradient{
  background: #c0dffe;
}
</style>
<?php
    $competition_content = json_decode($competition);
    $category_content = json_decode($category);
    $topic_content = json_decode($topic);
?>
<section class="bg-gradient text-white space-lg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h3 id="competition_name_show"><?php echo $competition_content->name ?></h3>
            </div>
        </div>
    </div>
</section>
<div class="container" style="padding-top:30px; margin-bottom: 20px;">
    <div style="display:none;" class="alert alert-success" id="success-alert">
    	<strong>更新成功</strong>
    </div>
    <div style="display:none;" class="alert alert-danger" id="danger-alert">
        <strong>競賽名稱與副標題皆為必填</strong>
    </div>
    <div class="row">
        <div class="col-md-3"><?php $this->load->view('competition/competition_sidebar'); ?> </div>
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li><a href="<?=base_url()?>competitions/hosted">所有競賽</a></li>
                <li><?php echo $competition_content->name ?></li>
            </ol>
            <h4>基本資訊</h4><hr>
            <form id="competition_info_form" method="post" action="<?=base_url();?>api/competition/editCompetitionInfo">
                <div class="form-group">
                    <label for="competition_name">競賽名稱</label>
                    <input type="text" id="competition_name" name="competition_name" maxlength="32" class="form-control" value="<?php echo $competition_content->name ?>" required="required">
                </div>
                <div class="form-group">
                    <label for="competition_subtitle">競賽副標</label>
                    <input type="text" id="competition_subtitle" name="competition_subtitle" maxlength="256" class="form-control" value="<?php echo $competition_content->subtitle ?>" required="required">
                </div>
                <div class="form-group">
                    <label for="competition_topic">所屬主題</label>
                    <select id="competition_topic" name="competition_topic" class="form-control" onchange="changeTopic(this)">
                    <?php
                        foreach($topic_content as $topic_val){
                    ?>
                        <option value="<?php echo $topic_val->competition_topic_id ?>" <?php echo ($topic_val->competition_topic_id==$competition_content->competition_topic_id)?'selected':'' ?>><?php echo $topic_val->name ?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="competition_category">所屬類別</label>
                    <select id="competition_category" name="competition_category" class="form-control">
                    <?php
                        foreach($category_content as $category_val){
                    ?>
                        <option value="<?php echo $category_val->competition_category_id ?>"><?php echo $category_val->name ?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="competition_starttime">競賽開始日期</label>
                    <div class="input-group" id="competition_starttime">
                        <input name="competition_starttime" type="text" class="form-control" value="" />
                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="competition_endtime">競賽結束日期</label>
                    <div class="input-group" id="competition_endtime">
                        <input name="competition_endtime" type="text" class="form-control" value="" />
                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="competition_deadline">競賽成果繳交期限</label>
                    <div class="input-group" id="competition_deadline">
                        <input name="competition_deadline" type="text" class="form-control" value="<?php echo $competition_content->deadline ?>" />
                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="competition_publish">發布狀態 <span style="color: #ce4844">#選擇發布即表示該課程會出現在學生端介面</span></label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="competition_publish" id="competition_publish1" value="0" <?php echo $competition_content->is_public==0?"checked":"" ?> >未發布
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="competition_publish" id="competition_publish2" value="1" <?php echo $competition_content->is_public==1?"checked":"" ?> >發布
                        </label>
                    </div>
                </div>
                <div style="text-align:center;">
                    <input name="competition_id" type="hidden" value="<?php echo $competition_content->competition_id ?>">
                    <input type="submit" id="saveBtn" class="btn btn-success form-control" value="儲存">
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/bootstrap-maxlength.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/moment.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript" charset="utf-8"></script>
<script>
var competition_start_time = '<?php echo $competition_content->start_time ?>';
var competition_end_time = '<?php echo $competition_content->end_time ?>';
var competition_deadline = '<?php echo $competition_content->deadline ?>';
$("input[maxlength]").maxlength();
$('#competition_info_form').submit(function(e){
    var postData = $('#competition_info_form').serializeArray();
    var formURL = $('#competition_info_form').attr("action");
    $.ajax({
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) {
            if(data["result"]=="success"){
                $('#competition_name_show').html($('#competition_name').val());
                $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#success-alert").slideUp(500);
                });
            }else{
                $("#danger-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#danger-alert").slideUp(500);
                });
            }
            
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //if fails      
        }
    });
    e.preventDefault(); //STOP default action
});
// 所屬領域 類別的預設值
$("#competition_category").val(<?php echo $competition_content->competition_category_id ?>);
$('#competition_starttime').datetimepicker({
    format:'YYYY-MM-DD HH:mm:ss',
    defaultDate:(competition_start_time)?competition_start_time:new Date(),
    showClose:true
});
$('#competition_endtime').datetimepicker({
    format:'YYYY-MM-DD HH:mm:ss',
    defaultDate:(competition_end_time)?competition_end_time:new Date(),
    showClose:true
});
$('#competition_deadline').datetimepicker({
    format:'YYYY-MM-DD HH:mm:ss',
    defaultDate:(competition_deadline)?competition_deadline:new Date(),
    showClose:true
});
function changeTopic(e){
    $('#competition_category').empty();
    var ele = $(e);
    var topicId = ele.val();
    $.ajax({
        url : "<?=base_url();?>api/competition/getCompetitionCategoryByTopicId",
        type: "GET",
        data : "topic_id="+topicId,
        success:function(data, textStatus, jqXHR) {
            for(var i=0;i<data.length;i++){
                var categoryData = data[i];
                $('#competition_category').append('<option value="'+categoryData.competition_category_id+'">'+categoryData.name+'</option>');
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //if fails      
        }
    });
}
</script>