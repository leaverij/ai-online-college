<!-- Modal -->
<div class="modal fade" id="webCamModal" tabindex="-1" role="dialog" aria-labelledby="webCamModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="webCamModalLabel">因為課程需求，請按確認讓我們留下您的相片</h4>
      </div>
      <div class="modal-body">
        <script type="text/javascript" src="<?=base_url();?>assets/js/webcam/webcam.js"></script>
		<script language="JavaScript">
		webcam.set_api_url( '<?=base_url();?>release/record');
		webcam.set_quality( 100 ); // 相片品質(1 - 100)
		webcam.set_shutter_sound( false ); // 播放拍照聲音
		//document.write( webcam.get_html(400, 300) );
		webcam.set_hook( 'onComplete', function(){ 
			$('#webCamModal').modal('hide');
		} );	
		function take_snapshot() {
			webcam.snap();
		}
		function init_web_cam(){
			document.getElementById('webcam').innerHTML = webcam.get_html(400, 300);
		}
	</script>
      </div>
      <center><div id="webcam" class=""></div></center>
      <div class="modal-footer">
      	<button type="button" class="btn btn-info" onClick="webcam.configure('privacy')">Flash設定</button>
		<button type="button" class="btn btn-danger" onClick="take_snapshot()">拍照</button>
        <button type="button" class="btn btn-primary" onClick="init_web_cam()">啟動相機</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" charset="utf-8">
	document.getElementById('webcam').innerHTML = webcam.get_html(400, 300);
</script>