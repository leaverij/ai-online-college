<?php
    $course_content = json_decode($course);
    $announcement_content = json_decode($announcements);
?>
<style>
#announcement_block {
    margin:10px 0px;
}
</style>
<div class="container" style="padding-top:30px; margin-bottom: 20px;">
    <div style="display:none;" class="alert alert-success" id="success-alert">
    	<strong>更新成功</strong>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view('course/course_sidebar'); ?> 
        </div> 
        <div class="col-md-9">
            <h3><?php echo $course_content->course_name ?></h3>
            <h4>編輯課程公告</h4>
            <form id="announcement_form" method="post" action="<?=base_url();?>api/course/updateCourseAnnouncement">
                <a id="addNewAnnouncementBtn" class="btn btn-success form-control">建立新公告</a>
                <div id="announcement_block" style="display:none">
                    <div id="announcement_body_block"></div>
                    <div style="text-align:center;">
                        <input name="course_id" type="hidden" value="<?php echo $course_content->course_id ?>">
                        <button id="saveBtn" data-id="" class="btn btn-success form-control" type="button">儲存</button>
                    </div>
                </div>
            </form>
            <hr>
            <h4>已發布的公告</h4>
            <table id="announcement_table" class="table table-striped">
                <thead>
                    <tr>
                        <th>標題</th>
                        <th>建立時間</th>
                        <th>公告時間</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                foreach($announcement_content as $announcement){
                ?>
                    <tr data-id="<?php echo $announcement->id ?>">
                        <td><?php echo $announcement->title ?></td>
                        <td><?php echo $announcement->create_date ?></td>
                        <td><?php echo $announcement->announce_date ?></td>
                        <td>
                            <button data-id="<?php echo $announcement->id ?>" data-title="<?php echo $announcement->title ?>" data-content="<?php echo $announcement->content ?>" type="button" class="btn btn-primary btn-sm showBtn" onclick="showAnnouncement(this)">編輯</button>
                            <button data-toggle="modal" data-target="#removeAnnouncementModal" data-id="<?php echo $announcement->id ?>" type="button" class="btn btn-danger btn-sm removeBtn">刪除</button>
                        </td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- 公告 Template -->
<script id="announcement-template" type="text/x-handlebars-template">
    <div class="form-group">
        <label>公告標題</label>
        <input id="announcement_title" type="text" value="{{title}}" class="form-control" maxlength="200">
    </div>
    <div class="form-group">
        <label>公告內容</label>
        <div id="announcement_content" class="form-control"></div>
    </div>
</script>

<!-- 移除公告確認Modal -->
<div id="removeAnnouncementModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h4>確定要刪除嗎?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button id="removeAnnouncementBtn" data-id="" type="button" class="btn btn-primary">確定</button>
            </div>
        </div>
    </div>
</div>

<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/bootstrap-maxlength.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/ckeditor/ckeditor.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/handlebars-v4.0.11.js" type="text/javascript" charset="utf-8"></script>
<script>
var announcementTemplate;
$("input[maxlength]").maxlength();
$('#saveBtn').on('click', function(){
    var title = $('#announcement_title').val();
    if(!title){
        alert('請輸入公告標題');
        return;
    }
    var content = CKEDITOR.instances['announcement_content'].getData();
    var announcement_id = $('#saveBtn').attr('data-id');
    var postData = {
        course_id:"<?php echo $course_content->course_id ?>",
        title:title,
        content:content,
        announcement_id:announcement_id
    };
    var formURL = $('#announcement_form').attr("action");
    $.ajax({
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) {
            if(!announcement_id){
                $('#announcement_table tbody').append(
                '<tr data-id="'+data.id+'">'+
                    '<td>'+title+'</td>'+
                    '<td>'+data.create_date+'</td>'+
                    '<td>'+data.announce_date+'</td>'+
                    '<td>'+
                        '<button data-id="'+data.id+'" data-title="'+title+'" data-content="'+content+'" type="button" class="btn btn-primary btn-sm showBtn" onclick="showAnnouncement(this)">編輯</button>'+
                        '<button type="button" data-toggle="modal" data-target="#removeAnnouncementModal" data-id="'+data.id+'" class="btn btn-danger btn-sm removeBtn">刪除</button>'+
                    '</td>'+
                '</tr>'
                );
            }else{
                var targetRow = $('#announcement_table tbody tr[data-id="'+announcement_id+'"]');
                targetRow.find('td:eq(0)').html(title);
                var targetBtn = targetRow.find('td:eq(4) .showBtn');
                targetBtn.attr('data-title', title);
                targetBtn.attr('data-content', content);
            }
            $('#announcement_block').hide();
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#success-alert").slideUp(500);
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //if fails      
        }
    });
});
$(document).ready(function(){
    announcementTemplate = Handlebars.compile($('#announcement-template').html());
    $('#addNewAnnouncementBtn').on('click', function(){
        $("#announcement_body_block").empty().append(announcementTemplate({
            id:'',
            title:''
        }));
        CKEDITOR.replace('announcement_content');
        $('#saveBtn').attr('data-id','');
        $('#announcement_block').show();
    });
    $('#removeAnnouncementModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var announcement_id = button.attr('data-id');
        var modal = $(this);
        modal.find('#removeAnnouncementBtn').attr('data-id', announcement_id);
    });
    $('#removeAnnouncementBtn').on('click', function(){
        var announcement_id= $(this).attr('data-id');
        $.ajax({
            url : '<?=base_url();?>api/course/removeAnnouncement',
            type: "POST",
            data : 'announcement_id='+announcement_id,
            success:function(data, textStatus, jqXHR) {
            $('#removeAnnouncementModal').modal('hide');
            $('#announcement_table tbody tr[data-id="'+announcement_id+'"]').remove();
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#success-alert").slideUp(500);
            });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //if fails      
            }
        });
    });
});
function showAnnouncement(e){
    var showBtn = $(e);
    var id = showBtn.attr('data-id');
    var title = showBtn.attr('data-title');
    var content = showBtn.attr('data-content');
    $("#announcement_body_block").empty().append(announcementTemplate({
        id:id,
        title:title
    }));
    CKEDITOR.replace('announcement_content');
    CKEDITOR.instances['announcement_content'].setData(content);
    $('#saveBtn').attr('data-id', id);
    $('#announcement_block').show();
}
</script>