<?php
    $course_content = json_decode($course);
    $meeting_content = json_decode($meetings);
?>
<link href="<?=base_url();?>assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<style>
#meeting_block {
    margin:10px 0px;
}
</style>
<div class="container" style="padding-top:30px; margin-bottom: 20px;">
    <div style="display:none;" class="alert alert-success" id="success-alert">
    	<strong>更新成功</strong>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view('course/course_sidebar'); ?> 
        </div> 
        <div class="col-md-9">
            <h3><?php echo $course_content->course_name ?></h3>
            <h4>編輯課程直播教學</h4>
            <form id="meeting_form" method="post">
                <a id="addNewMeetingBtn" class="btn btn-success form-control">建立新直播</a>
                <div id="meeting_block" style="display:none">
                    <div id="meeting_body_block"></div>
                    <div style="text-align:center;">
                        <input type="hidden" id="meeting_type" name="meeting_type" value="2">
                        <input name="course_id" type="hidden" value="<?php echo $course_content->course_id ?>">
                        <button id="saveBtn" data-id="" class="btn btn-success form-control" type="button">儲存</button>
                    </div>
                </div>
            </form>
            <hr>
            <h4>已建立的直播</h4>
            <table id="meeting_table" class="table table-striped">
                <thead>
                    <tr>
                        <th>主題</th>
                        <th>建立時間</th>
                        <th>會議時間</th>
                        <th>狀態</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                foreach($meeting_content as $meeting){
                ?>
                    <tr data-id="<?php echo $meeting->id ?>">
                        <td><?php echo $meeting->topic ?></td>
                        <td><?php echo $meeting->create_date ?></td>
                        <td><?php echo $meeting->start_time ?></td>
                        <td>
                        <?php
                        if($meeting->status==0){
                        ?>
                            <span class="label label-success">未開始</span>
                        <?php
                        }else{
                        ?>
                            <span class="label label-default">已結束</span>
                        <?php
                        }
                        ?>
                        </td>
                        <td>
                            <a target="_blank" class="btn btn-info btn-sm" href="<?php echo $meeting->start_url ?>">主持</a>
                        <?php
                        if($meeting->status==0){
                        ?>
                            <button data-start_url="<?php echo $meeting->start_url ?>" data-roomid="<?php echo $meeting->meeting_id ?>" data-id="<?php echo $meeting->id ?>" data-topic="<?php echo $meeting->topic ?>" data-description="<?php echo $meeting->description ?>" data-password="<?php echo $meeting->password ?>" data-date="<?php echo $meeting->start_time ?>" data-status="<?php echo $meeting->status ?>" type="button" class="btn btn-primary btn-sm showBtn" onclick="showMeeting(this)">編輯</button>
                        <?php
                        }else{
                        ?>
                            <button data-start_url="<?php echo $meeting->start_url ?>" data-roomid="<?php echo $meeting->meeting_id ?>" disabled data-id="<?php echo $meeting->id ?>" data-topic="<?php echo $meeting->topic ?>" data-description="<?php echo $meeting->description ?>" data-password="<?php echo $meeting->password ?>" data-date="<?php echo $meeting->start_time ?>" data-status="<?php echo $meeting->status ?>" type="button" class="btn btn-primary btn-sm showBtn" onclick="showMeeting(this)">編輯</button>
                        <?php
                        }
                        ?>
                            <button data-roomid="<?php echo $meeting->meeting_id ?>" data-toggle="modal" data-target="#removeMeetingModal" data-id="<?php echo $meeting->id ?>" type="button" class="btn btn-danger btn-sm removeBtn">刪除</button>
                        </td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- 直播 Template -->
<script id="meeting-template" type="text/x-handlebars-template">
    <div class="form-group">
        <label>直播主題</label>
        <input id="meeting_topic" type="text" value="{{topic}}" class="form-control" maxlength="200">
    </div>
    <div class="form-group">
        <label>直播說明</label>
        <textarea id="meeting_description" class="form-control" value="{{description}}">{{description}}</textarea>
    </div>
    <div class="form-group">
        <label>直播密碼</label>
        <input id="meeting_password" type="text" value="{{password}}" class="form-control">
    </div>
    <div class="form-group">
        <label>直播日期</label>
        <div class="input-group date" id="meeting_date">
            <input type="text" class="form-control" />
            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
        </div>
    </div>
    <div class="form-group">
        <label>直播時數(分)</label>
        <select id="meeting_duration" class="form-control">
            <option value="30" selected>30</option>
            <option value="35">35</option>
            <option value="40">40</option>
            <option value="45">45</option>
            <option value="50">50</option>
        </select>
    </div>
    <div class="form-group">
        <label>主持會議連結</label>
        <input type="text" class="form-control" value="{{start_url}}" disabled>
    </div>
    <input type="hidden" id="meeting_room_id" name="meeting_room_id" value="{{meeting_room_id}}">
</script>

<!-- 移除直播確認Modal -->
<div id="removeMeetingModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h4>確定要刪除嗎?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button id="removeMeetingBtn" data-roomid="" data-id="" type="button" class="btn btn-primary">確定</button>
            </div>
        </div>
    </div>
</div>

<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/bootstrap-maxlength.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/ckeditor/ckeditor.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/handlebars-v4.0.11.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/moment.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript" charset="utf-8"></script>
<script>
var meetingTemplate;
$(document).ready(function(){
    meetingTemplate = Handlebars.compile($('#meeting-template').html());
    $('#addNewMeetingBtn').on('click', function(){
        $("#meeting_body_block").empty().append(meetingTemplate({
            id:'',
            topic:'',
            password:'',
            description:'',
            meeting_room_id:'',
            start_url:''
        }));
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        $('#meeting_date').datetimepicker({
            format:'YYYY-MM-DD HH:mm:ss',
            minDate:new Date(),
            defaultDate:tomorrow,
            showClose:true
        });
        $('#saveBtn').attr('data-id','');
        $('#saveBtn').attr('data-status','0');
        $('#meeting_block').show();
    });
    $('#saveBtn').on('click', function(){
        var topic = $('#meeting_topic').val();
        if(!topic){
            alert('請輸入會議主題');
            return;
        }
        var meeting_id = $('#saveBtn').attr('data-id');
        var status = $('#saveBtn').attr('data-status');
        var md = $('#meeting_date').data("DateTimePicker").getDate();
        var meeting_date = moment(md).format('YYYY-MM-DD hh:mm:ss');//$('#meeting_date').data("DateTimePicker").getDate().format('YYYY-MM-DD HH:mm:ss');
        var description = $('#meeting_description').val();
        var password = $('#meeting_password').val();
        var postData = {
            meeting_id:meeting_id,
            course_id:"<?php echo $course_content->course_id ?>",
            topic:topic,
            description:description,
            date:meeting_date,
            password:password,
            type:$('#meeting_type').val(),
            duration:$('#meeting_duration').val(),
            meeting_room_id:$('#meeting_room_id').val()
        };
        var formURL = '<?=base_url();?>zoom/meeting_create';
        if(meeting_id){
            formURL = '<?=base_url();?>zoom/meeting_update';
        }
        $("#saveBtn").attr("disabled",true).text("處理中...");
        $.ajax({
            url : formURL,
            type: "POST",
            data : postData,
            success:function(data, textStatus, jqXHR) {
                $("#saveBtn").attr("disabled", false).text("儲存");
                if(!meeting_id){ //新增直播
                    $('#meeting_table tbody').append(
                        '<tr data-id="'+data.id+'">'+
                            '<td>'+topic+'</td>'+
                            '<td>'+data.create_date+'</td>'+
                            '<td>'+data.meeting_date+'</td>'+
                            '<td><span class="label label-success">未開始</span></td>'+
                            '<td>'+
                                '<a target="_blank" class="btn btn-info btn-sm" href="'+data.start_url+'">主持</a>'+
                                '<button data-starturl="'+data.start_url+'" data-roomid="'+data.meeting_room_id+'" data-id="'+data.id+'" data-topic="'+topic+'" data-description="'+description+'" data-password="'+password+'" data-date="'+meeting_date+'" data-status="'+status+'" type="button" class="btn btn-primary btn-sm showBtn" onclick="showMeeting(this)">編輯</button>'+
                                '<button type="button" data-toggle="modal" data-target="#removeMeetingModal" data-id="'+data.id+'" data-roomid="'+data.meeting_room_id+'" class="btn btn-danger btn-sm removeBtn">刪除</button>'+
                            '</td>'+
                        '</tr>'
                    );
                }else{
                    var targetRow = $('#meeting_table tbody tr[data-id="'+meeting_id+'"]');
                    targetRow.find('td:eq(0)').html(topic);
                    targetRow.find('td:eq(2)').html(meeting_date);
                    var targetBtn = targetRow.find('td:eq(4) .showBtn');
                    targetBtn.attr('data-topic', topic);
                    targetBtn.attr('data-description', description);
                    targetBtn.attr('data-password', password);
                    targetBtn.attr('data-date', meeting_date);
                }
                $('#meeting_block').hide();
                $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#success-alert").slideUp(500);
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
            //if fails      
            }
        });
    });
    $('#removeMeetingModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var meeting_id = button.attr('data-id');
        var meeting_room_id = button.attr('data-roomid');
        var modal = $(this);
        modal.find('#removeMeetingBtn').attr('data-id', meeting_id);
        modal.find('#removeMeetingBtn').attr('data-roomid', meeting_room_id);
    });
    $('#removeMeetingBtn').on('click', function(){
        var meeting_id= $(this).attr('data-id');
        var meeting_room_id = $(this).attr('data-roomid');
        $.ajax({
            url : '<?=base_url();?>zoom/meeting_delete',
            type: "POST",
            data : 'meeting_id='+meeting_id+'&meeting_room_id='+meeting_room_id,
            success:function(data, textStatus, jqXHR) {
            $('#removeMeetingModal').modal('hide');
            $('#meeting_table tbody tr[data-id="'+meeting_id+'"]').remove();
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#success-alert").slideUp(500);
            });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //if fails      
            }
        });
    });
    getUserList();
});
function showMeeting(e){
    var showBtn = $(e);
    var id = showBtn.attr('data-id');
    var topic = showBtn.attr('data-topic');
    var password = showBtn.attr('data-password');
    var description = showBtn.attr('data-description');
    var meeting_date = showBtn.attr('data-date');
    var status = showBtn.attr('data-status');
    var meeting_room_id = showBtn.attr('data-roomid');
    var start_url = showBtn.attr('data-starturl');
    $("#meeting_body_block").empty().append(meetingTemplate({
        id:id,
        topic:topic,
        password:password,
        description:description,
        meeting_room_id:meeting_room_id,
        start_url:start_url
    }));
    $('#meeting_date').datetimepicker({
        format:'YYYY-MM-DD HH:mm:ss',
        minDate:new Date(),
        defaultDate:moment(meeting_date),
        showClose:true
    });
    //var d = $('#meeting_date').data("DateTimePicker");//.date(meeting_date);
    $('#saveBtn').attr('data-id', id);
    $('#saveBtn').attr('data-status', status);
    $('#meeting_block').show();
}
function getUserList(){
    var postData = {

    };
    $.ajax({
        url : '<?=base_url();?>zoom/user_list',
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) {
            console.log(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
        //if fails      
        }
    });
}
</script>