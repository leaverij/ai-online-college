<style>
.coupon-code .field-counter{
	position: absolute;
	right: 20px;
	bottom: 7px;
	width: 25px;
	height: 20px;
	line-height: 20px;
	text-align: center;
	background: #7ac4cc;
	color: #fff;
	border-radius: 4px;
}
.hero {position:relative; border-radius:5px 5px 0 0;}
.hero .hero-meta {padding:30px;}
.hero-course h1 {font-size:1.7em; margin:0 0 10px;}
.hero-course p {color:rgba(255,255,255,0.85); font-size:1.1em; margin:0 0 20px;}
.hero-course .course-stats {}
.hero-course .course-stats strong {color:#fff; font-size:1.7em;}
.hero-course .course-stats .free {font-size:1.14em;}
.hero-course .course-stats p {color:rgba(255,255,255,0.8); font-size:0.92em; margin:0;}
.hero-bottom {background:#eee; padding:20px 30px; border-radius:0 0 5px 5px; box-shadow:0 1px 0 0 #dedede;}

.table-progression-bar .table {margin:0;}
.table-progression-bar .table td {/*height:15px;*/padding:3px 1px; border-top:none; border-collapse:collapse;}
.table-progression-bar a {height:15px; display:block; background:#fff; box-shadow:0 1px 0 0 rgba(0,0,0,0.1) inset;}
.table-progression-bar a:hover {background:#ddd;}/*#cfe4e5*/
.table-progression-bar a.progress-complete {background:#71bec6;}
.table-progression-bar a.progress-complete-unordered {background:#cfe4e5;}
.table-progression-bar a.in-progress {background:#71bec6;  background-image:linear-gradient(45deg,rgba(255,255,255,.25) 5%,transparent 5%,transparent 15%,rgba(255,255,255,.25) 15%,rgba(255,255,255,.25) 25%,transparent 25%,transparent 35%,rgba(255,255,255,.25) 35%,rgba(255,255,255,.25) 45%,transparent 45%,transparent 55%,rgba(255,255,255,.25) 55%,rgba(255,255,255,.25) 65%,transparent 65%,transparent 75%,rgba(255,255,255,.25) 75%,rgba(255,255,255,.25) 85%,transparent 85%,transparent 95%,rgba(255,255,255,.25) 95%);}
.table-progression-bar .table td:first-child a {border-radius:15px 0 0 15px;}
.table-progression-bar .table td:last-child a {border-radius:0 15px 15px 0;}

.unit-block:hover {border:1px solid #add7d9;}
.field-counter {position: absolute; right: 20px; bottom: 7px; width: 25px; height: 20px; line-height: 20px; text-align: center; background: #7ac4cc; color: #fff; border-radius: 4px;}
@media screen and (max-width: 768px) {
	.table-progression-bar .table {background:none;}
	.table-progression-bar.table-responsive { overflow:visible; border:none; margin:0;}
} 
</style>
<div class="container">
	<div class="row">
    	<div class="col-sm-12">
        	<div class="hero hero-course" style="background-color:#b7c0c7;">
                <?php $lab = json_decode($getSingleChapterContent); ?>
            	<div class="hero-meta">
                	<h1 style="color:#fff;"><?php echo $lab[0]->chapter_content_desc; ?></h1><!-- name -->
                    <p><?php echo $lab[0]->subtitle; ?></p><!-- desc -->
                    <div class="course-stats"><strong class="free">免費體驗</strong></div>
                </div>
            </div><!-- .hero.hero-course -->
            <div class="hero-bottom">
                <div style="display:table;">
					<style>
                    .hero-actions .btn-container {display:table-cell; padding-right:10px;}
                    </style>                	
                    <div class="hero-actions">
                        <div class="btn-container"><a href="<?php echo $lab[0]->target_url ?>" target="_blank" class="btn btn-success" role="button">啟動LAB</a></div>
                        <div class="btn-container"><a href="<?php echo base_url().'library'.$lab[0] -> course_url?>" class="btn btn-default" role="button">進階學習</a></div>
                    </div><!-- hero-actions -->
                    <div class="table-progression-bar table-responsive" style="display:table-cell; width:100%; vertical-align:middle;">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                <span class="sr-only">60% Complete</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!-- .hero-bottom -->
        </div>
    </div>
</div>

<!--     BLOCK COURSE CONTENT    -->
<section id="content">
	<div class="container">
    	<div class="row">
        	<!--     ACHIEVEMENTS    -->
        	<div class="col-md-12">
                <h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">說明</h3>        
                <div id="unit-list">
                    <?php echo $lab[0]->content; ?>
                </div>
            </div>
        </div>
    </div>
</section>