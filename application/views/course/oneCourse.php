<style>
.coupon-code .field-counter{
	position: absolute;
	right: 20px;
	bottom: 7px;
	width: 25px;
	height: 20px;
	line-height: 20px;
	text-align: center;
	background: #7ac4cc;
	color: #fff;
	border-radius: 4px;
}
.hero {position:relative; border-radius:5px 5px 0 0;}
.hero .hero-meta {padding:30px;}
.hero-course h1 {font-size:1.7em; margin:0 0 10px;}
.hero-course p {color:rgba(255,255,255,0.85); font-size:1.1em; margin:0 0 20px;}
.hero-course .course-stats {}
.hero-course .course-stats strong {color:#fff; font-size:1.7em;}
.hero-course .course-stats .free {font-size:1.14em;}
.hero-course .course-stats p {color:rgba(255,255,255,0.8); font-size:0.92em; margin:0;}
.hero-bottom {background:#eee; padding:20px 30px; border-radius:0 0 5px 5px; box-shadow:0 1px 0 0 #dedede;}

.table-progression-bar .table {margin:0;}
.table-progression-bar .table td {/*height:15px;*/padding:3px 1px; border-top:none; border-collapse:collapse;}
.table-progression-bar a {height:15px; display:block; background:#fff; box-shadow:0 1px 0 0 rgba(0,0,0,0.1) inset;}
.table-progression-bar a:hover {background:#ddd;}/*#cfe4e5*/
.table-progression-bar a.progress-complete {background:#71bec6;}
.table-progression-bar a.progress-complete-unordered {background:#cfe4e5;}
.table-progression-bar a.in-progress {background:#71bec6;  background-image:linear-gradient(45deg,rgba(255,255,255,.25) 5%,transparent 5%,transparent 15%,rgba(255,255,255,.25) 15%,rgba(255,255,255,.25) 25%,transparent 25%,transparent 35%,rgba(255,255,255,.25) 35%,rgba(255,255,255,.25) 45%,transparent 45%,transparent 55%,rgba(255,255,255,.25) 55%,rgba(255,255,255,.25) 65%,transparent 65%,transparent 75%,rgba(255,255,255,.25) 75%,rgba(255,255,255,.25) 85%,transparent 85%,transparent 95%,rgba(255,255,255,.25) 95%);}
.table-progression-bar .table td:first-child a {border-radius:15px 0 0 15px;}
.table-progression-bar .table td:last-child a {border-radius:0 15px 15px 0;}

.unit-block:hover {border:1px solid #add7d9;}
.field-counter {position: absolute; right: 20px; bottom: 7px; width: 25px; height: 20px; line-height: 20px; text-align: center; background: #7ac4cc; color: #fff; border-radius: 4px;}
@media screen and (max-width: 768px) {
	.table-progression-bar .table {background:none;}
	.table-progression-bar.table-responsive { overflow:visible; border:none; margin:0;}
} 
#unit-list {
	margin: 10px 0px;
}
.discussion-list {
	list-style:none; padding:0;
	margin:10px 0px;
}
.discussion-list li {overflow:hidden; padding:15px 0; border-top:1px solid #ededed;}
.discussion-list li:first-child {border-top:none; padding-top:0;}
.avatar {
	width:60px;
	height:60px;
	overflow:hidden;
	background-position:center;
	background-size:50px auto !important;
	border-radius:50px;
	border:5px solid #eee;}
.avatar img {width:60px; height:60px; display:none;}
.discussion-list .avatar {
	float:left;
	margin-left:-80px;
}
.discussion-meta {padding-left:80px;}
.discussion-meta h4 {font-size:1.14em;}
.discussion-meta p {color:#999;}
.discussion-meta p > strong {color:#999;}
.discussion-list .reply-count {padding-top:10px; color:#999; float:right; text-align:center;}
.discussion-list .reply-count strong {font-size:24px;}
.announcement_title { text-decoration:none; }
#meeting_table {
	margin: 10px 0px;
}
</style>
<?php $announcement_content = json_decode($announcements);?>
<?php $meeting_content = json_decode($meetings);?>
<?php $getOneTeacher=json_decode($getOneTeacher);?>
<?php $row2 = json_decode($getOneChapter); ?>
<?php 
if(!empty($getUserChapter)){
	$row3 = json_decode($getUserChapter); 
}
?>
<?php 
	$isDone=array();
	for($i=0;$i<count($row2);$i++){
		array_push($isDone,0);
	}
	if(!empty($row3)){
		foreach($row3 as $key2=>$value2){
			$i=0;
			foreach($row2 as $key=>$value){
				if($value2->chapter_id===$value->chapter_id){
					$isDone[$i]=1;
				}
				$i++;
			}
		}
	}
?>
<div class="container">
	<?php $this->load->view('breadcrumb'); ?>
</div>
<div class="container">
	<div class="row">
    	<div class="col-sm-12">
        	<div class="hero hero-course" style="background-color:<?php echo $getEachPreferentialPrice['category_color']?>;">
				<? foreach($row2 as $key=>$val){?><?}?>
            	<div class="hero-meta">
                	<h1 style="color:#fff;"><?php echo $getEachPreferentialPrice['course_name']?></h1>
                    <p><?php echo $getEachPreferentialPrice['course_desc']?></p>
                    <div class="course-stats">
                    	<?if($getEachPreferentialPrice['course_price']){?>
                    	<!-- 付費課程 -->
                    		<?if($getEachPreferentialPrice['preferential_price']){?>
                    			<!-- 優惠價課程 -->
                    			<strong>NT$<?php echo $getEachPreferentialPrice['preferential_price']?></strong> 
                    			<span style="color: rgba(255,255,255,.8); font-size: 1.28em;">(<del>NT$<?php echo $getEachPreferentialPrice['course_price']?></del>)</span>
                        		<p>優惠期限：<?php echo date("Y-m-d", strtotime($getEachPreferentialPrice['start_time']));?>～<?php echo date("Y-m-d", strtotime($getEachPreferentialPrice['end_time']));?></p>
                    		<?}else{?>
                    			<!-- 原價課程 -->
                    			<strong>NT$<?php echo $getEachPreferentialPrice['course_price']?></strong>
                    		<?}?>
                    	<?}else{?>
                    	<!-- 免費課程 -->
                    		<strong class="free">免費課程</strong>
                    	<?}?>
                    	
                    </div>
                </div>
            </div>
            <div class="hero-bottom">
                <div style="display:table;">
					<style>
                    .hero-actions .btn-container {display:table-cell; padding-right:10px;}
                    </style>                	
                    <div class="hero-actions">
                        <!-- 
                                付費課程 => 購買課程
                                免費課程 => 訂閱課程
                        -->
                        <?if($this->session->userdata('user_id')&& isset($segments)){?>
                            <?if($val->course_price=='0'){?>
                                <?if(!empty($getCourseOrderDetail['order_detail_id'])){?>
                                    <div class="btn-container"><a href="<?echo site_url($segments);?>" class="btn btn-primary" role="button">繼續課程</a></div>
                                <?}else{?>
                                    <div class="btn-container"><button data-course-id="<?echo $val->course_id?>" data-course-name="<?echo $val->course_name?>" type="button" class="btn btn-primary subscribe-course">訂閱課程</button></div>
                                <?}?> 
                            <?}else{?>
                                <?if(!empty($getCourseOrderDetail['order_detail_id'])){?>
                                    <div class="btn-container"><a href="<?echo site_url($segments);?>" class="btn btn-primary" role="button">繼續課程</a></div>
                                <?}else{?>
                                    <div class="btn-container"><a type="button" href="<?=base_url()?>pay/express/<?php echo $val->course_id?>" class="btn btn-primary">立即購買</a></div>
                                <?}?>  
                            <?}?>
                        <?}elseif(isset($segments)){?>
                            <!--未登入狀態下，付費課程：顯示購買課程，免費課程：顯示訂閱課程以及瀏覽課程-->
                            <?if($val->course_price=='0'){?>
                                <div class="btn-container"><a class="btn disabled" style="background:#ddd; border:1px solid #aaa; color:#666;">訂閱課程</a></div>
                                <div class="btn-container"><a href="<?echo site_url($segments);?>" class="btn btn-success" role="button">瀏覽課程</a></div>
                            <?}else{?>
                                <div class="btn-container"><a class="btn disabled" style="background:#ddd; border:1px solid #aaa; color:#666;">立即購買</a></div>
                                <!-- <div class="btn-container"><a href="<?echo site_url($segments);?>" class="btn btn-success" role="button">瀏覽課程</a></div> -->
                            <?}?>
                        <?}?>
                    </div><!-- hero-actions -->
                    <?if($this->session->userdata('user_id')&& isset($segments)){?>
                    		<?if(!empty($getCourseOrderDetail['order_detail_id'])){?>
                    			<div class="table-progression-bar table-responsive" style="display:table-cell; width:100%; vertical-align:middle;">
                                    <table class="table">
                                      <tbody>
                                          <tr>
                                              <!-- for loop here start 
                                              .progress-complete => 照學程順序完成的狀態
                                              .progress-complete-unordered => 未照學程順序完成的狀態
                                              .in-progress => 照學程進度的目前學習位置
                                              -->
                                              <? for ( $i=0; $i<count($progress); $i++) {?>
                                                    <td>
                                                        <a class="<?php echo $progress[$i]?>" title="<?php echo $chapter_name[$i]?>"></a>
                                                    </td>
                                              <? }?><!-- for loop end -->
                                          </tr>
                                      </tbody>
                                    </table>
                                </div><!-- table-progression-bar -->
                    		<?}?>
                    		
                    <?}?>
                </div>
            </div>
        </div>
    </div>
</div>

<!--     BLOCK COURSE CONTENT    -->
<section id="content">
	<div class="container">
    	<div class="row">
        	<div class="col-md-8">
				<ul class="nav nav-tabs">
					<li role="presentation" class="active"><a href="#announcement" style="font-size:1.5em; font-weight:400;" role="tab" data-toggle="tab">公告</a></li>
					<li role="presentation"><a href="#mission" style="font-size:1.5em; font-weight:400;" role="tab" data-toggle="tab">關卡 <span class="label label-default" style="font-size: 55%; vertical-align: middle; background-color: #c3c3c3;"><?php echo ($key)?$key+1:'0'; ?></span></a></li>
					<li role="presentation"><a href="<?=base_url();?>forum/course/<?php echo $course_url;?>" style="font-size:1.5em; font-weight:400;">討論區</a></li>
					<li role="presentation"><a href="#meeting" style="font-size:1.5em; font-weight:400;" role="tab" data-toggle="tab">直播</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="announcement">
					<?php
					if($this->session->userdata('user_id')){
						if(count($announcement_content)==0){
					?>
						<div style="margin:10px 0px;" class="alert alert-gray">暫時沒有公告 :)</div>
					<?php		
						}else{
					?>
						<ul class="discussion-list">
					<?php
						foreach($announcement_content as $announcement){
					?>
							<li>
								<div class="col-md-12">
                        			<div class="discussion-meta">
                            			<div class="avatar" style="background:url(<?=base_url();?>assets/img/user/128x128/default_user.png)">
                                			<img src=""/>
                            			</div>
                            			<h4>
                                			<a class="announcement_title"><?php echo $announcement->title;?></a>
                            			</h4>
										<p><?php echo $announcement->content; ?></p>
                            			<p>
                                			<?php echo $announcement->announce_date;?> ‧ 經由 <strong><?php echo $announcement->name;?></strong> 公告
                            			</p>
                        			</div>
                    			</div>
							</li>
					<?php
						}
					?>
						</ul>
					<?php
						}
					}else{
					?>
					<div style="margin:10px 0px;" class="alert alert-gray">請先登入 :)</div>
					<?php
					}
					?>
					</div><!-- #announcement -->
					<div role="tabpanel" class="tab-pane" id="mission">
						<div id="unit-list">
                    	<?php foreach($row2 as $key=>$val){?>
                      		<? if($val->chapter_id){?>
                        	<div class="unit-block">
                            <? if($this->session->userdata('user_id')){?>
                            	<?if($val->course_price=='0'){?>
                                	<? if(!empty($getCourseOrderDetail['order_detail_id'])){?>
                            	<a href="<?php echo base_url().'library/'.$library.'/'.$val->course_url.'/'.$val->chapter_url?>" style="display:block; padding:30px; color:#333; text-decoration:none;">
                                	<? }else{?>
                                <a class="subscribe-course" data-course-id="<?echo $val->course_id?>" data-course-name="<?echo $val->course_name?>" style="display:block; padding:30px; color:#333; text-decoration:none;">
                                	<? }?>
                                <?}else{?>
                                	<? if(!empty($getCourseOrderDetail['order_detail_id'])){?>
                                <a href="<?php echo base_url().'library/'.$library.'/'.$val->course_url.'/'.$val->chapter_url?>" style="display:block; padding:30px; color:#333; text-decoration:none;">
                                	<? }else{?>
                                <a href="<?=base_url()?>pay/express/<?php echo $val->course_id?>" style="display:block; padding:30px; color:#333; text-decoration:none;">


                                	<? }?>
                                <?}?>
                                <div class="unit-meta<?php echo (($isDone[$key]!=1)&&($this->session->userdata('name')))?' un-complete':''?>">
                                	<img src="<?=base_url(); ?>assets/img/badges/<?php echo $val ->chapter_pic;?>" />
                                			<?php if($isDone[$key]==1){?><?}?>
                                    	<h3><?php echo $val -> chapter_name; ?></h3>
                                    	<p class="meta-info">
                                    		<i class="fa fa-eye"></i>
                                    			<?foreach ($getHits as $key=>$hits){?>
                                    				<? if($val->chapter_id==$hits->chapter_id){?>
                                    					<?php echo $hits->hits?>
                                    				<?}?>
                                    			<?}?>
                                    		</p>
                                   			<p><?php echo $val -> chapter_desc; ?></p>
								</div>
                            </a>
							<?}else{?>
								<!-- 未登入狀態 -->
								<?if($val->course_price=='0'){?>
									<!-- 免費課程 -->
                                	<a href="<?php echo base_url().'library/'.$library.'/'.$val->course_url.'/'.$val->chapter_url?>" style="display:block; padding:30px; color:#333; text-decoration:none;">
                                    	<div class="unit-meta">
                                        	<img src="<?=base_url(); ?>assets/img/badges/<?php echo $val ->chapter_pic;?>" />
                                        	<h3><?php echo $val -> chapter_name; ?></h3>
                                        	<p class="meta-info">
                                            	<i class="fa fa-eye"></i> 
                                            	<?foreach ($getHits as $key=>$hits){?>
                                                	<? if($val->chapter_id==$hits->chapter_id){?>
                                                    	<?php echo $hits->hits?>
                                                	<?}?>
                                            	<?}?>
                                        	</p>
                                        	<p><?php echo $val -> chapter_desc; ?></p>
                                    	</div>
                                	</a>
                                <?}else{?>
                                	<!-- 付費課程 -->
                                	<a href="javascript: void(0)" class="pay-course" style="display:block; padding:30px; color:#333; text-decoration:none;">
                                    	<div class="unit-meta">
                                        	<img src="<?=base_url(); ?>assets/img/badges/<?php echo $val ->chapter_pic;?>" />
                                        	<h3><?php echo $val -> chapter_name; ?></h3>
                                        	<p class="meta-info">
                                            	<i class="fa fa-eye"></i> 
                                            	<?foreach ($getHits as $key=>$hits){?>
                                                	<? if($val->chapter_id==$hits->chapter_id){?>
                                                    	<?php echo $hits->hits?>
                                                	<?}?>
                                            	<?}?>
                                        	</p>
                                        	<p><?php echo $val -> chapter_desc; ?></p>
                                    	</div>
                                	</a>
                                <?}?>
                            <? }?>
                        </div>
                      <? }else{?>
                          <div class="alert alert-gray">課程製作中，敬請期待</div>
                      <? }?>
                    <? }?>
				</div>
				
				</div><!-- #mission -->
					<div role="tabpanel" class="tab-pane" id="meeting">
					<?php
					if($this->session->userdata('user_id')){
						if(count($meeting_content)!=0){
					?>
						<table id="meeting_table" class="table table-striped">
						<thead>
							<tr>
								<th>名稱</th>
								<th>時間</th>
								<!-- <th>狀態</th> -->
								<th>操作</th>
							</tr>
						</thead>
						<tbody>
					<?php
					foreach($meeting_content as $meeting){
					?>
							<tr>
								<td><?php echo $meeting->topic ?></td>
								<td><?php echo $meeting->start_time ?></td>
								<!-- <td></td> -->
								<td><a class="btn btn-success" target="_blank" href="<?php echo $meeting->join_url ?>">馬上加入</a></td>
							</tr>
					<?php
					}
					?>
						</tbody>
					</table>
					<?php
						}else{
					?>
						<div style="margin:10px 0px;" class="alert alert-gray">暫時沒有任何直播 :)</div>
					<?php
						}
					}else{
					?>
					<div style="margin:10px 0px;" class="alert alert-gray">請先登入 :)</div>
					<?php
					}
					?>

					</div><!-- #meeting -->
				</div>
                <!--<h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">關卡 <span class="label label-default" style="font-size: 55%; vertical-align: middle; background-color: #c3c3c3;"><?php echo ($key)?$key+1:'0'; ?></span></h3> -->
                
                

            </div><!-- .col-md-8 -->
           
            <!--     INSTRUCTOR    -->
            <style>
            .instructor-avatar{
				float:left;
				width:80px;
				height:80px;
				background-position:center;
				background-size:80px auto !important;
				border-radius:50px;}
			.instructor-avatar img{width:80px; height:80px; display:none;}
            </style>
            <div class="col-md-4">
                <h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">講師</h3>
                <div>
                	<div class="instructor-avatar" style="background:url(<?=base_url();?>assets/img/teacher/<?php echo $getOneTeacher['0']->teacher_pic;?>);">
                		<img src="<?=base_url();?>assets/img/teacher/<?php echo $getOneTeacher['0']->teacher_pic;?>" />
                    </div>    
                    <h4 style="padding-left:100px; line-height:4.5em;"><?php echo $getOneTeacher['0']->name;?></h4>
                    <p><?php echo $getOneTeacher['0']->introduction;?></p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="payModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #7ac4cc;border-radius: 4px 4px 0 0;">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel" style="color: #fff">購買課程</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-offset-1">
						<div class="form-group">
							<div class="row" style="margin-bottom: 20px">
								<div class="col-xs-8 col-sm-6 col-md-8">
  									<span class="pull-right"><?php echo $getEachPreferentialPrice['course_name']?></span>
  								</div>
  								<div class="col-xs-12 col-md-4">
  									<span><?php echo ($getEachPreferentialPrice['preferential_price'])?'<del>'.$getEachPreferentialPrice['course_price'].' 元</del>':$getEachPreferentialPrice['course_price'].' 元'?></span>
  								</div>
  								<?if($getEachPreferentialPrice['preferential_price']){?>
  									<div class="col-xs-8 col-sm-6 col-md-8">
  										<span class="pull-right">特價</span>
  									</div>
  									<div class="col-xs-12 col-md-4">
  										<span><?php echo $getEachPreferentialPrice['preferential_price'].' 元'?></span>
  									</div>
  								<?}?>
							</div>
							<div class="coupon-message" style="color: #d23837"></div>
							<div class="row input-coupon-field">
  								<div class="col-xs-8 col-sm-6 col-md-8 coupon-code">
  									<input type="text" class="form-control" maxlength="16" id="inputCoupon" placeholder="輸入優惠碼">
  									<!-- <span class="field-counter">16</span> -->
  								</div>
  								<div class="col-xs-12 col-md-4">
  									<button type="button" class="btn btn-success check-code-btn">套用</button>
  								</div>
							</div>
							<div class="row show-coupon" style="display:none">
  								<div class="col-xs-8 col-sm-6 col-md-8">
  									<span class="pull-right">優惠券:<span class="coupon"></span></span>
  								</div>
  								<div class="col-xs-12 col-md-4">
  									<span class="discount">-ss</span>
  								</div>
							</div>
							<div class="row" style="margin-top: 20px">
  								<div class="col-xs-8 col-sm-6 col-md-8">
  									<span class="pull-right">總金額</span>
  								</div>
  								<div class="col-xs-12 col-md-4">
  									<span class="total-price">
  										<?php echo ($getEachPreferentialPrice['preferential_price'])?$getEachPreferentialPrice['preferential_price']:$getEachPreferentialPrice['course_price']?> 
  									</span>元
  								</div>
							</div>
  						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<form id="buy-course-form">
					<input type="hidden" id="course_id" name="course_id" value="<?php echo $getEachPreferentialPrice['course_id']?>" />
					<input type="hidden" id="coupon_code" name="coupon_code" value="" />
					<input type="hidden" id="type" name="type" value="course" />
					<button type="button" id="modal-buy-course-btn" class="btn btn-primary">確定購買</button>
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- 以下為勞委會的CASE -->
<?php if($this->user_model->isSuper($this->session->userdata('email'))){?>
<!-- Modal -->
<div class="modal fade" id="webCamModal" tabindex="-1" role="dialog" aria-labelledby="webCamModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="webCamModalLabel">因為課程需求，請按確認讓我們留下您的相片</h4>
      </div>
      <div class="modal-body">
        <script type="text/javascript" src="<?=base_url();?>assets/js/webcam/webcam.js"></script>
        <?php $filename = date('YmdHis') . '.jpg';?>
		<script language="JavaScript">
		webcam.set_api_url( '<?=base_url();?>release/record/<?=$filename;?>');
		webcam.set_quality( 100 ); // 相片品質(1 - 100)
		webcam.set_shutter_sound( false ); // 播放拍照聲音
		//document.write( webcam.get_html(400, 300) );
		webcam.set_hook( 'onComplete', function(){ 
			$('#webCamModal').modal('hide');
		} );	
		function take_snapshot() {
			webcam.snap();
		}
		function init_web_cam(){
			document.getElementById('webcam').innerHTML = webcam.get_html(400, 300);
		}
	</script>
      </div>
      <center><div id="webcam" class=""></div></center>
      <div class="modal-footer">
      	<button type="button" class="btn btn-info" onClick="webcam.configure('privacy')">Flash設定</button>
		<button type="button" class="btn btn-danger" onClick="take_snapshot()">拍照</button>
        <button type="button" class="btn btn-primary" onClick="init_web_cam()">啟動相機</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" charset="utf-8">
	document.getElementById('webcam').innerHTML = webcam.get_html(400, 300);
</script>
<?}?>