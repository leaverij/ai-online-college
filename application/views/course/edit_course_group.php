<?php
    $course_content = json_decode($course);
    $content_groups = json_decode($content_groups);
?>
<div class="container" style="padding-top:30px; margin-bottom: 20px;">
    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view('course/course_sidebar'); ?> 
        </div> 
        <div class="col-md-9">
            <h3><?php echo $course_content->course_name ?></h3>
            <h4>學生分組列表</h4>
            <hr>
            <a class="btn btn-success" id="createGroupBtn"><i class="fa fa-plus" aria-hidden="true"></i> 新增分組</a><br /><br />
            <div id="new-group-block" style="display:none;">
                <form id="create_new_group_form" method="post" action="<?=base_url();?>api/course/createNewGroup">
                    <div class="form-group">
                        <label>分組名稱</label>
                        <input type="text" id="group_name" name="group_name" class="form-control" value="" maxlength="32">
                    </div>
                    <div style="text-align:center;">
                        <input type="submit" class="btn btn-success form-control" value="建立分組">
                    </div>
                    <input type="hidden" name="course_id" class="form-control" value="<?php echo $course_content->course_id ?>">
                </form>
            </div>
            <br />
            <table class="table table-striped table-bordered" id="groupList" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>分組名稱</th>
                        <th>建立日期</th>
                        <th>學生人數</th>
                        <th>編輯</th>
                        <th>刪除</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                foreach($content_groups as $cp){
                ?>
                <tr data-id="<?php echo $cp->content_group_id ?>">
                    <td><?php echo $cp->name ?></td>
                    <td><?php echo $cp->create_time ?></td>
                    <td><?php echo $cp->counts?></td>
                    <td><a href="<?=base_url();?>course/editCourseGroupStudent/<?php echo $course_content->course_id ?>/<?php echo $cp->content_group_id?>" class="btn btn-success">編輯</a></td>
                    <td><button data-id="<?php echo $cp->content_group_id ?>" data-name="<?php echo $cp->name ?>" data-toggle="modal" data-target="#removeGroupModal" class="btn btn-danger" type="button">刪除</button></td>
                </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Remove Group Modal -->
<div id="removeGroupModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog" role="document">
        <!--<form method="post" action="<?=base_url();?>api/course/removeGroup"> -->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="remove_group_name"></h4>
            </div>
            <div class="modal-body">
              <h4>確定刪除此分組?</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
              <button id="removeGroupBtn" type="button" class="btn btn-danger" data-id="">確定</button>             
            </div>
          </div>
        <!--</form>-->    
    </div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/bootstrap-maxlength.js" type="text/javascript" charset="utf-8"></script>
<script>
    $('input[maxlength]').maxlength();
    $(document).ready(function(){
        
        $('#createGroupBtn').on('click', function(){
            $('#new-group-block').show();
        });
        $('#create_new_group_form').submit(function(e){
            var postData = $('#create_new_group_form').serializeArray();
            var formURL = $('#create_new_group_form').attr("action");
            $.ajax({
                url : formURL,
                type: "POST",
                data : postData,
                success:function(data, textStatus, jqXHR) {
                    $('#new-group-block').hide();
                    $('#group_name').val('');
                    $('#groupList tbody').append(
                        '<tr data-id="'+data.id+'">'+
                            '<td>'+data.name+'</td>'+
                            '<td>'+data.create_date+'</td>'+
                            '<td>0</td>'+
                            '<td><a href="<?=base_url();?>course/editCourseGroupStudent/<?php echo $course_content->course_id ?>/'+data.id+'" class="btn btn-success">編輯</a></td>'+
                            '<td><button data-id="'+data.id+'" data-name="'+data.name+'" class="btn btn-danger" data-toggle="modal" data-target="#removeGroupModal" type="button">刪除</button></td>'+
                        '</tr>'
                    );
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //if fails      
                }
            });
            e.preventDefault(); //STOP default action
        });

        $('#removeGroupModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var removeGroupId = button.attr('data-id');
            var removeGroupName = button.attr('data-name');
            var modal = $(this);
            modal.find('#removeGroupBtn').attr('data-id', removeGroupId);
            modal.find('#remove_group_name').html(removeGroupName);
        });
        $('#removeGroupBtn').on('click', function(){
            var group_id= $(this).attr('data-id');
            $.ajax({
                url : '<?=base_url();?>api/course/removeCourseGroup',
                type: "POST",
                data : 'content_group_id='+group_id+'&course_id=<?php echo $course_content->course_id ?>',
                success:function(data, textStatus, jqXHR) {
                    $('#removeGroupModal').modal('hide');
                    $('#groupList tbody tr[data-id="'+group_id+'"]').remove();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //if fails      
                }
            });
        });
        
    });
    
</script>