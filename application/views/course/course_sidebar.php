<style type="text/css">
.sort-content {/*padding:0 15px;*/ border-right:1px solid #ddd; text-align:right;}
#sidebar-collapse .navbar-collapse {max-height:100%;}
.form-group h4 {margin:0; padding:0 15px 5px 0; font-size:1.14em; color:#71bec6;}
.form-group h4 > a {color:#71bec6; text-decoration:none;}
.sort-content .nav a {padding:5px 15px; color:#666; position:relative;}
.sort-content .nav a:hover, .sort-content .nav a:focus {background:none; color:#2c3947; font-weight:bold;}
.sort-content .nav a:hover:before {content:''; width:2px; height:30px; background-color:#279ca7; position:absolute; right:-15px; top:0;}
.sort-content .nav li.active a {background:none; color:#2c3947; font-weight:bold;}
.sort-content .nav li.active a:before {content:''; width:2px; height:30px; background-color:#279ca7; position:absolute; right:-15px; top:0;}
.sidebar-toggle {display:none;}
@media screen and (max-width: 768px) {
	.sidebar-toggle-head {display:none;}
	.sidebar-toggle {display:block;}
}
</style>
<?php
    $course_content = json_decode($course);
?>
	<div id="sidebar-collapse" class="" style="padding:0;">
		<div class="sort-content">
			<div id="tracks-sort" class="form-group">
            	<h4 class="sidebar-toggle">
					<a data-toggle="collapse" data-parent="#tracks-sort" href="#tracks-sort-container">課程編輯</a>
				</h4>
            	<h4 class="sidebar-toggle-head">課程資訊</h4>
				<ul id="tracks-sort-container" class="nav navbar-collapse collapse">
                    <li>
                        <a href="<?=base_url();?>course/edit/<?php echo $course_content->course_id ?>/info">基本資訊</a>
                    </li>	
                    <li>
                        <a href="<?=base_url();?>course/edit/<?php echo $course_content->course_id ?>/price">價格與優惠券</a>
                    </li>
				</ul><br />
				<h4 class="sidebar-toggle-head">課程內容</h4>
				<ul id="tracks-sort-container" class="nav navbar-collapse collapse">
					<li>
                        <a href="<?=base_url();?>course/edit/<?php echo $course_content->course_id ?>/announcement">課程公告</a>
                    </li>
                    <li>
                        <a href="<?=base_url();?>course/edit/<?php echo $course_content->course_id ?>/content">課程內容</a>
					</li>
					<li>
						<a href="<?=base_url();?>course/edit/<?php echo $course_content->course_id ?>/meeting">直播教學</a>
					</li>
				</ul><br />
				<h4 class="sidebar-toggle-head">課程管理</h4>
				<ul id="tracks-sort-container" class="nav navbar-collapse collapse">
					<li>
                        <a href="<?=base_url();?>course/edit/<?php echo $course_content->course_id ?>/student">學生列表</a>
                    </li>
					<li>
						<a href="<?=base_url();?>course/edit/<?php echo $course_content->course_id ?>/group">學生分組</a>
					</li>
				</ul>
			</div>
		</div>
	</div><!-- /sidebar-collapse-->