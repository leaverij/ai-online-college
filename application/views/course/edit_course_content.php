<?php
    $course_content = json_decode($course);
    $chapter_content = json_decode($chapters);
?>
<style>
.add-new-chapter {
    border: 1px dashed #a2a2a2;
    margin-top: 10px;
    cursor: pointer;
    padding: 10px;
}
.add-new-chapter:hover {
    background: #eef0f2;
}
.add-new-chapter a {
    text-decoration: none;
}
ul.chapter-list {
    margin: 20px 0px 20px 0px;
    padding-left: 0px;
}
ul.chapter-list li {
    list-style:none;
}
.chapter-block {
    background: #eef0f2;
    width: 100%;
    border: 1px solid #a2a2a2;
    margin-bottom: 5px;
    padding: 10px;
}
.chapter-sort {
    cursor: move;
    display: inline-block;
    vertical-align: middle;
    margin-right: 10px;
    border-right: 1px solid #b4b4b4;
    padding: 10px;
}
.chapter-name {
    display: inline-block;
    font-size: 18px;
    cursor: pointer;
    text-decoration: none !important;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    vertical-align: middle;
    max-width: 560px;
    width: calc(100% - 250px);
}
.edit-btn-block {
   padding: 4px;
   float: right; 
}
.edit-btn-block a{
    width: 35px;
    height: 35px;
    margin-left:5px;
}
#new-chapter-block {
    display: none;
    margin-top: 10px;
}
</style>
<div class="container" style="padding-top:30px; margin-bottom: 20px;">
    <div style="display:none;" class="alert alert-success" id="success-alert">
    	<strong>更新成功</strong>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view('course/course_sidebar'); ?> 
        </div> 
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li><a href="<?=base_url()?>course/teach">所有課程</a></li>
                <li>課程關卡編輯</li>
            </ol>
            <h3><?php echo $course_content->course_name ?></h3>
            <h4>編輯關卡內容</h4>
            <hr>
            <ul class="chapter-list">
            <?php
            foreach($chapter_content as $chapter_val){
            ?>
                <li id="chapter_<?php echo $chapter_val->chapter_id ?>">
                    <div class="chapter-block">
                        <span class="chapter-sort"><i class="fa fa-arrows" aria-hidden="true"></i></span>
                        <a class="chapter-name" href="<?=base_url();?>chapter/editChapterContent/<?php echo $course_content->course_id ?>/<?php echo $chapter_val->chapter_id ?>">
                            <span class="chapter_name_text"><?php echo $chapter_val->chapter_name ?></span>
                            <form method="post" action="<?=base_url();?>api/chapter/updateChapterName">
                                <input class="form-control chapter_name_input" style="display:none;" type="text" name="chapter_name" value="<?php echo $chapter_val->chapter_name ?>">
                                <input type="hidden" name="chapter_id" value="<?php echo $chapter_val->chapter_id ?>">
                            </form>
                        </a>
                        <span class="edit-btn-block">
                            <a onclick="saveChapterName(this)" data-chapterid="<?php echo $chapter_val->chapter_id ?>" class="btn btn-success edit-btn btn-save" style="display:none;"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
                            <a onclick="editChapterName(this)" class="btn btn-primary edit-btn btn-edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <a data-toggle="modal" data-target="#removeChapterModal" data-chapterid="<?php echo $chapter_val->chapter_id ?>" class="btn btn-danger edit-btn btn-remove"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </span>
                    </div>
                </li>
            <?php
            }
            ?>
            </ul>
            <div class="add-new-chapter">
                <a id="createNewChapterBtn"><i class="fa fa-plus" aria-hidden="true"></i> 新增關卡</a>
                <div id="new-chapter-block">
                    <form id="create_new_chapter_form" method="post" action="<?=base_url();?>api/chapter/createNewChapter">
                        <div class="form-group">
                            <label>關卡名稱</label>
                            <input type="text" id="new_chapter_name" name="new_chapter_name" class="form-control" value="">
                            <input type="hidden" name="new_course_id" class="form-control" value="<?php echo $course_content->course_id ?>">
                        </div>
                        <div style="text-align:center;">
                            <input type="submit" class="btn btn-success form-control" value="建立關卡">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 移除關卡確認Modal -->
<div id="removeChapterModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h4>確定要刪除嗎?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button id="removeChapterBtn" data-chapterid="" type="button" class="btn btn-primary">確定</button>
            </div>
        </div>
    </div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/bootstrap-maxlength.js" type="text/javascript" charset="utf-8"></script>
<script>
$(document).ready(function(){

    $( ".chapter-list" ).sortable({
        start: function(e, ui) {
            $(this).attr('data-previndex', ui.item.index());
        },
        update: function(e, ui) {
            var reorderArr = [];
            $('.chapter-list li').each(function(index, ele){
                var chapter_ele_id = $(ele).attr('id').split('_')[1];
                reorderArr.push(chapter_ele_id);
            });
            chapterReorder(reorderArr);
        }
    });

    $('#removeChapterModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var chapter_id = button.attr('data-chapterid');
        var modal = $(this);
        modal.find('#removeChapterBtn').attr('data-chapterid', chapter_id);
    });
});
$('#createNewChapterBtn').on('click', function(){
    $('#new-chapter-block').show();
});
$('#create_new_chapter_form').submit(function(e){
    var postData = $('#create_new_chapter_form').serializeArray();
    var formURL = $('#create_new_chapter_form').attr("action");
    $.ajax({
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) {
            $('#new-chapter-block').hide();
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#success-alert").slideUp(500);
            });
            $('#new_chapter_name').val('');
            $('ul.chapter-list').append(
                '<li id="chapter_'+data.id+'">'+
                    '<div class="chapter-block">'+
                        '<span class="chapter-sort"><i class="fa fa-arrows" aria-hidden="true"></i></span>'+
                        '<a class="chapter-name" href="<?=base_url();?>chapter/editChapterContent/<?php echo $course_content->course_id?>/'+data.id+'">'+
                            '<span class="chapter_name_text">'+data.name+'</span>'+
                            '<form method="post" action="<?=base_url();?>api/chapter/updateChapterName">'+
                                '<input class="form-control chapter_name_input" style="display:none;" type="text" name="chapter_name" value="'+data.name+'">'+
                                '<input type="hidden" name="chapter_id" value="'+data.id+'">'+
                            '</form>'+
                        '</a>'+
                        '<span class="edit-btn-block">'+
                            '<a onclick="saveChapterName(this)" data-chapterid="'+data.id+'" class="btn btn-success edit-btn btn-save" style="display:none;"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>'+
                            '<a onclick="editChapterName(this)" class="btn btn-primary edit-btn btn-edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>'+
                            '<a data-toggle="modal" data-chapterid="'+data.id+'" data-target="#removeChapterModal" class="btn btn-danger edit-btn btn-remove"><i class="fa fa-trash" aria-hidden="true"></i></a>'+
                        '</span>'+
                    '</div>'+
                '</li>'
            );
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //if fails      
        }
    });
    e.preventDefault(); //STOP default action
});

$('#removeChapterBtn').on('click', function(){
    var chapter_id= $(this).attr('data-chapterid');
    $.ajax({
        url : '<?=base_url();?>api/chapter/removeChapter',
        type: "POST",
        data : 'chapter_id='+chapter_id,
        success:function(data, textStatus, jqXHR) {
            $('#removeChapterModal').modal('hide');
            $('ul.chapter-list li#chapter_'+chapter_id).remove();
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#success-alert").slideUp(500);
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
                //if fails      
        }
    });
});
function editChapterName(e){
    var editBtn = $(e);
    editBtn.parent().find('a.btn-save').show();
    editBtn.parent().find('a.btn-edit').hide();
    editBtn.parent().find('a.btn-remove').hide();
    $(e).parent().parent().find('span.chapter_name_text').hide();
    $(e).parent().parent().find('input.chapter_name_input').show();
}
function saveChapterName(e){
    var saveBtn = $(e);
    saveBtn.hide();
    saveBtn.parent().find('a.btn-edit').show();
    saveBtn.parent().find('a.btn-remove').show();
    var chapter_name_input = saveBtn.parent().parent().find('input.chapter_name_input');
    var chapter_name_text = saveBtn.parent().parent().find('span.chapter_name_text');
    chapter_name_input.hide();
    var chapter_name_form = saveBtn.parent().parent().find('form');
    chapter_name_form.submit(function(e){
        e.preventDefault(); //STOP default action
        var postData = chapter_name_form.serializeArray();
        var formURL = chapter_name_form.attr("action");
        $.ajax({
            url : formURL,
            type: "POST",
            data : postData,
            success:function(data, textStatus, jqXHR) {
                chapter_name_text.html(chapter_name_input.val()).show();
                $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#success-alert").slideUp(500);
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //if fails      
            }
        });
    });
    chapter_name_form.submit();
}
function chapterReorder(reorderArr){
    $.ajax({
        type: 'post',
        data: 'reorderArr='+JSON.stringify(reorderArr),
        url: '<?=base_url()?>api/chapter/chapterReorder',
        success : function(data, error, xhr){
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#success-alert").slideUp(500);
            });
        },
        error: function(jqXHR, textStatus, errorThrown){

        }
    });
}
</script>