<?php
    $course_content = json_decode($course);
?>
<div class="container" style="padding-top:30px; margin-bottom: 20px;">
    <div style="display:none;" class="alert alert-success" id="success-alert">
    	<strong>更新成功</strong>
    </div>
    <div style="display:none;" class="alert alert-danger" id="danger-alert">
        <strong>請輸入課程名稱</strong>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view('course/course_sidebar'); ?> 
        </div> 
        <div class="col-md-9">
            <h3><?php echo $course_content->course_name ?></h3>
            <h4>編輯課程基本資訊</h4>
            <hr>
            <form id="course_info_form" method="post" action="<?=base_url();?>api/course/editCourseInfo">
                <div class="form-group">
                    <label for="course_name">課程名稱</label>
                    <input type="text" id="course_name" name="course_name" maxlength="50" class="form-control" value="<?php echo $course_content->course_name ?>" required="required">
                </div>
                <div class="form-group">
                    <label for="course_library">所屬領域</label>
                    
                    <select id="course_library" name="course_library" class="form-control">
                    <?php
                        $library_content = json_decode($library);
                        foreach($library_content as $library_val){
                    ?>
                        <option value="<?php echo $library_val->library_id ?>"><?php echo $library_val->library_name ?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="course_category">所屬類別</label>
                    <select id="course_category" name="course_category" class="form-control">
                    <?php
                        $category_content = json_decode($category);
                        foreach($category_content as $category_val){
                    ?>
                        <option value="<?php echo $category_val->course_category_id ?>"><?php echo $category_val->course_category_name ?></option>
                    <?php
                        }
                    ?>
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="course_category">課程狀態 <span style="color: #ce4844">#選擇上架即表示該課程會出現在學生端介面</span></label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="course_status" id="course_status1" value="0" <?php echo $course_content->status==0?"checked":"" ?> >未上架
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="course_status" id="course_status2" value="1" <?php echo $course_content->status==1?"checked":"" ?> >上架
                        </label>
                    </div>
                </div>
                <div style="text-align:center;">
                    <input name="course_id" type="hidden" value="<?php echo $course_content->course_id ?>">
                    <input type="submit" id="saveBtn" class="btn btn-success form-control" value="儲存">
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/bootstrap-maxlength.js" type="text/javascript" charset="utf-8"></script>
<script>
$("input[maxlength]").maxlength();
$('#course_info_form').submit(function(e){
    var postData = $('#course_info_form').serializeArray();
    var formURL = $('#course_info_form').attr("action");
    $.ajax({
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) {
            if(data["result"]=="success"){
                $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#success-alert").slideUp(500);
                });
            }else{
                $("#danger-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#danger-alert").slideUp(500);
                });
            }
            
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //if fails      
        }
    });
    e.preventDefault(); //STOP default action
});
// 所屬領域 類別的預設值
$("#course_library").val(<?php echo $course_content->library_id ?>);
$("#course_category").val(<?php echo $course_content->course_category_id ?>);
</script>