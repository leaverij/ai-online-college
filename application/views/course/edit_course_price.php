<?php
    $course_content = json_decode($course);
?>
<div class="container" style="padding-top:30px; margin-bottom: 20px;">
    <div style="display:none;" class="alert alert-success" id="success-alert">
    	<strong>更新成功</strong>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view('course/course_sidebar'); ?> 
        </div> 
        <div class="col-md-9">
            <h3><?php echo $course_content->course_name ?></h3>
            <h4>編輯課程價格與優惠券</h4>
            <hr>
            <form id="course_price_form" method="post" action="<?=base_url();?>api/course/editCoursePrice">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="course_name">課程費用</label>
                            <select id="course_price_method" name="course_price_method" class="form-control">
                            <?php
                                if($course_content->course_price==0){
                            ?>
                                <option value="0" selected>免費</option>
                                <option value="1">付費</option>
                            <?php
                                }else{
                            ?>
                                <option value="0">免費</option>
                                <option value="1" selected>付費</option>
                            <?php
                                }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="course_price_val">金額設定</label>
                            <input id="course_price_val" name="course_price_val" min="0" type="number" class="form-control" value="<?php echo $course_content->course_price ?>">
                        </div>
                    </div>
                </div>
                <div style="text-align:center;">
                    <input name="course_id" type="hidden" value="<?php echo $course_content->course_id ?>">
                    <input type="submit" id="saveBtn" class="btn btn-success form-control" value="儲存">
                </div>
            </form>
        </div>
    </div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script>
$('#course_price_form').submit(function(e){
    var postData = $('#course_price_form').serializeArray();
    var formURL = $('#course_price_form').attr("action");
    $.ajax({
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) {
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#success-alert").slideUp(500);
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //if fails      
        }
    });
    e.preventDefault(); //STOP default action
});
</script>