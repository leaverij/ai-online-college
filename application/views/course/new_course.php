<style>
.center {
    text-align:center;
}
.modal {
    position:relative;
    display:block;
    top:auto;
    right:auto;
    bottom:auto;
    left:auto;
    z-index:99;
}
</style>
<div class="container">
    <div style="padding-bottom:20px;">
        <div class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form method="post" action="<?=base_url();?>api/course/createCourse">
                        <div class="modal-header">
                            <h4 class="modal-title center">建立新課程</h4>
                        </div>
                        <div class="modal-body">
                            <div class="message">
                                <? if($this->session->flashdata('alert')):?>
                                    <? $data=$this->session->flashdata('alert'); ?>
                                    <div class="alert <?=$data['type']?> fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <?=$data['msg']?>
                                    </div>
                                <? endif; ?>
                            </div>
                            <div class="form-group">
                                <label>課程名稱</label>
                                <input type="text" name="name" class="form-control" maxlength="50" required="required">
                            </div>
                            <div class="form-group">
                                <label>所屬領域</label>
                                <select name="library" class="form-control">
                                <?php
                                    $library_content = json_decode($library);
                                    foreach($library_content as $library_val){
                                ?>
                                    <option value="<?php echo $library_val->library_id ?>"><?php echo $library_val->library_name ?></option>
                                <?php
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>所屬類別</label>
                                <select name="category" class="form-control">
                                <?php
                                    $category_content = json_decode($category);
                                    foreach($category_content as $category_val){
                                ?>
                                    <option value="<?php echo $category_val->course_category_id ?>"><?php echo $category_val->course_category_name ?></option>
                                <?php
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">儲存並開始</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/bootstrap-maxlength.js" type="text/javascript" charset="utf-8"></script>
<script>
$("input[maxlength]").maxlength();
</script>