<style>
.course-block-ul {
    margin: 20px 0px 20px 0px;
    padding-left: 0px;
}
.course-block-list {
    list-style: none;
    margin-bottom: 10px;
}
.course-block {
    background: #f7f7f7;
    border-radius: 3px;
    width: 100%;
    height: 100px;
    float: left;
    box-shadow: 0 1px #ffffff inset, 0 1px 2px rgba(0, 0, 0, 0.35);
    margin-bottom: 20px;
}
.course-thumbnail {
    float: left;
    border-radius: 3px 0 0 3px;
    margin-right: 10px;
}
.thumbnail {
    width: 100px;
    height: 100px;
}
.course-summary {
    width: 100%;
    height: 100%;
}
.course-info {
    margin-right: 10px;
    position: relative;
    width: 70%;
}
.course-title {
    text-overflow: ellipsis;
    color: #095B97;
    cursor: pointer;
}
</style>
<div class="container">
    <div style="padding-bottom:20px;">
        <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">我教授的課程</h1>
        <div class="row">
            <div class="col-md-12">
                <ul class="course-block-ul">
                <?php
                    $course_content = json_decode($courses);
                    foreach($course_content as $course_val){
                ?>
                <li class="course-block-list">
                    <div class="course-block">
                        <div class="course-thumbnail">
                            <img class="thumbnail" src="<?=base_url();?>assets/img/badges/<?php echo $course_val->course_pic ?>">
                        </div>
                        <div class="course-summary">
                            <div class="course-info">
                                <h4 class="course-title" onclick="window.location='<?=base_url();?>course/edit/<?php echo $course_val->course_id ?>/info'"><?php echo $course_val->course_name ?>
                                </h4>
                                <span class="course-create-date"><?php echo $course_val->create_time ?></span>
                                <div><?php echo $course_val->status==0?'<span class="label label-default">未上架</span>':'<span class="label label-success">上架</span>' ?> 
                                    <a style="font-size:0.75em;" class="upload-course-img btn btn-primary btn-xs" role="button" data-toggle="modal" data-target="#edit-picture-modal" data-course-id="<?php echo $course_val->course_id ?>" >修改課程圖片</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <?}?>
                </ul>
            </div>
        </div>
    </div>
</div>
<form id="updateBadges" class="form-horizontal" role="form" method="post" action="<?php echo base_url().'user/updateBadges'?>" enctype="multipart/form-data">
<div class="modal fade" id="edit-picture-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">修改照片</h4>
            </div>
            <div class="modal-body">
                <input type="file" name="userfile" class="course_img">
                <input type="hidden" name="course_id" class="course_id" value="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="submit" class="btn btn-primary">確認修改照片</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
</form>