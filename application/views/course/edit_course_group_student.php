<?php
    $course_content = json_decode($course);
    $content_group = json_decode($content_group);
?>
<link rel="stylesheet" href="<?=base_url();?>assets/js/icheck/skins/all.css">
<div class="container" style="padding-top:30px; margin-bottom: 20px;">
    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view('course/course_sidebar'); ?> 
        </div> 
        <div class="col-md-9">
            <h3><?php echo $course_content->course_name ?> 分組設定</h3>
            <h4>分組名稱: <?=$content_group[0]->name?></h4>
            <hr>
            <table class="table table-striped table-bordered" id="studentList" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td>選擇</td>
                        <th>姓名</th>
                        <th>Email</th>
                        <th>加入日期</th>
                        <th>最近登入日期</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $td_array = '';
                    foreach($student_list as $student){
                        $checked_val = empty($student['member_id'])?'':'checked';
                        $temp = '<tr>';
                            $temp .= '<td><input type="checkbox" value="'.$student['user_id'].'" '.$checked_val.'></td>';
                            // 姓名
                            $temp .= '<td>'.$student['name'].'</td>';
                            // Email
                            $temp .= '<td>'.$student['email'].'</td>';
                            // 加入日期
                            $temp .= '<td>'.$student['order_time'].'</td>';
                            // 最近登入日期
                            $temp .= '<td>'.$student['last_login'].'</td>';
                        $temp .= '</tr>';
                        $td_array .=$temp;
                    }
                    echo $td_array;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/icheck/icheck.min.js" type="text/javascript" charset="utf-8"></script>
<script>
    jQuery(document).ready(function($){

        //icheck
        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue'
        }).on('ifChecked', function(){
            var checkEle = $(this);
            var checkVal = checkEle.val();
            checkEle.closest('tr').addClass('added');
            updateGroupStudent(checkVal, <?=$content_group[0]->content_group_id?>, true);
        }).on('ifUnchecked', function(){
            var checkEle = $(this);
            var checkVal = checkEle.val();
            checkEle.closest('tr').removeClass('added');
            updateGroupStudent(checkVal, <?=$content_group[0]->content_group_id?>, false);
        });

    });
    function updateGroupStudent(user_id, content_group_id, checked){
        var checkedStr = (checked)?'check':'uncheck';
        $.ajax({
            url : "<?=base_url();?>api/course/updateGroupStudent",
            type: "POST",
            data : "user_id="+user_id+"&content_group_id="+content_group_id+"&checked="+checkedStr,
            success:function(data, textStatus, jqXHR) {

            },
            error: function(jqXHR, textStatus, errorThrown) {
                    
            }
        });
    }
</script>