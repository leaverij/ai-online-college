<?php
    $course_content = json_decode($course);
?>
<div class="container" style="padding-top:30px; margin-bottom: 20px;">
    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view('course/course_sidebar'); ?> 
        </div> 
        <div class="col-md-9">
            <h3><?php echo $course_content->course_name ?></h3>
            <h4>修課學生列表</h4>
            <hr>
            <table class="table table-striped table-bordered" id="studentList" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>姓名</th>
                        <th>Email</th>
                        <th>加入日期</th>
                        <th>最近登入日期</th>
                        <th>已完成課程</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $td_array = '';
                    foreach($student_list as $student){
                        $temp = '<tr>';
                            // 姓名
                            $temp .= '<td>'.$student['name'].'</td>';
                            // Email
                            $temp .= '<td>'.$student['email'].'</td>';
                            // 加入日期
                            $temp .= '<td>'.$student['order_time'].'</td>';
                            // 最近登入日期
                            $temp .= '<td>'.$student['last_login'].'</td>';
                            // 已完成課程
                            if(!empty($student['status']) && $student['status']==1){
                                $temp .= '<td><i style="color:green;" class="fa fa-check-circle-o" aria-hidden="true"></i></td>';
                            }else{
                                $temp .= '<td></td>';
                            }
                        $temp .= '</tr>';
                        $td_array .=$temp;
                    }
                    echo $td_array;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/dataTables.bootstrap.js" type="text/javascript"></script>
<link href="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet">
<script>
    jQuery(document).ready(function($){
        $('#studentList').DataTable({
            "bFilter": false,
            "bLengthChange": false,
            "pageLength": 10
        });
    })
</script>