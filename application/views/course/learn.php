<style>
ul.course-block-list {padding:0;}
ul.course-block-list > li {list-style:none; margin-bottom:30px;}
.course-card {
    border: 1px solid #d5d5d5;
}
.course-card-hero:before {content:''; height:100%; display:inline-block; margin-right:-0.25em; vertical-align:middle; z-index:1;}
</style>
<div class="container">
    <div style="padding-bottom:20px;">
        <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">我的課程</h1>
    </div>
    <div class="row">
    <?php
    foreach($courses as $course){
    ?>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <ul class="course-block-list">
                <li>
                    <div class="course-card">
                        <a href="<?=base_url(); ?>library/<?php echo $course['domain_url']; ?>/<?php echo $course['course_url'] ?>">
                            <div class="col-xs-8 content-meta">
                                <h3><?php echo $course['course_name'] ?></h3>
                                <div class="content-actions-container" style="position:absolute; bottom:30px; right:30px; left:30px;"><?php echo $course['teacher_name'] ?></div>
                            </div>
                            <div class="col-xs-4 course-card-hero" style="background:#f2f2f2; text-align:center; position:absolute; top:0; bottom:0; right:0;">
                                <img src="<?=base_url(); ?>assets/img/badges/<?php echo $course['course_pic']; ?>">
                            </div>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    <?php
    }
    ?>
    </div>
</div>