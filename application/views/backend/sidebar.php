<style>
.search-bar{position:relative;}
.search-bar .search-text {display:block; width:100%; padding:15px; color:#666; font-size:14px; background:#f9f9f9; border:none; border-bottom:1px solid #cfe4e5; border-radius:5px 5px 0 0;}
.search-bar ::-webkit-input-placeholder {color:#bcbcbc;/* WebKit browsers */}
.search-bar .search-text:focus::-webkit-input-placeholder {color:#e3e3e3; transition:ease 0.1s; -webkit-transition:ease 0.1s; -moz-transition:ease 0.1s; -ms-transition:ease 0.1s;}
.search-bar :-moz-placeholder {color:#bcbcbc;/* Mozilla Firefox 4 to 18 */}
.search-bar ::-moz-placeholder  {color:#bcbcbc;/* Mozilla Firefox 19+ */}
.search-bar :-ms-input-placeholder {color:#bcbcbc;/* Internet Explorer 10+ */}
.search-bar .search-btn{position:absolute; top:0; right:0; bottom:0; border:none; padding:0 15px; background:none;}
.search-bar .search-btn span{font-size:16px; color:#bcbcbc;}
</style>

<div class="container">    
    <!--     BLOCK SIDE BAR    -->
	<div class="row" style="margin-top:30px;">
		<style>
		.sidebar {margin-bottom:30px; background:#f5f5f5; border-radius:5px; box-shadow:0 1px 0 0 #ededed;}
		.nav-list li:last-child a{padding-bottom:6px;}
		.nav-list li:last-child a:hover {border-radius:0 0 5px 5px;}
		.nav-list li > a {padding:5px 25px;}
		#sidebar-collapse ul li h4 {font-size:16px;}
		#sidebar-collapse ul li h4 a:hover, #sidebar-collapse ul li.active a{text-decoration:none; background:#eeeeee; color:#24bbc9;}
		#sidebar-collapse ul li a {color:#666; font-size:15px;}
		#sidebar-collapse ul li a:hover {color:#24bbc9;}
		#sidebar-collapse .library-list li a {color:#888;}
        @media screen and (max-width: 768px) {
            .sidebar .navbar-toggle {display:block; margin:0; padding:10px 12px; cursor:pointer; color:#666; font-size:16px;}
        }
        </style>            
        <div class="col-sm-3">
        	<div class="sidebar">
                <div id="sidebar-collapse" class="collapse navbar-collapse" style="padding:0;">
                    <ul class="nav nav-stacked">
                      <li>
                      	<h4 style="margin:0; padding:10px 15px;">資源分類</h4>
                      	<ul class="nav-list nav">
                            <li class="">
                            	<a href="<?php echo base_url().'backend/listIndustry'?>">趨勢應用</a>
                            </li>
                            <li class="">
                            	<a href="<?php echo base_url().'backend/listSeminar'?>">相關活動</a>
                            </li>
                            <li class="">
                            	<a href="<?php echo base_url().'backend/listJobs'?>">企業徵才</a>
                            </li>
                             <li class="">
                            	<a href="<?php echo base_url().'backend/listResource'?>">國內外資源</a>
                            </li>
                        </ul>
                      </li>
                    </ul>
                    <ul class="nav nav-stacked">
                      <li>
                      	<h4 style="margin:0; padding:10px 15px;">優惠卷</h4>
                      	<ul class="nav-list nav">
                            <li class="">
                            	<a href="<?php echo base_url().'coupon'?>">優惠券</a>
                            </li>
                        </ul>
                      </li>
                    </ul>
                    <ul class="nav nav-stacked">
                      <li>
                        <h4 style="margin:0; padding:10px 15px;">使用者權限設定</h4>
                        <ul class="nav-list nav">
                            <li class="">
                              <a href="<?php echo base_url().'setting'?>">使用者權限設定</a>
                            </li>
                        </ul>
                      </li>
                    </ul>
                </div>
                <div class="">
                  <a class="navbar-toggle" data-toggle="collapse" data-target="#sidebar-collapse" style="float:none; position:relative; text-decoration:none; border-top:1px solid #cfe4e5; margin-top:-1px; border-radius:0 0 5px 5px;">
                    <span>後台管理介面</span>
                    <i class="fa fa-eye" style="position:absolute; top:50%; right:0; border:none; margin-top:-8px; padding:0 15px; background:none;"></i>
                    <span class="sr-only">Toggle navigation</span>
                  </a>
                </div>
            </div>
        </div>