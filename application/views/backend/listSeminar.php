<!-- 左邊區塊 -->
<?php $this->load->view('backend/sidebar'); ?>
<!-- 左邊end -->
		<style>
        .tour-start{margin-bottom:20px;padding:15px; background: #f5f5f5; -webkit-border-radius:4px; -moz-border-radius:4px; -ms-border-radius:4px; border-radius:4px;}
        .tour-start h4{margin-top:0;}
		.tour-start span{color:#999999;}
        </style>
  		<div class="col-sm-9">
        	<div>	
                <div class="row">
                	<div class="col-sm-12 add-plus-seminar">
    					<div class="thumbnail">
      						<div class="caption">
      							<button type="button" class="btn btn-success add-seminar-btn" style="width: 100%;height: 50px">
      								<i class="fa fa-plus fa-3x"></i>
      							</button>
      						</div>
    					</div>
                    </div>
                    <div class="col-sm-12 add-seminar" style="display: none">
    					<div class="thumbnail">
      						<div class="caption">
     							<form role="form" action="<?php echo base_url()?>backend/addSeminar" method="post" enctype="multipart/form-data">
      								<!-- <div class="form-group">
    								 	<label for="seminar_pic">活動顯示圖片</label>
    								 	<input type="file" id="img" name="userfile">
    								</div> -->
      								<div class="form-group">
    									<label for="seminar_title">活動標題</label>
    									<input type="text" class="form-control" id="seminar_title" name="seminar_title" placeholder="輸入活動標題">
    								</div>
    								<div class="form-group">
    									<label for="seminar_content">活動摘要</label>
    									<textarea class="form-control" rows="3" id="seminar_content" name="seminar_content" placeholder="輸入活動摘要"></textarea>
    									</div>
    								<div class="form-group">
    									<label for="seminar_url">活動網址</label>
    									<input type="text" class="form-control" id="seminar_url" name="seminar_url" placeholder="輸入活動網址">
    								</div>
    								<div class="form-group">
    									<label for="seminar_place">活動地點</label>
    									<input type="text" class="form-control" id="seminar_place" name="seminar_place" placeholder="輸入活動地點">
    								</div>
    								<div class="form-group">
    									<label for="seminar_host">主辦單位</label>
    									<input type="text" class="form-control" id="seminar_host" name="seminar_host" placeholder="輸入主辦單位">
    								</div>
    								<div class="form-group">
    									<label for="seminar_date">活動起訖時間</label>
    									<div class="container">
       										<div class='col-md-5'>
                								<div class='input-group start_time' id='' data-date-format="YYYY-MM-DD">
                    								<input type='text' class="form-control" name="seminar_start_time"/>
                    									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    								</span>
                								</div>
                							</div>
        									<div class='col-md-5'>
                								<div class='input-group end_time' id='' data-date-format="YYYY-MM-DD">
                   									<input type='text' class="form-control" name="seminar_end_time"/>
                   										<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                   										</span>
               									</div>
            								</div>
            							</div>
    								</div>
    								<div class="form-group">
    									<label for="jobs_category_id">活動分類</label>
                    					<select class="form-control" id="seminar_type" name="seminar_type">
                    						<option value="0">選擇分類</option>
                    						<option value="1">研討會</option>
                    						<option value="2">競賽</option>
                    						<option value="3">展覽</option>
                    						<option value="4">其他</option>
                    					</select>
                    				</div>
    								<div class="form-group">
    									<label for="jobs_category_id">依職務分類</label>
    									<div>
    										<?foreach($listJobsCategory as $do){?>
    										<label class="checkbox-inline">
    											<input type="checkbox" name="jobs_category_id[]" value="<?php echo $do->jobs_category_id;?>"><?php echo $do->jobs_category_name;?>
    										</label>
    										<?}?>
										</div>
                    				</div>
        							<p>	
        								<button type="submit" class="btn btn-info">新增</button>
                           				<button type="button" class="btn btn-danger cancle-add-seminar">取消</button>
        							</p>
        							</form>
      							</div>
    						</div>
                   		</div>
                	<?if ($listAllSeminar->num_rows() > 0){?>
                		<div class="col-sm-12" style="margin-bottom: 10px">
							<div class="row">
                    			<div class="col-md-1">點擊數</div>
                    			<div class="col-md-1">活動類型</div>
                    			<div class="col-md-6">活動資訊</div>
                    			<div class="col-md-1">建立者</div>
                    			<div class="col-md-1">分類</div>
                    			<div class="col-md-1">編輯</div>
                    			<div class="col-md-1">刪除</div>
							</div>
						</div>
                		<?foreach ($listAllSeminar->result() as $row){?>
                			<div class="col-sm-12 seminar" style="margin-bottom: 10px">
                				<div class="row">
                					<div class="col-md-1"><?echo $row->clicks;?> / <?echo $row->modal_clicks;?></div>
                					<div class="col-md-1">
                						<span class="label <?echo $row->label_name;?>">
                							<?switch ($row->seminar_name){	case "1":echo "研討會";break;
                															case "2":echo "競賽";break;
                															case "3":echo "展覽";break;
                															case "4":echo "其他";break;
                															default:echo "其他";}?>
                						</span>
                					</div>
                					<div class="col-md-6">
                						<div class="row">
                							<div class="col-md-12"><?echo $row->seminar_title;?></div>
                							<div class="col-md-12">
                								<?if($row->seminar_start_time == $row->seminar_end_time){?>
                                    				時間 ： <?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>
                                    			<?}else{?>
                                    				時間 ： <?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>～<?php echo date("Y-m-d", strtotime($row->seminar_end_time));?>
                                    			<?}?>
                							</div>
                							<div class="col-md-12">地點：<?echo $row->seminar_place;?></div>
                						</div>
                					</div>
                					<div class="col-md-1"><?echo $row->name;?></div>
                					<div class="col-md-1"><?echo $row->jobs_category_id;?>
                						<!-- <?foreach($listJobsCategory as $do){?>
                							<?if($do->jobs_category_id==$row->jobs_category_id) echo $do->jobs_category_name;?>
                						<?}?> -->
                					</div>
                					<div class="col-md-1"><button type="button" class="btn btn-success edit-seminar-btn pull-right">編輯</button></div>
                					<div class="col-md-1"><button type="button" class="btn btn-danger del-seminar pull-right" data-seminar-id="<?php echo $row->seminar_id?>">刪除</button></div>
                				</div>
                    		</div>
                    		<div class="col-sm-12 edit-seminar" style="display: none">
    							<div class="thumbnail">
      								<div class="caption">
      									<form role="form" action="<?php echo base_url()?>backend/updateSeminar" method="post" enctype="multipart/form-data">
      									<!-- <div class="form-group">
    									 	<label for="seminar_pic">活動顯示圖片</label>
    									 	<img src="<?=base_url();?>assets/img/seminar/<?echo $row->seminar_pic;?>" >
    									 	<input type="file" id="update-img" name="userfile">
    									 	<p class="help-block">您可以在此更換圖片</p>
    									</div> -->
      									<div class="form-group">
    										<label for="seminar_title">活動標題</label>
    										<input type="text" class="form-control" id="seminar_title" name="seminar_title" placeholder="輸入活動標題" value="<?echo $row->seminar_title;?>">
    									</div>
    									<div class="form-group">
    										<label for="seminar_content">活動摘要</label>
    										<textarea class="form-control" rows="3" id="seminar_content" name="seminar_content" placeholder="輸入活動摘要"><?echo $row->seminar_content;?></textarea>
    									</div>
    									<div class="form-group">
    										<label for="seminar_url">活動網址</label>
    										<input type="text" class="form-control" id="seminar_url" name="seminar_url" placeholder="輸入活動網址" value="<?echo $row->seminar_url;?>">
    									</div>
    									<div class="form-group">
    										<label for="seminar_place">活動地點</label>
    										<input type="text" class="form-control" id="seminar_place" name="seminar_place" placeholder="輸入活動地點" value="<?echo $row->seminar_place;?>">
    									</div>
    									<div class="form-group">
    										<label for="seminar_host">主辦單位</label>
    										<input type="text" class="form-control" id="seminar_host" name="seminar_host" placeholder="輸入主辦單位" value="<?echo $row->seminar_host;?>">
    									</div>
    									<div class="form-group">
    									<label for="seminar_date">活動起訖時間</label>
    									<div class="container">
       										<div class='col-md-5'>
                								<div class='input-group start_time' id='' data-date-format="YYYY-MM-DD">
                    								<input type='text' class="form-control" name="seminar_start_time" value="<?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>"/>
                    									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    								</span>
                								</div>
                							</div>
        									<div class='col-md-5'>
                								<div class='input-group end_time' id='' data-date-format="YYYY-MM-DD">
                   									<input type='text' class="form-control" name="seminar_end_time" value="<?php echo date("Y-m-d", strtotime($row->seminar_end_time));?>"/>
                   										<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                   										</span>
               									</div>
            								</div>
            							</div>
    									</div>
    									<div class="form-group">
    										<label for="jobs_category_id">活動分類</label>
                    						<select class="form-control" id="seminar_type" name="seminar_type">
                    							<option value="0" <? if($row->seminar_name=='0') echo 'selected="selected"'; ?>>選擇分類</option>
                    							<option value="1" <? if($row->seminar_name=='1') echo 'selected="selected"'; ?>>研討會</option>
                    							<option value="2" <? if($row->seminar_name=='2') echo 'selected="selected"'; ?>>競賽</option>
                    							<option value="3" <? if($row->seminar_name=='3') echo 'selected="selected"'; ?>>展覽</option>
                    							<option value="4" <? if($row->seminar_name=='4') echo 'selected="selected"'; ?>>其他</option>
                    						</select>
                    					</div>
    									<div class="form-group">
    										<label for="jobs_category_id">依職務分類</label>
    										<div>
    											<?foreach($listJobsCategory as $do){?>
    											<label class="checkbox-inline">
    												<input type="checkbox" name="jobs_category_id[]" value="<?php echo $do->jobs_category_id;?>"<?php echo (strpos($row->jobs_category_id,$do->jobs_category_id) !== false)?'checked':''?>><?php echo $do->jobs_category_name;?>
    											</label>
    											<?}?>
											</div>
                    					</div>
        								<p>	
        									<input type="hidden" name="seminar_id" value="<?php echo $row->seminar_id;?>" />
        									<button type="submit" class="btn btn-info">儲存</button>
                            				<button type="button" class="btn btn-danger cancle-edit-seminar">取消</button>
        								</p>
        								</form>
      								</div>
    							</div>
                    		</div>
						<?}
					}?>
                </div>
            </div>
		</div>
	</div>
</div><!-- /container -->