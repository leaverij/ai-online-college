<!-- 左邊區塊 -->
<?php $this->load->view('backend/sidebar'); ?>
<!-- 左邊end -->
		<style>
        .tour-start{margin-bottom:20px;padding:15px; background: #f5f5f5; -webkit-border-radius:4px; -moz-border-radius:4px; -ms-border-radius:4px; border-radius:4px;}
        .tour-start h4{margin-top:0;}
		.tour-start span{color:#999999;}
.tt-hint {
  color: #999
}

.tt-dropdown-menu {
  width: 422px;
  margin-top: 12px;
  padding: 8px 0;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-border-radius: 8px;
     -moz-border-radius: 8px;
          border-radius: 8px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
     -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
          box-shadow: 0 5px 10px rgba(0,0,0,.2);
}

.tt-suggestion {
  padding: 3px 20px;
  font-size: 18px;
  line-height: 24px;
}

.tt-suggestion.tt-cursor {
  color: #fff;
  background-color: #0097cf;
}

.tt-suggestion p {
  margin: 0;
}

        </style>
  		<div class="col-sm-9">
        	<div>	
                <div class="row">
                	<div class="col-sm-12 add-plus-industry">
    					<div class="thumbnail">
      						<div class="caption">
      							<button type="button" class="btn btn-success add-industry-btn" style="width: 100%;height: 50px">
      								<i class="fa fa-plus fa-3x"></i>
      							</button>
      						</div>
    					</div>
                    </div>
                    <div class="col-sm-12 add-industry" style="display: none">
    					<div class="thumbnail">
      						<div class="caption">
     							<form role="form" action="<?php echo base_url()?>backend/addIndustry" method="post" enctype="multipart/form-data">
      								<div class="form-group">
    									<label for="industry_title">趨勢應用標題</label>
    									<input type="text" class="form-control" id="industry_title" name="industry_title" placeholder="輸入趨勢應用標題">
    								</div>
    								<div class="form-group">
    									<label for="industry_url">趨勢應用網址</label>
    									<input type="text" class="form-control" id="industry_url" name="industry_url" placeholder="輸入趨勢應用網址">
    								</div>
    								<div class="form-group">
    									<label for="industry_source">趨勢應用出處</label>
    									<input type="text" class="form-control typeahead" id="industry_source" name="industry_source" placeholder="輸入趨勢應用出處">
    								</div>
    								<div class="form-group">
    									<label for="industry_origin_date">原資源日期</label>
    									<div class="container">
       										<div class='col-md-5'>
                								<div class='input-group date industry_origin_date' data-date-format="YYYY-MM-DD">
                    								<input type='text' class="form-control" name="industry_origin_date"/>
                    								<span class="input-group-addon">
                    									<span class="glyphicon glyphicon-calendar"></span>
                    								</span>
                								</div>
                							</div>
            							</div>
    								</div>
    								<div class="form-group">
    									<label for="jobs_category_id">依職務分類</label>
    									<div>
    										<?foreach($listJobsCategory as $do){?>
    										<label class="checkbox-inline">
    											<input type="checkbox" name="jobs_category_id[]" value="<?php echo $do->jobs_category_id;?>"><?php echo $do->jobs_category_name;?>
    										</label>
    										<?}?>
										</div>
                    					<!-- <select class="form-control" id="domain_id" name="domain_id">
                    						<option value="0">選擇職務</option>
                    						<?foreach($listJobsCategory as $do){?>
                    						<option value="<?php echo $do->jobs_category_id;?>"><?php echo $do->jobs_category_name;?></option>
                    						<?}?>
                    					</select> -->
                    				</div>
        							<p>	
        								<button type="submit" class="btn btn-info">新增</button>
                           				<button type="button" class="btn btn-danger cancle-add-industry">取消</button>
        							</p>
        							</form>
      							</div>
    						</div>
                   		</div>
                	<?if ($listAllIndustry->num_rows() > 0){?>
                		<div class="col-sm-12" style="margin-bottom: 10px">
							<div class="row">
                    			<div class="col-md-1">點擊數</div>
                    			<div class="col-md-5">標題[資料出處]</div>
                    			<div class="col-md-2">原資源日期</div>
                    			<div class="col-md-1">建立者</div>
                    			<div class="col-md-1">分類</div>
                    			<div class="col-md-1">編輯</div>
                    			<div class="col-md-1">刪除</div>
							</div>
						</div>
                		<?foreach ($listAllIndustry->result() as $row){?>
                			<div class="col-sm-12 industry" style="margin-bottom: 10px">
                				<div class="row">
                					<div class="col-md-1"><?echo $row->clicks;?></div>
                					<div class="col-md-5"><?echo $row->industry_title;?> <?php echo ($row->industry_source)?'[出自'. $row->industry_source.']':''?></div>
                					<div class="col-md-2"><?echo date("Y-m-d", strtotime($row->industry_origin_date));?></div>
                					<div class="col-md-1"><?echo $row->name;?></div>
                					<div class="col-md-1"><?echo $row->jobs_category_id;?>
                						<!-- <?foreach($listJobsCategory as $do){?>
                							<?if($do->jobs_category_id==$row->jobs_category_id) echo $do->jobs_category_name;?>
                						<?}?> -->
                					</div>
                					<div class="col-md-1"><button type="button" class="btn btn-success btn-sm edit-industry-btn pull-right">編輯</button></div>
                					<div class="col-md-1"><button type="button" class="btn btn-danger btn-sm del-industry pull-right" data-industry-id="<?php echo $row->industry_id?>">刪除</button></div>
                				</div>
                    		</div>
                    		<div class="col-sm-12 edit-industry" style="display: none">
    							<div class="thumbnail">
      								<div class="caption">
      									<form role="form" action="<?php echo base_url()?>backend/updateIndustry" method="post" enctype="multipart/form-data">
      									<div class="form-group">
    										<label for="industry_title">趨勢應用標題</label>
    										<input type="text" class="form-control" id="industry_title" name="industry_title" placeholder="輸入趨勢應用標題" value="<?echo $row->industry_title;?>">
    									</div>
    									<div class="form-group">
    										<label for="industry_url">趨勢應用網址</label>
    										<input type="text" class="form-control" id="industry_url" name="industry_url" placeholder="輸入趨勢應用網址" value="<?echo $row->industry_url;?>">
    									</div>
    									<div class="form-group">
    										<label for="industry_source">趨勢應用出處</label>
    										<input type="text" class="form-control typeahead" id="industry_source" name="industry_source" placeholder="輸入趨勢應用出處" value="<?echo $row->industry_source;?>">
    									</div>
    									<div class="form-group">
    										<label for="industry_origin_date">原資源日期</label>
    										<div class="container">
       											<div class='col-md-5'>
                									<div class='input-group industry_origin_date' id='' data-date-format="YYYY-MM-DD">
                    									<input type='text' class="form-control" name="industry_origin_date" value=" <?php echo date("Y-m-d", strtotime($row->industry_origin_date));?>"/>
                    									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                   										</span>
                									</div>
               									</div>
            								</div>
    									</div>
    									<div class="form-group">
    										<label for="jobs_category_id">依職務分類</label>
    										<div>
    											<?foreach($listJobsCategory as $do){?>
    											<label class="checkbox-inline">
    												<input type="checkbox" name="jobs_category_id[]" value="<?php echo $do->jobs_category_id;?>"<?php echo (strpos($row->jobs_category_id,$do->jobs_category_id) !== false)?'checked':''?>><?php echo $do->jobs_category_name;?>
    											</label>
    											<?}?>
											</div>
                    					</div>
        								<p>	
        									<input type="hidden" name="industry_id" value="<?php echo $row->industry_id;?>" />
        									<button type="submit" class="btn btn-info">儲存</button>
                            				<button type="button" class="btn btn-danger cancle-edit-industry">取消</button>
        								</p>
        								</form>
      								</div>
    							</div>
                    		</div>
						<?}
					}?>
                </div>
            </div>
		</div>
	</div>
</div><!-- /container -->