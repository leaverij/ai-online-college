<!-- 左邊區塊 -->
<?php $this->load->view('backend/sidebar'); ?>
<!-- 左邊end -->
		<style>
        .tour-start{margin-bottom:20px;padding:15px; background: #f5f5f5; -webkit-border-radius:4px; -moz-border-radius:4px; -ms-border-radius:4px; border-radius:4px;}
        .tour-start h4{margin-top:0;}
		.tour-start span{color:#999999;}
        </style>
  		<div class="col-sm-9">
        	<div>	
                <div class="row">
                	<div class="col-sm-12 add-plus-jobs">
    					<div class="thumbnail">
      						<div class="caption">
      							<button type="button" class="btn btn-success add-jobs-btn" style="width: 100%;height: 50px">
      								<i class="fa fa-plus fa-3x"></i>
      							</button>
      						</div>
    					</div>
                    </div>
                    <div class="col-sm-12 add-jobs" style="display: none">
    					<div class="thumbnail">
      						<div class="caption">
     							<form role="form" action="<?php echo base_url()?>backend/addJobs" method="post" enctype="multipart/form-data">
      								<div class="form-group">
    									<label for="comp_name">企業名稱</label>
    									<input type="text" class="form-control" id="comp_name" name="comp_name" placeholder="輸入徵才企業名稱">
    								</div>
      								<div class="form-group">
    									<label for="jobs_title">職缺名稱</label>
    									<input type="text" class="form-control" id="jobs_title" name="jobs_title" placeholder="輸入企業徵才標題">
    								</div>
    								<div class="form-group">
    									<label for="jobs_url">企業徵才網址</label>
    									<input type="text" class="form-control" id="jobs_url" name="jobs_url" placeholder="輸入企業徵才網址">
    								</div>
    								<div class="form-group">
    									<label for="jobs_category_id">依職務分類</label>
    									<div>
    										<?foreach($listJobsCategory as $do){?>
    										<label class="checkbox-inline">
    											<input type="checkbox" name="jobs_category_id[]" value="<?php echo $do->jobs_category_id;?>"><?php echo $do->jobs_category_name;?>
    										</label>
    										<?}?>
										</div>
                    				</div>
        							<p>	
        								<button type="submit" class="btn btn-info">新增</button>
                           				<button type="button" class="btn btn-danger cancle-add-jobs">取消</button>
        							</p>
        							</form>
      							</div>
    						</div>
                   		</div>
                	<?if ($listAllJobs->num_rows() > 0){?>
                		<div class="col-sm-12" style="margin-bottom: 10px">
							<div class="row">
                    			<div class="col-md-1">點擊數</div>
                    			<div class="col-md-5">徵才企業及職缺</div>
                    			<div class="col-md-2">建立日期</div>
                    			<div class="col-md-1">建立者</div>
                    			<div class="col-md-1">分類</div>
                    			<div class="col-md-1">編輯</div>
                    			<div class="col-md-1">刪除</div>
							</div>
						</div>
                		<?foreach ($listAllJobs->result() as $row){?>
                			<div class="col-sm-12 jobs" style="margin-bottom: 10px">
                				<div class="row">
                					<div class="col-md-1"><?echo $row->clicks;?></div>
                					<div class="col-md-5"><p>企業：<?echo $row->comp_name;?></p><p>職缺：<?echo $row->jobs_title;?></p></div>
                					<div class="col-md-2"><?echo date("Y-m-d", strtotime($row->jobs_post_date));?></div>
                					<div class="col-md-1"><?echo $row->name;?></div>
                					<div class="col-md-1"><?echo $row->jobs_category_id;?>
                						<!-- <?foreach($listJobsCategory as $do){?>
                							<?if($do->jobs_category_id==$row->jobs_category_id) echo $do->jobs_category_name;?>
                						<?}?> -->
                					</div>
                					<div class="col-md-1"><button type="button" class="btn btn-success btn-sm edit-jobs-btn pull-right">編輯</button></div>
                					<div class="col-md-1"><button type="button" class="btn btn-danger btn-sm del-jobs pull-right" data-jobs-id="<?php echo $row->jobs_id?>">刪除</button></div>
                				</div>
                    		</div>
                    		<div class="col-sm-12 edit-jobs" style="display: none">
    							<div class="thumbnail">
      								<div class="caption">
      									<form role="form" action="<?php echo base_url()?>backend/updateJobs" method="post" enctype="multipart/form-data">
    									<div class="form-group">
    										<label for="comp_name">企業名稱</label>
    										<input type="text" class="form-control" id="comp_name" name="comp_name" placeholder="輸入徵才企業名稱" value="<?echo $row->comp_name;?>">
    									</div>
    									<div class="form-group">
    										<label for="jobs_title">職缺名稱</label>
    										<input type="text" class="form-control" id="jobs_title" name="jobs_title" placeholder="輸入企業徵才標題" value="<?echo $row->jobs_title;?>">
    									</div>
    									<div class="form-group">
    										<label for="jobs_url">企業徵才網址</label>
    										<input type="text" class="form-control" id="jobs_url" name="jobs_url" placeholder="輸入企業徵才網址" value="<?echo $row->jobs_url;?>">
    									</div>
    									<div class="form-group">
    										<label for="jobs_category_id">依職務分類</label>
    										<div>
    											<?foreach($listJobsCategory as $do){?>
    											<label class="checkbox-inline">
    												<input type="checkbox" name="jobs_category_id[]" value="<?php echo $do->jobs_category_id;?>"<?php echo (strpos($row->jobs_category_id,$do->jobs_category_id) !== false)?'checked':''?>><?php echo $do->jobs_category_name;?>
    											</label>
    											<?}?>
											</div>
                    					</div>
        								<p>	
        									<input type="hidden" name="jobs_id" value="<?php echo $row->jobs_id;?>" />
        									<button type="submit" class="btn btn-info">儲存</button>
                            				<button type="button" class="btn btn-danger cancle-edit-jobs">取消</button>
        								</p>
        								</form>
      								</div>
    							</div>
                    		</div>
						<?}
					}?>
                </div>
            </div>
		</div>
	</div>
</div><!-- /container -->