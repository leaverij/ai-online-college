<!-- 左邊區塊 -->
<?php $this->load->view('backend/sidebar'); ?>
<!-- 左邊end -->
		<style>
        .tour-start{margin-bottom:20px;padding:15px; background: #f5f5f5; -webkit-border-radius:4px; -moz-border-radius:4px; -ms-border-radius:4px; border-radius:4px;}
        .tour-start h4{margin-top:0;}
		.tour-start span{color:#999999;}
		.tt-hint {
  color: #999
}

.tt-dropdown-menu {
  width: 422px;
  margin-top: 12px;
  padding: 8px 0;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0, 0, 0, 0.2);
  -webkit-border-radius: 8px;
     -moz-border-radius: 8px;
          border-radius: 8px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
     -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
          box-shadow: 0 5px 10px rgba(0,0,0,.2);
}

.tt-suggestion {
  padding: 3px 20px;
  font-size: 18px;
  line-height: 24px;
}

.tt-suggestion.tt-cursor {
  color: #fff;
  background-color: #0097cf;
}

.tt-suggestion p {
  margin: 0;
}
        </style>
  		<div class="col-sm-9">
        	<div>	
                <div class="row">
                	<div class="col-sm-12 add-plus-resource">
    					<div class="thumbnail">
      						<div class="caption">
      							<button type="button" class="btn btn-success add-resource-btn" style="width: 100%;height: 50px">
      								<i class="fa fa-plus fa-3x"></i>
      							</button>
      						</div>
    					</div>
                    </div>
                    <div class="col-sm-12 add-resource" style="display: none">
    					<div class="thumbnail">
      						<div class="caption">
     							<form role="form" action="<?php echo base_url()?>backend/addResource" method="post" enctype="multipart/form-data">
      								<div class="form-group">
    									<label for="resource_title">國內外資源標題</label>
    									<input type="text" class="form-control" id="resource_title" name="resource_title" placeholder="輸入國內外資源標題">
    								</div>
    								<div class="form-group">
    									<label for="resource_url">國內外資源網址</label>
    									<input type="text" class="form-control" id="resource_url" name="resource_url" placeholder="輸入國內外資源網址">
    								</div>
    								<div class="form-group">
    									<label for="resource_source">國內外資源出處</label>
    									<input type="text" class="form-control typeahead" id="resource_source" name="resource_source" placeholder="輸入國內外資源出處">
    								</div>
    								<div class="form-group">
    									<label for="resource_origin_date">原資源日期</label>
    									<div class="container">
       										<div class='col-md-5'>
                								<div class='input-group date resource_origin_date' id='' data-date-format="YYYY-MM-DD">
                    								<input type='text' class="form-control" name="resource_origin_date"/>
                    									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    								</span>
                								</div>
                							</div>
            							</div>
    								</div>
    								<div class="form-group">
    										<label for="resource_type">資源類型</label>
                    						<select class="form-control" id="resource_type" name="resource_type">
                    							<option value="0">選擇類型</option>
                    							<option value="1">影片</option>
                    							<option value="2">文件</option>
                    							<option value="3">文章</option>
                    							<option value="4">其他</option>
                    						</select>
                    					</div>
    								<div class="form-group">
    									<label for="jobs_category_id">依職務分類</label>
    									<div>
    										<?foreach($listJobsCategory as $do){?>
    										<label class="checkbox-inline">
    											<input type="checkbox" name="jobs_category_id[]" value="<?php echo $do->jobs_category_id;?>"><?php echo $do->jobs_category_name;?>
    										</label>
    										<?}?>
										</div>
                    				</div>
        							<p>	
        								<button type="submit" class="btn btn-info">新增</button>
                           				<button type="button" class="btn btn-danger cancle-add-resource">取消</button>
        							</p>
        							</form>
      							</div>
    						</div>
                   		</div>
                	<?if ($listAllResource->num_rows() > 0){?>
                		<div class="col-sm-12" style="margin-bottom: 10px">
							<div class="row">
                    			<div class="col-md-1">點擊數</div>
                    			<div class="col-md-1">資源類型</div>
                    			<div class="col-md-4">標題[資料出處]</div>
                    			<div class="col-md-2">原資源日期</div>
                    			<div class="col-md-1">建立者</div>
                    			<div class="col-md-1">分類</div>
                    			<div class="col-md-1">編輯</div>
                    			<div class="col-md-1">刪除</div>
							</div>
						</div>
                		<?foreach ($listAllResource->result() as $row){?>
                			<div class="col-sm-12 resource" style="margin-bottom: 10px">
                				<div class="row">
                					<div class="col-md-1"><?echo $row->clicks;?></div>
                					<div class="col-md-1">
                						<span class="label <?echo $row->label_name;?>">
                							<?switch ($row->resource_type){	case "1":echo "影片";break;
                															case "2":echo "文件";break;
                															case "3":echo "文章";break;
                															case "4":echo "其他";break;
                															default:echo "其他";}?>
                						</span>
                					</div>
                					<div class="col-md-4"><?echo $row->resource_title;?> <?php echo ($row->resource_source)?'[出自'. $row->resource_source.']':''?></div>
                					<div class="col-md-2"><?echo date("Y-m-d", strtotime($row->resource_origin_date));?></div>
                					<div class="col-md-1"><?echo $row->name;?></div>
                					<div class="col-md-1"><?echo $row->jobs_category_id;?>
                						<!-- <?foreach($listJobsCategory as $do){?>
                							<?if($do->jobs_category_id==$row->jobs_category_id) echo $do->jobs_category_name;?>
                						<?}?> -->
                					</div>
                					<div class="col-md-1"><button type="button" class="btn btn-success btn-sm edit-resource-btn pull-right">編輯</button></div>
                					<div class="col-md-1"><button type="button" class="btn btn-danger btn-sm del-resource pull-right" data-resource-id="<?php echo $row->resource_id?>">刪除</button></div>
                				</div>
                    		</div>
                    		<div class="col-sm-12 edit-resource" style="display: none">
    							<div class="thumbnail">
      								<div class="caption">
      									<form role="form" action="<?php echo base_url()?>backend/updateResource" method="post" enctype="multipart/form-data">
      									<div class="form-group">
    										<label for="resource_title">國內外資源標題</label>
    										<input type="text" class="form-control" id="resource_title" name="resource_title" placeholder="輸入國內外資源標題" value="<?echo $row->resource_title;?>">
    									</div>
    									<div class="form-group">
    										<label for="resource_url">國內外資源網址</label>
    										<input type="text" class="form-control" id="resource_url" name="resource_url" placeholder="輸入國內外資源網址" value="<?echo $row->resource_url;?>">
    									</div>
    									<div class="form-group">
    										<label for="resource_source">國內外資源出處</label>
    										<input type="text" class="form-control typeahead" id="resource_source" name="resource_source" placeholder="輸入國內外資源出處" value="<?echo $row->resource_source;?>">
    									</div>
    									<div class="form-group">
    										<label for="resource_origin_date">原資源日期</label>
    										<div class="container">
       											<div class='col-md-5'>
                									<div class='input-group resource_origin_date' id='' data-date-format="YYYY-MM-DD">
                    									<input type='text' class="form-control" name="resource_origin_date" value=" <?php echo date("Y-m-d", strtotime($row->resource_origin_date));?>"/>
                    									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                   										</span>
                									</div>
               									</div>
            								</div>
    									</div>
    									<div class="form-group">
    										<label for="resource_type">資源類型</label>
                    						<select class="form-control" id="resource_type" name="resource_type">
                    							<option value="0" <? if($row->resource_type=='0') echo 'selected="selected"'; ?>>選擇分類</option>
                    							<option value="1" <? if($row->resource_type=='1') echo 'selected="selected"'; ?>>影片</option>
                    							<option value="2" <? if($row->resource_type=='2') echo 'selected="selected"'; ?>>文件</option>
                    							<option value="3" <? if($row->resource_type=='3') echo 'selected="selected"'; ?>>文章</option>
                    							<option value="4" <? if($row->resource_type=='4') echo 'selected="selected"'; ?>>其他</option>
                    						</select>
                    					</div>
    									<div class="form-group">
    										<label for="jobs_category_id">依職務分類</label>
    										<div>
    											<?foreach($listJobsCategory as $do){?>
    											<label class="checkbox-inline">
    												<input type="checkbox" name="jobs_category_id[]" value="<?php echo $do->jobs_category_id;?>"<?php echo (strpos($row->jobs_category_id,$do->jobs_category_id) !== false)?'checked':''?>><?php echo $do->jobs_category_name;?>
    											</label>
    											<?}?>
											</div>
                    					</div>
        								<p>	
        									<input type="hidden" name="resource_id" value="<?php echo $row->resource_id;?>" />
        									<button type="submit" class="btn btn-info">儲存</button>
                            				<button type="button" class="btn btn-danger cancle-edit-resource">取消</button>
        								</p>
        								</form>
      								</div>
    							</div>
                    		</div>
						<?}
					}?>
                </div>
            </div>
		</div>
	</div>
</div><!-- /container -->