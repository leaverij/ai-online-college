<style>
.search-bar{position:relative;}
.search-bar .search-text {display:block; width:100%; padding:15px; color:#666; font-size:14px; background:#f9f9f9; border:none; border-bottom:1px solid #cfe4e5; border-radius:5px 5px 0 0;}
.search-bar ::-webkit-input-placeholder {color:#bcbcbc;/* WebKit browsers */}
.search-bar .search-text:focus::-webkit-input-placeholder {color:#e3e3e3; transition:ease 0.1s; -webkit-transition:ease 0.1s; -moz-transition:ease 0.1s; -ms-transition:ease 0.1s;}
.search-bar :-moz-placeholder {color:#bcbcbc;/* Mozilla Firefox 4 to 18 */}
.search-bar ::-moz-placeholder  {color:#bcbcbc;/* Mozilla Firefox 19+ */}
.search-bar :-ms-input-placeholder {color:#bcbcbc;/* Internet Explorer 10+ */}
.search-bar .search-btn{position:absolute; top:0; right:0; bottom:0; border:none; padding:0 15px; background:none;}
.search-bar .search-btn span{font-size:16px; color:#bcbcbc;}

.sidebar #more-sidebar {margin-bottom:30px; background:#f5f5f5; border-radius:5px; box-shadow:0 1px 0 0 #ededed;}
#more-sidebar ul li h4 {font-size:16px; margin:0;}
#more-sidebar ul li h4 a:hover, #more-sidebar ul li.active a{text-decoration:none; background:#eeeeee; color:#24bbc9;}
#more-sidebar ul li a {color:#666; font-size:15px; border-bottom:1px solid #e5e5e5;}
#more-sidebar ul li:first-child a{border-radius:5px 5px 0 0;}
#more-sidebar ul li:last-child a {border-radius:0 0 5px 5px; border-bottom:none;}
#more-sidebar ul li a:hover {color:#24bbc9;}
#more-sidebar .library-list li a {color:#888;}
#more-sidebar .nav-stacked > li + li {margin-top:0;}

.sorting.btn-group .btn {color:#fff; font-size:13px; background:#56606e; margin-right:2px;}
.sorting.btn-group .btn:hover {background:#778599;}
.sorting.btn-group .btn .caret {border-top:4px solid #fff;}
@media screen and (max-width: 768px) {
	.sidebar {padding:0;}
	.sidebar .navbar-toggle {display:block; margin:0; padding:10px 12px; cursor:pointer; color:#666; font-size:16px;}
}
</style>
<div class="sidebar">
	<form class="search-bar" action="" method="post" role="search" style="display: none">
		<div class="">
			<input class="search-text" type="text" name="search_string" placeholder="搜尋">
			<button type="submit" class="search-btn">
				<span class="glyphicon glyphicon-search"></span>
			</button>
		</div>
	</form>
	<div id="more-sidebar" style="padding:0;">
		<ul class="nav nav-stacked">
			<li data-more="seminar" class="moreTab">
				<a href="<?php echo base_url()?>track/seminar/<?php echo $jobs_category_url?>"> <h4>相關活動</h4> </a>
			</li>
			<li data-more="industry" class="moreTab">
				<a href="<?php echo base_url()?>track/industry/<?php echo $jobs_category_url?>"> <h4>趨勢應用</h4> </a>
			</li>
			<li data-more="resource" class="moreTab">
				<a href="<?php echo base_url()?>track/resource/<?php echo $jobs_category_url?>"> <h4>國內外資源</h4> </a>
			</li>
			<li data-more="jobs" class="moreTab">
				<a href="<?php echo base_url()?>track/jobs/<?php echo $jobs_category_url?>"> <h4>企業徵才</h4> </a>
			</li>
		</ul>
	</div>
</div>