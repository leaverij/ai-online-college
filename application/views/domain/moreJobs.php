<section class="domain-hero <?=$domain_url?>" style="min-height:250px; text-align:center;">
	<div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="hero-content" style="position:absolute; top:85px; right:0; left:0; text-align:center;">
                    <h1 style="font-size:2.3em; margin-top:0; letter-spacing:0.05em; color:#fff;"><?=$domain_name?></h1>
                    <p style="font-size:1.14em; color:#e9f3f7;">邁向職場成功之路</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
	<div class="container" style="padding:30px 0;">
    	<div class="row">
        	<div class="col-sm-3">
                <!-- sideBar -->
				<?php $this->load->view('domain/sidebar'); ?>
            </div>
            <div class="col-sm-9">
                <div class="tab-content">
                  <div class="tab-tittle" style="overflow:hidden;">
                  	<h4 id="more-title" style="font-size:1.5em; margin-bottom:0; float:left;">企業徵才</h4>
                  	<form class="search-bar" action="<?echo base_url()?>college/morejobs/<?php echo $domain_url;?>" method="post" role="search">
                    <div class="input-group input-group-sm row col-sm-6 pull-right">
                      <input type="text" class="form-control" name="search_string" value="<?php echo (isset($search_string))?$search_string:'';?>"/>
                      <div class="input-group-btn">
                          <button type="submit" class="btn btn-default search-btn" tabindex="-1">
                              <span class="glyphicon glyphicon-search"></span>
                          </button>                     
                      </div>
                    </div>
                    <input type="hidden" name="action" value="search"/>
                    </form>  
                    <!-- <div class="pull-right" style="display: none">
                        <div class="btn-group sorting">
                        	<a href="<?php echo base_url()?>college/morejobs/<?php echo $domain_url?>" class="btn" role="button">依最新資訊</a>
                        	<a href="#" class="btn" role="button">依歷史資料</a>            
                        </div>
                    </div> -->                   
                  </div>
                  <!-- tab_相關活動 -->
                  <!-- tab_趨勢應用 -->
                  <!-- tab_國內外資源 -->
                  <!-- tab_企業徵才 -->
                  <div id="jobs" style="padding-top:20px;">
                  	<?if (isset($search_string) && $search_string){?>
                  		<h4 style="color:#a7a7a7; font-size:14px; margin-top:0;" class="">搜尋結果如下：<?echo $search_string;?>(共<?echo count($listAllJobsByDomain->result())?>筆結果)</h4>
                    <?}?>
                      <ul class="" style="padding:0; list-style:none;">
                          <? if ($listAllJobsByDomain->num_rows() > 0){?>
                              <? foreach($listAllJobsByDomain->result() as $row){?>
                              	  <li style="padding:10px 0; border-bottom:1px solid #ddd">
                                  	<a class="record" data-type="jobs" data-id="<?php echo $row->jobs_id;?>" href="<?php echo $row->jobs_url;?>" target="_blank" style="text-decoration:none">
                                      <p style="color:#999; margin:0;">
                                          <strong style="color:#6f6f6f;">
                                              <?php echo date("Y.m.d", strtotime($row->jobs_post_date)) .' '.$row->comp_name?>
                                          </strong> 正在應徵 <strong style="color:#6f6f6f;"><?php echo $row->jobs_title?></strong>
                                      </p>
                                  </a>
                              </li>
                              <? } ?>
                          <? }?>
                      </ul>
                     <? if (!isset($search_string) && $last>1){?>
                      <? if ($listAllJobsByDomain->num_rows() > 0){?>
                      <ul class="pagination" style="">
                      	<li class=""><a href="?page=1"><i class="fa fa-angle-double-left"></i></a></li>
                        <!-- get 的page如果小於第三頁，設定page為3-->
                        <?$target = $page?>
                        <?if($page<3) $page=3;?>
                        <!-- get 的page如果大於總頁數，設定page-2-->
                        <?if($page+2>$last) $page=$last-2;?>
                        <? $index=1?>
                        <?for($i=$page-2;$i<$page+3;$i++){?>
                        	<?if($index=='1'){?>
                        		<li class=""><a href="?page=<?php echo ($page<=3)?'1':$i-1?>"><i class="fa fa-angle-left"></i></a></li>
                        	<?}?>
                        	<?if($i>'0'){?><!-- i==0 不顯示 -->
                        		<li class="<?php echo ($target==$i)?'active':''?>"><a href="?page=<?echo $i;?>"><?echo $i?></a></li>
                        	<?}?>
                        	<?if($index=='5'){?>
                        		<li class=""><a href="?page=<?php echo ($i==$last)?$last:$i+1?>"><i class="fa fa-angle-right"></i></a></li>
                        	<?}?>
                        	<? $index++;?>
                        <?}?>
                        <li class="">
                        	<a href="<?php echo ($last)?'?page='.$last:'javascript: void(0)'?>">
                        		<i class="fa fa-angle-double-right"></i>
                        	</a>
                        </li>
                      </ul>
                      <?}?>
                      <?}?>                    
                  </div>
                </div>            
            </div>
        </div>
    </div>
</section>