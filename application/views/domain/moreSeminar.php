<section class="domain-hero <?=$domain_url?>" style="min-height:250px; text-align:center;">
	<div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="hero-content" style="position:absolute; top:85px; right:0; left:0; text-align:center;">
                    <h1 style="font-size:2.3em; margin-top:0; letter-spacing:0.05em; color:#fff;"><?=$domain_name?></h1>
                    <p style="font-size:1.14em; color:#e9f3f7;">邁向職場成功之路</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
	<div class="container" style="padding:30px 0;">
    	<div class="row">
        	<div class="col-sm-3">
                <!-- sideBar -->
				<?php $this->load->view('domain/sidebar'); ?>
            </div>
            <div class="col-sm-9">
                <div class="tab-content">
                	<div class="tab-tittle">
                    	<form class="" action="<?echo base_url()?>college/moreseminar/<?php echo $domain_url;?>" method="post" role="search">
                            <input type="hidden" name="action" value="search"/>
                  			<h4 id="more-title" style="font-size:1.5em; margin-bottom:0; display:inline-block;">相關活動</h4>
                            <div class="input-group input-group-sm row col-sm-6 pull-right">
                                <input type="text" class="form-control" id="search_string" name="search_string" value="<?php echo (isset($search_string))?$search_string:'';?>"/>
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default search-btn" tabindex="-1">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <span id ="seminar"><?php echo $seminar_name;?></span><span class="caret"></span>
                                    </button>
                                    <ul id ="seminarUl" class="dropdown-menu">            	                	<li><a seminar-data-pjax href="<?php echo base_url()?>college/moreseminar/<?php echo $domain_url?>">依活動類型</a></li>
                                        <li><a seminar-data-pjax href="<?php echo base_url()?>college/moreseminar/<?php echo $domain_url?>/1">研討會</a></li>
                                        <li><a seminar-data-pjax href="<?php echo base_url()?>college/moreseminar/<?php echo $domain_url?>/2">競賽</a></li>
                                        <li><a seminar-data-pjax href="<?php echo base_url()?>college/moreseminar/<?php echo $domain_url?>/3">展覽</a></li>
                                        <li><a seminar-data-pjax href="<?php echo base_url()?>college/moreseminar/<?php echo $domain_url?>/4">其他</a></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                  </div>
                  <!-- tab_相關活動 -->
                  <div id="seminar-pjax-container" style="padding-top:20px;">
                  	<div>
                  		<?if (isset($search_string) && $search_string){?>
                  		<h4 style="color:#a7a7a7; font-size:14px; margin-top:0;">搜尋結果如下：<?echo $search_string;?>(共<?echo count($listAllSeminarByDomain->result())?>筆結果)</h4>
                  		<?}?>
                  	</div>
                  	<? if ($listAllSeminarByDomain->num_rows() > 0){?>
                      <div class="table-responsive">
                      	<table class="table">
                            <colgroup>
                                <col class="col-xs-1"><col class="col-xs-1"><col class="col-xs-5"><col class="col-xs-3"><col class="col-xs-3">
                            </colgroup>                          
                          	<thead>
                                <tr>
                                    <th>#</th>
                                    <th>類型</th>
                                    <th>活動名稱</th>
                                    <th>活動日期</th>
                                    <th>主辦單位</th>
                                </tr>
                            </thead>
                            <tbody>
                              <? foreach($listAllSeminarByDomain->result() as $key => $row){?>
                                <tr>
                                    <td><?php echo $key+1?></td>
                                    <td><?switch ($row->seminar_name){
                                    		case "1":echo "研討會";break;
                                    		case "2":echo "競賽";break;
                                    		case "3":echo "展覽";break;
                                    		case "4":echo "其他";break;
                                    		default:echo "其他";}?>
                                    </td>
                                    <td><a class="record" data-type="seminar" data-id="<?php echo $row->seminar_id;?>" style="display:block; text-decoration:none; white-space:nowrap; text-overflow:ellipsis; overflow:hidden;" data-toggle="modal" data-target="#seminar<?php echo $row->seminar_id;?>"><?php echo $row->seminar_title;?></a></td>
                                    <td><?if($row->seminar_start_time == $row->seminar_end_time){?>
                                    		<?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>
                                    	<?}else{?>
                                    		<?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>～<?php echo date("Y-m-d", strtotime($row->seminar_end_time));?>
                                    	<?}?></td>
                                    <td><?php echo $row->seminar_host;?></td>
                                </tr>
                                <div class="modal fade" id="seminar<?php echo $row->seminar_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    				<div class="modal-dialog">
                    					<div class="modal-content">
                    						<div class="modal-header">
                    							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    							<h4 class="modal-title" id="myModalLabel">
                    								<?switch ($row->seminar_name){
                    									case "1":echo "研討會";break;
                										case "2":echo "競賽";break;
                										case "3":echo "展覽";break;
                										case "4":echo "其他";break;
                										default:echo "其他";}?>資訊
                								</h4>
                    						</div>
                    						<div class="modal-body">
                    							<div class="form-horizontal">
                    								<div class="form-group">
                    									<label for="" class="col-sm-2 control-label">活動名稱</label>
                    									<div class="col-sm-10">
                    										<p class="form-control-static"><?php echo $row->seminar_title;?></p>
                    									</div>
                    								</div>
                    								<div class="form-group">
                    									<label for="" class="col-sm-2 control-label">主辦單位</label>
                    									<div class="col-sm-10">
                    										<p class="form-control-static"><?php echo $row->seminar_host;?></p>
                    									</div>
                    								</div>
                    								<div class="form-group">
                    									<label for="" class="col-sm-2 control-label">活動連結</label>
                    									<div class="col-sm-10">
                    										<p class="form-control-static"><a href="<?php echo $row->seminar_url;?>" target="_blank" class="modal_record" data-type="seminar" data-id="<?php echo $row->seminar_id;?>">連結</a></p>
                    									</div>
                    								</div>
                    								<div class="form-group">
                    									<label for="" class="col-sm-2 control-label">摘要</label>
                    									<div class="col-sm-10">
                    										<p class="form-control-static"><?php echo $row->seminar_content;?></p>
                    									</div>
                    								</div>
                    								<div class="form-group">
                    									<label for="" class="col-sm-2 control-label">時間</label>
                    									<div class="col-sm-10">
                    										<p class="form-control-static">
                    										<?if($row->seminar_start_time == $row->seminar_end_time){?>
                                    							時間 ： <?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>
                                    						<?}else{?>
                                    							時間 ： <?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>～<?php echo date("Y-m-d", strtotime($row->seminar_end_time));?>
                                    						<?}?>
                    										</p>
                    									</div>
                    								</div>
                    								<div class="form-group">
                    									<label for="" class="col-sm-2 control-label">地點</label>
                    									<div class="col-sm-10">
                    										<p class="form-control-static"><?php echo $row->seminar_place;?></p>
                    									</div>
                    								</div>
                    							</div>
                    						</div>
                    					</div><!-- /.modal-content -->
                    				</div><!-- /.modal-dialog -->
                    			</div><!-- /.modal -->
                               <? }?>
                            </tbody>
                          </table>
                      </div><!-- /.table-responsive -->
                      <? }?>
                      <? if (!isset($search_string) && $last>1){?>
                      <? if ($listAllSeminarByDomain->num_rows() > 0){?>
                      <ul class="pagination" style="">
                      	<li class=""><a href="?page=1"><i class="fa fa-angle-double-left"></i></a></li>
                        <!-- get 的page如果小於第三頁，設定page為3-->
                        <?$target = $page?>
                        <?if($page<3) $page=3;?>
                        <!-- get 的page如果大於總頁數，設定page-2-->
                        <?if($page+2>$last) $page=$last-2;?>
                        <? $index=1?>
                        <?for($i=$page-2;$i<$page+3;$i++){?>
                        	<?if($index=='1'){?>
                        		<li class=""><a href="?page=<?php echo ($page<=3)?'1':$i-1?>"><i class="fa fa-angle-left"></i></a></li>
                        	<?}?>
                        	<?if($i>'0'){?><!-- i==0 不顯示 -->
                        		<li class="<?php echo ($target==$i)?'active':''?>"><a href="?page=<?echo $i;?>"><?echo $i?></a></li>
                        	<?}?>
                        	<?if($index=='5'){?>
                        		<li class=""><a href="?page=<?php echo ($i==$last)?$last:$i+1?>"><i class="fa fa-angle-right"></i></a></li>
                        	<?}?>
                        	<? $index++;?>
                        <?}?>
                        <li class="">
                        	<a href="<?php echo ($last)?'?page='.$last:'javascript: void(0)'?>">
                        		<i class="fa fa-angle-double-right"></i>
                        	</a>
                        </li>
                      </ul>
                      <?}?>
                      <?}?>                      
                  </div>
                </div>            
            </div>
        </div>
    </div>
</section>