<style>
.track-card {background:#f5f5f5; position:relative; border-radius:5px; /*border:1px solid #ddd;*/ box-shadow:0 1px 0 #dedede;}
.track-card a {display:block; text-decoration:none;}
.track-card .content-meta {padding:30px 30px 60px; height:200px; position:relative;}
.track-card .content-meta h3 {color:#333; font-size:1.28em; margin:0 0 10px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;}
.track-card .content-meta p {color:#777; margin:10px 0;}
.track-card .content-meta ul {padding:0;}
.track-card .content-meta ul li {list-style:none; color:#777;}
.track-card .content-meta ul li > i {margin-right:5px; color:#aaa;}
.track-card.in-progress {background:#f5f5f5; border:none; box-shadow:0 1px 0 0 #dedede;}

.domain-nav {border-bottom:1px solid #ddd;}
.domain-nav ul {text-align:center;}
.domain-nav .nav-pills > li {float:none; display:inline-block;}

.resources-trends, .resources-activity,
.resources-HR, .resources-international {margin-bottom:30px;}

/*.resources-trends ul > li,
.resources-HR ul > li {list-style:disc;}*/
.resources-activity ul > li, .resources-trends ul > li,
.resources-HR ul > li, .resources-international ul > li,
.resources-discussion ul > li {list-style:none;}

.resources-activity ul {padding:0;}
.resources-activity ul li, .resources-international ul > li{padding:0 0 0 60px; margin-bottom:8px;}
.resources-activity ul li > h4 {margin:0 0 8px; font-size:1.14em;}
.resources-activity ul li > img {float:left; width:70px; height:70px; margin-left:-85px; border-radius:3px;}
.resources-activity .activity-meta p {font-size:12px; color:#777; margin:0 0 5px 0;}

.sorting.btn-group .btn, .sorting.input-group .btn {color:#fff; /*font-size:13px;*/ background:#56606e; margin-right:2px;}
.sorting.btn-group .btn:hover {background:#778599;}
.sorting.btn-group .btn .caret {border-top:4px solid #fff;}
</style>

<section class="domain-hero <?=$domain_url?>" style="min-height:250px; text-align:center;">
	<div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="hero-content" style="position:absolute; top:85px; right:0; left:0; text-align:center;">
                    <h1 style="font-size:2.3em; margin-top:0; letter-spacing:0.05em; color:#fff;"><?=$title?></h1>
                    <p style="font-size:1.14em; color:#e9f3f7;">邁向職場成功之路</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
	<!-- <div class="domain-nav">
        <div class="container">
            <ul class="nav nav-pills">
                <li>
                    <a href="#">專業學程</a>
                </li>
                <li style="display:none;">
                    <a href="#">學習資源</a>
                </li>
                <li>
                    <a href="#">討論區</a>
                </li>
                <li class="disabled" style="display:none;">
                    <a href="#">企業徵才</a>
                </li>
            </ul>
        </div>
    </div> -->
    <div class="container">
    	<h3 style="font-size:1.5em; display:inline-block; margin:20px 0;">專業學程</h3>
        <div class="pull-right" style="margin:20px 0;">
            <div class="btn-group btn-group-sm sorting">
                <a id ="allProficiency" college-data-pjax href="<?php echo base_url()?>college/domain/<?php echo $domain_url?>" class="btn" role="button">全部課程</a>
                <div class="btn-group btn-group-sm">
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                        <span id ="proficiency"><!-- 依學程深度分類 --><?php echo $proficiency?></span> <span class="caret"></span>
                    </button>
                    <ul id="proficiencyUl" class="dropdown-menu">
                        <li><a college-data-pjax href="<?php echo base_url()?>college/domain/<?php echo $domain_url?>/1">學習者</a></li>
                        <li><a college-data-pjax href="<?php echo base_url()?>college/domain/<?php echo $domain_url?>/2">練習者</a></li>
                        <li><a college-data-pjax href="<?php echo base_url()?>college/domain/<?php echo $domain_url?>/3">能力者</a></li>
                        <li><a college-data-pjax href="<?php echo base_url()?>college/domain/<?php echo $domain_url?>/4">專家</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--  TRACK LISTS / 學程清單  -->
    <div id="college-pjax-container" class="track-lists" style="">
    	<!-- <a college-data-pjax href="<?php echo base_url().'domain/mobile/1'?>">1</a>
    	<a college-data-pjax href="<?php echo base_url().'domain/mobile/2'?>">2</a> -->
        <div class="container">
            <div class="row">
                <?php $row = $getAllTracks->result()?>
                <?php $checkUserTrack = $getAllTracks->row();?>
                <?php $track_key = false;?>
                <?php if(!empty($checkUserTrack->email)){
                    $track_key = true;
                }?>
                <? foreach($row as $key =>$val){?>
                <div class="col-sm-4" style="margin-bottom:30px;">
                    <div class="track-card <?php echo ($track_key && $key=='0')?'in-progress':''?>">
                    	<a href="<?php echo base_url()?>track/trackContent/<?php echo $val->tracks_url?>">
                          <div class="content-meta" style="position:relative;">
                              <h3 style="margin:0;"><?php echo $val->tracks_name?></h3>
                              <div class="content-actions-container" style="position:absolute; bottom:30px; right:30px; left:30px;">
                                  <ul style="margin:0;">
                                  	<li>
                                    	<!-- <strong style="color:#666; font-size:1.5em;">58 </strong>位學習者 -->
                                    </li>
                                    <li></li>
                                  </ul>
                              </div>
                              <p>
                                  <?php echo $val->tracks_desc?>
                              </p>
                          </div>
                        </a>
                    </div>
                </div>
                <? }?>
            </div><!-- /row -->
        </div>
    </div>
</section>
<section class="resources" style="background:#f6f6f6; padding:30px 0;">
	<div class="container">
    	<div class="row">
        	<div class="col-sm-4">
            	<div class="content-discussion" style="background:#e5e5e5; border-radius:5px; padding:30px; margin-bottom:30px;">
                	<div class="content-discussion-container" style="padding-left:50px;">
                        <i class="fa fa-comments" style="float:left; margin-left:-50px; font-size:30px; color:#777;"></i>
                        <p style="color:#6f6f6f; margin-bottom:20px;">有不懂的地方? 可以在這裡與其他學習者一起進行社群討論喔。</p>    
                    </div>
                    <?php if($this->session->userdata('email')){?>
                    	<a data-toggle="modal" data-target="#modal-start" data-keyboard="false" data-backdrop="static" class="btn tin-can-ask" style="display:block; background:#299ba3; color:#fff; font-size:1.14em; letter-spacing:0.1em;">我要發問</a>
                    <?}else{?>
                    	<a href="<?php echo base_url()?>forum"class="btn" style="display:block; background:#299ba3; color:#fff; font-size:1.14em; letter-spacing:0.1em;">討論區</a>
                    <?}?>
                </div>
            </div>
            <style>
			#myTab.nav > li > a, #myTab.nav-pills > li > a {color:#333; padding:0 0 3px; border-radius:0;}
			#myTab.nav > li > a:hover, #myTab.nav > li > a:focus {border-bottom:2px solid #299ba3; background:none;}
            #myTab.nav-pills > li.active > a, #myTab.nav-pills > li.active > a:hover, #myTab.nav-pills > li.active > a:focus {color:#333; background:none; border-bottom:2px solid #299ba3;}
            </style>
        	<div class="col-sm-4">
            	<div class="resources-discussion" style="margin-bottom:30px;">
					<!-- Nav tabs -->
					<ul id="myTab"class="nav nav-pills" style="margin-bottom:10px;">
  						<li class="active" style="">
                        	<a href="#home" data-toggle="pill" style="">
                            	<h3 style="font-size:1.5em; margin-top:0px;margin-bottom:0px;">熱門討論</h3>
                            </a>
                        </li>
                        <li style="color:#666; font-size:1.5em; line-height:1em; margin:0 15px 0 20px;"> | </li>
  						<li>
                        	<a href="#profile" data-toggle="pill"  style="">
                            	<h3 style="font-size:1.5em; margin-top:0px;margin-bottom:0px;">最新討論</h3>
                            </a>
                        </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
  						<div class="tab-pane active" id="home">
  							<div id="HotDis">
                    			<ul class="" style="padding:0; list-style:none;">
                    			<? if ($listPopularDiscussions->num_rows() > 0){?>
                    				<? foreach($listPopularDiscussions->result() as $row){?>
                    				<li style="margin:0 0 6px;">
                        				<h4 style="font-size:14px; margin:0"><a href="<?php echo base_url()?>forum/forumContent/<?php echo $row->forum_topic_id?>"><?php echo $row->forum_topic;?></a></h4>
                            			<p style="font-size:12px; color:#999; margin:0;">
                                			<i class="fa fa-comment" style="font-size:12px;"></i> <?php echo ($row->count-1)?> ‧ 經由 <strong style="color:#6f6f6f;"><?php echo $row->name;?></strong> 提問
                            			</p>
                        			</li>
                        			<?}?>
                        		<?}?>
                    			</ul>
                    		</div>
  						</div>
  						<div class="tab-pane" id="profile">
  							<div id="NewDis">
                    			<ul class="" style="padding:0; list-style:none;">
                    			<? if ($listNewDiscussions->num_rows() > 0){?>
                    				<? foreach($listNewDiscussions->result() as $row){?>
                    				<li style="margin:0 0 6px;">
                        				<h4 style="font-size:14px; margin:0;"><a href="<?php echo base_url()?>forum/forumContent/<?php echo $row->forum_topic_id?>"><?php echo $row->forum_topic;?></a></h4>
                            			<p style="font-size:12px; color:#999; margin:0;">
                                			<i class="fa fa-comment" style="font-size:12px;"></i> <?php echo ($row->count-1)?> ‧ 經由 <strong style="color:#6f6f6f;"><?php echo $row->name;?></strong> 提問
                            			</p>
                        			</li>
                        			<?}?>
                        		<?}?>
                    			</ul>
                    		</div>
  						</div>
  					</div>
                    <p style="font-size:12px; font-weight:bold;">
                    	<a href="<?php echo base_url()?>forum" style="color:#2a6496;">
                        	更多熱門討論 <i class="fa fa-angle-double-right"></i>
                        </a>
                    </p>
                </div>
            </div>
        	<div class="col-sm-4">
            	<div class="resources-activity">
                    <h3 style="font-size:1.5em; margin-top:0px;">相關活動</h3> 
                    <ul>
                    	<? if ($listSeminarByDomain->num_rows() > 0){?>
                    		<? foreach($listSeminarByDomain->result() as $row){?>
                    			<li>
                                	<span style="float:left; line-height:1.3em; margin-left:-60px; width:50px;" class="label <?php echo $row->label_name;?>">
										<?switch ($row->seminar_name){	case "1":echo "研討會";break;
                														case "2":echo "競賽";break;
                														case "3":echo "展覽";break;
                														case "4":echo "其他";break;
                														default:echo "其他";}?>
                                    </span>
                    				<a class="record" data-type="seminar" data-id="<?php echo $row->seminar_id;?>" style="display:block; text-decoration:none; white-space:nowrap; text-overflow:ellipsis; overflow:hidden;" data-toggle="modal" data-target="#seminar<?php echo $row->seminar_id;?>">
                    					 <?php echo $row->seminar_title;?>
                    				</a>
                                    <p style="margin:0; font-size:12px; color:#999;">
                                    	<?if($row->seminar_start_time == $row->seminar_end_time){?>
                                    		時間 ： <?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>
                                    	<?}else{?>
                                    		時間 ： <?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>～<?php echo date("Y-m-d", strtotime($row->seminar_end_time));?>
                                    	<?}?>
                                    </p>
                    			</li>
                    			<!-- Modal -->
                    			<div class="modal fade" id="seminar<?php echo $row->seminar_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    				<div class="modal-dialog">
                    					<div class="modal-content">
                    						<div class="modal-header">
                    							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    							<h4 class="modal-title" id="myModalLabel">
                    								<?switch ($row->seminar_name){
                    									case "1":echo "研討會";break;
                										case "2":echo "競賽";break;
                										case "3":echo "展覽";break;
                										case "4":echo "其他";break;
                										default:echo "其他";}?>資訊</h4>
                    						</div>
                    						<div class="modal-body">
                    							<div class="form-horizontal">
                    								<div class="form-group">
                    									<label for="" class="col-sm-2 control-label">活動名稱</label>
                    									<div class="col-sm-10">
                    										<p class="form-control-static"><?php echo $row->seminar_title;?></p>
                    									</div>
                    								</div>
                    								<div class="form-group">
                    									<label for="" class="col-sm-2 control-label">主辦單位</label>
                    									<div class="col-sm-10">
                    										<p class="form-control-static"><?php echo $row->seminar_host;?></p>
                    									</div>
                    								</div>
                    								<div class="form-group">
                    									<label for="" class="col-sm-2 control-label">活動連結</label>
                    									<div class="col-sm-10">
                    										<p class="form-control-static"><a href="<?php echo $row->seminar_url;?>" target="_blank" class="modal_record" data-type="seminar" data-id="<?php echo $row->seminar_id;?>">連結</a></p>
                    									</div>
                    								</div>
                    								<div class="form-group">
                    									<label for="" class="col-sm-2 control-label">摘要</label>
                    									<div class="col-sm-10">
                    										<p class="form-control-static"><?php echo $row->seminar_content;?></p>
                    									</div>
                    								</div>
                    								<div class="form-group">
                    									<label for="" class="col-sm-2 control-label">時間</label>
                    									<div class="col-sm-10">
                    										<p class="form-control-static">
                    										<?if($row->seminar_start_time == $row->seminar_end_time){?>
                                    							時間 ： <?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>
                                    						<?}else{?>
                                    							時間 ： <?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>～<?php echo date("Y-m-d", strtotime($row->seminar_end_time));?>
                                    						<?}?>
                    										</p>
                    									</div>
                    								</div>
                    								<div class="form-group">
                    									<label for="" class="col-sm-2 control-label">地點</label>
                    									<div class="col-sm-10">
                    										<p class="form-control-static"><?php echo $row->seminar_place;?></p>
                    									</div>
                    								</div>
                    							</div>
                    						</div>
                    					</div><!-- /.modal-content -->
                    				</div><!-- /.modal-dialog -->
                    			</div><!-- /.modal -->
                    		<?}?>
                    	<?}?>
                    </ul>
                    <p style="font-size:12px; font-weight:bold;">
                    	<a id="moreSeminar" data-more="seminar" data-url="<?php echo $domain_url;?>" class="more" href="javascript: void(0)" style="color:#2a6496;">
                        	更多相關活動 <i class="fa fa-angle-double-right"></i>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    	<div class="row">
        	<div class="col-sm-4">
            	<div class="resources-HR">
                    <h3 style="font-size:1.5em; margin-top:0px;">企業徵才</h3> 
                    <ul class="" style="padding:0;">
                    	<? if ($listJobsByDomain->num_rows() > 0){?>
                    		<? foreach($listJobsByDomain->result() as $row){?>
                    		<li>
                        		<a class="record" data-type="jobs" data-id="<?php echo $row->jobs_id;?>" href="<?php echo $row->jobs_url;?>" target="_blank" style="text-decoration:none">
                                	<p style="color:#999;">
                                    	<strong style="color:#6f6f6f;">
                                    		<?php echo date("Y.m.d", strtotime($row->jobs_post_date)) .' '.$row->comp_name?>
                                    	</strong> 正在應徵 <strong style="color:#6f6f6f;"><?php echo $row->jobs_title?></strong>
                                	</p>
                        		</a>
                        	</li>
                        	<? } ?>
                        <? }?>
                    </ul>
                    <p style="font-size:12px; font-weight:bold;">
                    	<a id="moreJobs" data-more="jobs" class="more" data-url="<?php echo $domain_url;?>" href="javascript: void(0)" style="color:#2a6496;">
                        	更多企業徵才 <i class="fa fa-angle-double-right"></i>
                        </a>
                    </p>
                </div>
            </div>
        	<div class="col-sm-4">
            	<div class="resources-trends">
                    <h3 style="font-size:1.5em; margin-top:0px;">趨勢應用</h3> 
                    <ul class="" style="padding:0; list-style:none;">
                    	<? if ($listIndustryByDomain->num_rows() > 0){?>
                    		<? foreach($listIndustryByDomain->result() as $row){?>
                    			<li>
                                	<a class="record" data-type="industry" data-id="<?php echo $row->industry_id;?>" href="<?php echo $row->industry_url;?>" target="_blank">
                    				<!--<?php echo date("Y.m.d", strtotime($row->industry_post_date)) .' '.$row->industry_title?>-->
                                    	<?php echo $row->industry_title?>
                                    </a> 
                                    <span style="font-size:12px; color:#999;">(<?php echo date("Y.m.d", strtotime($row->industry_post_date))?>)</span>
                    			</li>
                    		<? }?>
                    	<? }?>
                    </ul>
                    <p style="font-size:12px; font-weight:bold;">
                    	<a id="moreIndustry" data-more="industry" data-url="<?php echo $domain_url;?>" class="more" href="javascript: void(0)" style="color:#2a6496;">
                        	更多趨勢應用 <i class="fa fa-angle-double-right"></i>
                        </a>
                    </p>
                </div>
            </div>
        	<div class="col-sm-4">
            	<div class="resources-international">
                    <h3 style="font-size:1.5em; margin-top:0px;">國內外資源</h3> 
                    <ul class="" style="padding:0; list-style:none;">
                        <? if ($listResourceByDomain->num_rows() > 0){?>
                    		<? foreach($listResourceByDomain->result() as $row){?>
                    			<li>
                    				<span style="float:left; line-height:1.3em; margin-left:-60px; width:50px;" class="label <?php echo $row->label_name;?>">
										<?switch ($row->resource_type){
											case "1":echo "影片";break;
                							case "2":echo "文件";break;
                							case "3":echo "文章";break;
                							case "4":echo "其他";break;
                							default:echo "其他";}?>
                                    </span>
                                	<a class="record" data-type="resource" data-id="<?php echo $row->resource_id;?>" href="<?php echo $row->resource_url;?>" target="_blank">
                    				<!--<?php echo date("Y.m.d", strtotime($row->resource_post_date)) .' '.$row->resource_title?>-->
                                    	<?php echo $row->resource_title?>
                                    </a>
                                    <span style="font-size:12px; color:#999;">(<?php echo date("Y.m.d", strtotime($row->resource_post_date))?>)</span>
                                </li>
                    		<? }?>
                    	<? }?>
                    </ul>
                    <p style="font-size:12px; font-weight:bold;">
                    	<a id="moreResource" data-more="resource" data-url="<?php echo $domain_url;?>" class="more" href="javascript: void(0)" style="color:#2a6496;">
                        	更多國內外資源 <i class="fa fa-angle-double-right"></i>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if($this->session->userdata('email')){?>
	<?//$this->load->view('forum/domainModalStartDiscuss');?>
<?}?>
<?$this->load->view('home/modal_login');?>
