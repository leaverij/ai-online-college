<style>
.search-bar{position:relative;}
.search-bar .search-text {display:block; width:100%; padding:15px; color:#666; font-size:14px; background:#f9f9f9; border:none; border-bottom:1px solid #cfe4e5; border-radius:5px 5px 0 0;}
.search-bar ::-webkit-input-placeholder {color:#bcbcbc;/* WebKit browsers */}
.search-bar .search-text:focus::-webkit-input-placeholder {color:#e3e3e3; transition:ease 0.1s; -webkit-transition:ease 0.1s; -moz-transition:ease 0.1s; -ms-transition:ease 0.1s;}
.search-bar :-moz-placeholder {color:#bcbcbc;/* Mozilla Firefox 4 to 18 */}
.search-bar ::-moz-placeholder  {color:#bcbcbc;/* Mozilla Firefox 19+ */}
.search-bar :-ms-input-placeholder {color:#bcbcbc;/* Internet Explorer 10+ */}
.search-bar .search-btn{position:absolute; top:0; right:0; bottom:0; border:none; padding:0 15px; background:none;}
.search-bar .search-btn span{font-size:16px; color:#bcbcbc;}

.sidebar {margin-bottom:30px; background:#f5f5f5; border-radius:5px; box-shadow:0 1px 0 0 #ededed; padding-bottom:10px;}
.nav-list li > a {padding:5px 25px;}
#sidebar-collapse ul li h4 {font-size:16px;}
#sidebar-collapse ul li h4 a:hover, #sidebar-collapse ul li.active a{text-decoration:none; background:#eeeeee; color:#24bbc9;}
#sidebar-collapse ul li a {color:#666; font-size:15px;}
#sidebar-collapse ul li a:hover {color:#24bbc9;}
#sidebar-collapse .library-list li a {color:#888;}

.sorting.btn-group .btn {color:#fff; font-size:13px; background:#56606e; margin-right:2px;}
.sorting.btn-group .btn:hover {background:#778599;}
.sorting.btn-group .btn .caret {border-top:4px solid #fff;}
@media screen and (max-width: 768px) {
	.sidebar {padding:0;}
	.sidebar .navbar-toggle {display:block; margin:0; padding:10px 12px; cursor:pointer; color:#666; font-size:16px;}
}
</style>
<script type="text/javascript" charset="utf-8">
	flag="<?php echo $this->session->flashdata('flag');?>"
</script>
<section class="domain-hero <?=$domain_url?>" style="min-height:250px; text-align:center;">
	<div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="hero-content" style="position:absolute; top:85px; right:0; left:0; text-align:center;">
                    <h1 style="font-size:2.3em; margin-top:0; letter-spacing:0.05em; color:#fff;"><?=$domain_name?></h1>
                    <p style="font-size:1.14em; color:#e9f3f7;">邁向職場成功之路</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
	<div class="container" style="padding:30px 0;">
    	<div class="row">
        	<div class="col-sm-3">
                
                <!-- sideBar -->
				<?php $this->load->view('domain/sidebar'); ?>
            </div>
            <div class="col-sm-9">
                <div class="tab-content">
                  <div class="tab-tittle" style="overflow:hidden;">
                  	<h4 id="more-title" style="font-size:1.5em; float:left;">相關活動</h4>
                    <div class="pull-right">
                        <div class="btn-group sorting">
                          <button type="button" class="btn">依最新資訊</button>
                          <button type="button" class="btn">依歷史資料</button>                  
                        </div>
                    </div>                    
                  </div>
                  <!-- tab_相關活動 -->
                  <div class="tab-pane <?php echo ($this->session->userdata('flag')=='seminar')?'active':''?>" id="seminar">
                      <div class="table-responsive">
                          <table class="table">
                            <colgroup>
                                <col class="col-xs-1"><col class="col-xs-1"><col class="col-xs-5"><col class="col-xs-3"><col class="col-xs-3">
                            </colgroup>                          
                          	<thead>
                                <tr>
                                    <th>#</th>
                                    <th>類型</th>
                                    <th>活動名稱</th>
                                    <th>活動日期</th>
                                    <th>主辦單位</th>
                                </tr>
                            </thead>
                            <tbody>
                              <? if ($listAllSeminarByDomain->num_rows() > 0){?>
                              <? foreach($listAllSeminarByDomain->result() as $key => $row){?>
                                <tr>
                                    <td><?php echo $key+1?></td>
                                    <td><?php echo $row->seminar_name;?></th>
                                    <td><a href="<?php echo $row->seminar_url;?>"><?php echo $row->seminar_title;?></a></td>
                                    <td><?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>～<?php echo date("Y-m-d", strtotime($row->seminar_end_time));?></td>
                                    <td><?php echo $row->seminar_host;?></td>
                                </tr>
                              <? }?>
                              <? }?>
                            </tbody>
                          </table>
                      </div>
                      <ul class="pagination" style="display: none"> <!--超過10比再出現分頁按鈕-->
                        <li class="disabled"><a href="#">&laquo;</a></li>
                        <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">...</a></li> <!--以此類推-->
                        <li><a href="#">&raquo;</a></li>
                      </ul>                      
                  </div>
                  <!-- tab_趨勢應用 -->
                  <div class="tab-pane <?php echo ($this->session->userdata('flag')=='industry')?'active':''?>" id="industry">
                      <ul class="" style="padding:0; list-style:none;">
                          <? if ($listAllIndustryByDomain->num_rows() > 0){?>
                              <? foreach($listAllIndustryByDomain->result() as $row){?>
                                  <li style="padding:10px 0; border-bottom:1px solid #ddd">
                                  	<a href="<?php echo $row->industry_url;?>" target="_blank">
                                      <?php echo date("Y.m.d", strtotime($row->industry_post_date)) .' '.$row->industry_title?>
                                    </a>
                                  </li>
                              <? }?>
                          <? }?>
                      </ul>
                      <ul class="pagination" style="display: none"> <!--超過10比再出現分頁按鈕-->
                        <li class="disabled"><a href="#">&laquo;</a></li>
                        <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">...</a></li> <!--以此類推-->
                        <li><a href="#">&raquo;</a></li>
                      </ul>                      
                  </div>
                  <!-- tab_國內外資源 -->
                  <div class="tab-pane <?php echo ($this->session->userdata('flag')=='resource')?'active':''?>" id="resource">
                      <ul class="" style="padding:0; list-style:none;">
                          <? if ($listAllResourceByDomain->num_rows() > 0){?>
                              <? foreach($listAllResourceByDomain->result() as $row){?>
                              	  <li style="padding:10px 0; border-bottom:1px solid #ddd">
                                  	<a href="<?php echo $row->resource_url;?>" target="_blank">
                                      <?php echo date("Y.m.d", strtotime($row->resource_post_date)) .' '.$row->resource_title?>
                                    </a>
                                  </li>
                              <? }?>
                          <? }?>
                      </ul>
                      <ul class="pagination" style="display: none"> <!--超過10比再出現分頁按鈕-->
                        <li class="disabled"><a href="#">&laquo;</a></li>
                        <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">...</a></li> <!--以此類推-->
                        <li><a href="#">&raquo;</a></li>
                      </ul>                      
                  </div>
                  <!-- tab_企業徵才 -->
                  <div class="tab-pane <?php echo ($this->session->userdata('flag')=='jobs')?'active':''?>" id="jobs">
                      <ul class="" style="padding:0; list-style:none;">
                          <? if ($listAllJobsByDomain->num_rows() > 0){?>
                              <? foreach($listAllJobsByDomain->result() as $row){?>
                              <li style="padding:10px 0; border-bottom:1px solid #ddd">
                                  <a href="<?php echo $row->jobs_url;?>" target="_blank" style="text-decoration:none">
                                      <p style="color:#999; margin:0;">
                                          <strong style="color:#6f6f6f;">
                                              <?php echo date("Y.m.d", strtotime($row->jobs_post_date)) .' '.$row->comp_name?>
                                          </strong> 正在應徵 <strong style="color:#6f6f6f;"><?php echo $row->jobs_title?></strong>
                                      </p>
                                  </a>
                              </li>
                              <? } ?>
                          <? }?>
                      </ul>
                      <ul class="pagination" style="display: none">
                        <li class="disabled"><a href="#">&laquo;</a></li>
                        <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">...</a></li> <!--以此類推-->
                        <li><a href="#">&raquo;</a></li>
                      </ul>                      
                  </div>
                </div>            
            </div>
        </div>
    </div>
</section>