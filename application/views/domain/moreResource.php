<section class="domain-hero <?=$domain_url?>" style="min-height:250px; text-align:center;">
	<div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="hero-content" style="position:absolute; top:85px; right:0; left:0; text-align:center;">
                    <h1 style="font-size:2.3em; margin-top:0; letter-spacing:0.05em; color:#fff;"><?=$domain_name?></h1>
                    <p style="font-size:1.14em; color:#e9f3f7;">邁向職場成功之路</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
	<div class="container" style="padding:30px 0;">
    	<div class="row">
        	<div class="col-sm-3">
                <!-- sideBar -->
				<?php $this->load->view('domain/sidebar'); ?>
            </div>
            <div class="col-sm-9">
                <div class="tab-content">
                	<div class="tab-tittle">
                    	<form class="" action="<?echo base_url()?>college/moreresource/<?php echo $domain_url;?>" method="post" role="search">
                            <input type="hidden" name="action" value="search"/>
                            <h4 id="more-title" style="font-size:1.5em; margin-bottom:0; display:inline-block;">國內外資源</h4>
                            <div class="input-group input-group-sm row col-sm-6 pull-right">
                                <input type="text" class="form-control" id="search_string" name="search_string" value="<?php echo (isset($search_string))?$search_string:'';?>"/>
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default search-btn" tabindex="-1">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>                     
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <span id ="resource"><?php echo $resource_type;?></span><span class="caret"></span>
                                    </button>
                                    <ul id ="resourceUl" class="dropdown-menu">
                                        <li><a resource-data-pjax href="<?php echo base_url()?>college/moreresource/<?php echo $domain_url?>">依資源類型</a></li>
                                        <li><a resource-data-pjax href="<?php echo base_url()?>college/moreresource/<?php echo $domain_url?>/1">影片</a></li>
                                        <li><a resource-data-pjax href="<?php echo base_url()?>college/moreresource/<?php echo $domain_url?>/2">文件</a></li>
                                        <li><a resource-data-pjax href="<?php echo base_url()?>college/moreresource/<?php echo $domain_url?>/3">文章</a></li>
                                        <li><a resource-data-pjax href="<?php echo base_url()?>college/moreresource/<?php echo $domain_url?>/4">其他</a></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                  </div>
                  <!-- tab_國內外資源 -->
                  <div id="resource-pjax-container" style="padding-top:20px;">
                  	<div>
                  		<?if (isset($search_string) && $search_string){?>
                  		<h4 style="color:#a7a7a7; font-size:14px; margin-top:0;">搜尋結果如下：<?echo $search_string;?>(共<?echo count($listAllResourceByDomain->result())?>筆結果)</h4>
                  		<?}?>
                  	</div>
                  	<? if ($listAllResourceByDomain->num_rows() > 0){?>
                  	<div class="table-responsive">
                          <table class="table">
                            <colgroup>
                                <col class="col-xs-1"><col class="col-xs-1"><col class="col-xs-10">
                            </colgroup>                          
                          	<thead>
                                <tr>
                                    <th>#</th>
                                    <th>類型</th>
                                    <th>資源名稱</th>
                                </tr>
                            </thead>
                            <tbody>
                              <? foreach($listAllResourceByDomain->result() as $key => $row){?>
                                <tr>
                                    <td><?php echo $key+1?></td>
                                    <td><?switch ($row->resource_type){	case "1":echo "影片";break;
                														case "2":echo "文件";break;
                														case "3":echo "文章";break;
                                                                        case "4":echo "其他";break;
                                                                        case "5":echo "平台";break;
                														default:echo "其他";}?>
                                    <td>
                                    	<a class="record" data-type="resource" data-id="<?php echo $row->resource_id;?>" href="<?php echo $row->resource_url;?>" target="_blank" style="text-decoration:none; white-space:nowrap; text-overflow:ellipsis; overflow:hidden;">
											<?php echo $row->resource_title?>
                                        </a>
                                        <span style="font-size:12px; color:#999; font-style:italic;"> <?php echo ($row->resource_source)?$row->resource_source:''?> </span> 
                                        <span style="font-size:12px; color:#999; font-style:italic;"> (<?php echo date("Y.m.d", strtotime($row->resource_post_date)) ?>)</span>
                                    </td>
                                </tr>
                              <? }?>
                            </tbody>
                          </table>
                      </div><!-- /.table-responsive -->
                      <? }?>
                      <? if (!isset($search_string) && $last>1){?>
                      <? if ($listAllResourceByDomain->num_rows() > 0){?>
                      <ul class="pagination" style="">
                      	<li class=""><a href="?page=1"><i class="fa fa-angle-double-left"></i></a></li>
                        <!-- get 的page如果小於第三頁，設定page為3-->
                        <?$target = $page?>
                        <?if($page<3) $page=3;?>
                        <!-- get 的page如果大於總頁數，設定page-2-->
                        <?if($page+2>$last) $page=$last-2;?>
                        <? $index=1?>
                        <?for($i=$page-2;$i<$page+3;$i++){?>
                        	<?if($index=='1'){?>
                        		<li class=""><a href="?page=<?php echo ($page<=3)?'1':$i-1?>"><i class="fa fa-angle-left"></i></a></li>
                        	<?}?>
                        	<?if($i>'0'){?><!-- i==0 不顯示 -->
                        		<li class="<?php echo ($target==$i)?'active':''?>"><a href="?page=<?echo $i;?>"><?echo $i?></a></li>
                        	<?}?>
                        	<?if($index=='5'){?>
                        		<li class=""><a href="?page=<?php echo ($i==$last)?$last:$i+1?>"><i class="fa fa-angle-right"></i></a></li>
                        	<?}?>
                        	<? $index++;?>
                        <?}?>
                        <li class="">
                        	<a href="<?php echo ($last)?'?page='.$last:'javascript: void(0)'?>">
                        		<i class="fa fa-angle-double-right"></i>
                        	</a>
                        </li>
                      </ul>
                      <?}?>
                      <?}?>                     
                  </div>
                </div>            
            </div>
        </div>
    </div>
</section>