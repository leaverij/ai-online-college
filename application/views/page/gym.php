<style>
	ul.course-block-list {padding:0;}
	ul.course-block-list > li {list-style:none; margin-bottom:30px;}
	.course-card-hero:before {content:''; height:100%; display:inline-block; margin-right:-0.25em; vertical-align:middle; z-index:1;}
</style>
<div class="container" style="padding-top:30px;">
	<div class="row">
    	<div class="col-md-3 col-sm-3 sidebar">
            <?php $this->load->view('library/sidebar'); ?> 
        </div><!-- /sidebar-->
		<div class="col-md-9 col-sm-9">
			<!-- 右邊區塊 -->
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12" style="margin-bottom:30px;">
					<h2 style="margin:0 0 8px 0; font-size:1.71em;">實作體驗</h2>
				</div>
			</div>
			<div class="row">
				<div class="lab">
					<ul class="course-block-list">
					<?php $labs = json_decode($getLabChapterContent); ?>
					<?foreach($labs as $key=>$lab){?>
						<li class="col-md-6 col-sm-6 col-xs-12">
							<div class="course-card" style="border:1px solid #d5d5d5;">
								<a href="<?php echo base_url().'page/gym_unit/'.$lab -> chapter_content_id?>">
									<div class="col-xs-8 content-meta">
										<h3 style="font-size:16px;"><?php echo $lab -> chapter_content_desc; ?></h3>
										<p><?php echo $lab->subtitle;?></p>
										<div class="content-actions-container" style="position:absolute; bottom:30px; right:30px; left:30px;">
										免費體驗!
										</div>
									</div>
									<div class="col-xs-4 course-card-hero" style="background:#f2f2f2; text-align:center; position:absolute; top:0; bottom:0; right:0;">
                        				<img src="<?=base_url(); ?>assets/img/badges/<?php echo $lab->chapter_content_img; ?>">
                        			</div>
								</a>
							</div>
						</li>
					<?}?>
					</ul>
				</div>
			</div>
			<!-- 右邊end -->
		</div>
	</div>
</div><!-- /container -->