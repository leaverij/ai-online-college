<div class="content">
	<div class="container">
        <div><p></p></div>
        <div class="jumbotron" style="background:#f2f4f7;"> 
        	<div class="row">
            	<div class="col-sm-12">
            		<div class="form-group">
                   		<label for="inputFile">功能說明：輸入csv檔案之後，系統會產生使用者的名字、email以及預設密碼</label>
                   		<input type="file" name="csv" id="csv">
                   		<p class="help-block">請注意！只接受.csv檔案！上限是50個email</p>
                   		<div id="table"></div>
                    </div>           	
                    <form action="<?php echo base_url()?>page/inputBatchEmail" method="post">
                    	<input type="hidden" name="action" value="batchEmail" />
                      	<input type="hidden" name="name" id="name" />
                      	<input type="hidden" name="email" id="email" />
                    	<input type="hidden" name="password" id="password" />
                    	<button type="submit" class="btn btn-primary" id="sendBatchMail">送出</button>
                    </form>
                    	
                </div>
            </div>
        </div>
    </div>
</div>