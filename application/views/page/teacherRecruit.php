<style>
#teacherRecruitModal .modal-header {border:none; text-align:center;}
#teacherRecruitModal .modal-body .section {margin-bottom:50px;}
#teacherRecruitModal .modal-body .section:last-child {margin-bottom:15px;}
#teacherRecruitModal .modal-body .section p {color:#767676; padding:0 30px;}
#teacherRecruitModal .modal-body .section img {
	width:130px;
	-webkit-filter: drop-shadow(0 3px 1px rgba(0,0,0,0.2));
    -ms-filter: "progid:DXImageTransform.Microsoft.Dropshadow(OffX=0, OffY=3, Color='#ccc')";
    filter: "progid:DXImageTransform.Microsoft.Dropshadow(OffX=0, OffY=3, Color='#ccc')";}
#teacherRecruitModal .modal-body .section ul {text-align:left; padding:15px 15px 15px 35px; margin:0;}
.recruit-info {margin:20px; background:#f6f9fa; border-radius:5px; box-shadow:0 3px #d9e4e8;}
</style>

<!-- Modal -->
<div class="modal fade" id="teacherRecruitModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title" id="myModalLabel">成為 Learning House 合作夥伴</h3>
      </div>
      <div class="modal-body" style="text-align:center;">
      	<div class="section">
            <p>Learning House是一個提供人們專業課程提升知識及技能的學習平台。</p>
            <p>我們擁有專業的老師、優質的課程以及優秀的製作與開發團隊，希望大家在Learning House 上學習專業知識與技能，在職涯發展路上提升自我價值。</p>       
        </div>
        <div class="section">
        	<img src="<?=base_url();?>assets/img/studio-shot.png" />
        </div>
        <div class="section">
        	<p>我們有完善的合作夥伴制度，您會加入我們的驚人的數位影片製作團隊，創造高品質的互動教學課程以提高Learning House的學生學習。</p>
        </div>
        <div class="section">
        	<h4>若您擁有以下特質</h4>
            <div class="recruit-info">
                <ul>
                    <li>具資通訊專業技能(系統開發、行動app開發、巨量資料…等)及教學經驗</li>
                    <li>積極、有責任感</li>
                    <li>發自內心的熱愛分享知識</li>
                </ul>
            </div>
        </div>
        <div class="section">
        	<h4 style="color:#0fabb9;">快與我們連絡!</h4>
            <p>(來信請附照片之簡歷)</p>
            <a href="mailto:learninghouse.service@gmail.com">learninghouse.service@gmail.com</a>
        </div>
      </div>
    </div>
  </div>
</div>