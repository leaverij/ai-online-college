<style type="text/css" media="screen">
    #editor { 
        height: 360px;
    }
</style>
<div class="content">
	<div class="container">
        <div class="col-md-2">
            <!--題目-->
            <div id="virtual-lab-question"></div>
        </div>
        <div class="col-md-10">
            <div class="col-md-8">
                <!--編輯器-->
                <div id="virtual-lab-editor"></div>
            </div>
            <div class="col-md-4">
                <!--結果-->
                <div id="virtual-lab-result"></div>
            </div>
        </div>
    </div>
</div>