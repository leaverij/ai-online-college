<div class="content">
	<div class="container">
        <div>
            <h2 style="font-size:1.8em;">關於 AI Online College</h2>
            <hr />
        </div>
    </div>
    
	<!-- 2欄 -->
    <style>
    .quote-text{border-left:none; background:#f2f4f7; padding:25px; margin:0 0 0 110px; border-radius:5px;}
	.quote-text:before{content:''; display:block; width:0; height:0; position:absolute; border:10px solid; left:90px; border-color:transparent #f2f4f7 transparent transparent;}
	.quote-text:after{}
	.quote-text h3{font-size:1.3em; margin:0 0 8px; font-weight:bold;}
	.quote-text p{line-height:1.5em; font-size:1.15em;}
    </style>
    <div class="container">
        <div id="masonry" class="row js-masonry" data-masonry-options='{ "itemSelector": ".item" }'>
        	<?if ($listTeam->num_rows() > 0){?>
                <?foreach ($listTeam->result() as $row){?>
                <div class="col-sm-6 item">
                	<div style="margin-bottom: 30px;">
                    	<img style="float:left; border-radius:50%;" src="<?=base_url();?>assets/img/member/<?echo $row->photo?>" width="80" height="80"/>
                    	<div style="position:relative;">                      
                     		<blockquote class="quote-text">
                        		<h3><?echo $row->name?></h3>
                           		<p style="margin-bottom:5px; color:#999999; font-size:1em;"><?echo $row->role?></p>
                           		<p><?echo $row->desc?></p>
                       		</blockquote>
                    	</div>
                	</div>
            	</div>
                <?}?>
            <?}?>
        </div>
    </div>




	<!-- 3欄 
    <div class="container">
        <h2 style="font-size:1.8em;">3欄的樣子</h2>
        <div id="masonry" class="row js-masonry" data-masonry-options='{ "itemSelector": ".item" }'>
        	<div class="col-sm-4 item">
                <div style="margin-bottom: 30px;">
                    <img style="float:left; border-radius:50%;" src="<?=base_url();?>assets/img/member/chih.JPG" width="80" />
                    <div style="position:relative;">                      
                        <blockquote class="quote-text">
                        	<h3>永鑫</h3>
                            <p>我們相信透過學習可以改變一個人的價值，Learning house希望在您職涯成長的路上貢獻一點力量，透過老師們用心錄製的課程及各式學習活動安排與指引，協助您在專業及技能上學習的更好，在職涯發展路上不斷提升自我價值。</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        	<div class="col-sm-4 item">
                <div style="margin-bottom: 30px;">
                    <img style="float:left; border-radius:50%;" src="<?=base_url();?>assets/img/member/chih.JPG" width="80" />
                    <div style="position:relative;">
                        
                        <blockquote class="quote-text">
                        	<h3>胤禎</h3>
                            <p>學習和睡眠一樣重要，要在最自在的狀態下，補足最多的能量，營造愉悅的學習經驗，是我們追求的品質和目標，從一門你有興趣的課程開始探索、開始體驗當學習變得得心應手，你也將變得無比富有。</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        	<div class="col-sm-4 item">
                <div style="margin-bottom: 30px;">
                    <img style="float:left; border-radius:50%;" src="<?=base_url();?>assets/img/member/change.JPG" width="80" height="80" />
                    <div style="position:relative;">                      
                        <blockquote class="quote-text">
                        	<h3>承致</h3>
                            <p>經由LearningHouse的線上影片學習，加上老師精心設計的題目，可以馬上知道對課程的熟悉度是否掌握住，而透過系統的服務，可以清楚知道自己學習的進度以及成就。</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        	<div class="col-sm-4 item">
                <div style="margin-bottom: 30px;">
                    <img style="float:left; border-radius:50%;" src="<?=base_url();?>assets/img/member/chih.JPG" width="80" />
                    <div style="position:relative;">                      
                        <blockquote class="quote-text">
                        	<h3>Chih Chih</h3>
                            <p>我們相信關注於理解與迎合人們的實際需求，如此設計出來的互動介面與應用程式，將會更實用且獲得更多的樂趣。我們致力於呈現具親和力的介面，而最棒的介面是存而不察的，它既不會阻礙您的學習，更會讓您照自己的方式在這裡提升自我。</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        	<div class="col-sm-4 item">
                <div style="margin-bottom: 30px;">
                    <img style="float:left; border-radius:50%;" src="<?=base_url();?>assets/img/member/208347.JPG" width="80" />
                    <div style="position:relative;">                      
                        <blockquote class="quote-text">
                        	<h3>奕均</h3>
                            <p>無論您人在何處，我們提供您相同品質的遠距學習服務，透過實作驗證您知識的質量，您能在這裡找到學習上問題的答案，一步步地拼湊個人的知識拼圖。</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div> -->
</div>