<div class="content">
	<div class="container">
        <div>
            <h2 style="font-size:1.8em;">個人資料蒐集同意書</h2>
            <hr/>
        </div>
        <div>
          <p><b>Learning House</b>
          	（以下簡稱本平台）為遵循個人資料保護法規定及本平台隱私權政策要求，在您提供個人資料予本平台前，有義務告知下列事項，請您詳閱，謝謝！</p>    
    <table class="table table-bordered table-hover">
        <tr>
            <td rowspan="2" width="100">1.蒐集目的</td>
            <td colspan="2">◆辦理本次活動及相關行政管理。</td>
        </tr>
        <tr>
            <td colspan="2">◆寄送Learning House平台或產業相關之活動訊息。
            ※您日後如不願再收到Learning House平台寄送之行銷訊息，可於收到前述訊息時，直接點選訊息內拒絕接受之連結。</td>
        </tr>
        <tr>
            <td rowspan="4">2.個人資料類別</td>
            <td>識別個人</td>
            <td>姓名、職業、聯絡方式、教育等。</td>
        </tr>
        <tr>
            <td>識別財務</td>
            <td>如金融機構帳戶之號碼與姓名、信用卡或簽帳卡之號碼等。</td>
        </tr>
        <tr>
            <td>個人描述</td>
            <td>年齡、性別或出生年月日。</td>
        </tr>
        <tr>
            <td>辨識政府資料者</td>
            <td>國民身分證統一編號或護照號碼。</td>
        </tr>
        <tr>
            <td rowspan="3">3.個人資料利用之期間、地區、對象及方式</td>
            <td>利用期間</td>
            <td>至蒐集目的消失為止。</td>
        </tr>
        <tr>
            <td>利用地區</td>
            <td>除蒐集之目的涉及國際業務或活動外，LearningHouse僅於中華民國領域內利用您的個人資料。</td>
        </tr>
        <tr>
            <td>對象及方式</td>
            <td>LearningHouse於蒐集目的之必要範圍內，利用您的個人資料。</td>
        </tr>
        <tr>
            <td>4.當事人權利</td>
            <td colspan="2">您可以於網站「個人資料保護專頁」公告之方式向LearningHouse行使查詢或請求閱覽、製給複製本、補充或更正、停止蒐集/處理/利用或刪除您的個人資料之權利。</td>
        </tr>
        <tr>
            <td>5.不提供正確個資之權益影響</td>
            <td colspan="2">若您不提供正確之個人資料予本平台，本平台將無法為您提供特定目的之相關服務。</td>
        </tr>
    </table>

        </div>
    </div>
    
    </div>
</div>