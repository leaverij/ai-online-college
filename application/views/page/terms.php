<div class="content">
	<div class="container">
        <div>
            <h2 style="font-size:1.8em;">AI Online College服務條款</h2>
            <hr />
        </div>
           <div>
             <table >
            <tr>
                <td width="50" style="width:37.5pt;padding:0cm 0cm 0cm 0cm">
                    <p><span></span></p>
                </td>
                <td>
                    <p><span><h3>為了保障您的權益，在註冊前請先詳細閱讀本同意書之所有內容，當您在點選「同意」後，即視為您已閱讀本同意書，並同意遵守以下所有同意書之規範。</h3></span></p>
                </td>
           </tr>
            <tr>
                <td >
                    <p><span>1.</span></p>
                </td>
                <td >
                    <p><span>遵守會員規範</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span> </span></p>
                </td>
                <td >
                    <p><span>在您於AI Online College註冊成為會員後，可以使用AI Online College所提供之各種服務。當您使用AI Online College時，即表示同意接受AI Online College之會員規範及所有注意事項之約束。</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span>2.</span></p>
                </td>
                <td >
                    <p><span>使用服務</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span></span></p>
                </td>
                <td >
                    <p><span>請您於註冊時儘可能提供符合真實之個人資料，您所登錄之資料事後若有變更時，也一併隨時更新。</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span>3.</span></p>
                </td>
                <td >
                    <p><span>個人資料之保護</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span> </span></p>
                </td>
                <td >
                    <p><span>對於會員所登錄或留存之個人資料，AI Online College在未獲得會員同意以前，決不對外揭露會員之姓名、地址、電子郵件地址及其他依法受保護之個人資料。下述幾種特殊情況不受此限：</span></p>
                    <table border="0" cellpadding="0" width="100%" style="width:100.0%">
                        <tr>
                            <td valign="top" >
                                <p><span>(1)</span></p>
                            </td>
                            <td >
                                <p><span>基於法律之規定</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" >
                                <p><span>(2)</span></p>
                            </td>
                            <td >
                                <p><span>受司法機關或其他有權機關基於法定程序之要求</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" >
                                <p><span>(3)</span></p>
                            </td>
                            <td >
                                <p><span>為保障AI Online College之財產及權益</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" >
                                <p><span>(4)</span></p>
                            </td>
                            <td >
                                <p><span>在緊急情況下為維護其他會員或第三人之人身安全</span></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span>4.</span></p>
                </td>
                <td >
                    <p><span>會員的義務與責任</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span> </span></p>
                </td>
                <td >
                    <p><span>會員對本身於AI Online College或透過AI Online College傳輸的一切內容自負全責。</span></p>
                    <table border="0" cellpadding="0" width="100%" style="width:100.0%">
                        <tr>
                            <td valign="top" >
                                <p><span>(1)</span></p>
                            </td>
                            <td >
                                <p><span>會員承諾遵守中華民國相關法規及一切國際網際網路規定與慣例。</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" >
                                <p><span>(2)</span></p>
                            </td>
                            <td >
                                <p><span>會員同意並保證不公布或傳送任何毀謗、不實、威脅、不雅、猥褻、不法、攻擊性、毀謗性或侵害他人智慧財產權的文字，圖片或任何形式的檔案於AI Online College上。</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" >
                                <p><span>(3)</span></p>
                            </td>
                            <td >
                                <p><span>非經AI Online College書面同意，會員不得於AI Online College上從事廣告或販賣商品行為。</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" >
                                <p><span>(4)</span></p>
                            </td>
                            <td >
                                <p><span>會員同意避免在公眾討論區討論私人事務，發表文章時，請尊重他人的權益及隱私權。</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" >
                                <p><span>(5)</span></p>
                            </td>
                            <td >
                                <p><span>會員同意必須充份尊重著作權，禁止發表侵害他人各項智慧財產權之文字、圖片或任何形式的檔案。</span></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span>5.</span></p>
                </td>
                <td>
                    <p><span>禁止從事違反法律規定之行為</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span></span></p>
                </td>
                <td>
                    <p><span>AI Online College就會員的行為是否符合會員規範有最終決定權。若AI Online College決定會員的行為違反本會員規範或任何法令，會員同意AI Online College得隨時停止帳號使用權或清除帳號，並停止使用AI Online College。會員在違反法律規定之情事，應自負法律責任。</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span>6.</span></p>
                </td>
                <td>
                    <p><span>服務之停止與更改</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span></span></p>
                </td>
                <td>
                    <p><span></span></p>
                    <table border="0" cellpadding="0" width="100%" style="width:100.0%">
                        <tr>
                            <td valign="top" >
                                <p><span>(1)</span></p>
                            </td>
                            <td >
                                <p><span>於發生下列情形之一時，AI Online College有權停止或中斷提供服務：</span></p>
                                <table border="0" cellpadding="0" width="100%" style="width:100.0%">
                                    <tr>
                                        <td valign="top" >
                                            <p><span>*</span></p>
                                        </td>
                                        <td >
                                            <p><span>對AI Online College之設備進行必要之保養及施工時</span></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <p><span>*</span></p>
                                        </td>
                                        <td >
                                            <p><span>發生突發性之設備故障時</span></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <p><span>*</span></p>
                                        </td>
                                        <td >
                                            <p><span>由於AI Online College所申請之ISP業者無法提供服務時</span></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" >
                                            <p><span>*</span></p>
                                        </td>
                                        <td >
                                            <p><span>因天災等不可抗力之因素致使AI Online College無法提供服務時</span></p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" >
                                <p><span>(2)</span></p>
                            </td>
                            <td >
                                <p><span>AI Online College可能因公司或ISP業者網路系統軟硬體設備之故障、失靈或人為操作上之疏失而造成全部或部份中斷、暫時無法使用、延遲或造成資料傳輸或儲存上之錯誤、或遭第三人侵入系統篡改或偽造變造資料等，會員不得因此而要求任何補償。</span></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span>7.</span></p>
                </td>
                <td>
                    <p><span>保管及通知義務</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span></span></p>
                </td>
                <td>
                    <p><span>您有責任維持密碼及帳號的機密安全。您必須完全負起因利用該密碼及帳號所進行之一切行動之責任。當密碼或帳號遭到未經授權之使用，或發生其他任何安全問題時，您必須立即通知AI Online College，每次您連線完畢，均要結束您的帳號使用。因您未遵守本項約定所生之任何損失或損害，我們將無法亦不予負責。</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span>8.</span></p>
                </td>
                <td>
                    <p><span>特別同意事項</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span></span></p>
                </td>
                <td>
                    <p><span>您同意於AI Online College所發表之一切內容僅代表您個人之立場與行為，並同意承擔所有相關衍生之責任，AI Online College不負任何責任。</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span>9.</span></p>
                </td>
                <td >
                    <p><span>擔保責任免除</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span> </span></p>
                </td>
                <td >
                    <table border="0" cellpadding="0" width="100%" style="width:100.0%">
                        <tr>
                            <td valign="top" >
                                <p><span>(1)</span></p>
                            </td>
                            <td >
                                <p><span>AI Online College保留隨時停止、更改各項服務內容或終止任一會員帳號使用之權利，無需事先通知會員本人。無論任何情形，就停止或更改服務或終止會員帳號使用所可能產生之困擾、不便或損害，AI Online College對任何會員或第三人均不負任何責任。</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" >
                                <p><span>(2)</span></p>
                            </td>
                            <td >
                                <p><span>AI Online College保留將來新增、修改或刪除各項服務之全部或一部之權利，且不另行個別通知會員不得因此而要求任何補償或賠償。</span></p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" >
                                <p><span>(3)</span></p>
                            </td>
                            <td >
                                <p><span>AI Online College提供之特定服務可能存在專屬之服務條款，在此情形下，雙方權利義務將依據該服務之專屬條款決定之。</span></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span>10.</span></p>
                </td>
                <td>
                    <p><span>損害賠償</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span></span></p>
                </td>
                <td>
                    <p><span>因會員違反相關法令或違背本同意書之任一條款，致AI Online College或其關係企業、受僱人、受託人、代理人及其他相關履行輔助人因而受有損害或支出費用（包括且不限於因進行民事、刑事及行政程序所支出之律師費用）時，會員應負擔損害賠償責任或填補其費用。</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span>11.</span></p>
                </td>
                <td>
                    <p><span>會員規範之修改</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span></span></p>
                </td>
                <td>
                    <p><span>AI Online College保留隨時修改本會員規範之權利，AI Online College將於修改會員規範時，於WEB首頁公告修改之內容，而不另對會員作個別通知。 若會員不同意修改的內容，請勿繼續使用AI Online College。如果會員繼續使用AI Online College，將視為會員已同意並接受本規範等增訂或修改內容之約束。</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span>12.</span></p>
                </td>
                <td>
                    <p><span>個別條款之效力</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span></span></p>
                </td>
                <td>
                    <p><span>本同意書所定之任何會員條款之全部或一部無效時，不影響其他條款之效力。</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span>13.</span></p>
                </td>
                <td>
                    <p><span>準據法及管轄法院</span></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><span></span></p>
                </td>
                <td>
                    <p><span>本同意書之解釋及適用以及會員因使用本服務而與AI Online College間所生之權利義務關係，應依中華民國法令解釋適用之。其因此所生之爭議，以臺灣臺北方法院為第一審管轄法院。</span></p>
                </td>
            </tr>
        </table>
        </div>
    </div>
    
    </div>
</div>