<div class="container">
	<div style="padding-bottom:20px;">
		<h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">AI準備度測驗</h1>
	</div>
</div>
<style>
.info-box {
    display: block;
    min-height: 90px;
    background: #fcf7f7;
    width: 100%;
    box-shadow: 0 1px 1px rgba(0,0,0,0.1);
    border-radius: 2px;
    margin-bottom: 15px;
}
.bg-green {
    background-color: #00a65a !important;
}
.info-box-icon {
    border-top-left-radius: 2px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 2px;
    display: block;
    float: left;
    height: 90px;
    width: 90px;
    text-align: center;
    font-size: 45px;
    line-height: 90px;
    background: rgba(0,0,0,0.2);
}
.info-box-content {
    padding: 5px 10px;
    margin-left: 90px;
}
.small-box-footer {
    position: relative;
    text-align: center;
    padding: 3px 0;
    color: black;
    display: block;
    z-index: 10;
    background: #d6e8df;
    text-decoration: none;
    margin-top:20px;
    text-align: right;
}
.info-box-text {
    display: block;
    font-size: 18px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}
.partner {
    width:100px;
    height:100px;
    margin:10px;
    vertical-align:top;
    display:inline-block;
    padding:8px;
    border:1px solid #ccc;
    float: none;
    box-shadow: 1px 1px 5px #CCC;
}
.partner img {
    max-width: 100%;
}
.partner-title {
    text-align: center; 
}
</style>
<div class="container">
    <div class="row">
    <?php
        $assessment_content = json_decode($assessments);
        foreach($assessment_content as $assessment_val){
    ?>
        <div class="col-md-6">
            <div class="info-box">
                <span class="info-box-icon bg-green">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text"><?php echo $assessment_val->name; ?></span>
                    <p><?php echo $assessment_val->description; ?></p>
                </div>
                <a href="<?php echo base_url().'page/ai_assessment/'?><?php echo $assessment_val->url;?>/<?php echo $r_user_id;?>" class="small-box-footer">立即測驗 <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
            </div>
        </div>
    <?php
        }
    ?>        
    </div>
</div><!-- .container -->
<div class="container" style="margin-top:70px;">
	<div style="padding-bottom:20px;">
		<h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">我們的客戶</h1>
	</div>
</div>
<div class="container" style="margin-bottom:20px;">
    <div class="row">
        <div class="col-md-6" style="min-height:100px; text-align:center;">
            <h4 class="partner-title"><u style="text-decoration: none;border-bottom: 6px solid red;">企業使用</u></h4>
            <div class="partner" style="width:150px;height:50px;padding:0px;">
                <img style="height:50px;" src="<?php echo base_url()?>/assets/img/partner/eva.png">
            </div>
            <div class="partner" style="width:150px;height:50px;padding:0px;">
                <img style="height:50px;" src="<?php echo base_url()?>/assets/img/partner/canon.png">
            </div>
            <div class="partner" style="width:150px;height:50px;padding:0px;">
                <img style="height:50px;" src="<?php echo base_url()?>/assets/img/partner/sinyi.jpg">
            </div>
            <div class="partner" style="width:150px;height:50px;padding:0px;">
                <img style="height:50px;" src="<?php echo base_url()?>/assets/img/partner/cpc.png">
            </div>
            <div class="partner" style="width:150px;height:50px;padding:0px;">
                <img style="height:50px;" src="<?php echo base_url()?>/assets/img/partner/taishin.jpg">
            </div>
        </div>
        <div class="col-md-6" style="min-height:100px; text-align:center;">
            <h4 class="partner-title"><u style="text-decoration: none;border-bottom: 6px solid green;">校園使用</u></h4>
            <div class="partner">
                <img src="<?php echo base_url()?>/assets/img/partner/ntu.jpg">
            </div>
            <div class="partner">
                <img src="<?php echo base_url()?>/assets/img/partner/nthu.png">
            </div>
            <div class="partner">
                <img src="<?php echo base_url()?>/assets/img/partner/ncku.png">
            </div>
            <div class="partner">
                <img src="<?php echo base_url()?>/assets/img/partner/ncu.png">
            </div>
            <div class="partner">
                <img src="<?php echo base_url()?>/assets/img/partner/nsysu.png">
            </div>
            <div class="partner">
                <img src="<?php echo base_url()?>/assets/img/partner/ccu.png">
            </div>
            <div class="partner">
                <img src="<?php echo base_url()?>/assets/img/partner/kmu.png">
            </div>
            <div class="partner">
                <img src="<?php echo base_url()?>/assets/img/partner/tku.png">
            </div>
        </div>
    </div>
</div>
<script>
/*$(document).ready(function(){
    
    $('.partner').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4
    });
    
});*/
</script>