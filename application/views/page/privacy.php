<div class="content">
	<div class="container">
		<div>
			<h2 style="font-size:1.8em;">AI Online College隱私權保護政策</h2>
			<hr />
		</div>
			<div>
				<table >
					<tr>
						<td width="50" style="width:37.5pt;padding:0cm 0cm 0cm 0cm"><p><span>壹、</span></p></td>
						<td ><p><span >適用範圍</span></p></td>
					</tr>
					<tr>
						<td >
							<tr>
								<td ><p><span ></span></p></td>
								<td ><p><span ></span></p>
									<table border="0" cellpadding="0" width="100%" style="width:100.0%">
										<tr>
											<td valign="top" >
												<p><span >一、</span></p>
											</td>
											<td >
												<p><span >AI Online College「隱私權聲明」適用於您與AI Online College洽辦業務、參與各項活動（如報名研討會/課程、加入網站會員、訂閱電子報等）或透過電話、傳真或AI Online College網站意見信箱提出詢問或建議時（包括AI Online College官網及AI Online College各業務部門網站），所涉及之個人資料蒐集、處理與利用行為。</span></p>
											</td>
										</tr>
										<tr>
											<td valign="top" >
												<p><span >二、</span></p>
											</td>
											<td >
												<p><span >凡經由AI Online College網站連結至第三方獨立管理、經營之網站，有關個人資料的保護，適用第三方或各該網站的隱私權政策，AI Online College不負任何連帶責任。</span></p>
											</td>
										</tr>
									</table>
									<p><span ></span></p>
								</td>
							</tr>
						</td>
						<td >
							<p></span></p>
						</td>
					</tr>
					<tr>
						<td ><p><span>貳、</span></p></td>
						<td ><p><span >個人資料之蒐集、處理及利用</span></p></td>
					</tr>
					<tr>
						<td ><p><span > </span></p></td>
						<td >
							<tr>
								<td ><p><span></span></p></td>
								<td ><p><span></span></p>
									<table border="0" cellpadding="0" width="100%" style="width:100.0%">
										<tr><td valign="top" ><p><span >一、</span></p></td>
											<td >
												<p><span>當您與AI Online College洽辦業務或參與AI Online College各項活動，我們將視業務或活動性質請您提供必要的個人資料，並在該特定目的範圍內處理及利用您的個人資料；非經您書面同意，AI Online College不會將個人資料用於其他用途。</span></p>
											</td>
										</tr>
										<tr>
											<td valign="top" ><p><span >二、</span></p></td>
											<td >
												<p><span>如果您使用電話、傳真或AI Online College網站意見信箱與AI Online College聯繫時，請您提供正確的電話、傳真號碼或電子信箱地址，作為回覆來詢事項之用。</span></p>
											</td>
										</tr>
										<tr>
											<td valign="top" ><p><span >三、</span></p></td>
											<td >
												<p><span>您的個人資料在處理過程中，AI Online College將遵守相關之流程及內部作業規範，並依據資訊安全之要求，進行必要之人員控管。</span></p>
											</td>
										</tr>
										<tr>
											<td valign="top" ><p><span >四、</span></p></td>
											<td ><p><span>單純瀏覽AI Online College網站及下載檔案之行為，AI Online College不會蒐集任何與個人身份有關之資訊。</span></p></td>
										</tr>
										<tr>
											<td><p><span ></span></p></td>
										</tr>
									</table>
								</td>
							</tr>
						</td>
					</tr>
					<tr>
						<td ><p><span>參、</span></p></td>
						<td ><p><span>與第三人共用個人資料</span></p></td>
					</tr>
					<tr>
						<td ><p><span > </span></p></td>
						<td >
							<tr>
								<td ><p><span > </span></p></td>
								<td ><p><span > </span></p>
									<table border="0" cellpadding="0" width="100%" style="width:100.0%">
										<tr>
											<td valign="top" ><p><span >一、</span></p></td>
											<td ><p><span>AI Online College絕不會提供、交換、出租或出售任何您的個人資料給其他個人、團體、私人企業或公務機關，但有法律依據或合約義務者，不在此限。</span></p></td>
										</tr>
										<tr>
											<td valign="top" ><p><span >二、</span></p></td>
											<td ><p><span>前項但書之情形包括不限於：</span></p>
												<tr>
													<td ><p><span ></span></p></td>
													<td ><p><span></span></p>
														<table border="0" cellpadding="0" width="100%" style="width:100.0%">
															<tr>
																<td valign="top" ><p><span >(1)</span></p></td>
																<td ><p><span>配合司法單位合法的調查。</span></p></td>
															</tr>
															<tr>
																<td valign="top" ><p><span >(2)</span></p></td>
																<td ><p><span>配合主管機關依職權或職務需要之調查或使用（例如審計部或會計師查帳）。</span></p></td>
															</tr>
															<tr>
																<td valign="top" ><p><span >(3)</span></p></td>
																<td ><p><span>基於善意相信揭露您的個人資料為法律所必需。</span></p></td>
															</tr>
															<tr>
																<td valign="top" ><p><span >(4)</span></p></td>
																<td ><p><span>當您在AI Online College網站的行為，違反AI Online College的服務條款或可能損害或妨礙AI Online College權益或導致任何人遭受損害時，經AI Online College研析揭露您的個人資料是為了辨識、聯絡或採取法律行動所必要者。</span></p></td>
															</tr>
															<tr>
																<td valign="top" ><p><span >(5)</span></p></td>
																<td><p><span>基於委外契約關係，AI Online College依約履行提供個人資料之義務。</span></p></td>
															</tr>
															<tr>
																<td><p><span ></span></p></td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td valign="top" ><p><span >三、</span></p></td>
													<td ><p><span>AI Online College委託廠商協助蒐集、處理或利用您的個人資料時，將對委外廠商或個人善盡監督管理之責。</span></p></td>
												</tr>
											<td><p><span ></span></p></td>
										</tr>
									</table>
								<td><p><span></span></p></td>
							</tr>
							<tr>
								<td ><p><span>肆、</span></p></td>
								<td ><p><span>cookie 之運用</span></p></td>
							</tr>
							<tr>
								<td ><p><span > </span></p></td>
								<td >
									<tr>
										<td ><p><span ></span></p></td>
										<td ><p><span></span></p>
											<table border="0" cellpadding="0" width="100%" style="width:100.0%">
												<tr>
													<td valign="top" ><p><span >一、</span></p></td>
													<td ><p><span>基於網站內部管理之需要及提供最佳個人化服務，AI Online College網站將在您的瀏覽器中寫入cookies並讀取記錄瀏覽者的 IP 位址、上網時間以及在各項資訊查閱之次數，進行網站流量和網路行為調查之總量分析，不會對「個別」瀏覽者進行分析。</span></p></td>
												</tr>
												<tr>
													<td valign="top" ><p><span >二、</span></p></td>
													<td ><p><span>若您不願接受 cookie 的寫入，您可將使用中之瀏覽器設定為拒絕 cookie 的寫入，但也因此會使網站某些功能無法正常執行。</span></p></td>
												</tr>
											</table>
											<p><span ></span></p>
										</td>
									</tr>
									<p><span></span></p>
								</td>
							</tr>
							<tr>
								<td ><p><span>伍、</span></p></td>
								<td ><p><span>伺服器紀錄</span></p></td>
							</tr>
							<tr>
								<td ><p><span></span></p></td>
								<td ><p><span>當您透過瀏覽器、應用程式或其他用戶端使用AI Online College網站時，我們的伺服器會自動記錄特定的技術性資訊。這些伺服器紀錄可能包含您的網頁要求、網際網路通訊協定位址、瀏覽器類型、瀏覽器語言、送出要求的日期和時間等資訊。此伺服器紀錄僅作為伺服器管理的參考，AI Online College不會利用此伺服器紀錄對「個別」瀏覽者進行分析。</span></p></td>
							</tr>
							<tr>
								<td ><p><span>陸、</span></p></td>
								<td ><p><span>隱私權聲明之修改</span></p></td>
							</tr>
							<tr>
								<td ><p><span > </span></p></td>
								<td ><p><span>本隱私權聲明將適時依據法律修正、相關技術之發展及內部管理制度之調整而配合修改，以落實保障您隱私權及網路安全之初衷。當AI Online College完成相關條款修改時，會立即將其刊登於AI Online College網站中，並以醒目標示提醒您前往點選閱讀。</span></p></td>
							</tr>
				</table>
		</div>
	</div>
</div>
</div>