<?php
$assessment_submit_content = json_decode($assessment_submit);
?>
<!DOCTYPE html>
<html lang="zh-tw">
  <head>
    <meta charset="utf-8">
    <meta name="keywords" content="線上學習,mooc,moox,learninghouse,learning,資策會,中壢中心" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="我們相信透過學習可以改變一個人的價值，Learning House希望在您職涯成長的路上貢獻一點力量，透過老師們用心錄製的課程及各式學習活動安排與指引，協助您在專業及技能上學習的更好，在職涯發展路上不斷提升自我價值">
    <meta name="author" content="LearningHouse Team">
    <meta name="copyright" CONTENT="本網頁著作權LearningHouse所有">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, minimal-ui" />
	<meta property="og:description" content="大家也趕快來學習吧，在這裡你可以學習到新的技能以及更多的知識喔!"/>
    <link rel="shortcut icon" href="<?=base_url();?>assets/ico/favicon.png">
	<title><?=$title;?>-<?=$this->config->item('site_name');?></title>
    <link href="<?=base_url();?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/css/learning-house.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=base_url();?>assets/js/html5shiv.js"></script>
      <script src="<?=base_url();?>assets/js/respond.min.js"></script>
    <![endif]-->
    <style>
	.box {
    	position: relative;
    	border-radius: 3px;
    	background: #ffffff;
    	border-top: 3px solid #d2d6de;
    	margin-bottom: 20px;
    	width: 100%;
    	box-shadow: 0 1px 1px rgba(0,0,0,0.1);
	}
	.box.box-success {
    	border-top-color: #00a65a;
	}
	.box-header.with-border {
    	border-bottom: 1px solid #f4f4f4;
	}
	.box-header {
    	color: #444;
    	display: block;
    	padding: 10px;
    	position: relative;
    	border-left: 1px solid #ccc;
    	border-right: 1px solid #ccc;
	}
	.box-title {
    	display: inline-block;
    	font-size: 18px;
    	margin: 0;
    	line-height: 1;
	}
	.box-body {
    	border-top-left-radius: 0;
    	border-top-right-radius: 0;
    	border-bottom-right-radius: 3px;
    	border-bottom-left-radius: 3px;
    	padding: 10px;
    	border: 1px solid #ccc;
	}
	.profile-user-img {
    	margin: 0 auto;
    	width: 100px;
    	padding: 3px;
    	border: 3px solid #d2d6de;
	}
	img {
    	display: block;
    	max-width: 100%;
    	height: auto;
    	vertical-align: middle;
	}
	.img-circle {
    	border-radius: 50%;
	}
	.profile-username {
    	font-size: 21px;
    	margin-top: 5px;
	}
	.text-center {
    	text-align: center;
	}
	.tooltip{ 
  		position:relative;
  		float:right;
	}
	.tooltip > .tooltip-inner {background-color: #eebf3f; padding:5px 15px; color:rgb(23,44,66); font-weight:bold; font-size:13px;}
	.popOver + .tooltip > .tooltip-arrow {	border-left: 5px solid transparent; border-right: 5px solid transparent; border-top: 5px solid #eebf3f;}
	section{
  		margin:100px auto; 
  		height:1000px;
	}
	.progress{
  		border-radius:0;
  		overflow:visible;
	}
	.progress-bar{
  		/*background:rgb(23,44,60);*/ 
  		-webkit-transition: width 1.5s ease-in-out;
  		transition: width 1.5s ease-in-out;
	}
	.barWrapper {
		border: none;
	}
	.callout {
		border-radius: 3px;
    	margin: 0 0 20px 0;
    	padding: 15px 30px 15px 15px;
    	border-left: 5px solid #eee;
	}
	.callout-success {
		border-color: #00733e;
		background-color: #00a65a !important;
	}
	.list-group-unbordered>.list-group-item {
		border-left: 0;
    	border-right: 0;
    	border-radius: 0;
    	padding-left: 0;
    	padding-right: 0;
	}
	.list-group-item a {
		text-decoration: none;
	}
	footer{background:url(<?=base_url();?>assets/img/LH-footer-bg.jpg); background-color:#787878; height:60px; color:#999; padding:20px 0;}
	footer a{color:#3cc; font-size:13px; margin-right:10px;}
	</style>
  </head>
  <body>
	<!-- Wrap all page content here -->
    <div id="wrap" style="padding-top:80px;">
    	<header class="navbar-default navbar-fixed-top">
      		<div class="container">
        		<div class="navbar-header">
          			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#lh-navigation">
            			<span class="icon-bar"></span>
            			<span class="icon-bar"></span>
            			<span class="icon-bar"></span>
          			</button>
          			<a class="navbar-brand" href="<?=base_url();?>">
		  				<img src="<?=base_url();?>assets/img/lh-logo-color.png">
                	</a>
        		</div>
				<style>
                    .scrollable-menu {height: auto; max-height: 600px; overflow-x: hidden;}
                    .notification-dropdown .fa-bell {font-size:1.3em; position:relative; color:#999;}
                    .notification-dropdown .badge {font-weight:normal; padding:2px 4px; background:#d9534f; position:absolute; top:-10px; left:15px;}
					.notification-container ul {padding:0;}
					.notification-container ul li {list-style:none; border-top:1px solid #ededed; padding:15px 0;}
					.notification-dropdown .notification-container ul li {padding:15px;}
					.notification-container ul li:first-child {border-top:none;}
					.avatar {
						width:60px;
						height:60px;
						overflow:hidden;
						background-position:center;
						background-size:50px auto !important;
						border-radius:50px;
						border:5px solid #eee;}
					.avatar img {width:60px; height:60px; display:none;}
					.forum.notification .avatar {margin:0 auto;}
					.notif-meta {}
					.notif-meta h4 {font-size:1.14em; margin:0;}
					.notif-meta p {margin:0; font-size:0.92em; color:#999; line-height:2em;}
					.notif-meta .time { font-size:0.85em; color:#bbb;}

                	#lh-navigation .fa-chevron-down {font-size:small;}
					#lh-navigation .dropdown-menu {box-shadow:5px 5px rgba(0,0,0,0.07);}
					#lh-navigation .dropdown-menu > .arrow {position:absolute; display:block; width:0; height:0; border-color:transparent; border-style:solid; border-bottom-color:#ccc; top:-22px; right:10px; border-top-width:0; border-width:11px;}
                	#lh-navigation .dropdown-menu > .arrow:after {position:absolute; display:block; width:0; height:0; border-color:transparent; border-style:solid; border-bottom-color:#fff; top:1px; content:" "; border-width:10px; top:-9px; right:-10px;}
                </style>
        		<nav  id="lh-navigation" class="collapse navbar-collapse">
         				<ul class="nav navbar-nav navbar-right" style="padding:15px 0;">
						 <li><a href="<?php echo base_url()?>page/preparation">檢測</a></li>
						<li><a href="<?php echo base_url()?>page/gym"><i style="color:green;" class="fa fa-flask" aria-hidden="true"></i>Gym</a></li>
						<li><a href="<?php echo base_url()?>library/ai">課程</a></li>
              			<li><a href="<?php echo base_url()?>library">學程</a></li>
              			<? if($this->session->userdata('name')):?>
						<li class="dropdown">
							<a href="#" data-toggle="dropdown">儀表板<i class="fa fa-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<div class="arrow"></div>
								<li><a href="<?php echo base_url()?>dashboard">個人儀表版</a></li>
								<li><a href="<?php echo base_url()?>dashboard/teacher">教師儀表版</a></li>
								<li><a href="<?php echo base_url()?>dashboard/admin">主管儀表版</a></li>
							</ul>
						</li>
              			<?endif?>
              			<li><a href="<?php echo base_url()?>forum">討論區</a></li>
                        <!-- SIGN IN/ SIGN OUT -->
            			<? if(!$this->session->userdata('alias')):?>
            			<li class="login"><a href="<?=base_url();?>home/login">登入/註冊</a></li>
            			<? endif?>
						<? if($this->session->userdata('alias')):?>
						<li class="dropdown">
              				<a href="#" data-toggle="dropdown">
              					<?php echo $this->session->userdata('alias')?> <!--<span class="caret"></span>-->
              					<i class="fa fa-chevron-down"></i>
                            </a>
              				<ul class="dropdown-menu">
                            	<div class="arrow"></div>
                				<li><a href="<?=base_url()?>profile/<?php echo $this->session->userdata('alias')?>">帳號管理</a></li>
                				
                				<?php if($this->user_model->checkJob()){?>
                				<li><a href="<?=base_url()?>vacancy/">企業徵才</a></li>
                				<?php }?>
                				<!-- <li><a href="<?=base_url()?>user/editPortfolio">個人履歷</a></li> -->
                                <li><a href="<?=base_url()?>user/portfolio/<?php echo $this->session->userdata('alias')?>">個人履歷</a></li>
                                <li><a href="<?=base_url()?>user/analysis">能力分析</a></li>
                				<?php if($this->user_model->checkBackend()){?>
                				<li><a href="<?=base_url()?>backend">後台管理介面</a></li>
                				<?php if($this->user_model->checkBacthEmail()){?>
                				<li><a href="<?=base_url()?>page/inputBatchEmail">群組寄信</a></li>
                				<?}?>
                				<?}?>
                				<?php if($this->user_model->isSuper($this->session->userdata('email'))){?>
                				<li><a href="<?=base_url()?>user/webCamPic">個人照片</a></li>
                				<?}?>
                				<? if($this->session->userdata('fb_login')): ?> 
									<li><?=anchor($this->session->userdata('fb_logout_url'),'登出',"class=''")?></li>
								<? else: ?>
									<li><a href="<?=base_url();?>home/logout">登出</a></li>
								<? endif;?>
              				</ul>
            			</li>
                        
            			<!-- <?php $row=json_decode($getDropdownNotification);?>
						<li class="notification-dropdown dropdown">
              				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
              					<i title="通知訊息" class="fa fa-bell">
                              		<span class="notifications-count badge"><?php if($getUnreadNotification!=0){echo $getUnreadNotification;}?></span>
                                </i>
                            </a>
              				<div class="notification-dropdown-content dropdown-menu" style="width:400px;">
                            	<div class="arrow"></div>
              					<h4 style="margin:0; padding:10px 0; border-bottom:1px solid #ededed; text-align:center;">通知訊息</h4>
              					<?php if(empty($row)){?>
              						<div style="text-align:center; color:#97a5a6; padding:15px;">您目前沒有任何的通知訊息</div>
              					<?php }else{?>
                                <div class="scroll-mask notification-container" style="overflow:hidden;">
                                	<ul class="scrollable-menu" style="list-style:none; max-height:500px; margin-right:-17px;">
	              					<?php $i=0;?>
	              					<?php foreach($row as $key=>$value){?>
		                				<li class="forum notification" style="list-style: none;">
			                				<a href="<?php if($row[$key]->notification_type==1 ||$row[$key]->notification_type==2){echo base_url().'forum/forumContent/'.$row[$key]->hyperlink;}?>" style="display:block; color:#333;">
			                					<div class="row" style="line-height:15px;overflow:hidden;">
			                						<div class="col-sm-8">
                                                    	<div class="notif-meta">
                                                            <p><?php if($row[$key]->notification_type==1){echo '討論區回應';}elseif($row[$key]->notification_type==2){echo '新問題';}?></p>
                                                            <h4><?php echo $row[$key]->notification_title;?></h4>
                                                            <p>經由 <strong><?php echo $row[$key]->alias;?></strong> <?php if($row[$key]->notification_type==1){echo '回答';}elseif($row[$key]->notification_type==2){echo '提問';}?></p>
                                                            <p class="time"><?php echo $this->theme_model->calTimeElapsed($row[$key]->notification_time);?></p>
			                							</div>
                                                    </div>
			                						<div class="col-sm-4">
						                				<div class="avatar" style="background-image: url('<?=base_url();?>assets/img/user/128x128/<?php echo $row[$key]->img;?>')">
						                					<img src="<?=base_url();?>assets/img/user/128x128/<?php echo $row[$key]->img;?>">
						                				</div>
			                						</div>
			                					</div>
		                					</a>
		                				</li>
		                				<?php if($row[$key]->is_read==0){?>
		                					<input type="hidden" name="unread[]" value="<?php echo $row[$key]->notification_id;?>">
		                				<?php }?>
		                				<?php $i++;?>
	                				<?php }?>
                                    </ul>
                                </div>
                				<?php }?>
                				<div style="border-top:1px solid #ededed; text-align:center;">
                                	<a href="<?=base_url();?>notifications" style="display:block; padding:10px 0;">檢視所有通知訊息</a>
                                </div>
              				</div>
            			</li> -->
            			<? endif;?>
          			</ul>
       		 	</nav><!--/.nav-collapse -->
      		</div>
    	</header>
        <div class="container" style="margin-top:30px;">
            <div class="row">
                <div class="col-md-4">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">檢測人</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <img class="profile-user-img img-responsive img-circle" src="<?=base_url();?>assets/img/user/128x128/default_user.png">
                                    <h3 class="profile-username text-center"><?php echo $user_id ?></h3>
									<p class="text-muted text-center">Section Manager</p>
									<ul class="list-group list-group-unbordered">
										<li class="list-group-item">
											<b><i class="fa fa-calendar" aria-hidden="true"></i> 施測日期</b>
											<a class="pull-right"><?php echo $assessment_submit_content->submit_date ?></a>
										</li>
									</ul>
									<a id="consultBtn" href="#" class="btn btn-primary btn-block"><b>我要諮詢</b></a>
									<br />
									<div style="display:none;" class="alert alert-success" id="success-alert">
    									<strong>已收到您的要求，我們會有專人聯繫您</strong>
    								</div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">檢測報告</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
									<div class="callout callout-success">
										<h4>準備度</h4>
									</div>
									<div class="form-group">
                                        <label>問題解決能力</label>
                                        <div class="barWrapper form-control">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="00" style="background:#337ab7;">   
                                                <span  class="popOver" data-toggle="tooltip" data-placement="top" title="5"> </span>     
											</div>
                                        </div>
                                    </div>
									<div class="form-group">
                                        <label>問題定義能力</label>
                                        <div class="barWrapper form-control">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="00" style="background:#f0ad4e;">   
                                                <span  class="popOver" data-toggle="tooltip" data-placement="top" title="3"> </span>     
                                            </div>
                                        </div>
                                    </div>  
                                </div>
							</div>
							<div class="row">
                                <div class="col-md-6" style="text-align:center;">
                                    <h4>得分</h4>
                                    <input type="text" value="<?php echo $score ?>" class="dial">
                                </div>
                                <div class="col-md-6" style="text-align:center;">
                                    <h4>類型</h4>
									<?php
										if($score<=36){
									?>
									<p>你可能傾向於將問題視為否定，而不是把它們視為進行令人興奮和必要改變的機會。你解決問題的方法比系統更直觀，這可能導致過去一些糟糕的經歷。通過更多練習，並採用更加結構化的方法，您將能夠發展這一重要的技能，並立即開始更有效地解決問題。</p>
									<?php
										}else if($score<=58){
									?>
									<p>你的解決問題的方法有點“碰運氣”。有時候你的解決方案工作得很好，有時候卻不行。你明白你應該做什麼，你認識到有一個結構化的解決問題的過程是重要的。但是，你並不總是遵循這個過程。通過一致性和承諾，您將看到顯著的改進。</p>
									<?php
										}else{
									?>
									<p>你是一個自信的問題解決者。你花時間去了解這個問題，理解一個好的決定的標準，並產生一些很好的選擇。因為你有系統地處理問題，所以你每次都要覆蓋要點 - 而且你的決定很好，但是很好，很好的計劃和很好的執行。您可以繼續完善解決問題的能力，並將其用於組織內的持續改進活動。通過你丟失點以下的部分，並進一步磨練你的技能！</p>
									<?php
										}
									?>
                                    <!--
									<input type="text" value="5" class="dial">
									-->
                                </div>
                            </div><br />
							<div class="row">
								<div class="col-md-12">
									<div id="capability" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto"></div>
								</div>
							</div><br /><br />
							<div class="row">
								<div class="col-md-12">
									<div id="compare" style="min-width: 310px; height: 400px; max-width: 800px; margin: 0 auto"></div>
								</div>
							</div><br /><br />
							<div class="row">
								<div class="col-md-12">
									<div class="callout callout-success">
										<h4>問題定義能力</h4>
									</div>
									<div class="row">
										<div class="col-md-4">
											<canvas id="def-chart"></canvas>
										</div>
										<div class="col-md-8">
											<table class="table">
												<thead>
													<tr>
														<th>定義層次</th>
														<th>表現</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Def-A</td>
														<td></td>
													</tr>
													<tr>
														<td>Def-B</td>
														<td></td>
													</tr>
													<tr>
														<td>Def-C</td>
														<td></td>
													</tr>
													<tr>
														<td>Def-D</td>
														<td></td>
													</tr>
													<tr>
														<td>Def-E</td>
														<td></td>
													</tr>
													<tr>
														<td>Def-F</td>
														<td></td>
													</tr>
													<tr>
														<td>Def-G</td>
														<td></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="callout callout-success">
										<h4>問題解決能力</h4>
									</div>
									<div class="row">
										<div class="col-md-4">
											<canvas id="sol-chart"></canvas>
										</div>
										<div class="col-md-8">
											<table class="table">
												<thead>
													<tr>
														<th>定義層次</th>
														<th>表現</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Def-A</td>
														<td></td>
													</tr>
													<tr>
														<td>Def-B</td>
														<td></td>
													</tr>
													<tr>
														<td>Def-C</td>
														<td></td>
													</tr>
													<tr>
														<td>Def-D</td>
														<td></td>
													</tr>
													<tr>
														<td>Def-E</td>
														<td></td>
													</tr>
													<tr>
														<td>Def-F</td>
														<td></td>
													</tr>
													<tr>
														<td>Def-G</td>
														<td></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- #wrap -->

	<footer>
		<div class="container">
    		<div class="row">
        		<div class="col-md-8">
            		<a href="http://ailt.iiiedu.org.tw/">關於我們</a>
                	<a href="<?=base_url();?>page/terms">服務條款</a>
                	<a href="<?=base_url();?>page/privacy">隱私權政策</a>
                	Copyright © AI Online College  2017
            	</div>
        	</div>
    	</div>
	</footer>
	
    <script src="<?=base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?=base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>assets/js/jquery.knob.min.js"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
  	<script>
  	$(document).ready(function(){
      $('.dial').knob({
          "skin":"tron",
          "width":"100",
          "height":"100"
      });
      $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
      $(".progress-bar").each(function(){
            each_bar_width = $(this).attr('aria-valuenow');
            $(this).width(each_bar_width + '%');
      });
	  //能力
	  var capability_src = <?=$capability?>;
	  var capability = [];
	  for(var c=0;c<capability_src.length;c++){
		  capability.push({
			  "name":capability_src[c].name,
			  "y":Number(capability_src[c].y)
		  });
	  }
	  Highcharts.chart('capability', {
    	chart: {
        	type: 'column'
    	},
    	credits: {
    		enabled:false
    	},
    	title: {
        	text: '能力分布'
    	},
    	subtitle: {
        	text: null
    	},
    	xAxis: {
        	type: 'category',
			labels: {
            	rotation: 30
        	}
    	},
    	yAxis: {
        	title: {
            	text: '分數'
        	}
    	},
    	legend: {
        	enabled: false
    	},
    	series: [{
        	name: '能力',
        	colorByPoint: true,
        	data: capability
    	}]
	  });
	  //compare
	  Highcharts.chart('compare', {
      	chart: {
        	type: 'scatter',
        	zoomType: 'xy'
      	},
      	title: {
        	text: '前500大企業準備度比較'
      	},
      	xAxis: {
        	title: {
            	enabled: true,
            	text: '問題定義能力'
        	},
        	startOnTick: true,
        	endOnTick: true,
        	showLastLabel: true
      	},
      	yAxis: {
        	title: {
            	text: '問題解決能力'
        	}
      	},
      	legend: {
        	enabled:false
      	},
      	credits:{
    		enabled:false
      	},
      	plotOptions: {
        	scatter: {
            	marker: {
                	radius: 5,
                	states: {
                    	hover: {
                        	enabled: true,
                        	lineColor: 'rgb(100,100,100)'
                    	}
                	}
            	},
            	states: {
                	hover: {
                    	marker: {
                        	enabled: false
                    	}
                	}
            	},
            	tooltip: {
                	headerFormat: '<b>{series.name}</b><br>',
                	pointFormat: '{point.x} , {point.y} '
            	}
        	}
      	},
      	series: [{
        	name: '您的準備度',
        	color: 'rgba(223, 83, 83, .5)',
        	data: [[161.2, 51.6]]
      	}, {
        	name: '前500大企業的準備度',
        	color: 'rgba(119, 152, 191, .5)',
        	data: [[174, 65], [175, 71], [193, 80], [186, 72], [187, 78],
            [181.5, 74.8], [184.0, 86.4], [184.5, 78.4], [175.0, 62.0], [184.0, 81.6],
            [180.0, 76.6], [177.8, 83.6], [192.0, 90.0], [176.0, 74.6], [174.0, 71.0],
            [184.0, 79.6], [192.7, 93.8], [171.5, 70.0], [173.0, 72.4], [176.0, 85.9],
            [176.0, 78.8], [180.5, 77.8], [172.7, 66.2], [176.0, 86.4], [173.5, 81.8],
            [178.0, 89.6], [180.3, 82.8], [180.3, 76.4], [164.5, 63.2], [173.0, 60.9],
            [183.5, 74.8], [175.5, 70.0], [188.0, 72.4], [189.2, 84.1], [172.8, 69.1],
            [170.0, 59.5], [182.0, 67.2], [170.0, 61.3], [177.8, 68.6], [184.2, 80.1],
            [186.7, 87.8], [171.4, 84.7], [172.7, 73.4], [175.3, 72.1], [180.3, 82.6],
            [182.9, 88.7], [188.0, 84.1], [177.2, 94.1], [172.1, 74.9], [167.0, 59.1],
            [169.5, 75.6], [174.0, 86.2], [172.7, 75.3], [182.2, 87.1], [164.1, 55.2],
            [163.0, 57.0], [171.5, 61.4], [184.2, 76.8], [174.0, 86.8], [174.0, 72.2],
            [177.0, 71.6], [186.0, 84.8], [167.0, 68.2], [171.8, 66.1], [182.0, 72.0],
            [167.0, 64.6], [177.8, 74.8], [164.5, 70.0], [192.0, 101.6], [175.5, 63.2],
            [171.2, 79.1], [181.6, 78.9], [167.4, 67.7], [181.1, 66.0], [177.0, 68.2],
            [174.5, 63.9], [177.5, 72.0], [170.5, 56.8], [182.4, 74.5], [197.1, 90.9],
            [180.1, 93.0], [175.5, 80.9], [180.6, 72.7], [184.4, 68.0], [175.5, 70.9],
            [180.6, 72.5], [177.0, 72.5], [177.1, 83.4], [181.6, 75.5], [176.5, 73.0],
            [175.0, 70.2], [174.0, 73.4], [165.1, 70.5], [177.0, 68.9], [192.0, 102.3],
            [176.5, 68.4], [169.4, 65.9], [182.1, 75.7], [179.8, 84.5], [175.3, 87.7],
            [184.9, 86.4], [177.3, 73.2], [167.4, 53.9], [178.1, 72.0], [168.9, 55.5],
            [157.2, 58.4], [180.3, 83.2], [170.2, 72.7], [177.8, 64.1], [172.7, 72.3],
            [165.1, 65.0], [186.7, 86.4], [165.1, 65.0], [174.0, 88.6], [175.3, 84.1],
            [185.4, 66.8], [177.8, 75.5], [180.3, 93.2], [180.3, 82.7], [177.8, 58.0],
            [177.8, 79.5], [177.8, 78.6], [177.8, 71.8], [177.8, 116.4], [163.8, 72.2],
            [188.0, 83.6], [198.1, 85.5], [175.3, 90.9], [166.4, 85.9], [190.5, 89.1],
            [166.4, 75.0], [177.8, 77.7], [179.7, 86.4], [172.7, 90.9], [190.5, 73.6],
            [185.4, 76.4], [168.9, 69.1], [167.6, 84.5], [175.3, 64.5], [170.2, 69.1],
            [190.5, 108.6], [177.8, 86.4], [190.5, 80.9], [177.8, 87.7], [184.2, 94.5],
            [176.5, 80.2], [177.8, 72.0], [180.3, 71.4], [171.4, 72.7], [172.7, 84.1],
            [172.7, 76.8], [177.8, 63.6], [177.8, 80.9], [182.9, 80.9], [170.2, 85.5],
            [167.6, 68.6], [175.3, 67.7], [165.1, 66.4], [185.4, 102.3], [181.6, 70.5],
            [172.7, 95.9], [190.5, 84.1], [179.1, 87.3], [175.3, 71.8], [170.2, 65.9],
            [193.0, 95.9], [171.4, 91.4], [177.8, 81.8], [177.8, 96.8], [167.6, 69.1],
            [167.6, 82.7], [180.3, 75.5], [182.9, 79.5], [176.5, 73.6], [186.7, 91.8],
            [188.0, 84.1], [188.0, 85.9], [177.8, 81.8], [174.0, 82.5], [177.8, 80.5],
            [171.4, 70.0], [185.4, 81.8], [185.4, 84.1], [188.0, 90.5], [188.0, 91.4],
            [182.9, 89.1], [176.5, 85.0], [175.3, 69.1], [175.3, 73.6], [188.0, 80.5],
            [188.0, 82.7], [175.3, 86.4], [170.5, 67.7], [179.1, 92.7], [177.8, 93.6],
            [175.3, 70.9], [182.9, 75.0], [170.8, 93.2], [188.0, 93.2], [180.3, 77.7],
            [177.8, 61.4], [185.4, 94.1], [168.9, 75.0], [185.4, 83.6], [180.3, 85.5],
            [174.0, 73.9], [167.6, 66.8], [182.9, 87.3], [160.0, 72.3], [180.3, 88.6],
            [167.6, 75.5], [186.7, 101.4], [175.3, 91.1], [175.3, 67.3], [175.9, 77.7],
            [175.3, 81.8], [179.1, 75.5], [181.6, 84.5], [177.8, 76.6], [182.9, 85.0],
            [177.8, 102.5], [184.2, 77.3], [179.1, 71.8], [176.5, 87.9], [188.0, 94.3],
            [174.0, 70.9], [167.6, 64.5], [170.2, 77.3], [167.6, 72.3], [188.0, 87.3],
            [174.0, 80.0], [176.5, 82.3], [180.3, 73.6], [167.6, 74.1], [188.0, 85.9],
            [180.3, 73.2], [167.6, 76.3], [183.0, 65.9], [183.0, 90.9], [179.1, 89.1],
            [170.2, 62.3], [177.8, 82.7], [179.1, 79.1], [190.5, 98.2], [177.8, 84.1],
            [180.3, 83.2], [180.3, 83.2]]
      		}]
  		});

		new Chart(document.getElementById('def-chart').getContext('2d'), {
    		type: 'radar',
    		data: {
				labels:['Def-A','Def-B','Def-C','Def-D','Def-E','Def-F','Def-G'],
				datasets:[{
					label: "問題定義能力",
            		backgroundColor: 'rgb(255, 99, 132)',
            		borderColor: 'rgb(255, 99, 132)',
					data:[20,15,12,13,10,15,19],
					fill:false
				}]
			},
    		options: {
				
			}
		});
		new Chart(document.getElementById('sol-chart').getContext('2d'), {
    		type: 'radar',
    		data: {
				labels:['Def-A','Def-B','Def-C','Def-D','Def-E','Def-F','Def-G'],
				datasets:[{
					label: "問題定義能力",
            		backgroundColor: 'rgb(255, 99, 132)',
            		borderColor: 'rgb(255, 99, 132)',
					data:[12,25,10,3,5,10,19],
					fill:false
				}]
			},
    		options: {
				
			}
		});

		$('#consultBtn').on('click', function showAlert(){
			$("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
               $("#success-alert").slideUp(500);
            });
		});

  	});
  	</script>
  </body>
</html>        