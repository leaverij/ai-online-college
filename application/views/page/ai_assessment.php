<style>
  .rangeBar{border-bottom: 2px solid #aaa; position: relative; display: flex;}
  .rangeBar div{display: inline-block; float: left; width: 50%; color: #999;}
  .examNum{text-align: right;}
  .radio-label{ display: inline-block; width: 80%; padding: 15px; font-size: 16px; cursor: pointer; border-bottom: 1px solid #aaa;}
  .radio-label:hover{background: #ebebeb;}
  .radio-label span{margin-left: 10px; white-space: pre-line; display: inline-flex;}
  .radioList{position: relative; cursor: pointer;}
  .radioList:before { content: ""; position: absolute; width: 20px; height: 20px; border: 2px solid #aaa; border-radius: 50px; top: -4px; left: -3px; background: #fff;}
  .radioList:checked:before { background: #92D050; border-color: transparent;}
  .btnBox{margin: 20px auto; text-align: center;}
  .btnBox p{color: #999;}
  #form-store-ad-choice h4{line-height: 1.75; white-space: pre-wrap;}
  #answer-list li {
    list-style: none;
    margin-left: -54px;
  }
</style>
<?php $assessment_content = json_decode($assessment); ?>
<div class="container" style="margin-top:30px">
    <div class="row">
    	<div class="col-md-10 col-md-offset-1">
        <div class="rangeBar">
				<div class="examRange">
					<h5><?php echo $title ?></h5>
				</div>
				<div class="examNum">
					<h4><span style="color: #92D050">第 <span id="current_question_no"></span> 題</span> / 共 <span id="total_question_counts"></span> 題</h4>
				</div>
			</div>
    <form id="form-store-ad-choice" method="post" action="<?=base_url();?>api/page/submitAIAssessment">
				<div>
					<h4 id="question"></h4>
					<ul id="answer-list">
          <?php
          if($assessment_content->url=="ai-solve"){
            for($i=1;$i<=5;$i++){
              switch($i){
                case 1:
          ?>
            <li>
              <label class="radio-label">
                <input type="radio" class="radioList" name="options" value="<?php echo $i ?>"/>
                <span>從不這樣</span>
              </label>
            </li>  
          <?php
                  break;
                case 2:
          ?>
            <li>
              <label class="radio-label">
                <input type="radio" class="radioList" name="options" value="<?php echo $i ?>"/>
                <span>很少這樣</span>
              </label>
            </li>  
          <?php
                  break;
                case 3:
          ?>
            <li>
              <label class="radio-label">
                <input type="radio" class="radioList" name="options" value="<?php echo $i ?>"/>
                <span>有時這樣</span>
              </label>
            </li>  
          <?php
                  break;
                case 4:
          ?>
            <li>
              <label class="radio-label">
                <input type="radio" class="radioList" name="options" value="<?php echo $i ?>"/>
                <span>經常這樣</span>
              </label>
            </li>  
          <?php
                  break;
                case 5:
          ?>
            <li>
              <label class="radio-label">
                <input type="radio" class="radioList" name="options" value="<?php echo $i ?>"/>
                <span>總是這樣</span>
              </label>
            </li>  
          <?php
                  break;
              }
            }
          }
          ?>                  
					</ul>
					<input type="hidden" name="user_id" value="<?php echo $user_id ?>">
					<input type="hidden" name="assessment_id" value="<?php echo $assessment_content->id ?>">
          <input type="hidden" id="question_id" name="question_id" value="">
          <input type="hidden" id="answer" name="answer" value="">
          <input type="hidden" id="answer_encode" name="answer_encode" value="">
          <input type="hidden" id="is_correct" name="is_correct" value="">
          <input type="hidden" id="answer_score" name="answer_score" value="">
          <input type="hidden" id="sub_ability_name" name="sub_ability_name" value="">
				</div>
				<div class="btnBox">
					<p>送出前請確認答案，送出後即無法再次修改答案！</p>
          <a id="nextQuestionBtn" class="btn btn-default">送出，下一題</a>
          <button style="display:none;" id="submitBtn" class="btn btn-default" type="submit">完成並送出</button>
				</div>
		</form>
        </div>
    </div>
</div>
<div id="completeModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h3><i style="color:green;" class="fa fa-check-circle fa-3" aria-hidden="true"></i> 恭喜您完成檢測!</h3>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-success" href="<?=base_url();?>page/aiassessmentreport/<?php echo $assessment_content->url ?>/<?php echo $assessment_content->id ?>/<?php echo $user_id ?>">立即查看檢測報告</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="<?=base_url();?>assets/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript">
var assessments = [];
var question_index = 0;
var assessment_url = "<?php echo $assessment_content->url ?>";
$('#form-store-ad-choice').submit(function(e){
    var postData = $('#form-store-ad-choice').serializeArray();
    var formURL = $('#form-store-ad-choice').attr("action");
    $.ajax({
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) {
            $('#completeModal').modal('show');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //if fails      
        }
    });
    e.preventDefault(); //STOP default action
});
$(document).ready(function(){
    $('input[name=options]').on('change', function(){
      var ele = $(this);
      $('#answer').val(ele.parent().text());
      $('#answer_encode').val(ele.val());
      if(assessment_url=='ai-definition'){
        $('#answer_score').val('0');
      }else{
        $('#answer_score').val(ele.val());
      }
      if(assessment_url=='ai-definition'){
        $('#is_correct').val(correct_answer==ele.val());
      }
    });
    var alljson = {
      token:"getchoicefromcbe",
      hashtag:"<?php echo $assessment_content->url ?>"
    }
    $.ajax({
      type: 'post',
      data: alljson,
      url: 'http://www.virtuallab.tw/vlApi/getAllChoice',
      success : function(data, error, xhr){
        assessments = data;
        /*
        for(var i=0;i<10;i++){
          assessments.push(data[i]);
        }
        */
        $('#total_question_counts').html(assessments.length);
        $('#current_question_no').html('1');
        showQuestion(assessments[0]);
        $('input[name=options]').prop('checked', false);
        if(assessment_url=='ai-definition'){
          getQuestionOption(assessments[0]['choice_bank_id'], assessments[0]['choice_bank_id']['answer']);
        }
      } 
    });
    $('#nextQuestionBtn').on('click', function(){
      var user_answer_ele = $('input.radioList:checked');
      var user_answer = user_answer_ele.val();
      var correct_answer = assessments[question_index]['answer'];
      var answer_score = 0;
      if(assessment_url=='ai-solve'){
        answer_score = user_answer_ele.val();
      }
      sendAnswer(
        assessments[question_index]['choice_bank_id'], 
        user_answer_ele.parent().text(), 
        user_answer_ele.val(), 
        user_answer_ele.val()==assessments[question_index]['answer'],
        answer_score,
        $('#sub_ability_name').val()
      );
      question_index++;
      var current_question_count = question_index+1;
      if(question_index==assessments.length-1){
        $('#nextQuestionBtn').hide();
        $('#submitBtn').show();
      }
      showQuestion(assessments[question_index]);
      $('#question_id').val(assessments[question_index]['choice_bank_id']);
      $('#current_question_no').html(current_question_count);
      $('input[name=options]').prop('checked', false);
      if(assessment_url=='ai-definition'){
        getQuestionOption(assessments[question_index]['choice_bank_id'], assessments[question_index]['answer']);
      }
    });
});
function showQuestion(question){
  $('#question').html(question.question);
  $('#sub_ability_name').val(question.sub_ability_name);
}
function getQuestionOption(question_id, correct_answer){
  $('#answer-list').empty();
  var json = {token:"getchoicefromcbe",choice_bank_id:question_id}
    $.ajax({
      type: 'post',
      data: json,
      url: 'http://www.virtuallab.tw/vlApi/getOneChoiceById',
      success : function(answerArr, error, xhr){
        var question = answerArr[0];
        for(var i=0;i<answerArr.length;i++){
          var answer = answerArr[i];
          $('#answer-list').append('<li><label class="radio-label">'+
            '<input type="radio" class="radioList" name="options" value="'+answer.option_encode+'"/>'+
            '<span>'+answer.option_content+'</span>'+
          '</label></li>');
          $('input[name=options]').on('change', function(){
            var ele = $(this);
            $('#answer').val(ele.parent().text());
            $('#answer_encode').val(ele.val());
            $('#is_correct').val(correct_answer==ele.val());
            $('#answer_score').val(0);
          });
        }
      }
    });
}
function sendAnswer(question_id, answer, answer_encode, is_correct, answer_score, sub_ability_name){
  $.ajax({
      type: 'post',
      url: "<?php echo base_url().'api/page/userAnswerAIAssessmentQuestion'?>",
      data: 'assessment_id=<?php echo $assessment_content->id ?>&user_id=<?php echo $user_id ?>&question_id='+question_id+'&answer='+answer+'&answer_encode='+answer_encode+'&is_correct='+is_correct+'&answer_score='+answer_score+'&sub_ability_name='+sub_ability_name,
      success : function(data, error, xhr){
        console.log(data);
      }
    });
}
</script>