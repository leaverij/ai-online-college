<script src="<?=base_url();?>assets/js/morris/morris.css" type="text/javascript" charset="utf-8"></script>
<style>
    .pure-counts {
        text-align:center;
        font-size:30px;
        color:#468847;
    }
</style>
<div class="container">
    <div>
        <div style="padding-bottom:20px;">
            <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">教師儀表板</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">課程數</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <p class="pure-counts"><?php echo $course_counts;?></p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">學生數</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <p class="pure-counts"><?php echo $student_counts;?></p>
                </div>
            </div>
        </div>
    </div><!-- .row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">Top 5 課程</h3>
                    </div>                                                                  
                </div>     
                <div class="panel-body padding-0">
                   <div id="top-courses" style="width:100%;"></div>  
                </div>                
            </div>
        </div>
    </div><!-- .row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">課程概況</h3>
                    </div>                                                                  
                </div>     
                <div class="panel-body padding-0">
                    <table id="course-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>名稱</th>
                                <th>修課人數</th>
                                <th>完課人數</th>
                                <th>完成率</th>
                                <th>討論數</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $td_array = '';
                            foreach($course_summary as $course){
                                $temp = '<tr>';
                                    // 課程名稱
                                    $temp .= '<td><a href="'.base_url().'dashboard/course/'.$course['id'].'">'.$course['name'].'</a></td>';
                                    // 課程修課人數
                                    $temp .= '<td>'.$course['user_counts'].'</td>';
                                    // 課程完課人數
                                    $temp .= '<td>'.$course['end_user_counts'].'</td>';
                                    // 平均完課率
                                    if($course['end_ratio']>80){
                                        $temp .= '<td>'.$course['end_ratio'].'%</td>';
                                    }else if($course['end_ratio']>60){
                                        $temp .= '<td>'.$course['end_ratio'].'%&nbsp;<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:#ec971f;"></i></td>';
                                    }else{
                                        $temp .= '<td>'.$course['end_ratio'].'%&nbsp;<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:red;"></i></td>';
                                    }
                                    // 課程討論數
                                    $temp .= '<td>'.$course['discussion_counts'].'</td>';
                                    $temp .= '</tr>';
                                    $td_array .=$temp;
                            }
                            echo $td_array;
                        ?>
                            <!--
                            <tr>
                                <td><a href="<?=base_url();?>dashboard/course">醫學影像分類</a></td>
                                <td>21</td>
                                <td>125</td>
                                <td>80%</td>
                                <td>22</td>
                            </tr>
                            <tr>
                                <td><a href="<?=base_url();?>dashboard/course">使用Keras實作影像辨識</a></td>
                                <td>18</td>
                                <td>118</td>
                                <td>33%&nbsp;<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:red;"></i></td>
                                <td>18</td>
                            </tr>
                            <tr>
                                <td><a href="<?=base_url();?>dashboard/course">醫學影像介紹</a></td>
                                <td>16</td>
                                <td>103</td>
                                <td>72%</td>
                                <td>8</td>
                            </tr>
                            <tr>
                                <td><a href="<?=base_url();?>dashboard/course">個人化理財推薦組合</a></td>
                                <td>13</td>
                                <td>71</td>
                                <td>72%</td>
                                <td>5</td>
                            </tr>
                            <tr>
                                <td><a href="<?=base_url();?>dashboard/course">預測台股股價指數</a></td>
                                <td>10</td>
                                <td>62</td>
                                <td>56%&nbsp;<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:red;"></i></td>
                                <td>11</td>
                            </tr>
                            -->
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div><!-- .row -->
</div><!-- .container -->
<? $this->load->view('home/modal_login');?>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/morris/raphael-min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/morris/morris.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/dataTables.bootstrap.js" type="text/javascript"></script>
<link href="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet">
<script>
    $(document).ready(function(){
        var top_courses = <?=$top_courses ?>;
        var top_course_array = [];
        for(var i=0;i<top_courses.length;i++){
            top_course_array.push({
                y: top_courses[i]['course_name'],
                a: top_courses[i]['user_counts']
            });
        }
        //Top 5 課程
        Morris.Bar({
            element: 'top-courses',
            data: top_course_array,
		    hoverCallback: function(index, options, content) {
			    return(content);
		    },
            xkey: 'y',
            ykeys: ['a'],
		    units: '個',
		    labels: ['人數'],
		    xLabelMargin: 0,
            barColors: ['#6495ED'],
            gridTextSize: '10px',
            hideHover: true,
            resize: true,
            gridLineColor: '#E5E5E5'
        }).on('click', function(i, row){
		    console.log(i, row);
        });

    });
    jQuery(document).ready(function($){
        $('#course-table').DataTable({
            "bFilter": false,
            "bLengthChange": false,
            "pageLength": 5
        });
    })
</script>