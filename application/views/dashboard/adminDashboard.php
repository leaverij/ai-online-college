<!--<link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/theme-default.css"/> -->
<script src="<?=base_url();?>assets/js/morris/morris.css" type="text/javascript" charset="utf-8"></script>

<style>
    .pure-counts {
        text-align:center;
        font-size:30px;
        color:#468847;
    }
</style>
<div class="container">
    <div>
        <div style="padding-bottom:20px;">
            <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">主管儀表板</h1>
        </div>
	</div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">學程數</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <p class="pure-counts"><?php echo $track_counts;?></p>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">課程數</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <p class="pure-counts"><?php echo $course_counts;?></p>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">教師數</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <p class="pure-counts"><?php echo $teacher_counts;?></p>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">學生數</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <p class="pure-counts"><?php echo $student_counts;?></p>
                </div>
            </div>
        </div>
    </div><!-- .row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">使用率</h3>
                    </div>                                                                  
                </div>     
                <div class="panel-body padding-0">
                   <!-- <canvas id="month-users" style="width:100%;"></canvas> -->
                   <div id="month-users" style="width:100%;"></div>  
                </div>                
            </div>
        </div>
    </div><!-- .row -->
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">裝置比例</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <div id="device-ratio" style="width:100%;"></div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">男女比例</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <div id="sex-ratio" style="width:100%;"></div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">領域學程完成比例</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <div id="domain-completion-ratio" style="width:100%;"></div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">類型花費時間比例</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <div id="type-time-spent-ratio" style="width:100%;"></div>
                </div>
            </div>
        </div>
    </div><!-- .row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">教師績效</h3>
                    </div>                                                                  
                </div>     
                <div class="panel-body padding-0">
                    <table class="table table-striped table-bordered" id="teacherP" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>姓名</th>
                                <th>課程數</th>
                                <th>平均註冊人數</th>
                                <th>平均完課率</th>
                                <th>吸睛數</th>
                                <th>互動分數</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $td_array = '';
                                foreach($teacher_performance as $teacher){
                                    $temp = '<tr>';
                                        // 姓名
                                        $temp .= '<td>'.$teacher['name'].'</td>';
                                        // 課程數
                                        $temp .= '<td>'.$teacher['course_count'].'</td>';
                                        // 平均註冊人數
                                        $temp .= '<td>'.$teacher['av_reg_count'].'</td>';
                                        // 平均完課率
                                        if($teacher['av_end_count']>80){
                                            $temp .= '<td>'.$teacher['av_end_count'].'%</td>';
                                        }else if($teacher['av_end_count']>60){
                                            $temp .= '<td>'.$teacher['av_end_count'].'%&nbsp;<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:#ec971f;"></i></td>';
                                        }else{
                                            $temp .= '<td>'.$teacher['av_end_count'].'%&nbsp;<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:red;"></i></td>';
                                        }
                                        // 	吸睛數
                                        $temp .= '<td>'.$teacher['gorgeous_num'].'</td>';
                                        // 互動分數
                                        if($teacher['interaction_num']>80){
                                            $temp .= '<td>'.$teacher['interaction_num'].'%</td>';
                                        }else if($teacher['interaction_num']>60){
                                            $temp .= '<td>'.$teacher['interaction_num'].'%&nbsp;<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:#ec971f;"></i></td>';
                                        }else{
                                            $temp .= '<td>'.$teacher['interaction_num'].'%&nbsp;<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:red;"></i></td>';
                                        }
                                    $temp .= '</tr>';
                                    $td_array .=$temp;
                                }
                                echo $td_array;
                            ?>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div><!-- .row -->
    
</div><!-- .container -->
<? $this->load->view('home/modal_login');?>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<!-- <script src="<?=base_url();?>assets/js/Chart.js" type="text/javascript" charset="utf-8"></script> -->
<script src="<?=base_url();?>assets/js/morris/raphael-min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/morris/morris.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/dataTables.bootstrap.js" type="text/javascript"></script>
<link href="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet">
<script>
    $(document).ready(function(){

        //註冊人數 v.s. 活躍人數
        Morris.Line({
            parseTime:false,
            element: 'month-users',
            data: <?php echo $usage;?>,
            xkey: 'y',
            ykeys: ['a', 'b'],
            labels: ['註冊人數', '活躍人數']
        });

        //裝置比例
        Morris.Donut({
		    element: 'device-ratio',
	  	    data: <?php echo $device_ratio;?>,
	  	    formatter: function (x, data) { return '比率：'+data.value+'%'; },
            colors: ['#33414E', '#1caf9a', '#FEA223', '#33cc33'],
        	resize: true
        });

        //男女比例
        Morris.Donut({
		    element: 'sex-ratio',
	  	    data: [
			    {label: "男", value: 80, formatted:'比率:80%'},
                {label: "女", value: 20, formatted:'比率:20%'}
	  	    ],
	  	    formatter: function (x, data) { return data.formatted; },
            colors: ['#33414E', '#1caf9a', '#FEA223', '#33cc33'],
        	resize: true
        });

        //領域學程完成比例
        Morris.Bar({
            element: 'domain-completion-ratio',
            data: <?php echo $complete_ratio;?>,
		    hoverCallback: function(index, options, content) {
			return(content);
		    },
            xkey: 'y',
            ykeys: ['a'],
		    units: '%',
		    labels: ['完成率'],
		    xLabelMargin: 0,
            barColors: ['#6495ED'],
            gridTextSize: '10px',
            hideHover: true,
            resize: true,
            gridLineColor: '#E5E5E5'
        }).on('click', function(i, row){
		    console.log(i, row);
        });
        
        //類型花費時間比例
        Morris.Donut({
		    element: 'type-time-spent-ratio',
	  	    data: [
                {label: "實作", value: 60, formatted:'比率:60%'},
			    {label: "文章", value: 15, formatted:'比率:15%'},
                {label: "編譯", value: 15, formatted:'比率:15%'},
                {label: "測驗", value: 10, formatted:'比率:10%'}
	  	    ],
	  	    formatter: function (x, data) { return data.formatted; },
            colors: ['#33414E', '#1caf9a', '#FEA223', '#33cc33'],
        	resize: true
        });
    });

    jQuery(document).ready(function($){
        $('#teacherP').DataTable({
            "bFilter": false,
            "bLengthChange": false,
            "pageLength": 5
        });
    })
</script>



































































