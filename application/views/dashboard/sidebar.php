<style>
.sidebar #dashboard-sidebar {margin-bottom:30px; background:#f5f5f5; border-radius:5px; box-shadow:0 1px 0 0 #ededed;}
#dashboard-sidebar ul li h4 {font-size:16px; margin:0;}
#dashboard-sidebar ul li h4 a:hover, #more-sidebar ul li.active a{text-decoration:none; background:#eeeeee; color:#24bbc9;}
#dashboard-sidebar ul li a {color:#666; font-size:15px; border-bottom:1px solid #e5e5e5;}
#dashboard-sidebar ul li:first-child a{border-radius:5px 5px 0 0;}
#dashboard-sidebar ul li:last-child a {border-radius:0 0 5px 5px; border-bottom:none;}
#dashboard-sidebar ul li a:hover {color:#24bbc9;}
#dashboard-sidebar .library-list li a {color:#888;}
#dashboard-sidebar .nav-stacked > li + li {margin-top:0;}
</style>
<div id="dashboard-sidebar" style="padding:0;">
    <ul class="nav nav-stacked">
         <li>
            <a href="#knowledge">
                <h4>知識課程</h4>                    
            </a>
         </li>
         <!-- <li class="">
            <a href="#technique">
                <h4>專案實作</h4>
            </a>
         </li> -->
         <!-- <li class=""><a href="#learningadventure">學習旅程</a></li> -->
         <li>
            <a href="#activity">
                <h4>歷史活動</h4>
            </a>
         </li>
         <li>
            <a href="#award_video">
                <h4>獎勵影片</h4>
            </a>
         </li>
         <!-- <li>
            <a href="<?=base_url()?>user/analysis">
                <h4>能力分析</h4>
            </a>
         </li> -->
    </ul>
</div>
