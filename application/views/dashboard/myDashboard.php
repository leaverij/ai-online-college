<div class="container">
	<div>
        <div style="padding-bottom:20px;">
            <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">個人儀表板</h1>
        </div>
	</div>
	<div class="row">
	<div class="sidebar col-md-3">
		<?php $this->load->view('dashboard/sidebar'); ?>
	</div>

<div class="col-md-9">
	<!-- 右邊區塊 -->
	<?php $row3 = json_decode($numOfCourseBadges); ?>
	<?php $row4 = json_decode($numOfUserCourseBadges); ?>
	<?php $row5 = json_decode($getUserActivity); ?>
	<?php $row6 = json_decode($getUserAwardVideo); ?>
	<?php if($this->session->userdata('email')){?>
	<div id="knowledge" class="hide">
		<!-- <h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">
        	知識類 <span style="font-size: 55%; vertical-align: middle; background-color: #c3c3c3;" class="label label-default"><?php echo $numOfKnowledges;?></span>
        </h3> -->
		<?php 
			$row2 = json_decode($knowledges);
			if($numOfKnowledges>0){
				$data=array('row2'=>$row2,'row3'=>$row3,'row4'=>$row4);
				$this->load->view('library/courseProgress',$data);
			}else{
		?>
			<h4 style="color:#a7a7a7;" class="text-center">您無進行中的知識課程</h4>
		<?php }?>
	</div>
	<div id="technique"  class="hide">
		<h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">
        	專案類 <span style="font-size: 55%; vertical-align: middle; background-color: #c3c3c3;" class="label label-default"><?php echo $numOfTechniques;?></span>
        </h3>
		<?php
			$row2 = json_decode($techniques); 
			if($numOfTechniques>0){
				$data=array('row2'=>$row2,'row3'=>$row3,'row4'=>$row4);
				$this->load->view('library/courseProgress',$data); 
			}else{
		?>
			<h4 style="color:#a7a7a7;" class="text-center">您無進行中的專案課程</h4>
		<?php }?>
	</div>
    
	<div id="activity" class="hide">
		<h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">
        	歷史活動
        </h3>
		<?php if(empty($row5)){?>
			<h4 style="color:#a7a7a7;" class="text-center">您還沒進行過任何的課程活動</h4>
		<?php }else{?>
			<?php foreach($row5 as $key5=>$value5){?>
            <ul style="list-style:none; padding:0;">
            	<li>
                	
                	<div style="padding:15px 0 15px 80px; border-bottom:1px solid #ededed;">
                		<img style="float:left; margin-left:-80px;" src="<?=base_url();?>assets/img/user/128x128/<?php echo $value5->img?>" width="50px"/>
						<h4 style="font-size:14px;">
							<a href="<?php echo base_url().'profile/'.$value5->alias;?>">
							<?php echo $value5->alias;?></a>
							<?php $_theme_model=new Theme_model();?>
							<?php 
							if($value5->content_type==0){
								echo '看了一個影片';
							}elseif($value5->content_type==1){
								echo '完成了一個測驗';
							}elseif($value5->content_type==2){
								echo '完成了一個挑戰';
							}
							?>
						</h4>
                        <div style="margin-bottom:10px;">
							<a style="cursor:pointer;" class="dashboard_challengeBtn" data-button='{"library":"<?php echo $value5->library_url;?>","course":"<?php echo $value5->course_url;?>","chapter":"<?php echo $value5->chapter_url;?>","content_order":"<?php echo $value5->content_order;?>","content_type":"<?php echo $value5->content_type;?>","chapter_content":"<?php echo $value5->chapter_content_url;?>"}'>
								<img style="float:left; margin-right:15px;" src="<?=base_url();?>assets/img/badges/<?php echo $value5->chapter_pic;?>" width="60"/>
							</a>
                            <div style="padding-top:10px;">
                                <a style="color:#565656;" href="<?php echo base_url().'library/'.$value5->library_url.'/'.$value5->course_url.'/'.$value5->chapter_url;?>">
                                    <strong style="font-size:1.3em; font-weight:600;"><?php echo $value5->chapter_name;?></strong>
                                </a>
                                <p style="margin-bottom:20px; color:#999999;"><?php echo $value5->library_name;?></p>
                            </div>
                            <div style="padding:10px; background:#f5f5f5; -webkit-border-radius:4px; -moz-border-radius:4px; -ms-border-radius:4px; border-radius:4px;">
                                <a style="font-size:16px;cursor:pointer;" class="dashboard_challengeBtn" data-button='{"library":"<?php echo $value5->library_url;?>","course":"<?php echo $value5->course_url;?>","chapter":"<?php echo $value5->chapter_url;?>","content_order":"<?php echo $value5->content_order;?>","content_type":"<?php echo $value5->content_type;?>","chapter_content":"<?php echo $value5->chapter_content_url;?>"}'>                        
									<?php if($value5->content_type==0){?>
                                        <i style="padding-right:3px; font-size:18px;" class="arrow_triangle-right_alt2"></i>
										<?php echo $value5->chapter_content_desc.' ('.$value5->duration.')';?>
                                    <?php }elseif($value5->content_type==1){?>
                                        <i style="padding-right:3px; font-size:18px;" class="fa fa-pencil"></i>
										<?php echo $value5->chapter_content_desc;?>
                                    <?php }elseif($value5->content_type==2){?>
                                        <i style="padding-right:3px; font-size:18px;" class="fa fa-keyboard-o"></i>
										<?php echo $value5->chapter_content_desc;?>
                                    <?php }?>
                                </a>
                            </div>
                        </div>
                        <p style="color:#bcbcbc;"><?php echo $_theme_model->calTimeElapsed($value5->accomplish_time);?></p>
                	</div>
                </li>
            </ul>
			<?php }?>
		<?php }?>
	</div>
	<div id="award_video" class="hide">
		<h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">
        	獎勵影片
        </h3>
		<?php if(empty($row6)){?>
			<h4 style="color:#a7a7a7;" class="text-center">您還未獲得任何的獎勵影片</h4>
		<?php }else{?>
			<?php foreach($row6 as $key6=>$value6){?>
            <ul style="list-style:none; padding:0;">
            	<li>
                	<div style="padding:15px 0 15px 70px; border-bottom:1px solid #ededed;">
                        <div style="margin-bottom:10px;">
							<a style="cursor:pointer;" class="award_video_challengeBtn" data-button='{"library":"<?php echo $value6->library_url;?>","course":"<?php echo $value6->course_url;?>","chapter":"<?php echo $value6->chapter_url;?>","content_order":"<?php echo $value6->content_order;?>","content_type":"<?php echo $value6->content_type;?>","chapter_content_url":"<?php echo $value6->chapter_content_url;?>","content":"<?php echo $value6->content;?>","chapter_content_desc":"<?php echo $value6->chapter_content_desc?>","email":"<?php echo $this->session->userdata('email');?>","subtitle":"<?php echo $value6->subtitle;?>","preview":"<?php echo $value6->subtitle;?>"}'>
								<img style="float:left; margin-right:15px;" src="<?=base_url();?>assets/img/badges/<?php echo $value6->chapter_pic;?>" width="60"/>
							</a>
                            <div style="padding-top:10px;">
                                <a style="color:#565656;" href="<?php echo base_url().'library/'.$value6->library_url.'/'.$value6->course_url.'/'.$value6->chapter_url;?>">
                                    <strong style="font-size:1.3em; font-weight:600;"><?php echo $value6->chapter_name;?></strong>
                                </a>
                                <p style="margin-bottom:20px; color:#999999;"><?php echo $value6->library_name;?></p>
                            </div>
                            <div style="padding:10px; background:#f5f5f5; -webkit-border-radius:4px; -moz-border-radius:4px; -ms-border-radius:4px; border-radius:4px;">
                                <a style="font-size:16px;cursor:pointer;" class="award_video_challengeBtn" data-button='{"library":"<?php echo $value6->library_url;?>","course":"<?php echo $value6->course_url;?>","chapter":"<?php echo $value6->chapter_url;?>","content_order":"<?php echo $value6->content_order;?>","content_type":"<?php echo $value6->content_type;?>","chapter_content_url":"<?php echo $value6->chapter_content_url;?>","content":"<?php echo $value6->content;?>","chapter_content_desc":"<?php echo $value6->chapter_content_desc?>","email":"<?php echo $this->session->userdata('email');?>","subtitle":"<?php echo $value6->subtitle;?>","preview":"<?php echo $value6->subtitle;?>"}'>                        
									<?php if($value6->content_type==0){?>
                                        <i style="padding-right:3px; font-size:18px;" class="arrow_triangle-right_alt2"></i>
                                    <?php }elseif($value6->content_type==1){?>
                                        <i style="padding-right:3px; font-size:18px;" class="fa fa-pencil"></i>
                                    <?php }elseif($value6->content_type==2){?>
                                        <i style="padding-right:3px; font-size:18px;" class="fa fa-keyboard-o"></i>
                                    <?php }?>
									<?php echo $value6->chapter_content_desc.' ('.$value6->duration.')';?>
                                </a>
                            </div>
							<p style="color:#bcbcbc;"><?php echo $_theme_model->calTimeElapsed($value6->accomplish_time);?></p>
                        </div>
                	</div>
                </li>
            </ul>
			<?php }?>
		<?php }?>
<div class="modal fade" id="award-video-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		<!-- <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title"></h4>
		</div> -->
			<div class="modal-body">
				<div class="col-md-8 col-md-offset-2 player_background" style="background:#393F48; padding:15px; border-radius:5px;">
					<div class="splash overlay_player play-button color-light">
							<div class="buttons hide">
								<span>0.25x</span>
								<span>0.5x</span>
								<span class="active">1x</span>
								<span>1.5x</span>
								<span>2x</span>
							</div>
						<div id="speedbtn" class="hide"></div>
						<div id="captionbtn" class="hide"></div>
					</div>
				</div>
			</div>
			<!-- <div class="modal-footer">
				<a href="." class="btn btn-primary" role="button">回到獎勵影片 <span class="glyphicon glyphicon-arrow-left"></span></a>
			        	</div> -->
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
	</div>
	<!-- 右邊end -->
	<? }?>
</div>
</div>
</div><!-- /container -->