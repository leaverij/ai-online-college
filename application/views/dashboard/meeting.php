<link href="<?=base_url();?>assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" />
<style>
    .pure-counts {
        text-align:center;
        font-size:30px;
        color:#468847;
    }
</style>
<div class="container">
    <div>
        <div style="padding-bottom:20px;">
            <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">課程直播分析</h1>
        </div>
	</div>
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>開始時間</label>
                        <div class="input-group date" id="since">
                            <input type="text" class="form-control" />
                            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>結束時間</label>
                        <div class="input-group date" id="until">
                            <input type="text" class="form-control" />
                            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label>快速篩選</label>
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default">今日</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default">今周</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default">今月</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default active">今年</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">不重複主持人數</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <p class="pure-counts">14</p>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">主持的直播數</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <p class="pure-counts">62</p>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">與會者總數</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <p class="pure-counts">347</p>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">時間總計</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <p class="pure-counts">85小時</p>
                </div>
            </div>
        </div>
    </div><!-- .row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">使用量</h3>
                    </div>                                                                  
                </div>                                
                <div class="panel-body padding-0">
                    <div id="unique-meeting-counts" style="width:100%;"></div>
                </div>
            </div>
        </div>
    </div><!-- .row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">直播明細</h3>
                    </div>                                                                  
                </div>     
                <div class="panel-body padding-0">
                    <table class="table table-striped table-bordered" id="teacherP" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>主題</th>
                                <th>主持人</th>
                                <th>課程</th>
                                <th>開始時間</th>
                                <th>時間(分)</th>
                                <th>觀看數</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Jupyter Q&A</td>
                                <td>曾筱倩</td>
                                <td>醫學影像處理</td>
                                <td>2018-01-01 11:00:00</td>
                                <td>40</td>
                                <td>45</td>
                            </tr>
                            <tr>
                                <td>Keras Q&A</td>
                                <td>林展賢</td>
                                <td>Keras建模</td>
                                <td>2017-12-22 14:30:00</td>
                                <td>30</td>
                                <td>28</td>
                            </tr>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </div><!-- .row -->
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/moment.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/morris/raphael-min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/morris/morris.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/dataTables.bootstrap.js" type="text/javascript"></script>
<link href="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet">
<script>
$(document).ready(function(){
    $('.date').datetimepicker({
        format:'YYYY-MM-DD HH:mm:ss',
        minDate:new Date(),
        defaultDate:new Date(),
        showClose:true
    });
    //註冊人數 v.s. 活躍人數
    Morris.Line({
        parseTime:false,
        element: 'unique-meeting-counts',
        data: <?php echo $usage;?>,
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['不重複的直播主持人數', '主持的直播數']
    });
});
</script>