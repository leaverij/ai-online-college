<style>
    .course-table {
        word-break: break-all;
    }
    .course-table tbody tr td {
        width:100px;
        height:20px;
        text-overflow: ellipsis;
        text-align:center;
    }
    .chapter-title,
    .chapter-content-title {
        text-align:center;
        vertical-align:middle;
    }
    .bg-default {
        background-color:#f2efef;
    }
    .bg-success {
        backgroud-color:#5cb85c;
    }
    .bg-warning {
        background-color:#f0ad4e;
    }
    .bg-danger {
        background-color:#d9534f;
    }
</style>
<?php
    $course_content = json_decode($course);
?>
<div class="container">
    <div>
        <div class="text-left">
		    <ol class="breadcrumb">
                <li><a href="<?php echo base_url().'dashboard/teacher'?>">儀表板</a></li>
                <li>課程儀表板</li>
            </div>
        </div>
        <div style="padding-bottom:20px;">
            <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;"><?php echo $course_content->course_name ?></h1>
        </div>
        <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">關卡通過率</h3>
                    </div>                                                                  
                </div>     
                <div class="panel-body padding-0">
                   <div id="chapter-completion-ratio" style="width:100%;"></div>  
                </div>                
            </div>
        </div>
    </div><!-- .row -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3 class="tms-panel-h3">關卡/任務概況</h3>
                    </div>                                                                  
                </div>     
                <div class="panel-body padding-0">
                    <div class="btn-group-parent btn-group btn-group-justified" role="group" aria-label="...">
                        <!--
                        <div class="btn-group" role="group">
                            <button data-type="progress" type="button" class="progress-btn btn btn-default active">進度</button>
                        </div>
                        -->
                        <div class="btn-group" role="group">
                            <button data-type="count" type="button" class="progress-btn btn btn-default active">點擊次數</button>
                        </div>
                        <div class="btn-group" role="group">
                            <button data-type="time" type="button" class="progress-btn btn btn-default">花費時間</button>
                        </div>
                    </div><br />
                    <table id="course-count-table" class="table table-bordered course-table" style="overflow: scroll;overflow: auto;">
                        <?php
                            $table = '';
                            foreach($chapter_content as $row_index=>$content){ 
                                if($row_index=='chapter'){
                                    $table .= '<thead>';
                                    $table .= '<tr>';
                                    $table .= '<td></td>';
                                    foreach($content as $content_data){
                                        $table .= '<td class="chapter-title" colspan="'.$content_data['span'].'">'.$content_data['name'].'</td>';
                                    }
                                    $table .= '</tr>';
                                }else if($row_index=='chapter_content_name'){
                                    $table .= '<tr>';
                                    $table .= '<td></td>';
                                    foreach($content as $chapter_content_data){
                                        $table .= '<td class="chapter-content-title">'.$chapter_content_data.'</td>';
                                    }
                                    $table .= '</tr>';
                                }else if($row_index=='chapter_content_type'){
                                    $table .= '<tr>';
                                    $table .= '<td></td>';
                                    foreach($content as $chapter_content_data){
                                        $table .= '<td class="chapter-content-title"><i class="';
                                        if($chapter_content_data==1){
                                            $table .= 'icon_check_alt check';
                                        }elseif($chapter_content_data==0){
                                            $table .= 'arrow_triangle-right_alt2';
                                        }elseif($chapter_content_data==1){
                                            $table .= 'fa fa-pencil';
                                        }elseif($chapter_content_data==2){
                                            $table .= 'fa fa-pencil';
                                        }elseif($chapter_content_data==3){
                                            $table .= 'fa fa-file-text-o';
                                        }elseif($chapter_content_data==4){
                                            $table .= 'fa fa-flask';
                                        }elseif($chapter_content_data==5){
                                            $table .= 'fa fa-window-maximize';
                                        }
                                        $table .= '"></i></td>';
                                    }
                                    $table .= '</tr>';
                                    $table .= '</thead>';
                                }else if($row_index=='couser_student'){
                                    $table .= '<tbody>';
                                    foreach($content as $content_data){
                                        $table .= '<tr>';
                                        foreach($content_data as $row=>$student_data){ 
                                            if($row==0){
                                                $table .= '<td>'.$student_data.'</td>';
                                            }else if($student_data=='0'){
                                                $table .= '<td></td>';
                                            }else{
                                                $table .= '<td class="bg-default">'.$student_data.'</td>';
                                            }
                                        }
                                        $table .= '</tr>';
                                    }
                                    $table .= '</tbody>';
                                }   
                            }
                            echo $table;
                        ?>
                    </table>
                    <table id="course-time-table" class="table table-bordered course-table" style="display:none;">
                        <tbody>
                            <tr>
                                <td></td>
                                <td class="chapter-title">機器分類訓練方式</td>
                                <td class="chapter-title" colspan="3">DIGITS創建資料集</td>
                                <td class="chapter-title" colspan="3">DIGITS訓練模型</td>
                                <td class="chapter-title" colspan="7">醫學影像運用</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="chapter-content-title">何謂分類</td>
                                <td class="chapter-content-title">DIGITS創建資料集說明</td>
                                <td class="chapter-content-title">創建自己的資料集</td>
                                <td class="chapter-content-title">牛刀小試</td>
                                <td class="chapter-content-title">DIGITS訓練模型</td>
                                <td class="chapter-content-title">訓練自己的模型</td>
                                <td class="chapter-content-title">正確的模型</td>
                                <td class="chapter-content-title">醫學影像運用</td>
                                <td class="chapter-content-title">建立糖尿病視網膜病變資料集</td>
                                <td class="chapter-content-title">自己動手建立資料集</td>
                                <td class="chapter-content-title">建立糖尿病視網膜病變模型</td>
                                <td class="chapter-content-title">自己動手建立模型</td>
                                <td class="chapter-content-title">測試建立的模型</td>
                                <td class="chapter-content-title">自己動手測試模型</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="chapter-content-title"><i class="fa fa-file-text-o"></i></td>
                                <td class="chapter-content-title"><i class="fa fa-file-text-o"></i></td>
                                <td class="chapter-content-title"><i class="fa fa-keyboard-o"></i></td>
                                <td class="chapter-content-title"><i class="fa fa-pencil"></i></td>
                                <td class="chapter-content-title"><i class="fa fa-file-text-o"></i></td>
                                <td class="chapter-content-title"><i class="fa fa-keyboard-o"></i></td>
                                <td class="chapter-content-title"><i class="fa fa-file-text-o"></i></td>
                                <td class="chapter-content-title"><i class="fa fa-file-text-o"></i></td>
                                <td class="chapter-content-title"><i class="fa fa-file-text-o"></i></td>
                                <td class="chapter-content-title"><i class="fa fa-keyboard-o"></i></td>
                                <td class="chapter-content-title"><i class="fa fa-file-text-o"></i></td>
                                <td class="chapter-content-title"><i class="fa fa-flask"></i></td>
                                <td class="chapter-content-title"><i class="fa fa-file-text-o"></i></td>
                                <td class="chapter-content-title"><i class="fa fa-flask"></i></td>
                            </tr>
                            <tr>
                                <td>江**</td>
                                <td class="bg-default">3:10</td>
                                <td class="bg-default">5:24</td>
                                <td class="bg-default">22:32</td>
                                <td class="bg-default">4:25</td>
                                <td class="bg-default">3:25</td>
                                <td class="bg-default">18:21</td>
                                <td class="bg-default">2:41</td>
                                <td class="bg-default">2:34</td>
                                <td class="bg-default">3:02</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>胡**</td>
                                <td class="bg-default">4:10</td>
                                <td class="bg-default">5:24</td>
                                <td class="bg-default">18:34</td>
                                <td class="bg-default">2:21</td>
                                <td class="bg-default">2:45</td>
                                <td class="bg-default">25:35</td>
                                <td class="bg-default">5:20</td>
                                <td class="bg-default">4:18</td>
                                <td class="bg-default">4:45</td>
                                <td class="bg-default">6:30</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>李**</td>
                                <td class="bg-default">4:20</td>
                                <td class="bg-default">6:13</td>
                                <td class="bg-default">30:34</td>
                                <td class="bg-default">5:22</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>李**</td>
                                <td class="bg-default">3:25</td>
                                <td class="bg-default">5:20</td>
                                <td class="bg-default">27:51</td>
                                <td class="bg-default">3:20</td>
                                <td class="bg-default">5:57</td>
                                <td class="bg-default">20:34</td>
                                <td class="bg-default">6:23</td>
                                <td class="bg-default">5:13</td>
                                <td class="bg-default">5:45</td>
                                <td class="bg-default">22:31</td>
                                <td class="bg-default">4:41</td>
                                <td class="bg-default">18:11</td>
                                <td class="bg-default">4:36</td>
                                <td class="bg-default">19:43</td>
                            </tr>
                        </tbody>
                    </table> 
                </div>                
            </div>
        </div>
    </div><!-- .row -->
    </div>
</div><!-- .container -->
<? $this->load->view('home/modal_login');?>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/morris/raphael-min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/morris/morris.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/highcharts.js" type="text/javascript" charset="utf-8"></script>
<script>
    $(document).ready(function(){
        var userChapter = <?php echo $course_chapter;?>;
        //關卡通過率
        Highcharts.chart('chapter-completion-ratio', {
            chart:{
                type:'column'
            },
            title:{
                text:null
            },
            credits:{
                enabled:false
            },
            xAxis:{
                categories: userChapter.categories
            },
            yAxis:{
                title:{
                    text:null
                },
                min:0
            },
            plotOptions:{
                column:{
                    stacking:'percent'
                }
            },
            series:[{
                name:'未完成',
                data:userChapter.unfinish
            },{
                name:'完成',
                data:userChapter.finish
            }]
        });

        $('.progress-btn').on('click', function(){
            var btn = $(this);
            btn.closest('.btn-group-parent').find('.btn-group button').removeClass('active');
            btn.addClass('active');
            var view_type = btn.attr('data-type');
            $('.course-table').hide();
            $('#course-'+view_type+'-table').show();
        });

    });
</script>