</div><!--/wrap--> 
<!--     FOOTER     -->
<style>
footer{background:url(<?=base_url();?>assets/img/LH-footer-bg.jpg); background-color:#787878; height:60px; color:#999; padding:20px 0;}
footer a{color:#3cc; font-size:13px; margin-right:10px;}
</style>
<footer>
	<div class="container">
    	<div class="row">
        	<div class="col-md-8">
            	<a href="http://ailt.iiiedu.org.tw/"><?=$this->lang->line('about_us');?></a>
                <a href="<?=base_url();?>page/terms">服務條款</a>
				<!-- <script>(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/Gsfw5yi9Qo4hpJQ15P7A.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()</script> -->
				<!-- <a href="javascript:void(0)" data-uv-lightbox="classic_widget" data-uv-mode="full" data-uv-primary-color="#cc6d00" data-uv-link-color="#007dbf" data-uv-default-mode="support" data-uv-forum-id="235255" data-uv-support-tab_name="問題反應" data-uv-feedback-tab_name="提供意見">問題反應</a> -->
				<!-- <a href="<?=base_url();?>page/service">問題反應</a> -->
                <a href="<?=base_url();?>page/privacy">隱私權政策</a>
                Copyright © AI Online College  2017
			</div>
			<div class="col-md-4">
				<select id="langpicker" style="float:right;width:50%;height:30px;">
					<option value="zh-tw" <?php if($this->session->userdata('locale')=='zh-tw' || $this->session->userdata('locale')=='')echo 'selected';?>>繁體中文</option>
					<option value="zh-cn" <?php if($this->session->userdata('locale')=='zh-cn')echo 'selected';?>>简体中文</option>
					<option value="english" <?php if($this->session->userdata('locale')=='english')echo 'selected';?>>English</option>
				</select>
            </div>
            <!-- <div class="col-md-4">
            	<span class="pull-right">Page rendered in <strong>{elapsed_time}</strong> seconds</span>
            </div> -->
        </div>
    </div>
</footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?=base_url();?>assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?=base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- bootstrap-switch -->
	<script src="<?=base_url();?>assets/bootstrap/js/bootstrap-switch.js"></script>
	<!-- include flowplayer -->
	<script type="text/javascript" src="<?=base_url();?>assets/js/flowplayer/flowplayer.min.js"></script>
    <!-- jQuery Md5 -->
	<script src="<?=base_url();?>assets/js/jQuery.md5.js"></script>
	<!-- Chart -->
	<script src="<?=base_url();?>assets/js/Chart.js"></script>
	<script src="<?=base_url();?>assets/js/jquery-validation/jquery.validate.js"></script>
	<!-- jQuery UI DateTimePicker
	<script src="<?=base_url();?>assets/js/jquery-ui-timepicker-addon/jquery-ui.js"></script> -->
	<!-- <script src="<?=base_url();?>assets/js/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.js"></script> -->
	<script src="<?=base_url();?>assets/js/jquery-ui-timepicker-addon/jquery-ui-sliderAccess.js"></script>
	<!-- <script src="<?=base_url();?>assets/js/jquery-ui-timepicker-addon/custom-datetimepicker.js"></script> -->
	<!-- Parser URL -->
	<script src="<?=base_url();?>assets/js/purl.js"></script>
	<!-- Block -->
	<script src="<?=base_url();?>assets/js/jquery.blockUI.js"></script>
    <!-- Masonry -->
	<script src="<?=base_url();?>assets/js/masonry/masonry.pkgd.min.js"></script>
    <!-- dotdotdot -->
    <script src="<?=base_url();?>assets/js/jQuery.dotdotdot-master/jquery.dotdotdot.js" type="text/javascript"></script>
	<!-- Pjax -->
	<script src="<?=base_url();?>assets/js/jquery-pjax.js"></script>
	<!--bootstrap-datepicker-->
	<script src="<?=base_url();?>assets/bootstrap/js/moment.js"></script>
	<script src="<?=base_url();?>assets/bootstrap/js/bootstrap-datetimepicker.js"></script>
	<!--bootstrap-typeahead-->
	<script src="<?=base_url();?>assets/bootstrap/js/typeahead.bundle.js"></script>
	<!--bootstrap-bootbox-->
	<script src="<?=base_url();?>assets/bootstrap/js/bootbox.min.js"></script>
	<script src="<?=base_url();?>assets/js/custom-tin-can.js"></script>
	<script src="http://ace.c9.io/build/src/ace.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=base_url();?>assets/js/custom.js"></script>
	<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
	<script src="<?=base_url();?>assets/js/jquery-ui/jquery-ui.min.js"></script>
<?php 
	if(!empty($getUserForumSubscription)){
		$row2=json_decode($getUserForumSubscription);
		$channel_string='';
		for($i=0;$i<count($row2);$i++){
			if(!isset($row2[$i+1]->forum_topic_id)){
				$temp='forumContent_'.$row2[$i]->forum_topic_id;
				$channel_string.=$temp;
			}else{
				$temp='forumContent_'.$row2[$i]->forum_topic_id.',';
				$channel_string.=$temp;		
			}
		}
// 			echo 'channel_string = '.$channel_string.'<br/>';
	}
	if(!empty($getUserSupportLibraryId)){
		$row3=json_decode($getUserSupportLibraryId);
		$support_channel_string='';
		for($i=0;$i<count($row3);$i++){
			if(!isset($row3[$i+1]->forum_topic_id)){
				$temp='library_'.$row3[$i]->library_id;
				$support_channel_string.=$temp;
			}else{
				$temp='library_'.$row3[$i]->library_id.',';
				$support_channel_string.=$temp;
			}
		}
// 					echo 'support_channel_string = '.$support_channel_string.'<br/>';
	}
?>
	<script>
		jQuery(document).ready(function($){
			$('#langpicker').change(function(){
				const lang = $('#langpicker').val();
				window.location = "<?=base_url();?>user/locale/"+lang
			})
		})
	</script>
  </body>
</html>