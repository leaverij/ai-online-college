<!--     BLOCK LH-INTRODUTION     -->
<section id="intro">
	<div class="container">
    	<div class="row">
        	<div class="col-md-8">
            	<div class="intro-content">
                	<h1>專家帶你學AI</h1>
                    <p class="subintro"><!-- 從這裡的專業課程找到提升你知識和技能的答案 --></p>
                    <!-- <p class="subintro">已經有 <span id="amount"><?//php echo $amount['amount'];?></span> 名學習者註冊</p> -->
                </div>
                	<a class="btn btn-warning btn-lg" href="<?php echo base_url().'library'?>" role="button">開始學習</a>
                	<a class="btn btn-success btn-lg phone-login" style="display: none;" href="<?=base_url();?>home/login" role="button">登入/註冊</a>
            </div>
        </div>
    </div>
</section>

<style>
#ai-assessment { 
    background-image:url('http://aioc.iiiedu.org.tw/assets/img/ai-back.png'); 
    background-size: cover; 
    display:block;  
    height:300px;
}
.center {
    text-align: center;
    margin-top: 90px;
}
</style>
<!--
<section id="ai-assessment">
    <div class="container" style="padding:50px 0;">
        <div class="row">
            <div class="col-md-4">
                <div class="intro-content">
                    <h1>AI準備度檢測</h1>
                    <p class="subintro">人工智慧時代來臨，想要不被機器取代，就要有定義與解決問題的能力!</p>
                </div>
            </div>
            <div class="col-md-4 center">
                <a href="<?php echo base_url().'page/preparation'?>" class="btn btn-warning btn-lg">馬上檢測</a>
            </div>
            <div class="col-md-4">

            </div>
        </div>
    </div>
</section>
--> 
<!--     BLOCK FEATURE     -->
<style>
#lh-feature .feature-box{text-align:center;}
#lh-feature .feature-box img{width:100px;}
#lh-feature .feature-meta{padding:0 20px; margin-bottom:30px;}
#lh-feature .feature-meta p{color:#767676; font-size:1.125em; line-height:1.6em;}
#lh-feature .feature-meta a{color:#24bbc9;}
#lh-feature .feature-meta a i{margin-left:2px; color:#6cd0d9;}
</style>
<section id="lh-feature">
	<div class="container" style="padding:50px 0;">
        <div class="row">
            <div class="col-md-3 feature-box">
                <img src="<?=base_url();?>assets/img/lh-feature-path.png" style="margin-bottom:20px;"  />
                <div class="feature-meta">
                    <h4>最佳學習指引</h4>
                    <p>人工智慧所需的資訊技能/職能養成，跟著途徑達成目標！</p>
                </div>
            </div>
            <div class="col-md-3 feature-box">
                <img src="<?=base_url();?>assets/img/lh-feature-steps.png" style="margin-bottom:20px;"  />
                <div class="feature-meta">
                    <h4>自己決定步調</h4>
                    <p>不必擔心跟不上進度，隨你的喜好，想學幾次就學幾次</p>
                </div>
            </div>
            <div class="col-md-3 feature-box">
                <img src="<?=base_url();?>assets/img/lh-feature-skills.png" style="margin-bottom:20px;"  />
                <div class="feature-meta">
                    <h4>學習變得輕鬆</h4>
                    <p>一指啟動深度學習所需建構的環境，遠離繁瑣的設定</p>
                </div>
            </div>
            <div class="col-md-3 feature-box">
                <img src="<?=base_url();?>assets/img/lh-feature-plus.png" style="margin-bottom:20px;"  />
                <div class="feature-meta">
                    <h4>遞出漂亮履歷</h4>
                    <p>持續學習取得成就，你的經驗值只會更多，不會減少:D</p>
                </div>
            </div>      
        </div>
    </div>
</section>

<!--     BLOCK BEST LECTURE     -->
<style>
#lh-best-lecture .lecture-box{text-align:center; background:#f6f9fa; padding:15px; margin-bottom:30px; -webkit-border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; border-radius:5px; -webkit-box-shadow:0 3px #d9e4e8; -moz-box-shadow:0 3px #d9e4e8; -ms-box-shadow:0 3px #d9e4e8; -o-box-shadow:0 3px #d9e4e8; box-shadow:0 3px #d9e4e8;}
#lh-best-lecture .lecture-box h4{margin-bottom:0; height:50px; line-height:1.4em;}
#lh-best-lecture .lecture-box a{color:#757575;}
#lh-best-lecture .lecture-box a:hover{color:#24bbc9; text-decoration:none;}
#lh-best-lecture .lecture-box img{max-width:100%; max-height:120px;}
</style>
<section id="lh-best-lecture" style="background:#edeff0;">
    <div class="container" style="padding:50px 0;">
    	<h3 style="color: #666; letter-spacing: 0.1em; text-align: center;">最新推薦學程</h3>
        <div class="row" style="margin:50px 0;">
            <!-- <?php $row = json_decode($partialCourse); ?>
            <?foreach($row as $key=>$val){?>
              <div class="col-md-3">
              	<div class="lecture-box">
                  <img src="<?=base_url();?>assets/img/<?php echo $val->course_pic?>" height="120px" />
                  <h4><?php echo $val->course_name?></h4>
              	 </div>
              </div>
            <?}?> -->
            <div class="col-md-3">
              	<div class="lecture-box">
                  <a href="<?=base_url();?>track/trackContent/ai-hospital-image"><img src="<?=base_url();?>assets/img/track/ai-vision.jpg" /></a>
                  <h4><a href="<?=base_url();?>track/trackContent/ai-hospital-image">醫學影像處理與分析</a></h4>
              	</div>
            </div>
            <div class="col-md-3">
              	<div class="lecture-box">
                  <a href="<?=base_url();?>track/trackContent/ai-hospital-data-analytics"><img src="<?=base_url();?>assets/img/track/ai-data.jpg" /></a>
                  <h4><a href="<?=base_url();?>track/trackContent/ai-hospital-data-analytics">醫療數據分析</a></h4>
              	</div>
            </div>
            <div class="col-md-3">
              	<div class="lecture-box">
                  <a href="<?=base_url();?>track/trackContent/ai-finance-credit"><img src="<?=base_url();?>assets/img/track/ai-finance-loan.jpg" /></a>
                  <h4><a href="<?=base_url();?>track/trackContent/ai-finance-credit">金融信貸應用</a></h4>
              	</div>
            </div>
            <div class="col-md-3">
              	<div class="lecture-box">
                  <a href="<?=base_url();?>track/trackContent/ai-business-marketing"><img src="<?=base_url();?>assets/img/track/ai-business-marketing.jpg"/></a>
                  <h4><a href="<?=base_url();?>track/trackContent/ai-business-marketing">商業經營與行銷管理</a></h4>
              	</div>
            </div>
        </div>
    </div>
</section>

<!--     FOOTER/LIBRARY LINK     -->
<style>
#footer-library{background:url(<?=base_url();?>assets/img/LH-footer-bg.jpg); background-color:#787878; padding:80px 0;}
#footer-library .link-list{list-style:none; padding:0;}
#footer-library .link-list a{color:#cdcdcd;}
#footer-library .link-list .list-heading{font-size:16px; margin-bottom:5px;}
#footer-library .link-list .list-heading a{color:#3CC;}
#footer-library .link-list li{color:#cdcdcd; font-size:13px;}
@media screen and (max-width: 480px) {.teacherRecruit {margin-bottom:50px;}}
@media screen and (max-width: 768px) {.teacherRecruit {margin-bottom:50px;}}
	
</style>
<section id="footer-library">
	<div class="container">
    	<div class="row">
        	<!-- Teacher Recruit -->
            <div class="col-sm-5 col-sm-push-7">
                <div class="teacherRecruit" style="padding-left:95px;">
                	<img src="<?=base_url();?>assets/img/teacher_recruit.png" style="float:left; width:80px; margin-left:-95px;" />
                    <h4 style="color:#f6f9fa; margin-top:0;">讓您的教學熱情被全世界看見!</h4>
                    <p style="color:#cdcdcd;">我們誠摯邀請具教學熱情、喜愛分享知識的您，與 AI Online College 一同努力被全世界的人看見。</p>
                	<!-- <a href="" style="color:#67e5dc;" data-toggle="modal" data-target="#teacherRecruitModal">成為 AI Online College 合作夥伴</a> -->
                </div>
                
				<?php $this->load->view('page/teacherRecruit');?>
            </div>
        	<div class="col-sm-7 col-sm-pull-5">
            	<div class="row">
                    <div class="col-sm-4">
                        <ul class="link-list">
                        	<li class="list-heading">
                            	<a href="<?=base_url().'library/';?>">課程分類</a>
                            </li>
                            <?php $row = json_decode( $getAll );?>
                            <?php foreach($row as $key=>$val){?>
                            	<?if($key<6){?>
                            		<li>
										<a href="<?php echo base_url().'library/'.$val -> library_url?>"><?php echo $val -> library_name; ?></a>
									</li>	
                            	<?}?>
							<?} ?>
                        </ul>   
                    </div>
                    <div class="col-sm-4" style="margin-top:27px;">
                        <ul class="link-list">
                            <?php $row = json_decode( $getAll );?>
                            <?php foreach($row as $key=>$val){?>
                            	<?if($key>5){?>
                            		<li>
										<a href="<?php echo base_url().'library/'.$val -> library_url?>"><?php echo $val -> library_name; ?></a>
									</li>	
                            	<?}?>
							<?} ?>
                        </ul>   
                    </div>
                    <!-- <div class="col-sm-4">
                        <ul class="link-list">
                        	<li class="list-heading">
                            	<a>工作能力/學程</a>
                            </li>
                            <li>
                            	<a>成為 Java 開發工程師</a>
                            </li>
                            <li>
                            	<a>成為 Mobile 開發工程師</a>
                            </li>
                        </ul>
                    </div> -->
                    <!-- <div class="col-sm-4">
                        <ul class="link-list">
                        	<li class="list-heading">
                            	<a>課程主題</a>
                            </li>
                            <li>
                            	<a href="<?=base_url();?>library/web-application/apache-struts2-framework">Apache Struts2 Framework</a>
                            </li>
                            <li>
                            	<a href="<?=base_url();?>library/web-application/android-ui">Android UI 設計與多媒體功能</a>
                            </li>
                            <li>
                            	<a href="<?=base_url();?>library/web-application/hello-objective-c">Objective-C 基礎入門</a>
                            </li>
                        </ul>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
<section/>