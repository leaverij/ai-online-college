<style>
.login-modal .login-content {padding:30px;}
.login-content label {font-size:1.14em; font-weight:normal;}
@media screen and (max-width: 480px) {
	.login-modal .login-content {padding:15px;}
}
</style>
<div class="container">
	<div class="row basic-data">
    	<div class="login-modal">
            <div class="col-sm-6" style="margin:0 auto; float:none;">
            	<div class="login-content">
            		<div class="message">
				<? if($this->session->flashdata('alert')):?>
				<? $data=$this->session->flashdata('alert'); ?>
    				<div class="alert <?=$data['type']?> fade in">
  						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
 						<?=$data['msg']?>
					</div>
				<? endif; ?>
				</div>
                	<div style="padding-bottom:20px;">
                		<h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">會員登入</h1>
                    </div>
                    <form id="loginForm" role="form" action="<?=base_url()?>home/login" method="post">
                      <div class="form-group">
                        <label for="exampleInputEmail1">信箱</label>
                        <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="登入Email" value="">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">密碼</label>
                        <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="密碼" value="">
                      </div>
                      <div class="form-group">
                        <button type="submit" class="btn btn-success" style="margin-bottom:15px;">登入</button>
                        <!--
						<a href="javascript: void(0)" id="fb_login" class="btn btn-default" style="margin-bottom:15px;" onclick="window.location='<?=$login_url?>'">
                            <img src="<?php echo base_url()?>assets/img/facebook.png" width="20px" style="float: left;margin-right: 5px;">
                            <span>Facebook登入</span>  
                        </a>
						-->
                        <a href="<?php echo base_url().'user/forgetPassword'?>" class="btn btn-link" style="margin-bottom:15px;">忘記密碼</a>
                        <a href="<?php echo base_url().'home/registration'?>" class="btn btn-link" style="margin-bottom:15px;">註冊AI Online College</a>
                        <input type="hidden" name="requestURL"	value="<?php echo $this->session->userdata('src');?>"><!--送出本網頁的路徑給Controller-->
                        <input type="hidden" name="action"	value="login">
                      </div>
                    </form>
                </div>
            </div>
    	</div>
    </div>

	<!-- 
	<div class="row row-offcanvas row-offcanvas-right basic-data">
        <div class="col-xs-12 col-sm-12">
        	<div class="row">
				<div class="col-md-12">
						<div class="login">
							<form id="loginForm" class="form-horizontal col-sm-offset-3" role="form" action="<?=base_url()?>home/login"method="post">
								<div class="text-left">
            						<h2>LearningHouse會員登入</h2>
            					</div>
            					<div class="col-sm-offset-1">
  								<div class="form-group">
    								<label for="inputEmail" class="col-sm-2 control-label">信箱：</label>
    								<div class="col-sm-4">
     									 <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="登入Email">
    								</div>
  								</div>
  								<div class="form-group">
    								<label for="inputPassword" class="col-sm-2 control-label">密碼：</label>
   									<div class="col-sm-4">
      									<input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="密碼">
    								</div>
  								</div>
  								<div class="form-group">
    								<span class="col-sm-offset-2 col-sm-10">
    									<button type="button" class="btn btn-info" id="login_reset">重置</button>
      									<button type="submit" class="btn btn-success">登入</button>
      									<a href="javascript: void(0)" id="fb_login" onclick="window.location='<?=$login_url?>'" class="btn btn-default">
                            				<img src="<?php echo base_url()?>assets/img/facebook.png" width="20px" style="float: left;margin-right: 5px;">
                            				<span>Facebook登入</span>  
                    					</a>
      									<a href="<?php echo base_url().'user/forgetPassword'?>" class="btn btn-link" role="button">忘記密碼</a>
      									<input type="hidden" name="requestURL"	value="<?php echo $this->session->userdata('src');?>">送出本網頁的路徑給Controller
      									<input type="hidden" name="action"	value="login">
    								</span>
  								</div>
  								</div>
							</form>
            			</div>
        		</div>
			</div>
		</div>
	</div>
    -->
</div>