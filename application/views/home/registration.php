<style>
.basic-data ,.credit-card {display:none}
.registration-modal .registration-content {padding:30px;}
.registration-content label {font-size:1.14em; font-weight:normal;}
@media screen and (max-width: 480px) {
	.registration-modal .registration-content {padding:15px;}
}
</style>
<div id="content">
    <div class="container">
        <div class="row row-offcanvas row-offcanvas-right add-member-desc">
            <div class="col-xs-12 col-sm-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="jumbotron">
                            <div class="registration">
                                <div class="text-left">
                                    <h2>加入 AI Online College ,立即享受新學習體驗&raquo;</h2>
                                </div>
                                <!--<p>歡迎加入AI Online College會員,本網站提供許多人工智慧領域應用的 On Job Trainning，您不需要事先理解晦澀難懂的演算法，讓專家透過直接實作帶領您進入AI的世界!</p> -->
                                <p><b>AI Online College 蒐集個人資料告知事項暨個人資料提供同意書</b></p>
                                <p><b>AI Online College(下稱 AIOC)</b>為遵守個人資料保護法令及本會個人資料保護政策、規章，於向您蒐集個人資料前，依法向您告知下列事項，敬請詳閱。</p>
                                <table class="table table-bordered table-hover">
        <tr>
            <td width="100">一、蒐集目的及類別</td>
            <td>AIOC 因辦理或執行業務、活動、計畫、提供服務及供 AIOC 用於內部行政管理、陳報主管機關、寄送 AIOC 產業相關活動訊息之蒐集目的，而需獲取您下列個人資料類別：姓名、聯絡方式(如電子信箱)，或其他得以直接或間接識別您個人之資料。<br />※您日後如不願再收到 AI Online College 平台寄送之行銷訊息，可於收到前述訊息時，直接點選訊息內拒絕接受之連結。</td>
        </tr>
        <tr>
            <td width="100">二、個人資料利用之期間、地區、對象及方式</td>
            <td>除涉及國際業務或活動外，您的個人資料僅供 AIOC 於中華民國領域、在前述蒐集目的之必要範圍內，以合理方式利用至蒐集目的消失為止。</td>
        </tr>
        <tr>
            <td width="100">三、當事人權利</td>
            <td>
                您可依前述業務、活動所定規則或依 AIOC（http://aioc.iiiedu.org.tw/）「個資當事人行使權利專頁」公告方式向本會行使下列權利：<br /> 
                (一)查詢或請求閱覽。<br /> 
                (二)請求製給複製本。<br /> 
                (三)請求補充或更正。<br /> 
                (四)請求停止蒐集、處理及利用。<br /> 
                (五)請求刪除您的個人資料。
            </td>
        </tr>
        <tr>
            <td width="100">四、不提供正確個資之權益影響</td>
            <td>若您不提供正確之個人資料予本平台，本平台將無法為您提供特定目的之相關服務。</td>
        </tr>
        <tr>
            <td colspan="2">五、您瞭解此一同意書符合個人資料保護法及相關法規之要求，且同意 AIOC 留存此同意書，供日後取出查驗。</td>
        </tr>
        <tr>
            <td colspan="2">
                <p style="text-align:center;">個人資料之同意提供：</p>
                一、本人已充分獲知且已瞭解上述 AIOC 告知事項。<br />
                二、本人同意 AIOC 於所列蒐集目的之必要範圍內，蒐集、處理及利用本人之個人資料。
                <p style="text-align:right; font-size:14px;">日期：民國106年11月21日</p>
                <p style="text-align:right; font-size:14px;">版本：P-V4-DEI</p>
            </td>
        </tr>
    </table>
                                <div class="text-center">
                                    <button id="add-member" type="button" class="btn btn-lg btn-success">同意</button>
                                    <a href="http://aioc.iiiedu.org.tw/" type="button" class="btn btn-lg btn-default">不同意</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row basic-data">
            <div class="registration-modal">
                <div class="col-sm-6" style="margin:0 auto; float:none;">
                    <div class="registration-content">
                        <div style="padding-bottom:20px;">
                            <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">會員登入</h1>
                        </div>
                        <form id="registrationForm" role="form" action="<?=base_url()?>user/addUser"method="post">
                            <div class="form-group">
                                <label for="inputName" class="control-label">姓名</label>
                                <input type="text" class="form-control" id="inputName" name="inputName" placeholder="請填入姓名">
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="control-label">信箱</label>
                                <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="請填Email">
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label">密碼  <small class="text-muted">長度為5～15個字元</small></label>
                                <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="請填Password">
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="control-label">確認密碼</label>
                                <input type="password" class="form-control" id="checkPassword" name="checkPassword" placeholder="請確認Password">
                            </div>
                            <!--
                            <div class="form-group">
                                <input type="checkbox" value='1' name="agreement">
                                <span id="agree">我已詳閱《<?=anchor('page/terms','服務條款','target="_blank"')?>》並同意本網站之《<?=anchor('page/enrollAgreement','個人資料蒐集規範','target="_blank"')?>》</span>
                            </div>
                            -->
       <!--註解到2014年八月底-->
                            <!-- <div class="text-left">
                                <h2>[Step2-付費]&raquo;</h2>
                            </div>
                            <div class="col-sm-offset-1">
                            <div class="form-group">
                                <label for="paid" class="col-sm-2 control-label">選擇付費方式：</label>
                                <div class="col-sm-10">
                                    <div class="radio-inline">
                                    <label>
                                        <input type="radio" name="paid" id="paid1" checked> 優惠卷條碼
                                    </label>
                                    </div>
                                    <div class="radio-inline">
                                    <label>
                                        <input type="radio" name="paid" id="paid2"> 線上信用卡付費
                                    </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group coupon">
                                <label for="coupon" class="col-sm-2 control-label">優惠卷條碼：</label>
                                <div class="col-sm-4">
                                     <input type="text" class="form-control" name="coupon" id="coupon" placeholder="請填入優惠卷條碼" maxlength="19">
                                </div>
                            </div>
                            <div class="form-group credit-card">
                                <label for="credit-card" class="col-sm-2 control-label">信用卡號：</label>
                                <div class="col-sm-4">
                                     <input type="text" class="form-control" name="credit-card" id="credit-card" placeholder="請填入信用卡號">
                                </div>
                            </div> -->
                            <div class="form-group">
                                <button type="submit" class="btn btn-success" style="margin-bottom:15px;">送出</button>
                                <button type="button" id="cancel-add-member" class="btn" style="color:#777; margin-bottom:15px;">取消</button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!--
        <div class="row row-offcanvas row-offcanvas-right basic-data">
            <div class="col-xs-12 col-sm-12">
                <div class="row">
                    <div class="col-md-12">
                            <div class="registration">
                                <form id="registrationForm" class="form-horizontal col-sm-offset-3" role="form" action="<?=base_url()?>user/addUser"method="post">
                                    <div class="text-left">
                                        <h2>填寫基本資料&raquo;</h2>
                                    </div>
                                    <div class="col-sm-offset-1">
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-2 control-label">姓名：</label>
                                        <div class="col-sm-4">
                                             <input type="text" class="form-control" id="inputName" name="inputName" placeholder="請填入姓名">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-2 control-label">信箱：</label>
                                        <div class="col-sm-4">
                                             <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="請填入信箱">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="col-sm-2 control-label">密碼：</label>
                                        <div class="col-sm-4">
                                            <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="請填入密碼">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="col-sm-2 control-label">再次輸入密碼：</label>
                                        <div class="col-sm-4">
                                            <input type="password" class="form-control" id="checkPassword" name="checkPassword" placeholder="請填入密碼">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="agree" class="col-sm-2 control-label"></label>
                                        <div class="col-sm-8">
                                            <input type="checkbox" value='1' name="agreement">
                                            <span id="agree">我已詳閱《<?=anchor('page/terms','服務條款','target="_blank"')?>》並同意本網站之《<?=anchor('page/enrollAgreement','個人資料蒐集規範','target="_blank"')?>》</span>
                                        </div>
                                    </div>
                                    </div> -->
               <!--註解到2014年八月底-->
                                    <!-- <div class="text-left">
                                        <h2>[Step2-付費]&raquo;</h2>
                                    </div>
                                    <div class="col-sm-offset-1">
                                    <div class="form-group">
                                        <label for="paid" class="col-sm-2 control-label">選擇付費方式：</label>
                                        <div class="col-sm-10">
                                            <div class="radio-inline">
                                            <label>
                                                <input type="radio" name="paid" id="paid1" checked> 優惠卷條碼
                                            </label>
                                            </div>
                                            <div class="radio-inline">
                                            <label>
                                                <input type="radio" name="paid" id="paid2"> 線上信用卡付費
                                            </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group coupon">
                                        <label for="coupon" class="col-sm-2 control-label">優惠卷條碼：</label>
                                        <div class="col-sm-4">
                                             <input type="text" class="form-control" name="coupon" id="coupon" placeholder="請填入優惠卷條碼" maxlength="19">
                                        </div>
                                    </div>
                                    <div class="form-group credit-card">
                                        <label for="credit-card" class="col-sm-2 control-label">信用卡號：</label>
                                        <div class="col-sm-4">
                                             <input type="text" class="form-control" name="credit-card" id="credit-card" placeholder="請填入信用卡號">
                                        </div>
                                    </div> -->
                                    <!--<div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="button" class="btn btn-info" id="cancel-add-member">取消</button>
                                            <button type="submit" class="btn btn-success">送出</button>
                                        </div>
                                    </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>-->
    </div>
</div>