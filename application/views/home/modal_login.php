<!-- satrt-discussion Modal -->
<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close cancle-discuss" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="modal-title">會員登入</h4>
      		</div>
      		<div class="modal-body">
      			<div class="message"></div>
      			
      			<form id="loginForm" role="form" action="<?=base_url()?>home/modalLogin" method="post">
                      <div class="form-group">
                        <label for="exampleInputEmail1">信箱</label>
                        <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="登入Email">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">密碼</label>
                        <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="密碼">
                      </div>
                      <div class="form-group">
                        <button id="modal-login-submit"type="submit" class="btn btn-success" style="margin-bottom:15px;">登入</button>
                        <!-- <a href="javascript: void(0)" id="fb_login" class="btn btn-default" style="margin-bottom:15px;" onclick="window.location='<?=$login_url?>'">
                            <img src="<?php echo base_url()?>assets/img/facebook.png" width="20px" style="float: left;margin-right: 5px;">
                            <span>Facebook登入</span>  
                        </a> -->
                        <a href="<?php echo base_url().'user/forgetPassword'?>" class="btn btn-link" style="margin-bottom:15px;">忘記密碼</a>
                        <a href="<?php echo base_url().'home/registration'?>" class="btn btn-link" style="margin-bottom:15px;">註冊LearningHouse</a>
                        <input type="hidden" name="requestURL"	value="<?php echo $this->session->userdata('src');?>"><!--送出本網頁的路徑給Controller-->
                        <input type="hidden" name="action"	value="login">
                      </div>
                    </form>
      		</div>
      		<div class="modal-footer">
        		
      		</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->