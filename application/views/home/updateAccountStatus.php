<style>
	.credit-card {
		display: none
	}
</style>
<div class="container">
	<div class="row row-offcanvas row-offcanvas-right basic-data">
		<div class="col-xs-12 col-sm-12">
			<div class="row">
				<div class="col-md-12">
					<div class="registration">
						<form id="registrationForm" class="form-horizontal col-sm-offset-3" role="form" action="<?=base_url() ?>user/updateAccountStatus"method="post">
							<div class="text-left">
								<h2>[請選擇付款方式]&raquo;</h2>
							</div>
							<div class="col-sm-offset-1">
								<div class="form-group">
    								<label for="inputEmail" class="col-sm-2 control-label">信箱：</label>
    								<div class="col-sm-4">
     									 <input type="email" class="form-control" id="email" name="email" value="<?php echo $email?>" placeholder="請填Email">
    								</div>
  								</div>
								<div class="form-group">
									<label for="paid" class="col-sm-2 control-label">選擇付費方式：</label>
									<div class="col-sm-10">
										<div class="radio-inline">
											<label>
												<input type="radio" name="paid" id="paid1" checked>
												優惠卷條碼 </label>
										</div>
										<div class="radio-inline">
											<label>
												<input type="radio" name="paid" id="paid2">
												線上信用卡付費 </label>
										</div>
									</div>
								</div>
								<div class="form-group coupon">
									<label for="coupon" class="col-sm-2 control-label">優惠卷條碼：</label>
									<div class="col-sm-4">
										<input type="text" class="form-control" name="coupon" id="coupon" placeholder="請填入優惠卷條碼" maxlength="19">
									</div>
								</div>
								<div class="form-group credit-card">
									<label for="credit-card" class="col-sm-2 control-label">信用卡號：</label>
									<div class="col-sm-4">
										<input type="text" class="form-control" name="credit-card" id="credit-card" placeholder="請填入信用卡號">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<button type="button" class="btn btn-info" id="cancel-add-member">
											取消
										</button>
										<button type="submit" class="btn btn-success">
											送出
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>