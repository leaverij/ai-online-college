<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="keywords" content="線上學習,mooc,moox,ai,learning,資策會" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="我們相信透過學習可以改變一個人的價值，AIOC希望在您職涯成長的路上貢獻一點力量，透過老師們用心錄製的課程及各式學習活動安排與指引，協助您在專業及技能上學習的更好，在職涯發展路上不斷提升自我價值">
    <meta name="author" content="LearningHouse Team">
    <meta name="copyright" CONTENT="本網頁著作權LearningHouse所有">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, minimal-ui" />
	<meta property="og:description" content="大家也趕快來學習吧，在這裡你可以學習到新的技能以及更多的知識喔!"/>
    <link rel="shortcut icon" href="<?=base_url();?>assets/ico/favicon.png">
	<title><?=$title;?>-<?=$this->config->item('site_name');?></title>
	<!-- flowplayer skin -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/flowplayer/minimalist.css"/>
	<!-- Video.js CSS-->
	<link href="<?=base_url();?>assets/css/video-js/video-js.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/css/video-js/video-speed.css" rel="stylesheet">
    <!-- jQuery UI -->
    <link href="<?=base_url();?>assets/css/jquery-ui-timepicker-addon/jquery-ui.css" rel="stylesheet">
    <!-- jQuery UI  TimePicker Addon-->
    <!-- <link href="<?=base_url();?>assets/css/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.css" rel="stylesheet"> -->
	<!-- Bootstrap core CSS -->
    <link href="<?=base_url();?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/bootstrap/css/bootstrap-switch.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/bootstrap/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?=base_url();?>assets/css/learning-house.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=base_url();?>assets/js/html5shiv.js"></script>
      <script src="<?=base_url();?>assets/js/respond.min.js"></script>
    <![endif]-->
    <script>var base_url = "<?php echo base_url();?>";var message;var doughnutData;var json_stmt;var json_id;var radarChartData;var barData;
    		var name="<?php echo $this->session->userdata('name'); ?>";
    		var email = 'mailto:'+"<?=$this->session->userdata('email');?>";
    </script>
  </head>
  <body>
	<!-- <?php include_once("analyticstracking.php") ?> -->
<!-- 	<div id="fb-root"></div>
	<script>(function(d, s, id) {
  		var js, fjs = d.getElementsByTagName(s)[0];
  		if (d.getElementById(id)) return;
  		js = d.createElement(s); js.id = id;
  		js.src = "//connect.facebook.net/zh_TW/all.js#xfbml=1&appId=1406708409569511";
  		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script> -->
	<!-- Wrap all page content here -->
    <div id="wrap" style="padding-top:80px;">
    <!-- Fixed navbar -->
    	<header class="navbar-default navbar-fixed-top">
      		<div class="container">
        		<div class="navbar-header">
          		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#lh-navigation">
            		<span class="icon-bar"></span>
            		<span class="icon-bar"></span>
            		<span class="icon-bar"></span>
          		</button>
          		<a class="navbar-brand" href="<?=base_url();?>">
		  			<img src="<?=base_url();?>assets/img/lh-logo-color.png">
                </a>
        		</div>
				<style>
                    .scrollable-menu {height: auto; max-height: 600px; overflow-x: hidden;}
                    .notification-dropdown .fa-bell {font-size:1.3em; position:relative; color:#999;}
                    .notification-dropdown .badge {font-weight:normal; padding:2px 4px; background:#d9534f; position:absolute; top:-10px; left:15px;}
					.notification-container ul {padding:0;}
					.notification-container ul li {list-style:none; border-top:1px solid #ededed; padding:15px 0;}
					.notification-dropdown .notification-container ul li {padding:15px;}
					.notification-container ul li:first-child {border-top:none;}
					.avatar {
						width:60px;
						height:60px;
						overflow:hidden;
						background-position:center;
						background-size:50px auto !important;
						border-radius:50px;
						border:5px solid #eee;}
					.avatar img {width:60px; height:60px; display:none;}
					.forum.notification .avatar {margin:0 auto;}
					.notif-meta {}
					.notif-meta h4 {font-size:1.14em; margin:0;}
					.notif-meta p {margin:0; font-size:0.92em; color:#999; line-height:2em;}
					.notif-meta .time { font-size:0.85em; color:#bbb;}

                	#lh-navigation .fa-chevron-down {font-size:small;}
					#lh-navigation .dropdown-menu {box-shadow:5px 5px rgba(0,0,0,0.07);}
					#lh-navigation .dropdown-menu > .arrow {position:absolute; display:block; width:0; height:0; border-color:transparent; border-style:solid; border-bottom-color:#ccc; top:-22px; right:10px; border-top-width:0; border-width:11px;}
                	#lh-navigation .dropdown-menu > .arrow:after {position:absolute; display:block; width:0; height:0; border-color:transparent; border-style:solid; border-bottom-color:#fff; top:1px; content:" "; border-width:10px; top:-9px; right:-10px;}
                </style>
        		<nav  id="lh-navigation" class="collapse navbar-collapse">
         				<ul class="nav navbar-nav navbar-right" style="padding:15px 0;">
						<!-- <li><a href="<?php echo base_url()?>page/preparation">檢測</a></li> -->
						<!--<li><a href="<?php echo base_url()?>page/gym"><i style="color:green;" class="fa fa-flask" aria-hidden="true"></i>Gym</a></li> -->
						<li><a href="<?php echo base_url()?>competitions">擂臺</a></li>
						<li><a href="<?php echo base_url()?>library/ai">課程</a></li>
              			<li><a href="<?php echo base_url()?>library">學程</a></li>
						<li><a href="<?php echo base_url()?>forum">討論區</a></li>
						<? if($this->session->userdata('name')):?>
							<li class="dropdown">
							  	<a href="#" data-toggle="dropdown">學習中心<i class="fa fa-chevron-down"></i></a>
						  		<ul class="dropdown-menu">
								  	<div class="arrow"></div>
									<li><a href="<?php echo base_url()?>course/learn">我的課程</a></li>
									<li><a href="<?php echo base_url()?>dashboard">個人儀表板</a></li>
									<li><a href="<?=base_url()?>user/portfolio/<?php echo $this->session->userdata('alias')?>">個人履歷</a></li>
									<li><a href="<?=base_url()?>vacancy/myjob">個人求職</a></li>
                  <li><a href="<?=base_url()?>datasets">我的Datasets</a></li>
                  <li><a href="<?=base_url()?>datasets/explore">探索Datasets</a></li>
								</ul>
							</li>
						<? endif;?>
						<? if($this->session->userdata('name') && $this->user_model->check_teacher_flag($this->session->userdata('email'))):?>
							<li class="dropdown">
							  	<a href="#" data-toggle="dropdown">教學中心<i class="fa fa-chevron-down"></i></a>
						  		<ul class="dropdown-menu">
								  	<div class="arrow"></div>
									<li><a href="<?php echo base_url()?>course/create">建立課程</a></li>
									<li><a href="<?php echo base_url()?>course/teach">課程管理</a></li>
									<li><a href="<?php echo base_url()?>dashboard/teacher">教學儀表板</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" data-toggle="dropdown">競賽中心<i class="fa fa-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<div class="arrow"></div>
									<li><a href="<?php echo base_url()?>competitions/hosted">競賽管理</a></li>
								</ul>
							</li>
						<? endif;?>
						<? if($this->session->userdata('name') && $this->user_model->checkBackend()):?>
							<li class="dropdown">
								<a href="#" data-toggle="dropdown">管理中心<i class="fa fa-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<div class="arrow"></div>
									<li><a href="<?php echo base_url()?>dashboard/admin">主管儀表版</a></li>
									<li><a href="<?php echo base_url()?>dashboard/meeting">課程直播分析</a></li>
									<li><a href="<?=base_url()?>vacancy/">企業徵才</a></li>
									<li><a href="<?=base_url()?>company/">企業資料維護</a></li>
									<li><a href="<?=base_url()?>setting">使用者管理</a></li>
									<!--
									<li><a href="<?=base_url()?>backend">後台管理介面</a></li>
									<li><a href="<?=base_url()?>page/inputBatchEmail">群組寄信</a></li>
									-->
								</ul>
							</li>
						<? endif;?>
                        <!-- SIGN IN/ SIGN OUT -->
            			<? if(!$this->session->userdata('alias')):?>
            			<li class="login"><a href="<?=base_url();?>home/login">登入/註冊</a></li>
            			<? endif?>
						<? if($this->session->userdata('alias')):?>
						<li class="dropdown">
              				<a href="#" data-toggle="dropdown">
              					<?php echo $this->session->userdata('alias')?> <!--<span class="caret"></span>-->
              					<i class="fa fa-chevron-down"></i>
                            </a>
              				<ul class="dropdown-menu">
								<div class="arrow"></div>
								<li><a href="<?=base_url()?>pay/paid">購買紀錄</a></li>
                				<li><a href="<?=base_url()?>profile/<?php echo $this->session->userdata('alias')?>">帳號管理</a></li>
                				<?php if($this->user_model->isSuper($this->session->userdata('email'))){?>
                				<li><a href="<?=base_url()?>user/webCamPic">個人照片</a></li>
                				<?}?>
                				<? if($this->session->userdata('fb_login')): ?> 
									<li><?=anchor($this->session->userdata('fb_logout_url'),'登出',"class=''")?></li>
								<? else: ?>
									<li><a href="<?=base_url();?>home/logout">登出</a></li>
								<? endif;?>
              				</ul>
            			</li>
                        
            			<!-- <?php $row=json_decode($getDropdownNotification);?>
						<li class="notification-dropdown dropdown">
              				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
              					<i title="通知訊息" class="fa fa-bell">
                              		<span class="notifications-count badge"><?php if($getUnreadNotification!=0){echo $getUnreadNotification;}?></span>
                                </i>
                            </a>
              				<div class="notification-dropdown-content dropdown-menu" style="width:400px;">
                            	<div class="arrow"></div>
              					<h4 style="margin:0; padding:10px 0; border-bottom:1px solid #ededed; text-align:center;">通知訊息</h4>
              					<?php if(empty($row)){?>
              						<div style="text-align:center; color:#97a5a6; padding:15px;">您目前沒有任何的通知訊息</div>
              					<?php }else{?>
                                <div class="scroll-mask notification-container" style="overflow:hidden;">
                                	<ul class="scrollable-menu" style="list-style:none; max-height:500px; margin-right:-17px;">
	              					<?php $i=0;?>
	              					<?php foreach($row as $key=>$value){?>
		                				<li class="forum notification" style="list-style: none;">
			                				<a href="<?php if($row[$key]->notification_type==1 ||$row[$key]->notification_type==2){echo base_url().'forum/forumContent/'.$row[$key]->hyperlink;}?>" style="display:block; color:#333;">
			                					<div class="row" style="line-height:15px;overflow:hidden;">
			                						<div class="col-sm-8">
                                                    	<div class="notif-meta">
                                                            <p><?php if($row[$key]->notification_type==1){echo '討論區回應';}elseif($row[$key]->notification_type==2){echo '新問題';}?></p>
                                                            <h4><?php echo $row[$key]->notification_title;?></h4>
                                                            <p>經由 <strong><?php echo $row[$key]->alias;?></strong> <?php if($row[$key]->notification_type==1){echo '回答';}elseif($row[$key]->notification_type==2){echo '提問';}?></p>
                                                            <p class="time"><?php echo $this->theme_model->calTimeElapsed($row[$key]->notification_time);?></p>
			                							</div>
                                                    </div>
			                						<div class="col-sm-4">
						                				<div class="avatar" style="background-image: url('<?=base_url();?>assets/img/user/128x128/<?php echo $row[$key]->img;?>')">
						                					<img src="<?=base_url();?>assets/img/user/128x128/<?php echo $row[$key]->img;?>">
						                				</div>
			                						</div>
			                					</div>
		                					</a>
		                				</li>
		                				<?php if($row[$key]->is_read==0){?>
		                					<input type="hidden" name="unread[]" value="<?php echo $row[$key]->notification_id;?>">
		                				<?php }?>
		                				<?php $i++;?>
	                				<?php }?>
                                    </ul>
                                </div>
                				<?php }?>
                				<div style="border-top:1px solid #ededed; text-align:center;">
                                	<a href="<?=base_url();?>notifications" style="display:block; padding:10px 0;">檢視所有通知訊息</a>
                                </div>
              				</div>
            			</li> -->
            			<? endif;?>
          			</ul>
       		 	</nav><!--/.nav-collapse -->
      		</div>
    	</header>
