<div id="content">
    <div class="container">
		<div class="well row">
			<span class="pull-right"><img src="<?php echo base_url()?>assets/img/user/128x128/<?php echo $profile['img']?>"></span>
			<div class="col-xs-12">
				<h3 style="line-height:1.4em;"><?php echo $profile['name']?></h3>
				<h4 style="line-height:1.4em;"><?php echo $profile['email']?>
					<?php if($profile['active_flag']=='0'){?>
						<span class="label label-warning">信箱尚未通過認證</span>
					<?}?> 
				</h4>
				<p class="text-muted">since <?php echo date_format(date_create($profile['register_date']), 'Y-m-d'); ?></p>
			</div>
			<div class="col-xs-12">
				<a style="font-size:0.75em;" href="<?php echo base_url().'user/edit'?>" class="btn btn-primary" role="button">編輯個人資料</a>
				<?php if($profile['active_flag']=='0'){?>
					<button id="resendActiveMailBtn" type="button" class="btn btn-info">重寄認證信</button><span id="resendActiveMail"></span>
				<?}?> 
			</div>
		</div>
		<div class="row">
			<div>
			  	<h3 style="float: left">我的履歷　</h3>
			</div>
			<div class="row" style="line-height:4.5em;">
				<input type="checkbox" name="my-tracks-checkbox" checked data-on-label="學程資訊" data-off-label="履歷資訊">
			</div>
		</div><!--/row-->
		<div class="row tracks">
			<?php $row = json_decode($getUserTracks); ?>
			<?php $row2 = json_decode($getUserTracksCourse); ?>
			<?if($row){?>
				<?foreach($row as $key=>$val){?>
			  	<div class="col-6 col-sm-12 col-md-12 col-lg-12" style="margin-top:10px;">
				<div class="thumbnail">
					<div class="caption">
						<div style="float: left;width: 70%;">
							<h4><?php echo $val->tracks_name?></h4>
							<h5><?php echo $val->tracks_desc?></h5>
						</div>
						<div style="float: left;width: 30%;">
							<img src="<?=base_url();?>assets/img/map.png">
						</div>
						<div style="clear:both;"></div>
						<hr>
						<div>
							<?foreach($row2 as $key2=>$val2){?>
								<?if($val->tracks_id == $val2->tracks_id){?>
									<img src="<?=base_url();?>assets/img/badges/<?php echo $val2->course_pic?>">
								<?}?>
							<?}?>
						</div>
					</div>
				</div>
				</div>
				<?}?>
			<?}else{?>
				您尚未學習過任何LearningHouse的課程喔！
			<?}?>
		</div>
		<div class="well row resume" style="display: none">
			<span class="pull-right"></span>
				<div class="col-xs-12">
					<h4 style="line-height:1.4em;">關於我</h4>
					<div id="about-me">
					  <iframe class="youtube-player" type="text/html" width="640" height="385" src="http://www.youtube.com/embed/SygkJv51Ixs" frameborder="0"> 
					  	</iframe>
					</div>	
					<h4 style="line-height:1.4em;">作品集</h4>
					<h4 style="line-height:1.4em;">履歷下載</h4>
				</div>
		 </div>
	</div><!-- /container -->
</div><!-- /content -->