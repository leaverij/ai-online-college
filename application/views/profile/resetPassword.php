<style>
.reset-modal .reset-content {padding:30px;}
.reset-content label {font-size:1.14em; font-weight:normal;}
</style>
<div class="container">
	<div class="row">
    	<div class="reset-modal">
            <div class="col-sm-5" style="margin:0 auto; float:none;">
            	<div class="reset-content">
                	<h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">重設密碼</h3>
                    <form id="resetForm" role="form" action="<?=base_url()?>user/resetPassword" method="post">
                      	<div class="form-group">
                        	<label for="exampleInputNewPassword1">新密碼</label>
							<input type="password" class="form-control" id="inputNewPassword" name="inputNewPassword" placeholder="請填入新密碼">
						</div>
                      	<div class="form-group">
                        	<label for="exampleInputNewPassword2">再次輸入新密碼</label>
                        	<input type="password" class="form-control" id="checkNewPassword" name="checkNewPassword" placeholder="請填入新密碼">
                      	</div>
                      	<div class="form-group">
                        	<button type="submit" class="btn btn-success" style="margin-bottom:15px;">送出</button>
                        	<input type="hidden" name="code" value="<?php echo $code?>">
                        	<input type="hidden" name="action"	value="resetPassword">
                      	</div>
                    </form>
                </div>
            </div>
    	</div>
    </div>
</div>