<style type="text/css">
#profile h4{
	line-height:10px;
}
</style>
<div id="content">
<div class="container">
	<div class="row">
		<div id="profile" class="well col-sm-12">
			<h3>基本資料</h3>
			<div class="container">
				<!-- padding order:top right bottom left -->
				<div class="col-sm-10">
					<div class="control-group">
						<h4 class="col-sm-2">姓名：</h4>
						<h4 class="col-sm-10"><?php echo $getOneUser['name']?></h4>
					</div>
					<div class="control-group">
						<h4 class="col-sm-2">單位：</h4>
						<h4 class="col-sm-10"><?php echo $getOneUser['company']?></h4>
					</div>
					<div class="control-group">
						<h4 class="col-sm-2">目前職稱：</h4>
						<h4 class="col-sm-10"><?php echo $getOneUser['job_title']?></h4>
					</div>
					<?//if($getOneUser['education_highest_level']!='0'){?>
					<div class="control-group">
						<h4 class="col-sm-2">最高學歷：</h4>
						<h4 class="col-sm-10"><?php echo $getOneUser['school']?>
						<?php if($getOneUser['education_highest_level']==1){echo '博士';
						}elseif($getOneUser['education_highest_level']==2){echo '碩士';
						}elseif($getOneUser['education_highest_level']==3){echo '大學技職';
						}elseif($getOneUser['education_highest_level']==4){echo '高中職';
						}?>
						</h4>
					</div>
					<? //}?>
					<?php if(!empty($getUserExperience['experience_name'])&&!empty($getUserExperience['experience_start'])&&!empty($getUserExperience['experience_end'])){?>
						<div class="control-group">
							<h4 class="col-sm-2">工作經歷：</h4>
							<h4 class="col-sm-10"><?php if(!empty($getUserExperience['experience_name'])&&!empty($getUserExperience['experience_start'])&&!empty($getUserExperience['experience_end'])){echo $getUserExperience['experience_name'].'('.$getUserExperience['experience_start'].'-'.$getUserExperience['experience_end'].')';}?></h4>
						</div>
					<?php }?>
					<div class="control-group">
						<h4 class="col-sm-2">擅長工具：</h4>
						<h4 class="col-sm-10"><?php echo $getOneUser['tools'];?></h4>
					<div class="control-group">
						<h4 class="col-sm-2">工作技能：</h4>
						<h4 class="col-sm-10"><?php echo $getOneUser['work_skills'];?></h4>
					</div>
					</div>
					<?php if(!empty($getUserAward['award_name'])){?>
						<div class="control-group">
							<h4 class="col-sm-2">獲獎紀錄</h4>
							<h4 class="col-sm-10"><?php if(!empty($getUserAward['award_name']))echo $getUserAward['award_name'];?></h4>
						</div>
					<?php }?>
					<div class="control-group">
						<h4 class="col-sm-2">簡介：</h4>
						<h4 class="col-sm-10"><?php echo $getOneUser['intro'];?></h4>
					</div>
					<div class="control-group">
						<h4 class="col-sm-2">連絡方式：</h4>
						<div class="col-sm-10" style="padding-bottom:20px;">
							<div class="col-sm-1"><a href="mailto:<?php echo $getOneUser['email'];?>" title="<?php echo $getOneUser['email'];?>"><img src="<?=base_url();?>assets/img/images/onepage/email_32.png" 	alt="Email" /></a></div>
							<div class="col-sm-1"><a href="#" title="<?php echo $getOneUser['mobile'];?>"><img src="<?=base_url();?>assets/img/images/onepage/phone_32.png" 	alt="Phone"/></a></div>
							<div class="col-sm-1"><a href="#" title="Skype"><img src="<?=base_url();?>assets/img/images/onepage/skype_32.png" 	alt="Skype" /></a></div>
							<div class="col-sm-1"><a href="https://www.facebook.com/jun760311" title="https://www.facebook.com/jun760311"><img src="<?=base_url();?>assets/img/images/onepage/facebook_32.png" 	alt="Facebook" /></a></div>
							<div class="col-sm-1"><a href="http://plus.google.com/112881927530753861924" title="http://plus.google.com/112881927530753861924"><img src="<?=base_url();?>assets/img/images/onepage/google+_32.png" 	alt="Google+" /></a></div>
							<!-- <div class="col-sm-1"><a href="#" title="Location"><img src="<?=base_url();?>assets/img/images/onepage/location_32.png" 	alt="Location" /></a></div>
							<div class="col-sm-1"><a href="#" title="Flickr"><img src="<?=base_url();?>assets/img/images/onepage/flickr_32.png" 	alt="Flickr" /></a></div>
							<div class="col-sm-1"><a href="#" title="Linkedin"><img src="<?=base_url();?>assets/img/images/onepage/linkedin_32.png" 	alt="Linkedin" /></a></div>
							<div class="col-sm-1"><a href="#" title="Twitter"><img src="<?=base_url();?>assets/img/images/onepage/twitter_32.png" 	alt="Twitter" /></a></div>
							<div class="col-sm-1"><a href="#" title="Vimeo"><img src="<?=base_url();?>assets/img/images/onepage/vimeo_32.png" 	alt="Vimeo" /></a></div>
							<div class="col-sm-1"><a href="#" title="Website"><img src="<?=base_url();?>assets/img/images/onepage/website_32.png" 	alt="Website" /></a></div>
							<div class="col-sm-1"><a href="#" title="Youtube"><img src="<?=base_url();?>assets/img/images/onepage/youtube_32.png" 	alt="Youtube" /></a></div> -->
						</div>
					</div>
				
				</div>
				<div class="pull-right col-sm-2" style="position:relative;top:0px;right:10px;">
					<a href="<?=base_url();?>profile/jun760311">
						<img src="<?=base_url();?>assets/img/member/208347.jpg" alt="劉奕均" width="150" height="150">
					</a>
				</div>
				<div class="pull-right col-sm-2" style="position:relative;top:15px;right:10px;">
					<iframe width="150" height="150" src="//www.youtube.com/embed/rqvrR5RGsZE?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
		<div id="performance" class="well col-sm-12">
			<h3>學習表現</h3>
			<div class="container">
				<div style="margin-bottom:50px;" class="row">
					<?php $row = json_decode($getUserChapterBadge); ?>
					<div class="row">
						<?foreach($row as $key=>$val){?>
							<? $image=urlencode(base_url()."assets/img/badges/".$val->chapter_pic);?>
							<? $url=urlencode(base_url().'library/'.$val->library_url.'/'.$val->course_url.'/'.$val->chapter_url);?>
							<div class="col-4 col-sm-4 col-md-4 col-lg-4" style="margin-top:10px;">
								<div class="thumbnail">
									<div class="caption">
										<div style="float: left;width: 70%;">
											<h4><?php echo $val->chapter_name?></h4>
											<p class="text-muted"><?php echo $val->accomplish_time?> 完成</p>
											<!-- <a class="btn btn-default" onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $title;?>&amp;p[description]=<?php echo $summary;?>&amp;p[summary]=<?php echo $summary;?>&amp;p[url]=<?php echo $url; ?>&amp;&p[images][0]=<?php echo $image;?>', 'sharer', 'toolbar=0,status=0,width=550,height=400');" target="_parent" href="javascript: void(0)">
																				<span>分享</span>
																				</a> -->
										</div>
										<div style="float: left;width: 30%;">
											<img src="<?=base_url(); ?>assets/img/badges/<?php echo $val->chapter_pic?>">
										</div>
										<div style="clear:both;"></div>
									</div>
								</div>
							</div>
						<?}?>
					 </div>
				</div>
			</div>
		</div>
        	<div id="portfolio-gallery" class="well col-sm-12">
			<h3>作品成果</h3>
                <div class="row">
					<div class="col-sm-6">
						<h4 class="thumbnail col-sm-12">e訂行網站首頁</h4>
							<div class="thumbnail col-sm-8">
								<div>
						<a href="http://54.83.1.187/WA105G4_implements_0528A_Jason/index.jsp">
									<div class="minicard-photo">
									<img style="float:left;height:400px;width:300px;" src="<?=base_url();?>assets/img/images/product/product1.png" alt=""/>
									</div>
						</a>
								</div>
							</div>
							<div class="thumbnail col-sm-4">
								<div class="caption" style="height:400px;">這個作品是資策會中壢中心Java養成班結訓時製作的結訓專題成果網頁，首頁以大圖磅礡的氣勢做為進場畫面，輪播數張具有視覺效果的列車圖片，旁邊有可快速查詢時刻表資料的小工具，圖片下方可瀏覽近期公告事項，上方附有三個國家的即時時鐘，網站支援多國語言，切換網站語系可以分別用中英文呈現。</div>
							</div>
					</div>
					<div class="col-sm-6">
						<h4 class="thumbnail col-sm-12">手機端座位查詢</h4>
							<div class="thumbnail col-sm-8">
								<div>
									<div class="minicard-photo"><img style="float:left;height:400px;width:300px;" src="<?=base_url();?>assets/img/images/product/Screenshot2013-05-23-02-41-04.jpg" alt=""/></div>
								</div>
							</div>
							<div class="thumbnail col-sm-4">
								<div class="caption" style="height:400px;">這個作品是資策會中壢中心Java養成班結訓時製作的結訓專題成果網頁，首頁以大圖磅礡的氣勢做為進場畫面，輪播數張具有視覺效果的列車圖片，旁邊有可快速查詢時刻表資料的小工具，圖片下方可瀏覽近期公告事項，上方附有三個國家的即時時鐘，網站支援多國語言，切換網站語系可以分別用中英文呈現。</div>
							</div>
					</div>
					<div class="col-sm-6">
						<h4 class="thumbnail col-sm-12">車廂管理</h4>
							<div class="thumbnail col-sm-8">
								<div>
									<div class="minicard-photo"><img style="float:left;height:400px;width:300px;" src="<?=base_url();?>assets/img/images/product/03.png" alt=""/></div>
								</div>
							</div>
							<div class="thumbnail col-sm-4">
								<div class="caption" style="height:400px;">這個作品是資策會中壢中心Java養成班結訓時製作的結訓專題成果網頁，首頁以大圖磅礡的氣勢做為進場畫面，輪播數張具有視覺效果的列車圖片，旁邊有可快速查詢時刻表資料的小工具，圖片下方可瀏覽近期公告事項，上方附有三個國家的即時時鐘，網站支援多國語言，切換網站語系可以分別用中英文呈現。</div>
							</div>
					</div>
					<div class="col-sm-6" style="margin-bottom: 20px;">
						<h4 class="thumbnail col-sm-12">車次管理</h4>
							<div class="thumbnail col-sm-8">
								<div>
									<div class="minicard-photo"><img style="float:left;height:400px;width:300px;" src="<?=base_url();?>assets/img/images/product/01.png" alt=""/></div>
								</div>
							</div>
							<div class="thumbnail col-sm-4">
								<div class="caption" style="height:400px;">這個作品是資策會中壢中心Java養成班結訓時製作的結訓專題成果網頁，首頁以大圖磅礡的氣勢做為進場畫面，輪播數張具有視覺效果的列車圖片，旁邊有可快速查詢時刻表資料的小工具，圖片下方可瀏覽近期公告事項，上方附有三個國家的即時時鐘，網站支援多國語言，切換網站語系可以分別用中英文呈現。</div>
							</div>
					</div>
					<div>
						<h3 class="col-sm-12">專題專案文件</h3>
						<div >
							<iframe class="col-sm-12" src="http://www.slideshare.net/slideshow/embed_code/31980332" width="479" height="511" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px 1px 0; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="https://www.slideshare.net/Jun_Liu/wa105e" title="WA105-e訂行結訓專案文件" target="_blank">WA105-e訂行結訓專案文件</a> </strong> from <strong><a href="http://www.slideshare.net/Jun_Liu" target="_blank">Jun_Liu</a></strong> </div>
						</div>
					</div>
                </div>
            </div>
		<!-- <div class="well col-sm-12">
			<?php //$this->load->view('profile/matchingList'); ?>
		</div> -->
	</div>
</div>
</div>