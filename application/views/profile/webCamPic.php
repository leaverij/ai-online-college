<div id="content">
    <div class="container">
		<div style="margin-bottom:50px;" class="row">
			<?php $row = json_decode($getPicture); ?>
			<div>
			  <?if($row){?>
			  	<h3>My pic</h3>
			  <?}?>
			</div>
			<div class="row">
				<?foreach($row as $key=>$val){?>
					<div class="col-6 col-sm-6 col-md-6 col-lg-6" style="margin-top:10px;">
						<div class="thumbnail">
							<div class="caption">
								<div style="float: left;width: 70%;">
									<h4>My pic</h4>
									<p class="text-muted"><?=$val->pic_date?></p>
            						</a>
								</div>
								<div style="float: left;width: 30%;">
									<img src="<?=base_url(); ?>assets/img/user/webcam/<?php echo $this->session->userdata('email')?>/<?=$val->filename?>">
								</div>
								<div style="clear:both;"></div>
							</div>
						</div>
					</div>
				<?}?>
			 </div>
		</div><!--/row-->
	</div><!-- /container -->
</div><!-- /content -->