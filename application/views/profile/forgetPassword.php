<style>
.forget-modal .forget-content {padding:30px;}
.forget-content label {font-size:1.14em; font-weight:normal;}
</style>
<div class="container">
	<div class="row">
    	<div class="forget-modal">
            <div class="col-sm-5" style="margin:0 auto; float:none;">
            	<div class="forget-content">
                    <div style="padding-bottom:20px;">
                        <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">忘記密碼了嗎?</h1>
                    </div>
                    <form id="forgetPasswordForm" role="form" action="<?=base_url()?>user/forgetPassword" method="post">
                      	<div class="form-group">
                        	<label for="inputEmail">請輸入您的信箱</label>
							<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="您註冊的email信箱">
						</div>
                      	<div class="form-group">
                        	<button type="submit" class="btn btn-success" style="margin-bottom:15px;">送出</button>
                        	<input type="hidden" name="action"	value="forgetPassword">
                      	</div>
                    </form>
                </div>
            </div>
    	</div>
    </div>
</div>