<?php
$title=urlencode("獲得新徽章：我在learninghouse完成一個關卡！");
$summary=urlencode("大家也趕快來學習吧，在這裡你可以學習到新的技能以及更多的知識喔!");
?>
<style>

</style>
<div id="content">
    <div class="container">
		<div class="col-sm-3">
			<div class="sidebar">
				<div id="sidebar-collapse" class="collapse navbar-collapse" style="padding:0;">
					<ul class="nav nav-stacked">
						<li>
							<a href="<?=base_url()?>profile/<?php echo $this->session->userdata('alias')?>">個人基本資料</a>
							<a href="<?=base_url()?>user/ai_capability/<?php echo $this->session->userdata('alias')?>">個人能力檢核</a>
                        </li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<form id="editProfileForm" class="form-horizontal" role="form" method="post" action="<?php echo base_url().'user/update'?>" enctype="multipart/form-data">
    	<div style="display:none;" class="alert alert-success" id="success-alert">
    		<strong>更新成功</strong>
    	</div>
		<div class="col-md-8 col-md-offset-2">
			<form id="editProfileForm" class="form-horizontal" role="form" method="post" action="<?=base_url();?>api/user/editProfile" enctype="multipart/form-data">
            	<div class="form-group col-sm-12">
            		<h3>編輯個人基本資訊</h3>
            		<hr>  
            	</div>
                <div class="form-group col-sm-12">
                    <label for="name" class="control-label col-sm-3"><span style="color:red;">＊</span>姓名</label>
                    <div class="col-sm-7">
                    	<input type="text" class="form-control" id="name" name="name" maxlength="50"  value="<?php echo $profile['name']?>" placeholder="請填入姓名">
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <label for="gender" class="control-label col-sm-3"><span style="color:red;">＊</span>性別</label>
                    <div class="col-sm-7">    
                        <label>
                            <input type="radio" name="gender" id="gender_status2" value="1" <?php echo $profile['gender']==1?"checked":"" ?> >男
                        </label>
                        <label>
                            <input type="radio" name="gender" id="gender_status1" value="0" <?php echo $profile['gender']==0?"checked":"" ?> >女
                        </label>
                    </div>
                </div>	
                <div class="form-group col-sm-12">
                    <label for="birthday" class="control-label col-sm-3"><span style="color:red;">＊</span>生日</label>
                    <div class="col-sm-7">  
                    	<input type="date" id="birthday" name="birthday" maxlength="50" class="form-control" value="<?php echo $profile['birthday']?>" placeholder="請填入生日">
                    </div>
                </div>		                	                
                <div class="form-group col-sm-12">
                    <label for="education_highest_level" class="control-label col-sm-3"><span style="color:red;">＊</span>最高學歷</label>
                    <div class="col-sm-7">  
	                    <select id="education_highest_level" name="education_highest_level" class="form-control">
	                        <option value="1" <?php if($profile['education_highest_level']==1){echo 'selected';}?>>高中職</option>
	                        <option value="2" <?php if($profile['education_highest_level']==2){echo 'selected';}?>>大學</option>
	                        <option value="3" <?php if($profile['education_highest_level']==3){echo 'selected';}?>>研究所</option>
	                        <option value="4" <?php if($profile['education_highest_level']==4){echo 'selected';}?>>博士</option>
	                    </select>
	                </div>
                </div>
                <div class="form-group col-sm-12">
                    <label for="mobile" class="control-label col-sm-3"><span style="color:red;">＊</span>連絡電話</label>
                    <div class="col-sm-7">  
                    	<input type="text" id="mobile" name="mobile" maxlength="50" class="form-control" value="<?php echo $profile['mobile']?>" placeholder="請填入電話">
                    </div>
                </div>
                <div class="form-group col-sm-12">
                    <label for="experience" class="control-label col-sm-3"><span style="color:red;">＊</span>相關經驗 (曾參與相關賽事、接案、工作或實習經歷)</label>
                    <div class="col-sm-7">  
                    	<textarea type="text" id="experience" name="experience" maxlength="50" rows="10" class="form-control" value="<?php echo $profile['experience']?>" placeholder="請填入相關經驗"><?php echo $profile['experience']?></textarea>
                    </div>
                </div>		                

                <div class="form-group">
                    <div class="form-group col-sm-12">
                    	<div class="col-sm-offset-5 col-sm-2">
                    		<input type="submit" id="saveBtn" class="btn btn-success form-control" value="儲存">
                    	</div>
                    </div>
                </div>
			
<!-- 					<div class="col-xs-12">
						<div class="col-xs-12">
							<h3 style="line-height:1.4em;"><?php echo $profile['alias']?></h3>
							<h4 style="line-height:1.4em;"><?php echo $profile['email']?>
								<?php if($profile['active_flag']=='0'){?>
									<span class="label label-warning">信箱尚未通過認證</span>
								<?}?> 
							</h4>
							<p class="text-muted">since <?php echo date_format(date_create($profile['register_date']), 'Y-m-d'); ?></p>
						</div>
						<div class="col-xs-12">
							<span class="profile-img">
								<img src="<?php echo base_url()?>assets/img/user/128x128/<?php echo $profile['img']?>">
							</span>
						</div>
						<hr>
						 <div class="col-xs-12"> -->
							<!-- 
								<a style="font-size:0.75em;" href="<?php echo base_url().'user/edit'?>" class="btn btn-primary" role="button">編輯個人資料</a>
							 -->
<!-- 							<a style="font-size:0.75em;" class="btn btn-danger" role="button" data-toggle="modal" data-target="#modifyPass">修改密碼</a>
							<?php if($profile['active_flag']=='0'){?>
								<button style="font-size:0.75em;" id="modifyEmailBtn" type="button" class="btn btn-danger"  data-toggle="modal" data-target="#change-email-modal">修改認證信箱</button>
								<button style="font-size:0.75em;" id="resendActiveMailBtn" 	type="button" class="btn btn-info">重寄認證信</button><span id="resendActiveMail"></span>
							<?}?>
							<a style="font-size:0.75em;" class="btn btn-default" role="button" data-toggle="modal" data-target="#edit-picture-modal">修改照片</a>
						 </div>
					</div> -->
<!-- 					<div class="modal fade" id="edit-picture-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title" id="myModalLabel">修改照片</h4>
								</div>
								<div class="modal-body">
									<input type="file" id="img" name="userfile">
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
									<button type="submit" class="btn btn-primary">確認修改照片</button>
					        	</div>
							</div>
						</div>
					</div> -->
			</form>

		</div>
		<!-- 
		<div class="well row">
			<div class="col-xs-12">
				<h3 style="line-height:1.4em;">總積分 <?echo $getTotalPoints;?></h3>
			</div>
			<div class="col-xs-12">
				<div class="col-8 col-sm-8 col-md-8 col-lg-8" style="margin-top:10px;">
					<? $row = json_decode($getPoints);?>
					<?$chart=array();?>
					<?//$color=array("#ecb61f","#38b473","#5dc7d2","#e34c48","#9a86c6","#f8855c","#adb7c6","#53519f","#f36284","#893d52","#53bbb4","#9ea5b3","#666");?>
					<ul style="list-style: none">
						<?php if(!empty($row)){?>
							<?php foreach($row as $key=>$val){?>
								<li class="col-xs-6 col-sm-4">
									<div>
										<span style="float: left;margin: 3px 0 0 -25px; color: #<?echo $val->library_color?>"><i class="fa fa-circle"></i></span>
										<h3><?php echo $val->sum?></h3>
										<p><?php echo $val->library_name?></p>
									</div>
								</li>
								<?php array_push($chart,'{value:'.$val->sum.',color:\'#'.$val->library_color.'\'}')?>
							<?}?>
						<?}?>
					</ul>
				</div>
				<div class="col-4 col-sm-4 col-md-4 col-lg-4" style="margin-top:10px;">
					<?php $chart = str_replace ('"'," ",json_encode( $chart ));?>
					<script> var doughnutData = <?echo $chart;?>;</script>
					<canvas id="canvas" height="200"></canvas>
				</div>
			</div>
		</div>
		 -->
		<!--
		<div style="margin-bottom:50px;" class="row">
			<?php $row = json_decode($getUserChapterBadge); ?>
			<div>
			  <?if($row){?>
			  	<h3>我的成就</h3>
			  <?}?>
			</div>
			<div class="row">
				<?foreach($row as $key=>$val){?>
					<? $image=urlencode(base_url()."assets/img/badges/".$val->chapter_pic);?>
					<? $url=urlencode(base_url().'library/'.$val->library_url.'/'.$val->course_url.'/'.$val->chapter_url);?>
					<div class="col-6 col-sm-6 col-md-6 col-lg-6" style="margin-top:10px;">
						<div class="thumbnail">
							<div class="caption">
								<div style="float: left;width: 70%;">
									<h4><?php echo $val->chapter_name?></h4>
									<p class="text-muted"><?php echo $val->accomplish_time?> 完成</p>
    	        					<a class="btn btn-default" onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $title;?>&amp;p[description]=<?php echo $summary;?>&amp;p[summary]=<?php echo $summary;?>&amp;p[url]=<?php echo $url; ?>&amp;&p[images][0]=<?php echo $image;?>', 'sharer', 'toolbar=0,status=0,width=550,height=400');" target="_parent" href="javascript: void(0)">
                    				<span>分享</span>
            						</a>
								</div>
								<div style="float: left;width: 30%;">
									<img src="<?=base_url(); ?>assets/img/badges/<?php echo $val->chapter_pic?>">
								</div>
								<div style="clear:both;"></div>
							</div>
						</div>
					</div>
				<?}?>
			 </div>
		</div>/row-->
	</div><!-- /container -->
</div><!-- /content -->
<!--修改照片-->
<!-- Modal -->
<div class="modal fade" id="change-email-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="modifyEmail" action="<?php echo base_url()?>user/updateUserEmail" method="post">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">修改認證信箱</h4>
				</div>
				<div class="modal-body">
	                <div class="form-group">
	                    <label for="contentEmail" class="col-sm-2 control-label">信箱</label>
	                    <div class="col-sm-10">
	                        <input type="text" class="form-control" id="contentEmail" name="contentEmail" placeholder="如果您的信箱收不到認證信，請換一個信箱" value="<?php echo $getOneUser['email']?>" <?php echo ($getOneUser['active_flag']=='0')?'':'disabled'?>>
	                    </div>
	                </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
					<button type="submit" class="btn btn-primary">確認修改信箱</button>
	        	</div>
	        </form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--修改密碼-->
<!-- Modal -->
<div class="modal fade" id="modifyPass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="modifyPassword" action="<?php echo base_url()?>user/modifyPassword" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">修改密碼</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="oldPassword" class="control-label">舊密碼</label>
					<input type="password" class="form-control" id="oldPassword" name="oldPassword" placeholder="請輸入舊密碼">
				</div>
				<div class="form-group">
					<label for="newPassword" class="control-label">新密碼</label>
					<input type="password" class="form-control" id="newPassword" name="newPassword" placeholder="請輸入新密碼">
				</div>
				<div class="form-group">
					<label for="checkPassword" class="control-label">確認新密碼</label>
					<input type="password" class="form-control" id="checkPassword" name="checkPassword" placeholder="請確認新密碼">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				<button type="submit" class="btn btn-primary">確認修改密碼</button>
			</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script>
// $("input[maxlength]").maxlength();
$('#editProfileForm').submit(function(e){
    var postData = $('#editProfileForm').serializeArray();
    var formURL = $('#editProfileForm').attr("action");
    $.ajax({
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) {
        	console.log(data);
            if(data["result"]=="success"){
                $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#success-alert").slideUp(500);
                });
            }else{
                $("#danger-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#danger-alert").slideUp(500);
                });
            }
            
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //if fails     
        }
    });
    e.preventDefault(); //STOP default action
});
</script>