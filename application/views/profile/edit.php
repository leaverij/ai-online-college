<div id="content">
<div class="container">
    <div class="row">
        <div class="col-md-8 well">
            <form id="editProfileForm" class="form-horizontal" role="form" method="post" action="<?php echo base_url().'user/update'?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="alias" class="col-sm-2 control-label">ID</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="alias" name="alias" placeholder="請填入ID" value="<?php echo $getOneUser['alias']?>" disabled>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="contentEmail" class="col-sm-2 control-label">信箱</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="contentEmail" name="contentEmail" placeholder="如果您的信箱收不到認證信，請換一個信箱" value="<?php echo $getOneUser['email']?>" <?php echo ($getOneUser['active_flag']=='0')?'':'disabled'?>>
                    </div>
					<?php if($getOneUser['active_flag']=='0'){?>
						<span class="label label-warning">信箱尚未通過認證</span>
					<?}?> 
                </div>
                <hr>
                <div class="form-group">
                    <label for="img" class="col-sm-2 control-label">照片</label>
                    <div class="col-sm-1">
                        <img src="<?php echo base_url()?>assets/img/user/128x128/<?php echo $getOneUser['img']?>">
                        <input type="file" id="img" name="userfile">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label"></label>
                    <div class="col-sm-10">
                        <?php echo $error;?>
                    </div>
                </div>
                <!-- 
                <hr>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">姓名</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="name" name="name" placeholder="請填入姓名" value="<?php echo $getOneUser['name']?>">
                    </div>
                </div>
                 -->
                <!-- 
                <hr>
                <div class="form-group">
                    <label for="intro" class="col-sm-2 control-label">個人簡介</label>
                    <div class="col-sm-6">
                        <textarea class="form-control" id="myintro" name="myintro" rows="3" placeholder="請填入個人簡介"><?php echo $getOneUser['intro']?></textarea>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">地址</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="address" name="address" placeholder="請填入地址" value="<?php echo $getOneUser['address']?>">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="mobile" class="col-sm-2 control-label">手機</label>
                    <div class="col-sm-6">
                        <input type="test" class="form-control" id="mobile" name="mobile" placeholder="請填入手機" value="<?php echo $getOneUser['mobile']?>">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="education_highest_level" class="col-sm-2 control-label">最高學歷</label>
                    <div class="col-sm-6">
                        <select id="education_highest_level" name="education_highest_level" class="form-control">
                            <option value="0">請選擇最高學歷</option>
                              <option value="1" <? if($getOneUser['education_highest_level']=='1') echo 'selected="selected"'; ?>>博士</option>
                              <option value="2" <? if($getOneUser['education_highest_level']=='2') echo 'selected="selected"'; ?>>碩士</option>
                              <option value="3" <? if($getOneUser['education_highest_level']=='3') echo 'selected="selected"'; ?>>大學技職</option>
                              <option value="4" <? if($getOneUser['education_highest_level']=='4') echo 'selected="selected"'; ?>>高中職</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="work_status" class="col-sm-2 control-label">就業狀態</label>
                    <div class="col-sm-6">
                        <select id="work_status" name="work_status" class="form-control">
                            <option value="0">請選擇就業狀態</option>
                              <option value="1" <? if($getOneUser['work_status']=='1') echo 'selected="selected"'; ?>>工作中(仍在職)</option>
                              <option value="2" <? if($getOneUser['work_status']=='2') echo 'selected="selected"'; ?>>無工作(待業中)</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <label for="work_skills" class="col-sm-2 control-label">擅長工具</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="work_skills" name="work_skills" placeholder="請填入擅長工具，如php、java.." value="<?php echo $getOneUser['work_skills']?>">
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                          <div class="checkbox">
                            <label>
                                  <input type="checkbox" name="recruiters" <?php echo ($getOneUser['recruiters']=='1')?'checked':''?>>我願意公布我的學習歷程供業者求才找尋之用
                            </label>
                          </div>
                    </div>
                  </div>
                 -->
                <hr>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">儲存資料</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-4">
        </div>
    </div>
</div><!-- /container -->
</div>