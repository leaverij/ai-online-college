<div class="container">
    <?php // print_r($jupyter);exit;?>
    <script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
    <script src="<?=base_url();?>assets/js/moment.js" type="text/javascript"></script>
    <script src="<?=base_url();?>assets/js/ez.countimer.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var library= "<?php echo $library;?>";
        var course= "<?php echo $course;?>";
        var chapter="<?php echo $chapter;?>";
        currentMovie="<?php echo $this->session->flashdata('currentMovie');?>";
        currentChapter="<?php echo $this->session->flashdata('currentChapter');?>";
        isQuizDone="<?php echo $this->session->flashdata('isQuizDone');?>";
        
        jQuery(document).ready(function($){
            <?php
                if(array_key_exists('instanceURL',$jupyter)){
                    if(!empty($jupyter['instanceURL'])){
                        ?> 
                            var current_progress = 100;
                            $("#dynamic")
                                .css("width", current_progress + "%")
                                .attr("aria-valuenow", current_progress)
                                .text(current_progress + "% Complete");
                            var instanceURL="<?php echo $jupyter['instanceURL'];?>";
                            var instanceID="<?php echo $jupyter['instanceID'];?>";

                            $('#stoplab').click(function(e){
                                $.ajax({
                                    url: base_url+ "ec2/ManageInstance?instanceid="+instanceID+"&action=STOP",
                                    success:function(data){
                                        const rdata = JSON.parse(data);
                                        if(rdata.status==='success'){
                                            $('#restartlab').show();
                                            $('#stoplab').hide();
                                            $('#openlab').prop('disabled', true);
                                            $('#lab-duration').countimer('stop');
                                        }  
                                    }
                                }); 
                            });
                            $('#restartlab').click(function(e){
                                $.ajax({
                                    url: base_url+ "ec2/ManageInstance?instanceid="+instanceID+"&action=START",
                                    success:function(data){
                                        const rdata = JSON.parse(data);
                                        if(rdata.status==='success'){
                                            $('#restartlab').hide();
                                            $('#stoplab').show();
                                            $('#openlab').prop('disabled', false);
                                            $('#openlab').click(function(e){
                                                window.open('http://'+rdata.dns);
                                            });
                                            $('#lab-duration').countimer('resume');
                                        }
                                    }
                                }); 
                            });
                            $('#startlab').hide();
                            <?php
                                if($jupyter['status']=='1'){ // 已啟動
                            ?>
                                    $('#stoplab').show();
                                    $('#openlab').prop('disabled', false);
                                    $('#openlab').click(function(e){
                                        window.open('http://'+instanceURL+'/');
                                    });
                                    var period="<?php echo $jupyter['periodtime'];?>";
                                    var phour = Math.floor(period / 3600);
                                    var pmin = Math.floor((period % 3600) / 60);
                                    var psec = period - phour*60 - pmin*60;
                                    $('#lab-duration').countimer({
                                        autoStart: true,
                                        initHours: phour,
                                        initMinutes: pmin,
                                        initSeconds: psec
                                    });
                            <?php
                                }else if($jupyter['status']=='0'){ // 停止中
                            ?>
                                    $('#restartlab').show();
                                    var period="<?php echo $jupyter['period'];?>";
                                    var phour = Math.floor(period / 3600);
                                    var pmin = Math.floor((period % 3600) / 60);
                                    var psec = period - phour*60 - pmin*60;
                                    $('#lab-duration').countimer({
                                        autoStart: false,
                                        initHours: phour,
                                        initMinutes: pmin,
                                        initSeconds: psec
                                    });
                            <?php
                                }
                            ?>
                            
                            
                        <?php
                    }
                }else{
                    ?> 
                        var courseId="<?php echo $jupyter['course_id'];?>";
                        var chapterId="<?php echo $jupyter['chapter_id'];?>";
                        var chapterContentId="<?php echo $jupyter['chapter_content_id'];?>";
                        $('#lab-duration').countimer({
                            autoStart : false
                        });
                        $('#startlab').click(function(e){
                            var current_progress = 0;
                            var interval = setInterval(function() {
                                current_progress += 10;
                                $("#dynamic")
                                .css("width", current_progress + "%")
                                .attr("aria-valuenow", current_progress)
                                .text(current_progress + "% Complete");
                                if (current_progress >= 90)
                                    clearInterval(interval);
                            }, 1500);
                             $.ajax({
                                url: base_url+ "ec2/createInstance?courseId="+courseId+"&chapterId="+chapterId+"&chapterContentId="+chapterContentId,
                                success:function(data){
                                    const r = JSON.parse(data);
                                    if(r.status==='success'){
                                        const url = r.url;
                                        const id = r.id;
                                        $('#startlab').hide();
                                        $('#stoplab').show();
                                        var current_progress = 100;
                                        $("#dynamic")
                                            .css("width", current_progress + "%")
                                            .attr("aria-valuenow", current_progress)
                                            .text(current_progress + "% Complete");
                                        $('#openlab').prop('disabled', false);
                                        $('#openlab').click(function(e){
                                            window.open('http://'+url);
                                        });
                                        $('#lab-duration').countimer('start');
                                        $('#stoplab').click(function(e){
                                            $.ajax({
                                                url: base_url+ "ec2/ManageInstance?instanceid="+id+"&action=STOP",
                                                success:function(data){
                                                    const rdata = JSON.parse(data);
                                                    if(rdata.status==='success'){
                                                        $('#restartlab').show();
                                                        $('#stoplab').hide();
                                                        $('#openlab').prop('disabled', true);
                                                        $('#lab-duration').countimer('stop');
                                                    }  
                                                }
                                            }); 
                                        });
                                        $('#restartlab').click(function(e){
                                            $.ajax({
                                                url: base_url+ "ec2/ManageInstance?instanceid="+id+"&action=START",
                                                success:function(data){
                                                    const rdata = JSON.parse(data);
                                                    if(rdata.status==='success'){
                                                        $('#restartlab').hide();
                                                        $('#stoplab').show();
                                                        $('#openlab').prop('disabled', false);
                                                        $('#openlab').click(function(e){
                                                            window.open('http://'+rdata.dns);
                                                        });
                                                        $('#lab-duration').countimer('resume');
                                                    }
                                                }
                                            }); 
                                        });
                                    }
                                }
                            }); 
                        });
                    <?php 
                }
            ?>
        });

    </script>
    <style>
    .progress, .progress-bar{
        height: 35px;
    }
    #lab-duration {
        font-size: 20px;
    }
    </style>
    <?php 
        $this->load->view('breadcrumb');
        $chapter_content = json_decode($getOneContent);
		$row2 = json_decode($getOneChapterContent);
		$getOneTeacher=json_decode($getOneTeacher);
		if(!empty($getUserContent)){
			$row3 = json_decode($getUserContent); 
			$data=array('row2'=>$row2,'row3'=>$row3);
		}else{
			$data=array('row2'=>$row2);
		}
	?>
    <div id="article-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4><?php echo $chapter_content['0']->chapter_content_desc;?></h4>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="progress">
                                        <div class="input-group">
                                            <div 
                                                class="progress-bar progress-bar-success progress-bar-striped active" 
                                                role="progressbar" 
                                                aria-valuenow="0" 
                                                aria-valuemin="0" 
                                                aria-valuemax="100" 
                                                style="width: 0%;"
                                                id="dynamic"
                                            >
                                                <span id="current-progress"></span>
                                            </div>
                                            <span class="input-group-btn">
                                                <button 
                                                    class="btn btn-primary" 
                                                    type="button"
                                                    id="startlab"
                                                >啟動 LAB</button>
                                                <button 
                                                    class="btn btn-primary" 
                                                    type="button"
                                                    id="stoplab"
                                                    style="display:none"
                                                >暫停 LAB</button>
                                                <button 
                                                    class="btn btn-primary" 
                                                    type="button"
                                                    id="restartlab"
                                                    style="display:none"
                                                >開始 LAB</button>
                                            </span>
                                            <span 
                                                class="input-group-addon" 
                                                id="lab-duration">
                                                <!-- 02:00:00 -->
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <?php 
                                if(!empty($chapter_content['0']->lab_type) && $chapter_content['0']->lab_type==1){
                            ?>
                            <div class="form-group">
                                <a  href=""
                                    class="btn btn-primary"
                                    style="width:100%"
                                    >打開 LAB</a>
                            </div>
                            <?php
                                }else{
                            ?>
                            <div class="form-group">
                                <button 
                                    type="button" 
                                    class="btn btn-primary"
                                    id="openlab"
                                    style="width:100%"
                                    disabled
                                    >打開 LAB</button>
                            </div>
                            <?php
                                }
                            ?>
                            
                        </div>
                    </div>
                    <?php
                        //if(strpos($chapter_content['0']->content,'pynb')!=0){
                    ?>
                    <!--
                    <h4>URL:<a target="_blank" href="<?php echo $chapter_content['0']->content;?>"><?php echo $chapter_content['0']->content;?></a></h4>
                    -->
                    <?php
                        //}else{
                    ?>
                    <!--
                    <iframe width="100%" height="600px" src="<?php echo $chapter_content['0']->content;?>">
                    -->
                    <?php
                        //}
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <p><?php echo $chapter_content['0']->content_desc ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><hr>
    <div id="achievement-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
					<?php $this->load->view('chapter/contentList',$data);?>
				</div>
                <div class="col-sm-4">
                    <div style="margin-bottom:30px;">
                        <h3 class="secondary-heading" style="font-size:1.5em; font-weight:600; margin-top:0;">講師</h3>
                        <div>
                            <div class="instructor-avatar">
                                <img style="width:80px;height:80px;" src="<?=base_url();?>assets/img/teacher/<?php echo $getOneTeacher['0']->teacher_pic;?>" />
                            </div>    
                            <h4 style="padding-left:100px; line-height:4.5em;"><?php echo $getOneTeacher['0']->name;?></h4>
                            <p><?php echo $getOneTeacher['0']->introduction;?></p>
                        </div>
                    </div>
                    <div class="teaching-resources">
                		<h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">教學資源下載區</h3>
                    	<?php $row = json_decode( $listResource );?>
							<?php if(!empty($row)){?>
								<?php foreach($row as $val){?>
                            		<h4 style="color:#a7a7a7;">
                            			<a class="download" data-id="<?php echo $val->dl_resource_id;?>" href="javascript: void(0)">
                            				<?php echo $val->dl_resource_url;?>
                            			</a>
                            		</h4>
                            	<?}?>
							<?}else{?>
								<h4 style="color:#a7a7a7;">目前尚無資源可下載</h4>	
							<?}?>
                    </div> 
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- #achievement-content -->
</div><!-- .container -->   
<? $this->load->view('home/modal_login');?>
<script>
/* $(document).ready(function(){
    var labDuration = moment.utc(<?php echo $chapter_content['0']->time_spent ?>*1000).format('HH:mm:ss');
    $('#lab-duration').html(labDuration);
    
}); */
</script>