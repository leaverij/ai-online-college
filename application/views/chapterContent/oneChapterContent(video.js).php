<style type="text/css">
#overlay {
	position: relative; 
	color: #FFF; 
	text-align: center;
	font-size: 20px;
	background-color: rgb(0, 0, 0);
	left:15px;
	z-index: 3;
}

#rewatch{
	position: relative; 
	top:40%;
}
#nextMission{
	position: relative; 
	top:41%;
}
#example_video_1{
	z-index:2;
}
</style>
<!-- 可外掛speed的舊版Video.js -->
	<script src="<?=base_url();?>assets/js/video-js/video.js"></script>
<!-- speed plugin -->
	<script src="<?=base_url();?>assets/js/video-js/video-speed.js" type="text/javascript" charset="utf-8"></script>
<div class="container">
	<?php 
		$this->load->view('breadcrumb');
		$row4 = json_decode($getOneContent);
		$row6 = json_decode($getNextContent);
		// $row5 = json_decode($getChapterPlaylist);
	?>
	<?php 
		// $playlist=array();
		// for($i=0;$i<count($row5);$i++){
			// array_push($playlist,$row5[$i]->content);
		// }
		// $index= array_search($row4['0']->content,$playlist);
	?>
    <div class="row">
		<div class="col-md-10 col-md-offset-2">
			<video id="example_video_1" class="video-js vjs-default-skin"  
			  controls preload="auto"
			  data-setup='{"example_option":true}'
			  autoplay="true">  
			 <source type='video/mp4' />

			 <!-- <track id="caption" kind="captions" src="UXD.vtt" srclang="zh" label="Chinese" default/> -->
			</video>
		</div>
	<div id="overlay" class="col-md-10 col-md-offset-2">
		<a id="rewatch" 	class="btn btn-primary" href="<?php echo base_url().'library/'.$library.'/'.$course.'/'.$chapter.'/'.$row4['0']->chapter_content_url?>"><span class="glyphicon glyphicon-repeat"></span> 再看一次 <?php echo $row4['0']->chapter_content_desc;?></a><br />
		<a id="nextMission" class="btn btn-success" href="<?php echo base_url().'library/'.$library.'/'.$course.'/'.$chapter.'/'.$row6['0']->chapter_content_url?>">下一個任務: <?php if($row6['0']->content_type==0){echo $row6['0']->chapter_content_desc;}elseif($row6['0']->content_type==1){echo '進行測驗';}else{echo '進行挑戰';}?> <span class="glyphicon glyphicon-arrow-right"></span></a><br />
	</div>
	</div>
    <script type="text/javascript">
		var chapter 			= "<?php echo $chapter;?>";
		var content 			= "<?php echo $row4['0']->content;?>";
		var chapter_content_url = "<?php echo $row4['0']->chapter_content_url;?>";
    </script>
	<?php $row2 = json_decode($getOneChapterContent); ?>
	<?php
		if(!empty($getUserContent)){
			$row3 = json_decode($getUserContent); 
			$data=array('row2'=>$row2,'row3'=>$row3,'row4'=>$row4);
		}else{
			$data=array('row2'=>$row2,'row4'=>$row4);
		}
		$this->load->view('chapter/contentList',$data);
	?>
</div><!-- /container -->