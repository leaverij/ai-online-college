<div class="container">
	<?php 
		$this->load->view('breadcrumb');
		$getOneTeacher=json_decode($getOneTeacher);
		$row4 = json_decode($getOneContent);
		$row6 = json_decode($getNextContent);
	?>
</div>

<?php 
	$data=array('row4'=>$row4,'row6'=>$row6);
	$this->load->view('chapterContent/flowplayer',$data);
?>
    <!--     BLOCK VIDEO PLAYER  -->
    <style>
    #achievement-content{padding:40px 0;}
    </style>
    <div id="achievement-content">
    	<div class="container">
			<script type="text/javascript">
                var chapter 			= "<?php echo $chapter;?>";
                var content 			= "<?php echo $row4['0']->content;?>";
                var chapter_content_url = "<?php echo $row4['0']->chapter_content_url;?>";
                var subtitle			= "<?php echo $row4['0']->subtitle;?>";
                var email				= "<?php echo $this->session->userdata('email');?>";
                var preview				= "<?php echo $row4['0']->preview;?>";
            </script>
        	<div class="row">
            	<div class="col-md-8">
					<?php $row2 = json_decode($getOneChapterContent); ?>
                    <?php
                        if(!empty($getUserContent)){
                            $row3 = json_decode($getUserContent); 
                            $data=array('row2'=>$row2,'row3'=>$row3,'row4'=>$row4,'chapter'=>$chapter);
                        }else{
                            $data=array('row2'=>$row2,'row4'=>$row4,'chapter'=>$chapter);
                        }
                        // if(!empty($chapter) && !empty($row4['0']->subtitle)){
                            // $this->load->view('chapterContent/parseCaption',$data);
                        // }
                        $this->load->view('chapter/contentList',$data);
                    ?>
                </div>
                <div class="col-md-4">
                	   <!--     INSTRUCTOR    -->
                    <style>
                    .instructor-avatar{
                        float:left;
                        width:80px;
                        height:80px;
                        background:url(<?=base_url();?>assets/img/teacher/<?php echo $getOneTeacher['0']->teacher_pic;?>);
                        background-position:center;
                        background-size:80px auto;
                        border-radius:50px;}
                    .instructor-avatar img{width:80px; height:80px; display:none;}
                    </style>
                    <div>
                        <h3 style="margin:0 0 30px;">講師</h3>
                        <div>
                            <div class="instructor-avatar">
                                <img src="<?=base_url();?>assets/img/teacher/<?php echo $getOneTeacher['0']->teacher_pic;?>" />
                            </div>    
                            <h4 style="padding-left:100px; line-height:4.5em;"><?php echo $getOneTeacher['0']->name;?></h4>
                            <p><?php echo $getOneTeacher['0']->introduction;?></p>
                        </div>
                    </div> 
                    <hr />
                        <div>
                		<h3 style="margin:0 0 30px;">教學資源下載區</h3>
                    	<h4 style="color:#a7a7a7;">目前尚無資源可下載</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

