<div class="container">
	<?php 
		$this->load->view('breadcrumb');
		$getOneTeacher=json_decode($getOneTeacher);
		$row4 = json_decode($getOneContent);
		$row6 = json_decode($getNextContent);
	?>
</div>
<div>
	<!--     BLOCK VIDEO PLAYER  -->
    <style>

    </style>
	<div id="player-content">
        <div class="container">
        	<div class="row">
            	<div class="col-md-10 col-md-offset-1" style="background:#666666; padding:15px; border-radius:5px;">
                    <div class="player play-button color-light">
                        <div class="buttons hide">
                            <span>0.25x</span>
                            <span>0.5x</span>
                            <span class="active">1x</span>
                            <span>1.5x</span>
                            <span>2x</span>
                        </div>
                        <?php if(!empty($row4['0']->subtitle)){?>
                            <div id="captionbtn"></div>
                        <?php }?>
                    </div>
                    <div id="overlay" class="hide">
                        <a id="rewatch" 	class="btn btn-primary" href="<?php echo base_url().'library/'.$library.'/'.$course.'/'.$chapter.'/'.$row4['0']->chapter_content_url?>"><span class="glyphicon glyphicon-repeat"></span> 再看一次 <?php echo $row4['0']->chapter_content_desc;?></a><br />
						<?php if(!empty($row6)){?>
							<a id="nextMission" class="btn btn-success <?=($row6['0'] -> content_open_flag=='0')?'disabled':'';?>" href="<?php echo base_url().'library/'.$library.'/'.$course.'/'.$chapter.'/'.$row6['0']->chapter_content_url?>">下一個任務: <?php if($row6['0']->content_type==0){echo $row6['0']->chapter_content_desc;}elseif($row6['0']->content_type==1){echo '進行測驗';}else{echo '進行挑戰';}?> <span class="glyphicon glyphicon-arrow-right"></span></a><br />
						<?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--     BLOCK VIDEO PLAYER  -->
    <style>
    #achievement-content{padding:40px 0;}
    </style>
    <div id="achievement-content">
    	<div class="container">
			<script type="text/javascript">
                var chapter 			= "<?php echo $chapter;?>";
                var content 			= "<?php echo $row4['0']->content;?>";
                var chapter_content_url = "<?php echo $row4['0']->chapter_content_url;?>";
                var subtitle			= "<?php echo $row4['0']->subtitle;?>";
                var email				= "<?php echo $this->session->userdata('email');?>";
                var preview				= "<?php echo $row4['0']->preview;?>";
            </script>
        	<div class="row">
            	<div class="col-md-8">
					<?php $row2 = json_decode($getOneChapterContent); ?>
                    <?php
                        if(!empty($getUserContent)){
                            $row3 = json_decode($getUserContent); 
                            $data=array('row2'=>$row2,'row3'=>$row3,'row4'=>$row4,'chapter'=>$chapter);
                        }else{
                            $data=array('row2'=>$row2,'row4'=>$row4,'chapter'=>$chapter);
                        }
                        // if(!empty($chapter) && !empty($row4['0']->subtitle)){
                            // $this->load->view('chapterContent/parseCaption',$data);
                        // }
                        $this->load->view('chapter/contentList',$data);
                    ?>
                </div>
                <div class="col-md-4">
                	   <!--     INSTRUCTOR    -->
                    <style>
                    .instructor-avatar{
                        float:left;
                        width:80px;
                        height:80px;
                        background:url(<?=base_url();?>assets/img/teacher/<?php echo $getOneTeacher['0']->teacher_pic;?>);
                        background-position:center;
                        background-size:80px auto;
                        border-radius:50px;}
                    .instructor-avatar img{width:80px; height:80px; display:none;}
                    </style>
                    <div>
                        <h3 style="margin:0 0 30px;">講師</h3>
                        <div>
                            <div class="instructor-avatar">
                                <img src="<?=base_url();?>assets/img/teacher/<?php echo $getOneTeacher['0']->teacher_pic;?>" />
                            </div>    
                            <h4 style="padding-left:100px; line-height:4.5em;"><?php echo $getOneTeacher['0']->name;?></h4>
                            <p><?php echo $getOneTeacher['0']->introduction;?></p>
                        </div>
                    </div> 
                    <hr />
                        <div>
                		<h3 style="margin:0 0 30px;">教學資源下載區</h3>
                    	<h4 style="color:#a7a7a7;">目前尚無資源可下載</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

