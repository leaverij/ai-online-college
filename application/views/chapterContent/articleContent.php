<div class="container">
    <script type="text/javascript">
    var library= "<?php echo $library;?>";
    var course= "<?php echo $course;?>";
    var chapter="<?php echo $chapter;?>";
    currentMovie="<?php echo $this->session->flashdata('currentMovie');?>";
    currentChapter="<?php echo $this->session->flashdata('currentChapter');?>";
    isQuizDone="<?php echo $this->session->flashdata('isQuizDone');?>";
    </script>
    <?php 
        $this->load->view('breadcrumb');
        $chapter_content = json_decode($getOneContent);
		$row2 = json_decode($getOneChapterContent);
		$getOneTeacher=json_decode($getOneTeacher);
		if(!empty($getUserContent)){
			$row3 = json_decode($getUserContent); 
			$data=array('row2'=>$row2,'row3'=>$row3);
		}else{
			$data=array('row2'=>$row2);
		}
	?>
    <div id="article-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4><?php echo $chapter_content['0']->chapter_content_desc;?></h4>
                    <?php echo $chapter_content['0']->content;?>
                </div>
            </div>
        </div>
    </div><hr>
    <style>
        #player-content {
            display:none;
        }
    </style>
    <div id="achievement-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
					<?php $this->load->view('chapter/contentList',$data);?>
				</div>
                <div class="col-sm-4">
                    <div style="margin-bottom:30px;">
                        <h3 class="secondary-heading" style="font-size:1.5em; font-weight:600; margin-top:0;">講師</h3>
                        <div>
                            <div class="instructor-avatar">
                                <img style="width:80px;height:80px;" src="<?=base_url();?>assets/img/teacher/<?php echo $getOneTeacher['0']->teacher_pic;?>" />
                            </div>    
                            <h4 style="padding-left:100px; line-height:4.5em;"><?php echo $getOneTeacher['0']->name;?></h4>
                            <p><?php echo $getOneTeacher['0']->introduction;?></p>
                        </div>
                    </div>
                    <div class="teaching-resources">
                		<h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">教學資源下載區</h3>
                    	<?php $row = json_decode( $listResource );?>
							<?php if(!empty($row)){?>
								<?php foreach($row as $val){?>
                            		<h4 style="color:#a7a7a7;">
                            			<a class="download" data-id="<?php echo $val->dl_resource_id;?>" href="javascript: void(0)">
                            				<?php echo $val->dl_resource_url;?>
                            			</a>
                            		</h4>
                            	<?}?>
							<?}else{?>
								<h4 style="color:#a7a7a7;">目前尚無資源可下載</h4>	
							<?}?>
                    </div> 
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- #achievement-content -->
</div><!-- .container -->   
<? $this->load->view('home/modal_login');?>