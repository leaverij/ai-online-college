<div class="container">
    <script type="text/javascript">
    var library= "<?php echo $library;?>";
    var course= "<?php echo $course;?>";
    var chapter="<?php echo $chapter;?>";
    currentMovie="<?php echo $this->session->flashdata('currentMovie');?>";
    currentChapter="<?php echo $this->session->flashdata('currentChapter');?>";
    isQuizDone="<?php echo $this->session->flashdata('isQuizDone');?>";
    </script>
    <?php 
        $this->load->view('breadcrumb');
        $chapter_content = json_decode($getOneContent);
		$row2 = json_decode($getOneChapterContent);
		$getOneTeacher=json_decode($getOneTeacher);
		if(!empty($getUserContent)){
			$row3 = json_decode($getUserContent); 
			$data=array('row2'=>$row2,'row3'=>$row3);
		}else{
			$data=array('row2'=>$row2);
		}
	?>
    <div id="article-content">
        <div class="container">
            <h4><?php echo $chapter_content['0']->chapter_content_desc;?></h4>
            <p><?php echo $chapter_content['0']->content_desc;?></p>
            <div class="row">
                <div class="col-md-8">
                    <div id="editor" style="width:100%;height:300px;"><?php echo $chapter_content['0']->content;?></div><br />
                    <button class="btn btn-success" type="submit" id="python_go">編譯</button>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#hintModal">點我看提示</button>
                </div>
                <div class="col-md-4">
                    <div id="result" style="width:100%;height:300px;" class="well"></div>
                </div>
            </div>
        </div>
    </div><hr>
    <div id="achievement-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
					<?php $this->load->view('chapter/contentList',$data);?>
				</div>
                <div class="col-sm-4">
                    <div style="margin-bottom:30px;">
                        <h3 class="secondary-heading" style="font-size:1.5em; font-weight:600; margin-top:0;">講師</h3>
                        <div>
                            <div class="instructor-avatar">
                                <img style="width:80px;height:80px;" src="<?=base_url();?>assets/img/teacher/<?php echo $getOneTeacher['0']->teacher_pic;?>" />
                            </div>    
                            <h4 style="padding-left:100px; line-height:4.5em;"><?php echo $getOneTeacher['0']->name;?></h4>
                            <p><?php echo $getOneTeacher['0']->introduction;?></p>
                        </div>
                    </div>
                    <div class="teaching-resources">
                		<h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">教學資源下載區</h3>
                    	<?php $row = json_decode( $listResource );?>
							<?php if(!empty($row)){?>
								<?php foreach($row as $val){?>
                            		<h4 style="color:#a7a7a7;">
                            			<a class="download" data-id="<?php echo $val->dl_resource_id;?>" href="javascript: void(0)">
                            				<?php echo $val->dl_resource_url;?>
                            			</a>
                            		</h4>
                            	<?}?>
							<?}else{?>
								<h4 style="color:#a7a7a7;">目前尚無資源可下載</h4>	
							<?}?>
                    </div> 
                </div>
            </div>
        </div><!-- .container -->
    </div><!-- #achievement-content -->
</div><!-- .container -->
<div id="progressModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>編譯中...</h4>
            </div>
            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="70" style="width: 70%">
                        <span class="sr-only">70</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 提示Modal -->
<div class="modal fade" id="hintModal" tabindex="-1" role="dialog" aria-labelledby="hintModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="hintModalLabel">編譯提示</h4>
      </div>
      <div class="modal-body">
        <?php if($chapter_content['0']->hint){
            echo $chapter_content['0']->hint;
        }else{
            echo "此題目沒有提示";
        }?>
      </div>
    </div>
  </div>
</div> 
<? $this->load->view('home/modal_login');?>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/ace.js" type="text/javascript" charset="utf-8"></script>
<script>
    $(document).ready(function(){
        var editor = ace.edit("editor");
    editor.setTheme("ace/theme/monokai");
    editor.getSession().setMode("ace/mode/python");
    $("#python_go").on('click', function(e) {
        $('#progressModal').modal('show');
  		var langid = 4;
  		var code = editor.getValue();
    	// var stdin = $('#stdin').val();
    	var stdin = "";
		var start_time = "2017-11-14 23:21:1";
		var cp_time = "2017-11-14 23:21:1";
		var json = {
			langid:langid,
			code:code,
			stdin:stdin,
			start_time:start_time,
			cp_time:cp_time
    	}
        $.ajax({
    	    type: 'post',
      	    data: json,
      	    url: 'http://54.238.56.49:80/go/compile',
      	    success : function(data, error, xhr){
                $('#progressModal').modal('hide');
      		    // 系統負荷不了，產生timeout
        	    if(data["data"].search("Execution Timed Out")>0){
        		    $("#result").html("<pre>"+data["data"]+"</pre>");  
          	    }else{
            	    if(data["err"].length>0){
            		    // 編譯失敗
              		    $("#result").html("<pre>"+data["err"]+"</pre>");  
            	    }else{
            		    // 編譯成功
              		    $("#result").html("<pre>"+data["data"]+"</pre>");  
            	    }
          	    }
            },
            error: function(xhr, status, err){
                $('#progressModal').modal('hide');
            }
        });
	});
    });
</script>