<div>
	<style>
	#player-content {margin-bottom:30px;}
	#player-content .overlay, #player-content .errorPage{background-color: rgb(0, 0, 0); text-align: center;}
	#player-content .overlay:before, #player-content .errorPage:before{content:""; height:100%; display:inline-block; vertical-align:middle; zoom:1;}
	#player-content .overlay .overlay-message, #player-content .errorPage .errorPage-message{width:90%; vertical-align:middle; display:inline-block;}
	#player-content .overlay .overlay-message h4{margin-top:0; color:#ffffff; font-weight:600;}
	#player-content .overlay .overlay-message p, #player-content .errorPage .errorPage-message p{font-size:16px; color:#dedede;}
	.overlay-message .rewatch{color:#dedede; opacity:0.8;}
	.overlay-message .rewatch:hover{cursor:pointer;color:#ffffff; text-decoration:none; opacity:1;}
	.overlay-message .rewatch span{color:#ffffff; font-size:18px; top:3px; right:2px;}	
    </style>
	<div id="player-content">
        <div class="container">
        	<div class="row">
            	<div class="col-md-10 col-md-offset-1 player_background" style="background:#393F48; padding:15px; border-radius:5px;">
					<!-- <div id="title"><p></p></div> -->
                    <div class="player play-button color-light">
						<div class="buttons hide">
                            <span>0.25x</span>
                            <span>0.5x</span>
                            <span class="active">1x</span>
                            <span>1.5x</span>
                            <span>2x</span>
                        </div>
						<div id="speedbtn" class="hide"></div>
                        <?php if(!empty($row2['0']->subtitle)){?>
                            <div id="captionbtn" class="hide"></div>
                        <?php }?>
                    </div>
                    <div class="overlay hide">
                    	<div class="overlay-message">
							<?php $email=$this->session->userdata('email');?>
                            <?if(!empty($email)){?>
                            	<?if($profile['active_flag']=='0'){?>
                            		<h4>還差最後一步</h4>
                               		<p>請認證您的信箱以觀看完整課程內容</p>
                                	<a class="btn btn-primary" href="<?php echo base_url().'profile/'.$this->session->userdata('alias');?>">認證您的信箱</a>
                            	<?}else{?>
                            		<p><a class="rewatch"></a><p/>
                                	<?php //if(!empty($row2)){?>
                                    	<a class="nextMission btn btn-success"></a><br />
                                	<?php //}?>
                            	<?}?>
                            <?}else{?>
                                <h4>註冊成為 Learning house 會員</h4>
                                <p>以觀看完整課程內容</p>
                                <a class="btn btn-primary" href="<?php echo base_url().'home/registration';?>">註冊成為 Learning house 會員</a>
                            <?}?>
                        </div>
                    </div>
					<div class="errorPage hide">
                    	<div class="errorPage-message">
							<p>播放影片時發生了錯誤，您要</p>
							<a id="error_replay" class="btn btn-success">重新播放影片?</a>
                        </div>
					</div>
                </div>
            </div>
        </div>
    </div>