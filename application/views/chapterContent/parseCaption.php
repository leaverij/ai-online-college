<?php
define('SRT_STATE_SUBNUMBER', 0);
define('SRT_STATE_TIME',      1);
define('SRT_STATE_TEXT',      2);
define('SRT_STATE_BLANK',     3);

$lines   = file(base_url()."/assets/subtitles/".$chapter."/".$row4['0']->subtitle);

$subs    = array();
$state   = SRT_STATE_SUBNUMBER;
$subNum  = 0;
$subText = '';
$subTime = '';

foreach($lines as $line) {
    switch($state) {
        case SRT_STATE_SUBNUMBER:
            $subNum = trim($line);
            $state  = SRT_STATE_TIME;
            break;

        case SRT_STATE_TIME:
            $subTime = trim($line);
            $state   = SRT_STATE_TEXT;
            break;

        case SRT_STATE_TEXT:
            if (trim($line) == '') {
                $sub = new stdClass;
                $sub->number = $subNum;
                list($sub->startTime, $sub->stopTime) = explode(' --> ', $subTime);
                $sub->text   = $subText;
                $subText     = '';
                $state       = SRT_STATE_SUBNUMBER;
				$sub->startTime	=str_replace(",",".",$sub->startTime);
				$sub->stopTime	=str_replace(",",".",$sub->stopTime);
				list($startHour, $startMinute,$startSecond)	=explode(':',$sub->startTime);
				list($stopHour, $stopMinute,$stopSecond)	=explode(':',$sub->stopTime);
				$sub->startSec	=$startHour*60	+$startMinute*60+$startSecond;
				$sub->stopSec	=$stopHour*60	+$stopMinute*60	+$stopSecond;
                $subs[]      = $sub;
            } else {
                $subText .= $line;
            }
            break;
    }
}
?>
<?php if(!empty($row4['0']->subtitle) && !empty($subs)){ ?>
	<div class="row" style="padding:30px 0 50px;">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<h2 style="margin:0 0 8px 0; text-align:center;">字幕</h2>
			</div>
		</div>
		<div class="row">
				<div id="transcript" class="col-md-10 col-md-offset-2 list-group">
					<?php foreach($subs as $key=>$value){?>
						<?php if($value->text){ ?>
						<a class="list-group-item col-md-10" style="cursor:pointer;" id="transcript_<?php echo $value->startSec;?>">
							<?php echo substr($value->startTime,3,-4);?>&nbsp;&nbsp;&nbsp;<?php echo $value->text;?>
						</a>
						<?php } ?>
					<?php }?>
				</div>
		</div>
	</div>
<?php }?>