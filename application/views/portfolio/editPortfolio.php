<?
$row= json_decode($getUserExperience);
$row2= json_decode($getUserAward);
$row3= json_decode($getUserLicense);
$row4= json_decode($getUserPortfolio);
$row6= json_decode($getUserSkill);
if(!empty($getUserEducation)){
	$row5= json_decode($getUserEducation);
}
// if(!empty($getUserTraining)){
$row7= json_decode($getUserTraining);
$row8= json_decode($getPortfolioSkill);
$row9= json_decode($getCareerSkill);
// }
$row10=json_decode($getPoints);
$row11=json_decode($getUserChapterBadge);
$row12=json_decode($getJobsCategory);
if(!empty($getSeniority)){
	$row13=json_decode($getSeniority);
}
///
// by JoeyC
///
$row14=json_decode($getLibAnalysis);
$row15=json_decode($getCourseScores);
$row17=json_decode($getAVGCourseScores);
$row18=json_decode($getChapterScores);
///
// by JoeyC End
///
$title=urlencode("獲得新徽章：我在learninghouse完成一個關卡！");
$summary=urlencode("大家也趕快來學習吧，在這裡你可以學習到新的技能以及更多的知識喔!");
?>
<style>
/* Landscape phones and down */
@media (max-width: 480px) {
	#canvas{
		clear:both;
		position: relative;
		left: -50px;
	}
	.video-resume iframe{
		min-width:200px;
		min-height:150px;
		width:100%;
		height:75%;
	}
}
@media(min-width:768px){
	#canvas{
		float:right;
	}
}
textarea{
	resize: vertical;
}
</style>
<!-- 左邊區塊 -->
<?php $this->load->view('portfolio/sidebar'); ?>
<!-- 左邊end -->
<div class="col-md-9">
	<!-- 右邊區塊 -->
		<form id="editProfileForm" class="form-horizontal" role="form" method="post" action="<?php echo base_url().'user/updatePortfolio'?>" enctype="multipart/form-data">
			<hr>
			<div class="item-container profile hide">
				<h3>基本資料</h3>
				<span style="color: #999999;">個人履歷的照片與帳號管理中的不同，請選擇較為正式的照片</span>
				<div class="well">
					<hr>
					<div class="form-group col-sm-12">
						<label for="name" class="col-sm-2 control-label"><span style="color:red;">＊</span>姓名</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" id="name" name="name" placeholder="請填入姓名" value="<?php echo $getOneUser['name']?>">
						</div>
						<div class="col-sm-4">
							<img src="<?php echo base_url()?>assets/img/user/128x128/<?php echo $getOneUser['resume_img']?>">
							<input type="file" id="resume_img" name="userfile">
						</div>
					</div>
					<hr>
					<div class="form-group">
						<label for="location" class="col-sm-2 control-label">所在地</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" id="location" name="location" placeholder="請填入所在地" value="<?php echo $getOneUser['location']?>">
						</div>
					</div>
					<hr>
					<div class="form-group">
						<label for="contact_email" class="col-sm-2 control-label">聯絡信箱</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" id="contact_email" name="contact_email" placeholder="請填入作為履歷連絡用的email信箱" value="<?php echo $getOneUser['contact_email']?>">
						</div>
					</div>
 					<hr>
					<div class="form-group">
						<label for="mobile" class="col-sm-2 control-label">手機</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" id="mobile" name="mobile" placeholder="請填入手機" value="<?php echo $getOneUser['mobile']?>">
						</div>
					</div>
					<hr>
					<div class="form-group">
						<label for="video_resume" class="col-sm-2 control-label">影音履歷</label>
						<div class="col-sm-6">
							<input class="form-control" type="text" name="video_resume" id="video_resume" size="70" placeholder="請填入youtube連結，格式如http://www.youtube.com/embed/bnv1Cmi9Ig0" value="<?php echo $getOneUser['video_resume'];?>"/>
						</div>
					</div>
					<?php if (!empty($getOneUser['video_resume'])){?>
						<hr>
						<div class="col-sm-offset-2 col-sm-12 video-resume" style="position:relative;top:15px;right:10px;">
							<iframe width="400" height="300" src="<?php echo $getOneUser['video_resume'];?>" frameborder="0" allowfullscreen></iframe>
						</div>
					<?php }?>
					<hr>
					<div class="form-group">
						<label for="intro" class="col-sm-2 control-label">關於我</label>
						<div class="col-sm-6">
							<textarea class="form-control" id="myintro" name="myintro" rows="3" placeholder="請填入關於您本人的簡介"><?php echo preg_replace( '!<br.*>!iU', "\n",$getOneUser['intro']);?></textarea>
						</div>
					</div>
                <hr>
                <div class="form-group">
                    <label for="work_status" class="col-sm-2 control-label">就業狀態</label> <!-- 工作中(仍在職) 無工作(待業中) -->
                    <div class="col-sm-6">
                        <select id="work_status" name="work_status" class="form-control">
                            <option value="0">請選擇就業狀態</option>
                              <option value="1" <? if($getOneUser['work_status']=='1') echo 'selected="selected"'; ?>>工作中(仍在職)</option>
                              <option value="2" <? if($getOneUser['work_status']=='2') echo 'selected="selected"'; ?>>無工作(待業中)</option>
                        </select>
                    </div>
                </div>
				</div>
				<div class="form-group">
					<div class="controls form-inline col-sm-12">
						<div class="next_div col-sm-offset-4 col-sm-1">
							<button type="button" class="btn btn-primary">下一步 <div class="fa fa-long-arrow-right"></div></button>
						</div>
					</div>
				</div>
			</div>
			<?/*
			<div class="item-container training_div hide">
			<h3>專業訓練</h3>
			<span style="color: #999999;">專業訓練可不填寫，但若填寫，需填入訓練名稱、訓練單位、開始年度及開始月份才可正確儲存</span>
				<div class="well">
					<?php //if(empty($row7)){?>
						<div class="training">
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="training_name">訓練名稱</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" id="training_name" name="training_name[]" placeholder="請填入專業訓練名稱" value="">
									</div>
									<label class="col-sm-2 control-label" for="training_unit">訓練單位</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" id="training_unit" name="training_unit[]" placeholder="請填入訓練單位名稱" value="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="training_start">開始年度</label>
									<div class="col-sm-4">
				                        <select id="training_start" name="training_start[]" class="form-control">
											<option value="0" selected>請選擇開始年度</option>
				                        	<?php //for($j=2014;$j>=1950;$j--){?>
												<option value="<?php //echo $j;?>">西元<?php //echo $j;?> 年 / 民國<?php //echo $j-1911;?> 年</option>
											<?php //}?>
				                        </select>
									</div>
									<div class="col-sm-3">
				                        <select id="training_start_month" name="training_start_month[]" class="form-control">
											<option value="0" selected>請選擇開始月份</option>
				                        	<?php //for($j=1;$j<=12;$j++){?>
												<option value="<?php //echo $j;?>"><?php //echo $j;?>月</option>
											<?php //}?>
				                        </select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="training_end">結束年度</label>
									<div class="col-sm-4">
				                        <select id="training_end" name="training_end[]" class="form-control">
											<option value="0" selected>請選擇結束年度</option>
				                        	<?php //for($j=2014;$j>=1950;$j--){?>
												<option value="<?php //echo $j;?>">西元<?php //echo $j;?> 年 / 民國<?php //echo $j-1911;?> 年</option>
											<?php //}?>
				                        </select>
									</div>
									<div class="col-sm-3">
				                        <select id="training_end_month" name="training_end_month[]" class="form-control">
											<option value="0" selected>請選擇結束月份</option>
				                        	<?php //for($j=1;$j<=12;$j++){?>
												<option value="<?php //echo $j;?>"><?php //echo $j;?>月</option>
											<?php //}?>
				                        </select>
									</div>
								</div>
							</div>
							<div class="pull-right">
								<a class="remove_training" href="javascript:void(0);"></a>
							</div>
						</div>
					<?php //}else{?>
						<?php //$i=0;?>
						<?php //foreach($row7 as $key=>$value){?>
						<?php //if($i>0){echo '<hr />';}?>
						<div class="training">
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="training_name">訓練名稱</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" id="training_name" name="training_name[]" placeholder="請填入專業訓練名稱" value="<?php //echo $row7[$key]->training_name;?>">
									</div>
									<label class="col-sm-2 control-label" for="job_title">訓練單位</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" id="training_unit" name="training_unit[]" placeholder="請填入訓練單位名稱" value="<?php //echo $row7[$key]->training_unit;?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="training_start">開始</label>
									<div class="col-sm-4">
				                        <select id="training_start" name="training_start[<?php //echo $i;?>]" class="form-control">
											<option value="0" <?php //if($row7[$key]->training_start=='0'){echo 'selected';}?>>請選擇開始年度</option>
				                        	<?php //for($j=2014;$j>=1950;$j--){?>
												<option value="<?php //echo $j;?>" <?php //if($row7[$key]->training_start==$j){echo 'selected';}?>>西元<?php //echo $j;?> 年 / 民國<?php //echo $j-1911;?> 年</option>
											<?php //}?>
				                        </select>
									</div>
									<div class="col-sm-3">
				                        <select id="training_start_month" name="training_start_month[<?php echo $i;?>]" class="form-control">
											<option value="0" <?php if($row7[$key]->training_start_month=='0'){echo 'selected';}?>>請選擇開始月份</option>
				                        	<?php for($j=1;$j<=12;$j++){?>
												<option value="<?php echo $j;?>" <?php if($row7[$key]->training_start_month==$j){echo 'selected';}?>><?php echo $j;?> 月</option>
											<?php }?>
				                        </select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="training_end">結束</label>
									<div class="col-sm-4">
				                        <select id="training_end" name="training_end[<?php echo $i;?>]" class="form-control">
											<option value="0" <?php if($row7[$key]->training_end=='0'){echo 'selected';}?>>請選擇結束年度</option>
				                        	<?php for($j=2014;$j>=1950;$j--){?>
												<option value="<?php echo $j;?>" <?php if($row7[$key]->training_end==$j){echo 'selected';}?>>西元<?php echo $j;?> 年 / 民國<?php echo $j-1911;?> 年</option>
											<?php }?>
				                        </select>
									</div>
									<div class="col-sm-3">
				                        <select id="training_end_month" name="training_end_month[<?php echo $i;?>]" class="form-control">
											<option value="0" <?php if($row7[$key]->training_end_month=='0'){echo 'selected';}?>>請選擇結束月份</option>
				                        	<?php for($j=1;$j<=12;$j++){?>
												<option value="<?php echo $j;?>" <?php if($row7[$key]->training_end_month==$j){echo 'selected';}?>><?php echo $j;?> 月</option>
											<?php }?>
				                        </select>
									</div>
								</div>
								<div class="pull-right">
									<a class="remove_training" href="javascript:void(0);"></a>
								</div>
							</div>
						</div>
						<?php //$i++;?>
						<?php //}?>
					<?php //}?>
					<hr>
					<a id="add_training" href="javascript:void(0);">新增專業訓練</a>
				</div>
				<div class="form-group">
					<div class="controls form-inline col-sm-12">
						<div class="prev_div col-sm-offset-4 col-sm-1">
							<button type="button" class="btn btn-primary"><div class="fa fa-long-arrow-left"></div> 上一步</button>
						</div>
						<div class="next_div col-sm-offset-2 col-sm-1">
							<button type="button" class="btn btn-primary">下一步 <div class="fa fa-long-arrow-right"></div></button>
						</div>
					</div>
				</div>
			</div>
			*/?>
			<div class="item-container experience_div hide">
				<h3>學歷</h3>
				<span style="color: #999999;">學歷可不填寫，但若填寫，需填入學校名稱、科系名稱、開始年度及開始月份才可正確儲存</span>
				<div class="well">
					<?php if(empty($row5)){?>
						<div class="education">
			                <div class="form-group">
			                	<div class="controls form-inline col-sm-12">
				                    <label for="degree" class="col-sm-2 control-label">學歷</label>
				                    <div class="col-sm-4">
				                        <select id="degree" name="degree[]" class="form-control">
											<option value="0" selected>請選擇學歷</option>
											<option value="1">高中職</option>
											<option value="2">大學</option>
											<option value="3">碩士</option>
											<option value="4">博士</option>
				                        </select>
				                    </div>
		               	 		</div>
			                </div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="school_name">學校名稱</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" id="school_name" name="school_name[]" placeholder="請填入學校名稱" value="">
									</div>
									<label class="col-sm-2 control-label" for="department">科系名稱</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" id="department" name="department[]" placeholder="請填入科系名稱" value="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="education_start">開始</label>
									<div class="col-sm-4">
				                        <select id="education_start" name="education_start[]" class="form-control">
											<option value="0" selected>請選擇開始年度</option>
				                        	<?php for($j=2014;$j>=1950;$j--){?>
												<option value="<?php echo $j;?>">西元<?php echo $j;?> 年 / 民國<?php echo $j-1911;?> 年</option>
											<?php }?>
				                        </select>
									</div>
									<div class="col-sm-3">
				                        <select id="education_start_month" name="education_start_month[]" class="form-control">
											<option value="0" selected>請選擇開始月份</option>
				                        	<?php for($j=1;$j<=12;$j++){?>
												<option value="<?php echo $j;?>"><?php echo $j;?> 月</option>
											<?php }?>
				                        </select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="education_end">結束</label>
									<div class="col-sm-4">
				                        <select id="education_end" name="education_end[]" class="form-control">
											<option value="0" selected>請選擇結束年度</option>
				                        	<?php for($j=2014;$j>=1950;$j--){?>
												<option value="<?php echo $j;?>">西元<?php echo $j;?> 年 / 民國<?php echo $j-1911;?> 年</option>
											<?php }?>
				                        </select>
									</div>
									<div class="col-sm-3">
				                        <select id="education_end_month" name="education_end_month[]" class="form-control">
											<option value="0" selected>請選擇結束月份</option>
				                        	<?php for($j=1;$j<=12;$j++){?>
												<option value="<?php echo $j;?>"><?php echo $j;?> 月</option>
											<?php }?>
				                        </select>
									</div>
									<div class="col-sm-3">
										<div class="radio">
										  <label>
										    <input type="radio" name="education_untilNow[]" value="1">
											畢業
										  </label>
										</div>
										<div class="radio">
										  <label>
										    <input type="radio" name="education_untilNow[]" value="2">
											肄業
										  </label>
										</div>
										<div class="pull-right">
											<a class="remove_education" href="javascript:void(0);"></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php }else{?>
						<?php $i=0;?>
						<?php foreach($row5 as $key=>$value){?>
						<?if($i>0){echo '<hr />';}?>
						<div class="education">
			                <div class="form-group">
			                	<div class="controls form-inline col-sm-12">
				                    <label for="degree" class="col-sm-2 control-label">學歷</label>
				                    <div class="col-sm-4">
				                        <select id="degree" name="degree[]" class="form-control">
											<option value="0" <? if($row5[$key]->degree=='0') echo 'selected="selected"'; ?>>請選擇學歷</option>
											<option value="1" <? if($row5[$key]->degree=='1') echo 'selected="selected"'; ?>>高中職</option>
											<option value="2" <? if($row5[$key]->degree=='2') echo 'selected="selected"'; ?>>大學</option>
											<option value="3" <? if($row5[$key]->degree=='3') echo 'selected="selected"'; ?>>碩士</option>
											<option value="4" <? if($row5[$key]->degree=='4') echo 'selected="selected"'; ?>>博士</option>
				                        </select>
				                    </div>
		               	 		</div>
			                </div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="school_name">學校名稱</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" id="school_name" name="school_name[]" placeholder="請填入學校名稱" value="<?php echo $row5[$key]->school_name;?>">
									</div>
									<label class="col-sm-2 control-label" for="department">科系名稱</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" id="department" name="department[]" placeholder="請填入科系名稱" value="<?php echo $row5[$key]->department;?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="education_start">開始</label>
									<div class="col-sm-4">
				                        <select id="education_start" name="education_start[<?php echo $i;?>]" class="form-control">
											<option value="0" <?php if($row5[$key]->education_start=='0'){echo 'selected';}?>>請選擇開始年度</option>
				                        	<?php for($j=2014;$j>=1950;$j--){?>
												<option value="<?php echo $j;?>" <?php if($row5[$key]->education_start==$j){echo 'selected';}?>>西元<?php echo $j;?> 年 / 民國<?php echo $j-1911;?> 年</option>
											<?php }?>
				                        </select>
									</div>
									<div class="col-sm-3">
				                        <select id="education_start_month" name="education_start_month[<?php echo $i;?>]" class="form-control">
											<option value="0" <?php if($row5[$key]->education_start_month==0){echo 'selected';}?>>請選擇開始月份</option>
				                        	<?php for($j=1;$j<=12;$j++){?>
												<option value="<?php echo $j;?>" <?php if($row5[$key]->education_start_month==$j){echo 'selected';}?>><?php echo $j;?> 月</option>
											<?php }?>
				                        </select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="education_end">結束</label>
									<div class="col-sm-4">
				                        <select id="education_end" name="education_end[<?php echo $i;?>]" class="form-control">
											<option value="0" <?php if($row5[$key]->education_end=='0'){echo 'selected';}?>>請選擇結束年度</option>
				                        	<?php for($j=2014;$j>=1950;$j--){?>
												<option value="<?php echo $j;?>" <?php if($row5[$key]->education_end==$j){echo 'selected';}?>>西元<?php echo $j;?> 年 / 民國<?php echo $j-1911;?> 年</option>
											<?php }?>
				                        </select>
									</div>
									<div class="col-sm-3">
				                        <select id="education_end_month" name="education_end_month[<?php echo $i;?>]" class="form-control">
											<option value="0" <?php if($row5[$key]->education_start_month==0){echo 'selected';}?>>請選擇結束月份</option>
				                        	<?php for($j=1;$j<=12;$j++){?>
												<option value="<?php echo $j;?>" <?php if($row5[$key]->education_end_month==$j){echo 'selected';}?>><?php echo $j;?> 月</option>
											<?php }?>
				                        </select>
									</div>
									<div class="col-sm-3">
										<div class="radio">
										  <label>
										    <input type="radio" name="education_untilNow[<?php echo $i;?>]" value="1" <?php if($row5[$key]->until_now=='1'){echo 'checked';}?>>
											畢業
										  </label>
										</div>
										<div class="radio">
										  <label>
										    <input type="radio" name="education_untilNow[<?php echo $i;?>]" value="2" <?php if($row5[$key]->until_now=='2'){echo 'checked';}?>>
											肄業
										  </label>
										</div>
										<div class="pull-right">
											<a class="remove_education" href="javascript:void(0);"></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php $i++;?>
						<?php }?>
					<?php }?>
					<hr>
					<a id="add_education" href="javascript:void(0);">新增學歷</a>
				</div>
				<h3>年資</h3>
				<div class="well">
					<?php if(empty($row13)){?>
					<div class="seniority">
						<div class="form-group">
							<div class="controls form-inline col-sm-12">
								<label class="col-sm-2 control-label" for="seniority_category">職務類別</label>
								<div class="col-sm-4">
			                        <select id="seniority_category" name="seniority_category[]" class="form-control">
										<option value="0" selected>請選擇職務類別</option>
										<option value="2">APP設計</option>
										<option value="3">Web開發</option>
										<option value="4">巨量資料分析</option>
			                        </select>
								</div>
								<div class="col-sm-3">
			                        <select id="year_of_experience" name="year_of_experience[]" class="form-control">
										<option value="-1" selected>無經驗</option>
										<option value="0">未滿1年</option>
										<option value="1">1年以上</option>
										<option value="2">2年以上</option>
										<option value="3">3年以上</option>
										<option value="4">4年以上</option>
										<option value="5">5年以上</option>
										<option value="6">6年以上</option>
										<option value="7">7年以上</option>
										<option value="8">8年以上</option>
										<option value="9">9年以上</option>
										<option value="10">10年以上</option>
			                        </select>
								</div>
							</div>
							<div class="pull-right">
								<a class="remove_seniority" href="javascript:void(0);"></a>
							</div>
						</div>
					</div>
					<?php }else{?>
						<?php foreach($row13 as $key=>$value){?>
							<div class="seniority">
								<div class="form-group">
									<div class="controls form-inline col-sm-12">
										<label class="col-sm-2 control-label" for="seniority_category">職務類別</label>
										<div class="col-sm-4">
					                        <select id="seniority_category" name="seniority_category[]" class="form-control">
												<option value="0" <? if($row13[$key]->jobs_category_id=='0') echo 'selected="selected"'; ?>>請選擇職務類別</option>
												<option value="2" <? if($row13[$key]->jobs_category_id=='2') echo 'selected="selected"'; ?>>APP設計</option>
												<option value="3" <? if($row13[$key]->jobs_category_id=='3') echo 'selected="selected"'; ?>>Web開發</option>
												<option value="4" <? if($row13[$key]->jobs_category_id=='4') echo 'selected="selected"'; ?>>巨量資料分析</option>
					                        </select>
										</div>
										<div class="col-sm-3">
					                        <select id="year_of_experience" name="year_of_experience[]" class="form-control">
												<option value="-1" <? if($row13[$key]->year=='-1') echo 'selected="selected"'; ?>>無經驗</option>
												<option value="0" <? if($row13[$key]->year=='0') echo 'selected="selected"'; ?>>未滿1年</option>
												<option value="1" <? if($row13[$key]->year=='1') echo 'selected="selected"'; ?>>1年以上</option>
												<option value="2" <? if($row13[$key]->year=='2') echo 'selected="selected"'; ?>>2年以上</option>
												<option value="3" <? if($row13[$key]->year=='3') echo 'selected="selected"'; ?>>3年以上</option>
												<option value="4" <? if($row13[$key]->year=='4') echo 'selected="selected"'; ?>>4年以上</option>
												<option value="5" <? if($row13[$key]->year=='5') echo 'selected="selected"'; ?>>5年以上</option>
												<option value="6" <? if($row13[$key]->year=='6') echo 'selected="selected"'; ?>>6年以上</option>
												<option value="7" <? if($row13[$key]->year=='7') echo 'selected="selected"'; ?>>7年以上</option>
												<option value="8" <? if($row13[$key]->year=='8') echo 'selected="selected"'; ?>>8年以上</option>
												<option value="9" <? if($row13[$key]->year=='9') echo 'selected="selected"'; ?>>9年以上</option>
												<option value="10" <? if($row13[$key]->year=='10') echo 'selected="selected"'; ?>>10年以上</option>
					                        </select>
										</div>
									</div>
									<div class="pull-right">
										<a class="remove_seniority" href="javascript:void(0);"></a>
									</div>
								</div>
							</div>					
						<?php }?>
					<?php }?>
					<a id="add_seniority" href="javascript:void(0);">新增年資</a>
				</div>
				<h3>經歷</h3>
				<span style="color: #999999;">經歷可不填寫，但若填寫，需填入單位名稱、職務名稱、開始年度及開始月份才可正確儲存</span>
				<div class="well">
					<?php if(empty($row)){?>
						<div class="work_experience">
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="company_name">單位名稱</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" id="company_name" name="company_name[]" placeholder="請填入單位名稱" value="">
									</div>
									<label class="col-sm-2 control-label" for="job_title">職務名稱</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" id="job_title" name="job_title[]" placeholder="請填入職務名稱" value="">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="experience_start">開始年度</label>
									<div class="col-sm-4">
				                        <select id="experience_start" name="experience_start[]" class="form-control">
											<option value="0" selected>請選擇開始年度</option>
				                        	<?php for($j=2014;$j>=1950;$j--){?>
												<option value="<?php echo $j;?>">西元<?php echo $j;?> 年 / 民國<?php echo $j-1911;?> 年</option>
											<?php }?>
				                        </select>
									</div>
									<div class="col-sm-3">
				                        <select id="experience_start_month" name="experience_start_month[]" class="form-control">
											<option value="0" selected>請選擇開始月份</option>
				                        	<?php for($j=1;$j<=12;$j++){?>
												<option value="<?php echo $j;?>"><?php echo $j;?>月</option>
											<?php }?>
				                        </select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="experience_end">結束年度</label>
									<div class="col-sm-4">
				                        <select id="experience_end" name="experience_end[]" class="form-control">
											<option value="0" selected>請選擇結束年度</option>
				                        	<?php for($j=2014;$j>=1950;$j--){?>
												<option value="<?php echo $j;?>">西元<?php echo $j;?> 年 / 民國<?php echo $j-1911;?> 年</option>
											<?php }?>
				                        </select>
									</div>
									<div class="col-sm-3">
				                        <select id="experience_end_month" name="experience_end_month[]" class="form-control">
											<option value="0" selected>請選擇結束月份</option>
				                        	<?php for($j=1;$j<=12;$j++){?>
												<option value="<?php echo $j;?>"><?php echo $j;?>月</option>
											<?php }?>
				                        </select>
									</div>
								</div>
							</div>
							<div class="pull-right">
								<a class="remove_experience" href="javascript:void(0);"></a>
							</div>
						</div>
					<?php }else{?>
						<?php $i=0;?>
						<?php foreach($row as $key=>$value){?>
						<?php if($i>0){echo '<hr />';}?>
						<div class="work_experience">
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="company_name">單位名稱</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" id="company_name" name="company_name[]" placeholder="請填入單位名稱" value="<?php echo $row[$key]->company_name;?>">
									</div>
									<label class="col-sm-2 control-label" for="job_title">職務名稱</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" id="job_title" name="job_title[]" placeholder="請填入職務名稱" value="<?php echo $row[$key]->job_title;?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="experience_start">開始</label>
									<div class="col-sm-4">
				                        <select id="experience_start" name="experience_start[<?php echo $i;?>]" class="form-control">
											<option value="0" <?php if($row[$key]->experience_start=='0'){echo 'selected';}?>>請選擇開始年度</option>
				                        	<?php for($j=2014;$j>=1950;$j--){?>
												<option value="<?php echo $j;?>" <?php if($row[$key]->experience_start==$j){echo 'selected';}?>>西元<?php echo $j;?> 年 / 民國<?php echo $j-1911;?> 年</option>
											<?php }?>
				                        </select>
									</div>
									<div class="col-sm-3">
				                        <select id="experience_start_month" name="experience_start_month[<?php echo $i;?>]" class="form-control">
											<option value="0" <?php if($row[$key]->experience_start_month=='0'){echo 'selected';}?>>請選擇開始月份</option>
				                        	<?php for($j=1;$j<=12;$j++){?>
												<option value="<?php echo $j;?>" <?php if($row[$key]->experience_start_month==$j){echo 'selected';}?>><?php echo $j;?> 月</option>
											<?php }?>
				                        </select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="controls form-inline col-sm-12">
									<label class="col-sm-2 control-label" for="experience_end">結束</label>
									<div class="col-sm-4">
				                        <select id="experience_end" name="experience_end[<?php echo $i;?>]" class="form-control">
											<option value="0" <?php if($row[$key]->experience_end=='0'){echo 'selected';}?>>請選擇結束年度</option>
				                        	<?php for($j=2014;$j>=1950;$j--){?>
												<option value="<?php echo $j;?>" <?php if($row[$key]->experience_end==$j){echo 'selected';}?>>西元<?php echo $j;?> 年 / 民國<?php echo $j-1911;?> 年</option>
											<?php }?>
				                        </select>
									</div>
									<div class="col-sm-3">
				                        <select id="experience_end_month" name="experience_end_month[<?php echo $i;?>]" class="form-control">
											<option value="0" <?php if($row[$key]->experience_end_month=='0'){echo 'selected';}?>>請選擇結束月份</option>
				                        	<?php for($j=1;$j<=12;$j++){?>
												<option value="<?php echo $j;?>" <?php if($row[$key]->experience_end_month==$j){echo 'selected';}?>><?php echo $j;?> 月</option>
											<?php }?>
				                        </select>
									</div>
								</div>
								<div class="pull-right">
									<a class="remove_experience" href="javascript:void(0);"></a>
								</div>
							</div>
						</div>
						<?php $i++;?>
						<?php }?>
					<?php }?>
					<hr>
					<a id="add_experience" href="javascript:void(0);">新增經歷</a>
				</div>
				<div class="form-group">
					<div class="controls form-inline col-sm-12">
						<div class="prev_div col-sm-offset-4 col-sm-1">
							<button type="button" class="btn btn-primary"><div class="fa fa-long-arrow-left"></div> 上一步</button>
						</div>
						<div class="next_div col-sm-offset-2 col-sm-1">
							<button type="button" class="btn btn-primary">下一步 <div class="fa fa-long-arrow-right"></div></button>
						</div>
					</div>
				</div>
			</div>
			<div class="item-container condition_div hide">
				<h3>求職方向</h3>
				<span style="color: #999999;">請選擇您有興趣的職務類別</span>
				<div class="well">
	                <div class="form-group">
	                    <label for="job_category_id" class="col-sm-2 control-label">職務類別</label>
	                    <div class="col-sm-6">
	                        <?php foreach($row12 as $key=>$value){?>
								<label class="checkbox-inline">
									<input type="checkbox" name="jobs_category_id[]" value="<?php echo $value->jobs_category_id;?>" <?php echo(in_array($value->jobs_category_id,explode(",",$getOneUser['match_jobs_category'])))?'checked':''?>><?php echo $value->jobs_category_name;?>
								</label>
							<?php }?>
								<label class="checkbox-inline">
									<input type="checkbox" name="jobs_category_id[]" value="5">專案管理
								</label>
	                    </div>
	                </div>
				</div>
			</div>
			<div class="item-container career_skill_div hide">
				<h3>專業技能</h3>
				<div class="well">
				<?php if(empty($row9)){?>
					<div class="career_skill form-group">
						<label for="career_skill_name" class="col-sm-2 control-label">專業技能</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" id="career_skill_name" name="career_skill_name[]" placeholder="請填入專業技能，如網路管理、伺服器維護等" value="">
						</div>
						<a class="remove_career_skill" href="javascript:void(0);"></a>
					</div>
				<?php }else{?>
					<?php $i=0;?>
					<?php foreach($row9 as $key=>$value){?>
						<div class="career_skill form-group">
						<?php //if($i>0){echo '<hr />';}?>
							<label for="career_skill_name" class="col-sm-2 control-label">專業技能</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="career_skill_name" name="career_skill_name[]" placeholder="請填入專業技能，如網路管理、伺服器維護等" value="<?php echo $row9[$key]->career_skill_name;?>">
							</div>
							<a class="remove_career_skill" href="javascript:void(0);"></a>
						</div>
						<?php $i++;?>
					<?php }?>
					
					<!-- <div style="line-height:25px;">
						<div class="col-sm-12">
						<?php //foreach($row9 as $key=>$value){?>
							<span class="label label-default"><?php //echo $row9[$key]->career_skill_name;?>　<a class="remove_skill glyphicon glyphicon-remove" href="javascript:void(0);"></a></span>
						<?php //}?>
						</div>
					</div> -->
				<?php }?>
				<a id="add_career_skill" href="javascript:void(0);">新增專業技能</a>
				</div>
				<div class="form-group">
					<div class="controls form-inline col-sm-12">
						<div class="prev_div col-sm-offset-4 col-sm-1">
							<button type="button" class="btn btn-primary"><div class="fa fa-long-arrow-left"></div> 上一步</button>
						</div>
						<div class="next_div col-sm-offset-2 col-sm-1">
							<button type="button" class="btn btn-primary">下一步 <div class="fa fa-long-arrow-right"></div></button>
						</div>
					</div>
				</div>
			</div>
			<div class="item-container skill_div hide">
				<h3>擅長工具</h3>
				<div class="well">
				<?php if(empty($row6)){?>
					<div class="skill form-group">
						<label for="skill_name" class="col-sm-2 control-label">擅長工具</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" id="skill_name" name="skill_name[]" placeholder="請填入工作技能，如Java、JavaScript等" value="">
						</div>
						<a class="remove_skill" href="javascript:void(0);"></a>
					</div>
				<?php }else{?>
					<?php $i=0;?>
					<?php foreach($row6 as $key=>$value){?>
						<div class="skill form-group">
						<?php //if($i>0){echo '<hr />';}?>
							<label for="skill_name" class="col-sm-2 control-label">擅長工具</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="skill_name" name="skill_name[]" placeholder="請填入工作技能，如Java、JavaScript等" value="<?php echo $row6[$key]->skill_name;?>">
							</div>
							<a class="remove_skill" href="javascript:void(0);"></a>
						</div>
						<?php $i++;?>
					<?php }?>
					
					<!-- <div style="line-height:25px;">
						<div class="col-sm-12">
						<?php //foreach($row6 as $key=>$value){?>
							<span class="label label-default"><?php //echo $row6[$key]->skill_name;?>　<a class="remove_skill glyphicon glyphicon-remove" href="javascript:void(0);"></a></span>
						<?php //}?>
						</div>
					</div> -->
				<?php }?>
				<a id="add_skill" href="javascript:void(0);">新增擅長工具</a>
				</div>
				<div class="form-group">
					<div class="controls form-inline col-sm-12">
						<div class="prev_div col-sm-offset-4 col-sm-1">
							<button type="button" class="btn btn-primary"><div class="fa fa-long-arrow-left"></div> 上一步</button>
						</div>
						<div class="next_div col-sm-offset-2 col-sm-1">
							<button type="button" class="btn btn-primary">下一步 <div class="fa fa-long-arrow-right"></div></button>
						</div>
					</div>
				</div>
			</div>
			<div class="item-container award_div hide">
 				<h3>獲獎記錄</h3>
				<div class="well">
					<?php if(empty($row2)){?>
						<div class="form-group form-inline award">
							<label for="awards" class="col-sm-2 control-label">獲獎記錄</label>
							<div class="col-sm-6">
								<input class="form-control" type="text" id="awards" name="awards[]" placeholder="請填入獲獎記錄" value="">
							</div>
							<a class="remove_award" href="javascript:void(0);"></a>
						</div>
					<?php }else{?>
						<?php foreach($row2 as $key=>$value){?>
							<div class="form-group form-inline award">
								<label for="awards" class="col-sm-2 control-label">獲獎記錄</label>
								<div class="col-sm-6">
									<input class="form-control" type="text" id="awards" name="awards[]" placeholder="請填入獲獎記錄" value="<?php if(isset($row2[$key]->award_name))echo $row2[$key]->award_name;?>">
								</div>
								<a class="remove_award" href="javascript:void(0);"></a>
							</div>
						<?php }?>
					<?php }?>
					<a id="add_award" href="javascript:void(0);">新增獲獎記錄</a>
				</div>
				<div class="form-group">
					<div class="controls form-inline col-sm-12">
						<div class="prev_div col-sm-offset-4 col-sm-1">
							<button type="button" class="btn btn-primary"><div class="fa fa-long-arrow-left"></div> 上一步</button>
						</div>
						<div class="next_div col-sm-offset-2 col-sm-1">
							<button type="button" class="btn btn-primary">下一步 <div class="fa fa-long-arrow-right"></div></button>
						</div>
					</div>
				</div>
			</div>
			<div class="item-container license_div hide">
				<h3>專業證照</h3>
				<div class="well">
					<?php if(empty($row3)){?>
						<div class="form-group license">
							<label for="licenses" class="col-sm-2 control-label">專業證照</label>
							<div class="col-sm-6">
								<input class="form-control" type="text" id="licenses" name="licenses[]" placeholder="請填入專業證照" value="">
							</div>
							<a class="remove_license" href="javascript:void(0);"></a>
						</div>
					<?php }else{?>
						<?php foreach($row3 as $key=>$value){?>
							<div class="form-group license">
								<label for="licenses" class="col-sm-2 control-label">專業證照</label>
								<div class="col-sm-6">
									<input class="form-control" type="text" id="licenses" name="licenses[]" placeholder="請填入專業證照" value="<?php if(isset($row3[$key]->license_name))echo $row3[$key]->license_name;?>">
								</div>
								<a class="remove_license" href="javascript:void(0);"></a>
							</div>
						<?php }?>
					<?php }?>
					<a id="add_license" href="javascript:void(0);">新增專業證照</a>
				</div>
				<div class="form-group">
					<div class="controls form-inline col-sm-12">
						<div class="prev_div col-sm-offset-4 col-sm-1">
							<button type="button" class="btn btn-primary"><div class="fa fa-long-arrow-left"></div> 上一步</button>
						</div>
						<div class="next_div col-sm-offset-2 col-sm-1">
							<button type="button" class="btn btn-primary">下一步 <div class="fa fa-long-arrow-right"></div></button>
						</div>
					</div>
				</div>
			</div>
			<div class="item-container autobiography_div hide">
				<h3>自傳</h3>
				<div class="well">
					<textarea class="form-control" name="autobiography" rows="20"><?php echo preg_replace( '!<br.*>!iU', "\n",$getOneUser['autobiography']);?></textarea>
				</div>
				<div class="form-group">
					<div class="controls form-inline col-sm-12">
						<div class="prev_div col-sm-offset-4 col-sm-1">
							<button type="button" class="btn btn-primary"><div class="fa fa-long-arrow-left"></div> 上一步</button>
						</div>
						<div class="next_div col-sm-offset-2 col-sm-1">
							<button type="button" class="btn btn-primary">下一步 <div class="fa fa-long-arrow-right"></div></button>
						</div>
					</div>
				</div>
			</div>
			<div class="item-container portfolio_div hide">
				<h3>作品與成果</h3>
				<span style="color: #999999;">作品與成果可不填寫，但若填寫，需填入作品名稱、作品類型才可正確儲存，若無上傳圖片，則系統會上傳預設圖片</span>
				<div class="thumbnail">
					<?php if(empty($row4)){?>
						<div class="portfolio well">
							<div class="form-group">
								<div class="controls form-inline">
									<label for="portfolio_name" class="col-sm-2 control-label">作品名稱</label>
									<div class="col-sm-4">
										<input class="form-control" type="text" id="portfolio_name" name="portfolio_name[]" placeholder="請填入作品名稱" value="">
									</div>
									<label for="portfolio_pic" class="col-sm-2 control-label">作品圖檔</label>
									<div class="col-sm-4">
										<input type="file" id="portfolio_pic" name="portfolio_pic[]">
									</div>
								</div>
							</div>
			                <div class="form-group">
			                    <label for="portfolio_type" class="col-sm-2 control-label">作品類型</label>
			                    <div class="col-sm-6">
			                        <select id="portfolio_type" name="portfolio_type[]" class="form-control">
										<option value="0" selected>請選擇作品類型</option>
										<option value="1">圖片</option>
										<option value="2">Youtube影片</option>
										<option value="3">SlideShare</option>
										<option value="4">外連網址</option>
			                        </select>
			                    </div>
			                </div>
					                <div class="form-group">
					                    <label for="portfolio_skill" class="col-sm-2 control-label">使用技術</label>
					                     <div>
						                     <div class="col-sm-6" style="cursor: pointer;">
							                    <div class="use_skill_div thumbnail" style="line-height:25px;min-height:34px;min-width:100%;"> 
							                    </div>
						                     </div>
					                     </div>
					                    <input name="portfolio_skill[]" type="hidden" value=""/>
					                </div>
							<div class="form-group">
								<label for="hyperlink" class="col-sm-2 control-label">超連結</label>
								<div class="col-sm-6">
									<input class="form-control" type="text" id="hyperlink" name="hyperlink[]" placeholder="請填入超連結" value="">
								</div>
							</div>
							<div class="form-group">
								<label for="portfolio_desc" class="col-sm-2 control-label">作品敘述</label>
								<div class="col-sm-6">
									<textarea class="col-sm-10 form-control" id="portfolio_desc" name="portfolio_desc[]" rows="5" placeholder="請填入作品敘述"></textarea>
								</div>
							</div>
							<div class="pull-right">
								<a class="remove_portfolio" href="javascript:void(0);"></a>
							</div>
						</div>
					<?php }else{?>
						<?php $i=0;?>
						<?php foreach($row4 as $key=>$value){?>
							<div class="portfolio well">
								<div class="form-group">
									<div class="controls form-inline">
										<label for="portfolio_name" class="col-sm-2 control-label">作品名稱</label>
										<div class="col-sm-4">
											<input class="form-control" type="text" id="portfolio_name" name="portfolio_name[]" placeholder="請填入作品名稱" value="<?php if(isset($row4[$key]->portfolio_name))echo $row4[$key]->portfolio_name;?>">
										</div>
										<label for="portfolio_pic" class="col-sm-2 control-label">作品圖檔</label>
										<div class="col-sm-4">
											<img src="<?php echo base_url()?>assets/img/user/portfolio/<?php echo $this->session->userdata('alias');?>/128x128/<?php if(isset($row4[$key]->portfolio_pic))echo $row4[$key]->portfolio_pic;?>">
											<input type="file" id="portfolio_pic" name="portfolio_pic[]" value="">
										</div>
									</div>
								</div>
				                <div class="form-group">
				                    <label for="portfolio_type" class="col-sm-2 control-label">作品類型</label>
				                    <div class="col-sm-6">
				                        <select id="portfolio_type" name="portfolio_type[]" class="form-control">
											<option value="0" <? if($row4[$key]->portfolio_type=='0') echo 'selected="selected"'; ?>>請選擇作品類型</option>
											<option value="1" <? if($row4[$key]->portfolio_type=='1') echo 'selected="selected"'; ?>>圖片</option>
											<option value="2" <? if($row4[$key]->portfolio_type=='2') echo 'selected="selected"'; ?>>Youtube影片</option>
											<option value="3" <? if($row4[$key]->portfolio_type=='3') echo 'selected="selected"'; ?>>SlideShare</option>
											<option value="4" <? if($row4[$key]->portfolio_type=='4') echo 'selected="selected"'; ?>>外連網址</option>
				                        </select>
				                    </div>
				                </div>
					                <div class="form-group">
					                    <label for="portfolio_skill" class="col-sm-2 control-label">使用技術</label>
					                     <div>
						                     <div class="col-sm-6" style="cursor: pointer;">
							                    <div class="use_skill_div thumbnail" style="line-height:25px;min-height:34px;min-width:100%;">
													<?php foreach($row8 as $key1=>$value1){?>
														<?php if($row4[$key]->user_portfolio_id==$row8[$key1]->user_portfolio_id){?>
															<?php foreach($row6 as $key2=>$value2){?>
																<?php if($row6[$key2]->user_skill_id==$row8[$key1]->user_skill_id){?>
																	<span class="label label-default"><?php echo $row6[$key2]->skill_name;?></span> <a class="fa fa-times" href="javascript:void(0);"></a>
																<?php }?>
															<?php }?>
														<?php }?>
													<?php }?>
							                    </div>
						                     </div>
					                     </div>
					                    <input name="portfolio_skill[]" type="hidden" value="<?php 
					                    	foreach($row8 as $key1=>$value1){
					                    		if($row4[$key]->user_portfolio_id==$row8[$key1]->user_portfolio_id){
					                    			if(!isset($row8[$key1+1]->user_portfolio_id)){
					                    				echo $row8[$key1]->user_skill_id;
					                    			}elseif($row8[$key1+1]->user_portfolio_id!=$row8[$key1]->user_portfolio_id){
														echo $row8[$key1]->user_skill_id;
					                    			}elseif($row8[$key1+1]->user_portfolio_id==$row8[$key1]->user_portfolio_id){
														echo $row8[$key1]->user_skill_id.',';
													}
					                    		}
					                    	}
					                    ?>"/>
					                </div>
								<div class="form-group">
									<label for="hyperlink" class="col-sm-2 control-label">超連結</label>
									<div class="col-sm-6">
										<input class="form-control" type="text" id="hyperlink" name="hyperlink[]" placeholder="請填入超連結" value="<?php if(isset($row4[$key]->hyperlink))echo $row4[$key]->hyperlink;?>">
									</div>
								</div>
								<div class="form-group">
									<label for="portfolio_desc" class="col-sm-2 control-label">作品敘述</label>
									<div class="col-sm-6">
									<textarea class="col-sm-10 form-control" id="portfolio_desc" name="portfolio_desc[]" rows="5" placeholder="請填入作品敘述"><?php if(isset($row4[$key]->portfolio_desc))echo preg_replace( '!<br.*>!iU', "\n",$row4[$key]->portfolio_desc);?></textarea>
									</div>
								</div>
								<div class="pull-right">
									<a class="remove_portfolio" href="javascript:void(0);"></a>
								</div>
							</div>
							<?php $i++;?>
						<?php }?>
					<?php }?>
				</div>
				<a id="add_portfolio" href="javascript:void(0);">新增作品</a>
				<div class="form-group">
					<div class="controls form-inline col-sm-12">
						<div class="prev_div col-sm-offset-4 col-sm-1">
							<button type="button" class="btn btn-primary"><div class="fa fa-long-arrow-left"></div> 上一步</button>
						</div>
						<div class="next_div col-sm-offset-2 col-sm-1">
							<button type="button" class="btn btn-primary">下一步 <div class="fa fa-long-arrow-right"></div></button>
						</div>
					</div>
				</div>
			</div>
			<div class="item-container society_div hide">
				<h3>社群</h3>
				<div class="well">
					<div class="form-group society">
						<label for="facebook" class="col-sm-2 control-label">Facebook</label>
						<div class="col-sm-10">
							<input class="form-control" type="text" id="facebook" name="facebook" placeholder="請填入Facebook連結" value="<?php if(!empty($getUserSociety['facebook'])){echo $getUserSociety['facebook'];}?>">
						</div>
					</div>
					<div class="form-group society">
						<label for="google_plus" class="col-sm-2 control-label">Google+</label>
						<div class="col-sm-10">
							<input class="form-control" type="text" id="google_plus" name="google_plus" placeholder="請填入Google+連結" value="<?php if(!empty($getUserSociety['google_plus'])){ echo $getUserSociety['google_plus'];}?>">
						</div>
					</div>
					<div class="form-group society">
						<label for="twitter" class="col-sm-2 control-label">Twitter</label>
						<div class="col-sm-10">
							<input class="form-control" type="text" id="twitter" name="twitter" placeholder="請填入Twitter連結" value="<?php if(!empty($getUserSociety['twitter'])){ echo $getUserSociety['twitter'];}?>">
						</div>
					</div>
					<div class="form-group society">
						<label for="linkedin" class="col-sm-2 control-label">LinkedIn</label>
						<div class="col-sm-10">
							<input class="form-control" type="text" id="linkedin" name="linkedin" placeholder="請填入LinkedIn連結" value="<?php if(!empty($getUserSociety['linkedin'])){ echo $getUserSociety['linkedin'];}?>">
						</div>
					</div>
					<div class="form-group society">
						<label for="github" class="col-sm-2 control-label">GitHub</label>
						<div class="col-sm-10">
							<input class="form-control" type="text" id="github" name="github" placeholder="請填入GitHub連結" value="<?php if(!empty($getUserSociety['github'])){ echo $getUserSociety['github'];}?>">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="controls form-inline col-sm-12">
						<div class="prev_div col-sm-offset-4 col-sm-1">
							<button type="button" class="btn btn-primary"><div class="fa fa-long-arrow-left"></div> 上一步</button>
						</div>
						<div class="next_div col-sm-offset-2 col-sm-1">
							<button type="button" class="btn btn-primary">下一步 <div class="fa fa-long-arrow-right"></div></button>
						</div>
					</div>
				</div>
			</div>
			<!-- Learning Analysis by JoeyC -->
			<div class="item-container joeyc_div hide">
				<h3>能力分析</h3>
				<?php
                    $c_score = 0;
                    $avg = 0;
                    foreach($row14 as $val){
                        echo "<p>";
                        echo "<table border='1'>";
                        echo "<tr><th>Name</th><th>Score</th><th>AVG Score</th></tr>";
                        echo "<tr>";
                        echo "<th>".$val->library_name."</th>";
                        echo "<th>".$val->avg."</th>";
                        echo "<th></th></tr>";
                        $sql = "select library_id, course_id, course_name from course where library_id = '$val->library_id'";
                        $con=mysqli_connect("localhost","root","root","aioc");
                        $result = mysqli_query($con,$sql);
                        while($c_score<count($row15) && $row15[$c_score]->course_id == null){
                            $c_score++;
                        }
                        while($avg<count($row17) && $row17[$avg]->course_id == null){
                            $avg++;
                        }
                        while($row16 = mysqli_fetch_object($result)){
                            echo "<tr>";
                            echo "<td>".$row16->course_name."</td>";
                            if($c_score<count($row15) && $row16->course_id == $row15[$c_score]->course_id){
                                echo "<td>".$row15[$c_score]->avg."</td>";
                                $c_score++;
                            }else{
                                echo "<td>&nbsp;</td>";
                            }
                            
                            if($avg<count($row17) && $row16->course_id == $row17[$avg]->course_id){
                                echo "<td>".$row17[$avg]->avg."</td>";
                                $avg++;
                            }else{
                                echo "<td>&nbsp;</td>";
                            }
                            echo "</tr>";
                        }
                        echo "</table>";
                        echo "</p>";
                    }
                    echo "所有關卡資料";
                    echo "<table border='1'>";
                    echo "<tr><th>知識庫ID</th><th>課程ID</th><th>關卡ID</th><th>關卡得分</th></tr>";
                    foreach($row18 as $val){
                        echo "<tr><td>".$val->library_id."</td><td>".$val->course_id."</td><td>".$val->chapter_id."</td><td>".$val->points."</td></tr>";
                    }
                    echo "</table>";
                ?>
			</div>
			<!-- by JoeyC End-->
			<div class="item-container performance_div hide">
				<h3>學習表現</h3>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="performance_open_flag" value="1" <?php if($getOneUser['performance_open_flag']==1){echo 'checked';}?>>是否開放學習表現供其他人觀看
					</label>
				</div>
				<hr>
				<div class="well row">
					<div class="col-xs-12">
						<h3 style="line-height:1.4em;">總積分 <?echo $getTotalPoints;?></h3>
					</div>
					<div class="col-xs-12">
						<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8" style="margin-top:10px;">
							<?$chart=array();?>
							<?//$color=array("#ecb61f","#38b473","#5dc7d2","#e34c48","#9a86c6","#f8855c","#adb7c6","#53519f","#f36284","#893d52","#53bbb4","#9ea5b3","#666");?>
							<ul style="list-style: none">
								<?php if(!empty($row10)){?>
									<?php foreach($row10 as $key=>$val){?>
										<li class="col-xs-12 col-sm-6 col-md-4">
											<div style="height: 70px;">
												<span style="float: left;margin: 3px 0 0 -25px; color: #<?echo $val->library_color?>"><i class="fa fa-circle"></i></span>
												<h3><?php echo $val->sum?></h3>
												<p><?php echo $val->library_name?></p>
											</div>
										</li>
										<?php array_push($chart,'{value:'.$val->sum.',color:\'#'.$val->library_color.'\'}')?>
									<?}?>
								<?}?>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" style="margin-top:10px;">
							<?php $chart = str_replace ('"'," ",json_encode( $chart ));?>
							<script> var doughnutData = <?echo $chart;?>;</script>
							<center>
								<canvas id="canvas" height="200"></canvas>
							</center>
						</div>
					</div>
				</div>
				<div style="margin-bottom:50px;" class="row">
					<div>
					  <?if($row11){?>
					  	<h3>我的成就</h3>
					  <?}?>
					</div>
					<div class="row">
						<?foreach($row11 as $key=>$val){?>
							<? $image=urlencode(base_url()."assets/img/badges/".$val->chapter_pic);?>
							<? $url=urlencode(base_url().'library/'.$val->library_url.'/'.$val->course_url.'/'.$val->chapter_url);?>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-top:10px;">
								<div class="thumbnail">
									<div class="caption" style="height: 140px;padding:10px;">
										<div style="float: left;width: 70%;">
											<h4><?php echo $val->chapter_name?></h4>
											<p class="text-muted"><?php echo $val->accomplish_time?> 完成</p>
		    	        					<a class="btn btn-default" onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $title;?>&amp;p[description]=<?php echo $summary;?>&amp;p[summary]=<?php echo $summary;?>&amp;p[url]=<?php echo $url; ?>&amp;&p[images][0]=<?php echo $image;?>', 'sharer', 'toolbar=0,status=0,width=550,height=400');" target="_parent" href="javascript: void(0)">
		                    				<span>分享</span>
		            						</a>
										</div>
										<div style="float: left;width: 30%;">
											<img src="<?=base_url(); ?>assets/img/badges/<?php echo $val->chapter_pic?>">
										</div>
										<div style="clear:both;"></div>
									</div>
								</div>
							</div>
						<?}?>
					 </div>
				</div><!--/row-->
				<div class="form-group">
					<div class="controls form-inline col-sm-12">
						<div class="prev_div col-sm-offset-4 col-sm-1">
							<button type="button" class="btn btn-primary"><div class="fa fa-long-arrow-left"></div> 上一步</button>
						</div>
						<div class="next_div col-sm-offset-2 col-sm-1">
							<button type="submit" class="btn btn-primary">儲存資料</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<div class="modal fade" id="select-skill-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<div>請選擇作品使用技術</div>
						<span style="color: #999999;">您可在擅長工具的頁籤中編輯，儲存後即可在此列表中選取</span>
					</div>
					<div class="modal-body">
						<?php foreach($row6 as $key=>$value){?>
							<button class="btn btn-default" value="<?php echo $row6[$key]->user_skill_id;?>" data-dismiss="modal"><?php echo $row6[$key]->skill_name;?></button>
						<?php }?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
		        	</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	<!-- 右邊end -->
</div>
</div>
</div><!-- /container -->