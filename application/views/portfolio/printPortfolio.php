<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="keywords" content="線上學習,mooc,moox,ai,learning,資策會" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="我們相信透過學習可以改變一個人的價值，AIOC希望在您職涯成長的路上貢獻一點力量，透過老師們用心錄製的課程及各式學習活動安排與指引，協助您在專業及技能上學習的更好，在職涯發展路上不斷提升自我價值">
    <meta name="author" content="LearningHouse Team">
    <meta name="copyright" CONTENT="本網頁著作權LearningHouse所有">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, minimal-ui" />
	<meta property="og:description" content="大家也趕快來學習吧，在這裡你可以學習到新的技能以及更多的知識喔!"/>
    <link rel="shortcut icon" href="<?=base_url();?>assets/ico/favicon.png">
	<title><?=$title;?>-<?=$this->config->item('site_name');?></title>
	<!-- flowplayer skin -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/flowplayer/minimalist.css"/>
	<!-- Video.js CSS-->
	<link href="<?=base_url();?>assets/css/video-js/video-js.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/css/video-js/video-speed.css" rel="stylesheet">
    <!-- jQuery UI -->
    <link href="<?=base_url();?>assets/css/jquery-ui-timepicker-addon/jquery-ui.css" rel="stylesheet">
    <!-- jQuery UI  TimePicker Addon-->
    <!-- <link href="<?=base_url();?>assets/css/jquery-ui-timepicker-addon/jquery-ui-timepicker-addon.min.css" rel="stylesheet"> -->
	<!-- Bootstrap core CSS -->
    <link href="<?=base_url();?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/bootstrap/css/bootstrap-switch.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/bootstrap/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?=base_url();?>assets/css/learning-house.css" rel="stylesheet">
	<link href="<?=base_url();?>assets/js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=base_url();?>assets/js/html5shiv.js"></script>
      <script src="<?=base_url();?>assets/js/respond.min.js"></script>
    <![endif]-->
  </head>
	<!-- Wrap all page content here -->
<?
$row= json_decode($getUserExperience);
$row2= json_decode($getUserAward);
$row3= json_decode($getUserLicense);
$row4= json_decode($getUserPortfolio);
$row6= json_decode($getUserSkill);
if(!empty($getUserEducation)){
	$row5= json_decode($getUserEducation);
}
// if(!empty($getUserTraining)){
// $row7= json_decode($getUserTraining);
$row8= json_decode($getPortfolioSkill);
$row9= json_decode($getCareerSkill);
// }
// $row10=json_decode($getPoints);
// $row11=json_decode($getUserChapterBadge);
$row12=json_decode($getJobsCategory);
if(!empty($getSeniority)){
	$row13=json_decode($getSeniority);
}
$title=urlencode("獲得新徽章：我在learninghouse完成一個關卡！");
$summary=urlencode("大家也趕快來學習吧，在這裡你可以學習到新的技能以及更多的知識喔!");
?>
<style>
/* Landscape phones and down */
@media (max-width: 480px) {
	#canvas{
		clear:both;
		position: relative;
		left: -50px;
	}
	.video-resume iframe{
		min-width:200px;
		min-height:150px;
		width:100%;
		height:75%;
	}
}
@media(min-width:768px){
	#canvas{
		float:right;
	}
}
textarea{
	resize: vertical;
}
</style>
<!-- 左邊區塊 -->
<!-- 此頁增加按鈕時，需在custom.js中editPortfolio sidebar區塊修改對應的程式碼 -->
<!-- 1.加入載入我的履歷後預設關閉的分頁 -->
<!-- 2.portfolio_map新增一個對應關係 -->
<!-- 3.重新整理頁面時，自動載入的分頁 -->
<div class="container">
	<div>
		<h2 style="font-size:1.71em;">AIOC</h2>
        <hr />
	</div>
<div class="row">
<div class="col-md-1"></div>
<!-- 左邊end -->
<div class="col-md-10">
	<!-- 右邊區塊 -->
	<h4><b><?php echo $getOneUser['name']?> </b><? if($getOneUser['work_status']=='1') echo '工作中(仍在職)'; ?> <? if($getOneUser['work_status']=='2') echo '無工作(待業中)'; ?></h4>
	<div class="well">
		<div class="row">
			<div class="col-md-10">
				<dl class="dl-horizontal">
  					<dt>所在地</dt><dd><?php echo $getOneUser['location']?></dd>
  					<dt>連絡信箱</dt><dd><?php echo $getOneUser['contact_email']?></dd>
  					<dt>手機</dt><dd><?php echo $getOneUser['mobile']?></dd>
  					<dt>關於我</dt><dd><?php echo preg_replace( '!<br.*>!iU', "\n",$getOneUser['intro']);?></dd>
				</dl>
			</div>
			<div class="col-md-2">
				<img src="<?php echo base_url()?>assets/img/user/128x128/<?php echo $getOneUser['resume_img']?>">
			</div>
		</div>
	</div>
	<h3>學歷</h3>
	<div class="well">
	<?php if(empty($row5)){?>
	<?php }else{?>
		<?php $i=0;?>
		<dl class="dl-horizontal">
		<?php foreach($row5 as $key=>$value){?>
			<?if($i>0){echo '<hr />';}?>
			<dt><?php echo $row5[$key]->school_name;?></dt><dd></dd>
			<dt>科系名稱</dt><dd><?php echo $row5[$key]->department;?></dd>
			<dt>學歷</dt><dd>
				<? if($row5[$key]->degree=='1') echo '高中職'; ?>
				<? if($row5[$key]->degree=='2') echo '大學'; ?>
				<? if($row5[$key]->degree=='3') echo '碩士'; ?>
				<? if($row5[$key]->degree=='4') echo '博士'; ?>
			</dd>
			<dt>就學期間</dt><dd><?echo $row5[$key]->education_start."/".$row5[$key]->education_start_month?>~<?echo $row5[$key]->education_end."/".$row5[$key]->education_end_month?>
				<?php if($row5[$key]->until_now=='1'){echo '畢業';}?>
				<?php if($row5[$key]->until_now=='2'){echo '肄業';}?>
			</dd>
			<?php $i++;?>
		<?php }?>
		</dl>
	<?php }?>
	</div>
	<h3>年資</h3>
	<div class="well">
		<?php if(empty($row13)){?>
		<?php }else{?>
			<?php foreach($row13 as $key=>$value){?>
				<dl class="dl-horizontal">
					<dt>職務類別</dt>
					<dd>
						<? if($row13[$key]->jobs_category_id=='0') echo '請選擇職務類別'; ?>
						<? if($row13[$key]->jobs_category_id=='2') echo 'APP設計'; ?>
						<? if($row13[$key]->jobs_category_id=='3') echo 'Web開發'; ?>
						<? if($row13[$key]->jobs_category_id=='4') echo '巨量資料分析'; ?>
						-
						<? if($row13[$key]->year=='-1') echo '無經驗'; ?>
						<? if($row13[$key]->year=='0') echo '未滿1年'; ?>
						<? if($row13[$key]->year=='1') echo '1年以上'; ?>
						<? if($row13[$key]->year=='2') echo '2年以上'; ?>
						<? if($row13[$key]->year=='3') echo '3年以上'; ?>
						<? if($row13[$key]->year=='4') echo '4年以上'; ?>
						<? if($row13[$key]->year=='5') echo '5年以上'; ?>
						<? if($row13[$key]->year=='6') echo '6年以上'; ?>
						<? if($row13[$key]->year=='7') echo '7年以上'; ?>
						<? if($row13[$key]->year=='8') echo '8年以上'; ?>
						<? if($row13[$key]->year=='9') echo '9年以上'; ?>
						<? if($row13[$key]->year=='10') echo '10年以上'; ?>

					</dd>
				</dl>
			<?php }?>
		<?php }?>
	</div>
	<h3>經歷</h3>
	<div class="well">
	<?php if(empty($row)){?>
	<?php }else{?>
		<?php $i=0;?>
		<dl class="dl-horizontal">
		<?php foreach($row as $key=>$value){?>
			<?if($i>0){echo '<hr />';}?>
			<dt>單位名稱</dt><dd><?php echo $row[$key]->company_name;?></dd>
			<dt>職務名稱</dt><dd><?php echo $row[$key]->job_title;?></dd>
			<dt>就業期間</dt><dd><?echo $row[$key]->experience_start."/".$row[$key]->experience_start_month?>~<?echo $row[$key]->experience_end."/".$row[$key]->experience_end_month?>
			</dd>
			<?php $i++;?>
		<?php }?>
		</dl>
	<?php }?>
	</div>
	<h3>求職方向</h3>
	<div class="well">
		<dl class="dl-horizontal">
			<dt>求職方向</dt><dd>
				<?php foreach($row12 as $key=>$value){?>
				<input type="checkbox" value="<?php echo $value->jobs_category_id;?>" disabled <?php echo(in_array($value->jobs_category_id,explode(",",$getOneUser['match_jobs_category'])))?'checked':''?>><?php echo $value->jobs_category_name;?>
				<?}?>
			</dd>
		</dl>
	</div>
	<h3>專業技能</h3>
	<div class="well">
		<?php if(empty($row9)){?>
		<?php }else{?>
			<?php $i=0;?>
			<dl class="dl-horizontal">
			<?php foreach($row9 as $key=>$value){?>
				<dt>專業技能</dt><dd><?php echo $row9[$key]->career_skill_name;?></dd>
			<?php $i++;?>
			<?php }?>
			</dl>
		<?php }?>
	</div>
	<h3>擅長工具</h3>
	<div class="well">
		<?php if(empty($row6)){?>
		<?php }else{?>
			<?php $i=0;?>
			<dl class="dl-horizontal">
			<?php foreach($row6 as $key=>$value){?>
				<dt>擅長工具</dt><dd><?php echo $row6[$key]->skill_name;?></dd>
			<?php $i++;?>
			<?php }?>
			</dl>
		<?php }?>
	</div>
	<h3>獲獎紀錄</h3>
	<div class="well">
		<?php if(empty($row2)){?>
		<?php }else{?>
			<dl class="dl-horizontal">
			<?php foreach($row2 as $key=>$value){?>
				<dt>獲獎紀錄</dt><dd><?php echo $row2[$key]->award_name;?></dd>
			<?php }?>
			</dl>
		<?php }?>
	</div>
	<h3>專業證照</h3>
	<div class="well">
		<?php if(empty($row3)){?>
		<?php }else{?>
			<dl class="dl-horizontal">
			<?php foreach($row3 as $key=>$value){?>
				<dt>專業證照</dt><dd><?php echo $row3[$key]->license_name;?></dd>
			<?php }?>
			</dl>
		<?php }?>
	</div>
	<h3>自傳</h3>
	<div class="well">
		<dl class="dl-horizontal">
			<dt>自傳</dt><dd><?php echo preg_replace( '!<br.*>!iU', "\n",$getOneUser['autobiography']);?></dd>
		</dl>
	</div>
	<h3>作品與成果</h3>
	<div class="well">
		<?php if(empty($row4)){?>
		<?php }else{?>
			<?php $i=0;?>
			<dl class="dl-horizontal">
			<?php foreach($row4 as $key=>$value){?>
				<dt>作品名稱</dt><dd><?echo $row4[$key]->portfolio_name?></dd>
				<dt>作品圖檔</dt><dd><img src="<?php echo base_url()?>assets/img/user/portfolio/<?php echo $this->session->userdata('alias');?>/128x128/<?php if(isset($row4[$key]->portfolio_pic))echo $row4[$key]->portfolio_pic;?>"></dd>
				<dt>作品類型</dt>
				<dd>
					<? if($row4[$key]->portfolio_type=='0') echo '請選擇作品類型'; ?>
					<? if($row4[$key]->portfolio_type=='1') echo '圖片'; ?>
					<? if($row4[$key]->portfolio_type=='2') echo 'Youtube影片'; ?>
					<? if($row4[$key]->portfolio_type=='3') echo 'SlideShare'; ?>
					<? if($row4[$key]->portfolio_type=='4') echo '外連網址'; ?>	
				</dd>
				<dt>使用技術</dt>
				<dd>
					<?php foreach($row8 as $key1=>$value1){?>
						<?php if($row4[$key]->user_portfolio_id==$row8[$key1]->user_portfolio_id){?>
							<?php foreach($row6 as $key2=>$value2){?>
								<?php if($row6[$key2]->user_skill_id==$row8[$key1]->user_skill_id){?>
									<span class="label label-default"><?php echo $row6[$key2]->skill_name;?></span>
								<?php }?>
							<?php }?>
						<?php }?>
					<?php }?>
				</dd>
				<dt>超連結</dt><dd><?echo $row4[$key]->hyperlink;?></dd>
				<dt>作品敘述</dt><dd><?echo preg_replace( '!<br.*>!iU', "\n",$row4[$key]->portfolio_desc);?></dd>
				<?php $i++;?>
			<?php }?>
			</dl>
		<?php }?>
	</div>
	<h3>社群</h3>
	<div class="well">
		<dl class="dl-horizontal">
			<dt>Facebook</dt><dd><?echo $getUserSociety['facebook']?></dd>
			<dt>Google+</dt><dd><?echo $getUserSociety['google_plus']?></dd>
			<dt>Twitter</dt><dd><?echo $getUserSociety['twitter']?></dd>
			<dt>LinkedIn</dt><dd><?echo $getUserSociety['linkedin']?></dd>
			<dt>GitHub</dt><dd><?echo $getUserSociety['github']?></dd>
		</dl>
	</div>
</div>
</div>
</div>
</html>
