<style>
.hero {position:relative; border-radius:5px 5px 0 0;}
.hero  {padding:10px;}
.hero-bottom {background:#eee; padding:20px 30px; border-radius:0 0 5px 5px; box-shadow:0 1px 0 0 #dedede;}
.circle {
    width: 100px;
    height: 100px;
    background: #C0C0C0;
    -moz-border-radius: 50px;
    -webkit-border-radius: 50px;
    border-radius: 50px;
    text-align:center;line-height:100px
}
</style>
<div class="container">
	<div style="padding-bottom:20px;">
		<h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">能力分析</h1>
	</div>
</div>
<div class="container">
    <ul id="myTab" class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#home" role="tab" data-toggle="tab">軟性能力</a></li>
        <li class=""><a href="#track" role="tab" data-toggle="tab">學程</a></li>
        <li class=""><a href="#class" role="tab" data-toggle="tab">個別課程</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">
        <div class="tab-pane active" id="home">
            <div class="row">
                <div class="col-sm-12">
                    <div class="hero-bottom">
                        <div class="row">
                            <div>
                                <script>
                                    var radarChartData = {labels: ["問題解決","表達互動","積極主動"],
                                    datasets: [{
                                        label: "dataset",
                                        fillColor: "rgba(151,187,205,0.3)",
                                        strokeColor: "rgba(151,187,205,1)",
                                        pointColor: "rgba(151,187,205,1)",
                                        pointStrokeColor: "#fff",
                                        pointHighlightFill: "#fff",
                                        pointHighlightStroke: "rgba(151,187,205,1)",
                                        data: [<?echo $radarData;?>]
                                    }]
                                    };
                                </script>
                                <canvas id="canvas" height="350"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="track">
            <div class="row">
            <?php $getUserBuyTracks=json_decode($getUserBuyTracks);?>
            <?php $getUserBuyTracksDetail=json_decode($getUserBuyTracksDetail);?>
            <?php $getAvgScore=json_decode($getAvgScore);?>
                <?foreach($getUserBuyTracks as $value){?>
                    <div class="col-sm-12">
                        <div class="hero" style="background-color:#aaa;">
                            <h4 style="color:#fff;"><?echo $value->tracks_name?></h4>
                        </div>
                        <div class="hero-bottom">
                            <div class="row">
                                <?foreach($getUserBuyTracksDetail as $value2){?>
                                    <?if($value->tracks_id == $value2->tracks_id){?>
                                        <div class="col-sm-4 col-md-4">
                                            <a class="" href="<?echo base_url()?>user/analysis/<?echo $value2->course_url;?>">
                                                <center>
                                                    <div class="circle">
                                                    <?foreach($getAvgScore as $score){?>
                                                        <?if($value2->course_id == $score->course_id){?>
                                                            <span style="font-size: 3em" class="<?=$score->class?>"><?=$score->avg?>%</span>
                                                        <?}?>
                                                    <?}?>
                                                    </div>
                                                    <h4><?echo $value2->course_name;?></h4>
                                                </center>
                                            </a>
                                        </div>
                                    <?}?>
                                <?}?>
                            </div>
                        </div>
                    </div>
                <?}?>
            </div>
        </div>
        <div class="tab-pane" id="class">
            <div class="row">
                <div class="col-sm-12">
                    <div class="hero" style="background-color:#aaa;">
                        <h4 style="color:#fff;">個別課程</h4>
                    </div>
                    <div class="hero-bottom">
                        <div class="row">
                        <?php $getUserBuyCourse=json_decode($getUserBuyCourse);?>
                            <?foreach($getUserBuyCourse as $value3){?>
                                <div class="col-sm-4 col-md-4">
                                    <a class="" href="<?echo base_url()?>user/analysis/<?echo $value3->course_url;?>">
                                        <center>
                                            <div class="circle">
                                                <?foreach($getAvgScore as $score){?>
                                                    <?if($value3->course_id == $score->course_id){?>
                                                        <span style="font-size: 3em"<?=$score->class?>"><?=$score->avg?>%</span>
                                                    <?}?>
                                                <?}?>
                                            </div>
                                            <h4><?echo $value3->course_name;?></h4>
                                        </center>
                                    </a>
                                </div>
                            <?}?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /container -->