<style>
.hero {position:relative; border-radius:5px 5px 0 0;}
.hero  {padding:10px;}
.hero-bottom {background:#eee; padding:20px 30px; border-radius:0 0 5px 5px; box-shadow:0 1px 0 0 #dedede;}
.bar-legend li span {width: 1em;height: 1em;display: inline-block;margin-right: 5px;}
.bar-legend {list-style: none;}
</style>
<div class="container">
    <div class="breadcrumb">
        <a href="<?php echo base_url()?>user/analysis">
            <i class="fa fa-arrow-left"></i>能力分析
        </a>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <?php $getUserBuyCourseChapter=json_decode($getUserBuyCourseChapter);?>
            <?php $getContentAvgScore=json_decode($getContentAvgScore);?>
            <?$chapter_name=array();$score = array();$score2 = array();?>
            <?foreach($getUserBuyCourseChapter as $value){?>
                <?php array_push($chapter_name,'"'.$value->chapter_name.'"')?>
                <?foreach($getContentAvgScore as $value2){?>
                    <?if($value->chapter_id==$value2->chapter_id){?>
                        <?php array_push($score,$value2->sum)?>
                        <?php array_push($score2,$value2->sum1)?>
                    <?}?>
                <?}?>
            <?}?>
            <?$chapter_name = implode(",", $chapter_name)?>
            <?$score = implode(",", $score)?>
            <?$score2 = implode(",", $score2)?>
            <div class="hero" style="background-color:#aaa;">
                <h4 style="color:#fff;"><?echo $result['course_name']?></h4>
            </div>
            <div class="hero-bottom">
                <div class="row">
                    <div>
                        <script> var barData = {labels : [<?echo $chapter_name?>],
                                datasets : [{
                                label: "我的分數",
                                fillColor : "green",strokeColor : "green",highlightFill: "lightgreen",highlightStroke: "lightgreen",
                                data : [<?echo $score;?>]},
                                {
                                label: "最高分數",
                                fillColor : "red",strokeColor : "red",highlightFill : "pink",highlightStroke : "pink",
                                data : [<?echo $score2;?>]
                                }]
                                };
                                var randomScalingFactor = function(){ return Math.round(Math.random()*100);};
                        </script>
                        <canvas id="canvas_bar"></canvas>
                        <div id="legend"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /container -->
<br />
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="hero" style="background-color:#aaa;">
                <h4 style="color:#fff;">圖表說明</h4>
            </div>
            <div class="hero-bottom">
                <div class="row">
                    <div>
                        <dl>
                            <dt>課程專業能力(左方長條圖)</dt>
                            <dd>顯示單一使用者於此課程下所有關卡之表現：深藍色部分為個人表現分數、淡藍色為修習該課程之所有學生之平均。</dd>
                            <dt>積極主動，藉由學生對於平台使用狀況，來了解其未來工作上的積極程度。</dt>
                            <dd>(計分方式：完成系統上課程關卡之累加統計)</dd>
                            <dt>表達互動，透過學生於平台發佈及回應的次數，來了解與人溝通的能力。</dt>
                            <dd>(計分方式：發佈資訊之投入程度，包含次數、內容屬性等)</dd>
                            <dt>問題解決，藉由使用者解決問題的能力，來分析其實務應用的能力。</dt>
                            <dd>(計分方式：發佈資訊所獲同儕認同之累加統計)</dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /container -->