<?
$row= json_decode($getUserExperience);
$row2= json_decode($getUserAward);
$row3= json_decode($getUserLicense);
$row4= json_decode($getUserPortfolio);
$row6= json_decode($getUserSkill);
if(!empty($getUserEducation)){
	$row5= json_decode($getUserEducation);
}
$row7= json_decode($getUserTraining);
$row8= json_decode($getPortfolioSkill);
$row9= json_decode($getCareerSkill);
$row10=json_decode($getPoints);
$row11=json_decode($getUserChapterBadge);
$title=urlencode("獲得新徽章：我在learninghouse完成一個關卡！");
$summary=urlencode("大家也趕快來學習吧，在這裡你可以學習到新的技能以及更多的知識喔!");
?>
<style>
#portfolio-content .instructor-avatar {
float: left;
width: 80px;
height: 80px;
background-position: center;
background-size: 80px auto !important;
border-radius: 50px;}
.instructor-avatar img{width:80px; height:80px; display:none;}
.breakword{
-ms-word-break: break-all;
     word-break: break-all;

     /* Non standard for webkit */
     word-break: break-word;

-webkit-hyphens: auto;
   -moz-hyphens: auto;
        hyphens: auto;
}
/* Landscape phones and down */
@media (max-width: 480px) {
	#portfolio-content #pop-portfolio-modal .modal-body img,#portfolio-content #pop-portfolio-modal .modal-body iframe{
		width:100%;
	}
	#canvas{
		clear:both;
		position: relative;
		left: -50px;
	}
	.video-resume iframe{
		min-width:200px;
		min-height:150px;
		width:100%;
		height:75%;
	}
}
@media(min-width:768px){
	#portfolio-content #pop-portfolio-modal .modal-body img,#portfolio-content #pop-portfolio-modal .modal-body iframe{
		width:500px;
	}
	#canvas{
		float:right;
	}	
	.video-resume iframe{
		width:600px;
		height:400px;
	}
}
@media print {
  a[href]:after {
    content: none;
  }
}
</style>
<div class="container">
    <div id="portfolio-content">
    	<div class="container">
			<h3 style="font-size:1.5em; font-weight:600; margin-top:0;">個人履歷</h3>
        	<div class="row">
                <div class="col-sm-12 col-md-4 portfolio-left">
					<div class="well">
						<div class="row">
							<div>
								<?php if($alias==$this->session->userdata('alias')){?>
									<a href="<?php echo base_url()?>user/editPortfolio" class="btn btn-default hidden-print" style="float:right;"><div class="fa fa-edit"></div>  編輯</a>
									<a href="<?php echo base_url()?>user/printPortfolio/<?echo $alias?>" class="btn btn-default hidden-print" style="float:right;" target="_blank"><div class="fa fa-edit"></div>  列印個人履歷</a>
								<?php }?>
								<div class="h4" style="padding:0px 0px 15px 0px;"><?php echo $getOneUser['name']?></div>
							</div>
							<div style="padding-top:10px;">
								<div class="instructor-avatar" style="background:url(<?=base_url();?>assets/img/user/128x128/<?php if(!empty($getOneUser['resume_img'])){echo $getOneUser['resume_img'];}else{echo 'default_user.png';}?>);">
									<img src="<?=base_url();?>assets/img/user/128x128/<?php if(!empty($getOneUser['resume_img'])){echo $getOneUser['resume_img'];}else{echo 'default_user.png';}?>">
								</div>
								<div class="col-sm-12" style="padding:0px 0px 15px 100px;">
									<?php if(!empty($getCurrentJob['company_name']) && !empty($getCurrentJob['job_title'])){?>									
										<div class="caption breakword"><?php echo $getCurrentJob['company_name']?></div>
										<div class="caption breakword"><?php echo $getCurrentJob['job_title']?></div>
									<?php }?>
									<?php if(!empty($getOneUser['location'])){?>
										<div class="caption breakword"><div class="fa fa-map-marker"></div>　 <?php echo $getOneUser['location']?></div>
									<?php }?>
									<?php if(!empty($getOneUser['mobile'])){?>
										<div class="caption breakword"><div class="fa fa-phone"></div>　 <?php echo $getOneUser['mobile']?></div>
									<?php }?>
									<?php if(!empty($getOneUser['email'])){?>
									<?php }?>
									<?php $mailto='';
										if(!empty($getOneUser['contact_email'])){
											$mailto=$getOneUser['contact_email'];
										}else{
											$mailto=$getOneUser['email'];
										}
									?>
									<div class="caption breakword">
										<div class="fa fa-envelope-o"></div>
										<!-- 下一行程式碼的開頭有個全形空白 -->
										　<a href="mailto:<?php echo $mailto;?>"><?php echo $mailto;?></a>
									</div>
									<?php if($getOneUser['work_status']==2){?>
										<div class="caption breakword">待業中</div>
									<?php }?>
									<!-- 
									<a href="#" class="btn btn-success"><div class="fa fa-star-o"></div>　加入追蹤清單</a>
									-->
								</div>
							</div>
						</div>
						<!-- 
						<hr />
						<div class="row">
							<div>
								<div class="h4" style="padding:0px 0px 15px 0px;">知識職能雷達圖</div>
								<div class="col-sm-12" style="padding-left:40px;">
									<a href="#" style="padding:20px 0px 15px 0px;">
										<div><img style="background-color:white;" src="<?//=base_url();?>assets\img\user\portfolio\radar\skill-radar.png" alt="知識職能雷達圖" width="200" height="200"></div>
									</a>
								</div>
							</div>
							<div style="padding:20px 0px 0px 120px;">
								<a href="">進一步了解>>></a>
							</div>
						</div>
						 -->
						 <!-- 
						<?php /*if(!empty($row7)){?>
						<hr />
						<div class="row">
							<?php if($alias==$this->session->userdata('alias')){?>
								<a href="<?php echo base_url()?>user/editPortfolio#training" class="btn btn-default" style="float:right;"><div class="fa fa-edit"></div>  編輯專業訓練</a>
							<?php }?>
							<div class="h4" style="padding:0px 0px 15px 0px;">專業訓練</div>
							<div style="line-height:25px;">
								<?php foreach($row7 as $key=>$value){?>
								<div>
									<div class="col-md-8 col-sm-8 col-xs-8" style="border-bottom: 1px solid #ccc;">
										<div class="breakword">
											<div><?php echo $row7[$key]->training_name.'<br>'.$row7[$key]->training_unit;?></div>
										</div>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4" style="border-bottom: 1px solid #ccc;">
										<div class="breakword">
											<div>
											<?php if($row7[$key]->training_start!=0 && $row7[$key]->training_start_month!=0){ 
												echo $row7[$key]->training_start.'/'.$row7[$key]->training_start_month.'~<br>';
											}elseif($row7[$key]->training_start!=0){	
												echo $row7[$key]->training_start.'~<br>';
											if($row7[$key]->training_end!=0 && $row7[$key]->training_end_month!=0){ 	
												echo $row7[$key]->training_end.'/'.$row7[$key]->training_end_month; 
											}elseif($row7[$key]->training_end!=0){ 
												echo $row7[$key]->training_end; 
											}else{
												echo '<br>';
											}
											?>
											</div>
										</div>
									</div>
								</div>
								<hr>
								<?php }?>
							</div>
						</div>
						<?php }*/?>
						 -->
						<?php if(!empty($row5)){?>
						<hr />
						<div class="row">
							<?php if($alias==$this->session->userdata('alias')){?>
								<a href="<?php echo base_url()?>user/editPortfolio#experience" class="btn btn-default hidden-print" style="float:right;"><div class="fa fa-edit"></div>  編輯學歷</a>
							<?php }?>
							<div class="h4" style="padding:0px 0px 15px 0px;">學歷</div>
							<div style="line-height:25px;">
								<?php foreach($row5 as $key=>$value){?>
									<div>
										<div class="col-md-8 col-sm-8 col-xs-8" style="border-bottom: 1px solid #ccc;">
											<div class="breakword">
												<div><?php echo $row5[$key]->school_name.'<br>'.$row5[$key]->department;?><?php if($row5[$key]->until_now==0){echo '(在學中)';}elseif($row5[$key]->until_now==1){echo '(畢)';}elseif($row5[$key]->until_now==2){echo '(肄)';}?></div>
											</div>
										</div>
										<div class="col-md-4 col-sm-4 col-xs-4" style="border-bottom: 1px solid #ccc;">
											<div class="breakword">
												<div>
													<?php 
													if($row5[$key]->education_start!=0 && $row5[$key]->education_start_month!=0){ 	
														echo $row5[$key]->education_start.'/'.$row5[$key]->education_start_month.'~<br>'; 
													}elseif($row5[$key]->education_start!=0){ 	
														echo $row5[$key]->education_start.'~<br>'; 
													} if($row5[$key]->until_now==0){
														echo '<br>';
													}elseif($row5[$key]->education_end!=0 && $row5[$key]->education_end_month!=0){ 	
														echo $row5[$key]->education_end.'/'.$row5[$key]->education_end_month; 
													}elseif($row5[$key]->education_end!=0){ 	echo $row5[$key]->education_end; } 
													?>
												</div>
											</div>
										</div>
									</div>
									<hr>
								<?php }?>
							</div>
						</div>
						<?php }?>
						<?php if(!empty($row)){?>
						<hr />
						<div class="row">
							<?php if($alias==$this->session->userdata('alias')){?>
								<a href="<?php echo base_url()?>user/editPortfolio#experience" class="btn btn-default hidden-print" style="float:right;"><div class="fa fa-edit"></div>  編輯經歷</a>
							<?php }?>
							<div class="h4" style="padding:0px 0px 15px 0px;">經歷</div>
							<div style="line-height:25px;">
								<?php foreach($row as $key=>$value){?>
								<div>
									<div class="col-md-8 col-sm-8 col-xs-8" style="border-bottom: 1px solid #ccc;">
										<div class="breakword">
											<div><?php echo $row[$key]->company_name.'<br>'.$row[$key]->job_title;?></div>
										</div>
									</div>
									<div class="col-md-4 col-sm-4 col-xs-4" style="border-bottom: 1px solid #ccc;">
										<div class="breakword">
											<div>
											<?php 
											if($row[$key]->experience_start!=0 && $row[$key]->experience_start_month!=0){ 
												echo $row[$key]->experience_start.'/'.$row[$key]->experience_start_month.'~<br>';
											}elseif($row[$key]->experience_start!=0){	
												echo $row[$key]->experience_start.'~<br>';
											}
											if($row[$key]->experience_end!=0 && $row[$key]->experience_end_month!=0){ 	
												echo $row[$key]->experience_end.'/'.$row[$key]->experience_end_month; 
											}elseif($row[$key]->experience_end!=0){ 
												echo $row[$key]->experience_end; 
											}else{
												echo '<br>';
											}
											?>
											</div>
										</div>
									</div>
								</div>
								<hr>
								<?php }?>
							</div>
						</div>
						<?php }?>
						<?php if(!empty($row9)){?>
						<hr />
						<div class="row">
							<?php if($alias==$this->session->userdata('alias')){?>
								<a href="<?php echo base_url()?>user/editPortfolio#career_skill" class="btn btn-default hidden-print" style="float:right;"><div class="fa fa-edit"></div>  編輯專業技能</a>
							<?php }?>
							<div class="h4" style="padding:0px 0px 15px 0px;">專業技能</div>
							<div style="line-height:25px;">
								<div class="col-sm-12">
									<?php foreach($row9 as $key=>$value){?>
									<span class="label label-default"><?php echo $row9[$key]->career_skill_name;?></span>
									<?php }?>
								</div>
							</div>
						</div>
						<?php }?>
						<?php if(!empty($row6)){?>
						<hr />
						<div class="row">
							<?php if($alias==$this->session->userdata('alias')){?>
								<a href="<?php echo base_url()?>user/editPortfolio#skill" class="btn btn-default hidden-print" style="float:right;"><div class="fa fa-edit"></div>  編輯擅長工具</a>
							<?php }?>
							<div class="h4" style="padding:0px 0px 15px 0px;">擅長工具</div>
							<div style="line-height:25px;">
								<div class="col-sm-12">
									<?php foreach($row6 as $key=>$value){?>
									<span class="label label-default"><?php echo $row6[$key]->skill_name;?></span>
									<?php }?>
								</div>
							</div>
						</div>
						<?php }?>
						<?php if(!empty($row2)){?>
						<hr />
						<div class="row">
							<?php if($alias==$this->session->userdata('alias')){?>
								<a href="<?php echo base_url()?>user/editPortfolio#award" class="btn btn-default hidden-print" style="float:right;"><div class="fa fa-edit"></div>  編輯獲獎記錄</a>
							<?php }?>
							<div class="h4" style="padding:0px 0px 15px 0px;">獲獎記錄</div>
							<div style="line-height:25px;">
								<div class="col-sm-12">
									<?php foreach($row2 as $key=>$value){?>
									<span class="label label-default"><?php echo $row2[$key]->award_name;?></span>
									<?php }?>
								</div>
							</div>
						</div>
						<?php }?>
						<?php if(!empty($row3)){?>
						<hr />
						<div class="row">
							<?php if($alias==$this->session->userdata('alias')){?>
								<a href="<?php echo base_url()?>user/editPortfolio#license" class="btn btn-default hidden-print" style="float:right;"><div class="fa fa-edit"></div>  編輯專業證照</a>
							<?php }?>
							<div class="h4" style="padding:0px 0px 15px 0px;">專業證照</div>
							<div style="line-height:25px;">
								<div class="col-sm-12">
									<?php foreach($row3 as $key=>$value){?>
									<span class="label label-default"><?php echo $row3[$key]->license_name;?></span>
									<?php }?>
								</div>
							</div>
						</div>
						<?php }?>
						<?php if(!empty($getUserSociety['facebook'])||!empty($getUserSociety['google_plus'])||!empty($getUserSociety['twitter'])||!empty($getUserSociety['linkedin'])||!empty($getUserSociety['github'])){?>
						<hr />
							<div class="row">
								<?php if($alias==$this->session->userdata('alias')){?>
									<a href="<?php echo base_url()?>user/editPortfolio#society" class="btn btn-default hidden-print" style="float:right;"><div class="fa fa-edit"></div>  編輯社群</a>
								<?php }?>
								<div class="h4" style="padding:0px 0px 15px 0px;">社群</div>
								<div style="letter-spacing: 10px;">
									<div class="col-sm-12">
										<?php if(!empty($getUserSociety['facebook'])){?>
											<a href="<?php echo $getUserSociety['facebook']?>"><i class="fa fa-facebook fa-2x"></i></a>
										<?php }?>
										<?php if(!empty($getUserSociety['google_plus'])){?>
											<a href="<?php echo $getUserSociety['google_plus']?>"><i class="fa fa-google-plus fa-2x"></i></a>
										<?php }?>
										<?php if(!empty($getUserSociety['twitter'])){?>
											<a href="<?php echo $getUserSociety['twitter']?>"><i class="fa fa-twitter fa-2x"></i></a>
										<?php }?>
										<?php if(!empty($getUserSociety['linkedin'])){?>
											<a href="<?php echo $getUserSociety['linkedin']?>"><i class="fa fa-linkedin fa-2x"></i></a>
										<?php }?>
										<?php if(!empty($getUserSociety['github'])){?>
											<a href="<?php echo $getUserSociety['github']?>"><i class="fa fa-github-alt fa-2x"></i></a>
										<?php }?>
									</div>
								</div>
							</div>
							<?php }?>
					</div>
				</div>
				<div class="col-sm-12 col-md-8 portfolio-right">
					<ul class="nav nav-tabs">
						<?php if(!empty($getOneUser['video_resume'])){?>
							<li <? if($getOneUser['performance_open_flag']!=1){echo ' class="active"';}?>><a href="#video-resume">影音履歷</a></li>
						<?php }?>
						<li<?php if(empty($getOneUser['video_resume']) && $getOneUser['performance_open_flag']!=1){echo ' class="active"';}?>><a href="#portfolio">作品與成果</a></li>
						<li><a href="#autobiography">自傳</a></li>
						<?php if($getOneUser['performance_open_flag']==1){?><li class="active"><a href="#performance">學習表現</a></li><?php }?>
					</ul>
					<div class="well">
						<div class="row">
							<?php if($alias==$this->session->userdata('alias')){?>
								<?php if(!empty($getOneUser['video_resume'])){?>
									<a href="<?php echo base_url()?>user/editPortfolio#resume-profile" class="portfolio-right-edit btn btn-default hidden-print" style="float:right;"><div class="fa fa-edit"></div>  編輯影音履歷</a>
								<?php }else{?>
									<a href="<?php echo base_url()?>user/editPortfolio#product" class="portfolio-right-edit btn btn-default hidden-print" style="float:right;"><div class="fa fa-edit"></div>  編輯作品與成果</a>
								<?php }?>
								
							<?php }?>
							<hr />
							<?php if(!empty($getOneUser['video_resume'])){?>
								<div class="video-resume <?php if($getOneUser['performance_open_flag']==1){echo ' hide';}?>">
									<div class="col-sm-12">
										<?php if (!empty($getOneUser['video_resume'])){?>
											<div class="col-sm-12" style="position:relative;top:15px;right:10px;min-height:150px;min-width:200px;">
												<iframe width="420" height="315" src="<?php echo $getOneUser['video_resume'];?>" frameborder="0" allowfullscreen></iframe>
											</div>
										<?php }?>
									</div>
								<?php if(!empty($getOneUser['intro'])){?>
								<hr />
								<div class="col-sm-12">
									<?php if($alias==$this->session->userdata('alias')){?>
										<!-- 
										<a href="<?php //echo base_url()?>user/editPortfolio#resume-profile" class="btn btn-default" style="float:right;"><div class="fa fa-edit"></div>  編輯關於我</a>
										 -->
									<?php }?>
									<div class="h4" style="padding:0px 0px 15px 0px;">關於我</div>
									<div style="line-height:25px;">
										<div class="col-sm-12">
											<?php echo $getOneUser['intro'];?>
										</div>
									</div>
								</div>
								<?php }?>
								</div>
							<?php }?>
							<div class="portfolio<?php if($getOneUser['performance_open_flag']==1){echo ' hide';}?>">
								<div class="col-sm-12">
	<!-- 								<div class="col-sm-4">
										<div class="thumbnail">
											<div class="col-sm-12" style="padding-left:10px;">
												<button class="btn btn-success" style="width:100%; height:210px;"><i class="fa fa-plus fa-5x"></i></button>
											</div>
										</div>
									</div> -->
									<?php foreach($row4 as $key=>$value){?>
										<?php 
										$portfolio_skill='';
										foreach($row8 as $key1=>$value1){
											if($row4[$key]->user_portfolio_id==$row8[$key1]->user_portfolio_id){
												if(!isset($row8[$key1+1]->user_portfolio_id)){
													foreach($row6 as $key2=>$value2){
														if($row6[$key2]->user_skill_id==$row8[$key1]->user_skill_id){
															$portfolio_skill.=$row6[$key2]->skill_name;
														}
													}
												}elseif($row8[$key1+1]->user_portfolio_id!=$row8[$key1]->user_portfolio_id){
													foreach($row6 as $key2=>$value2){
														if($row6[$key2]->user_skill_id==$row8[$key1]->user_skill_id){
															$portfolio_skill.=$row6[$key2]->skill_name;
														}
													}
												}elseif($row8[$key1+1]->user_portfolio_id==$row8[$key1]->user_portfolio_id){
													foreach($row6 as $key2=>$value2){
														if($row6[$key2]->user_skill_id==$row8[$key1]->user_skill_id){
															$portfolio_skill.=$row6[$key2]->skill_name.',';
														}
													}
												}
											}
										}
										?>
										<div class="col-md-4 col-sm-6 col-xs-12">
											<div style="margin-bottom:30px;" class="thumbnail" data-button='{"portfolio_name":"<?php echo $row4[$key]->portfolio_name;?>",
																											"portfolio_pic":"<?php echo $row4[$key]->portfolio_pic;?>",
																											"portfolio_desc":"<?php echo $row4[$key]->portfolio_desc;?>",
																											"hyperlink":"<?php echo $row4[$key]->hyperlink;?>",
																											"portfolio_type":"<?php echo $row4[$key]->portfolio_type;?>",
																											"portfolio_order":"<?php echo $row4[$key]->portfolio_order;?>",
																											"alias":"<?php echo $alias;?>",
																											"portfolio_skill":"<?php echo $portfolio_skill;?>"}'>
												<!-- <div class="col-sm-12 col-xs-12"> -->
													<!-- <a href="<?php /*if($row4[$key]->portfolio_type==4){echo $row4[$key]->hyperlink;}else{ echo 'javascript:void(0);';}*/?>"> -->
													<a href="javascript:void(0);">
														<?php if($row4[$key]->portfolio_pic!='browser.png'){?>
														<center><div><img src="<?=base_url();?>assets/img/user/portfolio/<?php echo $alias;?>/128x128/<?php echo $row4[$key]->portfolio_pic;?>"height="100%" alt="<?php echo $row4[$key]->portfolio_name;?>"></div></center>
														<?php }else{?>
														<center><div><img src="<?=base_url();?>assets/img/user/portfolio/<?php echo $row4[$key]->portfolio_pic;?>"height="100%" alt="<?php echo $row4[$key]->portfolio_name;?>"></div></center>	
														<?php }?>
													</a>
												<!-- </div> -->
												<!-- <div class="col-sm-12 col-xs-12" style="padding-left:10px;"> -->
													<div class="caption"><?php echo $row4[$key]->portfolio_name;?></div>
												<!-- </div> -->
												<!--
												<hr />
												<div class="col-sm-12 col-xs-12" style="padding-left:10px;">
													<div><i class="fa fa-eye"></i> <?php //echo $row4[$key]->hits;?></div>
 												</div>
 												-->
											</div>
										</div>
									<?php }?>
								</div>
							</div>
							<div class="col-sm-12 autobiography hide">
								<?php if(!empty($getOneUser['autobiography'])){?>
									<div class="thumbnail h4" style="padding-left: 10px;">
										<!-- <div class="col-sm-12 col-xs-12"> -->
											<!-- <div class="h4"> -->
												<?php echo $getOneUser['autobiography'];?>
											<!-- </div> -->
										<!-- </div> -->
									</div>
								<?php }?>
							</div>
			<div class="performance <?if($getOneUser['performance_open_flag']!=1){echo ' hide';}?>">
<!-- 				<h3>學習表現</h3> -->
				<div class="row">
					<div class="col-xs-12">
						<h3 style="line-height:1.4em;">總積分 <?echo $getTotalPoints;?></h3>
					</div>
					<div class="col-xs-12">
						<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8" style="margin-top:10px;">
							<?$chart=array();?>
							<?//$color=array("#ecb61f","#38b473","#5dc7d2","#e34c48","#9a86c6","#f8855c","#adb7c6","#53519f","#f36284","#893d52","#53bbb4","#9ea5b3","#666");?>
							<ul style="list-style: none">
								<?php if(!empty($row10)){?>
									<?php foreach($row10 as $key=>$val){?>
										<li class="col-xs-12 col-sm-6 col-md-4">
											<div style="height: 70px;">
												<span style="float: left;margin: 3px 0 0 -25px; color: #<?echo $val->library_color?>"><i class="fa fa-circle"></i></span>
												<h3><?php echo $val->sum?></h3>
												<p><?php echo $val->library_name?></p>
											</div>
										</li>
										<?php array_push($chart,'{value:'.$val->sum.',color:\'#'.$val->library_color.'\'}')?>
									<?}?>
								<?}?>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 col-md-push-1" style="margin-top:10px;padding-right: 0px;">
							<?php $chart = str_replace ('"'," ",json_encode( $chart ));?>
							<script> var doughnutData = <?echo $chart;?>;</script>
							<canvas id="canvas" height="200"></canvas>
						</div>
					</div>
				</div>
				<div style="margin-bottom:50px;" class="row">
					<div class="col-xs-12">
					  <?if($row11){?>
					  	<h3>我的成就</h3>
					  <?}?>
					</div>
					<div>
						<?foreach($row11 as $key=>$val){?>
							<? $image=urlencode(base_url()."assets/img/badges/".$val->chapter_pic);?>
							<? $url=urlencode(base_url().'library/'.$val->library_url.'/'.$val->course_url.'/'.$val->chapter_url);?>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin-top:10px;">
								<div class="thumbnail" style="height: 180px;">
									<div class="caption">
										<div style="float: left;width: 60%;">
											<h4><?php echo $val->chapter_name?></h4>
											<p class="text-muted"><?php echo $val->accomplish_time?> 完成</p>
											<?php if($alias==$this->session->userdata('alias')){?>
					    	        			<a 	class="btn btn-default" 
													style="position: absolute;bottom: 10px;" 
													onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $title;?>&amp;p[description]=<?php echo $summary;?>&amp;p[summary]=<?php echo $summary;?>&amp;p[url]=<?php echo $url; ?>&amp;&p[images][0]=<?php echo $image;?>', 'sharer', 'toolbar=0,status=0,width=550,height=400');" 
													target="_parent" 
													href="javascript: void(0)">
					                    			<span>分享</span>
		            							</a>
											<?php }?>
										</div>
										<div style="float: left;width: 40%;">
											<img src="<?=base_url(); ?>assets/img/badges/<?php echo $val->chapter_pic?>">
										</div>
									</div>
								</div>
							</div>
						<?}?>
					 </div>
				</div><!--/row-->
			</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="pop-portfolio-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
							</div>
							<div class="modal-body">
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        	</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
    </div>
</div><!-- /container -->
</div>
</div>