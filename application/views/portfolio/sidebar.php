<!-- 此頁增加按鈕時，需在custom.js中editPortfolio sidebar區塊修改對應的程式碼 -->
<!-- 1.加入載入我的履歷後預設關閉的分頁 -->
<!-- 2.portfolio_map新增一個對應關係 -->
<!-- 3.重新整理頁面時，自動載入的分頁 -->
<div class="container">
	<div>
		<h2 style="font-size:1.71em;">編輯個人履歷</h2>
        <hr />
	</div>
	<div class="row">
	<div class="sidebar col-md-3 portfolio-sidebar">
  		<ul class="nav nav-pills nav-stacked">
  			 <li class="">
             	<a href="#resume-profile">
                	<h4 style="margin:0;">基本資料</h4>                    
             	</a>
             </li>
             <!-- 
  			 <li class="">
             	<a href="#training">
                	<h4 style="margin:0;">專業訓練</h4>
                </a>
             </li>
              -->
  			 <li class="">
             	<a href="#experience">
                	<h4 style="margin:0;">學歷/年資/經歷</h4>
                </a>
             </li>
  			 <li class="">
             	<a href="#condition">
                	<h4 style="margin:0;">求職方向</h4>
                </a>
             </li>
  			 <li class="">
             	<a href="#career_skill">
                	<h4 style="margin:0;">專業技能</h4>
                </a>
             </li>
  			 <li class="">
             	<a href="#skill">
                	<h4 style="margin:0;">擅長工具</h4>
                </a>
             </li>
  			 <li class="">
             	<a href="#award">
                	<h4 style="margin:0;">獲獎記錄</h4>
                </a>
             </li>
  			 <li class="">
             	<a href="#license">
                	<h4 style="margin:0;">專業證照</h4>
                </a>
             </li>
  			 <li class="">
             	<a href="#autobiography">
                	<h4 style="margin:0;">自傳</h4>
                </a>
             </li>
  			 <li class="">
             	<a href="#product">
                	<h4 style="margin:0;">作品與成果</h4>
                </a>
             </li>
  			 <li class="">
             	<a href="#society">
                	<h4 style="margin:0;">社群</h4>
                </a>
             </li>
			 <!-- by JoeyC -->
			 <li class="">
             	<a href="#analysis">
                	<h4 style="margin:0;">能力分析</h4>
                </a>
             </li>
			 <!-- by JoeyC End -->
  			 <li class="">
             	<a href="#performance">
                	<h4 style="margin:0;">學習表現</h4>
                </a>
             </li>
  			 <li class="">
				<div class="row" style="line-height:4.5em;">
					履歷狀態　<input type="checkbox" name="recruiters" <?php if($getOneUser['recruiters']=='1') echo 'checked'; ?> data-button='{"recruiters":"<?php echo $getOneUser['recruiters']==0?1:0;?>"}' data-on-label="開放中" data-off-label="關閉中">
				</div>
             </li>
			<a href="<?php echo base_url().'user/portfolio/'.$this->session->userdata('alias');?>" class="btn btn-success">
				<h4 style="margin:0;">履歷預覽</h4>
			</a>
		</ul>
	</div>