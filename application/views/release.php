<div class="container">
	<div class="row basic-data">
        <div class="col-sm-12">
        	<table class="table table-striped">
				<tr><th>時間</th><th>作者</th><th>更新內容</th></tr>
				<tr><td>2014-02-14</td><td>change</td><td>討論區分頁完成，討論區分類下的分頁也已經完成</td></tr>
				<tr><td>2014-02-14</td><td>change</td><td>測驗區及影片區可直接發問問題</td></tr>
				<tr><td>2014-02-14</td><td>change</td><td>看影片時如果按下發問問題，影片會暫停，取消發問則影片繼續撥放</td></tr>
				<tr><td>2014-02-17</td><td>change</td><td>//檢查使用者是否登入<br>$this->user_model->isLogin();</td></tr>
				<tr><td>2014-02-18</td><td>change</td><td>將所有徽章都收到img/badges底下囉</td></tr>
				<tr><td>2014-02-18</td><td>change</td><td>新增勞委會要用的webcam，須為superuser才能使用</td></tr>
				<tr><td>2014-03-02</td><td>change</td><td>新增markdown語法</td></tr>
				<tr><td></td><td></td><td></td></tr>
			</table>
    	</div>
    </div>
</div>