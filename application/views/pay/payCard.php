<div class="content">
	<div class="container">
		<div>
			<h2 style="font-size:1.8em;">付款頁面(測試)</h2>
			<hr />
		</div>
	</div>
</div>
<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-1">
				<form class="form-horizontal" role="form" name="formCheck" action="https://w3.iiiedu.org.tw/FormCheck.php"method="post">
					<div class="form-group">
						<label for="OrderDetailSN" class="col-sm-2 control-label">OrderDetailSN</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="OrderDetailSN" name="OrderDetailSN" placeholder="課程訂單編碼" value="CLD_">
						</div>
					</div>
					<div class="form-group">
						<label for="Amount" class="col-sm-2 control-label">Amount</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="Amount" name="Amount"placeholder="刷卡金額">
						</div>
					</div>
					<div class="form-group">
						<label for="ReturnURL" class="col-sm-2 control-label">ReturnURL</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="ReturnURL" name="ReturnURL" placeholder="回傳網址" value="<?php echo base_url()?>pay/payCardResult">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-success">
								送出
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>