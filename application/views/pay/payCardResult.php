<div class="content">
	<div class="container">
		<div>
			<h2 style="font-size:1.8em;">付款回傳頁面(測試)</h2>
			<hr />
		</div>
	</div>
</div>
<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-1">
				<?if($result['Status'] && $result['Status'] == '4'){
					echo '<h4>交易成功</h4>';
				}else{
				//交易失敗
					echo '<h4>交易失敗</h4>';
				}?>
				<form class="form-horizontal" role="form">
				<div class="form-group">
					<label for="OrderDetailSN" class="col-sm-2 control-label">課程訂單編碼</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="OrderDetailSN" name="OrderDetailSN" value="<?php echo $result['OrderDetailSN']?>">
					</div>
				</div>
				<div class="form-group">
					<label for="Amount" class="col-sm-2 control-label">刷卡金額</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="Amount" name="Amount" value="<?php echo $result['Amount']?>">
					</div>
				</div>
				<div class="form-group">
					<label for="Status" class="col-sm-2 control-label">狀態</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="Status" name="Status" value="<?php echo $result['Status']?>">
					</div>
				</div>
				<div class="form-group">
					<label for="CardSN" class="col-sm-2 control-label">刷卡編號</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="CardSN" name="CardSN" value="<?php echo $result['CardSN']?>">
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
