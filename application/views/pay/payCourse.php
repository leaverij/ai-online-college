<?php
    $course_data = json_decode($course);
?>
<style>
.course-card {border: 1px solid #d5d5d5;}
.course-card-hero:before {content:''; height:100%; display:inline-block; margin-right:-0.25em; vertical-align:middle; z-index:1;}
.tab-pane {margin-top:10px;}
.coupon-code .field-counter{
    position: absolute;
    right: 20px;
    bottom: 7px;
    width: 25px;
    height: 20px;
    line-height: 20px;
    text-align: center;
    background: #7ac4cc;
    color: #fff;
    border-radius: 4px;
}
.field-counter {position: absolute; right: 20px; bottom: 7px; width: 25px; height: 20px; line-height: 20px; text-align: center; background: #7ac4cc; color: #fff; border-radius: 4px;}
</style>
<div class="container">
    <div>
        <div style="padding-bottom:20px;">
            <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">結帳</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h4>您的項目</h4>
            <hr>
            <div class="course-card">
                <div class="row">
                    <div class="col-xs-8 content-meta">
                        <h3><?php echo $course_data->course_name ?></h3>
                        <p><?php echo $course_data->name ?></p>
                        <div class="content-actions-container" style="position:absolute; bottom:30px; right:30px; left:30px;">NT <?php echo $course_data->course_price ?> 元</div>
                    </div>
                    <div class="col-xs-4 course-card-hero" style="background:#f2f2f2; text-align:center; position:absolute; top:0; bottom:0; right:0;">
                        <img src="<?=base_url(); ?>assets/img/badges/<?php echo $course_data->course_pic; ?>">
                    </div>
                </div>
            </div><br />
            <div class="form-group">
                <label>優惠碼</label>
                <div class="coupon-message" style="color: #d23837"></div>
                <div class="row input-coupon-field">
                    <div class="col-xs-8 col-sm-6 col-md-8 coupon-code">
                        <input type="text" class="form-control" maxlength="16" id="inputCoupon" placeholder="請輸入優惠碼">
                        <!-- <span class="field-counter">16</span> -->
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <button type="button" class="btn btn-success check-code-btn">套用</button>
                    </div>
                </div>
                <div class="row show-coupon" style="display:none">
                    <div class="col-xs-8 col-sm-6 col-md-8">
                        <span class="pull-left"><span class="coupon"></span></span>
                    </div>
                    <div class="col-xs-12 col-md-4">
                        <span class="discount">-ss</span>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="col-md-8 totalMoney">
            <span class="hideOrigin">
                <h4>總計：NT <?php echo $course_data->course_price ?> </h4>
            </span>
            <span class="showDel" style="display:none">
                <h4>總計： NT <span class="total-price"><?php echo $course_data->course_price ?></span>  <small><del>NT <?php echo $course_data->course_price ?> </del></small></h4>
            </span>
            <hr>
        </div>
        <div class="col-md-8 payMoney">
            <h4>填寫付款資訊</h4>
            <hr>
            <div class="form-group">
                <label>購買人</label>
                <input type="text" name="order_person" class="form-control">
            </div>
            <div class="form-group">
                <label>電子信箱</label>
                <input type="text" name="order_email" class="form-control">
            </div>
            <div class="form-group">
                <label>聯絡電話</label>
                <input type="text" name="order_phone" class="form-control">
            </div>
            <div class="form-group">
                <label>發票選項</label>
                <ul class="nav nav-pills">
                    <li role="presentation" class="active"><a href="#invoice_0" data-toggle="tab">電子發票</a></li>
                    <li role="presentation"><a href="#invoice_1" data-toggle="tab">企業統編</a></li>
                </ul>
                <div class="tab-content clearfix">
                    <div class="tab-pane active" id="invoice_0">
                        <div class="form-group">
                            <ul class="nav nav-pills">
                                <li role="presentation" class="active"><a href="#invoice_ele_0" data-toggle="tab">電子信箱</a></li>
                                <li role="presentation"><a href="#invoice_ele_1" data-toggle="tab">手機載具</a></li>
                            </ul>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="invoice_ele_0">
                                    <input type="text" class="form-control" placeholder="">
                                </div>
                                <div class="tab-pane" id="invoice_ele_1">
                                    <input type="text" name="invoice_mobile" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="invoice_1">
                        <div class="form-group">
                            <label>發票抬頭</label>
                            <input type="text" name="invoice_title" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>統一編號</label>
                            <input type="text" name="invoice_number" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>地址</label>
                            <input type="text" name="invoice_address" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div style="text-align:center;margin-bottom:50px;">
                <a href="#" class="btn btn-primary form-control">前往付款</a>
            </div>
        </div>
        <div class="col-md-8 buyRightNow" style="display:none">
            <form id="buy-course-form">
                <input type="hidden" id="course_id" name="course_id" value="<?php echo $course_data->course_id ?>" />
                <input type="hidden" id="coupon_code" name="coupon_code" value="" />
                <input type="hidden" id="type" name="type" value="course" />
                <input type="hidden" name="course_src"  value="<?php echo $this->session->userdata('course_src');?>"><!--送出本網頁的路徑給Controller-->
                <button type="button" id="modal-buy-course-btn" class="btn btn-primary btn-lg">立即購買</button>
            </form>
        </div>
    </div><!-- .row -->
</div><!-- .container -->