<div class="container">
    <div style="padding-bottom:20px;">
        <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">購買紀錄</h1>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
            if(count($courses)!=0){
            ?>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>項目</th>
                        <th>日期</th>
                        <th>金額</th>
                    </tr>
                </thead>
                <tbody>
            <?php
            foreach($courses as $course){
            ?>
                    <tr>
                        <td><a href="<?=base_url(); ?>library/<?php echo $course['domain_url']; ?>/<?php echo $course['course_url'] ?>"><?php echo $course['course_name']?></a></td>
                        <td><?php echo $course['order_time']?></td>
                        <td>NT$ <?php echo $course['price']?></td>
                    </tr>
            <?php
            }
            ?>
                </tbody>
            </table>
            <?php
            }else{
            ?>
            <div style="margin:10px 0px;" class="alert alert-gray">還沒有購買任何課程喔! <a href="<?=base_url()?>library/ai">探索課程</a></div>
            <?php
            }
            ?>
        </div>
    </div>
</div>