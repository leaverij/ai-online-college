<div class="container">
	<div class="breadcrumb">
    	<a href="<?php echo base_url()?>track/domain/<?echo $jobs_category_url?>">
        	<i class="fa fa-arrow-left"></i> 
            <? echo $getCategoryName?>
        </a>
    </div>
</div>
<section>
	<div class="container">
    	<div class="row">
        	<div class="col-sm-3">
                <!-- sideBar -->
				<?php $this->load->view('domain/sidebar'); ?>
            </div>
            <div class="col-sm-9">
                <div class="tab-content">
                  <div class="tab-tittle" style="overflow:hidden;">
                  	<h4 id="more-title" style="font-size:1.5em; margin-bottom:0; float:left;">企業徵才</h4>
                  	<form class="search-bar" action="<?echo base_url()?>track/jobs/<?php echo $jobs_category_url;?>" method="post" role="search">
                    <div class="input-group input-group-sm row col-sm-6 pull-right">
                      <input type="text" class="form-control" name="search_string" value="<?php echo (isset($search_string))?$search_string:'';?>"/>
                      <div class="input-group-btn">
                          <button type="submit" class="btn btn-default search-btn" tabindex="-1">
                              <span class="glyphicon glyphicon-search"></span>
                          </button>                     
                      </div>
                    </div>
                    <input type="hidden" name="action" value="search"/>
                    </form>  
                  </div>
                  <!-- tab_企業徵才 -->
                  <div id="jobs" style="padding-top:20px;">
                  	<?if (isset($search_string) && $search_string){?>
                  		<h4 style="color:#a7a7a7; font-size:14px; margin-top:0;" class="">搜尋結果如下：<?echo $search_string;?>(共<?echo count($listAllJobsByCategory->result())?>筆結果)</h4>
                    <?}?>
                      <ul class="" style="padding:0; list-style:none;">
                          <? if ($listAllJobsByCategory->num_rows() > 0){?>
                              <? foreach($listAllJobsByCategory->result() as $row){?>
                              	  <li style="padding:10px 0; border-bottom:1px solid #ddd">
                                  	<a class="record" data-type="jobs" data-id="<?php echo $row->jobs_id;?>" href="<?php echo $row->jobs_url;?>" target="_blank" style="text-decoration:none">
                                      <p style="margin:0;">
                                          <strong><?php echo $row->comp_name?></strong> 正在應徵 <strong><?php echo $row->jobs_title?></strong>
                                          <span style="font-size:12px; color:#999; font-style:italic;"><?php echo date("Y.m.d", strtotime($row->jobs_post_date))?></span>
                                      </p>
                                  </a>
                              </li>
                              <? } ?>
                          <? }?>
                      </ul>
                     <? if (!isset($search_string) && $last>1){?>
                      <? if ($listAllJobsByCategory->num_rows() > 0){?>
                      <ul class="pagination" style="">
                      	<li class=""><a href="?page=1"><i class="fa fa-angle-double-left"></i></a></li>
                        <!-- get 的page如果小於第三頁，設定page為3-->
                        <?$target = $page?>
                        <?if($page<3) $page=3;?>
                        <!-- get 的page如果大於總頁數，設定page-2-->
                        <?if($page+2>$last) $page=$last-2;?>
                        <? $index=1?>
                        <?for($i=$page-2;$i<$page+3;$i++){?>
                        	<?if($index=='1'){?>
                        		<li class=""><a href="?page=<?php echo ($page<=3)?'1':$i-1?>"><i class="fa fa-angle-left"></i></a></li>
                        	<?}?>
                        	<?if($i>'0'){?><!-- i==0 不顯示 -->
                        		<li class="<?php echo ($target==$i)?'active':''?>"><a href="?page=<?echo $i;?>"><?echo $i?></a></li>
                        	<?}?>
                        	<?if($index=='5'){?>
                        		<li class=""><a href="?page=<?php echo ($i==$last)?$last:$i+1?>"><i class="fa fa-angle-right"></i></a></li>
                        	<?}?>
                        	<? $index++;?>
                        <?}?>
                        <li class="">
                        	<a href="<?php echo ($last)?'?page='.$last:'javascript: void(0)'?>">
                        		<i class="fa fa-angle-double-right"></i>
                        	</a>
                        </li>
                      </ul>
                      <?}?>
                      <?}?>                    
                  </div>
                </div>            
            </div>
        </div>
    </div>
</section>