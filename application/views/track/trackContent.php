<style>
@import url(http://fonts.googleapis.com/css?family=Lily+Script+One|Concert+One|Capriola);
.hero {position:relative; border-radius:5px 5px 0 0;}
.hero .hero-meta {padding:30px;}
.hero-track {background:#f5f5f5; border-bottom:1px solid #ddd;}
.hero-track h1 {font-size:1.7em; margin:0 0 10px;}
.hero-track p {color:#666; font-size:1.1em; margin:0 0 20px;}
.hero-track .track-stats {}
.hero-track .track-stats strong {color:#666; font-size:1.5em;}
.hero-track .track-stats p {color:#999; font-size:0.92em; margin:0; letter-spacing:0.05em;}
.hero-track .hero-track-actions {position:absolute; right:30px; top:-8px;}
.hero-track .hero-track-actions .btn-switch-track {background:#299ba3; color:#fff; font-size:1.14em; border-radius:0; position:relative; box-shadow:3px 3px 0 rgba(0,0,0,0.1);}
.hero-track .hero-track-actions .btn-switch-track:hover {background:#369299;}
.hero-track .hero-track-actions .btn-switch-track:before {border-bottom:8px solid #106c73; border-left:8px solid transparent; content:""; display:block; position:absolute; top:0; left:-9px;}
.hero-bottom {background:#eee; padding:20px 30px; border-radius:0 0 5px 5px; box-shadow:0 1px 0 0 #dedede;}
.track-learn ul {padding-left:20px;}
.track-learn ul li {color:#666; font-size:1.14em; padding:5px 0;}

#track-begin {margin-left:115px; display:inline-block; float:left; margin-top:-80px; position:relative;}
#track-begin img {
	width:100px;
	margin-left:-80px;
	-webkit-filter: drop-shadow(0 3px rgba(0,0,0,0.15));
    -ms-filter: "progid:DXImageTransform.Microsoft.Dropshadow(OffX=0, OffY=0, Color='#ccc')";
    filter: "progid:DXImageTransform.Microsoft.Dropshadow(OffX=0, OffY=0, Color='#ccc')";}

#track {margin-bottom:30px;}
.syllabus {margin-left:60px; position:relative;}
.syllabus .path {width:5px; height:100%; background:#ddd; position:absolute; z-index:-1;}
.syllabus:last-child .path {height:40px;}
.syllabus .track-achievement {padding:20px 0 0 15px;}
.syllabus .track-achievement > div {background:#f5f5f5; border-radius:5px; box-shadow:0 1px 0 0 #dedede;}
.achievement-meta {padding:20px 30px; cursor:pointer;}
.achievement-meta img {
	float:left;
	width:65px;
	margin:-8px 0 0 -75px;
	-webkit-filter: drop-shadow(0 3px rgba(0,0,0,0.15));
    -ms-filter: "progid:DXImageTransform.Microsoft.Dropshadow(OffX=0, OffY=0, Color='#ccc')";
    filter: "progid:DXImageTransform.Microsoft.Dropshadow(OffX=0, OffY=0, Color='#ccc')";}

.achievement-meta h3 {margin:0 0 10px; font-size:1.3em; /*font-weight:bold;*/ color:#333;}
.achievement-meta h3 i {float:right; color:#999;}
.track-steps {
	float:left;
	margin:-7px 0 -7px -58px;
	-webkit-filter: drop-shadow(0 3px rgba(0,0,0,0.15));
	-ms-filter: 'progid:DXImageTransform.Microsoft.Dropshadow(OffX=0, OffY=3, Color='#ccc')';
	filter: 'progid:DXImageTransform.Microsoft.Dropshadow(OffX=0, OffY=3, Color='#ccc')';}
.achievement-meta:hover h3 {color:#000;}
.achievement-meta:hover h3 i {color:#666;}
.track-steps span {display:block; color:#fff; font-size:1.4em; line-height:1.8em; text-align:center; background:#aaa; width:35px; height:35px; border-radius:50%;}
.achievement-steps {}/*這層定義外觀*/
.achievement-meta > p {margin:0; line-height:1.6em; color:#777; padding-right:10px;}
.achievement-steps ul {padding:0; margin-bottom:0; background:#eee; border-radius:0 0 5px 5px; border-top:1px solid #cfe4e5;}
.achievement-steps ul li {list-style:none; border-bottom:1px solid #fefefe;}
.achievement-steps ul li:last-child {list-style:none; border:none;}
.achievement-steps ul li a {color:#666; padding:15px 30px 15px 80px; display:block; text-decoration:none; /*border-bottom:1px solid #e2e2e2;*/ cursor:pointer;}
.achievement-steps ul li a:hover {background:#f5f5f5;}
.achievement-steps ul li a > i {float:left; margin:-5px 0 -5px -47px;}
.achievement-steps ul li a > strong {font-size:1.14em; font-weight:normal; display:block; overflow:hidden;}

.track-icon-stage {font-family: 'Concert One', cursive; background:url(<?=base_url(); ?>assets/img/icon-stage.svg) no-repeat; width:27px; height:30px; color:#fff; font-size:1.3em; font-style:normal; text-align:center; line-height:1.6em;}
.track-icon-stage.complete {text-indent:-999em; background:url(<?=base_url(); ?>assets/img/icon-stage-complete.svg) no-repeat;}

.instructor-avatar{
	float:left;
	width:80px;
	height:80px;
	background-position:center;
	background-size:80px auto !important;
	border-radius:50px;}
.instructor-avatar img{width:80px; height:80px; display:none;}
.track-instructor ul {padding:0;}
.track-instructor ul li{list-style:none; margin-bottom:20px;}
.track-instructor ul li > h4 {padding-left:100px; line-height:4.5em;}

.table-progression-bar .table {margin:0;}
.table-progression-bar .table td {/*height:15px;*/padding:3px 1px; border-top:none; border-collapse:collapse;}
.table-progression-bar a {height:15px; display:block; background:#fff; box-shadow:0 1px 0 0 rgba(0,0,0,0.1) inset;}
.table-progression-bar a:hover {background:#ddd;}/*#cfe4e5*/
.table-progression-bar a.progress-complete {background:#71bec6;}
.table-progression-bar a.progress-complete-unordered {background:#cfe4e5;}
.table-progression-bar a.in-progress {background:#71bec6;  background-image:linear-gradient(45deg,rgba(255,255,255,.25) 5%,transparent 5%,transparent 15%,rgba(255,255,255,.25) 15%,rgba(255,255,255,.25) 25%,transparent 25%,transparent 35%,rgba(255,255,255,.25) 35%,rgba(255,255,255,.25) 45%,transparent 45%,transparent 55%,rgba(255,255,255,.25) 55%,rgba(255,255,255,.25) 65%,transparent 65%,transparent 75%,rgba(255,255,255,.25) 75%,rgba(255,255,255,.25) 85%,transparent 85%,transparent 95%,rgba(255,255,255,.25) 95%);}
.table-progression-bar .table td:first-child a {border-radius:15px 0 0 15px;}
.table-progression-bar .table td:last-child a {border-radius:0 15px 15px 0;}
@media screen and (max-width: 768px) {
	.table-progression-bar .table {background:none;}
	.table-progression-bar.table-responsive { overflow:visible; border:none; margin:0;}
}
.coupon-code .field-counter{
	position: absolute;
	right: 20px;
	bottom: 7px;
	width: 25px;
	height: 20px;
	line-height: 20px;
	text-align: center;
	background: #7ac4cc;
	color: #fff;
	border-radius: 4px;
} 
</style>
<div class="container">
	<div class="breadcrumb">
    	<a href="<?php echo base_url()?>track/domain/<?echo $jobs_category_url?>">
        	<i class="fa fa-arrow-left"></i>
			<? echo $getCategoryName?>
        </a>
    </div>
</div>
<?php $row = json_decode( $getTrackContent );?>
<!-- TRACK HERO BLOCK -->
<div class="container">
	<div class="row">
    	<div class="col-sm-12">
			<div class="hero hero-track">
            	<div class="hero-meta">
                	<h1><?php echo $row[0]->tracks_name?></h1>
                    <p><?php echo $row[0]->tracks_desc;?></p>
                    <div class="track-stats">
                    	<strong><?php echo $getTracksPeopleNo;?> </strong>位學習者
                        <p>正在挑戰這個職業地圖!</p>
                    </div>
                </div>
                <div class="hero-track-actions">
                	<?if($this->session->userdata('email')){?>
                		<?if($learning_status=='1'){?>
                			<a href="<?php echo base_url()?>college/unsubscribe/<?php echo $this->uri->segment(3, 0)?>" class="btn btn-switch-track">取消學程</a>
                		<?}elseif($learning_status=='2'){?>
                			<a href="<?php echo base_url()?>college/subscribe/<?php echo $this->uri->segment(3, 0)?>" class="btn btn-switch-track">換這個學程</a>
                		<?}else{?>
                			<a href="<?php echo base_url()?>college/subscribe/<?php echo $this->uri->segment(3, 0)?>" class="btn btn-switch-track">挑戰學程</a>
                		<?}?>
                	<?}?>
                </div>
            </div>
            <div class="hero-bottom">
            	<div style="display:table;">
					<style>
                    .hero-actions .btn-container {display:table-cell; padding-right:10px;}
                    </style>                	
                	<div class="hero-actions">
                        <?if($this->session->userdata('email')){?>
                            <!-- 檢查是否購買過該學程了，如果都買了，就直接顯示瀏覽課程囉。 
                                  檢查是否購買的方法有兩種，第一個是直接檢查是否有訂單編號，第二個是檢查學程內的課程是否全部都購買過了-->
                            <?$paid=0;if(!isset($checkBuyTracks->order_id)){?>
                                <? foreach ($getTracksPrice as $trackPrice){?>
                                    <?if(!$trackPrice->order_detail_id){?>
                                        <?$paid++;?>
                                        <!-- 如果 paid>0 ，表示還有沒有購買的課程-->
                                    <?}?>
                                <?}?>
                            <?}?>
                            <?if($paid > 0){?>
                                <div class="btn-container"><a class="btn btn-primary" data-toggle="modal" data-target="#payModal">購買學程</a></div>
                            <?}?>
                        <?}else{?>
                            <div class="btn-container"><a class="btn disabled" style="background:#ddd; border:1px solid #aaa; color:#666;">購買學程</a></div>
                        <?}?>
                    </div>
                    <div class="table-progression-bar table-responsive" style="display:table-cell; width:100%; vertical-align:middle;">
                        <table class="table">
                          <tbody>
                              <tr>
                                  <!-- for loop here start 
                                  .progress-complete => 照學程順序完成的狀態
                                  .progress-complete-unordered => 未照學程順序完成的狀態
                                  .in-progress => 照學程進度的目前學習位置
                                  -->
                                  <?$chapterNo=0;$test=0;?>
                                  <?for ( $i=0; $i<count($row); $i++) {?>
                                    <?if ($i == 0 || $row[$i]->courseKey != $row[$i-1]->courseKey) {?>
                                        <?for ( $j=0; $j<count($row); $j++) {?>
                                            <?if($row[$i]->course_id==$row[$j]->course_id && $row[$j]->chapter_name){?>
                                                <?$chapterNo++;?>
                                            <?}?>
                                        <?}?>
                                        <td><a class="<?echo $progress[$test];?>" title="<?echo $row[$i]->course_name.'　'.($chapterNo).'個關卡'?>"></a></td>
                                        <?$chapterNo=0;?>
                                        <?$test++;?>
                                    <?}?>
                                  <?}?><!-- for loop end -->
                              </tr>
                          </tbody>
                        </table>
                    </div><!-- table-progression-bar -->
                </div>
            </div>
            <div id="track-begin" style="display:none;">
                <img src="<?=base_url(); ?>assets/img/LearningTrack_superProgrammer.png">
            </div>
        </div>
    </div>
</div>

<!-- TRACK LIST BLOCK -->
<div class="container">
    <div class="row">
        <div class="col-sm-9">
            <div id="track">
            	<?$courseNo=1;$chapterNo=1;?>
 				<?for ( $i=0; $i<count($row); $i++) {?>
 				<?if ($i == 0 || $row[$i]->courseKey != $row[$i-1]->courseKey) {?>
 				<div class="syllabus">
            		<div class="path"></div>
                    	<div class="track-achievement">
                          	<div>
                              	<div class="achievement-meta">
                                  	<div class="track-steps" style="float:left; margin:-7px 0 -7px -58px; -webkit-filter: drop-shadow(0 3px rgba(0,0,0,0.15)); -ms-filter: 'progid:DXImageTransform.Microsoft.Dropshadow(OffX=0, OffY=3, Color='#ccc')'; filter: 'progid:DXImageTransform.Microsoft.Dropshadow(OffX=0, OffY=3, Color='#ccc')';}">
                                      	<span style="display:none; color:#fff; font-size:1.4em; line-height:1.8em; text-align:center; background:#aaa; width:35px; height:35px; border-radius:50%;">
                                      		<?foreach ($getUserTracksChapter as $key3 => $val3){?>
                                            	<?if($row[$i]->course_id == $val3->course_id){?>
                                           			<?if(($key3 ==0 && $val3->course_pass) || ($key3 > 0 && $getUserTracksChapter[$key3]->course_id != $getUserTracksChapter[($key3-1)]->course_id && $val3->course_pass)){
                                           				//echo 'pass';
                                           			}?>
                                          		<?}?>
                                            <?}?>
                                      		<? echo $courseNo;?>
                                      	</span>
                                  	</div>
                                    <img style="border-radius:50%;" src="<?php echo base_url() ?>assets/img/badges/<?php echo $row[$i]->course_pic?>" />
                                    <h3><?php echo $row[$i]->course_name?><i class="fa fa-chevron-down" style="display:none;"></i></h3>
                            		<p><?php echo $row[$i]->course_desc?></p>
                              	</div>
                              	<div class="achievement-steps" style="display:none"> 
                            		<ul>
                                	<?for ( $j=0; $j<count($row); $j++) {?>
                                		<?if($row[$i]->course_id==$row[$j]->course_id && $row[$j]->chapter_name){?>
                                    		<li>
                                    			<a href="<?php echo base_url()?>library/<?echo $row[$i]->library_url?>/<?echo $row[$i]->course_url?>/<?echo $row[$j]->chapter_url?>">
                                            		<i class="track-icon-stage 
                                            		<?foreach ($getUserTracksChapter as $val3){?>
                                            			<?if($row[$j]->chapter_id == $val3->chapter_id){?>
                                            				complete
                                            			<?}?>
                                            		<?}?>
                                            		"><?php echo $chapterNo?></i>	
                                            	<strong><?php echo $row[$j]->chapter_name?></strong>
                                            	</a>
                                    		</li>
                                    		<?$chapterNo++;?>
                                    	<?}?>
                                    <?}?>
                                    </ul>
                            	</div>
                          	</div>
                      	</div>
                  	</div>
                  	<?$courseNo++;$chapterNo=1;?>
                  	<?}?>
 				<?}?>
            	</div>
        	</div><!-- /track -->
        	<div class="col-sm-3">
        	<div class="track-learn">
                <h3 class="secondary-heading" style="font-size:1.5em; margin-top:20px;">你將學會</h3> 
                <ul class="check-list">
                	<?php echo $row[0]->will_learn?>
                </ul>  
            </div>
        	<div class="track-instructor">
                <h3 class="secondary-heading" style="font-size:1.5em;">講師</h3> 
                <ul>
                	<?php $trackTeacher = json_decode( $getTrackTeacher );?>
                	<?if(!empty($trackTeacher)){?>
                		<?foreach($trackTeacher as $val){?>
                    	<li>
                        	<div class="instructor-avatar" style="background:url(<?=base_url();?>assets/img/teacher/<?php echo $val->teacher_pic?>);">
                            	<img src="<?=base_url();?>assets/img/teacher/<?php echo $val->teacher_pic?>" />
                        	</div>    
                        	<h4><?php echo $val->name?></h4>
                        	<p><?php echo $val->introduction;?></p>
                    	</li>
                  		<?}?>
                  <?}?>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="payModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: #7ac4cc;border-radius: 4px 4px 0 0;">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel" style="color: #fff">購買學程</h4>
				
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-offset-1">
						<div class="form-group">
							<div class="row" style="margin-bottom: 20px">
								<?$total_price=0?>
								<?foreach ($getTracksPrice as $trackPrice){?>
									<div class="col-xs-8 col-sm-6 col-md-8">
  										<span class="pull-right"><?echo $trackPrice->course_name?></span>
  									</div>
  									<?if($trackPrice->order_detail_id){?>
  										<div class="col-xs-12 col-md-4">
  											<span>已購</span>
  										</div>
  									<?}else{?>
  										<div class="col-xs-12 col-md-4">
  											<span><?echo ($trackPrice->preferential_price)?'<del>'.$trackPrice->course_price.'</del>':$trackPrice->course_price?></span>
  											<?if($trackPrice->preferential_price){?>
  												<?$total_price = $total_price + $trackPrice->preferential_price?>
  											<?}else{?>
  												<?$total_price = $total_price + $trackPrice->course_price?>	
  											<?}?>
  										</div>
  										<?if($trackPrice->preferential_price){?>
  											<div class="col-xs-8 col-sm-6 col-md-8">
  												<span class="pull-right">特價</span>
  											</div>
  											<div class="col-xs-12 col-md-4">
  												<span><?echo ($trackPrice->preferential_price)?></span>
  											</div>
  										<?}?>
  									<?}?>
								<?}?>
							</div>
							<div class="coupon-message" style="color: #d23837"></div>
							<div class="row input-coupon-field">
  								<div class="col-xs-8 col-sm-6 col-md-8 coupon-code">
  									<input type="text" class="form-control" maxlength="16" id="inputCoupon" placeholder="輸入優惠碼">
  									<span class="field-counter">16</span>
  								</div>
  								<div class="col-xs-12 col-md-4">
  									<button type="button" class="btn btn-success check-code-btn">檢查</button>
  								</div>
							</div>
							<div class="row show-coupon" style="display:none">
  								<div class="col-xs-8 col-sm-6 col-md-8">
  									<span class="pull-right">優惠券:<span class="coupon"></span></span>
  								</div>
  								<div class="col-xs-12 col-md-4">
  									<span class="discount">-ss</span>
  								</div>
							</div>
							<div class="row" style="margin-top: 20px">
  								<div class="col-xs-8 col-sm-6 col-md-8">
  									<span class="pull-right">總金額</span>
  								</div>
  								<div class="col-xs-12 col-md-4">
  									<span class="total-price"><?echo $total_price?></span>元
  								</div>
							</div>
  						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<form id="buy-tracks-form">
					<input type="hidden" id="tracks_id" name="tracks_id" value="<?php echo $row[0]->tracks_id?>" />
					<input type="hidden" id="coupon_code" name="coupon_code" value="" />
					<input type="hidden" id="type" name="type" value="tracks" />
					<button type="button" id="modal-buy-tracks-btn" class="btn btn-primary">確定購買</button>
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->