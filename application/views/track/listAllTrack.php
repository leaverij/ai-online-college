<style>
.track-card {position:relative; border-radius:5px; border:1px solid #ddd;}
.track-card .content-meta {padding:30px; min-height:250px;}
.track-card .content-meta h3 {font-size:1.57em; margin:0 0 25px;}
.track-card .content-meta ul {padding:0;}
.track-card .content-meta ul li {list-style:none; color:#777;}
.track-card .content-meta ul li > i {margin-right:5px; color:#aaa;}
.track-card.in-progress {background:#f5f5f5; border:none; box-shadow:0 1px 0 0 #dedede;}
</style>

<div id="content">
    <div class="container">
        <div class="row">
        	<div class="col-sm-12">
                <div class="track-hero" style="background:url(<?=base_url(); ?>assets/img/track-hero-bg.png) no-repeat center; background-size:cover; text-align:center; position:relative; border-radius:5px; border-bottom:1px solid #ddd; height:300px; margin-bottom:30px;">
                	<div class="col-sm-6 col-sm-offset-3">
                        <div class="hero-content" style="position:absolute; top:100px; right:0; left:0;">
                            <h1 style="font-size:1.71em; letter-spacing:0.05em; color:#fff;">挑戰一個學程</h1>
                            <p style="font-size:1.14em; color:#e9f3f7;">Learning House 最佳學習指引</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  TRACK LISTS  -->
        <div class="row">
        	<?php $row = $getAllTracks->result()?>
        	<?php $checkUserTrack = $getAllTracks->row();?>
        	<?php $track_key = false;?>
        	<?php if(!empty($checkUserTrack->email)){
        		$track_key = true;
        	}?>
			<? foreach($row as $key =>$val){?>
        	<div class="col-sm-6" style="margin-bottom:30px;">
                <div class="track-card <?php echo ($track_key&&$key=='0')?'in-progress':''?>">
                	<div class="content-meta">
                    	<h3><?php echo $val->tracks_name?></h3>
                        <div>
                        	<?php echo $val->tracks_desc?>
                        </div>
                        <div style="position:absolute; bottom:30px;">
                        	<?php if(!$this->session->userdata('email')){?>
                        		<a href="<?php echo base_url()?>track/trackContent/<?php echo $val->tracks_url?>" class="btn" style="background:#299ba3; color:#fff; font-size:1.14em; letter-spacing:0.1em;">
                            		詳細內容
                            	</a>
                        	<?}elseif($track_key&&$key=='0'){?>
                        		<a href="<?php echo base_url()?>track/trackContent/<?php echo $val->tracks_url?>" class="btn" style="background:#99ca6a; color:#fff; font-size:1.14em; letter-spacing:0.1em;">
                            		繼續<i class="fa fa-chevron-right" style="color:#fff; margin-left:5px;"></i>
                            	</a>
                        	<?}elseif($track_key){?>
                        		<a href="<?php echo base_url()?>track/trackContent/<?php echo $val->tracks_url?>" class="btn" style="background:#299ba3; color:#fff; font-size:1.14em; letter-spacing:0.1em;">
                            		換一個學程
                            	</a>
                        	<?}else{?>
                        		<a href="<?php echo base_url()?>track/trackContent/<?php echo $val->tracks_url?>" class="btn" style="background:#299ba3; color:#fff; font-size:1.14em; letter-spacing:0.1em;">
                            		挑戰一個學程
                            	</a>
                        	<?}?>
                        </div>
                    </div>
                </div>
            </div>
        	<?}?>
        </div><!-- /row -->
    </div>
</div>