<div class="container" style="padding-top:30px;">
	<div class="row">
    	<div class="col-md-3 col-sm-3 sidebar">
            <!-- load sidebar.php -->
            <? $this->load->view('library/sidebar');?>
        </div><!-- /sidebar-->
        <style>
        .track-card {border:1px solid #e2e2e2; border-radius:5px;}
		.track-card img {border-radius:4px 4px 0 0; width: 100%;}
		.track-card .content-meta {padding:10px 15px;}
		.track-card .content-meta h4 {margin:0 0 5px; font-size:1.14em; font-weight:bold;}
		.track-card .content-meta span {color:#888;}
		
        </style>
        <div class="col-md-9 col-sm-9">
        	<!-- load searchbar.php -->
        	<? $this->load->view('library/searchbar');?>
            <? $row = $getAllTracks->result()?>
            <? if ($getAllTracks->num_rows() > 0){?>
            	<div class="row">
            <? foreach($row as $key =>$val){?>
                <div class="col-md-4 col-sm-6" style="margin-bottom:30px;">
                    <div class="track-card">
                        <img src="<?php echo base_url()?>assets/img/track/<?php echo $val->pic?>" />
                        <div class="content-meta">
                            <h4><?php echo $val->tracks_name?></h4>
                            <span><?php echo($val->total!=0)?$val->total.'門課':'&nbsp;' ?></span>
                        </div>
                        <!-- 該學程內是否有課程 -->
                        <?if($val->total!=0){?>
                        	<!-- 登入 -->
                        	<?if($this->session->userdata('email')){?>
                        		<?for($i=0;$i<count($tracks_id);$i++) {?>
                        			<?if($tracks_id[$i]==$val->tracks_id){?>
                        				<?if(!empty($getSubscribeTrack->tracks_id)){?>
                        					<?if($tracks_id[$i]==$getSubscribeTrack->tracks_id){?>
                        						<div class="content-actions-container" style="background:#f5f5f5; border-radius:0 0 4px 4px;">
                                                    <a class="clearfix" href="<?php echo base_url()?>track/trackContent/<?php echo $val->tracks_url?>" style="font-size:1.14em; display:block; padding:15px; color:#999; text-align:center; text-decoration:none;">
                                                    <? $sum = ($passAmount[$i]/$val->total)*100?>
                                                    <span class="pull-left"><?echo $passAmount[$i].' / '.$val->total?></span>
                                                    <div style="padding:5px 0 0 40px;">
                                                        <div class="progress" style="margin:0; height:10px; background:#eee;">
                                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?echo $sum;?>" aria-valuemin="0" aria-valuemax="100" style="width:<?echo $sum;?>%;">
                                                                <span class="sr-only"><?echo $sum;?>% Complete</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </a>
                        						</div>
                        					<?}else{?>
                        						<div class="content-actions-container" style="background:#279ca7; border-radius:0 0 4px 4px;">
                                                    <a href="<?php echo base_url()?>track/trackContent/<?php echo $val->tracks_url?>" style="font-size:1.14em; display:block; padding:15px; color:#fff; text-align:center; text-decoration:none;">
                                                        探索學程<i class="fa fa-arrow-right" style="font-size:12px; margin-left:5px;"></i>
                                                    </a>
                                            	</div>
                        					<?}?>
                        				
                        			<?}else{?>
                        				<div class="content-actions-container" style="background:#279ca7; border-radius:0 0 4px 4px;">
                                            <a href="<?php echo base_url()?>track/trackContent/<?php echo $val->tracks_url?>" style="font-size:1.14em; display:block; padding:15px; color:#fff; text-align:center; text-decoration:none;">
                                                挑戰學程<i class="fa fa-arrow-right" style="font-size:12px; margin-left:5px;"></i>
                                            </a>
                                        </div>
                        			<?}?>
                        			<?}?>
                        		<?}?>
                        	<!-- 未登入 -->
                        	<?}else{?>
                        		<div class="content-actions-container" style="background:#279ca7; border-radius:0 0 4px 4px;">
                          			<a href="<?php echo base_url()?>track/trackContent/<?php echo $val->tracks_url?>" style="font-size:1.14em; display:block; padding:15px; color:#fff; text-align:center; text-decoration:none;">
                            			挑戰學程<i class="fa fa-arrow-right" style="font-size:12px; margin-left:5px;"></i>
                            		</a>
                        		</div>
                        	<?}?>
                		<!-- 該學程內無課程，顯示即將上線 -->
                        <?}else{?>
                        	<div class="content-actions-container" style="background:#f5f5f5; border-radius:0 0 4px 4px;">
                          		<a href="" style="display:block; padding:15px; color:#999; text-align:center; text-decoration:none;">
                            		即將上線
                            	</a>
                        	</div>
                        <?}?>
                    </div>
                </div>
           <?}?>
           </div><!-- /row -->
           <?}else{?>
           <div class="row">
            	<div class="col-md-12">
                	<div class="upcoming-releases">
                    	<p>目前尚無學程喔! 就從左邊的 <strong>課程分類</strong> 開始學習吧!</p> 
                        <p>您也可至其他 <strong>職務分類</strong> 找出最適合您的學程挑戰，成為職場上搶手的好人才!</p>
                    </div>
                </div>
            </div> 
           <?}?>
            <!-- 以下為知識中心  -->
            <div class="row news">
				<style>
                #myTab.nav > li > a, #myTab.nav-pills > li > a {color:#333; padding:0 0 3px; border-radius:0;}
                #myTab.nav > li > a:hover, #myTab.nav > li > a:focus {border-bottom:2px solid #299ba3; background:none;}
                #myTab.nav-pills > li.active > a, #myTab.nav-pills > li.active > a:hover, #myTab.nav-pills > li.active > a:focus {color:#333; background:none; border-bottom:2px solid #299ba3;}
                
				.news {margin-bottom:30px}
                .news .secondary-heading {margin-top:0px;}
				.news .secondary-heading h3 {margin:0px; font-size:18px;}
				.news .secondary-heading h3 a {color:#333;}
				.news .secondary-heading h3 a:hover, .news .secondary-heading h3 a.active {color:#279ca7; text-decoration:none;}
				.news ul {padding:0;}
				.news ul li {list-style:none; margin-bottom:6px;}
				.news .news-activities ul, .news .news-resources ul {padding-left:60px;}
				.news .news-forum ul a, .news .news-activities ul a {display:block; text-decoration:none; white-space:nowrap; text-overflow:ellipsis; overflow:hidden; cursor:pointer;}
				.news .news-activities ul .label, .news .news-resources ul .label {width:50px; float:left; margin-left:-60px; line-height:1.3em;}
				.news .news-forum ul p, .news .news-activities ul p {margin-bottom:0; font-size:12px; color:#999;}
				.news .time {font-size:12px; color:#999;}
				.news a.more {font-size:12px; font-weight:bold; color:#2a6496;}
				.news-forum-more {font-size:12px; font-weight:bold; color:#2a6496;}
                </style>
                <div class="col-sm-6">
                    <div class="news-forum">
                        <!-- Nav tabs -->
                        <div id="myTab"class="nav nav-pills secondary-heading">
                        	<h3 class="pull-left">
                            	<a href="#home" class="active discussion-flag" data-toggle="pill">熱門討論</a> |
                                <a href="#profile" class="discussion-flag" data-toggle="pill">最新討論</a>
                            </h3>
                            <a href="<?php echo base_url()?>forum" class="news-forum-more pull-right">
                                更多 <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="home">
                                <ul id="HotDis">
                                <? if ($listPopularDiscussions->num_rows() > 0){?>
                                    <? foreach($listPopularDiscussions->result() as $row){?>
                                    <li>
                                        <a href="<?php echo base_url()?>forum/forumContent/<?php echo $row->forum_topic_id?>"><?php echo $row->forum_topic;?></a>
                                        <p>
                                            <i class="fa fa-comment"></i> <?php echo ($row->count-1)?> ‧ 經由 <strong style="color:#6f6f6f;"><?php echo $row->alias;?></strong> 提問
                                        </p>
                                    </li>
                                    <? }?>
                                <? }?>
                                </ul>
                            </div>
                            <div class="tab-pane" id="profile">
                                <ul id="NewDis">
                                <? if ($listNewDiscussions->num_rows() > 0){?>
                                    <? foreach($listNewDiscussions->result() as $row){?>
                                    <li>
                                        <a href="<?php echo base_url()?>forum/forumContent/<?php echo $row->forum_topic_id?>"><?php echo $row->forum_topic;?></a>
                                        <p>
                                            <i class="fa fa-comment"></i> <?php echo ($row->count-1)?> ‧ 經由 <strong style="color:#6f6f6f;"><?php echo $row->alias;?></strong> 提問
                                        </p>
                                    </li>
                                    <? }?>
                                <? }?>
                                </ul>
                            </div>
                        </div>
                        <a href="<?php echo base_url()?>forum" class="more hidden">
                            更多熱門討論 <i class="fa fa-angle-double-right"></i>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="news-activities">
                    	<div class="secondary-heading clearfix">
                        	<h3 class="pull-left">相關活動</h3>
                            <a id="moreSeminar" class="more pull-right" data-more="seminar" data-url="<?php echo $jobs_category_url;?>" href="javascript: void(0)">
                            	更多 <i class="fa fa-plus"></i>
                            </a>
                        </div> 
                        <ul>
                            <? if ($listSeminarByJobsCategory->num_rows() > 0){?>
                                <? foreach($listSeminarByJobsCategory->result() as $row){?>
                                    <li>
                                        <span class="label <?php echo $row->label_name;?>">
                                            <?switch ($row->seminar_name){	case "1":echo "研討會";break;
                                                                            case "2":echo "競賽";break;
                                                                            case "3":echo "展覽";break;
                                                                            case "4":echo "其他";break;
                                                                            default:echo "其他";}?>
                                        </span>
                                        <a class="record" data-type="seminar" data-id="<?php echo $row->seminar_id;?>" data-toggle="modal" data-target="#seminar<?php echo $row->seminar_id;?>">
                                             <?php echo $row->seminar_title;?>
                                        </a>
                                        <p>
                                            <? if($row->seminar_start_time == $row->seminar_end_time){?>
                                                時間 ： <?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>
                                            <? }else{?>
                                                時間 ： <?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>～<?php echo date("Y-m-d", strtotime($row->seminar_end_time));?>
                                            <? }?>
                                        </p>
                                    </li>
                                    <!-- Modal-相關活動 -->
                                    <div class="modal fade" id="seminar<?php echo $row->seminar_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title" id="myModalLabel">
                                                        <?switch ($row->seminar_name){
                                                            case "1":echo "研討會";break;
                                                            case "2":echo "競賽";break;
                                                            case "3":echo "展覽";break;
                                                            case "4":echo "其他";break;
                                                            default:echo "其他";}?>資訊</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-2 control-label">活動名稱</label>
                                                            <div class="col-sm-10">
                                                                <p class="form-control-static"><?php echo $row->seminar_title;?></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-2 control-label">主辦單位</label>
                                                            <div class="col-sm-10">
                                                                <p class="form-control-static"><?php echo $row->seminar_host;?></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-2 control-label">活動連結</label>
                                                            <div class="col-sm-10">
                                                                <p class="form-control-static"><a href="<?php echo $row->seminar_url;?>" target="_blank" class="modal_record" data-type="seminar" data-id="<?php echo $row->seminar_id;?>">連結</a></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-2 control-label">摘要</label>
                                                            <div class="col-sm-10">
                                                                <p class="form-control-static"><?php echo $row->seminar_content;?></p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-2 control-label">時間</label>
                                                            <div class="col-sm-10">
                                                                <p class="form-control-static">
                                                                <?if($row->seminar_start_time == $row->seminar_end_time){?>
                                                                	<?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>
                                                                <?}else{?>
                                                                	<?php echo date("Y-m-d", strtotime($row->seminar_start_time));?>～<?php echo date("Y-m-d", strtotime($row->seminar_end_time));?>
                                                                <?}?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" class="col-sm-2 control-label">地點</label>
                                                            <div class="col-sm-10">
                                                                <p class="form-control-static"><?php echo $row->seminar_place;?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                <? }?>
                            <? }?>
                        </ul>
                        <a id="moreSeminar" class="more hidden" data-more="seminar" data-url="<?php echo $jobs_category_url;?>" href="javascript: void(0)">
                            更多相關活動 <i class="fa fa-angle-double-right"></i>
                        </a>
                    </div>
                </div>
            </div><!-- /row -->
            <div class="row news">
                <div class="col-sm-12">
                    <div class="news-trends">
                    	<div class="secondary-heading clearfix">
                        	<h3 class="pull-left">趨勢應用</h3>
                            <a id="moreIndustry" class="more pull-right" data-more="industry" data-url="<?php echo $jobs_category_url;?>" href="javascript: void(0)" style="color:#2a6496;">
                                更多 <i class="fa fa-plus"></i>
                            </a>
                        </div> 
                        <ul>
                            <? if ($listIndustryByJobsCategory->num_rows() > 0){?>
                                <? foreach($listIndustryByJobsCategory->result() as $row){?>
                                    <li>
                                        <a class="record" data-type="industry" data-id="<?php echo $row->industry_id;?>" href="<?php echo $row->industry_url;?>" target="_blank">
                                            <?php echo $row->industry_title?>
                                        </a> 
                                        <span class="time">(<?php echo date("Y.m.d", strtotime($row->industry_post_date))?>)</span>
                                    </li>
                                <? }?>
                            <? }?>
                        </ul>
                        <a id="moreIndustry" class="more hidden" data-more="industry" data-url="<?php echo $jobs_category_url;?>" href="javascript: void(0)">
                            更多趨勢應用 <i class="fa fa-angle-double-right"></i>
                        </a>
                    </div>
                </div>
            </div><!-- /row -->
            <div class="row news">
              <div class="col-sm-12">
                  <div class="news-resources">
                  	  <div class="secondary-heading clearfix">
                    	  <h3 class="pull-left">國內外資源</h3>
                          <a id="moreResource" class="more pull-right" data-more="resource" data-url="<?php echo $jobs_category_url;?>" href="javascript: void(0)">
                              更多 <i class="fa fa-plus"></i>
                          </a>
                      </div> 
                      <ul>
                          <? if ($listResourceByJobsCategory->num_rows() > 0){?>
                              <? foreach($listResourceByJobsCategory->result() as $row){?>
                                  <li>
                                      <span class="label <?php echo $row->label_name;?>">
                                          <?switch ($row->resource_type){
                                              case "1":echo "影片";break;
                                              case "2":echo "文件";break;
                                              case "3":echo "文章";break;
                                              case "4":echo "其他";break;
                                              case "5":echo "平台";break;
                                              default:echo "其他";}?>
                                      </span>
                                      <a class="record" data-type="resource" data-id="<?php echo $row->resource_id;?>" href="<?php echo $row->resource_url;?>" target="_blank">
                                          <?php echo $row->resource_title?>
                                      </a>
                                      <span class="time">(<?php echo date("Y.m.d", strtotime($row->resource_post_date))?>)</span>
                                  </li>
                              <? }?>
                          <? }?>
                      </ul>
                      <a id="moreResource" class="more hidden" data-more="resource" data-url="<?php echo $jobs_category_url;?>" href="javascript: void(0)">
                          更多國內外資源 <i class="fa fa-angle-double-right"></i>
                      </a>
                  </div>
              </div>
            </div><!-- /row -->
            <div class="row news">
                <div class="col-sm-12">
                    <div class="news-jobs">
                    	<div class="secondary-heading clearfix">
                        	<h3 class="pull-left">企業徵才</h3>
                            <a id="moreJobs" class="more pull-right" data-more="jobs" data-url="<?php echo $jobs_category_url;?>" href="javascript: void(0)">
                                更多 <i class="fa fa-plus"></i>
                            </a>
                        </div> 
                        <ul>
                            <? if ($listJobsByJobsCategory->num_rows() > 0){?>
                                <? foreach($listJobsByJobsCategory->result() as $row){?>
                                <li>
                                    <a class="record" data-type="jobs" data-id="<?php echo $row->jobs_id;?>" href="<?php echo $row->jobs_url;?>" target="_blank" style="text-decoration:none">
                                        <strong><?php echo $row->comp_name?></strong> 正在應徵 <strong><?php echo $row->jobs_title?></strong>
                                        <span class="time"><?php echo date("Y.m.d", strtotime($row->jobs_post_date))?></span>
                                    </a>
                                </li>
                                <? } ?>
                            <? }?>
                        </ul>
                        <a id="moreJobs" class="more hidden" data-more="jobs" data-url="<?php echo $jobs_category_url;?>" href="javascript: void(0)">
                            更多企業徵才 <i class="fa fa-angle-double-right"></i>
                        </a>
                    </div>
                </div>
            </div><!-- /row -->
        </div>
    </div>
</div>

<?php if($this->session->userdata('email')){?>
	<?$this->load->view('forum/domainModalStartDiscuss');?>
<? }?>
<? $this->load->view('home/modal_login');?>
