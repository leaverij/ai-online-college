			<style>
            ul.course-block-list {padding:0;}
			ul.course-block-list > li {list-style:none; margin-bottom:30px;}
			.course-card-hero:before {content:''; height:100%; display:inline-block; margin-right:-0.25em; vertical-align:middle; z-index:1;}
            </style>
            <div class="row">
            	<ul class="course-block-list">
                  <? foreach($row2 as $key=>$val){?>
                    <?php
                        foreach($row3 as $key3=>$val3){
                            if($val3->course_url==$val->course_url) {
                                $numOfCourseBadges = $val3->numOfBadges;
                            }
                        }
                        foreach($row4 as $key4=>$val4){
                            if($val4->course_url==$val->course_url) {
                                $numOfUserCourseBadges = $val4->numOfBadges;
                            }
                        }
                    ?>                   
                	<li class="col-md-6 col-sm-6">
                        <div class="course-card" style="border:1px solid #e2e2e2;">
                            <a href="<?php echo base_url().'library/'.$val -> library_url.'/'.$val->course_url?>">
                              <div class="col-xs-8 content-meta">
                                  <h3 style="font-size:16px;"><?php echo $val -> course_name; ?></h3>
								  <?php $email=$this->session->userdata('email');?>
                                   <?php if(!empty($email) && $numOfCourseBadges !='0'){?>
                                    <div class="progress" style="margin:0; height:10px; position:absolute; bottom:30px; right:30px; left:30px;">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $numOfCourseBadges==0?0:$numOfUserCourseBadges/$numOfCourseBadges*100;?>%;">
                                            <span class="sr-only"><?php echo $numOfCourseBadges==0?0:$numOfUserCourseBadges/$numOfCourseBadges*100;?>% Complete</span>
                                        </div>
                                    </div>
								  <?php }?>
                                  <p style="font-size:13px; padding-bottom:10px;">
                                      <?php echo $val -> course_desc; ?>
                                  </p>
                              </div>
                              <div class="col-xs-4 course-card-hero" style="background:#f5f5f5; text-align:center; position:absolute; top:0; bottom:0; right:0;">
                              	 <img src="<?=base_url(); ?>assets/img/badges/<?php echo $val -> course_pic; ?>">
                              </div>
                            </a>
                        </div>
                    </li>
                  <? }?>
                </ul>
            </div>
            
            <!-- <ul id="unit-list" class="list-unstyled" style="padding-top:20px;">
                <? foreach($row2 as $key=>$val){?>
                    <?php
                        foreach($row3 as $key3=>$val3){
                            if($val3->course_url==$val->course_url) {
                                $numOfCourseBadges = $val3->numOfBadges;
                            }
                        }
                        foreach($row4 as $key4=>$val4){
                            if($val4->course_url==$val->course_url) {
                                $numOfUserCourseBadges = $val4->numOfBadges;
                            }
                        }
                    ?>                   
                    <li>
                    	<div class="unit-block">
                            <div class="col-sm-10">
                                <div class="unit-meta">
                                    <img src="<?=base_url(); ?>assets/img/badges/<?php echo $val -> course_pic; ?>" width="80">
                                    <h3><?php echo $val -> course_name; ?></h3>
                                    <p><?php echo $val -> course_desc; ?></p>
                                    <?php $email=$this->session->userdata('email');?>
                                    <?php if(!empty($email) && $numOfCourseBadges !='0'){?>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $numOfCourseBadges==0?0:$numOfUserCourseBadges/$numOfCourseBadges*100;?>%;">
                                                <span class="sr-only"><?php echo $numOfCourseBadges==0?0:$numOfUserCourseBadges/$numOfCourseBadges*100;?>% Complete</span>
                                            </div>
                                        </div>
                                    <?php }?>
                                </div>
                            </div>
                            <div class="col-sm-2" style="text-align:center;">
                            	<div class="enter-btn">
                                	<a class="btn btn-primary" href="<?php echo base_url().'library/'.$val -> library_url.'/'.$val->course_url?>" role="button">進入</a>
                                </div>
                            </div>
                        </div>
                    </li>
                <?}?>
            </ul> -->
