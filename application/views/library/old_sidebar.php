<style>
.search-bar{position:relative;}
.search-bar .search-text {display:block; width:100%; padding:15px; color:#666; font-size:14px; background:#f9f9f9; border:none; border-bottom:1px solid #cfe4e5; border-radius:5px 5px 0 0;}
.search-bar ::-webkit-input-placeholder {color:#bcbcbc;/* WebKit browsers */}
.search-bar .search-text:focus::-webkit-input-placeholder {color:#e3e3e3; transition:ease 0.1s; -webkit-transition:ease 0.1s; -moz-transition:ease 0.1s; -ms-transition:ease 0.1s;}
.search-bar :-moz-placeholder {color:#bcbcbc;/* Mozilla Firefox 4 to 18 */}
.search-bar ::-moz-placeholder  {color:#bcbcbc;/* Mozilla Firefox 19+ */}
.search-bar :-ms-input-placeholder {color:#bcbcbc;/* Internet Explorer 10+ */}
.search-bar .search-btn{position:absolute; top:0; right:0; bottom:0; border:none; padding:0 15px; background:none;}
.search-bar .search-btn span{font-size:16px; color:#bcbcbc;}

.sidebar {margin-bottom:30px; background:#f5f5f5; border-radius:5px; box-shadow:0 1px 0 0 #ededed; padding-bottom:10px;}
.nav-list li > a {padding:5px 25px;}
#sidebar-collapse ul li h4 {font-size:16px;}
#sidebar-collapse ul li h4 a:hover, #sidebar-collapse ul li.active a{text-decoration:none; background:#eeeeee; color:#24bbc9;}
#sidebar-collapse ul li a {color:#666; font-size:15px;}
#sidebar-collapse ul li a:hover {color:#24bbc9;}
#sidebar-collapse .library-list li a {color:#888;}
@media screen and (max-width: 768px) {
	.sidebar {padding:0;}
	.sidebar .navbar-toggle {display:block; margin:0; padding:10px 12px; cursor:pointer; color:#666; font-size:16px;}
}
</style>

<div class="container">    
    <!--     BLOCK SIDE BAR    -->
	<div class="row" style="margin-top:30px;">
		<?php $row = json_decode( $getAll );
        $sum = 0;
        foreach($row as $val){
            $sum +=  $val -> total;
        }?>
        <div class="col-sm-3">
        	<div class="sidebar">
                <form class="search-bar" action="<?php echo base_url().'library/search'?>" method="post" role="search">
                    <div class="">
                        <input class="search-text" type="text" name="search_string" placeholder="今天想要找甚麼課程呢？" value="<?php echo (isset($search_string))?$search_string:'';?>">
                        <button type="submit" class="search-btn">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>                    
                    </div>
                </form>
                <!--<div class="">
                  <a class="navbar-toggle" data-toggle="collapse" data-target="#test-sidebar-collapse" style="float:none;">
                    <span>瀏覽課程庫</span>
                    <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>  
                  </a>
                </div>-->
                <div id="sidebar-collapse" class="collapse navbar-collapse" style="padding:0;">
                    <ul class="nav nav-stacked">
                      <li>
                        <a href="<?php echo base_url().'library'?>" style="display:block; padding:10px 15px;">
                            <span class="badge pull-right"><?php //echo $sum;?></span>
                            <h4 style="margin:0;">課程庫</h4>
                        </a>
                      </li>
                      <li>
                      	<h4 style="margin:0; padding:10px 15px;">課程分類</h4>
                      	<ul class="nav-list nav">
							<?php foreach($row as $val){?>
                            <li class="<?php echo $val -> library_url; ?>">
                            	<a href="<?php echo base_url().'library/'.$val -> library_url?>">
                                	<span class="badge pull-right" style="margin-top:2px;"><?php echo ($val -> deep_dives)?$val -> deep_dives:'0'?></span>
									<?php echo $val -> library_name; ?>
                                </a>
                            </li>
                            <? } ?>
                        </ul>
                      </li>
                    </ul>
                </div>
                <div class="">
                  <a class="navbar-toggle" data-toggle="collapse" data-target="#sidebar-collapse" style="float:none; position:relative; text-decoration:none; border-top:1px solid #cfe4e5; margin-top:-1px; border-radius:0 0 5px 5px;">
                    <span>瀏覽課程庫</span>
                    <i class="fa fa-eye" style="position:absolute; top:50%; right:0; border:none; margin-top:-8px; padding:0 15px; background:none;"></i>
                    <span class="sr-only">Toggle navigation</span>
                <!--<span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>-->  
                  </a>
                </div>
            </div>
<!--        
            <form class="search-bar" action="<?php echo base_url().'library/search'?>" method="post" role="search">
                <div class="form-group">
                    <input type="text" name="search_string" class="form-control" placeholder="今天想要找甚麼課程呢？">
                    <button type="submit" class="search-btn">
                    	<span class="glyphicon glyphicon-search"></span>
                    </button>                    
                </div>
            </form>

                    
            <ul class="nav nav-pills nav-stacked" style="margin-bottom:30px;">
                 <li class="">
                 	<a href="<?php echo base_url().'library'?>">
                    	<span class="badge pull-right"><?php //echo $sum;?></span>
                        <h4 style="margin:0;">課程庫</h4>
                    </a>
                 </li>
                 <li class="nav-header"><h4 style="margin:0;">課程分類</h4></li>
                   <?php foreach($row as $val){?>
                 <li class="<?php echo $val -> library_url; ?>"><a href="<?php echo base_url().'library/'.$val -> library_url?>"><span class="badge pull-right"><?php echo $val -> total; ?></span><?php echo $val -> library_name; ?></a></li>
                   <?} ?>
                 <li class="nav-header">工作能力/學程</li> 
            </ul>
-->        
        </div>