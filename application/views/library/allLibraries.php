		<style>
        .tour-start{margin-bottom:20px;padding:15px; background: #f5f5f5; -webkit-border-radius:4px; -moz-border-radius:4px; -ms-border-radius:4px; border-radius:4px;}
        .tour-start h4{margin-top:0;}
		.tour-start span{color:#999999;}		
        </style>
        <?php $row = json_decode( $getAll );?>
        <style>
        	<?foreach($row as $val){?>
        	.course-card.<?echo $val->library_url?> {border-left:10px solid #<?echo $val->library_color?>;}
        	<?}?>
        </style>
<div class="container" style="padding-top:30px;">
	<div class="row">
		<!-- load sidebar.php -->
		<?php $this->load->view('library/sidebar'); ?> 
  		<div class="col-sm-9">
        	<div class="tour-start hidden-xs">
            	<h4>歡迎來到課程庫!</h4>
                <span>任何你有興趣的課程分類，都可以點選進入瀏覽!</span>
            </div>
        	<div id="library">	
                <div class="row"> 
                    <?foreach($row as $val){?>
                        <div class="col-sm-6" style="margin-bottom:30px;">
                            <div class="course-card topic <?php echo $val -> library_url; ?>">
                                <a href="<?php echo base_url().'library/'.$val -> library_url?>">
                                  <div class="content-meta">
                                      <h3><?php echo $val -> library_name; ?></h3>
                                      <p>
                                          <?php echo $val -> library_desc; ?>
                                      </p>
                                  </div>
                                </a>
                            </div>
                        </div>
                    <? } ?>
                
                	<!-- Ex-ver. backup / 前一版備份
                    <?php $row = json_decode( $getAll );
                    foreach($row as $val){?>
                        <div class="col-sm-6">
                            <div class="thumbnail <?php echo $val -> library_url; ?>" style="margin-bottom:20px;">
                                <div class="text-center">
                                    <h3><?php echo $val -> library_name; ?></h3>
                                </div>
                                <div class="caption text-left">
                                    <?php echo $val -> library_desc; ?>
                                </div>
                                <div class="caption">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <p>知識秘笈　<span class="badge"><?php echo ($val -> deep_dives)?$val -> deep_dives:'0'; ?></span></p>
                                            <p>專案實作　<span class="badge"><?php echo ($val -> project)?$val -> project:'0'; ?></span></p>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="enter-btn"><a href="<?php echo base_url().'library/'.$val -> library_url?>" class="btn btn-primary" role="button">進入</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? } ?>
                    -->
                    
                </div>
            </div>
		</div>
	</div>
</div><!-- /container -->