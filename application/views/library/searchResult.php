<style>
.secondary-heading {margin-top:0px;}
.secondary-heading h3 {margin:0px; font-size:18px;}
ul.course-block-list {}
ul.course-block-list > li {list-style:none; margin-bottom:30px;}
.course-card-hero:before {content:''; height:100%; display:inline-block; margin-right:-0.25em; vertical-align:middle; z-index:1;}
</style>
<div class="container" style="padding-top:30px;">
	<div class="row">
    	<div class="col-md-3 col-sm-3">
            <!-- load sidebar.php -->
            <? $this->load->view('library/sidebar');?>
        </div>
        <div class="col-md-9 col-sm-9">
        	<!-- load searchbar.php -->
        	<? $this->load->view('library/searchbar');?>
        	<?php $tracks_search = json_decode($searchJobsCategory); ?>
        	<div class="row">
				<?if($tracks_search){?>
					<div class="col-md-12">
						<div class="secondary-heading"><h3>依職務分類</h3></div>
            		</div>
					<?foreach($tracks_search as $key=>$val){?>
						<div class="col-md-4 col-sm-4" style="margin-bottom:30px;">
                    		<div class="track-card" style="border:1px solid #e2e2e2; border-radius:5px;">
                        		<img src="<?php echo base_url()?>assets/img/track/<?php echo $val->pic?>" style="border-radius:4px 4px 0 0; width: 100%" />
                        		<div class="content-meta" style="padding:10px 15px;">
                            		<h4 style="margin:0 0 5px; font-size:1.14em; font-weight:bold;"><?php echo $val->tracks_name?></h4>
                            		<span style="color:#888;"><?php echo($val->total!=0)?$val->total.'門課':'&nbsp;' ?></span>
                        		</div>
                        		<!-- 該學程內是否有課程 -->
                        		<?if($val->total!=0){?>
                        			<!-- 登入 -->
                        			<?if($this->session->userdata('email')){?>
                        				<?for($i=0;$i<count($tracks_id);$i++) {?>
                        					<?if($tracks_id[$i]==$val->tracks_id){?>
                        					<?//if($tracksPassAmount[$i]>0){}?>
                        						<div class="content-actions-container" style="background:#f5f5f5; border-radius:0 0 4px 4px;">
                          							<a href="<?php echo base_url()?>track/trackContent/<?php echo $val->tracks_url?>" style="display:block; padding:15px; color:#999; text-align:center; text-decoration:none;">
                            						<? $sum = ($tracksPassAmount[$i]/$val->total)*100?>
                            						<div class="pull-left"><?echo $val->total.'/'.$tracksPassAmount[$i]?></div>
                            						<div style="">
                            							<div class="progress" style="margin:10px 0 0 30px; height:10px;">
                                        					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?echo $sum;?>" aria-valuemin="0" aria-valuemax="100" style="width:<?echo $sum;?>%;">
                                            				<span class="sr-only"><?echo $sum;?>% Complete</span>
                                        					</div>
                                    					</div>
                                    				</div>
                            						</a>
                        						</div>
                        					<?}?>
                        				<?}?>
                        			<!-- 未登入 -->
                        			<?}else{?>
                        				<div class="content-actions-container" style="background:#7ac4cc; border-radius:0 0 4px 4px;">
                          					<a href="<?php echo base_url()?>track/trackContent/<?php echo $val->tracks_url?>" style="display:block; padding:15px; color:#fff; text-align:center; text-decoration:none;">
                            				挑戰學程<i class="fa fa-arrow-right" style="font-size:12px; margin-left:5px;"></i>
                            				</a>
                        				</div>
                        			<?}?>
                				<!-- 該學程內無課程，顯示即將上線 -->
                        		<?}else{?>
                        			<div class="content-actions-container" style="background:#f5f5f5; border-radius:0 0 4px 4px;">
                          				<a href="" style="display:block; padding:15px; color:#999; text-align:center; text-decoration:none;">
                            			即將上線
                            			</a>
                        			</div>
                        		<?}?>
                    		</div>
                		</div>
        			<?}?>
        		<?}?>
        	</div>
        	
        	<div class="row">
        	<?php $course_search = json_decode($searchCourse); ?>
			<?if($course_search){?>
			<div class="col-md-12"><div class="secondary-heading"><h3>依課程分類</h3></div>
            </div>
			<?foreach($course_search as $key=>$val){?>
			<ul class="course-block-list" style="padding:0;">
				<li class="col-md-6 col-sm-6 col-xs-12">
                        <div class="course-card" style="border:1px solid #e2e2e2;">
                            <a href="<?php echo base_url().'library/'.$val -> library_url.'/'.$val->course_url?>">
                              <div class="col-xs-8 content-meta">
                                  <h3 style="font-size:16px;"><?php echo $val -> course_name; ?></h3>
								  <!-- 該學程內是否有課程 -->
								  <?if($val->total!=0){?>
								  	<!-- 登入 -->
                        			<?if($this->session->userdata('email')){?>
                        				<?for($i=0;$i<count($course_id);$i++) {?>
                        					<?if($course_id[$i]==$val->course_id){?>
                        						<div class="content-actions-container" style="background:#f5f5f5; border-radius:0 0 4px 4px;">
                            						<? $sum = ($coursePassAmount[$i]/$val->total)*100?>
                            						<div class="progress" style="margin:0; height:10px; position:absolute; bottom:30px; right:30px; left:30px;">
                                       					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?echo $sum?>" aria-valuemin="0" aria-valuemax="100" style="width: <?echo $sum?>%;">
                                            				<span class="sr-only"><?echo $sum?>% Complete</span>
                                        				</div>
                                    				</div>
                        						</div>
                        					<?}?>
                        				<?}?>
                        			<?}?>
                        		  <?}?>
                                  <p style="font-size:13px; padding-bottom:10px;">
                                      <?php echo $val -> course_desc; ?>
                                  </p>
                              </div>
                              <div class="col-xs-4 course-card-hero" style="background:#f5f5f5; text-align:center; position:absolute; top:0; bottom:0; right:0;">
                              	 <img src="<?=base_url(); ?>assets/img/badges/<?php echo $val -> course_pic; ?>">
                              </div>
                            </a>
                        </div>
                    </li>
                </ul>
             <?}?>
             <?}?>
             </div>
			 <?if(!$tracks_search && !$course_search){?>
				<div class="row">
				<div class="col-md-12">
                	<div class="alert alert-gray">
                		<p>您搜尋的關鍵字<strong><?echo $search_string?></strong>尚無資料！請再換一個關鍵字搜尋看看唷!</p>
                        <p>您也可至其他 <strong>職務分類</strong>或<strong>課程分類</strong> 找出最適合您的學程挑戰！</p>
                    </div>
                </div>
                </div>
			 <?}?>
        </div>
    </div>
</div>