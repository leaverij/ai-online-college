<!-- 左邊區塊 -->
<?php $this -> load -> view('library/sidebar'); ?>
<!-- 左邊end -->
<div class="col-md-9">
	<!-- 右邊區塊 -->
	<?php $row_search = json_decode($search); ?>
	<?if($row_search){?>
		<h4 style="color:#a7a7a7;" class="text-center">課程搜尋：<?echo $search_string;?>(共<?echo count($row_search)?>筆結果)</h4>
	<?foreach($row_search as $key=>$val){?>
	<div class="row">
		<div style="margin-bottom:50px;" class="col-xs-12 col-sm-12 col-md-12">
			<div class="row">
				<div style="text-align: center; margin-top: 20px;" class="col-xs-4 col-md-2">
					<img src="<?=base_url(); ?>assets/img/badges/<?php echo $val -> course_pic; ?>" width="80">
				</div>
				<div class="col-xs-4 col-md-8">
					<h3><?php echo $val -> course_name; ?></h3>
						<?php echo $val -> course_desc; ?>
				</div>
				<div style="text-align: center; margin-top: 20px;" class="col-xs-4 col-md-2">
					<p class="text-left">
						<a href="<?php echo base_url().'library/'.$val -> library_url.'/'.$val->course_url?>" class="btn btn-primary" role="button">進入</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<?}?>
	<?}else{?>
		<h4 style="color:#a7a7a7;" class="text-center">課程搜尋：<?echo $search_string;?>(共0筆結果)</h4>
	<?}?>
	<!-- 右邊end -->
</div>
</div>
</div><!-- /container -->