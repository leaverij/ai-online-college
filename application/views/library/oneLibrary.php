<style>
	ul.course-block-list {padding:0;}
	ul.course-block-list > li {list-style:none; margin-bottom:30px;}
	.course-card-hero:before {content:''; height:100%; display:inline-block; margin-right:-0.25em; vertical-align:middle; z-index:1;}
</style>
<div class="container" style="padding-top:30px;">
	<div class="row">
    	<div class="col-md-3 col-sm-3 sidebar">
            <!-- load sidebar.php -->
            <?php $this->load->view('library/sidebar'); ?> 
        </div><!-- /sidebar-->
		<div class="col-md-9 col-sm-9">
			<!-- load searchbar.php -->
        	<? $this->load->view('library/searchbar');?>
			<!-- 右邊區塊 -->
			<?php $row2 = json_decode($oneLibraryByKnowledge); ?>
			<?php $courseCategory = json_decode($getCourseCategory); ?>
			<?php $preferentialPrice = json_decode($getPreferentialPrice); ?>
			<?//php $row3 = json_decode($numOfCourseBadges); ?>
			<?//php $row4 = json_decode($numOfUserCourseBadges); ?>
			<?foreach($row2 as $key=>$val){?>
				<?if($key =='0'){?>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12" style="margin-bottom:30px;">
							<h2 style="margin:0 0 8px 0; font-size:1.71em;"><?php echo $val -> library_name; ?></h2>
							<p style="font-size: 1.14em; color: #666666;"><?php echo $val -> library_desc; ?>
						</div>
					</div>
				<?}?>
			<?}?>
		<?//foreach($row2 as $key=>$val){?>
		<?//}?>
	<!-- <h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;"> 
    	知識類 <span style="font-size: 55%; vertical-align: middle; background-color: #c3c3c3;" class="label label-default"><?php echo ($val->course_type)?$key+1:'0'; ?></span>
    </h3> -->
	<?//foreach($row2 as $key=>$val){?>
		<!-- 如果course沒有值表示連一個課程都沒有，則顯示目前尚無課程喔!  -->
		<?if($val->course_id){?>
			<?//php $data=array('row2'=>$row2,'row3'=>$row3,'row4'=>$row4);?>
			<?//php $this->load->view('library/courseProgress',$data);?>
            <div class="row">
            	<?foreach($courseCategory as $key=>$courseCategoryVal){?>
				<?php echo (count($courseCategory)>1)?'<div class="col-md-6 col-sm-6 col-xs-12 '.$courseCategoryVal->course_category_name.'">':'<div class="'.$courseCategoryVal->course_category_name.'">';?>
            	<ul class="course-block-list">
            		<?foreach($row2 as $key=>$val){?>
            			<?if($courseCategoryVal->course_category_id==$val->course_category_id){?>
                  			<?php echo (count($courseCategory)>1)?'<li>':'<li class="col-md-6 col-sm-6 col-xs-12">';?>
                        	<div class="course-card" style="border:1px solid #d5d5d5;">
                            	<a href="<?php echo base_url().'library/'.$val -> library_url.'/'.$val->course_url?>">
                              		<div class="col-xs-8 content-meta">
                                  		<h3 style="font-size:16px;"><?php echo $val -> course_name; ?></h3>
								  		<!-- 登入 -->
								  		<?if($this->session->userdata('email')){?>
                        					<div class="content-actions-container" style="position:absolute; bottom:30px; right:30px; left:30px;">
                        						<?foreach($preferentialPrice as $preferentialPriceVal){?><!--顯示libray_url底下的所有課程-->
                        							<?php if($preferentialPriceVal->course_id==$val -> course_id){?>
                        								<?if($preferentialPriceVal->live_flag){?>
                        									<?for($i=0;$i<count($course_id);$i++) {?>
								  								<?if($course_id[$i]==$val->course_id){?>
								  									<? $sum = ($coursePassAmount[$i]/$val->total)*100?>
								  									<div class="progress" style="margin:0; height:10px;">
								  										<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?echo $sum?>" aria-valuemin="0" aria-valuemax="100" style="width: <?echo $sum?>%;">
								  											<span class="sr-only"><?echo $sum?>% Complete</span>
                                        								</div>
                                    								</div>
                        										<?}?>
                        									<?}?> 
                        								<?}else{?>
                        									<?if($preferentialPriceVal->preferential_id){?>
                        										<p><?php echo date("Y-m-d", strtotime($preferentialPriceVal->start_time));?>～<?php echo date("Y-m-d", strtotime($preferentialPriceVal->end_time));?></p>
	                        									限時優惠價  <?php echo 'NT '.$preferentialPriceVal -> preferential_price.' 元'; ?>
	                        									<p><del><small>原價NT <?echo $val -> course_price;?>元</small></del></p>
                        									<?}else{?>
                        										<?php echo $val -> course_price == 0 ?'免費課程':'NT '.$val -> course_price.' 元'; ?>
                        									<?}?>
                        								<?}?>
                        							<?}?>
                        						<?}?>
                        					</div>
                        		  		<?}else{?>
                        					<div class="content-actions-container" style="position:absolute; bottom:30px; right:30px; left:30px;">
                        						<?foreach($preferentialPrice as $preferentialPriceVal){?>
                        							<?php if($preferentialPriceVal->course_id==$val -> course_id){?>
                        								<?if($preferentialPriceVal->preferential_id){?>
                        									<p><?php echo date("Y-m-d", strtotime($preferentialPriceVal->start_time));?>～<?php echo date("Y-m-d", strtotime($preferentialPriceVal->end_time));?></p>
	                        								限時優惠價  <?php echo 'NT '.$preferentialPriceVal -> preferential_price.' 元'; ?>
	                        								<p><del><small>原價NT <?echo $val -> course_price;?>元</small></del></p>
                        								<?}else{?>
                        									<?php echo $val -> course_price == '0' ?'免費課程':'NT '.$val -> course_price.' 元'; ?>
                        								<?}?>
                        							<?}?>
                        						<?}?>
                        					</div>
                        		  		<?}?>
                        				<!-- <p style="font-size:13px; padding-bottom:10px;">
                        					<?php echo $val -> course_desc; ?>
                        				</p> -->
                        			</div>
                        			<div class="col-xs-4 course-card-hero" style="background:#f2f2f2; text-align:center; position:absolute; top:0; bottom:0; right:0;">
                        				<img src="<?=base_url(); ?>assets/img/badges/<?php echo $val -> course_pic; ?>">
                        			</div>
                        		</a>
                    		</div>
                			</li>
                  		<?}?>
					<?}?>
				</ul>
          	</div>
     	<?}?>
  		</div>
		<?}else{?>
			<div class="upcoming-releases">
				<p>目前尚無課程喔! 就從左邊的 <strong>課程分類</strong> 開始學習吧!</p>
				<p>您也可至其他 <strong>職務分類</strong> 找出最適合您的學程挑戰，成為職場上搶手的好人才!</p>
			</div>
		<?}?>
	<?//}?>
	<?//php $row2 = json_decode($oneLibraryByProject); ?>
	<?//foreach($row2 as $key=>$val){?>
	<?//}?>
    
    <!-- <div>
	<h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">
    	專案類 <span style="font-size: 55%; vertical-align: middle; background-color: #c3c3c3;" class="label label-default"><?php echo ($val->course_type)?$key+1:'0'; ?></span>
    </h3> -->
		<?//if($val->course_id){?>
			<?//php $data=array('row2'=>$row2,'row3'=>$row3,'row4'=>$row4);?>
			<?//php $this->load->view('library/courseProgress',$data);?>
		<?//}else{?>
			<!-- <h4 style="color:#a7a7a7;" class="text-center">專案課程製作中，敬請期待</h4> -->
		<?//}?>
    <!-- </div> -->
    
	<!-- 右邊end -->
</div>
</div>
</div><!-- /container -->