<style>
.search-bar ::-webkit-input-placeholder {color:#bcbcbc; font-size:14px; line-height:1.8em;/* WebKit browsers */}
.search-bar .search-text:focus::-webkit-input-placeholder {color:#e3e3e3; transition:ease 0.1s; -webkit-transition:ease 0.1s; -moz-transition:ease 0.1s; -ms-transition:ease 0.1s; font-size:14px; line-height:1.8em;}
.search-bar :-moz-placeholder {color:#bcbcbc; font-size:14px; line-height:1.8em;/* Mozilla Firefox 4 to 18 */}
.search-bar ::-moz-placeholder  {color:#bcbcbc; font-size:14px; line-height:1.8em;/* Mozilla Firefox 19+ */}
.search-bar :-ms-input-placeholder {color:#bcbcbc; font-size:14px; line-height:1.8em;/* Internet Explorer 10+ */}
</style>
<form class="search-bar" action="<?php echo base_url().'library/search'?>" method="post" role="search" style="margin-bottom:30px;">
	<input type="hidden" name="action" value="search"/>
	<div class="input-group input-group-lg">
		<input  type="text" class="form-control" name="search_string" value="<?php echo (isset($search_string))?$search_string:'';?>" placeholder="今天想要找甚麼課程呢？"/>
		<div class="input-group-btn">
			<button type="submit" class="btn btn-default search-btn" tabindex="-1">
				<span class="glyphicon glyphicon-search" style="color:#999;"></span>
			</button>
		</div>
	</div>
</form>
