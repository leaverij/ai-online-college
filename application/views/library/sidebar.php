<!--<div class="content-sort">
	<div class="">
		<a class="navbar-toggle" data-toggle="collapse" data-target="#tracks-sort" style="float:none; position:relative; text-decoration:none; border-top:1px solid #cfe4e5; margin-top:-1px; border-radius:0 0 5px 5px;">
			<span>依職務分類</span>
			<span class="sr-only">Toggle navigation</span>
    	</a>
	</div>
	<div id="tracks-sort" class="collapse navbar-collapse" style="padding:0;">
		<? $jobs = json_decode($getAllJobsCategory); ?>
		<ul id="tracks-sort-container" class="nav nav-stacked navbar-collapse">
			<li>
				<h4 class="tracks-nav-side"><a>依職務分類</a></h4>
				<ul class="nav-list nav">
          		<?php foreach($jobs as $val){?>
					<li class="<?php echo $val -> jobs_category_url; ?>">
						<a href="<?php echo base_url().'track/domain/'.$val -> jobs_category_url?>"><?php echo $val -> jobs_category_name;?></a>
					</li>
				<?}?>
          		</ul>
        	</li>
        </ul>
   	</div>
   	
   	<div class="">
		<a class="navbar-toggle" data-toggle="collapse" data-target="#topics-sort" style="float:none; position:relative; text-decoration:none; border-top:1px solid #cfe4e5; margin-top:-1px; border-radius:0 0 5px 5px;">
			<span>依課程分類</span>
			<span class="sr-only">Toggle navigation</span>
    	</a>
	</div>
	<div id="topics-sort" class="collapse navbar-collapse" style="padding:0;">
		<? $row = json_decode($getAll); ?>
		<ul id="topics-sort-container" class="nav nav-stacked navbar-collapse">
			
			<li>
				<h4 class="topic-nav-side"><a>依課程分類</a></h4>
				<ul class="nav-list nav">
          		<?php foreach($row as $val){?>
					<li class="<?php echo $val -> library_url; ?>">
						<a href="<?php echo base_url().'library/'.$val -> library_url?>"><?php echo $val -> library_name; ?>
						<span class="badge" style="margin-left:5px;"><?php echo ($val -> deep_dives)?$val -> deep_dives:'0'?></span> </a>
					</li>
				<?}?>
          		</ul>
        	</li>
        </ul>
   	</div>
</div>-->

<style type="text/css">
.sort-content {/*padding:0 15px;*/ border-right:1px solid #ddd; text-align:right;}
#sidebar-collapse .navbar-collapse {max-height:100%;}
.form-group h4 {margin:0; padding:0 15px 5px 0; font-size:1.14em; color:#71bec6;}
.form-group h4 > a {color:#71bec6; text-decoration:none;}
.sort-content .nav a {padding:5px 15px; color:#666; position:relative;}
.sort-content .nav a:hover, .sort-content .nav a:focus {background:none; color:#2c3947; font-weight:bold;}
.sort-content .nav a:hover:before {content:''; width:2px; height:30px; background-color:#279ca7; position:absolute; right:-15px; top:0;}
.sort-content .nav li.active a {background:none; color:#2c3947; font-weight:bold;}
.sort-content .nav li.active a:before {content:''; width:2px; height:30px; background-color:#279ca7; position:absolute; right:-15px; top:0;}
.sidebar-toggle {display:none;}
/*
#sidebar-collapse ul li h4 {font-size:16px;}
#sidebar-collapse ul li h4 a:hover, #sidebar-collapse ul li.active a{text-decoration:none; background:#eeeeee; color:#24bbc9;}
#sidebar-collapse ul li a {color:#666; font-size:15px;}
#sidebar-collapse ul li a:hover {color:#24bbc9;}
#sidebar-collapse .library-list li a {color:#888;}
*/
@media screen and (max-width: 768px) {
	.sidebar-toggle-head {display:none;}
	.sidebar-toggle {display:block;}
}
</style>
	<div id="sidebar-collapse" class="" style="padding:0;">
		<div class="sort-content">
			<div id="tracks-sort" class="form-group">
            	<h4 class="sidebar-toggle">
					<a data-toggle="collapse" data-parent="#tracks-sort" href="#tracks-sort-container">依職務分類</a>
				</h4>
            	<h4 class="sidebar-toggle-head">
				依領域分類<!-- 依職務分類 -->
				</h4>
				<? $jobs = json_decode($getAllJobsCategory); ?>
				<ul id="tracks-sort-container" class="nav navbar-collapse collapse">
					<?php foreach($jobs as $val){?>
					<li class="<?php echo $val -> jobs_category_url; ?>">
						<a href="<?php echo base_url().'track/domain/'.$val -> jobs_category_url?>"><?php echo $val -> jobs_category_name;?></a>
					</li>
					<?}?>
				</ul>
			</div>
			<div id="topics-sort" class="form-group">
				<h4 class="sidebar-toggle">
                	<a data-toggle="collapse" data-parent="#topics-sort" href="#topics-sort-container">依課程分類</a>
                </h4>
				<h4 class="sidebar-toggle-head">
                	依課程分類
                </h4>
				<? $row = json_decode($getAll); ?>
				<ul id="topics-sort-container" class="nav navbar-collapse collapse">
					<?php foreach($row as $val){?>
					<li class="<?php echo $val -> library_url; ?>">
						<a href="<?php echo base_url().'library/'.$val -> library_url?>"><?php echo $val -> library_name; ?>
						<span class="badge" style="margin-left:5px;"><?php echo ($val -> deep_dives)?$val -> deep_dives:'0'?></span> </a>
					</li>
					<?}?>
				</ul>
			</div>
		</div>
	</div><!-- /sidebar-collapse-->