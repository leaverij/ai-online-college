<?php $listPublic = json_decode($listPublic);?>
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">Explore Datasets</h1>
        </div>
        
        <div class="col-md-12">
            <?php if(empty($listPublic)){?>
            目前沒有公開的datasets，上傳一個供大家使用吧！
            <?}else{?>
                <div class="row">
                <?php foreach($listPublic as $key=>$value){?>
                    <div class="col-md-6">
                    <div style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd;">
                        <h4><a href="<?=base_url()?><?echo $value->alias;?>/datasets/<?php echo $value->datasets_name;?>">
                            <?echo $value->alias;?>/datasets/<?php echo $value->datasets_name;?></a> <?php echo ($value->open==1)?'<span class="glyphicon glyphicon-globe" aria-hidden="true">':'<span class="glyphicon glyphicon-globe" aria-hidden="true">';?></h4>
                        <h5><?php echo ($value->datasets_desc)?$value->datasets_desc:"沒有提供描述";?></h5>
                        <span>上傳時間：<?php echo $value->create_date;?></span>
                    </div>
                    </div>
                <?}?>
                </div>
            <?}?>
        </div>
    </div>
</div>