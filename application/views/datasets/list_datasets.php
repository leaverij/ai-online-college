<link href="<?=base_url();?>assets/js/fine-uploader/fine-uploader-new.css" rel="stylesheet">
<script src="<?=base_url();?>assets/js/fine-uploader/fine-uploader.js"></script>
<script src="<?=base_url();?>assets/js/timeago.js"></script>
<script src="<?=base_url();?>assets/js/clipboard/dist/clipboard.min.js"></script>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="padding-top: 5px">
            <?if($self){?>
                <a class="" href="<?php echo base_url().'datasets'?>" ><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> 返回Datasets</a>
            <?}else{?>
                <a class="" href="<?php echo base_url().'datasets/explore'?>" ><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> 返回Explore Datasets</a>
            <?}?>
        </div>
        
        <div class="col-md-12">
            <div class="well">
                <span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
                <a href="<?echo current_url()?>">  <?echo urldecode(uri_string());?></a> | <?php echo $totalsize;?> | 上次修改：<span class="need_to_be_rendered" data-timeago="<?echo $filemtime;?>"></span>
                <span class="pull-right" style="padding-left: 5px;padding-right:5px">
                    <a class="btn btn-default " href="<?php echo base_url().'datasets/dl/'.$alias.'/'.$decode_datasets_name?>" role="button">下載</a>
                </span>
                <?if($self){?>
                <span class="pull-right" style="padding-left: 5px;padding-right:5px">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm">刪除Datasets</button>
                </span>
                <span class="pull-right" style="padding-left: 5px;padding-right:5px">
                    <button type="button" class="btn btn-primary" id="uploadData" >上傳檔案
                    </button>
                </span>
                <?}?>
            </div>
            
            <?php if(count($datasets_info)>0){?>
                <table class="table table-bordered table-hover">
                    <?php foreach($datasets_info as $value){?>
                        <tr>
                            <td>
                                <a href="<?echo base_url().$alias.'/datasets/'.$decode_datasets_name.'/'.$value["filename"]?>">
                                    <span class="glyphicon glyphicon-file" aria-hidden="true"></span>  
                                    <?echo $value["filename"]?>
                                </a>
                                <div class="pull-right tooltip1">
                                    <button type="button" class="btn btn-primary btn-xs" data-url="<?php echo base_url().'assets/datasets/'.$alias.'/'.$datasets_name.'/'.urlencode($value["filename"])?>"> 複製url </button>
                                </div>
                            </td>
                            <td>
                                <?echo $value["filesize"]?>
                                
                            </td>
                        </tr>
                    <?}?>
                </table>
            <?}else{?>
                datasets內沒有上傳任何檔案
            <?}?>
        </div>
    </div>
</div>

<!-- Modal -->
<?if($self){?>
<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">上傳檔案</h4>
        </div>
        <div class="modal-body">
            <div class="alert alert-success" role="alert">
                您最多可以一次上傳10個副檔名為['ipynb','jpg','png','csv','zip','txt','jpeg']的檔案，請注意，單一檔案不能超過5Mb。
            </div>
            <script type="text/template" id="qq-template-validation">
            <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="拖曳檔案到這邊">
                <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
                </div>
                <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                    <span class="qq-upload-drop-area-text-selector"></span>
                </div>
                <div class="qq-upload-button-selector qq-upload-button">
                    <div>選擇檔案</div>
                </div>
                <span class="qq-drop-processing-selector qq-drop-processing">
                    <span>Processing dropped files...</span>
                    <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
                </span>
                <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                    <li>
                        <div class="qq-progress-bar-container-selector">
                            <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                        </div>
                        <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                        <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                        <span class="qq-upload-file-selector qq-upload-file"></span>
                        <span class="qq-upload-size-selector qq-upload-size"></span>
                        <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">取消</button>
                        <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">重試</button>
                        <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">刪除</button>
                        <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                    </li>
                </ul>

                <dialog class="qq-alert-dialog-selector">
                    <div class="qq-dialog-message-selector"></div>
                    <div class="qq-dialog-buttons">
                        <button type="button" class="qq-cancel-button-selector">關閉</button>
                    </div>
                </dialog>
                <dialog class="qq-confirm-dialog-selector">
                    <div class="qq-dialog-message-selector"></div>
                    <div class="qq-dialog-buttons">
                        <button type="button" class="qq-cancel-button-selector">否</button>
                        <button type="button" class="qq-ok-button-selector">是</button>
                    </div>
                </dialog>
                <dialog class="qq-prompt-dialog-selector">
                    <div class="qq-dialog-message-selector"></div>
                    <input type="text">
                    <div class="qq-dialog-buttons">
                        <button type="button" class="qq-cancel-button-selector">取消</button>
                        <button type="button" class="qq-ok-button-selector">是</button>
                    </div>
                </dialog>
            </div>
        </script>
        <div id="fine-uploader-validation"></div>
        <script>
        var restrictedUploader = new qq.FineUploader({
            element: document.getElementById("fine-uploader-validation"),
            template: 'qq-template-validation',
            request: {
                endpoint: '<?php echo base_url().'datasets/upload/'.$decode_datasets_name?>'
            },
            thumbnails: {
                placeholders: {
                    waitingPath: '<?=base_url();?>assets/js/fine-uploader/placeholders/waiting-generic.png',
                    notAvailablePath: '<?=base_url();?>assets/js/fine-uploader/placeholders/not_available-generic.png'
                }
            },
            validation: {
                allowedExtensions: ['jpeg', 'jpg', 'txt','png','csv','zip','ipynb'],
                itemLimit: 20,
                sizeLimit: 5000000 // 5 MiB 接近5 Mb
            },
            // These are all the DEFAULT values...
            failedUploadTextDisplay: {
                mode: 'custom',
                responseProperty: 'error',
            },
            messages:{
                typeError:'不支援{file}此副檔名. 請上傳以下格式: {extensions}.'
            }
        });
        </script>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
        </div>
        </div>
    </div>
</div>
<?}?>

<?if($self){?>
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">刪除Datasets</h4>
      </div>
      <div class="modal-body">
        請注意，刪除後，資料將無法取得。
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button class="btn btn-danger pull-right" type="button" id="delete-datasets">刪除</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?}?>
<script>
// local
var test_local_dict = function(number, index, total_sec) {
    return [
    ['剛剛', '片刻後'],
    ['%s秒前', '%s秒後'],
    ['1分鐘前', '1分鐘後'],
    ['%s分鐘前', '%s分鐘後'],
    ['1小時前', '1小時後'],
    ['%s小時前', '%s小時後'],
    ['1天前', '1天後'],
    ['%s天前', '%s天後'],
    ['1週前', '1週後'],
    ['%s週前', '%s週後'],
    ['1月前', '1月後'],
    ['%s月前', '%s月後'],
    ['1年前', '1年後'],
    ['%s年前', '%s年後']
    ][index];
};
timeago.register('test_local', test_local_dict);
var timeagoInstance = timeago();
timeagoInstance.render(document.querySelectorAll('.need_to_be_rendered'), 'test_local');
</script>
<script>
var clipboard = new ClipboardJS('.btn', {
    text: function(trigger) {
        return trigger.getAttribute('data-url');
    }
});

clipboard.on('success', function(e) {
    console.info('Action:', e.action);
    console.info('Text:', e.text);
    console.info('Trigger:', e.trigger);
    e.clearSelection();
});

clipboard.on('error', function(e) {
    console.error('Action:', e.action);
    console.error('Trigger:', e.trigger);
});
</script>