<script src="<?=base_url();?>assets/js/timeago.js"></script>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="padding-top: 5px">
             <a class="" href="<?echo base_url().$user_alias.'/datasets/'.$datasets_name?>" ><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> 返回 <?php echo urldecode($datasets_name)?></a>
        </div>
        <div class="col-md-12">
            <div class="well">
                <span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
                <a href="<?echo base_url().$user_alias.'/datasets/'.$datasets_name?>"> <?php echo $user_alias?>/datasets/<?php echo urldecode($datasets_name)?> </a> | 上次修改：<span class="need_to_be_rendered" data-timeago="<?echo $filemtime;?>"></span>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading"><?php echo urldecode($file);?></div>
                <div class="panel-body">
                    <?php if($ext=="img"){?>
                        <img src="<?php echo $display_url?>" alt="picture"  width="50%" style="margin:0px auto;display:block" />
                    <?}else{?>
                        <?php $url = base_url().'assets/datasets/'.$user_alias.'/'.$datasets_name.'/'.$file;
                            $file = fopen($url, "r") or exit('error');;
                            $data = '';
                            while(!feof($file)) {
                                $data .=  fgets($file); 
                            }
                            fclose($file);
                            echo "<pre>".htmlspecialchars($data)."</pre>";
                        ?>
                    <?}?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
// local
var test_local_dict = function(number, index, total_sec) {
    return [
    ['剛剛', '片刻後'],
    ['%s秒前', '%s秒後'],
    ['1分鐘前', '1分鐘後'],
    ['%s分鐘前', '%s分鐘後'],
    ['1小時前', '1小時後'],
    ['%s小時前', '%s小時後'],
    ['1天前', '1天後'],
    ['%s天前', '%s天後'],
    ['1週前', '1週後'],
    ['%s週前', '%s週後'],
    ['1月前', '1月後'],
    ['%s月前', '%s月後'],
    ['1年前', '1年後'],
    ['%s年前', '%s年後']
    ][index];
};
timeago.register('test_local', test_local_dict);
var timeagoInstance = timeago();
timeagoInstance.render(document.querySelectorAll('.need_to_be_rendered'), 'test_local');
</script>

