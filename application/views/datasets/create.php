<div class="container">
    <div class="row">
        <div class="col-md-10">
            <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">新增Datasets</h1>
        </div>
        
        <div class="col-md-10">
            <form id="create_datasets" method="post" action="<?php echo base_url().'datasets/create'?>">
                <div class="form-group">
                    <label for="datasets_name" class="control-label">Datasets 名稱</label>
                    <input type="text" class="form-control" id="datasets_name" name="datasets_name" placeholder="Datasets 名稱" >
                </div>
                <div class="form-group">
                    <label for="datasets_desc">Datasets 描述</label>
                    <input type="text" class="form-control" id="datasets_desc" name="datasets_desc" placeholder="Datasets描述">
                </div>
                <div class="form-group">
                    <label for="">公開</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="open" id="open1" value="1" checked><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Public
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="open" id="open2" value="0" ><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Private
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">建立Datasets</button>
                <a class="btn btn-default" href="<?php echo base_url().'datasets'?>" role="button">返回Datasets</a>
            </form>

        </div>
    </div>
</div>
