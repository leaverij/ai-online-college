<?php $readAll = json_decode($readAll);?>
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">Datasets</h1>
        </div>
        <div class="col-md-2" style="margin:0; padding:20px 0 10px;">

            <a class="btn btn-success" href="<?php echo base_url().'datasets/create'?>" role="button">新增Datasets</a>
        </div>
        <div class="col-md-10">
            <?php if(empty($readAll)){?>
            尚未上傳Datasets，您可以探索公開的Datasets或上傳自己的Datasets
            <?}else{?>
                <?php foreach($readAll as $key=>$value){?>
                    <div style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd;">
                        <h4><a href="<?=base_url()?><?echo $this->session->userdata('alias');?>/datasets/<?php echo $value->datasets_name;?>">
                            <?echo $this->session->userdata('alias');?>/datasets/<?php echo $value->datasets_name;?></a> <?php echo ($value->open==1)?'<span class="glyphicon glyphicon-globe" aria-hidden="true">':'<span class="glyphicon glyphicon-lock" aria-hidden="true">';?></h4>
                        <h5><?php echo ($value->datasets_desc)?$value->datasets_desc:"沒有提供描述";?></h5>
                        <span>上傳時間：<?php echo $value->create_date;?></span>
                    </div>
                <?}?>
            <?}?>
        </div>
    </div>
</div>