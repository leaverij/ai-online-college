<style>
.search-bar{position:relative;}
.search-bar .search-text {display:block; width:100%; padding:15px; color:#666; font-size:14px; background:#f9f9f9; border:none; border-bottom:1px solid #cfe4e5; border-radius:5px 5px 0 0;}
.search-bar ::-webkit-input-placeholder {color:#bcbcbc;/* WebKit browsers */}
.search-bar .search-text:focus::-webkit-input-placeholder {color:#e3e3e3; transition:ease 0.1s; -webkit-transition:ease 0.1s; -moz-transition:ease 0.1s; -ms-transition:ease 0.1s;}
.search-bar :-moz-placeholder {color:#bcbcbc;/* Mozilla Firefox 4 to 18 */}
.search-bar ::-moz-placeholder  {color:#bcbcbc;/* Mozilla Firefox 19+ */}
.search-bar :-ms-input-placeholder {color:#bcbcbc;/* Internet Explorer 10+ */}
.search-bar .search-btn{position:absolute; top:0; right:0; bottom:0; border:none; padding:0 15px; background:none;}
.search-bar .search-btn span{font-size:16px; color:#bcbcbc;}
.sidebar {margin-bottom:30px; background:#f5f5f5; border-radius:5px; box-shadow:0 1px 0 0 #ededed;}
.nav-list li:last-child a{padding-bottom:6px;}
.nav-list li:last-child a:hover {border-radius:0 0 5px 5px;}
.nav-list li > a {padding:5px 25px;}
#sidebar-collapse ul li h4 {font-size:16px;}
#sidebar-collapse ul li a:hover, #sidebar-collapse ul li.active > a{text-decoration:none; background:#eeeeee; color:#24bbc9;}
#sidebar-collapse ul li a {color:#666; font-size:15px;}
#sidebar-collapse ul li a:hover {color:#24bbc9;}
#sidebar-collapse .library-list li a {color:#888;}
@media screen and (max-width: 768px) {
	.sidebar .navbar-toggle {display:block; margin:0; padding:10px 12px; cursor:pointer; color:#666; font-size:16px;}
}
</style>
<div class="container">
	<div style="padding-bottom:20px;">
		<h1 style="margin:0; padding:20px 0 10px; border-bottom:1px solid #ddd; font-size:1.5em;">討論區</h1>
	</div>
</div>
<div class="container">
	<div id="pjax-container" class="row">
		<!-- CATEGORY FILTER -->
        <div class="col-sm-3 col-sm-push-9">
			<div class="enter-btn" style="margin-bottom:20px;">
				<?php if($this->session->userdata('email')){?>
            	<a id="start-discussion" class="btn btn-success tin-can-ask"  data-toggle="modal" data-target="#modal-start">我要發問</a>
            	<?}?>
            </div>
                <div class="sidebar">
                    <form class="search-bar" action="" method="post" role="search">
                        <div class="">
                            <input class="search-text" type="text" name="search_string" placeholder="搜尋">
                            <button type="submit" class="search-btn">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>                    
                        </div>
                    </form>
                    <div id="sidebar-collapse" class="collapse navbar-collapse" style="padding:0;">
                    	<?php $sideBar = json_decode( $sideBar );?>
                    	<?php $last_seg = $this->uri->total_segments()?>
                        <ul class="nav nav-stacked">
                          <li>
                            <a data-pjax href="<?php echo base_url().'forum'?>"><h4 style="margin:0;">全部課程</h4></a>
                            <ul class="nav-list nav">
                            	<?php foreach($sideBar as $val){?>
                                <li class="<?php echo $val -> library_url; ?> <?php echo ($this->uri->segment($last_seg)==$val -> library_url)?'active':''?>">
                            		<a data-pjax href="<?php echo base_url().'forum/domain/'.$val -> library_url?>">
										<?php echo $val -> library_name; ?>
                                	</a>
                            	</li>
                                <?}?>
                            </ul>
                          </li>
                        </ul>
                    </div>
                    <div class="">
                      <a class="navbar-toggle" data-toggle="collapse" data-target="#sidebar-collapse" style="float:none; position:relative; text-decoration:none; border-top:1px solid #cfe4e5; margin-top:-1px; border-radius:0 0 5px 5px;">
                        <span>瀏覽全部</span>
                        <i class="fa fa-eye" style="position:absolute; top:50%; right:0; border:none; margin-top:-8px; padding:0 15px; background:none;"></i>
                        <span class="sr-only">Toggle navigation</span>
                      </a>
                    </div>
                </div>
            
		</div>
        
        <!-- DISCUSSION LIST -->
        <style>
			.discussion-list {list-style:none; padding:0;}
			.discussion-list li {overflow:hidden; padding:15px 0; border-top:1px solid #ededed;}
			.discussion-list li:first-child {border-top:none; padding-top:0;}
			.avatar {
				width:60px;
				height:60px;
				overflow:hidden;
				background-position:center;
				background-size:50px auto !important;
				border-radius:50px;
				border:5px solid #eee;}
			.avatar img {width:60px; height:60px; display:none;}
            .discussion-list .avatar {
				float:left;
				margin-left:-80px;}
			.discussion-meta {padding-left:80px;}
			.discussion-meta h4 {font-size:1.14em;}
			.discussion-meta p {color:#999;}
			.discussion-meta p > strong {color:#999;}
			.discussion-list .reply-count {padding-top:10px; color:#999; float:right; text-align:center;}
			.discussion-list .reply-count strong {font-size:24px;}
        </style>
        <?php $row = json_decode( $getTopic );?>
        
		<div class="col-sm-9 col-sm-pull-3">
			<?if(!empty($row)){?>
            <ul class="discussion-list">
                    <?foreach($row as $val){?>
            	<li>
                	<div class="col-xs-9">
                        <div class="discussion-meta">
                            <div class="avatar" style="background:url(<?=base_url();?>assets/img/user/128x128/<?php echo $val->img;?>)">
                                <img src="<?=base_url();?>assets/img/user/128x128/<?php echo $val->img;?>"/>
                            </div>
                            <h4>
                                <a href="<?php echo base_url().'forum/forumContent/'.$val->forum_topic_id;?>">
                                	<?php echo $val->forum_topic;?>
                                </a>
                            </h4>
                            <p>
                                <?php echo $val->posted_time;?> ‧ 經由 <strong><?php echo $val->alias;?></strong> 提問
                            </p>
                            <p><span class="label label-default"><?php echo $val->library_name;?></span></p>
                        </div>
                    </div>
                    <div class="col-xs-3">
                    	<div class="reply-count" <?php echo ($val->solved=='1'?'style="color:#5fcf80"':'');?>>
                        	<strong><?php echo ($val->solved=='1'?'<i class="fa fa-check"></i>':'');?><?php echo $val->answers;?></strong>
                            <p>回 應</p>
                        </div>
                    </div>
                </li>
                <?}?>
              </ul>
            <!-- Pagination -->
            <div style="margin-top:30px;">
                <ul class="pager">
                  <li class="previous <?php echo ($previous)?'':'disabled'?>"><a href="<?php echo ($previous)?'?page='.$previous:'javascript: void(0)'?>">&larr; 較新</a></li>
                  <li class="next <?php echo ($next)?'':'disabled'?>"><a href="<?php echo ($next)?'?page='.$next:'javascript: void(0)'?>">較舊 &rarr;</a></li>
                </ul>                
            </div>
            <?}else{?>
        	<div class="alert alert-gray">討論區暫時沒有任何問題提問 :)</div>
        <?}?>
        </div>
        
	</div>
</div><!-- /container -->
<?php if($this->session->userdata('email')){?>
	<?$this->load->view('forum/modalStartDiscuss');?>
<?}?>
<!-- XAPI -->
<script language="JavaScript" src="<?=base_url();?>assets/js/includeXAPI.js"></script>
<script>
    if(name){//檢查是否有login
        var initTime = new Date();
        var email = 'mailto:'+"<?=$this->session->userdata('email');?>";
        var accessType = "http://activitystrea.ms/schema/1.0/page";
        var agent = new ADL.XAPIStatement.Agent(email,name);
        var contentVerb = ADL.verbs.accessed;
        var authority = {"account":{"homePage":"http://140.92.88.88:80/mLRSTest","name":'2AEB66648CC123B8566D'},"name":"learninghouse","objectType":"Agent"};
        var activityId = "<?=base_url();?>forum";
        var accessCourseTag ="discussion";
        var activity = new ADL.XAPIStatement.Activity(activityId,accessCourseTag,accessCourseTag,accessType);
        var context = new ADL.XAPIStatement.Context(
            '',
            {"objectType":"Agent","mbox":'mailto:learninghouse.service@gmail.com',"name":'learninghouse'},
            '',{
                "parent":[{
                    "id":activityId,"objectType":"Activity",
                    "definition":{
                        "name":{"en-US":accessCourseTag},
                        "description":{"en-US":accessCourseTag},
                        "type":"http://activitystrea.ms/schema/1.0/page"
                    }
                }]
               },
            '','','','','');
        var leaveTime = new Date();
        var lastTime = leaveTime-initTime;
        var result = new ADL.XAPIStatement.Result('','','','','',{"<?=base_url();?>forum/time":lastTime});
        var stmt = new ADL.XAPIStatement(agent,contentVerb,activity,context,result,authority);
        stmt.generateId();
        var json_stmt = JSON.stringify(stmt);
        //console.log(JSON.stringify(stmt));
        
        //console.log(json);
        
        // ADL.XAPIWrapper.changeConfig({
            // 'endpoint': 'https://140.92.88.88/mLRSTest/TCAPI/',
                // 'user': '2AEB66648CC123B8566D',
            // 'password': 'CACA3D203627B194301B'
            // });
        // ADL.XAPIWrapper.sendStatement(stmt);
    }   
</script>