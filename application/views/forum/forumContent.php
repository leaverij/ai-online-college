<style>
	.search-bar{position:relative;}
	.search-bar .search-text {display:block; width:100%; padding:15px; color:#666; font-size:14px; background:#f9f9f9; border:none; border-bottom:1px solid #cfe4e5; border-radius:5px 5px 0 0;}
	.search-bar ::-webkit-input-placeholder {color:#bcbcbc;/* WebKit browsers */}
	.search-bar .search-text:focus::-webkit-input-placeholder {color:#e3e3e3; transition:ease 0.1s; -webkit-transition:ease 0.1s; -moz-transition:ease 0.1s; -ms-transition:ease 0.1s;}
	.search-bar :-moz-placeholder {color:#bcbcbc;/* Mozilla Firefox 4 to 18 */}
	.search-bar ::-moz-placeholder  {color:#bcbcbc;/* Mozilla Firefox 19+ */}
	.search-bar :-ms-input-placeholder {color:#bcbcbc;/* Internet Explorer 10+ */}
	.search-bar .search-btn{position:absolute; top:0; right:0; bottom:0; border:none; padding:0 15px; background:none;}
	.search-bar .search-btn span{font-size:16px; color:#bcbcbc;}
	.sidebar {margin-bottom:30px; background:#f5f5f5; border-radius:5px; box-shadow:0 1px 0 0 #ededed;}
	.nav-list li:last-child a{padding-bottom:6px;}
	.nav-list li:last-child a:hover {border-radius:0 0 5px 5px;}
	.nav-list li > a {padding:5px 25px;}
	#sidebar-collapse ul li h4 {font-size:16px;}
	#sidebar-collapse ul li h4 a:hover, #sidebar-collapse ul li.active a{text-decoration:none; background:#eeeeee; color:#24bbc9;}
	#sidebar-collapse ul li a {color:#666; font-size:15px;}
	#sidebar-collapse ul li a:hover {color:#24bbc9;}
	#sidebar-collapse .library-list li a {color:#888;}
	@media screen and (max-width: 768px) {
		.sidebar .navbar-toggle {display:block; margin:0; padding:10px 12px; cursor:pointer; color:#666; font-size:16px;}
	}
	
	.discussion-question {padding-left:80px;}
	.discussion-question .avatar {
		float:left;
		margin-left:-80px;}
	.discussion-answer .avatar {
		float:left;
		margin-left:-80px;}
		
	.comments-meta .avatar {
		width:50px;
		height:50px;
		background-size:40px auto !important;
		float:left;
		margin-left:-70px;}
	.discussion-question-comment-post .avatar, .discussion-answer-comment-post .avatar {
		width:50px;
		height:50px;
		background-size:40px auto !important;
		float:left;
		margin-left:-70px;}
	.discussion-answer-main.discussion-answer-best {background:#f1fff1; border: 1px solid #d4f1dd; padding:20px 0 20px 95px; margin:0 0 0 -95px; border-radius:5px;}
	.discussion-answer-best .vote-best-answer {color:#5fcf80;}/*原PO角度*/
	.discussion-answer-best .best-answer {color:#5fcf80;}/*其他人角度*/
	.discussion-answer-best .discussion-answer-voting .vote-up a {background:#fff;}
	.discussion-answer-voting, .discussion-question-voting {width:35px;}
	.discussion-answer-voting .vote a, .discussion-question-voting .vote a{display:block; text-align:center; padding:5px 0; color:#898d91; font-size:1.14em; line-height:1em}
	.discussion-answer-voting .vote a i, .discussion-question-voting .vote a i {font-size:1.28em;}
	.discussion-question-voting .vote-up a {background:#fff; border-radius:5px;}
	.discussion-answer-voting .vote-up a {background:#f5f5f5; border-radius:5px;}
	.discussion-answer-voting .vote-up a.voted, .discussion-question-voting .vote-up a.voted {background:#5cb85c; color:#fff;}
	.discussion-answer-voting .vote-down a.voted, .discussion-question-voting .vote-down a.voted {color:#d9534f;}
	.discussion-answer-voting .vote-up a:hover, .discussion-question-voting .vote-up a:hover {background:#5cb85c; text-decoration:none; color:#fff;}
	.discussion-answer-voting .vote-down a:hover, .discussion-question-voting .vote-down a:hover {color:#d9534f;} 
	.discussion-answer-voting .vote-count, .discussion-question-voting .vote-count {display:block; text-align:center;}
	
	.discussion-question .time, .discussion-answer .time {font-size:0.71em;}
</style>

<div class="container">
	<div class="breadcrumb">
    	<a href="<?php echo base_url()?>forum">
        	<i class="fa fa-arrow-left"></i> 
            討論區
        </a>
    </div>
</div>
<div class="container">
	<div class="row">
		<!-- CATEGORY FILTER -->
        <div class="col-sm-3 col-sm-push-9">
			<?php if($this->session->userdata('email')){?>
            	<div class="enter-btn" style="margin-bottom:20px;">
            		<a id="start-discussion"class="btn btn-success tin-can-ask"  data-toggle="modal" data-target="#modal-start">我要發問</a>
            	</div>
            <? }?>
            <div class="sidebar">
            	<form class="search-bar" action="" method="post" role="search">
            		<div class="">
            			<input class="search-text" type="text" name="search_string" placeholder="搜尋">
            			<button type="submit" class="search-btn">
            				<span class="glyphicon glyphicon-search"></span>
                        </button>                    
                    </div>
                </form>
                <div id="sidebar-collapse" class="collapse navbar-collapse" style="padding:0;">
                	<?php $sideBar = json_decode( $sideBar );?>
                	<ul class="nav nav-stacked">
                		<li>
                			<h4 style="margin:0; padding:10px 15px;"><a href="<?php echo base_url().'forum'?>">全部課程</a></h4>
                            <ul class="nav-list nav">
                                <?php foreach($sideBar as $val){?>
                                <li class="<?php echo $val -> library_url; ?>">
                            		<a data-pjax href="<?php echo base_url().'forum/domain/'.$val -> library_url?>">
										<?php echo $val -> library_name; ?>
                                	</a>
                            	</li>
                                <?}?>
                            </ul>
                          </li>
                        </ul>
                    </div>
                    <div class="">
                      <a class="navbar-toggle" data-toggle="collapse" data-target="#sidebar-collapse" style="float:none; position:relative; text-decoration:none; border-top:1px solid #cfe4e5; margin-top:-1px; border-radius:0 0 5px 5px;">
                        <span>瀏覽全部</span>
                        <i class="fa fa-eye" style="position:absolute; top:50%; right:0; border:none; margin-top:-8px; padding:0 15px; background:none;"></i>
                        <span class="sr-only">Toggle navigation</span>
                      </a>
                    </div>
                </div>
		</div>
                
		<div class="col-sm-9 col-sm-pull-3">
			<?php $topic = json_decode( $getForumTopic )?>
        	<?php $content = json_decode( $getForumContent )?>
        	<?php $comment = json_decode( $getForumComment )?>
        	<?php if($this->session->userdata('email')){?>
        		<?php $userTopicVote = json_decode( $getUserTopicVote )?>
        		<?php $userContentVote = json_decode( $getUserContentVote )?>
        	<? }?>
			<?// foreach($content as $key =>$val){?>
			<?//if($key=='0'){?>
			<? foreach($topic as $key =>$val){?>
			<!-- DISCUSSION QUESTION / 發問者的問題標題以及問題內容 -->
			<div class="" style="background:#f5f5f5; padding:30px 0 30px 15px; border-radius:5px; box-shadow:0 1px 0 0 #ededed;">
                <div class="discussion-question">
                	<div class="discussion-question-main">
                    	<div class="col-sm-10 col-xs-10">
                            <div class="avatar" style="background:url(<?php echo base_url()?>assets/img/user/128x128/<?php echo $val->img?>); border:5px solid #fff;">
                                <img src="<?php echo base_url()?>assets/img/user/128x128/<?php echo $val->img?>"/>
                            </div>
                            <h4 style="margin:0 0 5px 0; font-size:1.3em; font-weight:600; color:#565656;">
                                <div class="ft<?php echo $val->forum_content_id?> title<?php echo $val->forum_topic_id?>">
                                    <?php echo $val->forum_topic?>
                                </div>                            
                            </h4>
                            <div class="discussion-topic-meta dtm<?php echo $val->forum_content_id?>">
                                <p style="color:#b2b2b2;">
                                    經由 <strong style="color:#787878;"><span class="origin-author alias<?php echo $val->forum_content_id?>"><?php echo $val->alias?></span></strong> 提問 
                                    ‧ <span class="time"><?php echo $val->content_posted_time?></span>
                                </p>
                                <p><span class="label label-default"><?php echo $val->library_name;?></span></p>
                            </div>
                            <div class="question markdown-block">
                                <?//php $forum_content =  getTextArray($val->forum_content);?>
                                <span class="dat<?php echo $val->forum_content_id?>">
                                    <?php echo $this->md->defaultTransform($val->forum_content);?>
                                </span>
                            </div>
                            <div class="show-edit-content<?php echo $val->forum_content_id?>" style="display: none">
                                <form role="form" action="<?php echo base_url()?>forum/updateForumTopic" method="post">
                                <div class="form-group">
                                    <input type="text" name="forum_topic" class="form-control"value="<?php echo $val->forum_topic?>"/>
                                </div>
                                <div class="form-group">
                                    <textarea name="forum_content" class="form-control" rows="10" cols="40"><?php echo $val->forum_content?></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="forum_topic_id" value="<?php echo $val->forum_topic_id?>" /> 
                                    <input type="hidden" name="forum_content_id" value="<?php echo $val->forum_content_id?>" />
                                    <p>您可以使用Markdown來格式化您的問題，如果您不熟悉Markdown的撰寫方式，請參考<a class="show-markdown-cheatsheet" href="javascript: void(0)" data-toggle="modal" data-target="#markdown-modal">Markdown Cheatsheet</a></p>             
                                    <button type="submit" class="btn btn-primary update-topic" style="margin-right:5px;">儲存</button>
                                    <button type="button" class="btn cancel-edit-topic" data-edit-content-id = "<?php echo $val->forum_content_id?>" style="margin-right:5px;">取消</button>
                                </div>
                                </form>
                            </div>
                        </div>
                        <!-- VOTE BLOCK -->
                        <?php if($this->session->userdata('email')){?>
                            <? foreach($userTopicVote as $userTopicVoteVal){?>
                                <? if($userTopicVoteVal->forum_content_id == $val->forum_content_id){?>
                                <div class="col-sm-2 col-xs-2">
                                	<button id="unsubscribeForumTopic" data-button='{"forum_topic_id":"<?php echo $val->forum_topic_id?>","status":"<?php echo ($isTopicSubscribed==1)?0:1;?>"}' class="btn btn-primary" style="margin-bottom:5px;"><?php echo($isTopicSubscribed==1)?'取消訂閱':'加入訂閱';?></button>
                                    <div class="discussion-question-voting pull-right">
                                        <div class="vote vote-up">
                                            <a class="<?php echo ($userTopicVoteVal->vote=='1')?'voted':''?>" data-content="您已經投過票囉！" data-vote-id = "<?php echo $val->forum_content_id?>" data-topic-id = "<?php echo $val->forum_topic_id?>" data-web = "learninghouse" href="javascript: void(0)">
                                                <i class="fa fa-chevron-up"></i>
                                                <strong class="vote-count"><?php echo $val->votes?></strong>
                                            </a>
                                        </div>
                                        <div class="vote vote-down">
                                            <a class="<?php echo ($userTopicVoteVal->vote=='-1')?'voted':''?>" data-content="您已經投過票囉！" data-vote-id = "<?php echo $val->forum_content_id?>" data-topic-id = "<?php echo $val->forum_topic_id?>" data-web = "learninghouse" href="javascript: void(0)">
                                                <i class="fa fa-chevron-down"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <? }?>
                            <? }?>
                        <? }else{?>
                            <div class="col-sm-2 col-xs-2">
                                <div class="discussion-question-voting pull-right">
                                    <div class="vote vote-up">
                                        <a class="" data-vote-id = "<?php echo $val->forum_content_id?>" data-topic-id = "<?php echo $val->forum_topic_id?>" href="javascript: void(0)">
                                            <i class="fa fa-chevron-up"></i>
                                            <strong class="vote-count"><?php echo $val->votes?></strong>
                                        </a>
                                    </div>
                                    <div class="vote vote-down">
                                        <a class="" data-vote-id = "<?php echo $val->forum_content_id?>" data-topic-id = "<?php echo $val->forum_topic_id?>" href="javascript: void(0)">
                                            <i class="fa fa-chevron-down"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <? }?>
                        <!-- /VOTE BLOCK -->
                        <div class="discussion-actions da<?php echo $val->forum_content_id?>" style="clear:both; padding-left:15px; color:#999;">
                            <!--<span><?php echo $val->content_posted_time?></span> ‧--> 
                            <div class="add-commit" style="display:inline-block;">
                                <a class="comment-click" data-comment = "comment<?php echo $val->forum_content_id?>" href="javascript: void(0)">回覆</a>
                                <?php if($this->session->userdata('user_id')==($val->user_id)){?>
                                     ‧ <a class="edit-topic" data-edit-content-id = "<?php echo $val->forum_content_id?>" href="javascript: void(0)">編輯</a>
                                <? }?>    
                            </div>
                            
                        </div>
                    </div>
                    <!-- DISCUSSION QUESTION COMMENTS -->
                    <? foreach($comment as $val2){?>
                        <? if($val->forum_content_id == $val2->forum_content_id){?>
                        <div class="discussion-question-comments" style="margin-top:20px; border-top:1px solid #eee;">
                            <div style="padding:20px 30px 0 0;">
                                <div class="comments-meta" style="padding-left:70px;">
                                    <div class="avatar" style="background:url(<?php echo base_url()?>assets/img/user/128x128/<?php echo $val2->img?>); border:5px solid #fff;">
                                        <img src="<?php echo base_url()?>assets/img/user/128x128/<?php echo $val2->img?>"/>
                                    </div>
                                    <div class="discussion-comments-meta dcm<?php echo $val2->forum_comment_id?>">
                                        <p style="color:#b2b2b2;">
                                            經由 <strong style="color:#787878; margin-bottom:"><?php echo $val2->alias?></strong> 回答 
                                            ‧ <span class="time"><?php echo $val2->comment_posted_time?></span>
                                        </p>
                                    </div>
                                    <div class="discussion-comments-text">
                                        <div class="markdown-block">
                                            <?//php $forum_comment = getTextArray($val2->forum_comment);?>
                                            <span class="dct<?php echo $val2->forum_comment_id?>">
                                                <?php echo $this->md->defaultTransform($val2->forum_comment);?>
                                            </span>
                                        </div>                                
                                    </div>
                                    <div class="show-edit-comment<?php echo $val2->forum_comment_id?>" style="display: none">
                                        <form role="form" action="<?php echo base_url()?>forum/updateForumComment" method="post">
                                        <div class="form-group">
                                            <textarea name="forum_comment" class="form-control" rows="10" cols="40"><?php echo $val2->forum_comment?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="forum_topic_id" value="<?php echo $val->forum_topic_id?>" />
                                            <input type="hidden" name="forum_comment_id" value="<?php echo $val2->forum_comment_id?>" />
                                            <p>您可以使用Markdown來格式化您的問題，如果您不熟悉Markdown的撰寫方式，請參考<a class="show-markdown-cheatsheet" href="javascript: void(0)" data-toggle="modal" data-target="#markdown-modal">Markdown Cheatsheet</a></p>             
                                            <button type="submit" class="btn btn-primary update-comment" style="margin-right:5px;">儲存</button>
                                            <button type="button" class="btn cancel-edit-comment" data-edit-comment-id = "<?php echo $val2->forum_comment_id?>" style="margin-right:5px;">取消</button>
                                        </div>
                                        </form>
                                    </div>
                                    <div class="discussion-actions da<?php echo $val2->forum_comment_id?>" style="clear:both; color:#999;">
                                        <!--<span><?php echo $val2->comment_posted_time?></span>--> 
                                        <div class="add-comments" style="display:inline-block;">
                                            <!-- ‧ <a class="comment-click" data-comment = "comment<?php echo $val->forum_content_id?>" href="javascript: void(0)">回覆</a>-->
                                            <?php if($this->session->userdata('user_id')==($val2->user_id)){?>
                                                <a class="edit-comment" data-edit-comment-id = "<?php echo $val2->forum_comment_id?>" href="javascript: void(0)">編輯</a>
                                            <? }?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <? }?>
                    <? }?>
                    <!-- DISCUSSION QUESTION COMMENTS POST / 題目區塊的回覆(最上面喔) -->
                    <?php if($this->session->userdata('email')){?>
                    <div class="discussion-question-comment-post comment<?php echo $val->forum_content_id?> comment-close" style="margin-top:20px; padding:0 0 0 70px;display: none">
                        <div class="avatar" style="background:url(<?php echo base_url()?>assets/img/user/128x128/<?php echo $this->user_model->getImg($this->session->userdata('email'))?>); border:5px solid #fff;">
                            <img src="<?php echo base_url()?>assets/img/user/128x128/<?php echo $this->user_model->getImg($this->session->userdata('email'))?>"/>
                        </div>
                        <form id="form-leave-comment" role="form" action="<?php echo base_url()?>forum/addForumComment" method="post">
                            <div class="form-group">
                                <textarea id="leave-comment" name="forum_comment" class="form-control" rows="10" placeholder="輸入您要回覆的敘述文字"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="forum_topic_id" value="<?php echo $val->forum_topic_id?>" /> 
                                <input type="hidden" name="forum_content_id" value="<?php echo $val->forum_content_id?>" />
					            <div class="checkbox subscribeEmailCheckbox <?php echo($isTopicSubscribed!=1)?'':'hide';?>">
					               <label>
					                   <input style="color:#999; font-weight:normal;" type="checkbox" name="notify-me" value="1" checked> 有人回應討論串時透過Email通知我
					               </label>
					            </div>  
                                <button type="submit" class="btn btn-primary leave-comment" data-topic-id="<?php echo $val->forum_topic_id?>" data-content-id="<?php echo $val->forum_content_id?>" style="margin-right:5px;">回覆</button>
                                <button type="button" class="btn comment-cancel" style="margin-right:5px;">取消</button>
                                <label style="color:#999; font-weight:normal;">
                                  <!-- <input type="checkbox"> 當有人回應，請透過Email通知我。 -->
                                </label>                     
                            </div>
                        </form>                    
                    </div>  <!-- /discussion-question-comment-post -->   
                    <? }?>  
                </div> <!-- /discussion-question -->
            </div>
            <? }?>
            
            <h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">回應 
            	<?php echo (count($content)=='0')?'':'<span class="label label-default" style="font-size: 55%; vertical-align: middle; background-color: #c3c3c3;">'.(count($content)).'</span>'?>
            </h3>
            
			<?//}else{?>
			
			<!-- DISCUSSION ANSWER / 除了發問者以外的其他問題內容 -->
			<div class="discussion-answer-container" style="margin-bottom:30px;">
            	<? foreach($content as $key =>$val){?>
                <div class="discussion-answer" style="padding:0 0 20px 95px; border-bottom:1px solid #eee; margin-bottom:20px;">
                    <div class="discussion-answer-main<?php echo ($val->best_answer=='1')?' discussion-answer-best':''?>">
                        <div class="col-sm-10 col-xs-10">
                            <div class="avatar" style="background:url(<?php echo base_url()?>assets/img/user/128x128/<?php echo $val->img?>)">
                                <img src="<?php echo base_url()?>assets/img/user/128x128/<?php echo $val->img?>"/>
                            </div>
                            <div class="discussion-answer-meta dam<?php echo $val->forum_content_id?>">
                                <p style="color:#b2b2b2;">
                                    經由 <strong style="color:#787878;"><span class="alias<?php echo $val->forum_content_id?>"><?php echo $val->alias?></span></strong> 回答 
                                    ‧ <span class="time"><?php echo $val->content_posted_time?></span>
                                </p>
                            </div>
                            <div class="discussion-answer-text ">
                                <div class="markdown-block">
                                    <?//php $forum_content =  getTextArray($val->forum_content);?>
                                    <span class="dat<?php echo $val->forum_content_id?>">
                                        <?php echo $this->md->defaultTransform($val->forum_content);?>
                                    </span>
                                </div>                                
                            </div>
                            <div class="show-edit<?php echo $val->forum_content_id?>" style="display: none">
                                <form role="form" action="<?php echo base_url()?>forum/updateForumContent" method="post">
                                <div class="form-group">
                                    <textarea name="forum_content" class="form-control" rows="10" cols="40"><?php echo $val->forum_content?></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="forum_topic_id" value="<?php echo $val->forum_topic_id?>" />
                                    <input type="hidden" name="forum_content_id" value="<?php echo $val->forum_content_id?>" />
                                    <p>您可以使用Markdown來格式化您的問題，如果您不熟悉Markdown的撰寫方式，請參考<a class="show-markdown-cheatsheet" href="javascript: void(0)" data-toggle="modal" data-target="#markdown-modal">Markdown Cheatsheet</a></p>             
                                    <button type="submit" class="btn btn-primary update-content" style="margin-right:5px;">儲存</button>
                                    <button type="button" class="btn cancel-edit-content" data-edit-content-id = "<?php echo $val->forum_content_id?>" style="margin-right:5px;">取消</button>
                                </div>
                                </form>
                            </div>
                        </div>
                        <!-- VOTE BLOCK -->
                        <?php if($this->session->userdata('email')){?>
                            <? foreach($userContentVote as $userContentVoteVal){?>
                                <? if($userContentVoteVal->forum_content_id == $val->forum_content_id){?>
                                <div class="col-sm-2 col-xs-2 vb<?php echo $val->forum_content_id?>">
                                    <div class="discussion-answer-voting pull-right">
                                        <div class="vote vote-up">
                                            <a class="<?php echo ($userContentVoteVal->vote=='1')?'voted':''?>" data-content="您已經投過票囉！" data-vote-id = "<?php echo $val->forum_content_id?>" data-topic-id = "<?php echo $val->forum_topic_id?>" href="javascript: void(0)">
                                                <i class="fa fa-chevron-up"></i>
                                                <strong class="vote-count"><?php echo $val->votes?></strong>
                                            </a>
                                        </div>
                                        <div class="vote vote-down">
                                            <a class="<?php echo ($userContentVoteVal->vote=='-1')?'voted':''?>" data-content="您已經投過票囉！" data-vote-id = "<?php echo $val->forum_content_id?>" data-topic-id = "<?php echo $val->forum_topic_id?>" href="javascript: void(0)">
                                                <i class="fa fa-chevron-down"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <? }?>
                            <? }?>
                        <? }else{?>
                            <div class="col-sm-2 col-xs-2 vb<?php echo $val->forum_content_id?>">
                                <div class="discussion-answer-voting pull-right">
                                    <div class="vote vote-up">
                                        <a class="" data-content="您已經投過票囉！" data-vote-id = "<?php echo $val->forum_content_id?>" data-topic-id = "<?php echo $val->forum_topic_id?>" href="javascript: void(0)">
                                            <i class="fa fa-chevron-up"></i>
                                            <strong class="vote-count"><?php echo $val->votes?></strong>
                                        </a>
                                    </div>
                                    <div class="vote vote-down">
                                        <a class="" data-content="您已經投過票囉！" data-vote-id = "<?php echo $val->forum_content_id?>" data-topic-id = "<?php echo $val->forum_topic_id?>" href="javascript: void(0)">
                                            <i class="fa fa-chevron-down"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <? }?>
                        <!-- /VOTE BLOCK -->
                        <div class="discussion-answer-actions daa<?php echo $val->forum_content_id?>" style="clear:both; padding-left:15px; color:#999;">
                            <!--<span><?php echo $val->content_posted_time?></span> ‧ -->
                            <div class="add-commit" style="display:inline-block;">
                                <a class="comment-click" data-comment = "comment<?php echo $val->forum_content_id?>" href="javascript: void(0)">回覆</a>
                                <?php if($this->session->userdata('user_id')==($val->topic_user)){?>
                                	<?if(((strtotime($this->theme_model->nowTime())-strtotime($val->solved_time))/(60*60))<=1){?>
                                		‧ <a class="wait-an-hour best-answer" href="javascript: void(0)">最佳解答</a>
                                	<?}else{?>
                                		‧ <a class="vote-best-answer" vote-best-answer = "<?php echo $val->forum_content_id?>" data-topic-id = "<?php echo $val->forum_topic_id?>" href="javascript: void(0)">最佳解答</a>
                                	<?}?>
                                	
                                <? }else if($val->best_answer=='1'){?>
                                	‧ <span class="best-answer">最佳解答</span>
                                <? }?>
                                <?php if($this->session->userdata('user_id')==($val->user_id)){?>
                                	‧ <a class="edit-content" data-edit-content-id = "<?php echo $val->forum_content_id?>" href="javascript: void(0)">編輯</a>
                                <?}?>
                            </div>
                        </div>
                    </div>  <!-- /discussion-answer-main -->
                    
                    <!-- DISCUSSION ANSWER COMMENTS -->
                    <? foreach($comment as $val2){?>
                        <? if($val->forum_content_id == $val2->forum_content_id){?>
                        <div class="discussion-answer-comments" style="margin-top:20px; border-top:1px solid #eee;">
                            <div style="padding:20px 30px 0 0;">
                                <div class="comments-meta" style="padding-left:70px;">
                                    <div class="avatar" style="background:url(<?php echo base_url()?>assets/img/user/128x128/<?php echo $val2->img?>)">
                                        <img src="<?php echo base_url()?>assets/img/user/128x128/<?php echo $val2->img?>"/>
                                    </div>
                                    <div class="discussion-comments-meta dcm<?php echo $val2->forum_comment_id?>">
                                        <p style="color:#b2b2b2;">
                                            經由 <strong style="color:#787878; margin-bottom:"><?php echo $val2->alias?></strong> 回答 
                                            ‧ <span class="time"><?php echo $val2->comment_posted_time?></span>
                                        </p>
                                    </div>
                                    <div class="discussion-comments-text">
                                        <div class="markdown-block">
                                            <?//php $forum_comment =  getTextArray($val2->forum_comment);?>
                                            <span class="dct<?php echo $val2->forum_comment_id?>">
                                                <?php echo $this->md->defaultTransform($val2->forum_comment);?>
                                            </span>
                                        </div>                                
                                    </div>
                                    <div class="show-edit-comment<?php echo $val2->forum_comment_id?>" style="display: none">
                                        <form role="form" action="<?php echo base_url()?>forum/updateForumComment" method="post">
                                        <div class="form-group">
                                            <textarea name="forum_comment" class="form-control" rows="10" cols="40"><?php echo $val2->forum_comment?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" name="forum_topic_id" value="<?php echo $val->forum_topic_id?>" />
                                            <input type="hidden" name="forum_comment_id" value="<?php echo $val2->forum_comment_id?>" />
                                            <p>您可以使用Markdown來格式化您的問題，如果您不熟悉Markdown的撰寫方式，請參考<a class="show-markdown-cheatsheet" href="javascript: void(0)" data-toggle="modal" data-target="#markdown-modal">Markdown Cheatsheet</a></p>             
                                            <button type="submit" class="btn btn-primary update-comment" style="margin-right:5px;">儲存</button>
                                            <button type="button" class="btn cancel-edit-comment" data-edit-comment-id = "<?php echo $val2->forum_comment_id?>" style="margin-right:5px;">取消</button>
                                        </div>
                                        </form>
                                    </div>
                                    <div class="discussion-actions da<?php echo $val2->forum_comment_id?>" style="clear:both; color:#999;">
                                        <!--<span><?php echo $val2->comment_posted_time?></span>-->
                                        <div class="add-comments" style="display:inline-block;">
                                            <!-- ‧ <a class="comment-click" data-comment = "comment<?php echo $val->forum_content_id?>" href="javascript: void(0)">回覆</a>-->
                                            <?php if($this->session->userdata('user_id')==($val2->user_id)){?>
                                                <a class="edit-comment" data-edit-comment-id = "<?php echo $val2->forum_comment_id?>" href="javascript: void(0)">編輯</a>
                                            <? }?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <? }?>
                    <? }?>
                    <!-- DISCUSSION ANSWER COMMENTS POST / 回覆 -->
                    <?php if($this->session->userdata('email')){?>
                    <div class="discussion-answer-comment-post comment<?php echo $val->forum_content_id?> comment-close" style="margin-top:20px; padding:0 0 0 70px;display: none">
                        <div class="avatar" style="background:url(<?php echo base_url()?>assets/img/user/128x128/<?php echo $this->user_model->getImg($this->session->userdata('email'))?>)">
                            <img src="<?php echo base_url()?>assets/img/user/128x128/<?php echo $this->user_model->getImg($this->session->userdata('email'))?>"/>
                        </div>
                        <form class="form-leave-comment" role="form" action="<?php echo base_url()?>forum/addForumComment" method="post">
                            <div class="form-group">
                                <textarea id="leave-comment" name="forum_comment" class="form-control" rows="10" placeholder="輸入您要回覆的敘述文字"></textarea>
                            </div>
                            <div class="form-group"> 
                                <input type="hidden" name="forum_topic_id" value="<?php echo $val->forum_topic_id?>" />
                                <input type="hidden" name="forum_content_id" value="<?php echo $val->forum_content_id?>" />
                                <div class="checkbox subscribeEmailCheckbox <?php echo($isTopicSubscribed!=1)?'':'hide';?>">
					               <label>
					                   <input style="color:#999; font-weight:normal;" type="checkbox" name="notify-me" value="1" checked> 有人回應討論串時透過Email通知我
					               </label>
					            </div>   
                                <button type="submit" class="btn btn-primary leave-comment" data-topic-id="<?php echo $val->forum_topic_id?>" data-content-id="<?php echo $val->forum_content_id?>" style="margin-right:5px;">回覆</button>
                                <button type="button" class="btn comment-cancel" style="margin-right:5px;">取消</button>
                            </div>
                        </form>                    
                    </div>  <!-- /discussion-answer-post -->   
                    <?}?>                      
                </div> <!-- /discussion-answer -->
                <? }?>
                <!-- DISCUSSION ANSWER POST / 回答 -->
				<style>
                .discussion-answer-post .avatar {
                    float:left;
                    margin-left:-80px;}
                </style>
                <?php if($this->session->userdata('email')){?>
                <div class="discussion-answer-post" style="padding:0 0 20px 110px;">
                    <div class="avatar" style="background:url(<?php echo base_url()?>assets/img/user/128x128/<?php echo $this->user_model->getImg($this->session->userdata('email'))?>)">
                        <img src="<?php echo base_url()?>assets/img/user/128x128/<?php echo $this->user_model->getImg($this->session->userdata('email'))?>"/>
                    </div>
                    <form id="form-leave-answer" action="<?php echo base_url()?>forum/addForumContent" method="post">
                    	<div class="form-group">
                        	<textarea name = "forum_content" class="form-control" rows="3" id="leave-answer"placeholder="我要回答問題"></textarea>
                        </div>
                        <div class="form-group">   
                        	<input type="hidden" name="forum_topic_id" value="<?php echo $val->forum_topic_id?>" />
                        	<p>您可以使用Markdown來格式化您的問題，如果您不熟悉Markdown的撰寫方式，請參考<a class="show-markdown-cheatsheet" href="javascript: void(0)" data-toggle="modal" data-target="#markdown-modal">Markdown Cheatsheet</a></p>
					      <div class="checkbox subscribeEmailCheckbox <?php echo($isTopicSubscribed!=1)?'':'hide';?>">
					        <label>
					          <input style="color:#999; font-weight:normal;" type="checkbox" name="notify-me" value="1" checked> 有人回應討論串時透過Email通知我
					        </label>
					      </div>             
                            <button type="submit" class="btn btn-primary leave-answer" style="margin-right:5px;">回答問題</button>
                    	</div>
                    </form>                    
                </div>  <!-- /discussion-answer-post -->
                <? }else{?>
                	<div class="alert alert-gray">需登入才能回答問題</div>
                <? }?> 
            </div> <!-- /discussion-answer-container -->
            
        </div>
	</div>
</div><!-- /container -->
<?php if($this->session->userdata('email')){?>
	<?$this->load->view('forum/modalStartDiscuss');?>
<?}?>

<!-- Markdown Modal -->
<div class="modal fade" id="markdown-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Markdown Cheatsheet</h4>
      </div>
      <div class="modal-body">
      	<?php $this->load->view('forum/mdMethod'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<? $this->load->view('home/modal_login');?>

<script language="JavaScript" src="<?=base_url();?>assets/js/includeXAPI.js"></script>
<? foreach($topic as $key =>$val){?>
    <?php $forum_topic_id = $val->forum_topic_id?>
    <?php $email = 'mailto:'.$val->email?>
    <?php $alias = $val->alias?>
    <?php $forum_topic = $val->forum_topic?>
    <?php $library_name =  $val->library_name;?>
<?}?>
<script>
    if(name){
        var initTime = new Date();
        var email = 'mailto:'+"<?=$this->session->userdata('email');?>";
        var accessType = "<?=base_url();?>topic";
        var agent = new ADL.XAPIStatement.Agent(email,name);
        var contentVerb = ADL.verbs.read;
        var authority = {"account":{"homePage":"http://140.92.88.88:80/mLRSTest","name":'2AEB66648CC123B8566D'},"name":"learninghouse","objectType":"Agent"};
        var activityId = "<?=base_url();?>forum/forumContent/<?echo $val->forum_topic_id?>";
        var accessCourseTag = "<?=$library_name?>";
        var activity = new ADL.XAPIStatement.Activity(activityId,'<?=$forum_topic?>','<?=$forum_topic?>',accessType);
        activity.addExtensions({"http://localhost/learninghouse/author":"<?=$alias?>"});
        var context = new ADL.XAPIStatement.Context(
            '',
            {"objectType":"Agent","mbox":"<?=$email?>","name":"<?=$alias?>"},
            '',{
                "parent":[{
                    "id":activityId,"objectType":"Activity",
                    "definition":{
                        "name":{"en-US":accessCourseTag},
                        "description":{"en-US":accessCourseTag}
                    }
                }]
               },
            '','','','','');
        var leaveTime = new Date();
        var lastTime = leaveTime-initTime;
        var result = new ADL.XAPIStatement.Result('',true,'','','',{"<?=base_url();?>forum/forumContent/time":lastTime,
                                                                    "<?=base_url();?>forum/forumContent/startTime":initTime,
                                                                    "<?=base_url();?>forum/forumContent/endTime":leaveTime});
        var stmt = new ADL.XAPIStatement(agent,contentVerb,activity,context,result,authority);
        stmt.generateId();
        var json_stmt = JSON.stringify(stmt);
        //console.log(JSON.stringify(stmt));
        // ADL.XAPIWrapper.changeConfig({
            // 'endpoint': 'https://140.92.88.88/mLRSTest/TCAPI/',
                // 'user': '2AEB66648CC123B8566D',
            // 'password': 'CACA3D203627B194301B'
            // });
        // ADL.XAPIWrapper.sendStatement(stmt);
    }   
</script>