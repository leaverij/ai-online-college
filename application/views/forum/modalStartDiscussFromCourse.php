<!-- satrt-discussion Modal -->

<?if(!empty($getQuestion)){?>
	<?php $row = $getQuestion->row();?>
	<?php $content_desc =  $row->chapter_content_desc?>
<?}?>
<div class="modal fade" id="modal-start" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">發表新問題至討論區</h4>
      		</div>
      		<!-- 討論區發問問題 -->
      		<form id="startDiscuss" role="form" action="<?php echo base_url()?>forum/add" method="post">
      			<div class="modal-body">
              <input type="hidden" name="library_id" id="library_id" value="<?php echo $library_id?>"/>
              <input type="hidden" name="course_url" id="course_url" value="<?php echo $course_url?>"/>
      				
					<div class="form-group">
              <label for="forum_topic">問題標題</label>
                <?if(!empty($content_desc)){?>
      					<input type="text" class="form-control" name="forum_topic" id="forum_topic" placeholder="請輸入問題標題" value="[測驗]<?php echo $content_desc?>"/>
      					<?}else{?>
      					<input type="text" class="form-control" name="forum_topic" id="forum_topic" placeholder="請輸入問題標題" value=""/>
      					<?}?>
                    </div>
                    <div class="form-group">
                        <label for="forum_content">問題描述</label>
      					<textarea class="form-control" name="forum_content" rows="8" cols="40" placeholder="請輸入問題描述"></textarea>
      				</div>
      				<div class="form-group">
                        <label for="forum_markdown">
                        	您可以使用Markdown來格式化您的問題，如果您不熟悉Markdown的撰寫方式，<br>
                        	請參考<a class="show-markdown-cheatsheet" href="javascript: void(0)">Markdown Cheatsheet</a>
                        </label>
					</div>
					  <div class="form-group">
					    <div>
					      <div class="checkbox">
					        <label>
					          <input type="checkbox" name="notify-me" value="1" checked> 有人回應討論串時透過Email通知我
					        </label>
					      </div>
					    </div>
					  </div>
					<div class="form-group markdown-cheatsheet" style="display: none">
                        <label for="forum_markdown_cheatsheet">Markdown Cheatsheet</label>
                        <?php $this->load->view('forum/mdMethod'); ?>
					</div>
      			</div>
      			<div class="modal-footer">
      				
        			<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        			<button id ="tin-can-submit-form" type="submit" class="btn btn-primary">送出</button>
      			</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->