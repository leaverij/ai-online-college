<p>Markdown是一種撰寫純文本格式的語法，您可以透過Markdown將Text轉化成HTML，以下是一些較常用的樣式！
	您也可以參考<a href="http://daringfireball.net/projects/markdown/basics"><b>Markdown</b></a>線上文件</p>
<h5><b>超連結</b></h5>
<p>使用方法： [超連結範例](http://example.com/)</p>
<h5><b>程式碼</b></h5>
<p>在輸入的文字前面輸入四個空白鍵</p>
<p><?php echo $this->md->defaultTransform('    四個空白鍵')?></p>
也可以輸入程式碼
<p><?php echo $this->md->defaultTransform('    <p>p標籤</p>')?></p>
<h5><b>斜體字</b></h5>
<p>使用方法：*斜體字範例*</p>
<?php echo $this->md->defaultTransform('*斜體字範例*')?>
<h5><b>粗體字</b></h5>
<p>使用方法：**粗體字範例**</p>
<?php echo $this->md->defaultTransform('**粗體字範例**')?>
<h5><b>項目清單</b>(項目符號及編號後面請保留一個空格才能產生作用)</h5>
<p>
	+ Candy
</p>
<p>
	+ Gum
</p>
<p>
	+ Peanuts
</p>
<h5><b>編號</b></h5>
<p>
	1. Red
</p>
<p>
	2. Green
</p>
<p>
	3. Blue
</p>