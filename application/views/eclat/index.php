<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title?></title>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?=base_url()?>/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?=base_url()?>/assets/css/fontawesome/font-awesome.min.css">
        <style>
        #upload_file_name {
            display: inline-block;
            margin-right: 5px;
        }
        #progressModal {
            z-index: 9999;
        }
        #progressModal .modal-header,
        #progressModal .modal-body {
            background: #eee;
        }
        </style>
    </head>
    <body>
        <header class="navbar-default navbar-fixed-top" style="height:80px;">
            <div class="container"></div>
            <div class="navbar-header">
          		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#lh-navigation">
            		<span class="icon-bar"></span>
            		<span class="icon-bar"></span>
            		<span class="icon-bar"></span>
          		</button>
          		<a class="navbar-brand" href="<?=base_url();?>">
		  			<img src="<?=base_url();?>assets/img/eclat.jpg" style="height:50px;">
                </a>
        	</div>
        </header>
        <div class="container" style="margin-top:100px;">
            <h4>轉檔</h4>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">上傳 txt 檔案</label>
                        <div class="row">
                            <div class="col-md-8">
                                <span id="upload_file_name"></span>
                                <span style="display:none" id="uploaded_label"><i style="color:green;" class="fa fa-check-circle-o" aria-hidden="true"></i></span>
                                <button type="button" onclick="removeFile(this)" style="display:none" id="removeFileBtn" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
                            </div>
                            <div class="col-md-4" id="fileContainer">
                                <a id="pickFileBtn" class="btn btn-primary" href="javascript:;">選擇檔案</a>
                                <a id="uploadFileBtn" class="btn btn-primary" href="javascript:;" disabled>上傳檔案</a>
                                <a data-name="" data-date="" id="transFileBtn" data-loading-text="轉檔中" class="btn btn-success" href="javascript:;" disabled>執行轉檔</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h4>轉檔歷史</h4>
            <hr>
            <table id="historyTable" class="table table-striped">
                <thead>
                    <tr>
                        <th>檔案名稱</th>
                        <th>上傳時間</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $file_content = json_decode($files);
                    foreach($file_content as $file){
                ?>
                    <tr data-id="<?php echo $file->id ?>">
                        <td><?php echo $file->name ?></td>
                        <td><?php echo $file->upload_date ?></td>
                        <?php
                            $file_name = $file->name;
                            $pure_file_name = explode(".", $file_name);
                        ?>
                        <td><a target="_blank" class="btn btn-primary" href="<?=base_url()?>assets/eclat/<?php echo $pure_file_name[0] ?>.csv">查看</a></td>
                    </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
        </div>
    <!-- 上傳檔案 Progress Modal -->
    <div id="progressModal" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-header">
                <h4 class="modal-title">上傳中</h4>
            </div>
            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                        <span class="sr-only"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?=base_url()?>/assets/js/jquery-1.11.0.min.js"></script>
    <script src="<?=base_url()?>/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>assets/js/plupload-2.3.6/js/plupload.full.min.js" type="text/javascript" charset="utf-8"></script>
    <script>
    $(document).ready(function(){
        initUpload();
        $('#transFileBtn').on('click', function(){
            var $btn = $(this).button('loading');
            var fileName = $(this).attr('data-name');
            var uploadDate = $(this).attr('data-date');
            $.ajax({
                url: '/eclat/transFile',
                data:'name='+fileName,
                type:"POST",
                success: function(result){
                    $('#historyTable tbody').prepend('<tr><td>'+fileName+'.txt</td><td>'+uploadDate+'</td><td><a target="_blank" class="btn btn-primary" href="<?=base_url()?>assets/eclat/'+fileName+'.csv">查看</a></td><tr>');
                    $btn.button('reset');
                    $('#upload_file_name').empty();
                    $('#removeFileBtn').hide();
                    $('#pickFileBtn').attr('disabled', false);
                    $('#uploadFileBtn').attr('disabled', true);
                    $('#transFileBtn').attr('disabled', true);
                    $('#uploaded_label').hide();
                },
                error:function(xhr, ajaxOptions, thrownError){
                    $btn.button('reset');
                    alert('發生問題，請洽系統管理員');
                    console.log(xhr.status);
                    console.log(thrownError);
                }
         	});
        });
    });
    function initUpload(){
        var uploader = new plupload.Uploader({
	        runtimes : 'html5,flash,silverlight,html4',
	        browse_button : 'pickFileBtn', // you can pass an id...
	        container: document.getElementById('fileContainer'), // ... or DOM Element itself
	        url : '<?=base_url()?>eclat/uploadFiles',
	        flash_swf_url : '<?=base_url()?>assets/js/plupload-2.3.6/js/Moxie.swf',
	        silverlight_xap_url : '<?=base_url()?>assets/js/plupload-2.3.6/js/Moxie.xap',
	        filters : {
		        max_file_size : '10mb',
		        mime_types: [
			        {title : "", extensions : "txt"}
		        ]
	        },
	        init: {
		        PostInit: function() {
			        document.getElementById('upload_file_name').innerHTML = '請選擇 txt 檔案';
			        document.getElementById('uploadFileBtn').onclick = function() {
				    uploader.start();
				    return false;
			    };
		    },
		    FilesAdded: function(up, files) {
			    plupload.each(files, function(file) {
                    $('#upload_file_name').empty().html('<div id="' + file.id + '">' + file.name + '</div>');
			    });
                $('#removeFileBtn').show();
                $('#pickFileBtn').attr('disabled', true);
                $('#uploadFileBtn').attr('disabled', false);
                $('#transFileBtn').attr('disabled', true);
		    },
            BeforeUpload: function(up, file){
                $('#uploadFileBtn').attr('disabled', true);
                var fileNameStr = file.name;
                up.settings.multipart_params = {
                    fileName:file.name
                };
                $('.progress-bar').css('width','0%');
                $('#progressModal').modal('show');
            },
		    UploadProgress: function(up, file) {
                $('.progress-bar').css('width',file.percent+'%');
		    },
            FileUploaded: function(up, file, result){
                var res = JSON.parse(result.response);
                var filePureName = getFilePureName(file.name);
                $('#progressModal').modal('hide');
                $('#uploaded_label').show();
                $('#transFileBtn').attr('disabled', false);
                $('#transFileBtn').attr('data-name', filePureName);
                $('#transFileBtn').attr('data-date', res.upload_date);
            },
		    Error: function(up, err) {
                $('#progressModal').modal('hide');
		    }
	    }
    });
    uploader.init();
}
function removeFile(e){
    var removeBtn = $(e);
    $('#upload_file_name').empty();
    $('#removeFileBtn').hide();
    $('#pickFileBtn').attr('disabled', false);
    $('#uploadFileBtn').attr('disabled', true);
    $('#transFileBtn').attr('disabled', true);
    $('#uploaded_label').hide();
}
function getFilePureName(filename){
    return filename.substring(0, filename.lastIndexOf('.'));
}
    </script>
    </body>
</html>