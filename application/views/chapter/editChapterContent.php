<?php
    $course_content = json_decode($course);
    $chapter_content = json_decode($chapters);
    $target_chapter_content = json_decode($chapter);
    $chapter_content = $target_chapter_content->chapter_contents;
?>
<link href="http://vjs.zencdn.net/6.4.0/video-js.css" rel="stylesheet">
<!-- support IE8 -->
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
<style>
ul.chapter-content-list {
    margin: 20px 0px 20px 0px;
    padding-left: 0px;
}
ul.chapter-content-list li {
    list-style:none;
}
.chapter-content-block {
    background: #eef0f2;
    width: 100%;
    border: 1px solid #a2a2a2;
    margin-bottom: 5px;
    padding: 10px;
}
.chapter-type {
    cursor: move;
    display: inline-block;
    vertical-align: middle;
    margin-right: 10px;
    border-right: 1px solid #b4b4b4;
    padding: 10px;
}
.chapter-content-name {
    display: inline-block;
    font-size: 18px;
    cursor: pointer;
    text-decoration: none !important;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    vertical-align: middle;
    max-width: 560px;
    width: calc(100% - 250px);
}
.edit-btn-block {
   padding: 4px;
   float: right; 
}
#editor { 
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
}
#add-chapter-content-row {
    display:none;
}
.edit-chapter-content-block {
    display: none;
}
.video-js {
    width: 100%;
}
.hide {
    display: none;
}
#upload_file_name {
    display: inline-block;
    margin-right: 5px;
}
#progressModal {
    z-index: 9999;
}
#progressModal .modal-header,
#progressModal .modal-body {
    background: #eee;
}
#code_editor,#right_code_editor,#add_code_editor,#add_right_code_editor {
    width: 100%;
    height: 300px;
}
#new-video {
    width: 100%;
    height: 300px;
}
.chapter-sort {
    cursor: move;
    display: inline-block;
    vertical-align: middle;
    margin-right: 10px;
    border-right: 1px solid #b4b4b4;
    padding: 10px;
}
</style>
<div class="container" style="padding-top:30px; margin-bottom: 20px;">
    <div style="display:none;" class="alert alert-success" id="success-alert">
    	<strong>更新成功</strong>
    </div>  
    <div class="row">
        <div class="col-md-3">
            <?php $this->load->view('course/course_sidebar'); ?> 
        </div> 
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li><a href="<?=base_url()?>course/teach">所有課程</a></li>
                <li><a href="<?=base_url()?>course/edit/<?php echo $course_content->course_id ?>/content">課程關卡列表</a></li>
                <li>課程任務編輯</li>
            </ol>
            <h3><?php echo $course_content->course_name ?></h3>
            <h4><?php echo $target_chapter_content->chapter_name ?></h4>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-primary addChapterContentBtn" data-type="0">新增影片</a>
                    <a class="btn btn-primary addChapterContentBtn" data-type="3">新增文章</a>
                    <a class="btn btn-primary addChapterContentBtn" data-type="1">新增測驗</a>
                    <a class="btn btn-primary addChapterContentBtn" data-type="5">新增編譯</a>
                    <a class="btn btn-primary addChapterContentBtn" data-type="4">新增實做</a>
                </div>
            </div>
            <div class="row" id="add-chapter-content-row">
                <div class="col-md-12" style="margin:10px 0px;">
                    <div class="form-group">
                        <label for="add-chapter-content-name">任務名稱</label>
                        <div class="input-group">
                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-video-camera" aria-hidden="true"></i></span>
                            <input type="text" id="add-chapter-content-name" name="add-chapter-content-name" value="" class="form-control" maxlength="256">
                        </div>                        
                    </div>
                    <div id="add-chapter-content-block"></div>
                    <input type="hidden" id="video_duration" name="video_duration" value="0">
                    <input type="hidden" name="add-chapter-id" value="<?php echo $target_chapter_content->chapter_id ?>">
                    <a id="cancelAddChapterContentBtn" class="btn btn-default">取消</a>
                    <a id="createNewChapterContentBtn" data-type="" class="btn btn-primary">建立</a>
                </div>
            </div>
            <div class="row">
                <ul class="chapter-content-list">
                <?php
                foreach($chapter_content as $chapter_content_val){
                ?>
                    <li id="chapter_content_<?php echo $chapter_content_val->chapter_content_id ?>" data-order="<?php echo $chapter_content_val->content_order ?>">
                        <div class="chapter-content-block">
                            <span class="chapter-sort"><i class="fa fa-arrows" aria-hidden="true"></i></span>
                            <?php
                            $type_icon = '';
                            $type_icon_label = '';
                            switch($chapter_content_val->content_type){
                                case 0:
                                    $type_icon = '<i class="fa fa-video-camera" aria-hidden="true"></i>';
                                    $type_icon_label = '影片';
                                    break;
                                case 1:
                                case 2:
                                    $type_icon = '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
                                    $type_icon_label = '測驗';
                                    break;
                                case 3:
                                    $type_icon = '<i class="fa fa-file-text" aria-hidden="true"></i>';
                                    $type_icon_label = '文章';
                                    break;
                                case 4:
                                    $type_icon = '<i class="fa fa-flask" aria-hidden="true"></i>';
                                    $type_icon_label = '實做';
                                    break;
                                case 5:
                                    $type_icon = '<i class="fa fa-keyboard-o" aria-hidden="true"></i>';
                                    $type_icon_label = '編譯';
                                    break;
                            }
                            ?>
                            <span class="chapter-type"><?php echo $type_icon ?>&nbsp;<?php echo $type_icon_label ?></span>
                            <a onclick="getExistChapterContent(this)" class="chapter-content-name" data-id="<?php echo $chapter_content_val->chapter_content_id ?>" data-type="<?php echo $chapter_content_val->content_type ?>">
                                <span class="chapter_content_name_text"><?php echo $chapter_content_val->chapter_content_desc ?></span>
                                <form method="post" action="<?=base_url();?>api/content/updateChapterContentName">
                                    <input class="form-control chapter_content_name_input" style="display:none;" type="text" name="chapter_content_name" value="<?php echo $chapter_content_val->chapter_content_desc ?>">
                                    <input type="hidden" name="chapter_content_id" value="<?php echo $chapter_content_val->chapter_content_id ?>">
                                </form>
                            </a>
                            <span class="edit-btn-block">
                                <a onclick="saveChapterContentName(this)" data-chaptercontentid="<?php echo $chapter_content_val->chapter_content_id ?>" class="btn btn-success edit-btn btn-save" style="display:none;"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
                                <a onclick="editChapterContentName(this)" class="btn btn-primary edit-btn btn-edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <a data-toggle="modal" data-target="#removeChapterContentModal" data-chaptercontentid="<?php echo $chapter_content_val->chapter_content_id ?>" class="btn btn-danger edit-btn btn-remove"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </span>
                        </div><!-- .chapter-content-block -->
                        <div class="edit-chapter-content-block">

                        </div>
                    </li>
                <?php
                }
                ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- 移除關卡確認Modal -->
<div id="removeChapterContentModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h4>確定要刪除嗎?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button id="removeChapterContentBtn" data-chapterid="" type="button" class="btn btn-primary">確定</button>
            </div>
        </div>
    </div>
</div>
<!-- 上傳檔案 Progress Modal -->
<div id="progressModal" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-header">
            <h4 class="modal-title">上傳中</h4>
        </div>
        <div class="modal-body">
            <div class="progress">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                    <span class="sr-only"></span>
                </div>
            </div>
        </div>
    </div>
</div>

<script id="add-content-template" type="text/x-handlebars-template">
    <div class="form-group">
        <label for="add-chapter-content-desc-editor">任務內容</label>
        <div id="add-chapter-content-desc-editor">{{content_desc}}</div>
    </div>
</script>
<!-- 新增影片任務 Template -->
<script id="add-video-content-template" type="text/x-handlebars-template">
    <div class="form-group">
        <label for="add-chapter-content-desc-editor">任務內容</label>
        <div id="add-chapter-content-desc-editor">{{content_desc}}</div>
    </div>
    <div class="form-group">
        <label for="">上傳 MP4 (檔案大小勿超過100MB,使用英文檔名)</label>
        <div class="row">
            <div class="col-md-8">
                <span id="upload_file_name"></span>
                <span style="display:none" id="uploaded_label"><i style="color:green;" class="fa fa-check-circle-o" aria-hidden="true"></i></span>
                <button data-type="0" type="button" onclick="removeFile(this)" style="display:none" id="removeFileBtn" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
            </div>
            <div class="col-md-4" id="fileContainer">
                <a id="pickFileBtn" class="btn btn-primary" href="javascript:;">選擇檔案</a>
                <a id="uploadFileBtn" class="btn btn-primary" href="javascript:;" disabled>上傳檔案</a>
                <input type="hidden" id="s3_url" value="">
                <input type="hidden" id="s3_name" value="">
                <input type="hidden" id="s3_uuid" value="">
            </div>
        </div>
    </div>
    <div id="new-video-preview" class="form-group" style="display:none;">
        <label>影片預覽</label>
        
    </div>
</script>
<!-- 新增實作任務 Template -->
<script id="add-lab-content-template" type="text/x-handlebars-template">
    <div class="form-group">
        <label for="add-chapter-content-desc-editor">任務內容</label>
        <div id="add-chapter-content-desc-editor">{{content_desc}}</div>
    </div>
    <div class="form-group" style="display:none;">
        <label for="">上傳 Jupyter Notebook (檔案大小勿超過10MB,使用英文檔名)</label>
        <div class="row">
            <div class="col-md-8">
                <span id="upload_file_name"></span>
                <span style="display:none" id="uploaded_label"><i style="color:green;" class="fa fa-check-circle-o" aria-hidden="true"></i></span>
                <button data-type="4" type="button" onclick="removeFile(this)" style="display:none" id="removeFileBtn" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
            </div>
            <div class="col-md-4" id="fileContainer">
                <a id="pickFileBtn" class="btn btn-primary" href="javascript:;">選擇檔案</a>
                <a id="uploadFileBtn" class="btn btn-primary" href="javascript:;" disabled>上傳檔案</a>
                <input type="hidden" id="s3_url" value="">
                <input type="hidden" id="s3_name" value="">
                <input type="hidden" id="s3_uuid" value="">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label>實作類型</label>
        <select id="labTypeSelector" class="form-control" onchange="changeLabType()">
            <option value="0" selected>個人實作</option>
            <option value="1">團體實作</option>
        </select>
    </div>
    <div class="form-group" id="labDurationSection">
        <label>實做時間</label>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>時</label>
                    <select id="lab-hour" class="form-control">
                        <option value="0">0</option>
                        <option value="1" selected>1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>分</label>
                    <select id="lab-minute" class="form-control">
                        <option value="0" selected>0</option>
                        <option value="15">15</option>
                        <option value="15">30</option>
                        <option value="15">45</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group" id="labGroupSection" style="display:none;">
        <label>學生分組</label>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>選擇</th>
                    <th>分組名稱</th>
                </tr>
            </thead>
            <tbody>
            {{#list content_groups}}
                {{name}}
            {{/list}}
            </tbody>
        </table>
    </div>
</script>
<!-- 新增測驗任務 Template -->
<script id="add-quiz-content-template" type="text/x-handlebars-template">
    <div class="form-group">
        <label for="add-chapter-content-desc-editor">任務內容</label>
        <div id="add-chapter-content-desc-editor">{{content_desc}}</div>
    </div>
    <div class="form-group">
        <label for="">題庫</label>
        <div class="row">
            <div class="col-md-6" id="question_block">
                
            </div>
            <div class="col-md-6">
                <div> 目前題數：<span id="current_no">0</span> 題[請出兩題以上]</div>
                <div id="pick_question">
                
                </div>
            </div>
        </div>
    </div>
</script>
<script id="exist-content-template" type="text/x-handlebars-template">
    <div class="form-group">
        <label for="exist-chapter-content-editor">任務內容</label>
        <div id="exist-chapter-content-editor">{{chapter_content}}</div>
    </div>
    <div class="form-group">
        <label for="content_desc">任務說明</label>
        <textarea id="content_desc" value="{{content_desc}}" class="form-control">{{content_desc}}</textarea>
    </div>
    <div class="form-group">
        <a class="btn btn-default">取消</a>
        <a onclick="updateChapterContent(this)" data-id="{{chapter_content_id}}" class="btn btn-primary">儲存</a>
    </div>
</script>
<!-- 新增編譯任務 Template -->
<script id="add-compile-content-template" type="text/x-handlebars-template">
    <div class="form-group">
        <label for="add-chapter-content-desc-editor">任務內容</label>
        <div id="add-chapter-content-desc-editor">{{content_desc}}</div>
    </div>
    <div class="form-group">
        <label>任務提示</label>
        <input type="text" class="form-control" id="add_hint" placeholder="請輸入提示內容">
    </div>
    <div class="form-group">
        <label>預設程式碼</label>
        <div id="add_code_editor"></div>
    </div>
    <div class="form-group">
        <label>正確答案程式碼</label>
        <div id="add_right_code_editor"></div>
    </div>
</script>
<!-- 已編輯影片 Template -->
<script id="exist-video-content-template" type="text/x-handlebars-template">
    <div class="form-group">
        <label for="exist-chapter-content-desc-editor">任務內容</label>
        <div id="exist-chapter-content-desc-editor"></div>
    </div>
    <div class="form-group">
        <label>影片預覽</label>
        <video id="my-video" class="video-js" controls preload="auto" width="100%" height="auto"
            poster="MY_VIDEO_POSTER.jpg" data-setup="{}">
            <source id="video-source" src="{{video_url}}" type='video/mp4'>
        </video>
    </div>
    <div class="form-group">
        <a onclick="updateChapterContentDesc(this)" data-id="{{chapter_content_id}}" class="btn btn-primary">儲存</a>
    </div>
</script>
<!-- 已編輯測驗 Template -->
<script id="exist-assessment-content-template" type="text/x-handlebars-template">
    <div class="form-group">
        <label for="exist-chapter-content-desc-editor">任務內容</label>
        <div id="exist-chapter-content-desc-editor"></div>
    </div>
    <div class="form-group">
        <label for="exist-chapter-content-quiz">測驗題目（目前不提供新增刪減題目，如要修改，請刪除此關卡重新建立）</label>
        <div id="quiz"></div>
    </div>
    <div class="form-group">
        <a onclick="updateChapterContentDesc(this)" data-id="{{chapter_content_id}}" class="btn btn-primary">儲存</a>
    </div>
</script>
<!-- 已編輯文章 Template -->
<script id="exist-article-content-template" type="text/x-handlebars-template">
    <div class="form-group">
        <label for="exist-chapter-content-desc-editor">任務內容</label>
        <div id="exist-chapter-content-desc-editor"></div>
    </div>
    <div class="form-group">
        <a onclick="updateChapterContentDesc(this)" data-id="{{chapter_content_id}}" class="btn btn-primary">儲存</a>
    </div>
</script>
<!-- 已編輯實做 Template -->
<script id="exist-lab-content-template" type="text/x-handlebars-template">
    <div class="form-group">
        <label for="exist-chapter-content-desc-editor">任務內容</label>
        <div id="exist-chapter-content-desc-editor"></div>
    </div>
    <!--
    <div class="form-group">
        <label>Jupyter Notebook</label>
        {{#if jupyter_name}}
        <p>{{jupyter_name}}</p>
        {{else}}
        <p>未上傳檔案</p>
        {{/if}}
    </div>
    -->
    <div class="form-group">
        <label>實作時間</label>
        <p>{{time time_spent}}</p>
    </div>
    <div class="form-group">
        <label>實作類型</label>
        <p>{{lab_type}}</p>
    </div>
    <div class="form-group" id="existLabGroup" style="display:none;">
        <label>學生分組</label>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>選擇</th>
                    <th>分組名稱</th>
                </tr>
            </thead>
            <tbody>
            {{#list content_groups}}
                {{name}}
            {{/list}}
            </tbody>
        </table>
    </div>
    <div class="form-group">
        <a onclick="updateLabChapterContent(this)" data-type="{{lab_type_int}}" data-id="{{chapter_content_id}}" class="btn btn-primary">儲存</a>
    </div>
</script>
<!-- 已編輯編譯 Template -->
<script id="exist-compile-content-template" type="text/x-handlebars-template">
    <div class="form-group">
        <label for="exist-chapter-content-desc-editor">問題說明</label>
        <div id="exist-chapter-content-desc-editor"></div>
    </div>
    <div class="form-group">
        <label>任務提示</label>
        <input type="text" class="form-control" id="hint" placeholder="請輸入提示內容" value="{{hint}}">
    </div>
    <div class="form-group">
        <label>預設程式碼</label>
        <div id="code_editor">{{code}}</div>
    </div>
    <div class="form-group">
        <label>正確答案程式碼</label>
        <div id="right_code_editor">{{right_code}}</div>
    </div>
    <div class="form-group">
        <a onclick="updateCompileContent(this)" data-id="{{chapter_content_id}}" class="btn btn-primary">儲存</a>
    </div>
</script>

<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/handlebars-v4.0.11.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/ckeditor/ckeditor.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/bootstrap-maxlength.js" type="text/javascript" charset="utf-8"></script>
<script src="http://vjs.zencdn.net/6.4.0/video.js"></script>
<script src="<?=base_url();?>assets/js/plupload-2.3.6/js/plupload.full.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/ace.js" type="text/javascript" charset="utf-8"></script>
<script>
$("input[maxlength]").maxlength();

var addVideoContentTemplate,
    addLabContentTemplate,
    addQuizContentTemplate,
    addLabContentTemplate, 
    addCompileContentTemplate,
    existContentTemplate, 
    existVideoContentTemplate, 
    existAssessmentContentTemplate,
    existArticleContentTemplate, 
    existLabContentTemplate,
    existCompileContentTemplate;
var current_chapter = <?=$chapter?>;
var current_chapter_contents = current_chapter.chapter_contents;
var add_code_editor, code_editor,add_right_code_editor,right_code_editor;
var content_groups = <?=$content_groups?>;
$(document).ready(function(){

    $( ".chapter-content-list" ).sortable({
        start: function(e, ui) {
            $(this).attr('data-previndex', ui.item.index());
        },
        update: function(e, ui) {
            var reorderArr = [];
            $('.chapter-content-list li').each(function(index, ele){
                var content_ele_id = $(ele).attr('id').split('_')[2];
                reorderArr.push(content_ele_id);
            });
            chapterContentReorder(reorderArr);
        }
    });

    addVideoContentTemplate = Handlebars.compile($('#add-video-content-template').html());
    addLabContentTemplate = Handlebars.compile($('#add-lab-content-template').html());
    addQuizContentTemplate = Handlebars.compile($('#add-quiz-content-template').html());
    addCompileContentTemplate = Handlebars.compile($('#add-compile-content-template').html());

    existContentTemplate = Handlebars.compile($('#exist-content-template').html());
    existVideoContentTemplate = Handlebars.compile($('#exist-video-content-template').html());
    existAssessmentContentTemplate = Handlebars.compile($('#exist-assessment-content-template').html());
    existArticleContentTemplate = Handlebars.compile($('#exist-article-content-template').html());
    existLabContentTemplate = Handlebars.compile($('#exist-lab-content-template').html());
    existCompileContentTemplate = Handlebars.compile($('#exist-compile-content-template').html());
    addContentTemplate = Handlebars.compile($('#add-content-template').html());
    Handlebars.registerHelper('time', function(text, url) {
        return moment.utc(text*1000).format("HH:mm:ss");
    });
    Handlebars.registerHelper('list', function(items, options) {
        var out = "";
        for(var i=0, l=items.length; i<l; i++) {
            var checkedStr = (items[i]['is_active']=='1')?'checked':'';
            out = out + ("<tr><td><input type='checkbox' name='lab_group' value='"+items[i].content_group_id+"' "+checkedStr+"></td><td>" + options.fn(items[i]) + "</td></tr>");
        }
        return out;
    });

    $('#removeChapterContentModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var chapter_content_id = button.attr('data-chaptercontentid');
        var modal = $(this);
        modal.find('#removeChapterContentBtn').attr('data-chaptercontentid', chapter_content_id);
    });
    //新增任務編輯
    $('.addChapterContentBtn').on('click', function(){
        $('#add-chapter-content-name').html('');
        var chapterContentType = $(this).attr('data-type');
        $('#createNewChapterContentBtn').attr('data-type', chapterContentType);
        switch(chapterContentType){
            case '0':
                $('#sizing-addon1').html('<i class="fa fa-video-camera" aria-hidden="true"></i>');
                $("#add-chapter-content-block").empty().append(addVideoContentTemplate({
                    content_desc:''
                }));
                initUpload(0);
                break;
            case '1':
                $('#sizing-addon1').html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>');
                $("#add-chapter-content-block").empty().append(addQuizContentTemplate({
                    content_desc:''
                })); 
                var json = {token:"getchoicefromcbe"};
                $.ajax({
                    type: 'post',
                    data: json,
                    url: 'http://www.virtuallab.tw/vlApi/getAllChoice',
                    success : function(data, error, xhr){
                        var temp_data='';
                        for (var i = 0;i<data.length;i++) {
                            var temp = '<tr>'+
                            '<td><div class="checkbox"><label>'+
      '<input id="q'+data[i].choice_bank_id+'" type="checkbox" name="checkBoxChoice[]" onclick="myFunction('+data[i].choice_bank_id+')">'+data[i].choice_bank_id+
    '</label></div></td>'+
                            '<td>題目：<span id="question_content_'+data[i].choice_bank_id+'">'+ data[i].question +'</span><p>答案：<span id="origin_answer_'+data[i].choice_bank_id+'">' + data[i].plain_answer +'</span><span style="display:none;" id="correct_answer_'+data[i].choice_bank_id+'">' + data[i].answer +'</span></td></tr>';
                            temp_data = temp_data+temp;
                        }
                        $("#question_block").html('<table id="table_id" class="display">'+
                    '<thead><tr><th>題目</th><th>選擇</th></tr></thead>'+
                    '<tbody>'+ temp_data +'</tbody></table>');
                        $('#table_id').DataTable( {
                            "language": {
                                "lengthMenu": "每頁顯示 _MENU_ 筆",
                                "zeroRecords": "搜尋不到資料",
                                "info": "顯示頁面 _PAGE_ of _PAGES_",
                                "infoEmpty": "No records available",
                                "infoFiltered": "(filtered from _MAX_ total records)"
                            }
                        } );
                    }
                });
                break;
            case '2':
                $('#sizing-addon1').html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>');
                $("#add-chapter-content-block").empty().append(addContentTemplate({
                    content_desc:''
                }));
                break;
            case '3':
                $('#sizing-addon1').html('<i class="fa fa-file-text" aria-hidden="true"></i>');
                $("#add-chapter-content-block").empty().append(addContentTemplate({
                    content_desc:''
                }));
                break;
            case '4': //實做
                $('#sizing-addon1').html('<i class="fa fa-flask" aria-hidden="true"></i>');
                $("#add-chapter-content-block").empty().append(addLabContentTemplate({
                    content_desc:'',
                    content_groups:content_groups
                }));
                initUpload(4);
                break;
            case '5':
                $('#sizing-addon1').html('<i class="fa fa-keyboard-o" aria-hidden="true"></i>');
                $("#add-chapter-content-block").empty().append(addCompileContentTemplate({
                    content_desc:''
                }));
                add_code_editor = ace.edit("add_code_editor");
                add_code_editor.setTheme("ace/theme/monokai");
                add_code_editor.getSession().setMode("ace/mode/python");
                add_right_code_editor = ace.edit("add_right_code_editor");
                add_right_code_editor.setTheme("ace/theme/monokai");
                add_right_code_editor.getSession().setMode("ace/mode/python");                
                break;
        }
        CKEDITOR.replace('add-chapter-content-desc-editor');
        $('#add-chapter-content-row').show();
    });
    //新增任務
    $('#createNewChapterContentBtn').on('click', function(){
        var title = $('#add-chapter-content-name').val();
        if(!title || title.trim().length==0){
            alert('請輸入關卡名稱');
            return;
        }
        title = title.trim();
        var content_desc = CKEDITOR.instances['add-chapter-content-desc-editor'].getData();
        var type = $(this).attr('data-type');
        var type_icon = '';
        var type_icon_label = '';
        var upload_data_url = '';
        switch(type){
            case '0':
                type_icon = '<i class="fa fa-video-camera" aria-hidden="true"></i>';
                type_icon_label = '影片';
                upload_data_url = 'chapter_id=<?php echo $target_chapter_content->chapter_id ?>&title='+title+'&content='+$('#s3_url').val()+'&type='+type+'&content_desc='+content_desc+'&s3_uuid='+$('#s3_uuid').val()+'&s3_name='+$('#s3_name').val()+'&video_duration='+$('#video_duration').val();
                break;
            case '1':
                type_icon = '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
                type_icon_label = '測驗';
                upload_data_url = 'chapter_id=<?php echo $target_chapter_content->chapter_id ?>&title='+title+'&content='+content_desc+'&type='+type+'&content_desc='+content_desc+'&apiquiz=1&'+question_array+option_content_array+correct_answer_array+origin_answer_array+answer_array;
                break;
            case '2':
                type_icon = '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
                type_icon_label = '測驗?';
                upload_data_url = 'chapter_id=<?php echo $target_chapter_content->chapter_id ?>&title='+title+'&content='+content_desc+'&type='+type+'&content_desc='+content_desc;
                break;
            case '3':
                type_icon = '<i class="fa fa-file-text" aria-hidden="true"></i>';
                type_icon_label = '文章';
                upload_data_url = 'chapter_id=<?php echo $target_chapter_content->chapter_id ?>&title='+title+'&content='+content_desc+'&type='+type+'&content_desc='+content_desc;
                break;
            case '4':
                var labGroups = [];
                if($('#labTypeSelector').val()==1){ //團體
                    $('#add-chapter-content-block input:checkbox[name="lab_group"]').each(function(i) {
                        var checkEle = $(this);
                        var groupValue = this.value;
                        if(checkEle.is(':checked')){
                            labGroups.push({
                                content_group_id:groupValue,
                                is_active:true
                            });
                        }
                    });
                    if(labGroups.length==0){
                        alert('至少勾選一個群組');
                        return;
                    }
                }
                type_icon = '<i class="fa fa-flask" aria-hidden="true"></i>';
                type_icon_label = '實做';
                var labHour = parseInt($('#lab-hour').val());
                var labMinute = parseInt($('#lab-minute').val());
                var time_spent = labHour*60*60+labMinute*60;
                upload_data_url = 'chapter_id=<?php echo $target_chapter_content->chapter_id ?>&title='+title+'&content='+$('#s3_url').val()+'&type='+type+'&content_desc='+content_desc+'&s3_uuid='+$('#s3_uuid').val()+'&s3_name='+$('#s3_name').val()+'&time_spent='+time_spent+'&lab_type='+$('#labTypeSelector').val()+'&lab_group='+JSON.stringify(labGroups);
                break;
            case '5':
                var code_content = add_code_editor.getValue();
                var right_code_content = add_right_code_editor.getValue();
                type_icon = '<i class="fa fa-keyboard-o" aria-hidden="true"></i>';
                var hint = $("#add_hint").val();
                type_icon_label = '編譯';
                upload_data_url = 'chapter_id=<?php echo $target_chapter_content->chapter_id ?>&title='+title+'&content='+code_content+'&type='+type+'&content_desc='+content_desc+'&right_code_content='+right_code_content+'&hint='+hint;
                break;
            }
        $.ajax({
			type: "POST",
			cache: false,
			async: false,
			url: "<?=base_url()?>api/content/createNewChapterContent",
            data: upload_data_url,
			success :function(data){
                if(data.success){
                    $('#add-chapter-content-row').hide();
                    $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                        $("#success-alert").slideUp(500);
                    });
                    //append new chapter_content
                    $('.chapter-content-list').append(
                        '<li id="chapter_content_'+data.id+'" data-order="'+data.content_order+'">'+
                            '<div class="chapter-content-block">'+
                                '<span class="chapter-sort"><i class="fa fa-arrows" aria-hidden="true"></i></span>'+
                                '<span class="chapter-type">'+type_icon+'&nbsp;'+type_icon_label+'</span>'+
                                '<a onclick="getExistChapterContent(this)" class="chapter-content-name" data-id="'+data.id+'" data-type="'+type+'">'+
                                    '<span class="chapter_content_name_text">'+title+'</span>'+
                                    '<form method="post" action="<?=base_url();?>api/content/updateChapterContentName">'+
                                        '<input class="form-control chapter_content_name_input" style="display:none;" type="text" name="chapter_content_name" value="'+title+'">'+
                                        '<input type="hidden" name="chapter_content_id" value="'+data.id+'">'+
                                    '</form>'+
                                '</a>'+
                                '<span class="edit-btn-block">'+
                                    '<a onclick="saveChapterContentName(this)" data-chaptercontentid="'+data.id+'" class="btn btn-success edit-btn btn-save" style="display:none;"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>'+
                                    '<a onclick="editChapterContentName(this)" class="btn btn-primary edit-btn btn-edit"><i class="fa fa-pencil" aria-hidden="true"></i></a>'+
                                    '<a data-toggle="modal" data-target="#removeChapterContentModal" data-chaptercontentid="'+data.id+'" class="btn btn-danger edit-btn btn-remove"><i class="fa fa-trash" aria-hidden="true"></i></a>'+
                                '</span>'+
                            '</div>'+
                            '<div class="edit-chapter-content-block">'+
                            '</div>'+
                    '   </li>'
                    );
                }
			}
		});
    });

    //取消新增任務編輯
    $('#cancelAddChapterContentBtn').on('click', function(){
        $('#add-chapter-content-row').hide();
    });
});
$('#removeChapterContentBtn').on('click', function(){
    var chapter_content_id= $(this).attr('data-chaptercontentid');
    $.ajax({
        url : '<?=base_url();?>api/content/removeChapterContent',
        type: "POST",
        data : 'chapter_content_id='+chapter_content_id,
        success:function(data, textStatus, jqXHR) {
            $('#removeChapterContentModal').modal('hide');
            $('ul.chapter-content-list li#chapter_content_'+chapter_content_id).remove();
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#success-alert").slideUp(500);
            });
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //if fails      
        }
    });
});
function updateChapterContentDesc(e){
    var saveEle = $(e);
    var target_chapter_content_id = saveEle.attr('data-id');
    var content_desc = CKEDITOR.instances['exist-chapter-content-desc-editor'].getData();
    $.ajax({
        url : '<?=base_url();?>api/content/updateChapterContentDesc',
        type: "POST",
        data : 'chapter_content_id='+target_chapter_content_id+'&content_desc='+content_desc,
        success:function(data, textStatus, jqXHR) {
            alert('已儲存!');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //if fails      
        }
    });
}
function updateLabChapterContent(e){
    var saveEle = $(e);
    var target_chapter_content_id = saveEle.attr('data-id');
    var lab_type = saveEle.attr('data-type');
    var content_desc = CKEDITOR.instances['exist-chapter-content-desc-editor'].getData();
    var labGroups = [];
    if(lab_type=='1'){ //團體
        $('#existLabGroup input:checkbox[name="lab_group"]').each(function(i) {
            var checkEle = $(this);
            var groupValue = this.value;
            labGroups.push({
                content_group_id:groupValue,
                is_active:checkEle.is(':checked')
            });
        });
        if(labGroups.length==0){
            alert('至少勾選一個群組');
            return;
        }
    }
    $.ajax({
        url : '<?=base_url();?>api/content/updateLabChapterContent',
        type: "POST",
        data : 'chapter_content_id='+target_chapter_content_id+'&content_desc='+content_desc+'&lab_type='+lab_type+'&lab_group='+JSON.stringify(labGroups),
        success:function(data, textStatus, jqXHR) {
            alert('已儲存!');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //if fails      
        }
    });
}
function updateCompileContent(e){
    var saveEle = $(e);
    var target_chapter_content_id = saveEle.attr('data-id');
    var content_desc = CKEDITOR.instances['exist-chapter-content-desc-editor'].getData();
    var content = code_editor.getValue();
    var right_code_content = right_code_editor.getValue();
    var hint = $("#hint").val();
    if(!content_desc){
        alert('請輸入題目');
        return;
    }
    $.ajax({
        url : '<?=base_url();?>api/content/updateCompileContent',
        type: "POST",
        data : 'chapter_content_id='+target_chapter_content_id+'&content_desc='+content_desc+'&content='+content+'&right_code_content='+right_code_content+'&hint='+hint,
        success:function(data, textStatus, jqXHR) {
            alert('已儲存!');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            //if fails      
        }
    });
}
function getExistChapterContent(e){
    var editEle = $(e);
    $('.edit-chapter-content-block').empty().hide();
    var target_chapter_content_id = editEle.attr('data-id');
    var target_content_type = editEle.attr('data-type');
    var editTargetEle = editEle.parent().parent().find('.edit-chapter-content-block');
    $.ajax({
		type: "GET",
		cache: false,
		async: false,
		url: "<?=base_url()?>api/content/getChapterContent?course_id=<?php echo $course_content->course_id?>&chapter_content_id="+target_chapter_content_id,
		success :function(data){
            switch(target_content_type){
                case '0': //影片
                    var chapterUrlArr = data.chapter_content_url.split('-');
                    chapterUrl = chapterUrlArr[0]+'-'+chapterUrlArr[1];
                    editTargetEle.empty().append(existVideoContentTemplate({
                        chapter_content_id:data.chapter_content_id,
                        video_url:data.content//'http://learninghousemediafiles.s3.hicloud.net.tw/'+chapterUrl+'/'+data.content
                    }));
                    break;
                case '1': //測驗
                case '2':
                    editTargetEle.empty().append(existAssessmentContentTemplate({
                        chapter_content_id:data.chapter_content_id
                    }));
                    getQuiz(target_chapter_content_id);
                    
                    break;
                case '3': //文章
                    editTargetEle.empty().append(existArticleContentTemplate({
                        chapter_content_id:data.chapter_content_id
                    }));
                    break;
                case '4': //實做
                    editTargetEle.empty().append(existLabContentTemplate({
                        chapter_content_id:data.chapter_content_id,
                        jupyter_name:data.aws_s3_name,
                        time_spent:data.time_spent,
                        lab_type:(data.lab_type==0)?'個人':'團體',
                        lab_type_int: data.lab_type,
                        content_groups:(data.lab_groups)?data.lab_groups:[]
                    }));
                    if(data.lab_type==0){
                        $('#existLabGroup').hide();
                    }else{
                        $('#existLabGroup').show();
                    }
                    break;
                case '5': //編譯
                    editTargetEle.empty().append(existCompileContentTemplate({
                        chapter_content_id:data.chapter_content_id,
                        code:data.content,
                        right_code:data.right_code_content,
                        hint:data.hint
                    }));
                    code_editor = ace.edit("code_editor");
                    code_editor.setTheme("ace/theme/monokai");
                    code_editor.getSession().setMode("ace/mode/python");
                    right_code_editor = ace.edit("right_code_editor");
                    right_code_editor.setTheme("ace/theme/monokai");
                    right_code_editor.getSession().setMode("ace/mode/python");
                    break;
            }
            CKEDITOR.replace('exist-chapter-content-desc-editor');
            CKEDITOR.instances['exist-chapter-content-desc-editor'].setData(data.content_desc);
            editTargetEle.show();
		}
	});
}
function editChapterContentName(e){
    var editBtn = $(e);
    editBtn.parent().find('a.btn-save').show();
    editBtn.parent().find('a.btn-edit').hide();
    editBtn.parent().find('a.btn-remove').hide();
    $(e).parent().parent().find('span.chapter_content_name_text').hide();
    $(e).parent().parent().find('input.chapter_content_name_input').show();
}
function saveChapterContentName(e){
    var saveBtn = $(e);
    saveBtn.hide();
    saveBtn.parent().find('a.btn-edit').show();
    saveBtn.parent().find('a.btn-remove').show();
    var chapter_content_name_input = saveBtn.parent().parent().find('input.chapter_content_name_input');
    var chapter_content_name_text = saveBtn.parent().parent().find('span.chapter_content_name_text');
    chapter_content_name_input.hide();
    var chapter_content_name_form = saveBtn.parent().parent().find('form');
    chapter_content_name_form.submit(function(e){
        e.preventDefault(); //STOP default action
        var postData = chapter_content_name_form.serializeArray();
        var formURL = chapter_content_name_form.attr("action");
        $.ajax({
            url : formURL,
            type: "POST",
            data : postData,
            success:function(data, textStatus, jqXHR) {
                chapter_content_name_text.html(chapter_content_name_input.val()).show();
                $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                    $("#success-alert").slideUp(500);
                });
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //if fails      
            }
        });
    });
    chapter_content_name_form.submit();
}
var uploader;
function initUpload(upload_file_type){
        uploader = new plupload.Uploader({
	    runtimes : 'html5,flash,silverlight,html4',
	    browse_button : 'pickFileBtn', // you can pass an id...
	    container: document.getElementById('fileContainer'), // ... or DOM Element itself
	    url : '<?=base_url()?>s3/uploadFiles',
	    flash_swf_url : '<?=base_url()?>assets/js/plupload-2.3.6/js/Moxie.swf',
	    silverlight_xap_url : '<?=base_url()?>assets/js/plupload-2.3.6/js/Moxie.xap',
	    filters : {
		    max_file_size : '100mb',
		    mime_types: [
			    {title : (upload_file_type==0)?'mp4 file':'jupyter notebook file', extensions : (upload_file_type==0)?'mp4':'ipynb'}
		    ]
	    },
	    init: {
		    PostInit: function() {
			    document.getElementById('upload_file_name').innerHTML = ('請選擇 '+((upload_file_type==0)?'mp4':'ipynb')+' 檔案');
			    document.getElementById('uploadFileBtn').onclick = function() {
				    uploader.start();
				    return false;
			    };
		    },
		    FilesAdded: function(up, files) {
			    plupload.each(files, function(file) {
                    $('#upload_file_name').empty().html('<div id="' + file.id + '">' + file.name + '</div>');
			    });
                $('#removeFileBtn').show();
                $('#pickFileBtn').attr('disabled', true);
                $('#uploadFileBtn').attr('disabled', false);
		    },
            BeforeUpload: function(up, file){
                $('#uploadFileBtn').attr('disabled', true);
                var fileNameStr = file.name;
                var fileExt = getFileExtension(fileNameStr);
                up.settings.multipart_params = {
                    courseId:"<?php echo $course_content->course_id?>",
                    chapterId:"<?php echo $target_chapter_content->chapter_id?>",
                    fileUUID: generateUUID(),
                    teacherId:"<?php echo $course_content->teacher_id?>",
                    fileName:file.name,
                    fileExt:fileExt,
                    fileType:upload_file_type
                };
                $('#createNewChapterContentBtn').attr('disable', true);
                $('.progress-bar').css('width','0%');
                $('#progressModal').modal('show');
            },
		    UploadProgress: function(up, file) {
                $('.progress-bar').css('width',file.percent+'%');
		    },
            FileUploaded: function(up, file, result){
                var upload_result = JSON.parse(result.response);
                if(upload_result){
                    $('#progressModal').find('.modal-title').html('處理中');
                    $('#new-video-preview').show();
                    $('#new-video-preview').append(
                        '<video id="new-video" class="video-js" controls preload="auto" poster="MY_VIDEO_POSTER.jpg">'+
                            '<source id="new-video-source" src="'+upload_result['s3_url']+'" type="video/mp4">'+
                        '</video>'
                    );
                    var newVideoPlayer = videojs('new-video');
                    newVideoPlayer.on('loadedmetadata', function(){
                        var video_duration = Math.floor(newVideoPlayer.duration());
                        $('#video_duration').val(video_duration);
                        $('#createNewChapterContentBtn').attr('disable', false);
                        $('#progressModal').find('.modal-title').html('上傳中');
                        $('#progressModal').modal('hide');
                    });
                    //newVideoPlayer.src(upload_result['s3_url']);
                    $('#s3_url').val(upload_result['s3_url']);
                    $('#s3_name').val(upload_result['s3_name']);
                    $('#s3_uuid').val(upload_result['s3_uuid']);
                    
                    $('#uploaded_label').show();
                    
                }else{ //error
                    $('#progressModal').modal('hide');
                }
            },
		    Error: function(up, err) {
                $('#progressModal').modal('hide');
		    }
	    }
    });
    uploader.init();
}
function removeFile(e){
    uploader.removeFile(uploader.files[0]);
    var removeBtn = $(e);
    var content_type = removeBtn.attr('data-type');
    if(content_type=='0'){
        if($('#new-video').length){
            videojs('new-video').dispose();
        }
        $('#new-video-preview').hide();
    }
    $('#upload_file_name').empty();
    $('#removeFileBtn').hide();
    $('#pickFileBtn').attr('disabled', false);
    $('#uploadFileBtn').attr('disabled', true);
    $('#uploaded_label').hide();
}
function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
}
function getFileExtension(filename){
    return filename.substring(filename.lastIndexOf('.')+1, filename.length) || filename;
}
var question_array='',option_content_array='',correct_answer_array='',origin_answer_array='',answer_array='',count=0,total=0;
function myFunction(id) {
    // 使用此id ajax取得題目選項
    var checkbox = document.getElementById('q'+id);
    // total=$('input[name="checkBoxChoice[]"]:checked').length;
    if (checkbox.checked == true){
        // 選題目
        data = "<span id='picked_"+id+"'>題目："+$("#question_content_"+id).text()+"<p>答案："+$("#origin_answer_"+id).text()+"</span><p>";
        $("#pick_question").append(data);
        question_array = question_array+"question_array[]="+$("#question_content_"+id).text()+"&";
        correct_answer_array = correct_answer_array+"correct_answer_array[]="+$("#correct_answer_"+id).text()+"&";
        origin_answer_array = origin_answer_array+"origin_answer_array[]="+$("#origin_answer_"+id).text()+"&";
        getQuestionDetailFromCbeAPI(id);
        total=total+1;
    }else{
        // 去掉題目(暫時不作去掉題目 2018/1/9)
        $("span").remove("#picked_"+id);
        total=total-1;
    }
    $("#current_no").text(total);
}
function getQuestionDetailFromCbeAPI(choice_bank_id){
    var json = {token:"getchoicefromcbe",choice_bank_id:choice_bank_id};
    $.ajax({
        type: 'post',
        data: json,
        url: 'http://www.virtuallab.tw/vlApi/getOneChoiceById',
        success : function(data, error, xhr){
            var temp = [];
            // question_array.push(data[0].question);
            
            for (var i = 0;i<data.length;i++) {
                temp.push(data[i].option_content);
                option_content_array = option_content_array+"option_content_array["+count+"]["+i+"]="+data[i].option_content+"&";
                answer_array = answer_array+"answer_array["+count+"]["+i+"]="+data[i].option_encode+"&";
            }
            count=count+1;    
        }
        
    });
}
function chapterContentReorder(reorderArr){
    $.ajax({
        type: 'post',
        data: 'reorderArr='+JSON.stringify(reorderArr),
        url: '<?=base_url()?>api/content/chapterContentReorder',
        success : function(data, error, xhr){
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#success-alert").slideUp(500);
            });
        },
        error: function(jqXHR, textStatus, errorThrown){

        }
    });
}
function getQuiz(target_chapter_content_id){
    $.ajax({
        type: "GET",
        cache: false,
        async: false,
        url: "<?=base_url()?>api/content/getQuiz?chapter_content_id="+target_chapter_content_id,
        success : function(data, error, xhr){
             for (var i = 0;i<data.length;i++) {
                $("#quiz").append("<p>"+(i+1)+"."+data[i].question_content+"</p>");
                $("#quiz").append("<blockquote>答案："+data[i].origin_answer+"</blockquote>");
             }
             
        },
        error: function(jqXHR, textStatus, errorThrown){

        }
    });
}
function changeLabType(){
    var labType = document.getElementById("labTypeSelector").value;
    if(labType==0){
        $('#labGroupSection').hide();
        $('#labDurationSection').show();
    }else{
        $('#labDurationSection').hide();
        $('#labGroupSection').show();
    }
}
</script>