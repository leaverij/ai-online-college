<script language="JavaScript" src="<?=base_url();?>assets/js/includeXAPI.js"></script>
	<?php
		$isDone=array();
		for($i=0;$i<count($row2);$i++){
			array_push($isDone,0);
		}
		if(!empty($row3)){
			foreach($row3 as $key2=>$value2){
				$i=0;
				foreach($row2 as $key=>$value){
					if($value2->chapter_content_id===$value->chapter_content_id){
						$isDone[$i]=1;
					}
					$i++;
				}
			}
		}else{
			foreach($row2 as $key=>$value){
				if($this->session->userdata($value->chapter_content_url)){
					$isDone[$value->content_order-1]=1;
				}
			}
		}
	?>
	<script type="text/javascript">
	var userProgress="<?php echo array_search(0,$isDone);?>";
	var hasStarted	="<?php echo array_search(1,$isDone);?>";
	</script>
	<? foreach($row2 as $key=>$val){?>
	<? }?>

    <?php $total=$key+1;?>

    <h2 style="margin:0 0 8px 0; font-size:1.71em;"><?php echo $val->chapter_name?></h2>
    <p style="font-size: 1.14em; color: #666666;"><?php echo $val->chapter_desc?></p>
    <link href="<?=base_url()?>assets/img/fb/<?=$val->chapter_pic?>" rel="image_src" type="image/jpeg">
    <?php if($this->session->userdata('email')){?>
    
    <div id="sidetab-wrap">
    	<div class="sidetab tin-can-ask" data-toggle="modal" data-target="#modal-start" data-keyboard="false" data-backdrop="static">
    		<span class="fa fa-bullhorn"></span> 
            <b>我要發問</b>
    	</div>
    </div>
    <?}?>
    <h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">
    	任務 <span style="font-size: 0.6em; vertical-align: middle; background-color: #c3c3c3;" class="label label-default"><?php echo ($key)?$key+1:'0'; ?></span>
    </h3>
    	<div class="achievement-task-list">
        	<ul>
            <? foreach($row2 as $key=>$val){?>
                <? if($val->chapter_content_id){?>
               		<? if($val -> content_open_flag=='1'){?>
                    <li class="<?php echo $val->chapter_content_url;?>">
                        <a id="challengeBtn_<?=$key+1;?>" data-button='{"subtitle":"<?php echo $val->subtitle;?>","content":"<?php echo $val->content;?>","chapter_content_url":"<?php echo $val->chapter_content_url;?>","preview":"<?php echo $val->preview;?>","content_type":"<?php echo $val->content_type;?>","chapter":"<?php echo $chapter;?>","chapter_content_desc":"<?php echo $val->chapter_content_desc?>","content_open_flag":"<?php echo $val->content_open_flag;?>","email":"<?php echo $this->session->userdata('email');?>","current_content_order":"<?php echo $key+1;?>","next_content_order":"<?php echo $key+2;?>","total":"<?php echo $total;?>","active_flag":"<?php echo isset($profile['active_flag'])?$profile['active_flag']:'';?>"}' <?php if($val->content_type!=='0'){echo ' href="'.base_url().'library/'.$library.'/'.$course.'/'.$chapter.'/'.$val->chapter_content_url.'"';}?>>
							<i class="<?php 
								if($isDone[$key]==1){
									echo 'icon_check_alt check';
								}elseif($val->content_type==0){
									echo 'arrow_triangle-right_alt2';
								}elseif($val->content_type==1){
									echo 'fa fa-pencil';
								}elseif($val->content_type==2){
									echo 'fa fa-pencil';
								}elseif($val->content_type==3){
									echo 'fa fa-file-text-o';
								}elseif($val->content_type==4){
									echo 'fa fa-flask';
								}elseif($val->content_type==5){
									echo 'fa fa-window-maximize';
								}?>"></i>
                            <p class="pull-right hidden-xs">
								<? if($val->content_type=='0'){?> <!-- 影片 -->
                                <?php echo $val->duration;?>
                                <?php }elseif($val->content_type=='1'){?> <!-- 測驗 -->
                                <?php echo $val->quantity.' 個問題';?>
                                <?php }elseif($val->content_type=='2'){?> <!-- 填充 -->
								<?php }elseif($val->content_type=='3'){?> <!-- 文章 -->
								<?php }elseif($val->content_type=='4'){?> <!-- 實作 -->
								<?php }elseif($val->content_type=='5'){?> <!-- 編譯 -->
								<?php }else{}?>
                            </p>                     
                            <strong><?=$val -> chapter_content_desc;?></strong>
                        </a>
                    </li>
                	<? }?>
                <? }else{?>
                    <h1 class="text-center">任務內容製作中，敬請期待</h1>
                <? }?>
            <? }?>
            </ul>
        </div>
<?php if($this->session->userdata('email')){?>
	<?$this->load->view('forum/ajaxModalStartDiscuss');?>
<?}?>
