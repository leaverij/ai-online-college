<div class="container">
<script type="text/javascript">
var library= "<?php echo $library;?>";
var course= "<?php echo $course;?>";
var chapter="<?php echo $chapter;?>";
currentMovie="<?php echo $this->session->flashdata('currentMovie');?>";
currentChapter="<?php echo $this->session->flashdata('currentChapter');?>";
isQuizDone="<?php echo $this->session->flashdata('isQuizDone');?>";
</script>
	<?php 
		$this->load->view('breadcrumb');
		$row2 = json_decode($getOneChapterContent);
		$getOneTeacher=json_decode($getOneTeacher);
		if(!empty($getUserContent)){
			$row3 = json_decode($getUserContent); 
			$data=array('row2'=>$row2,'row3'=>$row3);
		}else{
			$data=array('row2'=>$row2);
		}
		if($row2['0']->content_type==0){
			$this->load->view('chapterContent/flowplayer',$data);
		}
		
	?>
    <style>
    /*#achievement-content{padding:40px 0;}*/
    </style>
    <div id="achievement-content">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-8">
					<?php $this->load->view('chapter/contentList',$data);?>
				</div>
                <div class="col-sm-4">
                    <style>
                    .instructor-avatar{
                        float:left;
                        width:80px;
                        height:80px;
                        background:url(<?=base_url();?>assets/img/teacher/<?php echo $getOneTeacher['0']->teacher_pic;?>);
                        background-position:center;
                        background-size:80px auto;
                        border-radius:50px;}
                    .instructor-avatar img{width:80px; height:80px; display:none;}
                    </style>
                    <div>
                      <!-- 以下為勞委會的CASE -->
<?php if($this->user_model->isSuper($this->session->userdata('email'))){?>
<!-- Modal -->
        <script type="text/javascript" src="<?=base_url();?>assets/js/webcam/webcam.js"></script>
        <?php $filename = date('YmdHis') . '.jpg';?>
		<script language="JavaScript">
		var pp =0;
		webcam.set_api_url( '<?=base_url();?>release/record/'+pp+'-<?=$filename;?>');
		webcam.set_quality( 100 ); // 相片品質(1 - 100)
		webcam.set_shutter_sound( false ); // 播放拍照聲音
		//document.write( webcam.get_html(400, 300) );
		webcam.set_hook( 'onComplete', function(){ 
			webcam.reset();
		} );	
		function take_snapshot() {
			webcam.snap();
		}
		setInterval(function() {
			webcam.snap();
			pp = pp+1;
			webcam.set_api_url( '<?=base_url();?>release/record/'+pp+'-<?=$filename;?>');
		}, 5000);
	</script>
      	<center><div id="webcam" class=""></div></center>
		<button style="display:none;" type="button" class="btn btn-danger" onClick="take_snapshot()">拍照</button>
<script type="text/javascript" charset="utf-8">
	document.getElementById('webcam').innerHTML = webcam.get_html(320, 240);
</script>
<?}?>
                    </div>
                    <div style="margin-bottom:30px;">
                        <h3 class="secondary-heading" style="font-size:1.5em; font-weight:600; margin-top:0;">講師</h3>
                        <div>
                            <div class="instructor-avatar">
                                <img style="width:80px;height:80px;" src="<?=base_url();?>assets/img/teacher/<?php echo $getOneTeacher['0']->teacher_pic;?>" />
                            </div>    
                            <h4 style="padding-left:100px; line-height:4.5em;"><?php echo $getOneTeacher['0']->name;?></h4>
                            <p><?php echo $getOneTeacher['0']->introduction;?></p>
                        </div>
                    </div> 
                    <div class="teaching-resources">
                		<h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">教學資源下載區</h3>
                    	<?php $row = json_decode( $listResource );?>
							<?php if(!empty($row)){?>
								<?php foreach($row as $val){?>
                            		<h4 style="color:#a7a7a7;">
                            			<a class="download" data-id="<?php echo $val->dl_resource_id;?>" href="javascript: void(0)">
                            				<?php echo $val->dl_resource_url;?>
                            			</a>
                            		</h4>
                            	<?}?>
							<?}else{?>
								<h4 style="color:#a7a7a7;">目前尚無資源可下載</h4>	
							<?}?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- /container -->
</div>
<? $this->load->view('home/modal_login');?>