<style>
.insert-coupon .content {padding:30px;}
.content label {font-size:1.14em; font-weight:normal;}
@media screen and (max-width: 480px) {
	.insert-coupon .content {padding:15px;}
}
</style>
<div class="container">
	<a href="<?php echo base_url();?>coupon">全部coupon</a>
	<div class="row basic-data">
    	<div class="insert-coupon">
            <div class="col-sm-6" style="margin:0 auto; float:none;">
            	<div class="content">
                	<h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">新增優惠卷</h3>
                    <form id="insertCouponForm" role="form" action="<?=base_url()?>coupon/insertCoupon"method="post">
                      <div class="form-group">
                        <label for="inputNumber">數量</label>
                        <input type="number" class="form-control" id="gen_number" name="gen_number" placeholder="請填入數量">
                      </div>
                      <div class="form-group">
                        <label for="inputPrice">優惠卷面額</label>
                        <input type="text" class="form-control" id="price" name="price" placeholder="優惠卷面額">
                      </div>
                      <div class="form-group">
                      	<label for="couponDate">優惠卷起訖時間</label>
    						<div class="container">
       							<div class='col-md-5'>
                					<div class='input-group start_time' id='' data-date-format="YYYY-MM-DD">
                    					<input type='text' class="form-control" name="start_datetimepicker"/>
                    					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
              						</div>
             					</div>
        						<div class='col-md-5'>
                					<div class='input-group end_time' id='' data-date-format="YYYY-MM-DD">
                   						<input type='text' class="form-control" name="end_datetimepicker"/>
                   						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
               						</div>
            					</div>
            				</div>
                      </div>
                      <div class="form-group">
                        <label for="inputComment">備註</label>
                        <input type="text" class="form-control" id="comment" name="comment" placeholder="備註">
                      </div>
                      <div class="form-group">
                      	<input type="hidden" name="action"	value="insertCoupon">
                        <button type="submit" class="btn btn-success" style="margin-bottom:15px;">送出</button>
                        <button type="button" class="btn" style="margin-bottom:15px;">取消</button>
                      </div>
                    </form>
                </div>
            </div>
    	</div>
    </div>
</div>