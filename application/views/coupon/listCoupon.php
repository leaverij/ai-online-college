<!-- 左邊區塊 -->
<?php $this->load->view('backend/sidebar'); ?>
<!-- 左邊end -->
  		<div class="col-sm-9">
        	<div>	
                <div class="row">
                	<div class="col-sm-12 add-plus-coupon">
    					<div class="thumbnail">
      						<div class="caption">
      							<button type="button" class="btn btn-success add-coupon-btn" style="width: 100%;height: 50px">
      								<i class="fa fa-plus fa-3x"></i>
      							</button>
      						</div>
    					</div>
                    </div>
                    <div class="col-sm-12 add-coupon" style="display: none">
                    	<form id="addCouponForm" role="form" action="<?=base_url()?>coupon/addCoupon"method="post">
                    		<div class="form-group">
                        		<label for="inputNumber">數量</label>
                         		<input type="number" class="form-control" id="gen_number" name="gen_number" placeholder="請輸入要產生的組數">
                      		</div>
                      		<div class="form-group">
                        		<label for="inputPrice">優惠券面額</label>
                        		<input type="text" class="form-control" id="price" name="price" placeholder="請輸入優惠券面額">
                      		</div>
                      		<div class="form-group">
                      			<label for="couponDate">優惠券起訖時間</label>
    							          <div class="container">
       								        <div class='col-md-4'>
                						    <div class='input-group start_time' id='' data-date-format="YYYY-MM-DD">
                    						  <input type='text' class="form-control" name="start_time"/>
                    						  <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                                </div>
             						      </div>
                              <div class='col-md-4'>
                						    <div class='input-group end_time' id='' data-date-format="YYYY-MM-DD">
                   							  <input type='text' class="form-control" name="end_time"/>
                   							    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
               							    </div>
            						      </div>
            					      </div>
                      		</div>
                      		<div class="form-group">
                        		<label for="inputComment">備註</label>
                        		<input type="text" class="form-control" id="comment" name="comment" placeholder="請輸入備註或其他事項">
                      		</div>
                      		<div class="form-group">
                        		<button type="submit" class="btn btn-success" style="margin-bottom:15px;">送出</button>
                        		<button type="button" class="btn cancle-add-coupon" style="margin-bottom:15px;">取消</button>
                      		</div>
                    	</form>
					</div>
                    
					<?php if ($coupon_qry->num_rows() > 0){?>
						<div class="col-sm-12" style="margin-bottom: 10px">
							<div class="row">
                    			<div class="col-md-4">優惠券條碼/起訖時間</div>
                    			<div class="col-md-1">面額</div>
                    			<div class="col-md-3">使用狀態</div>
                    			<div class="col-md-2">備註</div>
                    			<div class="col-md-1">編輯</div>
                    			<div class="col-md-1">刪除</div>
							</div>
						</div>
						<?php foreach($coupon_qry->result() as $coupon){?>
							<div class="col-sm-12 coupon" style="margin-bottom: 10px">
								<div class="row">
									<div class="col-md-4">
										<p><?php echo $coupon->coupon_code;?></p>
										<p><?php echo date("Y-m-d", strtotime($coupon->start_time));?>～<?php echo date("Y-m-d", strtotime($coupon->end_time));?></p>
									</div>
									<div class="col-md-1">
                    <?php echo $coupon->price == '0' ?'免費課程券': $coupon->price.'元 ';?>
                  </div>
									<div class="col-md-3">
                    <?php echo $coupon->used_flag == '1'?'已被'.$coupon->user_email.'使用，課程ID為'.$coupon->course_id:'尚未使用';?>
                  </div>
									<div class="col-md-2"><?php echo $coupon->comment;?></div>			
									<div class="col-md-1"><button type="button" class="btn btn-success btn-sm edit-coupon-btn pull-right">編輯</button></div>
									<div class="col-md-1"><button type="button" class="btn btn-danger btn-sm del-coupon pull-right" data-coupon-id="<?php echo $coupon->coupon_id?>">刪除</button></div>
								</div>
							</div>
							<div class="col-sm-12 edit-coupon" style="display: none">
    							<div class="thumbnail">
      								<div class="caption">
      									<form role="form" action="<?php echo base_url()?>coupon/updateCoupon" method="post">
      									<div class="form-group">
    										<label for="price">面額</label>
    										<input type="text" class="form-control" id="price" name="price" placeholder="輸入優惠券面額" value="<?echo $coupon->price;?>">
    									</div>
    									<div class="form-group">
    										<label for="used_flag">狀態</label>
    										<select class="form-control" id="used_flag" name="used_flag">
                    							<option value="0" <? if($coupon->used_flag=='0') echo 'selected="selected"'; ?>>尚未使用</option>
                    							<option value="1" <? if($coupon->used_flag=='1') echo 'selected="selected"'; ?>>已使用</option>
                    						</select>
    									</div>
    									<div class="form-group">
    										<label for="seminar_host">備註</label>
    										<input type="text" class="form-control" id="comment" name="comment" placeholder="輸入備註" value="<?echo $coupon->comment;?>">
    									</div>
    									<div class="form-group">
    									<label for="seminar_date">起訖時間</label>
    									<div class="container">
       										<div class='col-md-4'>
                								<div class='input-group start_time' id='' data-date-format="YYYY-MM-DD">
                    								<input type='text' class="form-control" name="start_time" value="<?php echo date("Y-m-d", strtotime($coupon->start_time));?>"/>
                    									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    								</span>
                								</div>
                							</div>
        									<div class='col-md-4'>
                								<div class='input-group end_time' id='' data-date-format="YYYY-MM-DD">
                   									<input type='text' class="form-control" name="end_time" value="<?php echo date("Y-m-d", strtotime($coupon->end_time));?>"/>
                   										<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                   										</span>
               									</div>
            								</div>
            							</div>
    									</div>
        								<p>	
        									<input type="hidden" name="coupon_id" value="<?php echo $coupon->coupon_id;?>" />
        									<button type="submit" class="btn btn-info">儲存</button>
                            				<button type="button" class="btn btn-danger cancle-edit-coupon">取消</button>
        								</p>
        								</form>
      								</div>
    							</div>
                    		</div>	
						<?}?>
					<?}?>
                </div>
            </div>
		</div>
	</div>
</div><!-- /container -->