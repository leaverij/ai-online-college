<style>
.update-coupon .content {padding:30px;}
.content label {font-size:1.14em; font-weight:normal;}
@media screen and (max-width: 480px) {
	.update-coupon .content {padding:15px;}
}
</style>
<div class="container">
	<a href="<?php echo base_url();?>coupon">全部coupon</a>
	<div class="row basic-data">
    	<div class="update-coupon">
            <div class="col-sm-6" style="margin:0 auto; float:none;">
            	<div class="content">
                	<h3 class="secondary-heading" style="font-size:1.5em; font-weight:600;">修改優惠卷</h3>
                    <form id="insertCouponForm" role="form" action="<?php echo base_url()?>index.php/coupon/updateCoupon"method="post">
                      <div class="form-group">
                        <label for="inputNumber">編號</label>
                        <input type="number" class="form-control" id="gen_number" name="gen_number" placeholder="請填入數量">
                      </div>
                      <div class="form-group">
                      	<label for="inputCouponId" class="col-sm-2 control-label">條碼編號：</label>
						<span class="form-control-static" disabled></span>
						<input type="hidden" name="coupon_code" id="coupon_code" value=""/>
					  </div>
                      <div class="form-group">
                        <label for="inputStartDate">條碼生效時間</label>
                        <input type="text" class="form-control" id="start_datetimepicker" name="start_datetimepicker" placeholder="條碼生效時間">
                      </div>
                      <div class="form-group">
                        <label for="inputEndDate">條碼失效時間</label>
                        <input type="text" class="form-control" id="end_datetimepicker" name="end_datetimepicker" placeholder="條碼失效時間">
                      </div>
                      <div class="form-group">
						<label for="inputStatus" class="col-sm-2 control-label">未使用：</label>
						<input type="radio" name="used_flag" id="used_flag0" value="0" class="required">
			  		  </div>
					  <div class="form-group">
						<label for="inputStatus" class="col-sm-2 control-label">已使用：</label>
						<input type="radio" name="used_flag" id="used_flag1" value="1" >
					  </div>
                      <div class="form-group">
                        <label for="inputComment">備註</label>
                        <input type="text" class="form-control" id="comment" name="comment" placeholder="備註">
                      </div>
                      <div class="form-group">
                      	<input type="hidden" name="action"	value="updateCoupon">
                        <button type="submit" class="btn btn-success" style="margin-bottom:15px;">送出</button>
                        <button type="button" class="btn" style="margin-bottom:15px;">取消</button>
                      </div>
                    </form>
                </div>
            </div>
    	</div>
    </div>
</div>