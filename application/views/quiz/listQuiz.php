<script language="JavaScript" src="<?=base_url();?>assets/js/includeXAPI.js"></script>
<?php if($this->session->userdata('email')){?>
    <div id="sidetab-wrap">
    	<div class="sidetab tin-can-ask" data-toggle="modal" data-target="#modal-start" data-keyboard="false" data-backdrop="static">
    		<span class="fa fa-bullhorn"></span> 
            <b>我要發問</b>
    	</div>
    </div>
<?}?>
<div class="container">
	<?php $this->load->view('breadcrumb'); ?>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-8">
					<div class="row">
						<div class="col-md-12">
							<?php $row = $getQuestion->row();?>
							<?php $next_url = $next_content_url->row();?>
							<?php $last_url = $last_content_url->row();?>
							<h2><p class="text-center"><?php echo $row->chapter_content_desc?>
								<?php $previous = rtrim($_SERVER['PATH_INFO'], $this->uri->segment(5)); ?>
								<?php $previous = substr($previous,1,strlen($previous))?><!-- 去掉第一個斜線 -->
								</p>
							</h2> 
						</div>
						<div class="col-md-12">
							<div class="col-md-12" style="border-bottom: solid">
								<div class="">
									<div class="question-count pull-right badge">
                                    	<span class="current-quizNo">1</span> <span>of</span> <?php echo count($quizNo)?>
                                    </div>
                                    <?if ($row->question_type==3){?>
                                    	<div class="question">
											<h3 class="question_content">　</h3>
										</div>
                                    <?}else{?>
                                    	<div class="question">
											<h3 class="question_content"><?php echo $row->question_content;?></h3>
										</div>
                                    <?}?>
									
								</div>
							</div>
							<div id="feedback"></div>
							<div class="col-md-12">
								<div class="answers">
								<?php if ($row->question_type==0){?>
									<div class="content">
									<?php foreach ($getQuestion->result() as $key=>$answer){?>
										<!-- 單選 -->
										<h4>(<?php echo chr(65+$key)?>)
											<a href="<?php echo base_url().'library/'.$library.'/'.$course.'/'.$chapter.'/'.$content.'?answer='.$answer->answer.'&question_id='.$answer->question_id;?>" 
												class="answer" onclick="return false"><?php echo $answer->options_content;?></a>
                                        </h4>
									<?}?>
									</div>
								<?}elseif($row->question_type==2){?>
									<div class="content">
									<?php foreach ($getQuestion->result() as $key=>$answer){?>
										<!-- 是非 -->
										<h4>(<?php echo $answer->options_content;?>)
											<a href="<?php echo base_url().'library/'.$library.'/'.$course.'/'.$chapter.'/'.$content.'?answer='.$answer->answer.'&question_id='.$answer->question_id;?>" 
												class="answer" onclick="return false"><?php echo ($answer->options_content=='O')?'True':'False';?></a>
                                        </h4>
									<?}?>
									</div>
								<?}elseif ($row->question_type==1){?>
									<!-- 複選 -->
									<div class="content">
									<?php foreach ($getQuestion->result() as $key=>$answer){?>
										<h4>
      										<div class="checkbox">
      											<label>
      												<input type="checkbox" name="answers" data-question-id="<?php echo $answer->question_id;?>" 
      												data-options-id="<?php echo $answer->options_id;?>" value="<?php echo $answer->answer;?>"> (<?php echo chr(65+$key)?>)
      													<?php echo $answer->options_content;?>
      											</label>
      										</div>
                                        </h4>
									<?}?>
									<a href="javascript:void(0);" class="multiple-choice-submit btn btn-success btn-lg" role="button">送出答案</a>
									</div>
								<?}elseif ($row->question_type==3){?>
									<!-- 填充 -->
									<div class="content">
										<h4 class="fill-in-blank">
											<div><?php echo $row->question_content;?></div>
										</h4>
										<a href="javascript:void(0);" class="fill-in-blank-quiz btn btn-success btn-lg" data-question-id="<?php echo $row->question_id;?>"role="button">送出答案</a>
									</div>
								<?}?>
								</div>
								<hr>
								<a class="returnList btn btn-primary btn-lg" role="button">離開測驗</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<div class="score-counter">
						<div class="col-md-6">
    						<div class="correct well text-center">
      							<strong>0</strong>正確
    						</div>
    					</div>
    					<div class="col-md-6">
    						<div class="incorrect well text-center">
      							<strong>0</strong>不正確
    						</div>
    					</div>
  					</div>
  					<div class="">
						<div class="col-md-12">
							你需要 <span id="correct_answer"><?php echo $row->pass_conditions;?></span> 個或更多正確答案以通過測驗！
    					</div>
  					</div>
				</div>
        	</div>
		</div>
	</div>
</div><!-- /container -->
<!-- 完成測驗且為最後一個測驗 -->
<div class="modal fade" id="quiz-finish-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<img alt="Launching the Website" src="<?php echo base_url()?>assets/img/badges/<?php echo $row->chapter_pic;?>">
				<h2>做得好!</h2>
				<p>恭喜您，您已完成此關卡並得到新的徽章！</p>
				<?if((isset($last_url->chapter_content_url))&&($last_url->content_type=='0')){?>
					<div class="flowplayer" data-engine="flash" data-volume="0.5">
   						<video autoplay>
      						<source  type="video/mp4" src="http://learninghousemediafiles.s3.hicloud.net.tw/<?php echo $chapter.'/'.$last_url->content?>">
   						</video>
					</div>
				<?}?>
			</div>
			<div class="modal-footer">
				<?if((isset($last_url->chapter_content_url))&&($last_url->content_type=='0')){?>
				<?}else{?>
					<?if(isset($next_url->chapter_content_url)){?>
						<a href="<?php echo base_url().'library/'.$library.'/'.$course.'/'.$chapter?>" class="btn btn-success" role="button">繼續新的任務 <span class="glyphicon glyphicon-arrow-right"></span></a>
					<?}?>
				<?}?>
				<a class="returnList btn btn-primary" role="button">回到任務總覽 <span class="glyphicon glyphicon-arrow-left"></span></a>
        	</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- 完成測驗 -->
<div class="modal fade" id="quiz-completed-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<img alt="Launching the Website" src="<?php echo base_url()?>assets/img/badges/<?php echo $row->chapter_pic;?>">
				<h2>做得好!</h2>
				<p>恭喜您，您已完成此關卡！</p>
			</div>
			<div class="modal-footer">
				<?if((isset($next_url->chapter_content_url))){?>
					<?if(($last_url->chapter_content_url)==($next_url->chapter_content_url)){?>
						<a class="returnList btn btn-primary" role="button">回到任務總覽 <span class="glyphicon glyphicon-arrow-left"></span></a>
					<?}else{?>
						<a href="<?php echo base_url().'library/'.$library.'/'.$course.'/'.$chapter?>" class="btn btn-success" role="button">繼續新的任務 <span class="glyphicon glyphicon-arrow-right"></span></a>					
					<?}?>
				<?}else{?>
					<a class="returnList btn btn-primary" role="button">回到任務總覽 <span class="glyphicon glyphicon-arrow-left"></span></a>
				<?}?>
        	</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- 測驗未通過 -->
<div class="modal fade" id="quiz-failed-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<h2>還差一點!</h2>
				<p>很抱歉！您答對的題數未達通過關卡的條件，請重新測驗！</p>
			</div>
			<div class="modal-footer">
				<a href="javascript:window.location.href=window.location.href" class="btn btn-success" role="button">重新測驗 <span class="glyphicon glyphicon-repeat"></span></a>
				<a class="returnList btn btn-primary" role="button">回到任務總覽 <span class="glyphicon glyphicon-arrow-left"></span></a>
        	</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php if($this->session->userdata('email')){?>
	<?$this->load->view('forum/ajaxModalStartDiscuss');?>
<?}?>
<?$this->load->view('home/modal_login');?>
<script>
	var next_url="<?php if(!empty($next_url)){echo $next_url->content_order;}?>";
	var last_url="<?php if(!empty($last_url)){echo $last_url->content_order;}?>";
	var quizNo = <?php echo json_encode($quizNo); ?>;
	var chapter_content_url = "<?php echo $chapter_content_url; ?>";
	var pass_conditions = <?php echo $row->pass_conditions?>;
	var chapter_content_id = <?php echo $row->chapter_content_id?>;
</script>