<?php 
$row=$getOneJobVacancy['0'];
$row2=json_decode($getJobsCategory);
?>
<style>
textarea{
	resize: vertical;
}
</style>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<form id="updateVacancyForm" class="form-horizontal" role="form" method="post" action="<?php echo base_url().'vacancy/updateJobVacancy/'.$row->job_vacancy_id;?>" enctype="multipart/form-data">
				<div class="item-container job-item">
					<h3>編輯職缺</h3>
					<span style="color: #999999;">此頁為提供企業端一編輯畫面，新增一筆職缺。主要分為兩部分：1.職務資訊設定。2.求才條件。3.應徵方式</span>
					<div class="well" style="min-height: 470px;">
						<span class="h4" style="color: #999999;">職務資訊</span>
						<hr>
						<div class="form-group col-sm-12">
							<label for="job_name" class="col-sm-2 control-label"><span style="color:red;">＊</span>職務名稱</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="job_name" name="job_name" placeholder="請填入職務" value="<?php echo $row->job_name;?>">
							</div>
						</div>
						<div class="form-group col-sm-12">
							<label for="jobs_category_id" class="col-sm-2 control-label"><span style="color:red;">＊</span>職務類別</label>
							<div class="col-sm-6">
								<select class="form-control" name="jobs_category_id" id="jobs_category_id">
									<?php foreach($row2 as $key=>$value){?>
										<option value="<?php echo $value->jobs_category_id;?>" <?php if($row->jobs_category_id==$value->jobs_category_id){echo 'selected';};?>><?php echo $value->jobs_category_name;?></option>
									<?php }?>
									<option value="5">專案管理</option>
								</select>
							</div>
						</div>
						<div class="form-group col-sm-12">
							<label for="address" class="col-sm-2 control-label"><span style="color:red;">＊</span>上班地點</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="address" name="address" placeholder="請填入地址" value="<?php echo $row->address;?>">
							</div>
						</div>
						<div class="form-group col-sm-12">
							<label for="name" class="col-sm-2 control-label"><span style="color:red;">＊</span>工作內容</label>
							<div class="col-sm-6">
								<textarea class="form-control" id="job_content" name="job_content" rows="10" placeholder="請填入工作內容"><?php echo preg_replace( '!<br.*>!iU', "\n",$row->job_content);?></textarea>
							</div>
							<span style="color: #999999;">(中文限2000字，英文限4000字)</span>
						</div>
					</div>
				</div>
				<div class="item-container match-item">
					<div class="well" style="min-height: 420px;">
						<span class="h4" style="color: #999999;">求才條件</span>
						<hr>
						<div class="form-group col-sm-12">
							<label for="degree_limit" class="col-sm-2 control-label"><span style="color:red;">＊</span>學歷要求</label>
							<div class="col-sm-3">
								<select class="form-control" name="degree_limit" id="degree_limit">
									<option value="0" <?php if($row->degree_limit==0){echo 'selected';};?>>不拘</option>
									<option value="1" <?php if($row->degree_limit==1){echo 'selected';};?>>高中職</option>
									<option value="2" <?php if($row->degree_limit==2){echo 'selected';};?>>大學</option>
									<option value="3" <?php if($row->degree_limit==3){echo 'selected';};?>>研究所</option>
									<option value="4" <?php if($row->degree_limit==4){echo 'selected';};?>>博士</option>
								</select>
							</div>
						</div>
						<div class="form-group col-sm-12">
							<label for="working_experience" class="col-sm-2 control-label"><span style="color:red;">＊</span>年資要求</label>
							<div class="col-sm-3">
								<select class="form-control" name="working_experience" id="working_experience">
									<option value="-1" <?php if($row->working_experience==-1){echo 'selected';};?>>不拘</option>
									<option value="0" <?php if($row->working_experience==0){echo 'selected';};?>>未滿1年</option>
									<option value="1" <?php if($row->working_experience==1){echo 'selected';};?>>1年以上</option>
									<option value="2" <?php if($row->working_experience==2){echo 'selected';};?>>2年以上</option>
									<option value="3" <?php if($row->working_experience==3){echo 'selected';};?>>3年以上</option>
									<option value="4" <?php if($row->working_experience==4){echo 'selected';};?>>4年以上</option>
									<option value="5" <?php if($row->working_experience==5){echo 'selected';};?>>5年以上</option>
									<option value="6" <?php if($row->working_experience==6){echo 'selected';};?>>6年以上</option>
									<option value="7" <?php if($row->working_experience==7){echo 'selected';};?>>7年以上</option>
									<option value="8" <?php if($row->working_experience==8){echo 'selected';};?>>8年以上</option>
									<option value="9" <?php if($row->working_experience==9){echo 'selected';};?>>9年以上</option>
									<option value="10" <?php if($row->working_experience==10){echo 'selected';};?>>10年以上</option>
								</select>
							</div>
						</div>
						<div class="form-group col-sm-12">
							<label for="match_skill" class="col-sm-2 control-label">擅長工具</label>
		                     <div>
			                     <div class="col-sm-6" style="cursor: pointer;">
				                    <div class="match_skill_div thumbnail" style="line-height:25px;min-height:34px;min-width:100%;">
				                    <?php if(!empty($row->skill)){?>
					                    <?php foreach(explode(',',$row->skill) as $key=>$value){?>
					                    	<span class="label label-default"><?php echo $value;?></span> <a class="fa fa-times" href="javascript:void(0);"></a>  
					                    <?php }?>
				                    <?php }?>
				                    </div>
			                     </div>
		                     </div>
		                     <input name="match_skill" type="hidden" value="<?php if(!empty($row->skill)){echo $row->skill;}?>"/>
						</div>
						<div class="form-group col-sm-12">
							<label for="match_license" class="col-sm-2 control-label">專業證照</label>
		                     <div>
			                     <div class="col-sm-6" style="cursor: pointer;">
				                    <div class="match_license_div thumbnail" style="line-height:25px;min-height:34px;min-width:100%;">
				                    <?php if(!empty($row->license)){?>
					                    <?php foreach(explode(',',$row->license) as $key=>$value){?>
					                    	<span class="label label-default"><?php echo $value;?></span> <a class="fa fa-times" href="javascript:void(0);"></a>  
					                    <?php }?>
				                    <?php }?>
				                    </div>
			                     </div>
		                     </div>
		                     <input name="match_license" type="hidden" value="<?php if(!empty($row->license)){echo $row->license;}?>"/>
						</div>
						<div class="form-group col-sm-12">
							<label for="additional_demand" class="col-sm-2 control-label">其他條件</label>
							<div class="col-sm-6">
								<textarea class="form-control" id="additional_demand" name="additional_demand" rows="3" placeholder=""><?php echo preg_replace( '!<br.*>!iU', "\n",$row->additional_demand);?></textarea>
							</div>
							<span style="color: #999999;">(中文限2000字、中文限4000字)</span>
						</div>
					</div>
				</div>
				<div class="item-container contact-item">
					<div class="well" style="min-height: 390px;">
						<span class="h4" style="color: #999999;">應徵方式</span>
						<hr>
						<span style="color: #999999;">請至少填寫連絡人之Email供求職者連繫</span>
						<hr>
						<div class="form-group col-sm-12">
							<label for="contact" class="col-sm-2 control-label"><span style="color:red;">＊</span>聯絡人</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="contact" name="contact" placeholder="請填入聯絡人" value="<?php echo $row->contact;?>">
							</div>
						</div>
						<div class="form-group col-sm-12">
							<label for="job_name" class="col-sm-2 control-label"><span style="color:red;">＊</span>Email</label>
							<div class="col-sm-6">
								<input type="text" class="form-control" id="contact_email" name="contact_email" placeholder="請填入Email" value="<?php echo $row->contact_email;?>">
							</div>
						</div>
						<div class="form-group col-sm-12">
							<label for="by_phone" class="col-sm-2 control-label"><!-- <input type="checkbox" checked>  -->電話聯絡</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" id="by_phone" name="by_phone" placeholder="" value="<?php echo $row->by_phone;?>">
							</div>
							<span style="color: #999999;">例如：03-425-7387</span>
						</div>
						<div class="form-group col-sm-12">
							<label for="by_others" class="col-sm-2 control-label">其他方式</label>
							<div class="col-sm-6">
								<textarea class="form-control" id="by_others" name="by_others" rows="3" placeholder=""><?php echo preg_replace( '!<br.*>!iU', "\n",$row->by_others);?></textarea>
							</div>
							<span style="color: #999999;">(限120字)</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="controls form-inline col-sm-12">
						<a class="btn btn-default col-sm-offset-3 col-sm-1" href="<?php echo base_url().'vacancy/listOneJobVacancy/'.$row->job_vacancy_id;?>">預覽</a>
						<button type="submit" class="btn btn-success col-sm-offset-1 col-sm-1">儲存</button>
						<a class="btn <?php echo $row->vacancy_open_flag==0? 'btn-success': 'btn-danger'?> col-sm-offset-1 col-sm-2" href="<?php echo base_url().'vacancy/updateVacancyStatus/'.$row->job_vacancy_id;?>"><?php echo $row->vacancy_open_flag==0? '發佈職缺': '關閉職缺'?></a>
					</div>
				</div>
			</form>
			<div class="modal fade" id="match-skill-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<div>請輸入職缺需求的擅長工具</div>
						</div>
						<div class="modal-body">
			                <div class="form-group">
			                    <label for="skill_name" class="col-sm-2 control-label">擅長工具名稱</label>
			                    <div class="col-sm-10">
			                        <input type="text" class="form-control" id="skill_name" name="skill_name" placeholder="請輸入擅長工具名稱" value="">
			                    </div>
			                </div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">結束編輯</button>
							<button type="button" id="add_match_skill_btn" class="btn btn-success">新增擅長工具</button>
			        	</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			<div class="modal fade" id="match-license-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<div>請輸入職缺需求的專業證照</div>
						</div>
						<div class="modal-body">
			                <div class="form-group">
			                    <label for="license_name" class="col-sm-2 control-label">專業證照名稱</label>
			                    <div class="col-sm-10">
			                        <input type="text" class="form-control" id="license_name" name="license_name" placeholder="請輸入專業證照名稱" value="">
			                    </div>
			                </div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">結束編輯</button>
							<button type="button" id="add_match_license_btn" class="btn btn-success">新增專業證照</button>
			        	</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
		</div>
	</div>
</div><!-- /container -->