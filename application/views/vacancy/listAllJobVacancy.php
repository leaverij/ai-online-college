<div class="container">
	<div class="row">
    	<div class="col-sm-6">
			<?php if(!empty($getOneUser['company_img'])){?>
            	<div style="margin:20px 0 30px; overflow:hidden;">
                    <a style="display:block; float:left; margin-right:15px;">
                        <img alt="" src="<?php echo base_url().'assets/img/user/company/128x128/'.($getOneUser['company_img']=='LH-default-company.jpg'?'LH-default-company.jpg':$getOneUser['company_img']);?>">
                    </a>
                    <div>
                        <a href="<? echo 'http://'.$getOneUser['company_website']; ?>" style="color:#000;">
                            <h1 style="font-size:1.57em"><? echo $getOneUser['company_name']; ?></h1>
                        </a>
                        <a href="#" role="button" data-toggle="modal" data-target="#edit-company-modal">修改企業資訊</a>
                    </div>
                </div>
            <?php }else{?>
            	<div style="margin:20px 0 30px;">
                    <a href="#" role="button" data-toggle="modal" data-target="#edit-company-modal">
                        設定公司資料
                    </a>
                </div>
            <?php }?>
        </div>
    </div>
</div>
<div class="container">
	<div class="row">
    	<div class="col-sm-12">
        	<div>
                <form id="deleteVacancyForm" class="form-horizontal" role="form" method="post" action="<?php echo base_url().'vacancy/deleteJobVacancy'?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if(!empty($getJobVacancy)){?>
                            <button type="submit" name="deleteJobVacancy" value="deleteJobVacancy" class="btn btn-default col-md-2 col-xs-12"><i class="fa fa-trash-o"></i> 刪除職缺</button>
                            <?php }?>
                            <a class="btn btn-default col-md-offset-8 col-md-2 col-xs-12" href="<?php echo base_url().'vacancy/addJobVacancy';?>"><i class="fa fa-plus"></i> 新增職缺</a>
                        </div>
                    </div>
                    <div class="tab-content">
                        <?php if(!empty($getJobVacancy)){?>
                            <div style="padding-top: 20px;">
                                <div class="table-responsive">
                                    <table class="table">
                                        <colgroup>
                                            <col class="col-xs-2">
                                            <!-- <col class="col-xs-2"> -->
                                            <col class="col-xs-4">
                                            <col class="col-xs-2">
                                            <col class="col-xs-2">
                                            <col class="col-xs-1">
                                            <col class="col-xs-1">
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th><?php if(!empty($getJobVacancy)){?><input id="selectAllVacancyBtn" name="selectAllVacancyBtn" type="checkbox"><?php }?></th>
                                                <!-- <th>最後更新日期</th> -->
                                                <th>職缺名稱</th>
                                                <th>職務類別</th>
                                                <th>刊登狀態</th>
                                                <th>主動應徵</th>
                                                <th>符合筆數</th>
                                                <th>編輯</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <? foreach($getJobVacancy as $key => $row){?>
                                                <tr>
                                                    <td><input name="deleteVacancyId[]" class="deleteVacancyId" type="checkbox" value="<?php echo $row->job_vacancy_id;?>"></td>
                                                    <!-- <td><?php //echo date("m/d", strtotime($row->lasted_update_time))?></td> -->
                                                    <td><a href="<?php echo base_url().'vacancy/listOneJobVacancy/'.$row->job_vacancy_id;?>" class="record" style="display: block; text-decoration: none; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" title="點擊查看詳細職缺內容"><?php echo $row->job_name;?></a></td>
                                                    <td><?php echo $row->jobs_category_name;?></td>
                                                    <td><?php echo $row->vacancy_open_flag==0?'已關閉':'刊登中';?></td>
                                                    <td><a href="<?php echo base_url().'vacancy/listApplyPerson/'.$row->job_vacancy_id;?>" class="record" style="display: block; text-decoration: none; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" title="點擊查看主動應徵的對象"><?php echo $numOfApplyPerson[$key];?></a></td>
                                                    <td><a href="<?php echo base_url().'vacancy/getMatchPerson/'.$row->job_vacancy_id;?>" class="record" style="display: block; text-decoration: none; white-space: nowrap; text-overflow: ellipsis; overflow: hidden;" title="點擊查看合適的媒合對象"><?php echo $numOfJobVacancy[$key];?></a></td>
                                                    <td><a href="<?php echo base_url().'vacancy/editJobVacancy/'.$row->job_vacancy_id;?>" class="btn btn-default"><div class="fa fa-edit"></div>  編輯職缺</a></td>
                                                </tr>
                                            <? }?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                        <? }else{?>
                            <tr>
                                <h4 style="color:#a7a7a7;" class="text-center">您尚未新增任何工作職缺</h4>
                            </tr>
                        <? }?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edit-company-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="editCompanyForm" class="form-horizontal" role="form" method="post" action="<?php echo base_url().'user/updateUserCompany'?>" enctype="multipart/form-data" style="margin-bottom:0;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">企業資訊</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="company_name" class="col-sm-2 control-label">公司名稱</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="company_name" name="company_name" placeholder="請輸入公司名稱" value="<?php echo $getOneUser['company_name'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="company_website" class="col-sm-2 control-label">公司網址</label>
                        <div class="input-group col-sm-10">
                            <span class="input-group-addon">http://</span>
                            <input type="text" id="company_website" name="company_website" class="form-control" placeholder="請輸入公司名稱" value="<?php echo $getOneUser['company_website'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="company_img">Logo</label>
                        <div class="col-sm-10">
                            <input type="file" id="company_img" name="userfile">
                            <p class="help-block">圖片支援的格式如下：png、jpg、jpeg</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">儲存</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


