<?php 
$row=$getOneJobVacancy;
$row2=json_decode($getJobsCategory);
?>
<style>
.breakword{
-ms-word-break: break-all;
     word-break: break-all;

     /* Non standard for webkit */
     word-break: break-word;

-webkit-hyphens: auto;
   -moz-hyphens: auto;
        hyphens: auto;
}
.job-info {
	padding-right: 15px;
	padding-left: 15px;
}
</style>
<section>
	<div class="container">
		<div class="row" style="margin:20px;">
			<div class="col-sm-12" style="margin-top:20px;">
				<a href="<?php if(!empty($getOneUser['company_website'])){echo 'http://'.$getOneUser['company_website'];}else{echo '#';}?>">
					<?php if(!empty($getOneUser['company_img'])){?>
					<img alt="" src="<?php echo base_url().'assets/img/user/company/128x128/'.($getOneUser['company_img']=='LH-default-company.jpg'?'LH-default-company.jpg':$getOneUser['company_img']);?>">
					<?php }?>
				</a>
			</div>
			<div class="col-sm-8" style="margin-top:20px;">
				<div class="thumbnail" style="padding-bottom: 20px;">
					<div class="h3 job-info" style="margin-top:20px;">
					<?php 
// 					foreach($row2 as $key=>$value){
// 						if($value->jobs_category_id==$row['jobs_category_id']){
// 							echo $value->jobs_category_name;
// 						}
// 					}
					?>
					<?php echo $row['job_name'];?>-<?php echo $getOneUser['company_name'];?>
					</div>
					<hr>
					<h4 style="padding-left: 5px;">工作內容</h4>
					<div class="job-info">
						<?php echo $row['job_content'];?>
					</div>
					<hr>
					<h4 style="padding-left: 5px;">上班地點</h4>
					<div class="job-info"><?php echo $row['address'];?></div>
					<hr>
					<h4 style="padding-left: 5px;">工作條件</h4>
					<div class="job-info">
						<div>年資要求：<?php if($row['working_experience']==-1){echo '不拘';}elseif($row['working_experience']==0){echo '未滿一年';}else{echo $row['working_experience'].'年以上'; }?></div>
						<div>學歷要求：<?php if($row['degree_limit']==0){echo '不拘';}elseif($row['degree_limit']==1){echo '高中職以上';}elseif($row['degree_limit']==2){echo '大學以上';}elseif($row['degree_limit']==3){echo '研究所以上';}?></div>
						<div>科系要求：<?php foreach($row2 as $key=>$value){
						if($value->jobs_category_id==$row['jobs_category_id']){
							echo $value->jobs_category_name;
						}
					}?></div>
						<div>擅長工具：<?php if(empty($row['skill'])){echo '不拘';}else{echo $row['skill'];}?></div>
						<div>專業證照：<?php if(empty($row['license'])){echo '不拘';}else{echo $row['license'];}?></div>
						<div>附加條件：<?php if(empty($row['additional_demand'])){echo '不拘';}else{echo $row['additional_demand'];}?></div>
					</div>
				</div>
			</div>
			
			<div class="col-sm-4" style="margin-top:20px;">
				<div class="thumbnail">
					<div class="h4" style="padding-left: 9px;">應徵方式</div>
					<hr>
					<div class="caption breakword"><div class="fa fa-user"></div>　 <?php echo $row['contact'];?></div>
					<hr>
					<?php if(!empty($row['by_phone'])){?>
					<div class="caption breakword"><div class="fa fa-phone"></div>　<?php echo $row['by_phone'];?></div>
					<hr>
					<?php }?>
					<?php if(!empty($row['contact_email'])){?>
					<div class="caption breakword"><div class="fa fa-envelope-o"></div>　<?php echo $row['contact_email'];?></div>
					<hr>
					<?php }?>
					<center>
						<button class="btn" style="width:200px; margin-bottom: 20px;" disabled>我要應徵</button>
					</center>
				</div>
			</div>				
		</div>
	</div>
</section>