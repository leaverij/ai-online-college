<?php // print_r($list);exit;?>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h3>我的求職</h3>
        </div>
        <div class="col-md-6" >
            <button id="apply_record_btn" class="btn btn-success" onclick="location.href='applyRecord'" style="float: right; font-size: 16px; margin-top: 20px;">應徵紀錄</button>
        </div>
    </div>
    <div class="row">
        <hr>
    </div>
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <label>職務類別</label>
                <select class="form-control" id="job_category">
                    <option value="0" <?php if($selection['job_category']==''){echo 'selected';}?>>不拘</option>
                <?php
                    $category_content = json_decode($job_category);
                    foreach ($category_content as $category_val) { 
                        if($selection['job_category'] == $category_val->jobs_category_id){
                    ?>
                            <option value="<?php echo $category_val->jobs_category_id ?>" selected><?php echo $category_val->jobs_category_name ?></option>   
                    <?php
                        }
                        else{
                    ?>
                            <option value="<?php echo $category_val->jobs_category_id ?>"><?php echo $category_val->jobs_category_name ?></option>
                    <?php
                        }
                    }
                ?>                    

                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>學歷</label>
                <select class="form-control" id="degree_limit">
                    <option  value="0" <?php if($selection['degree_limit']=='0'){echo 'selected';}?>>不拘</option>
                    <option  value="1" <?php if($selection['degree_limit']=='1'){echo 'selected';}?>>高中職</option>
                    <option  value="2" <?php if($selection['degree_limit']=='2'){echo 'selected';}?>>大學</option>
                    <option  value="3" <?php if($selection['degree_limit']=='3'){echo 'selected';}?>>研究所</option>
                    <option  value="4" <?php if($selection['degree_limit']=='4'){echo 'selected';}?>>博士</option>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>年資</label>
                <select class="form-control" id="working_experience">
                    <option value="-1" <?php if($selection['working_experience']=='-1'){echo 'selected';}?>>不拘</option>
                    <option value="0" <?php if($selection['working_experience']=='0'){echo 'selected';}?>>未滿一年</option>
                    <option value="1" <?php if($selection['working_experience']=='1'){echo 'selected';}?>>1年以上</option>
                    <option value="2" <?php if($selection['working_experience']=='2'){echo 'selected';}?>>2年以上</option>
                    <option value="3" <?php if($selection['working_experience']=='3'){echo 'selected';}?>>3年以上</option>
                    <option value="4" <?php if($selection['working_experience']=='4'){echo 'selected';}?>>4年以上</option>   
                    <option value="5" <?php if($selection['working_experience']=='5'){echo 'selected';}?>>5年以上</option>
                    <option value="6" <?php if($selection['working_experience']=='6'){echo 'selected';}?>>6年以上</option>
                    <option value="7" <?php if($selection['working_experience']=='7'){echo 'selected';}?>>7年以上</option>
                    <option value="8" <?php if($selection['working_experience']=='8'){echo 'selected';}?>>8年以上</option>   
                    <option value="9" <?php if($selection['working_experience']=='9'){echo 'selected';}?>>9年以上</option>
                    <option value="10" <?php if($selection['working_experience']=='10'){echo 'selected';}?>>10年以上</option>
                </select>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <label>職務名稱</label>
                <input id="job_name" type="text" class="form-control" value="<?php echo $selection['job_name'];?>">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label></label>
                <button id="jobsubmit" type="submit" class="btn btn-primary form-control">搜尋</button>
            </div>
        </div>
    </div>
    <div class="row">   
        <div class="col-md-12">
            <div class="table-responsive table-striped">
                <table class="table" id="job-table">
                    <thead>
                        <th>刊登日期</th>
                        <th>職務名稱</th>
                        <th>求才公司</th>
                        <th>上班地點</th>
                        <th>學歷要求</th>
                        <th>年資要求</th>
                    </thead>
                    <tbody>
                        <?php
                            $job_vacancy = json_decode($job_vacancy);
                            $td_array = '';
                            foreach($job_vacancy as $jobs){
                                $temp = '<tr>';
                                    // 刊登日期
                                    $temp .= '<td>'.$jobs->posted_time.'</td>';
                                    // 職務名稱
                                    $temp .= '<td>'.'<a href='.base_url().'vacancy/listOneJobVacancy/'.$jobs->job_vacancy_id.'>'.$jobs->job_name .'</a></td>';
                                    // 求才公司
                                    $temp .= '<td>'.$jobs->company_name.'</td>';
                                    // 上班地點
                                    $temp .= '<td>'.$jobs->address.'</td>';
                                    // 學歷要求
                                    switch ($jobs->degree_limit){
                                    case '0':
                                      $temp .= '<td>不拘</td>';
                                      break; 
                                    case '1':
                                      $temp .= '<td>高中職</td>';
                                      break;
                                    case '2':
                                      $temp .= '<td>大學</td>';
                                      break;
                                    case '3':
                                      $temp .= '<td>研究所</td>';
                                      break;
                                    case '4':
                                      $temp .= '<td>博士</td>';
                                      break;
                                    default:
                                      $temp .= '<td>不拘</td>';
                                    }
                                    // 年資要求
                                    switch ($jobs->working_experience){
                                    case '-1':
                                      $temp .= '<td>不拘</td>';
                                      break;  
                                    case '0':
                                      $temp .= '<td>未滿一年</td>';
                                      break;                                    
                                    case '1':
                                      $temp .= '<td>1年以上</td>';
                                      break;
                                    case '2':
                                      $temp .= '<td>2年以上</td>';
                                      break;
                                    case '3':
                                      $temp .= '<td>3年以上</td>';
                                      break;
                                    case '4':
                                      $temp .= '<td>4年以上</td>';
                                      break;
                                    case '5':
                                      $temp .= '<td>5年以上</td>';
                                      break;                                    
                                    case '6':
                                      $temp .= '<td>6年以上</td>';
                                      break;
                                    case '7':
                                      $temp .= '<td>7年以上</td>';
                                      break;
                                    case '8':
                                      $temp .= '<td>8年以上</td>';
                                      break;
                                    case '9':
                                      $temp .= '<td>9年以上</td>';
                                      break;
                                    case '10':
                                      $temp .= '<td>10年以上</td>';
                                      break;                                      
                                    default:
                                      $temp .= '<td>不拘</td>';
                                    }
                                $temp .= '</tr>';
                                $td_array .=$temp;
                            }
                            echo $td_array;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/dataTables.bootstrap.js" type="text/javascript"></script>
<link href="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet">
<script>
    jQuery(document).ready(function($){
        $("#jobsubmit").click(function () {  
            var job_category = $("#job_category").val();
            var degree_limit = $("#degree_limit").val();
            var working_experience = $("#working_experience").val();
            var job_name = $("#job_name").val();
            let url = new URL("<?=base_url();?>vacancy/myjob");
            const params = {
                job_category: job_category,
                degree_limit: degree_limit,
                working_experience: working_experience,
                job_name: job_name
            }
            Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
            window.location.href = url;
        });  

        $('#job-table').DataTable({
            "bFilter": false,
            "bLengthChange": false,
            "pageLength": 10
        });
    })
</script>