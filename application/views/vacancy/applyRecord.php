<div class="container">
    <h3>應徵紀錄</h3>
    <div class="row">   
        <div class="col-md-12">
            <div class="table-responsive table-striped">
                <table class="table" id="apply-job-table">
                    <thead>
                        <th>應徵日期</th>
                        <th>職務名稱</th>
                        <th>公司名稱</th>
                    </thead>
                    <tbody>
                        <?php
                            $apply_record = json_decode($apply_record);
                            $td_array = '';
                            foreach($apply_record as $jobs){
                                $temp = '<tr>';
                                    // 應徵日期
                                    $temp .= '<td>'.$jobs->apply_date.'</td>';
                                    // 職務名稱
                                    $temp .= '<td>'.$jobs->job_name .'</td>';
                                    // 公司名稱
                                    $temp .= '<td>'.$jobs->company_name.'</td>'; 
                                $temp .= '</tr>';
                                $td_array .=$temp;
                            }
                            echo $td_array;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/dataTables.bootstrap.js" type="text/javascript"></script>
<link href="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet">
<script>
    jQuery(document).ready(function($){
        $('#apply-job-table').DataTable({
            "bFilter": false,
            "bLengthChange": false,
            "pageLength": 10
        });
    })
</script>