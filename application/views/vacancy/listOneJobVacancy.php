<?php 
$row=$getOneJobVacancy['0'];
$row2=json_decode($getJobsCategory);
// print_r($this->session->all_userdata());exit;
?>
<style>
.breakword{
-ms-word-break: break-all;
     word-break: break-all;

     /* Non standard for webkit */
     word-break: break-word;

-webkit-hyphens: auto;
   -moz-hyphens: auto;
        hyphens: auto;
}
.job-info {
	padding-right: 15px;
	padding-left: 15px;
}
.apply-form-item{
	margin-bottom: 20px
}
.apply-form-item>label {
    display: inline-block;
    width: 154px;
    text-align: right;
    line-height: 28px;
    vertical-align: top;
}
.apply-form-item>.form-item-content.longer-content {
    width: 50%;
	padding-left: 20px;
}
.apply-form-item>.form-item-content {
    display: inline-block;
    vertical-align: top;
    width: 50%;
}
.apply-form-item textarea {
    width: 100%;
    height: 124px;
    padding: 8px;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}
.whole-line-alert {
    padding-top: 3px;
    font-size: 15px;
}
.error-msg {
    display: none;
}
.options-wrapper {
    position: relative;
    padding-top: 6px;
}
.options-wrapper .message-count {
    color: #999;
    float: right;
}
.apply-form-item>.form-item-content {
    display: inline-block;
    vertical-align: top;
    width: 50%;
	padding-left: 20px;
}
</style>
<section>
	<div class="container">
		<div class="row" style="margin:20px;">
			<div class="col-sm-12" style="margin-top:20px;">
				<a href="<?php if(!empty($row->company_website)){echo 'http://'.$row->company_website;}else{echo '#';}?>">
					<?php if(!empty($row->company_img)){?>
						<img alt="" src="<?php echo base_url().'assets/img/user/company/128x128/'.($row->company_img=='LH-default-company.jpg'?'LH-default-company.jpg':$row->company_img);?>">
					<?php }?>
				</a>
			</div>
			<div class="col-sm-8" style="margin-top:20px;">
				<div class="thumbnail" style="padding-bottom: 20px;">
					<div class="h3 job-info" style="margin-top:20px;">
					<?php 
// 					foreach($row2 as $key=>$value){
// 						if($value->jobs_category_id==$row->jobs_category_id){
// 							echo $value->jobs_category_name;
// 						}
// 					}
					?>
					<?php echo $row->job_name;?>-<?php echo $row->company_name;?>
					</div>
					<hr>
					<h4 style="padding-left: 5px;">工作內容</h4>
					<div class="job-info">
						<?php echo $row->job_content;?>
					</div>
					<hr>
					<h4 style="padding-left: 5px;">上班地點</h4>
					<div class="job-info"><?php echo $row->address;?></div>
					<hr>
					<h4 style="padding-left: 5px;">工作條件</h4>
					<div class="job-info">
						<div>年資要求：<?php if($row->working_experience==-1){echo '不拘';}elseif($row->working_experience==0){echo '未滿一年';}else{echo $row->working_experience.'年以上'; }?></div>
						<div>學歷要求：<?php if($row->degree_limit==0){echo '不拘';}elseif($row->degree_limit==1){echo '高中職以上';}elseif($row->degree_limit==2){echo '大學以上';}elseif($row->degree_limit==3){echo '研究所以上';}?></div>
						<div>職務分類：<?php foreach($row2 as $key=>$value){
						if($value->jobs_category_id==$row->jobs_category_id){
							echo $value->jobs_category_name;
						}
					}?></div>
						<div>擅長工具：<?php if(empty($row->skill)){echo '不拘';}else{echo $row->skill;}?></div>
						<div>專業證照：<?php if(empty($row->license)){echo '不拘';}else{echo $row->license;}?></div>
						<div>附加條件：<?php if(empty($row->additional_demand)){echo '不拘';}else{echo $row->additional_demand;}?></div>
					</div>
				</div>
			</div>
			
			<div class="col-sm-4" style="margin-top:20px;">
				<div class="thumbnail">
					<div class="h4" style="padding-left: 9px;">應徵方式</div>
					<hr>
					<div class="caption breakword"><div class="fa fa-user"></div>　 <?php echo $row->contact;?></div>
					<hr>
					<?php if(!empty($row->by_phone)){?>
					<div class="caption breakword"><div class="fa fa-phone"></div>　<?php echo $row->by_phone;?></div>
					<hr>
					<?php }?>
					<?php if(!empty($row->contact_email)){?>
					<div class="caption breakword"><div class="fa fa-envelope-o"></div>　<?php echo $row->contact_email;?></div>
					<hr>
					<?php }?>
					<center>
						<?php 
							if($this->session->userdata('user_id')===$getOneJobVacancy[0]->user_id){
								echo '<button class="btn" style="width:200px; margin-bottom: 20px;" disabled>無法應徵</button>';
							}else if($isJobApply===true){
								echo '<button class="btn" style="width:200px; margin-bottom: 20px;" disabled>已應徵</button>';
							}else{
								echo '<button class="btn btn-primary" style="width:200px; margin-bottom: 20px;" data-toggle="modal" data-target="#application">我要應徵</button>';
							}
						?>
					</center>
				</div>
			</div>				
		</div>
	</div>
	<div class="modal fade" id="application" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
			<h4 class="modal-title">主動應徵</h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
			</div>
			<div class="modal-body">
				<form id="form1" name="form1" method="post" action="<?=base_url()?>vacancy/applyjob">
					<div class="apply-form-item">
						<label>應徵工作：</label>
						<div class="form-item-content longer-content">
							<div class="job-item">
								<div class="job-name-wrapper">
									<!-- 單筆 顯示職務名稱 -->
									<label for="<?=$getOneJobVacancy[0]->job_vacancy_id;?>">
										<a class="job-name" title="<?=$getOneJobVacancy[0]->job_name;?>" href="<?=base_url().'vacancy/listOneJobVacancy/'.$getOneJobVacancy[0]->job_vacancy_id;?>" target="_blank">
										<div><?=$getOneJobVacancy[0]->job_name;?></div>
										</a>
									</label>
									<div class="whole-line-alert"><?=$getOneJobVacancy[0]->company_name;?></div>
									<input type="hidden" name="applyjob_id" value="<?=$getOneJobVacancy[0]->job_vacancy_id;?>">
								</div>
							</div>
						</div>
					</div>

					<div class="apply-form-item">
						<label for="recommendation">自我推薦信：</label>
						<div class="form-item-content">
							<div class="recommendation-content">
								<textarea id="job_com_content" name="applymsg_show" rows="8" class="w600 text-with-counter" maxlength="100">您好，我叫<?=$this->session->userdata('name');?>，近日得知貴公司在徵人，希望能有參加面試的機會，謝謝！</textarea>
							</div>
						</div>
					</div>
					
					<div style="text-align: center;">
						<div class="button-list">
							<button type="submit" class="btn btn-primary waves-effect waves-light" style="width: 100px; margin-right:10px;">應徵</button>
							<button type="button" class="btn btn-default waves-effect " data-dismiss="modal" style="width: 100px;">取消</button>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
								
			</div>
		</div>
		</div>
	</div>
</section>