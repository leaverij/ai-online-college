<?php 
$row=$getVacancyMatchFactor;
$row2=$compositeQueryJobVacancy;
$row3=$highestDegree;
$row4=$currentJob;
// if(!empty($compositeQueryJobVacancy)){	
// 	$row2=json_decode($compositeQueryJobVacancy);
// }
?>
<section>
	<div class="container">
		<style>
	        .tour-start{margin-bottom:20px;padding:15px; background: #f5f5f5; -webkit-border-radius:4px; -moz-border-radius:4px; -ms-border-radius:4px; border-radius:4px;}
	        .tour-start h4{margin-top:0;}
			.tour-start span{color:#999999;}
			.instructor-avatar {
				width: 80px;
				height: 80px;
				background-position: center;
				background-size: 80px auto !important;
				border-radius: 50px;}
			.instructor-avatar img{width:80px; height:80px; display:none;}
        </style>
		<div class="row" style="margin:20px;">
			<div class="col-sm-12">
				<div class="tour-start hidden-xs">
	            	<h3><?php echo $row['0']->job_name;?></h3>
	                <span>職務分類：<?php echo $row['0']->jobs_category_name;?></span>
	                <br>
	                <span>年資：<?php if($row['0']->working_experience==-1){echo '不拘';}elseif($row['0']->working_experience==0){echo '未滿一年';}else{echo $row['0']->working_experience.'年以上'; }?></span>
	                <br>
	                <span>學歷：<?php if($row['0']->degree_limit==0){echo '不拘';}elseif($row['0']->degree_limit==1){echo '高中職以上';}elseif($row['0']->degree_limit==2){echo '大學以上';}elseif($row['0']->degree_limit==3){echo '研究所以上';}?></span>
	                <br>
	                <span>擅長工具：<?php if(!empty($row['0']->skill)){echo $row['0']->skill;}else{echo '不拘';}?></span>
	                <br>
	                <span>專業證照：<?php if(!empty($row['0']->license)){echo $row['0']->license;}else{echo '不拘';}?></span>
				</div>
			</div>
			<div class="col-sm-12">
				<h4>主動應徵人才 (<?php echo count($row2);?>)</h4>
				<div class="col-sm-12">
					<?php if(!empty($row2)){?>
					<?php foreach($row2 as $key=>$value){?>
						<div style="margin-bottom:15px;height: 150px;" class="thumbnail">
							<div class="col-sm-2" style="padding-left:10px;">
								<div class="instructor-avatar" style="clear: both;background:url(<?=base_url();?>assets/img/user/128x128/<?php echo $value->resume_img;?>);">
									<img src="<?=base_url();?>assets/img/user/128x128/<?php echo $value->resume_img;?>">
								</div>
								<div class="caption"><?php //if(empty($value->experience_end)){echo '在職中';}else{echo $value->experience.'年經驗';}?></div>
							</div>
							<div class="col-sm-5">
								<div class="caption">
								<a href="<?php echo base_url().'user/printPortfolio/'.$value->alias;?>">
									<?php echo $value->name;?>
								</a>
								</div>
								<span style="color: #999999;"><?php if(!empty($value->location)){?><div class="fa fa-map-marker"></div> <?php echo $value->location;}?></span>
								<?php if(!empty($row3[$key]->school_name)){?>
									<div class="caption">
										<?php echo $row3[$key]->school_name.$row3[$key]->department;?>
									</div>
								<?php }?>
								<?php if(!empty($row4[$key]->company_name)){?>
									<div class="caption">
										<?php echo $row4[$key]->company_name;?>
									</div>
								<?php }?>
							</div>
							<div class="col-sm-5">
								<div class="caption"><?php if(!empty($value->apply_msg)){echo $value->apply_msg;}else{echo '這個懶惰的人目前沒有寫自我推薦信';}?></div>
							</div>
						</div>
					<?php }?>
					<?php }else{?>
						<h4 style="color:#a7a7a7;" class="text-center">目前尚未有人應徵</h4>
					<?php }?>
				</div>
				<script type="text/javascript" src="<?php echo base_url().'assets/js/holder.js';?>"></script>
				<!-- 
				<div class="col-sm-4">
				<?php //if(!empty($row2)){?>
					<img src="holder.js/350x<?php echo count($row2)*150+(count($row2)-1)*15;?>">
				<?php //}?>
				</div>
				 -->
			</div>
		</div>
	</div>
</section>