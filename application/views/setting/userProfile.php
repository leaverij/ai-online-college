<div class="content">
	<div class="container">
		<div>
			<h2 style="font-size:1.8em;">檢視使用者資料</h2>
			<hr />
		</div>
	</div>
</div>
<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="h4 col-sm-2">姓名 </div>
				<div class="h4 col-sm-10"><? echo $userProfile['name'] ?></div>
			</div>
			<div class="col-sm-12">
				<div class="h4 col-sm-2">性別 </div>
				<div class="h4 col-sm-10">
					<? if ($userProfile['gender'] == 0){
							echo '女';
						}
						elseif ($userProfile['gender'] == 1){
							echo '男';
						}
						
					?>					
				</div>
			</div>
			<div class="col-sm-12">
				<div class="h4 col-sm-2">生日 </div>
				<div class="h4 col-sm-10"><? echo $userProfile['birthday'] ?></div>
			</div>
			<div class="col-sm-12">
				<div class="h4 col-sm-2">E-mail </div>
				<div class="h4 col-sm-10"><? echo $userProfile['email'] ?></div>
			</div>
			<div class="col-sm-12">
				<div class="h4 col-sm-2">最高學歷 </div>
				<div class="h4 col-sm-10">
					<? if ($userProfile['education_highest_level'] == 1){
							echo '高中職';
						}
						elseif ($userProfile['education_highest_level'] == 2){
							echo '大學';
						}
						elseif ($userProfile['education_highest_level'] == 3){
							echo '研究所';
						}
						elseif ($userProfile['education_highest_level'] == 4){
							echo '博士';
						}
						
					?>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="h4 col-sm-2">連絡電話 </div>
				<div class="h4 col-sm-10"><? echo $userProfile['mobile'] ?></div>
			</div>									
		</div>
	</div>
</div>