<!-- <?php $row = json_decode($getAlluser);?> -->
<div class="content">
	<div class="container">
		<div>
			<h2 style="font-size:1.8em;">使用者管理</h2>
			<hr />
		</div>
	</div>
</div>
<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<div class="">
						<table class="table table-striped table-bordered" id="userList" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>姓名</th>
									<th>Email</th>
                        			<th>管理者</th>
                        			<th>老師</th>
                                    <th>企業</th>
                    			</tr>
                			</thead>
                			<tbody>
                				<tr>
                                    <th>姓名</th>
                                    <th>Email</th>
                                    <th>管理者</th>
                                    <th>老師</th>
                                    <th>企業</th>
                                </tr>
                			</tbody>
            			</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="<?=base_url();?>assets/js/jquery-2.1.4.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/js/dataTables.bootstrap.js" type="text/javascript"></script>
<link href="<?=base_url();?>assets/js/DataTables/DataTables-1.10.16/css/dataTables.bootstrap.css" rel="stylesheet">
<script src="<?=base_url();?>assets/bootstrap/js/bootstrap-switch.js"></script>
<script>
var url = window.location.href;
jQuery(document).ready(function($){
	$('#userList').DataTable({
		"processing": true,
        "serverSide": true,
        "ajax": "<?=base_url();?>setting/getData",
        "order": [],
        "columns" : [
             {
                'data':'0',
                "render": function ( data, type, val, meta ) {           
                    return "<a href='"+url+"/userProfile/"+val[4]+"'>"+val[0]+"</a>";
                 }                

             },
             {'data':'1'},
             {
                 sortable: true,
                 "render": function ( data, type, val, meta ) {
                     // var buttonID = "user_"+val[4]+"_"+val[3];
                     if(val[2]==1){
                        return '<button class="btn btn-info backend" role="button" data-user-id="'+val[4]+'" value="'+val[2]+'">開啟</button>';
                     }else{
                        return '<button class="btn btn-default backend" role="button" data-user-id="'+val[4]+'" value="'+val[2]+'">關閉</button>';
                     }
                     
                 }
             },
             {
                 sortable: true,
                 "render": function ( data, type, val, meta ) {
                     // var buttonID = "user_"+val[4]+"_"+val[3];
                     if(val[3]==1){
                        return '<button class="btn btn-success teacher" role="button" data-user-id="'+val[4]+'" value="'+val[3]+'">開啟</button>';
                     }else{
                        return '<button class="btn btn-default teacher" role="button" data-user-id="'+val[4]+'" value="'+val[3]+'">關閉</button>';
                     }
                     
                 }
             },
             {
                 sortable: true,
                 "render": function ( data, type, val, meta ) {
                     // var buttonID = "user_"+val[4]+"_"+val[3];
                     if(val[5]==1){
                        return '<button class="btn btn-primary enterprise" role="button" data-user-id="'+val[4]+'" value="'+val[5]+'">開啟</button>';
                     }else{
                        return '<button class="btn btn-default enterprise" role="button" data-user-id="'+val[4]+'" value="'+val[5]+'">關閉</button>';
                     }
                     
                 }
             }
             ]
	});
    $('#userList tbody').on( 'click', '.backend', function () {
        user_id = $(this).data("user-id");
        value = ($(this).val()==1)?"0":"1";
        var cell = $(this);
        $.ajax({
            url: base_url + "setting/updateBackendFlag",
            type:"POST",
            data: {user_id:user_id,backend_flag: value},
            success: function (data) {
                if(value==1){
                    cell.removeClass('btn-default');
                    cell.addClass('btn-info');
                    cell.text("開啟");
                    cell.val(1);
                }else{
                    cell.removeClass('btn-info');
                    cell.addClass('btn-default');
                    cell.text("關閉");
                    cell.val(0);
                }
                
            },
            error: function (data) {
                alert('Ajax request 發生錯誤');
            }
        });
    });
    $('#userList tbody').on( 'click', '.teacher', function () {
        user_id = $(this).data("user-id");
        value = ($(this).val()==1)?"0":"1";
        var cell = $(this);
        $.ajax({
            url: base_url + "setting/updateTeacherFlag",
            type:"POST",
            data: {user_id:user_id,teacher_flag: value},
            success: function (data) {
                if(value==1){
                    cell.removeClass('btn-default');
                    cell.addClass('btn-success');
                    cell.text("開啟");
                    cell.val(1);
                }else{
                    cell.removeClass('btn-success');
                    cell.addClass('btn-default');
                    cell.text("關閉");
                    cell.val(0);
                }
            },
            error: function (data) {
                alert('Ajax request 發生錯誤');
            }
        });
    });
    $('#userList tbody').on( 'click', '.enterprise', function () {
        user_id = $(this).data("user-id");
        value = ($(this).val()==1)?"0":"1";
        var cell = $(this);
        $.ajax({
            url: base_url + "setting/updateEnterpriseFlag",
            type:"POST",
            data: {user_id:user_id,enterprise_flag: value},
            success: function (data) {
                if(value==1){
                    cell.removeClass('btn-default');
                    cell.addClass('btn-primary');
                    cell.text("開啟");
                    cell.val(1);
                }else{
                    cell.removeClass('btn-primary');
                    cell.addClass('btn-default');
                    cell.text("關閉");
                    cell.val(0);
                }
            },
            error: function (data) {
                alert('Ajax request 發生錯誤');
            }
        });
    });
})

</script>