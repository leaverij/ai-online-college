<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('ZOOMAPI_KEY', '9f2b37d4bb9db69e57e41aa9183239cdEY1B'); //測試 API_KEY
define('ZOOMAPI_SECRET', '3236117793364a341a1b17655203840aHU10'); //測試 API_SECRET
define('ZOOMAPI_SERVICE_URL', 'https://zoomnow.net/API/zntw_api.php');
define('ZOOMAPI_USER_ID', "iAlp_FpOQdOWdjuEQbBhRA"); //測試 USER_ID 93233767515ab2fed882f7028df5c9

class Zoom extends CI_Controller {
	/**
     * 建構子
     * 把所有頁面會共用的功能在此處理
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('course_model');
    }	

	public function index()
	{
		// echo("ZOOMAPI_KEY： ".ZOOMAPI_KEY);
		// echo("ZOOMAPI_SECRET ".ZOOMAPI_SECRET);
		// echo("ZOOMAPI_SERVICE_URL ".ZOOMAPI_SERVICE_URL);
		// echo("ZOOMAPI_USER_ID ".ZOOMAPI_USER_ID);

    }

	/**
	 * 學生頁面
	 * @return [type] [description]
	 */
	function student_view(){
		$this->theme->load('zoom/student_view',$data);
	}


	/**
	 * 教師頁面
	 * @return [type] [description]
	 */
	function teacher_view(){
		$this->theme->load('zoom/teacher_view',$data);
	}

	/**
	 * 管理頁面
	 * @return [type] [description]
	 */
	function admin_view(){
		$this->theme->load('zoom/admin_view',$data);
	}

	function callService($data, $other_data){
		ksort($data);
		
		$data["check_value"] = $this->makeMacValue($data);
		$data["API_Key"] = ZOOMAPI_KEY;
		
		$postFields = http_build_query($data);
		
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, ZOOMAPI_SERVICE_URL);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($ch);

		$errorMsg = curl_error($ch);
		curl_close($ch);
		if (!$response) {
			$return = array();
			$return["code"] = 9999;
			$return["ZOOMAPI_SERVICE_URL"] = ZOOMAPI_SERVICE_URL;
			$return["postFields"] = $postFields;
			$return["message"] = $errorMsg;
            $response = json_encode($return);
        }
        $responses_json = json_decode($response, true);
        $return = array();
        $return['success'] = true;
        switch($data['api']){
            case "meeting_create":
                $this->course_model->updateCourseMeetingZoom(
                    $responses_json['data']['uuid'],
                    $responses_json['data']['id'],
                    $responses_json['data']['start_url'],
                    $responses_json['data']['join_url'],
                    $other_data['meeting_id']
                );
                $return['id'] = $other_data['meeting_id'];
                $return['create_date'] = $other_data['create_date'];
                $return['meeting_date'] = $data['start_time'];
                $return['start_url'] = $responses_json['data']['start_url'];
                $return['meeting_room_id'] = $responses_json['data']['id'];
                break;
            case "meeting_update":
                $exist_meeting = $this->course_model->getSingleCourseMeeting($other_data['meeting_id'])->row_array();
                $return['id'] = $other_data['meeting_id'];
                $return['create_date'] = $exist_meeting['create_date'];
                $return['meeting_date'] = $data['start_time'];
                $return['start_url'] = $exist_meeting['start_url'];
                $return['meeting_room_id'] = $exist_meeting['meeting_id'];
                break;
            case 'meeting_delete':
                break;
            default:
        }

        $return = json_encode($return);
		$this->output
    	->set_content_type('application/json')
    	->set_output($return);
		
	}

	/**
	 * 建立檢查碼
	 * @return [type] [description]
	 */
	function makeMacValue($data){
		$aryAPI = array();
		foreach($data as $k => $v) {
		    if (trim($v) == "")
		        continue;
		    $aryAPI[$k] = trim($v);
		}

		//echo http_build_query($aryAPI);
		//echo "<BR>";
		$encode_str = "API_Key=".ZOOMAPI_KEY."&".urldecode(http_build_query($aryAPI))."&API_Secret=".ZOOMAPI_SECRET;
		//echo $encode_str;
		//echo "<BR>";
		$encode_str = strtolower($encode_str);
		//echo $encode_str;
		//echo "<BR>";
		$CheckMacValue = strtoupper(md5($encode_str));
		//echo $CheckMacValue;
		//echo "<BR>";
		return $CheckMacValue;
	}

	/**
	 * 取得​使用者​清單
	 * @return [type] [description]
	 */
	public function user_list(){
		$data["api"] = "user_list";
		$data["page_size"] = 30;
		$data["page_number"] = 1;

		
		$this->callService($data);	
	}

	/**
	 * 取得單一使用者資料
	 * @return [type] [description]
	 */
	public function user_get(){
		$data["api"] = "user_get";
		$data["id"] = ZOOMAPI_USER_ID;
	

		
		$this->callService($data);	
	}

    //建立直播
	public function meeting_create(){
        //將直播資訊加入資料庫
        $course_id = $this->input->post("course_id");
        $meeting_type = (int)$this->input->post("type");
        $meeting_topic = $this->input->post("topic");
        $teacher_id = $this->session->userdata('user_id');
        $meeting_description = $this->input->post("description");
        $meeting_create_date = mdate("%Y-%m-%d %H:%i:%s",time());
        $meeting_start_time = $this->input->post("date");
        $meeting_duration = (int)$this->input->post("duration");
        $meeting_password = $this->input->post("password");
        $meeting_data = array(
            'course_id'=>$course_id,
            'type'=>$meeting_type,
            'teacher_id'=>$teacher_id,
            'topic'=>$meeting_topic,
            'description'=>$meeting_description,
            'create_date'=>$meeting_create_date,
            'start_time'=>$meeting_start_time,
            'duration'=>$meeting_duration,
            'timezone'=>'Asia/Taipei',
            'password'=>$meeting_password,
            'host_id'=>ZOOMAPI_USER_ID
        );
        $new_meeting_id = $this->course_model->createCourseMeeting($meeting_data);
        $other_data['meeting_id'] = $new_meeting_id;
        $other_data['create_date'] = $meeting_create_date;
        //呼叫 ZOOM API 建立直播
        $data["api"] = "meeting_create";
        //$data["description"] = $meeting_description;
        $data["duration"] = $meeting_duration;
		$data["host_id"] = ZOOMAPI_USER_ID;
		$data["password"] = $meeting_password;
        $data["start_time"] = $meeting_start_time;
        $data["timezone"] = "Asia/Taipei";
        $data["topic"] = $meeting_topic;	
        $data["type"] = $meeting_type;
		
		// $data["option_jbh"] = "";
		// $data["option_start_type"] = "";
		// $data["option_host_video"] = "";
		// $data["option_participants_video"] = "";
		// $data["option_audio"] = "";
		$this->callService($data, $other_data);	
	}

	/**
	 * 刪除直播
	 * 會議開始後無法執行 會回傳3002錯誤
	 * @return [type] [description]
	 */
	public function meeting_delete(){
        $meeting_id = $this->input->post("meeting_id");
        $other_data['meeting_id'] = $meeting_id;
        $meeting_room_id = $this->input->post("meeting_room_id");
        $this->course_model->removeMeeting($meeting_id);
		$data["api"] = "meeting_delete";
		$data["host_id"] = ZOOMAPI_USER_ID;
		$data["id"] = $meeting_room_id;
		
		$this->callService($data, $other_data);	
	}


	/**
	 * 取得會議列表
	 * @return [type] [description]
	 */
	public function meeting_list(){
		$data["api"] = "meeting_list";
		$data["host_id"] = ZOOMAPI_USER_ID;
		
		$this->callService($data);	
	}

	/**
	 * 取得單一會議
	 * 會議開始後無法執行 會回傳3002錯誤
	 * @return [type] [description]
	 */
	public function meeting_get($meeting_id){
		$data["api"] = "meeting_get";
		$data["host_id"] = ZOOMAPI_USER_ID;
		$data["id"] = $meeting_id;
		
		$this->callService($data);	
	}

	//更新直播
	public function meeting_update(){
        $meeting_id = $this->input->post("meeting_id");
        $course_id = $this->input->post("course_id");
        $meeting_type = (int)$this->input->post("type");
        $meeting_topic = $this->input->post("topic");
        $teacher_id = $this->session->userdata('user_id');
        $meeting_description = $this->input->post("description");
        $meeting_start_time = $this->input->post("date");
        $meeting_duration = (int)$this->input->post("duration");
        $meeting_password = $this->input->post("password");
        $meeting_room_id = $this->input->post("meeting_room_id");
        $this->course_model->updateCourseMeeting($course_id, $teacher_id, $meeting_topic, $meeting_description, $meeting_start_time, $meeting_id);
        
        $other_data['meeting_id'] = $meeting_id;
        
        $data["api"] = "meeting_update";
        $data["duration"] = $meeting_duration;
		$data["host_id"] = ZOOMAPI_USER_ID;
		$data["id"] = $meeting_room_id;
		$data["topic"] = $meeting_topic;		
        $data["password"] = $meeting_password;
        $data["start_time"] = $meeting_start_time;
		$data["type"] = $meeting_type;

		$this->callService($data, $other_data);	
	}
	/**
	 * 結束會議
	 * 當會議已經啟動就需要透過此API結束會議
	 * @param  [type] $meeting_id [description]
	 * @return [type]             [description]
	 */
	public function meeting_end($meeting_id){
		$data["api"] = "meeting_end";
		$data["host_id"] = ZOOMAPI_USER_ID;
		$data["id"] = $meeting_id;
		
		$this->callService($data);	
	}
}
