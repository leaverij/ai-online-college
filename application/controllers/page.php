<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
		$this->load->model('page_model');
		$this->load->model('mail_model');
		$this->load->model('library_model');
		$this->load->library('user_agent');
    }

	function index()
	{
	}
	//關於我們
	function about()
	{
		$data['title']='about';
		$data['listTeam'] = $this->page_model->listTeam();
		$this->theme_model->loadTheme('page/about',$data);
	}
	//隱私權 privacy
	function privacy()
	{
		$data['title']='隱私權保護政策';
		$this->theme_model->loadTheme('page/privacy',$data);
	} 
	//服務條款terms
	function terms()
	{
		$data['title']='服務條款';
		$this->theme_model->loadTheme('page/terms',$data);
	} 
	//個人資料蒐集規範
	function enrollAgreement(){
		$data['title']='個人資料蒐集規範';
		$this->theme_model->loadTheme('page/enrollAgreement',$data);
	}
	//sendgrid測試申請用
	function sendgrid(){
		// $this->load->library('email');		
		// $this->email->from('noreply@good2u.com.tw', 'change_test_mail');
		// $this->email->to('jcz16@hotmail.com');
		// $this->email->subject('歡迎您加入LerningHouse');
		// $this->email->message('這是來自learninghouse的測試郵件');
		// $this->email->send();
	}
	//問題反應
	function service()
	{
		$data['title']='問題反應';
		$this->theme_model->loadTheme('page/service',$data);
	}
	//訊息頁面，目前有驗證碼失效頁面 redirect('page/message/noEditPermission');
	function message($str){
		$data['title'] = $str;
		$data['header'] = '';
		if($str=='errorCode'){
			$data['message'] = '驗證碼已失效，請點選<a href="'.base_url().'user/forgetPassword"> 忘記密碼 </a>重新寄送[重設密碼]信！';
		}else if($str=='sendMail'){
			$data['message'] = '重設密碼信已經寄到您的註冊信箱'.$this->session->flashdata('sendMail').'了！';
		}else if($str=='updatePassword'){
			$data['message'] = '密碼已經更新成功，請使用新密碼重新登入！';
		}else if($str=='activeMailFailure'){
			$data['message'] = '信箱認證失敗，請重新認證！';
		}else if($str=='activeMailSuccess'){
			$data['message'] = '信箱認證成功，歡迎您加入AI Online College跟我們一同學習！';
			$data['header'] = '<meta http-equiv="Refresh" content="2; URL='.base_url().'library">';
		}else if($str=='activedMail'){
			$data['message'] = '信箱已經認證成功過了，歡迎您加入AI Online College跟我們一同學習！';
			$data['header'] = '<meta http-equiv="Refresh" content="2; URL='.base_url().'library">';
		}else if($str=='sendActiveMail'){
			$data['message'] = '恭喜您註冊成功。系統已經自動寄發了一封認證信到您的email信箱。<br>您可以直接開始瀏覽，或稍候至您的信箱做email認證。<br>如果信箱填寫錯誤導致沒有收到認證信，可到個人資料區重新修改！';
			$data['header'] = '<meta http-equiv="Refresh" content="5; URL='.base_url().'library">';
		}else if($str=='track'){
			$data['message'] = '目前沒有此學程地圖喔！';
			$data['header'] = '<meta http-equiv="Refresh" content="5; URL='.base_url().'track/domain/'.$this->uri->segment(4).'">';
		}else if($str=='permission'){
			$data['message'] = '您沒有編輯的權限喔！';
			$data['header'] = '<meta http-equiv="Refresh" content="5; URL='.base_url().'library">';
		}else if($str=='modifyPassword'){
			$data['message'] = '密碼已經更新成功！';
			$data['header'] = '<meta http-equiv="Refresh" content="1; URL='.base_url().'profile/'.$this->uri->segment(4).'">';
		}else if($str=='errorPassword'){
			$data['message'] = '您輸入的密碼有誤，請重新輸入！';
			$data['header'] = '<meta http-equiv="Refresh" content="2; URL='.base_url().'profile/'.$this->uri->segment(4).'">';
		}
		$this->theme_model->loadTheme('page/message',$data);
	}
	function inputBatchEmail(){
		$data['title']='批次寄信';
		//檢查有無權限
		$this->user_model->isBacthEmail();
		if($this->input->post("action")=="batchEmail"){
			echo '<meta charset="utf-8">';
			$name = $this->input->post("name");
			$email = $this->input->post("email");
			$password = $this->input->post("password");
			$arr_name = explode(",", $name);
			$arr_email = explode(",", $email);
			$arr_password = explode(",", $password);
			$length = count($arr_name);
			if($length>100){
				$length = 100;
			}
			for($i=0;$i<$length;$i++){
				$inputName = trim($arr_name[$i]);
				$inputEmail = trim($arr_email[$i]);
				$inputPassword= trim($arr_password[$i]);
				$checkmail = $this->user_model->checkEmail($inputEmail);
				if($checkmail=='0'){//該email尚未被註冊
					$activeCode = $this->theme_model->hash_password($checkmail);
					$alias = strstr($inputEmail, '@', true);
        			$j = 10;
        			while ($j > 0) {
            			$j--;
            			$checkalias = $this->user_model->checkAlias($alias);
            			if($checkalias=='1'){
                			$alias = $alias.rand(1,20);
                			$checkalias = $this->user_model->checkAlias($alias);
                			if($checkalias=='0'){
                    			break;
                			}
            			}
        			}
					$password_hashed = $this->theme_model->hash_password($inputPassword);
					$data = array( 'name' => $inputName,
						'email' => $inputEmail,
                       	'alias' => $alias,
                       	'password' => $password_hashed,
                       	'register_date' => $this->theme_model->nowTime(),
                       	'last_login' => $this->theme_model->nowTime(),
                       	'active_code' => $activeCode,
                       	'active_flag' =>'0');
					//批次增加使用者
					$this -> user_model -> addUser($data);
					//開始批次寄認證信給使用者
        			$activeData = array('email' => $inputEmail,'active_code' => $activeCode);
        			$this->mail_model->sendBatchMail($activeData,$inputPassword);
        		}
			}
			redirect('page/inputBatchEmail');
		}else{
			$this->theme_model->loadTheme('page/inputBatchEmail',$data);
		}
		
	}
	//virtualab
	function vl()
	{
		$data['title']='about';
		//$data['listTeam'] = $this->page_model->listTeam();
		$this->theme_model->loadTheme('page/vl',$data);
	}

	//實作體驗
	function gym(){
		$data['title']='實作';
		$data['getAllJobsCategory'] = json_encode($this->library_model->getAllJobsCategory()->result());
		$data['getAll'] = json_encode($this->library_model->getAll()->result());
		$data['getLabChapterContent'] = json_encode($this->library_model->getLabChapterContent()->result());
		$this->theme_model->loadTheme('page/gym',$data);
	}

	//實作體驗單元
	function gym_unit($chapter_content_id){
		$data['title'] = '實作';
		$data['getSingleChapterContent'] = json_encode($this->library_model->getSingleChapterContent($chapter_content_id)->result());
		$this->theme_model->loadTheme('course/gym_course',$data);
	}

	//AI準備度檢測
	function preparation(){
		$data['title'] = 'AI準備度測驗';
		$data['assessments'] = json_encode($this->page_model->listAllAIAssessment()->result());
		$data['r_user_id'] = uniqid();
		$this->theme_model->loadTheme('page/ai_preparation', $data);
	}

	//AI準備度檢測作答
	function ai_assessment(){
		$userId = $this->uri->segment(4);
		$assessment_url = $this->uri->segment(3);
		$assessment_content = $this->page_model->listAllAIAssessmentByUrl($assessment_url)->row_array();
		$checkUserStartAIAssessment = $this->page_model->checkUserStartAIAssessment($userId, $assessment_content['id'])->num_rows();
		if($checkUserStartAIAssessment==0){
			$access_ai_assessment = array(
				'user_id'=>$userId, 
				'assessment_id'=>$assessment_content['id'],
				'start_date' => mdate("%Y-%m-%d %H:%i:%s",time())
			);
			$this->page_model->insertAIAssessmentUser($access_ai_assessment);
		}
		$data['title'] = $assessment_content['name'];
		$data['assessment'] = json_encode($assessment_content);
		$data['user_id'] = $userId;
		$this->theme_model->loadTheme('page/ai_assessment', $data);
	}

	//AI準備度檢測報告
	function aiassessmentreport($assessment_url, $assessment_id, $user_id){
		$assessment_content = $this->page_model->listAllAIAssessmentById($assessment_id)->row_array();
		$data['title'] = $assessment_content['name'];
		$data['assessment'] = json_encode($assessment_content);
		// $data['getDropdownNotification']=json_encode($this->notifications_model->getDropdownNotification($this->session->userdata('email'))->result());
    	$data['getUnreadNotification']=$this->notifications_model->getUnreadNotification($this->session->userdata('email'));
    	$data['getUserForumSubscription']=json_encode($this->forum_subscription_model->getUserForumSubscription($this->session->userdata('email'))->result());
    	if($this->forum_assistant_model->getUserSupportLibraryId($this->session->userdata('user_id'))->num_rows()>0){
	    	$data['getUserSupportLibraryId']=json_encode($this->forum_assistant_model->getUserSupportLibraryId($this->session->userdata('user_id'))->result());
		}
		$data['user_id'] = $user_id;
		$data['assessment_submit'] = json_encode($this->page_model->getUserAIAssessmentSubmitResult($assessment_id, $user_id)->row_array());
		if($assessment_url=="ai-definition"){
			$data['score'] = $this->page_model->getAIAssessmentScoreByUserIdDef($assessment_id, $user_id)->row()->score;
		}else{
			$data['score'] = $this->page_model->getAIAssessmentScoreByUserId($assessment_id, $user_id)->row()->score;
		}
		if($assessment_url=="ai-definition"){
			$data['capability'] = json_encode($this->page_model->getAIAssessmentCapabilityDef($assessment_id, $user_id)->result());
		}else{
			$data['capability'] = json_encode($this->page_model->getAIAssessmentCapability($assessment_id, $user_id)->result());
		}
		$this->load->view('page/ai_assessment_report', $data);
	}

	//繳交AI準備度檢測
	/*
	function submitAIAssessment(){
		$assessment_id = $this->security->xss_clean($this->input->post('assessment_id'));
			$user_id = $this->security->xss_clean($this->input->post('user_id'));
			$question_id = $this->security->xss_clean($this->input->post('question_id'));
			$answer = $this->security->xss_clean($this->input->post('answer'));
			$answer_encode = $this->security->xss_clean($this->input->post('answer_encode'));
		$is_correct = $this->security->xss_clean($this->input->post('is_correct'));
		$is_correct_bool = filter_var($is_correct, FILTER_VALIDATE_BOOLEAN);
			$user_assessment_answer = array(
		  'assessment_id'=>$assessment_id,
		  'question_id'=>$question_id,
				'user_id'=>$user_id, 		  		  
		  'answer'=>$answer,
		  'answer_date' => mdate("%Y-%m-%d %H:%i:%s",time()),
		  'answer_encode'=>$answer_encode,
		  'is_correct'=>$is_correct_bool
		);
		$this->page_model->userAnswerAIAssessmentQuestion($user_assessment_answer);
		$submit_date = mdate("%Y-%m-%d %H:%i:%s",time());
		$user_submit = array(
		  'assessment_id'=>$assessment_id,
		  'user_id'=>$user_id,
		  'submit_date'=>$submit_date
		);
		$this->page_model->userSubmitAIAssessment($user_submit);
  
		$assessment_content = $this->page_model->listAllAIAssessmentById($assessment_id)->row_array();
		$data['title'] = $assessment_content['name'];
		$data['assessment'] = json_encode($assessment_content);
		$data['getDropdownNotification']=json_encode($this->notifications_model->getDropdownNotification($this->session->userdata('email'))->result());
		$data['getUnreadNotification']=$this->notifications_model->getUnreadNotification($this->session->userdata('email'));
		$data['getUserForumSubscription']=json_encode($this->forum_subscription_model->getUserForumSubscription($this->session->userdata('email'))->result());
		if($this->forum_assistant_model->getUserSupportLibraryId($this->session->userdata('user_id'))->num_rows()>0){
			$data['getUserSupportLibraryId']=json_encode($this->forum_assistant_model->getUserSupportLibraryId($this->session->userdata('user_id'))->result());
		}
		$data['user_id'] = $user_id;
		$data['submit_date'] = $submit_date;
		$this->load->view('page/ai_assessment_report', $data);
	}
	*/

}

/* End of file page.php */
/* Location: ./application/controllers/page.php */