<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');  
require(APPPATH.'libraries/REST_Controller.php');

class Datasets extends REST_Controller {
	function __construct(){
		parent::__construct();
        $this->load->model('datasets_model');
        $this->load->library('upload');
    }
	
    function index_get(){
    	$this->user_model->isLogin();
       	$data['title']="Datasets";
       	$user_id = $this->session->userdata('user_id');
       	$alias = $this->session->userdata('alias');
       	$dir = "./assets/datasets/".$alias;
       	$dir .= "/*.tar.gz";
       	foreach (glob($dir) as $filename) {
       		unlink($filename);
      	}
		$data['readAll'] = json_encode($this->datasets_model->readAll($user_id));
        $this->theme_model->loadTheme('datasets/index',$data);
	}
	function create_get(){
		$this->user_model->isLogin();
       	$data['title']="Create Datasets";
        $this->theme_model->loadTheme('datasets/create',$data);
	}
	function create_post(){
		$user_id = $this->session->userdata('user_id');
		$alias = $this->session->userdata('alias');
		// 欲新增的datasets名稱
		$datasets_name = $this->input->post('datasets_name',TRUE);
		// 檢查datasets是否有$alias的資料夾，如果沒有，新增$alias資料夾
		if(! is_dir('./assets/datasets/'.$alias)){
			mkdir('./assets/datasets/'.$alias, 0777);
		};
		// 在$alias資料夾內新增$datasets_name資料夾
		mkdir('./assets/datasets/'.$alias.'/'.$datasets_name, 0777);
		$datasets_desc = $this->input->post('datasets_desc',TRUE);
		$open = $this->input->post('open',TRUE);
		$datasets = array(
			'datasets_name' => $datasets_name,
			'datasets_desc'=>$datasets_desc,
			'open'=>$open,
			'user_id'=>$user_id);
		$this->datasets_model->create($datasets);
		redirect('datasets');
	}
	function listAll_get($user_alias,$datasets_name){
		$this->user_model->isLogin();
       	$data['title']="Datasets";
       	$user_id = $this->session->userdata('user_id');
       	$alias = $this->session->userdata('alias');
       	$data['datasets_name'] = $datasets_name;
       	$decode_datasets_name = urldecode($datasets_name);

       	// check $datasets_name是否是public
       	// if true => ok; false => private
       	$dir = "./assets/datasets/".$user_alias."/".$decode_datasets_name;
       	if($user_alias==$alias){
       		$data['self']=true;
       	}else{
       		$data['self']=false;
			$isPrivate=$this->datasets_model->is_private_datasets($user_alias,$datasets_name);
			if($isPrivate==1){
				echo json_encode(array("success"=>"sorry,private"),JSON_UNESCAPED_UNICODE);
    			die;
			}
       	}

		if (file_exists($dir)) {
    		$data['filemtime'] = date ("Y-m-d H:i:s", filemtime($dir));
		} else {
    		echo json_encode(array("success"=>"資料夾不存在"),JSON_UNESCAPED_UNICODE);
    		die;
		}
		$dir .= "/*";
       	$datasets = array();
       	$totalsize=0;
       	foreach (glob($dir) as $filename) {
    		if(!is_dir($filename)){
   				$temp = array(
					'filename' => basename($filename),
					'filepath'=> str_replace("./","",$filename),
					'filesize'=> $this->formatSizeUnits(filesize($filename))
				);
   				array_push($datasets, $temp);
   				$totalsize=$totalsize+filesize($filename);
   			}
		}
		$data['totalsize']=$this->formatSizeUnits($totalsize);
		$data['decode_datasets_name'] = $decode_datasets_name;
      	$data['alias'] = $user_alias;
       	$data['datasets_info'] = $datasets;
        $this->theme_model->loadTheme('datasets/list_datasets',$data);
	}
	function check_datasets_name_post(){
		$user_id = $this->session->userdata('user_id');
		$datasets_name = $this->input->post('datasets_name',TRUE);
		echo $this->datasets_model->check_datasets_name($datasets_name,$user_id);
	}
	function dl_get($alias,$datasets_name){
        try{
        	$decode_datasets_name = urldecode($datasets_name);
    		$dir = "./assets/datasets/".$alias;
       		$dir .= "/*.tar.gz";
       		foreach (glob($dir) as $filename) {
       			unlink($filename);
      		}
       		$rand = random_string('alnum',8);
    		$a = new PharData('./assets/datasets/'.$alias.'/'.$rand.'.tar');
    		// ADD FILES TO archive.tar FILE
    		$dir = "./assets/datasets/".$alias."/".$decode_datasets_name;
       		$a->buildFromDirectory($dir);
    		$a->compress(Phar::GZ);
    		unlink('./assets/datasets/'.$alias.'/'.$rand.'.tar');
    		$filename=$rand.'.tar.gz';
    		$temp = "./assets/datasets/".$alias."/".$filename;
    		header("Content-disposition: attachment; filename=$filename");
			header("Content-type: application/tar.gz");
			readfile($temp);
		}catch (Exception $e){
			echo "Exception : " . $e;
		}

	}
	function upload_post($datasets_name){
		header("Content-Type: text/plain");
		$alias = $this->session->userdata('alias');
		$decode_datasets_name=urldecode($datasets_name);
        $dir = "./assets/datasets/".$alias.'/'.$decode_datasets_name;
        $config['upload_path'] = $dir;
		$config['allowed_types'] = 'ipynb|jpg|png|csv|zip|txt|jpeg';
		$config['max_size']	= '5120';
        $this->load->library('upload',$config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload("qqfile")){
           	echo json_encode(array("success"=> false, "error"=> $this->upload->display_errors()),JSON_UNESCAPED_UNICODE);
		}else{
			echo json_encode(array("success"=>true));
		}
	}
	// 檔案的單位轉換
	function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824){
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }elseif ($bytes >= 1048576){
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }elseif ($bytes >= 1024){
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }elseif ($bytes > 1){
            $bytes = $bytes . ' bytes';
        }elseif ($bytes == 1){
            $bytes = $bytes . ' byte';
        }else{
            $bytes = '0 bytes';
        }
        return $bytes;
	}
	function listAll_delete($user_alias,$datasets_name){
		$this->user_model->isLogin();
		$user_id = $this->session->userdata('user_id');
		$alias = $this->session->userdata('alias');
		if($user_alias!=$alias) die;
		$decode_datasets_name = urldecode($datasets_name);
       	$dir = "./assets/datasets/".$user_alias."/".$decode_datasets_name;
		if (file_exists($dir)) {
			$this->rrmdir($dir);
			$data=array('delete_flag' =>1);
			$this->datasets_model->update($user_id,$decode_datasets_name,$data);
			echo json_encode(array("success"=>true)); 
		}else{
			echo json_encode(array("success"=>false)); 
		}
       	
	}
	function rrmdir($dir) { 
		if (is_dir($dir)) { 
			$objects = scandir($dir); 
			foreach ($objects as $object) { 
				if ($object != "." && $object != "..") { 
					if (is_dir($dir."/".$object))
						rrmdir($dir."/".$object);
					else
						unlink($dir."/".$object); 
				} 
			}
			rmdir($dir); 
		} 
	}
	function explore_get(){
    	$this->user_model->isLogin();
       	$data['title']="Explore Datasets";
       	$user_id = $this->session->userdata('user_id');
       	$data['listPublic'] = json_encode($this->datasets_model->listPublic($user_id));
       	$this->theme_model->loadTheme('datasets/explore',$data);

	}
	function singleFile_get($user_alias,$datasets_name,$file){
		$this->user_model->isLogin();
       	$data['title']="Datasets";
       	$user_id = $this->session->userdata('user_id');
       	$alias = $this->session->userdata('alias');
       	if($user_alias==$alias){
       	
       	}else{
			$isPrivate=$this->datasets_model->is_private_datasets($user_alias,$datasets_name);
			if($isPrivate==1){
				echo json_encode(array("success"=>"sorry,private"),JSON_UNESCAPED_UNICODE);
    			die;
			}
       	}
       	$data['datasets_name'] = $datasets_name;
       	$decode_datasets_name = urldecode($datasets_name);
       	$data['user_alias'] = $user_alias;
       	$data['file']=$file;
       	$display_url = base_url().'assets/datasets/'.$user_alias.'/'.$decode_datasets_name.'/'.urldecode($file);
       	$url = 'assets/datasets/'.$user_alias.'/'.$decode_datasets_name.'/'.urldecode($file);
       	if (file_exists($url)) {
    		// echo json_encode(array("success"=>true));
		} else {
    		echo json_encode(array("success"=>"檔案不存在"),JSON_UNESCAPED_UNICODE);
    		die;
		}
		$data['display_url'] = $display_url;
       	$dir = "./assets/datasets/".$user_alias."/".$decode_datasets_name;
       	if (file_exists($dir)) {
    		$data['filemtime'] = date ("Y-m-d H:i:s", filemtime($dir));
		} else {
    		echo json_encode(array("success"=>"資料夾不存在"),JSON_UNESCAPED_UNICODE);
    		die;
		}
       	// 'ipynb|jpg|png|csv|zip|txt|jpeg';
		$ext = pathinfo($display_url, PATHINFO_EXTENSION);
		switch (strtolower($ext)) {
   			case "jpg":
   			case "jpeg":
   			case "png":
     			$data['ext']='img';
     		break;
   			case "ipynb":
     			$data['ext']='ipynb';
     		break;
     		case "csv":
     			$data['ext']='csv';
     		break;
     		case "zip":
     			$data['ext']='zip';
     		break;
     		case "txt":
     			$data['ext']='txt';
     		break;
   			default:
     			$data['ext']='txt';
		}
        $this->theme_model->loadTheme('datasets/single_file',$data);
	}
}

/* End of file datasets.php */
/* Location: ./application/controllers/datasets.php */