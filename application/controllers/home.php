<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
		$this->load->model('user_model');
		$this->load->model('course_model');
		$this->load->model('library_model');
		$this->load->model('points_model');
    }

	function index()
	{
		if($this->session->userdata('email')){
			redirect('library');
		}
		$data['title']='首頁';
		$data['amount'] = $this->user_model->amount()->row_array();
		$data['partialCourse'] = json_encode($this->course_model->getPartialCourse()->result());
		$data['getAll'] = json_encode($this->library_model->getAll()->result());
		$this->load->view('message');
		$this->theme_model->loadTheme('home/home',$data);
	}
	function registration()
	{
		$data['title']='註冊';
		$this->theme_model->loadTheme('home/registration',$data);
	}
	function logout()
	{
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('alias');
		$this->session->unset_userdata('user_id');
		//提示訊息
		$this->theme_model->flash_session("alert-success","您已經登出");
		//清掉FB session
		if($this->session->userdata('fb_login')):
			$this->load->library('facebook');
			$this->facebook->destroySession();
		endif;			
		redirect('/home');
	}
	function fb_logout()
	{
		$fb=$this->session->userdata('fb_data');
        $this->session->unset_userdata('fb_data');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('alias');
		$this->session->unset_userdata('user_id');
		redirect($fb['logoutUrl']);
	}
	function login()
	{
		$data['title']='登入';
		if($this->session->userdata('email')){
			redirect('library');
		}
		
		$action=$this->security->xss_clean($this->input->post("action"));
		$requestURL=$this->security->xss_clean($this->input->post("requestURL"));
		$email=$this->security->xss_clean($this->input->post("inputEmail"));
		$password=$this->security->xss_clean($this->input->post("inputPassword"));
		if($action=='login'){
			//信箱或密碼有空值(前端有js擋住了，如繞過js這邊會再確認)
			if($email==null ||$password==null){
				//提示訊息
				$this->theme_model->flash_session("alert-danger","登入資料不完整");
				redirect('home/login');
			}else{
				$getOneUser = $this->user_model->getOneUser($email);
				$row=$getOneUser->row_array();
				$count= $row['count'];
				$dbPassword = $row['password'];
				
				//把email丟入驗證coupon的有效時間
					//$check_time = $this->user_model->checkCouponTime($email)->row_array();
				//檢查coupon開始時間小於今日
					//$checkCouponLtTime = $this->user_model->checkCouponLtTime($email)->row_array();
				//把email丟入驗證信用卡的有效時間
					//還沒寫---->信用卡的方法
				$check = $this->theme_model->compare_password($password,$dbPassword);
				//信箱密碼輸入正確 
				if($check){
					//if($check_time){
						$this->theme_model->set_session($email);
						//提示訊息
						$this->theme_model->flash_session("alert-success","登入成功，歡迎回來");
						//改寫登入次數與登入時間
						$count=$count+1;
						$last_login = $this->theme_model->nowTime();
						$data = array(	'last_login' => $last_login,
					              		'count' =>$count);
						$this -> user_model -> updateUser($email,$data);
						if($requestURL==(base_url().'home/login') || $requestURL==(base_url().'home/registration') || $requestURL==(base_url().'home')|| $requestURL==(base_url().'page/message/updatePassword')){
							$this->session->unset_userdata('src');
							redirect('library/ai');
						}else{
							$this->session->unset_userdata('src');
							redirect($requestURL);
						}
					// }elseif($checkCouponLtTime){
						// if($checkCouponLtTime['start_time'] > $this->theme_model->nowTime()){
							// $this->theme_model->flash_session("alert-danger","帳號啟用時間尚未開始!");
							// //可以重導到重新輸入coupon那頁，還沒寫
							// redirect('home');
						// }
					// }else{
						// $this->theme_model->flash_session("alert-danger","帳號有效時間已過，請重新輸入優惠條碼或使用信用卡!");
						// //重導到重新輸入coupon那頁
						// $this->session->set_userdata('temp_email', $email);
						// redirect('user/updateAccountStatus');
					// }
				}else{
					//信箱密碼輸入錯誤
					$this->theme_model->flash_session("alert-danger","信箱或密碼不正確，請重新輸入");
					redirect('home/login');
				}
			}
		}
		$this->load->library('facebook');
        $user = $this->facebook->getUser();
		////
		if ($user) {
		  try {
		    // Proceed knowing you have a logged in user who's authenticated.
		    $user_profile = $this->facebook->api('/me');
		    //set_fb_session
		    $fb_data = array(  			
			'fb_login'  => '1',
			'fb_logout_url' => $this->facebook->getLogoutUrl(array('next' => base_url().'home/logout'))
			);
			$this->session->set_userdata($fb_data);
			
		  } catch (FacebookApiException $e) {
		    error_log($e);
		    $user = null;
		    //unset_fb_session
		    $this->session->unset_userdata('fb_login');
		    $this->session->unset_userdata('fb_logout_url');
		  }
		}
		if ($user):
            //$data['logout_url'] = $this->facebook->getLogoutUrl();
        	$check_email = $this->user_model->checkFbEmail($user_profile['email']);
			//get alias
			$alias = strstr($user_profile['email'], '@', true);
			$i = 10;
			while ($i > 0) {
				$i--;
				$checkalias = $this->user_model->checkAlias($alias);
				if($checkalias=='1'){
					$alias = $alias.rand(1,20);
					$checkalias = $this->user_model->checkAlias($alias);
				if($checkalias=='0'){
					break;
				}
			}
			}
			$now = $this->theme_model->nowTime();
			$row = $check_email->row();
			$count = $row->count;
			$count = $count+1;
			if($check_email->num_rows()==0)://資料庫無資料_新用戶 
				$fb_insert_data = array(	
					'name' => $user_profile['name'], 
					'email' => $user_profile['email'],
					'alias' => $alias,
					'last_login' => $now,
					'register_date' => $now,
					'fb_id' => $user_profile['id'],
					'active_flag'=>'1');
				$this->user_model->fb_insert_new($fb_insert_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				//設定session
				$this->theme_model->set_session($user_profile['email']);
				//提示訊息
				//$this->ym->flash_session("alert-success",'Facebook註冊成功，歡迎您');	
				redirect('library');
			elseif($row->email==$user_profile['email']&&$row->fb_id=='')://資料庫有mail，但尚未綁定fb帳號
				$fb_update_data=array(
					'fb_id'=>$user_profile['id'],
					'last_login'=> $now,
					'count' =>$count,
					'active_flag'=>'1');
				$this->user_model->fb_update($user_profile['email'],$fb_update_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				//設定session
				$this->theme_model->set_session($user_profile['email']);
				//提示訊息
				//$this->ym->flash_session("alert-success","成功綁定 email ");	
				redirect('library');
			elseif($row->email==$user_profile['email']&&$row->fb_id==$user_profile['id']):
				$fb_update_data=array(
					'last_login'=> $now,
					'count' =>$count,
                    'active_flag'=>'1');
				$this->user_model->fb_update($user_profile['email'],$fb_update_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				$this->theme_model->set_session($user_profile['email']);
				redirect('library');
			endif;
        else:
            $data['login_url'] = $this->facebook->getLoginUrl(array('scope' => 'email'));
        endif;
		$this->theme_model->loadTheme('home/login',$data);
	}
	//取得線上總人數
	function updateAmount(){
		echo json_encode($this->user_model->amount()->result());
	}
	//新增積分分數
	function insertPoint(){
		$course_id ='';
		$chapter_id ='';
		$content_id ='';
		$forum_content_id = '';
		$chapter_content_url = $this->input->post("chapter_content_url");
		$getCourseChapterID = $this->points_model->getCourseChapterID($chapter_content_url);
		$course_id = $getCourseChapterID->course_id;
		$chapter_id = $getCourseChapterID->chapter_id;
		$content_id = $getCourseChapterID->chapter_content_id;
		$user_id = $this->session->userdata('user_id'); 
		$completed = $this->input->post("completed");
		$this->points_model->insertPoint($user_id,$completed,$course_id,$chapter_id,$content_id,$forum_content_id);
	}
	function modalLogin()
	{
		//$action=$this->security->xss_clean($this->input->post("action"));
		$requestURL=$this->security->xss_clean($this->input->post("requestURL"));
		$email=$this->security->xss_clean($this->input->post("inputEmail"));
		$password=$this->security->xss_clean($this->input->post("inputPassword"));
		
		//信箱或密碼有空值(前端有js擋住了，如繞過js這邊會再確認)
		if($email==null ||$password==null){
			//提示訊息
			$this->theme_model->flash_session("alert-danger","登入資料不完整");
			echo '登入資料不完整';die();
		}else{
			$getOneUser = $this->user_model->getOneUser($email);
			$row=$getOneUser->row_array();
			$count= $row['count'];
			$dbPassword = $row['password'];
			
			//把email丟入驗證coupon的有效時間
				//$check_time = $this->user_model->checkCouponTime($email)->row_array();
			//檢查coupon開始時間小於今日
				//$checkCouponLtTime = $this->user_model->checkCouponLtTime($email)->row_array();
			//把email丟入驗證信用卡的有效時間
				//還沒寫---->信用卡的方法
			$check = $this->theme_model->compare_password($password,$dbPassword);
			//信箱密碼輸入正確 
			if($check){
				//if($check_time){
					$this->theme_model->set_session($email);
					//提示訊息
					$this->theme_model->flash_session("alert-success","登入成功，歡迎回來");
					//改寫登入次數與登入時間
					$count=$count+1;
					$last_login = $this->theme_model->nowTime();
					$data = array(	'last_login' => $last_login,
				              		'count' =>$count);
					$this -> user_model -> updateUser($email,$data);
					if($requestURL==(base_url().'home/login') || $requestURL==(base_url().'home/registration') || $requestURL==(base_url().'home')|| $requestURL==(base_url().'page/message/updatePassword')){
						$this->session->unset_userdata('src');
						echo 'login success';
						die();
					}else{
						$this->session->unset_userdata('src');
						echo 'login success';
						die();
					}
				// }elseif($checkCouponLtTime){
					// if($checkCouponLtTime['start_time'] > $this->theme_model->nowTime()){
						// $this->theme_model->flash_session("alert-danger","帳號啟用時間尚未開始!");
						// //可以重導到重新輸入coupon那頁，還沒寫
						// redirect('home');
					// }
				// }else{
					// $this->theme_model->flash_session("alert-danger","帳號有效時間已過，請重新輸入優惠條碼或使用信用卡!");
					// //重導到重新輸入coupon那頁
					// $this->session->set_userdata('temp_email', $email);
					// redirect('user/updateAccountStatus');
				// }
			}else{
				//信箱密碼輸入錯誤
				$this->theme_model->flash_session("alert-danger","信箱或密碼不正確，請重新輸入");
				echo 'login fail';
				die();
			}
		}
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */