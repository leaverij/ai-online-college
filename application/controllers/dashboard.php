<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
		$this->load->model('user_model');
		$this->load->model('course_model');
		$this->load->model('chapter_model');
		$this->load->model('content_model');
		$this->load->model('library_model');
		$this->load->model('user_course_model');
		$this->load->model('user_chapter_model');
		$this->load->model('user_content_model');
		$this->load->model('dashboard_model');
		$this->load->model('accesslog_model');
    }

	function index(){
		$data['title']='個人儀表板';
		$this->user_model->isLogin();
		$data=$this->getUserCourseProgress($data,'dashboard');
		$email=$this->session->userdata('email');
		$data['getUserActivity']=$this->getUserActivity($email);
		$data['getUserAwardVideo']=$this->getUserAwardVideo($email);
		$this->theme_model->loadTheme('dashboard/myDashboard',$data);
	}

	//主管dashboard
	function admin(){
		$data['title']='主管儀表板';
		$this->user_model->isLogin();
		$this->user_model->isBackend();
		$track_counts_query = $this->dashboard_model->getAllTrackCounts()->row();
		$data['track_counts'] = $track_counts_query->track_counts;
		$course_counts_query = $this->dashboard_model->getAllCourseCounts()->row();
		$data['course_counts'] = $course_counts_query->course_counts;
		$teacher_counts_query = $this->dashboard_model->getAllTeacherCounts()->row();
		$data['teacher_counts'] = $teacher_counts_query->teacher_counts;
		$student_counts_query = $this->dashboard_model->getAllStudentCounts()->row();
		$data['student_counts'] = $student_counts_query->student_counts;

		$date_from = date('Y-01-01');
		$date_end = date('Y-12-31');

		// 使用率
		$usage_array = array(
			array('y'=>'1', 'a'=>0, 'b'=> 0),
			array('y'=>'2', 'a'=>0, 'b'=> 0),
			array('y'=>'3', 'a'=>0, 'b'=> 0),
			array('y'=>'4', 'a'=>0, 'b'=> 0),
			array('y'=>'5', 'a'=>0, 'b'=> 0),
			array('y'=>'6', 'a'=>0, 'b'=> 0),
			array('y'=>'7', 'a'=>0, 'b'=> 0),
			array('y'=>'8', 'a'=>0, 'b'=> 0),
			array('y'=>'9', 'a'=>0, 'b'=> 0),
			array('y'=>'10', 'a'=>0, 'b'=> 0),
			array('y'=>'11', 'a'=>0, 'b'=> 0),
			array('y'=>'12', 'a'=>0, 'b'=> 0)
		);
		$usage_register = $this->dashboard_model->getRegisterUsage($date_from, $date_end);
		$usage_login = $this->dashboard_model->getLoginUsage($date_from, $date_end);
		foreach($usage_array as $index=>$usage){
			foreach($usage_register as $register){
				if($register['month']==$usage['y']){
					$usage_array[$index]['a']=(int)$register['count'];
				}
			}
			foreach($usage_login as $login){
				if($login['month']==$usage['y']){
					$usage_array[$index]['b']=(int)$login['count'];
				}
			}
			$usage_array[$index]['y'] = $usage_array[$index]['y'].'月';
		}
		$data['usage'] = json_encode($usage_array);

		// 裝置比例
		$device_ratio_array = array(
			array('label'=>'Web', 'value'=>0),
			array('label'=>'Android', 'value'=>0),
			array('label'=>'iOS', 'value'=>0),
			array('label'=>'Others', 'value'=>0)
		);
		$device_ratio = $this->dashboard_model->getDeviceRatio($date_from, $date_end);
		foreach($device_ratio_array as $index=>$ratio_array){
			foreach($device_ratio as $ratio){
				switch ($ratio['os']) {
					case 'Windows 10';
					case 'Windows 8.1';
					case 'Windows 8';
					case 'Windows 7';
					case 'Windows Vista';
					case 'Windows Server 2003/XP x64';
					case 'Windows XP';
					case 'Windows 2000';
					case 'Windows ME';
					case 'Windows 98';
					case 'Windows 95';
					case 'Mac OS X';
					case 'Mac OS 9';
					case 'Linux';
					case 'Ubuntu';
						if($ratio_array['label']=='Web'){
							$device_ratio_array[$index]['value'] = $device_ratio_array[$index]['value'] + $ratio['count'];
						}
						break;
					case 'iPhone';
					case 'iPod';
					case 'iPad';
						if($ratio_array['label']=='iOS'){
							$device_ratio_array[$index]['value'] = $device_ratio_array[$index]['value'] + $ratio['count'];
						}
						break;
					case 'Android';
						if($ratio_array['label']=='Android'){
							$device_ratio_array[$index]['value'] = $device_ratio_array[$index]['value'] + $ratio['count'];
						}
						break;
					case 'BlackBerry';
					case 'Mobile';
					case 'Unknown OS Platform';
						if($ratio_array['label']=='Others'){
							$device_ratio_array[$index]['value'] = $device_ratio_array[$index]['value'] + $ratio['count'];
						}
						break;
				}
			}			
		}
		$data['device_ratio'] = json_encode($device_ratio_array);

		// 領域學程完成比例
		$complete_ratio = $this->dashboard_model->getCompleteRatio($date_from, $date_end);
		$data['complete_ratio'] = json_encode($complete_ratio);

		// 教師績效
		$teacher_performance = $this->dashboard_model->getTeacherPerformance($date_from, $date_end);
		$data['teacher_performance'] = $teacher_performance;

		$this->theme_model->loadTheme('dashboard/adminDashboard', $data);
	}

	//教師dashboard
	function teacher(){
		$data['title']='教師儀表板';
		$this->user_model->isLogin();
		$this->user_model->isTeacher();
		$user_id = $this->session->userdata('user_id');
		//課程數
		$course_counts_query = $this->dashboard_model->getAllCourseCountsByTeacher($user_id)->row();
		$data['course_counts'] = $course_counts_query->course_counts;
		//學生數
		$student_counts_query = $this->dashboard_model->getAllStudentCountsByTeacher($user_id)->row();
		$data['student_counts'] = $student_counts_query->student_counts;
		//Top 5 課程
		$data['top_courses'] = json_encode($this->dashboard_model->getTopCourseStudentCountsByTeacher($user_id, 5)->result(),JSON_UNESCAPED_UNICODE);
		
		$date_from = date('Y-01-01');
		$date_end = date('Y-12-31');
		//課程概況
		$course_summary = $this->dashboard_model->getTeachedCourseSummary($date_from, $date_end, $user_id);
		$data['course_summary'] = $course_summary;

		$this->theme_model->loadTheme('dashboard/teacherDashboard',$data);
	}

	//課程dashboard
	function course($course_id){
		$data['title']='課程儀表板';
		$user_id = $this->session->userdata('user_id');
		$data['course'] = json_encode($this->course_model->getTeachedCourse($user_id, $course_id)->row_array());

		$course_chapter_result = $this->chapter_model->teacherGetChapters($course_id, $user_id)->result_array();
		$couser_student_result = $this->course_model->getStudentByCourseid($course_id)->result_array();

		$course_chapter = array();
		$chapter_content = array();
		$couser_student = array();
		$student_index = 0;

		foreach($course_chapter_result as $index=>$chapter){
			// 關卡通過率
			$user_chapter = $this->chapter_model->getUserChapterCount($chapter['chapter_id'])->row_array();
			$course_chapter['categories'][$index] = $chapter['chapter_name'];
			$course_chapter['finish'][$index] = 0;
			if($user_chapter['ucfinish']!=''){
				$course_chapter['finish'][$index] = (int)$user_chapter['ucfinish'];
			}
			$course_chapter['unfinish'][$index] = (int)($user_chapter['ucjoin'] - $user_chapter['ucfinish']);

			// 關卡/任務概況 - 點擊次數
			$chapter_content_result = $this->content_model->getOneChapterContentByChapterId($chapter['chapter_id'])->result_array();
			$chapter_content['chapter'][$index]['name'] = $chapter['chapter_name'];
			
			$accesslog_result = $this->accesslog_model->getaccessLogByChapterId($chapter['chapter_id'])->result_array();
			// print_r($chapter_content_result);
			foreach($chapter_content_result as $con_index=>$content){
				$student_index ++;
				$chapter_content['chapter_content_name'][] = $content['chapter_content_desc'];
				$chapter_content['chapter_content_type'][] = $content['content_type'];
				$chapter_content['chapter'][$index]['span'] = $con_index + 1;

				foreach($couser_student_result as $csindex=>$student){
					$couser_student[$csindex][0] = $student['user_name'];
					$couser_student[$csindex][$student_index] = 0;
					foreach($accesslog_result as $accesslog){
						if($accesslog['user_id']==$student['user_id'] && $content['chapter_content_id']==$accesslog['chapter_content_id']){
							$couser_student[$csindex][$student_index] = $accesslog['click_num'];
						}
					}
				}
			}
		}

		// 關卡通過率
		$data['course_chapter'] = json_encode($course_chapter);

		// 關卡/任務概況 - 點擊次數
		$data['chapter_content'] = $chapter_content;
		$data['chapter_content']['couser_student'] = $couser_student;


		$this->theme_model->loadTheme('dashboard/courseDashboard',$data);
	}

	//測驗dashboard
	function assessment(){
		$data['title']='測驗儀表板';
		$this->theme_model->loadTheme('dashboard/visca_assessment', $data);
	}
	
	function getUserCourseProgress($data,$from){
		$email	= 	$this->session->userdata('email');
		//只列出未完成的課程進度
		if($from=='dashboard'){
			$option1=array(	'user.email'		=>$email,
							'course.course_type'=>'1',
							'user_course.status'=>'0');	
			$option2=array(	'user.email'		=>$email,
							'course.course_type'=>'2',
							'user_course.status'=>'0');
		$techniques=$this->user_course_model->getUserCourse($option1);
		$knowledges=$this->user_course_model->getUserCourse($option2);
		$data['techniques']=json_encode($techniques->result());
		$data['knowledges']=json_encode($knowledges->result());
		$data['numOfKnowledges']=$knowledges->num_rows();
		$data['numOfTechniques']=$techniques->num_rows();
		//列出所有課程進度
		}elseif($from=='library'){
			$option1=array(	'user.email'		=>$email,
							'course.course_type'=>'1');	
			$option2=array(	'user.email'		=>$email,
							'course.course_type'=>'2');
		$techniques=$this->library_model->getOneLibrary($data['library'],'1');
		$knowledges=$this->library_model->getOneLibrary($data['library'],'2');
		$data['techniques']=json_encode($techniques->result());
		$data['knowledges']=json_encode($knowledges->result());
		$data['numOfKnowledges']=$knowledges->num_rows();
		$data['numOfTechniques']=$techniques->num_rows();
		}
		//取得各課程下的徽章數量
		$data['numOfCourseBadges']		=json_encode($this->getCourseBadges($knowledges,$techniques));
		//取得使用者各課程的徽章數量
		$data['numOfUserCourseBadges']	=json_encode($this->getUserCourseBadges($knowledges,$techniques));
		return $data;
	}
	
	function getCourseBadges($knowledges,$techniques){
		$courseBadges=array();
		foreach($knowledges->result() as $key=>$value){
			$courseBadge=new stdClass();
			$courseBadge->course_url	=$value->course_url;
			$courseBadge->numOfBadges	=$this->chapter_model->getCourseBadges($value->course_url);
			array_push($courseBadges,$courseBadge);
		}
		foreach($techniques->result() as $key=>$value){
			$courseBadge=new stdClass();
			$courseBadge->course_url	=$value->course_url;
			$courseBadge->numOfBadges	=$this->chapter_model->getCourseBadges($value->course_url);
			array_push($courseBadges,$courseBadge);
		}
		return $courseBadges;
	}
	
	function getUserCourseBadges($knowledges,$techniques){
		$email	= 	$this->session->userdata('email');
		$userCourseBadges=array();
		foreach($knowledges->result() as $key=>$value){
			$userCourseBadge=new stdClass();
			$option3=array(	'user.email'			=>$email,
							'course.course_url'		=>$value->course_url,			
							'user_chapter.status'	=>'1');						
			$userCourseBadge->course_url	=$value->course_url;
			$userCourseBadge->numOfBadges	=$this->user_chapter_model->getUserCourseBadges($option3);
			array_push($userCourseBadges,$userCourseBadge);
		}
		foreach($techniques->result() as $key=>$value){
			$userCourseBadge=new stdClass();
			$option3=array(	'user.email'			=>$email,
							'course.course_url'		=>$value->course_url,			
							'user_chapter.status'	=>'1');						
			$userCourseBadge->course_url	=$value->course_url;
			$userCourseBadge->numOfBadges	=$this->user_chapter_model->getUserCourseBadges($option3);
			array_push($userCourseBadges,$userCourseBadge);
		}
		return $userCourseBadges;
	}
	
	function recrodContent()
	{
		//post from oneChapterContent.php
		$user_email	= 	$this->session->userdata('email');
		$chapter_content_url=	$this->input->post('chapter_content_url');
		if($user_email){
		$user	=$this->user_model->getOneUser($user_email)->result();
		$record_url = $this->course_model->getCourseChapterContentId($chapter_content_url)->result(); 
		print_r($record_url);
		$user_id	=$user['0']->user_id;
		$course_id  =$record_url['0']->course_id;
		$chapter_id =$record_url['0']->chapter_id;
		$content_id =$record_url['0']->chapter_content_id;
		//user_course
		$user_course	=array(	'user_id'			=>$user_id,
								'course_id'			=>$course_id);
		if(($newCourse=$this->user_course_model->isCourseSubscribed($user_course))<1){
			$this->user_course_model->addUserCourse($user_course);
		}
		$user_course_id=$this->user_course_model->getUserCourseId($user_course);
		$user_course_id=$user_course_id['0']->user_course_id;
		//user_chapter
		$user_chapter	=array(	'user_course_id'	=>$user_course_id,
								'chapter_id'		=>$chapter_id);
		if(($newChapter=$this->user_chapter_model->isChapterSubscribed($user_chapter))<1){
			$this->user_chapter_model->addUserChapter($user_chapter);
		}
		$user_chapter_id=$this->user_chapter_model->getUserChapterId($user_chapter);
		$user_chapter_id=$user_chapter_id['0']->user_chapter_id;
		//user_content
		$user_content	=array(	'user_chapter_id'	=>$user_chapter_id,
								'chapter_content_id'=>$content_id);
		$this->user_content_model->addUserContent($user_content);
		}else{
			$data = array($chapter_content_url  => $chapter_content_url);
			$this->session->set_userdata($data);
		}
	}
	
	function getUserActivity($email){
		$getUserActivity=json_encode($this->user_content_model->getUserActivity($email)->result());
		return $getUserActivity;
	}
	
	function getUserAwardVideo($email){
		$getUserAwardVideo=json_encode($this->user_course_model->getUserAwardVideo($email)->result());
		return $getUserAwardVideo;
	}

	//課程直播分析
	function meeting(){
		$data['title'] = '課程直播分析';
		$this->user_model->isLogin();
		$this->user_model->isBackend();
		$date_from = date('Y-01-01');
		$date_end = date('Y-12-31');

		// 使用率
		$usage_array = array(
			array('y'=>'1', 'a'=>0, 'b'=> 0),
			array('y'=>'2', 'a'=>0, 'b'=> 0),
			array('y'=>'3', 'a'=>0, 'b'=> 0),
			array('y'=>'4', 'a'=>0, 'b'=> 0),
			array('y'=>'5', 'a'=>0, 'b'=> 0),
			array('y'=>'6', 'a'=>0, 'b'=> 0),
			array('y'=>'7', 'a'=>0, 'b'=> 0),
			array('y'=>'8', 'a'=>0, 'b'=> 0),
			array('y'=>'9', 'a'=>0, 'b'=> 0),
			array('y'=>'10', 'a'=>0, 'b'=> 0),
			array('y'=>'11', 'a'=>0, 'b'=> 0),
			array('y'=>'12', 'a'=>0, 'b'=> 0)
		);
		$usage_register = $this->dashboard_model->getRegisterUsage($date_from, $date_end);
		$usage_login = $this->dashboard_model->getLoginUsage($date_from, $date_end);
		foreach($usage_array as $index=>$usage){
			foreach($usage_register as $register){
				if($register['month']==$usage['y']){
					$usage_array[$index]['a']=(int)$register['count'];
				}
			}
			foreach($usage_login as $login){
				if($login['month']==$usage['y']){
					$usage_array[$index]['b']=(int)$login['count'];
				}
			}
			$usage_array[$index]['y'] = $usage_array[$index]['y'].'月';
		}
		$data['usage'] = json_encode($usage_array);
		$this->theme_model->loadTheme('dashboard/meeting', $data);
	}

}
/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */