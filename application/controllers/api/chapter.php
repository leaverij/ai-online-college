<?php

header('Access-Control-Allow-Origin: *');  
require(APPPATH.'libraries/REST_Controller.php');
class Chapter extends REST_Controller {

    public function __construct()
    {
      parent::__construct();
      $this->load->model('chapter_model');
    }
  
    public function index_get()
	{
		  echo 'Chapter RESTful API';
    }

    //更新關卡名稱
    function updateChapterName_post(){
        $chapter_name = $this->security->xss_clean($this->input->post('chapter_name'));
        $chapter_id = $this->security->xss_clean($this->input->post('chapter_id'));
        $this->chapter_model->updateChapterName($chapter_id,$chapter_name);
        $this->response(array('success'=>true));
    }

    //建立新關卡
    function createNewChapter_post(){
        $chapter_name = $this->security->xss_clean($this->input->post('new_chapter_name'));
        $course_id = $this->security->xss_clean($this->input->post('new_course_id'));
        $chapter_content = array(
            'chapter_name'=>$chapter_name,
            'chapter_pic'=>'LH-default-stage.png',
            'chapter_diff'=>0,
            'chapter_desc'=>'',
            'chapter_url'=>uniqid(),
            'create_time'=>mdate("%Y-%m-%d %H:%i:%s",time()),
            'course_id'=>$course_id
        );
        $last_id = $this->chapter_model->createNewChapter($chapter_content);
        $this->response(array('success'=>true,'id'=>$last_id,'name'=>$chapter_name));
    }

    //刪除關卡
    function removeChapter_post(){
        $chapter_id = $this->security->xss_clean($this->input->post('chapter_id'));
        $this->chapter_model->removeChapter($chapter_id);
        $this->response(array('success'=>true));
    }

    //更新關卡順序
    function chapterReorder_post(){
        $reorderArr = json_decode($this->security->xss_clean($this->input->post('reorderArr')), true);
        foreach($reorderArr as $key => $co){
            $this->chapter_model->updateChapterOrder($co, $key+1);
        }
        $this->response(array('success'=>true));
    }

}

?>