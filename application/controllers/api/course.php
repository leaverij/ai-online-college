<?php

header('Access-Control-Allow-Origin: *');  
require(APPPATH.'libraries/REST_Controller.php');
class Course extends REST_Controller {

    public function __construct()
    {
      parent::__construct();
      $this->load->model('course_model');
      $this->load->model('library_model');
    }
  
    public function index_get()
	{
		  echo 'Course RESTful API';
    }

    //建立新課程
    function createCourse_post(){
        $user_id = $this->session->userdata('user_id');
        $course_name = $this->security->xss_clean($this->input->post('name'));
        if($course_name){
            $library = $this->security->xss_clean($this->input->post('library'));
            $course_category = $this->security->xss_clean($this->input->post('category'));
            $course_content = array(
                'course_name'=>$course_name,
                'course_pic'=>'LH-default-course.png',
                'course_type'=>2,
                'course_desc'=>'',
                'course_price'=>0,
                'course_url'=>uniqid(),
                'learning_target'=>'',
                'suitable_for'=>'',
                'prior_knowledge'=>'',
                'references'=>'',
                'create_time'=>mdate("%Y-%m-%d %H:%i:%s",time()),
                'status'=>'0',
                'sync_course'=>'0',
                'teacher_id'=>$user_id,
                'library_id'=>$library,
                'course_category_id'=>$course_category
            );
            $new_course_id = $this->course_model->createCourse($course_content);
            $data['title'] = '課程基本資訊';
            $data['course'] = json_encode($this->course_model->getTeachedCourse($user_id, $new_course_id)->row_array());
            $data['library'] = json_encode($this->library_model->getAllLibrary()->result());
            $data['category'] = json_encode($this->library_model->getAllCourseCategory()->result()); 
            $this->theme_model->loadTheme('course/edit_course_info', $data);
            redirect('course/edit/'.$new_course_id.'/info');
        }else{
            $this->theme_model->flash_session("alert-danger","請輸入課程名稱");
            redirect('course/create');
        }
    }

    //更新課程基本資訊
    function editCourseInfo_post(){
        $teacher_id = $this->session->userdata('user_id');
        $course_id = $this->security->xss_clean($this->input->post('course_id'));
        $course_name = $this->security->xss_clean($this->input->post('course_name'));
        if($course_name){
            $course_library = $this->security->xss_clean($this->input->post('course_library'));
            $course_category = $this->security->xss_clean($this->input->post('course_category'));
            $status = $this->security->xss_clean($this->input->post('course_status'));
            $this->course_model->updateCourseInfo($course_id,$teacher_id,$course_name,$course_library,$course_category,$status);
            $this->response(array('result'=>"success"));
        }else{
            // $this->theme_model->flash_session("alert-danger","請輸入課程名稱");
            // redirect('course/edit/'.$course_id.'/info');
            $this->response(array('result'=>"fail"));
        }
    }

    //更新課程價格
    function editCoursePrice_post(){
        $teacher_id = $this->session->userdata('user_id');
        $course_id = $this->security->xss_clean($this->input->post('course_id'));
        $course_price_method = $this->security->xss_clean($this->input->post('course_price_method'));
        $course_price_val = $this->security->xss_clean($this->input->post('course_price_val'));
        $course_price = 0;
        if($course_price_method==1){
            $course_price = $course_price_val;
        }
        $this->course_model->updateCoursePrice($course_id, $course_price);
        $this->response(array('success'=>true));
    }

    //更新課程公告
    function updateCourseAnnouncement_post(){
        $teacher_id = $this->session->userdata('user_id');
        $course_id = $this->security->xss_clean($this->input->post('course_id'));
        $announcement_id = $this->security->xss_clean($this->input->post('announcement_id'));
        $announcement_title = $this->security->xss_clean($this->input->post('title'));
        $announcement_content = $this->security->xss_clean($this->input->post('content'));
        $create_date = mdate("%Y-%m-%d %H:%i:%s",time());
        $announce_date = $create_date;
        $announcement = array(
            'title'=>$announcement_title,
            'content'=>$announcement_content,
            'teacher_id'=>$teacher_id,
            'course_id'=>$course_id,
            'create_date'=>$create_date,
            'announce_date'=>$announce_date,
            'is_delete'=>'0',
            'view_counts'=>0
        );
        if(empty($announcement_id)){
            $new_announcement_id = $this->course_model->createCourseAnnouncement($announcement);
            $this->response(array(
                'success'=>true,
                'id'=>$new_announcement_id,
                'create_date'=>$create_date,
                'announce_date'=>$announce_date
            ));
        }else{
            $this->course_model->updateCourseAnnouncement($announcement_id, $announcement_title, $announcement_content);
            $existAnnouncement = $this->course_model->getSingleCourseAnnouncement($announcement_id)->row_array();
            $this->response(array(
                'success'=>true,
                'id'=>$existAnnouncement['id'],
                'create_date'=>$existAnnouncement['create_date'],
                'announce_date'=>$existAnnouncement['announce_date']
            ));
        }
    }

    //移除課程公告
    function removeAnnouncement_post(){
        $announcement_id = $this->security->xss_clean($this->input->post('announcement_id'));
        $this->course_model->removeAnnouncement($announcement_id);
        $this->response(array('success'=>true));
    }

    //建立課程直播
    function updateCourseMeeting_post(){
        $teacher_id = $this->session->userdata('user_id');
        $meeting_id = $this->security->xss_clean($this->input->post('meeting_id'));
        $course_id = $this->security->xss_clean($this->input->post('course_id'));
        $password = $this->security->xss_clean($this->input->post('password'));
        $meeting_topic = $this->security->xss_clean($this->input->post('topic'));
        $meeting_description = $this->security->xss_clean($this->input->post('description'));
        $start_time = $this->security->xss_clean($this->input->post('date'));
        $meeting_type = $this->security->xss_clean($this->input->post('type'));
        if(empty($meeting_id)){
            $create_date = mdate("%Y-%m-%d %H:%i:%s",time());
            $meeting = array(
                'course_id'=>$course_id,
                'type'=>$meeting_type,
                'teacher_id'=>$teacher_id,
                'topic'=>$meeting_topic,
                'description'=>$meeting_description,
                'create_date'=>$create_date,
                'start_time'=>$start_time,
                'duration'=>30,
                'timezone'=>'',
                'password'=>$password
            );
            $new_meeting_id = $this->course_model->createCourseMeeting($meeting);
            $this->response(array(
                'success'=>true,
                'id'=>$new_meeting_id,
                'create_date'=>$create_date,
                'meeting_date'=>$start_time
            ));
        }else{
            $this->course_model->updateCourseMeeting($course_id, $teacher_id, $meeting_topic, $meeting_description, $start_time);
            $exist_meeting = $this->course_model->getSingleCourseMeeting($meeting_id)->row_array();
            $this->response(array(
                'success'=>true,
                'id'=>$exist_meeting['id'],
                'create_date'=>$exist_meeting['create_date'],
                'meeting_date'=>$exist_meeting['start_time']
            ));
        }
    }

    //移除課程直播
    function removeMeeting_post(){
        $meeting_id = $this->security->xss_clean($this->input->post('meeting_id'));
        $this->course_model->removeMeeting($meeting_id);
        $this->response(array('success'=>true));
    }

    //建立學生分組
    function createNewGroup_post(){
        $teacher_id = $this->session->userdata('user_id');
        $group_name = $this->security->xss_clean($this->input->post('group_name'));
        if(empty($group_name)){
            $group_name = '新的分組';
        }
        $course_id = $this->security->xss_clean($this->input->post('course_id'));
        $create_date = mdate("%Y-%m-%d %H:%i:%s",time());
        $content_group = array(
            'name'=>$group_name,
            'create_time'=>$create_date,
            'course_id'=>$course_id,
            'teacher_id'=>$teacher_id
        );
        $new_group_id = $this->course_model->createNewCourseGroup($content_group);
        $this->response(array(
            'success'=>true,
            'id'=>$new_group_id,
            'name'=>$group_name,
            'create_date'=>$create_date
        ));
    }

    //更新課程群組學生
    function updateGroupStudent_post(){
        $user_id = $this->security->xss_clean($this->input->post('user_id'));
        $content_group_id = $this->security->xss_clean($this->input->post('content_group_id'));
        $checked = $this->security->xss_clean($this->input->post('checked'));
        if($checked=='check'){
            $data = array(
                'user_id'=>$user_id,
                'content_group_id'=>$content_group_id
            );
            $this->course_model->addCourseGroupStudent($data);
        }else{
            $this->course_model->removeCourseGroupStudent($user_id, $content_group_id);
        }
    }

    //教師刪除課程分組
    function removeCourseGroup_post(){
        $teacher_id = $this->session->userdata('user_id');
        $course_id = $this->security->xss_clean($this->input->post('course_id'));
        $content_group_id = $this->security->xss_clean($this->input->post('content_group_id'));
        $this->course_model->removeCourseGroup($content_group_id, $teacher_id, $course_id);
        $this->response(array('success'=>true));
    }

}

?>