<?php

header('Access-Control-Allow-Origin: *');  
require(APPPATH.'libraries/REST_Controller.php');
class Content extends REST_Controller {

    public function __construct()
    {
      parent::__construct();
      $this->load->model('content_model');
    }
  
    public function index_get()
	{
		  echo 'Chapter Content RESTful API';
    }

    //更新任務名稱
    function updateChapterContentName_post(){
        $chapter_content_name = $this->security->xss_clean($this->input->post('chapter_content_name'));
        $chapter_content_id = $this->security->xss_clean($this->input->post('chapter_content_id'));
        $this->content_model->updateChapterContentName($chapter_content_id, $chapter_content_name);
        $this->response(array('success'=>true));
    }

    //刪除任務名稱
    function removeChapterContent_post(){
        $chapter_content_id = $this->security->xss_clean($this->input->post('chapter_content_id'));
        $this->content_model->removeChapterContent($chapter_content_id);
        $this->response(array('success'=>true));
    }

    //取得任務內容
    function getChapterContent_get(){
        $course_id = $this->security->xss_clean($this->input->get('course_id'));
        $chapter_content_id = $this->security->xss_clean($this->input->get('chapter_content_id'));
        $data = $this->content_model->getChapterContentByChapterContentId($chapter_content_id)->row_array();
        if($data['content_type']=='4' && $data['lab_type']=='1'){ //團體實作
            $data['lab_groups'] = $this->content_model->getChapterContentLabGroup($course_id, $chapter_content_id)->result_array();
        }
        $this->response($data);
    }
    //取得任務內容
    function getQuiz_get(){
        $chapter_content_id = $this->security->xss_clean($this->input->get('chapter_content_id'));
        $data = $this->content_model->getQuiz($chapter_content_id)->result();
        $this->response($data);
    }    

    //新增任務
    function createNewChapterContent_post(){
        $chapter_id = $this->security->xss_clean($this->input->post('chapter_id'));
        $chapter_content_order = $this->content_model->getTotalChapterContentCounts($chapter_id)->row()->counts;
        $chapter_content_order++;
        $chapter_content_title = $this->security->xss_clean($this->input->post('title'));
        $chapter_content = $this->security->xss_clean($this->input->post('content'));
        $content_desc = $this->security->xss_clean($this->input->post('content_desc'));
        // 編譯
        $right_code_content = $this->security->xss_clean($this->input->post('right_code_content'));
        $hint = $this->security->xss_clean($this->input->post('hint'));
        // 判斷是否測驗
        $apiquiz = $this->security->xss_clean($this->input->post('apiquiz'));
        $question_array = $this->security->xss_clean($this->input->post('question_array'));
        $correct_answer_array = $this->security->xss_clean($this->input->post('correct_answer_array'));
        $origin_answer_array = $this->security->xss_clean($this->input->post('origin_answer_array'));
        $option_content_array = $this->security->xss_clean($this->input->post('option_content_array'));
        $answer_array = $this->security->xss_clean($this->input->post('answer_array'));

        $chapter_content_type = $this->security->xss_clean($this->input->post('type'));
        $chapter_content_url = uniqid();
        $s3_name = $this->security->xss_clean($this->input->post('s3_name'));
        $s3_uuid = $this->security->xss_clean($this->input->post('s3_uuid'));
        $time_spent = $this->security->xss_clean($this->input->post('time_spent'));
        $video_duration = $this->security->xss_clean($this->input->post('video_duration'));
        
        if(empty($apiquiz)) $apiquiz = '0';
        if(empty($no_array)) $no_array = '0';
        if(empty($s3_name)) $s3_name = '';
        if(empty($s3_uuid)) $s3_uuid = '';
        if(empty($time_spent)) $time_spent = 0;
        if(empty($video_duration)) $video_duration = 0;
        if(empty($right_code_content)) $right_code_content = null;
        if(empty($hint)) $hint = null;
        $new_chapter_content = array(
            'chapter_content_desc'=>$chapter_content_title,
            'chapter_content_url'=>$chapter_content_url,
            'chapter_id'=>$chapter_id,
            'question'=>'',
            'content'=>$chapter_content,
            'subtitle'=>'',
            'content_type'=>$chapter_content_type,
            'preview'=>0,
            'hits'=>0,
            'user_hits'=>0,
            'duration'=>'',
            'snapshot'=>'',
            'content_open_flag'=>1,
            'content_order'=>$chapter_content_order,
            'award_video'=>0,
            'is_gym'=>0,
            'create_date'=>mdate("%Y-%m-%d %H:%i:%s",time()),
            'content_desc'=>$content_desc,
            'aws_s3_uuid'=>$s3_uuid,
            'aws_s3_name'=>$s3_name,
            'time_spent'=>$time_spent,
            'apiquiz'=>$apiquiz,
            'video_duration'=>$video_duration,
            'right_code_content'=>$right_code_content,
            'hint'=>$hint,
            'lab_type'=>($chapter_content_type==4)?($this->security->xss_clean($this->input->post('lab_type'))):0
        );
        
        
        // 題目
        // print_r($question_array);
        // print_r($correct_answer_array);
        // print_r($origin_answer_array);
        // print_r($option_content_array);
        // 選項
        
        
        $insertId = $this->content_model->createNewChapterContent($new_chapter_content);
        if($chapter_content_type==4 && ($this->security->xss_clean($this->input->post('lab_type')))==1){
            //團體實作任務
            $labGroups = json_decode($this->security->xss_clean($this->input->post('lab_group')), true);
            foreach($labGroups as $labGroup){
                $this->content_model->updateLabContentGroup(
                    $insertId, $labGroup['content_group_id'], $labGroup['is_active']
                );
            }
        }
        if($apiquiz==1){
            // 塞測驗 
            $new_quiz_detail = array(
                'quiz_detail_desc'=>$chapter_content_title,
                'quiz_detail_url'=>" ",
                'chapter_content_id'=>$insertId,
                'create_time'=>mdate("%Y-%m-%d %H:%i:%s",time()),
                'pass_conditions'=>count($question_array)-1,
                'quantity'=>count($question_array)
            );
            $insertQuizDetailId = $this->content_model->createNewQuizDetail($new_quiz_detail);
            // for
            for($i=0;$i<count($question_array);$i++){
                $new_question = array(
                    'question_content'=>$question_array[$i],
                    'question_type'=>0,
                    'question_hint'=>" ",
                    'correct_answer'=>$correct_answer_array[$i],
                    'origin_answer'=>$origin_answer_array[$i],
                    'score'=>1,
                    'quiz_detail_id'=>$insertQuizDetailId
                );
                $insertQuestionId = $this->content_model->createNewQuestion($new_question);
                // for
                for($j=0;$j<count($option_content_array[$i]);$j++){
                    $new_options = array(
                        'options_content'=>$option_content_array[$i][$j],
                        'answer'=>$answer_array[$i][$j],
                        'question_id'=>$insertQuestionId
                    );
                    $insertOptionsId = $this->content_model->createNewOptions($new_options);
                }
            }
        }
        
        $this->response(array(
            'success'=>true,
            'id'=>$insertId,
            'content_order'=>$chapter_content_order
        ));
    }

    //更新任務內容
    function updateChapterContent_post(){
        $chapter_content_id = $this->security->xss_clean($this->input->post('chapter_content_id'));
        $chapter_content = $this->security->xss_clean($this->input->post('content'));
        $this->content_model->updateChapterContent($chapter_content_id, $chapter_content);
        $this->response(array('success'=>true));
    }

    //更新任務內容說明
    function updateChapterContentDesc_post(){
        $chapter_content_id = $this->security->xss_clean($this->input->post('chapter_content_id'));
        $chapter_content_desc = $this->security->xss_clean($this->input->post('content_desc'));
        $this->content_model->updateChapterContentDesc($chapter_content_id, $chapter_content_desc);
        $this->response(array('success'=>true));
    }

    //更新實作任務
    function updateLabChapterContent_post(){
        $chapter_content_id = $this->security->xss_clean($this->input->post('chapter_content_id'));
        $chapter_content_desc = $this->security->xss_clean($this->input->post('content_desc'));
        $lab_type = $this->security->xss_clean($this->input->post('lab_type'));
        $this->content_model->updateChapterContentDesc($chapter_content_id, $chapter_content_desc);
        //團體實作任務
        if($lab_type==1){
            $labGroups = json_decode($this->security->xss_clean($this->input->post('lab_group')), true);
            foreach($labGroups as $labGroup){
                $this->content_model->updateLabContentGroup(
                    $chapter_content_id, $labGroup['content_group_id'], ($labGroup['is_active'])?'1':'0'
                );
            }
        }
        $this->response(array('success'=>true));
    }

    //更新編譯任務內容
    function updateCompileContent_post(){
        $chapter_content_id = $this->security->xss_clean($this->input->post('chapter_content_id'));
        $chapter_content_desc = $this->security->xss_clean($this->input->post('content_desc'));
        $content = $this->security->xss_clean($this->input->post('content'));
        $right_code_content = $this->security->xss_clean($this->input->post('right_code_content'));
        $hint = $this->security->xss_clean($this->input->post('hint'));
        if(empty($right_code_content)) $right_code_content = null;
        if(empty($hint)) $hint = null;
        $this->content_model->updateCompileContent($chapter_content_id, $chapter_content_desc, $content,$right_code_content,$hint);
        $this->response(array('success'=>true));
    }

    //更新任務順序
    function chapterContentReorder_post(){
        $reorderArr = json_decode($this->security->xss_clean($this->input->post('reorderArr')), true);
        foreach($reorderArr as $key => $co){
            $this->content_model->updateChapterContentOrder($co, $key+1);
        }
        $this->response(array('success'=>true));
    }

}

?>