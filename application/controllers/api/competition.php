<?php

header('Access-Control-Allow-Origin: *');  
require(APPPATH.'libraries/REST_Controller.php');

class Competition extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('competition_model');
        $this->load->model('datasets_model');
    }

    //建立新競賽
    function createCompetition_post(){
        $user_id = $this->session->userdata('user_id');
        $competition_name = $this->security->xss_clean($this->input->post('name'));
        $competition_subtitle = $this->security->xss_clean($this->input->post('subtitle'));
        $competition_topic = $this->security->xss_clean($this->input->post('topic'));
        $competition_category = $this->security->xss_clean($this->input->post('category'));
        if($competition_name && $competition_subtitle){
            $competition_content = array(
                'name' => $competition_name,
                'subtitle' => $competition_subtitle,
                'competition_category_id' => $competition_category,
                'competition_topic_id' => $competition_topic,
                'create_time' => mdate("%Y-%m-%d %H:%i:%s",time()),
                'creator_id' => $user_id
            );
            $new_competition_id = $this->competition_model->createNewCompetition($competition_content);
            $first_competition_detail = array(
                'title' => '競賽說明',
                'creator_id' => $user_id,
                'content' => '競賽說明',
                'create_time' => mdate("%Y-%m-%d %H:%i:%s",time()),
                'competition_id' => $new_competition_id,
                'is_publish' => true
            );
            $this->competition_model->createNewCompetitionDetail($first_competition_detail);
            $data['competition'] = json_encode($this->competition_model->getHostedCompetition($user_id, $new_competition_id)->row_array());
            /*
            $alias = $this->session->userdata('alias');
            if(! is_dir('./assets/datasets/'.$alias)){
                mkdir('./assets/datasets/'.$alias, 0777);
            };
            mkdir('./assets/datasets/'.$alias.'/'.$competition_name, 0777);
            $datasets = array(
                'datasets_name' => $competition_name,
                'datasets_desc'=>$competition_subtitle,
                'open'=>false,
                'user_id'=>$user_id,
                'competition_id'=>$new_competition_id);
            $this->datasets_model->create($datasets);
            */
            $this->theme_model->loadTheme('competition/edit_competition_info', $data);
            redirect('competition/edit/'.$new_competition_id.'/info');
        }else{
            $this->theme_model->flash_session("alert-danger","競賽名稱與副標題皆為必填");
            redirect('competitions/create');
        }
    }

    //修改競賽基本資訊
    function editCompetitionInfo_post(){
        $user_id = $this->session->userdata('user_id');
        $competition_id = $this->security->xss_clean($this->input->post('competition_id'));
        $competition_name = $this->security->xss_clean($this->input->post('competition_name'));
        $competition_subtitle = $this->security->xss_clean($this->input->post('competition_subtitle'));
        $competition_publish = $this->security->xss_clean($this->input->post('competition_publish'));
        $competition_starttime = $this->security->xss_clean($this->input->post('competition_starttime'));
        $competition_endtime = $this->security->xss_clean($this->input->post('competition_endtime'));
        $competition_deadline = $this->security->xss_clean($this->input->post('competition_deadline'));
        if($competition_name && $competition_subtitle){
            $competition_topic = $this->security->xss_clean($this->input->post('competition_topic'));
            $competition_category = $this->security->xss_clean($this->input->post('competition_category'));
            $old_competition = $this->competition_model->getHostedCompetition($user_id, $competition_id)->row_array();
            $this->competition_model->updateCompetitionInfo($competition_id, $user_id, $competition_name, $competition_subtitle, $competition_category, $competition_publish, $competition_starttime, $competition_endtime, $competition_deadline, $competition_topic);
            //$alias = $this->session->userdata('alias');
            //rename('./assets/datasets/'.$alias.'/'.$old_competition['name'], './assets/datasets/'.$alias.'/'.$competition_name);
            //$this->datasets_model->update_datasets_name_by_competition($competition_id, array('datasets_name' =>$competition_name));
            $this->response(array('result'=>"success"));
        }else{
            $this->response(array('result'=>"fail"));
        }
    }

    //建立或更新新競賽細節內容
    function updateCompetitionDetail_post(){
        $this->user_model->isLogin();
        $user_id = $this->session->userdata('user_id');
        $title = $this->security->xss_clean($this->input->post('title'));
        $competition_id = $this->security->xss_clean($this->input->post('competition_id'));
        $competition_detail_id = $this->security->xss_clean($this->input->post('competition_detail_id'));
        $content = $this->security->xss_clean($this->input->post('content'));
        $is_publish = $this->security->xss_clean($this->input->post('is_publish'));
        if(!empty($competition_detail_id)){
            $this->competition_model->updateCompetitionDetail($title, $content, $is_publish, $competition_detail_id, $competition_id, $user_id);
            $this->response(array(
                'success'=>true,
                'id'=>$competition_detail_id
            ));
        }else{
            $competition_detail = array(
                'title' => $title,
                'creator_id' => $user_id,
                'content' => $content,
                'create_time' => mdate("%Y-%m-%d %H:%i:%s",time()),
                'competition_id' => $competition_id,
                'is_publish' => $is_publish
            );
            $new_competition_detail_id = $this->competition_model->createNewCompetitionDetail($competition_detail);
            $this->response(array(
                'success'=>true,
                'id'=>$new_competition_detail_id
            ));
        }
    }

    //取得競賽類別
    function getCompetitionCategoryByTopicId_get(){
        $topic_id = $this->security->xss_clean($this->input->get('topic_id'));
        $categories = $this->competition_model->getCompetitionCatgoriesByTopicId($topic_id)->result_array();
        $this->response($categories);
    }

    //教師取得單一競賽細節
    function getOneCompetitionDetail_get(){
        $competition_id = $this->security->xss_clean($this->input->get('competition_id'));
        $competition_detail_id = $this->security->xss_clean($this->input->get('competition_detail_id'));
        $competiton_detail = $this->competition_model->getOneCompetitionDetail($competition_id, $competition_detail_id)->row_array();
        $this->response($competiton_detail);
    }

}

?>