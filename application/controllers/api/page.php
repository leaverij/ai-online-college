<?php

header('Access-Control-Allow-Origin: *');  
require(APPPATH.'libraries/REST_Controller.php');
class Page extends REST_Controller {

    public function __construct()
    {
      parent::__construct();
      $this->load->model('page_model');
    }
  
    public function index_get()
	  {
		  echo 'Page RESTful API';
    }

    function t_get(){
      $url = "https://westus.api.cognitive.microsoft.com/emotion/v1.0/recognize";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Ocp-Apim-Subscription-Key: c98f151978b948bd956c71c0cf3d61ee'
			));
			$img_data = array("url" => "https://www.askideas.com/media/24/Baby-Funny-Smile-Face-Image.jpg");                                                                    
      $data_string = json_encode($img_data, JSON_UNESCAPED_SLASHES);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);  
      $output = curl_exec($ch);
      $curl_errno = curl_errno($ch);
      if (curl_errno($ch)) {
        $content = 'ERROR -> ' . curl_errno($ch) . ': ' . curl_error($ch);
      } 
      curl_close($ch);
      echo $output;
    }

    //使用者回答AI準備度測驗問題
	  function userAnswerAIAssessmentQuestion_post(){
		  $assessment_id = $this->security->xss_clean($this->input->post('assessment_id'));
		  $user_id = $this->security->xss_clean($this->input->post('user_id'));
		  $question_id = $this->security->xss_clean($this->input->post('question_id'));
		  $answer = $this->security->xss_clean($this->input->post('answer'));
		  $answer_encode = $this->security->xss_clean($this->input->post('answer_encode'));
      $is_correct = $this->security->xss_clean($this->input->post('is_correct'));
      $is_correct_bool = filter_var($is_correct, FILTER_VALIDATE_BOOLEAN);
      $answer_score = $this->security->xss_clean($this->input->post('answer_score'));
      $sub_ability_name = $this->security->xss_clean($this->input->post('sub_ability_name'));
		  $user_assessment_answer = array(
        'assessment_id'=>$assessment_id,
        'question_id'=>$question_id,
			  'user_id'=>$user_id, 		  		  
        'answer'=>$answer,
        'answer_date' => mdate("%Y-%m-%d %H:%i:%s",time()),
        'answer_encode'=>$answer_encode,
        'is_correct'=>$is_correct_bool,
        'answer_score'=>$answer_score,
        'sub_ability_name'=>$sub_ability_name
      );
      $this->page_model->userAnswerAIAssessmentQuestion($user_assessment_answer);
		  $result = array('success'=>true);
		  $this->response($result);
    }

    //繳交AI準備度檢測
    function submitAIAssessment_post(){
      $assessment_id = $this->security->xss_clean($this->input->post('assessment_id'));
      $user_id = $this->security->xss_clean($this->input->post('user_id'));
      $question_id = $this->security->xss_clean($this->input->post('question_id'));
      $answer = $this->security->xss_clean($this->input->post('answer'));
      $answer_encode = $this->security->xss_clean($this->input->post('answer_encode'));
      $is_correct = $this->security->xss_clean($this->input->post('is_correct'));
      $is_correct_bool = filter_var($is_correct, FILTER_VALIDATE_BOOLEAN);
      $answer_score = $this->security->xss_clean($this->input->post('answer_score'));
      $sub_ability_name = $this->security->xss_clean($this->input->post('sub_ability_name'));
      $user_assessment_answer = array(
        'assessment_id'=>$assessment_id,
        'question_id'=>$question_id,
        'user_id'=>$user_id, 		  		  
        'answer'=>$answer,
        'answer_date' => mdate("%Y-%m-%d %H:%i:%s",time()),
        'answer_encode'=>$answer_encode,
        'is_correct'=>$is_correct_bool,
        'answer_score'=>$answer_score,
        'sub_ability_name'=>$sub_ability_name
      );
      $this->page_model->userAnswerAIAssessmentQuestion($user_assessment_answer);
      $submit_date = mdate("%Y-%m-%d %H:%i:%s",time());
      $user_submit = array(
        'assessment_id'=>$assessment_id,
        'user_id'=>$user_id,
        'submit_date'=>$submit_date
      );
      $this->page_model->userSubmitAIAssessment($user_submit);
      $result = array('success'=>true);
      $this->response($result);
    }
    
}

?>