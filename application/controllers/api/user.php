<?php

header('Access-Control-Allow-Origin: *');  
require(APPPATH.'libraries/REST_Controller.php');
class User extends REST_Controller {

    public function __construct()
    {
      parent::__construct();
      $this->load->model('library_model');
      $this->load->model('user_model');
    }
  
    public function index_get()
	{
		  echo 'Course RESTful API';
    }

    function editProfile_post(){
        
        $profile_data = array(
                    'name' => $this->security->xss_clean($this->input->post("name")),
                    'gender' => $this->security->xss_clean($this->input->post("gender")),
                    'mobile' => $this->security->xss_clean($this->input->post("mobile")),
                    'birthday' => $this->security->xss_clean($this->input->post("birthday")),
                    'experience' => preg_replace('/[\n\r\t]/','',nl2br($this->security->xss_clean($this->input->post("experience")))),
                    'education_highest_level' => $this->security->xss_clean($this->input->post("education_highest_level")),
        );
        if($profile_data){

            $this->user_model->updateUser($this->session->userdata('email'),$profile_data);
            $this->response(array('result'=>"success"));  
        }else{
            $this->response(array('result'=>"fail"));
        }
    }

}

?>