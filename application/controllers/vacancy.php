<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vacancy extends CI_Controller {
	
	function __construct(){
        parent::__construct();
        $this->load->model('job_vacancy_model');
        $this->load->model('tracks_model');
		$this->load->model('user_portfolio_model');
		$this->load->model('job_apply_model');
    }

	function index(){
		// $data['title']='企業端職缺維護';
		//檢查使用者是否登入
        $this->user_model->isLogin();
		redirect(base_url().'vacancy/listAllJobVacancy');
	}
	
	function addJobVacancy(){
		$data['title']='職缺設定';
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$data['getJobsCategory'] = json_encode($this->tracks_model->getJobsCategory()->result());
		$this->theme_model->loadTheme('vacancy/addJobVacancy',$data);
	}
	function insertJobVacancy(){
		$job_vacancy=array(	'job_name'			=>$this->security->xss_clean($this->input->post('job_name'			)),
							'jobs_category_id'	=>$this->security->xss_clean($this->input->post('jobs_category_id'	)),
							'address'			=>$this->security->xss_clean($this->input->post('address'			)),
							'job_content'		=>preg_replace('/[\n\r\t]/','',nl2br($this->security->xss_clean($this->input->post("job_content")))),
							'contact'			=>$this->security->xss_clean($this->input->post('contact'			)),
							'contact_email'		=>$this->security->xss_clean($this->input->post('contact_email'		)),
							'by_phone'			=>$this->security->xss_clean($this->input->post('by_phone'			)),
							'by_others'			=>preg_replace('/[\n\r\t]/','',nl2br($this->security->xss_clean($this->input->post("by_others")))),
							'match_county'		=>$this->security->xss_clean($this->input->post('match_county'		)),
							'match_district'	=>$this->security->xss_clean($this->input->post('match_district'	)),
							'degree_limit'		=>$this->security->xss_clean($this->input->post('degree_limit'		)),
							'working_experience'=>$this->security->xss_clean($this->input->post('working_experience')),
							'department_type_id'=>$this->security->xss_clean($this->input->post('department_type_id')),
							'skill'				=>$this->security->xss_clean($this->input->post('match_skill'				)),
							'license'			=>$this->security->xss_clean($this->input->post('match_license'			)),
							'additional_demand'	=>preg_replace('/[\n\r\t]/','',nl2br($this->security->xss_clean($this->input->post("additional_demand")))),
							'posted_time'		=>$this->theme_model->nowTime(),
							'lasted_update_time'=>$this->theme_model->nowTime(),
							'vacancy_open_flag'	=>'1',
							'user_id'			=>$this->session->userdata('user_id')
						);
		if ('insertJobVacancy'==$this->security->xss_clean($this->input->post('insertJobVacancy'	))) {
			$job_vacancy_id=$this->job_vacancy_model->addJobVacancy($job_vacancy);
			redirect(base_url().'vacancy/listAllJobVacancy');
		} elseif ('previewJobVacancy'==$this->security->xss_clean($this->input->post('previewJobVacancy'	))) {
			$this->session->set_userdata('job_vacancy',$job_vacancy);
			redirect(base_url().'vacancy/previewJobVacancy');
		} else{
		}
	}

	function listAllJobVacancy(){
		echo '<meta charset="utf-8">';
		$data['title']='管理職缺';
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$data['getOneUser'] = $this->user_model->getOneUser($this->session->userdata('email'))->row_array();
		$data['getJobVacancy']=$this->job_vacancy_model->getJobVacancy($this->session->userdata('user_id'))->result();
		$numOfJobVacancy=array();
		$numOfApplyPerson=array();
		for($i=0;$i<count($data['getJobVacancy']);$i++){
			$getVacancyMatchFactor=$this->job_vacancy_model->getVacancyMatchFactor($data['getJobVacancy'][$i]->job_vacancy_id);
			$compositeQueryJobVacancy=$this->job_vacancy_model->compositeQueryJobVacancy($getVacancyMatchFactor->result());
			$numOfJobVacancy[$i]=$compositeQueryJobVacancy->num_rows();
			$getApplyPerson = $this->job_vacancy_model->getApplyPerson($data['getJobVacancy'][$i]->job_vacancy_id);
			$numOfApplyPerson[$i]=$getApplyPerson->num_rows();
		}
		$data['numOfJobVacancy']=$numOfJobVacancy;
		$data['numOfApplyPerson']=$numOfApplyPerson;
		$this->theme_model->loadTheme('vacancy/listAllJobVacancy',$data);
	}

	function listApplyPerson($job_vacancy_id){
		$data['title']='主動應徵列表';
		//檢查使用者是否登入
		$this->user_model->isLogin();		
		$getVacancyMatchFactor=$this->job_vacancy_model->getVacancyMatchFactor($job_vacancy_id);
		$getPersonList=$this->job_vacancy_model->getApplyPerson($job_vacancy_id);
		$match_person=$getPersonList->result();
		$highest_degree=array();
		$current_job=array();
		for($i=0;$i<count($match_person);$i++){
			$getUserEducation=$this->user_portfolio_model->getHighestDegree($match_person[$i]->email)->row();
			$getCurrentJob=$this->user_portfolio_model->getCurrentJob($match_person[$i]->email)->row();
			$highest_degree[$i]=$getUserEducation;
			$current_job[$i]=$getCurrentJob;
		}
		$data['getVacancyMatchFactor']=$getVacancyMatchFactor->result();
		$data['highestDegree']=$highest_degree;
		$data['currentJob']=$current_job;
		$data['compositeQueryJobVacancy']=$getPersonList->result();
		$this->theme_model->loadTheme('vacancy/listApplyPerson',$data);
	}
	
	function getMatchPerson($job_vacancy_id){
		$data['title']='媒合列表';
		//檢查使用者是否登入
		$this->user_model->isLogin();
		echo '<meta charset="utf-8">';
		$getVacancyMatchFactor=$this->job_vacancy_model->getVacancyMatchFactor($job_vacancy_id);
		$compositeQueryJobVacancy=$this->job_vacancy_model->compositeQueryJobVacancy($getVacancyMatchFactor->result());
		$highest_degree=array();
		$current_job=array();
		$match_person=$compositeQueryJobVacancy->result();
		for($i=0;$i<count($match_person);$i++){
			$getUserEducation=$this->user_portfolio_model->getHighestDegree($match_person[$i]->email)->row();
			$getCurrentJob=$this->user_portfolio_model->getCurrentJob($match_person[$i]->email)->row();
			$highest_degree[$i]=$getUserEducation;
			$current_job[$i]=$getCurrentJob;
		}
		$data['highestDegree']=$highest_degree;
		$data['currentJob']=$current_job;
		$data['getVacancyMatchFactor']=$getVacancyMatchFactor->result();
		$data['compositeQueryJobVacancy']=$compositeQueryJobVacancy->result();
		$this->theme_model->loadTheme('vacancy/listOneMatchPerson',$data);
	}
	
	function listOneJobVacancy($job_vacancy_id=''){
		// echo '<meta charset="utf-8">';
		//檢查使用者是否登入
		$this->user_model->isLogin();
		//查無此工作職缺
		if(empty($job_vacancy_id)||!is_numeric($job_vacancy_id)){
			$data['title']='查無此工作職缺';
			$data['errorMessage']='查無此工作職缺';
			$this->theme_model->loadTheme('vacancy/errorPage',$data);
		}else{
			$data['title']='詳細職缺資料';
			$getOneJobVacancy=$this->job_vacancy_model->getOneJobVacancy($job_vacancy_id)->result();
		}
		//查無此工作職缺
		if(empty($getOneJobVacancy)){
			$data['title']='查無此工作職缺';
			$data['errorMessage']='查無此工作職缺';
			$this->theme_model->loadTheme('vacancy/errorPage',$data);
		//關閉中但為用戶所管理的工作職缺
		}elseif($getOneJobVacancy['0']->vacancy_open_flag==0 &&$getOneJobVacancy['0']->user_id==$this->session->userdata('user_id')){
			$data['getOneJobVacancy']=$getOneJobVacancy;
			$data['getJobsCategory'] = json_encode($this->tracks_model->getJobsCategory()->result());
			$this->theme_model->loadTheme('vacancy/listOneJobVacancy',$data);
		//工作職缺關閉中
		}elseif($getOneJobVacancy['0']->vacancy_open_flag==0){
			$data['title']='此工作職缺關閉中';
			$data['errorMessage']='此工作職缺關閉中';
			$this->theme_model->loadTheme('vacancy/errorPage',$data);
		//工作職缺開放中
		}else{
			$data['getOneJobVacancy']=$getOneJobVacancy;
			$data['getJobsCategory'] = json_encode($this->tracks_model->getJobsCategory()->result());
			$JobApply = $this->job_apply_model->getJobVacancy($job_vacancy_id, $this->session->userdata('user_id'))->result();
			$data['isJobApply'] = false;
			if(!empty($JobApply)){
				$data['isJobApply'] = true;
			}
			$this->theme_model->loadTheme('vacancy/listOneJobVacancy',$data);			
		}
	}
	
	function editJobVacancy($job_vacancy_id){
		//檢查使用者是否登入
		$this->user_model->isLogin();
		//查無此工作職缺
		if(empty($job_vacancy_id)||!is_numeric($job_vacancy_id)){
			$data['title']='查無此工作職缺';
			$data['errorMessage']='查無此工作職缺';
			$this->theme_model->loadTheme('vacancy/errorPage',$data);
		}else{
			$getOneJobVacancy=$this->job_vacancy_model->getOneJobVacancy($job_vacancy_id)->result();
			$data['title']='編輯職缺';
		}
		//您無編輯的權限
		if($getOneJobVacancy['0']->user_id!=$this->session->userdata('user_id')){
			$data['title']='您無編輯的權限';
			$data['errorMessage']='您無編輯的權限';
			$this->theme_model->loadTheme('vacancy/errorPage',$data);
		//查無此工作職缺
		}elseif(empty($getOneJobVacancy)){
			$data['title']='查無此工作職缺';
			$data['errorMessage']='查無此工作職缺';
			$this->theme_model->loadTheme('vacancy/errorPage',$data);			
		//用戶所管理的工作職缺
		}else{
			$data['getOneJobVacancy']=$getOneJobVacancy;
			$data['getJobsCategory'] = json_encode($this->tracks_model->getJobsCategory()->result());
			$this->theme_model->loadTheme('vacancy/editJobVacancy',$data);
		}
	}
	function updateJobVacancy($job_vacancy_id){
		$job_vacancy=array(	'job_name'			=>$this->security->xss_clean($this->input->post('job_name'			)),
							'jobs_category_id'	=>$this->security->xss_clean($this->input->post('jobs_category_id'	)),
							'address'			=>$this->security->xss_clean($this->input->post('address'			)),
							'job_content'		=>preg_replace('/[\n\r\t]/','',nl2br($this->security->xss_clean($this->input->post("job_content")))),
							'contact'			=>$this->security->xss_clean($this->input->post('contact'			)),
							'contact_email'		=>$this->security->xss_clean($this->input->post('contact_email'		)),
							'by_phone'			=>$this->security->xss_clean($this->input->post('by_phone'			)),
							'by_others'			=>preg_replace('/[\n\r\t]/','',nl2br($this->security->xss_clean($this->input->post("by_others")))),
							'match_county'		=>$this->security->xss_clean($this->input->post('match_county'		)),
							'match_district'	=>$this->security->xss_clean($this->input->post('match_district'	)),
							'degree_limit'		=>$this->security->xss_clean($this->input->post('degree_limit'		)),
							'working_experience'=>$this->security->xss_clean($this->input->post('working_experience')),
							'department_type_id'=>$this->security->xss_clean($this->input->post('department_type_id')),
							'skill'				=>$this->security->xss_clean($this->input->post('match_skill'				)),
							'license'			=>$this->security->xss_clean($this->input->post('match_license'			)),
							'additional_demand'	=>$this->security->xss_clean($this->input->post('additional_demand'	)),
							'posted_time'		=>$this->theme_model->nowTime(),
							'lasted_update_time'=>$this->theme_model->nowTime(),
							'user_id'			=>$this->session->userdata('user_id')
		);
		$this->job_vacancy_model->updateJobVacancy($job_vacancy_id,$job_vacancy);
		redirect(base_url().'vacancy/listAllJobVacancy');
	}
	
	function previewJobVacancy(){
		$data['title']='預覽職缺';
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$data['getOneUser'] = $this->user_model->getOneUser($this->session->userdata('email'))->row_array();
		$data['getOneJobVacancy']=$this->session->userdata('job_vacancy');
		$data['getJobsCategory'] = json_encode($this->tracks_model->getJobsCategory()->result());
		$this->theme_model->loadTheme('vacancy/previewJobVacancy',$data);
	}
	
	function deleteJobVacancy(){
		$this->user_model->isLogin();
		$deleteVacancyId=$this->security->xss_clean($this->input->post('deleteVacancyId'));
		if(!empty($deleteVacancyId)){
			echo 'deleteVacancyId<br>';
			print_r($deleteVacancyId);
			for($i=0;$i<count($deleteVacancyId);$i++){
				$this->job_vacancy_model->deleteJobVacancy($this->session->userdata('user_id'),$deleteVacancyId[$i]);
			}
			redirect(base_url().'vacancy/listAllJobVacancy');
		}else{
			redirect(base_url().'vacancy/listAllJobVacancy');
		}
	}
	
	function updateVacancyStatus($job_vacancy_id){
		$this->user_model->isLogin();
		$getOneJobVacancy=$this->job_vacancy_model->getOneJobVacancy($job_vacancy_id)->row();
		$status=($getOneJobVacancy->vacancy_open_flag==1)?0:1;
		$job_vacancy=array('vacancy_open_flag'	=>$status);
		$this->job_vacancy_model->updateJobVacancy($job_vacancy_id,$job_vacancy);
		redirect(base_url().'vacancy/listAllJobVacancy');
	}
	
	function myJob(){
        $input_para = $this->input->get();
        $qdata=array('job_category'=>'', 'degree_limit'=>'', 'working_experience'=>'', 'job_name'=>'');
        if($input_para['job_category']!='0'){
            $qdata['job_category'] = $this->security->xss_clean($input_para['job_category']);
        }
        if($input_para['degree_limit']!='0'){
            $qdata['degree_limit'] = $this->security->xss_clean($input_para['degree_limit']);
        }
        if($input_para['working_experience']!='-1'){
            $qdata['working_experience'] = $this->security->xss_clean($input_para['working_experience']);
        }	
        if(trim($input_para['job_name'])!=''){
            $qdata['job_name'] = $this->security->xss_clean($input_para['job_name']);
        }
        $this->user_model->isLogin(); 
		$data['title']='我的求職';
		$data['job_vacancy']=json_encode($this->job_apply_model->getAllJobList($qdata)->result());
		$data['job_category']=json_encode($this->job_apply_model->getAllJobCategory()->result());
		$data['getOneUser'] = $this->user_model->getOneUser($this->session->userdata('email'))->row_array();
		$data['selection'] = $qdata;
		$this->theme_model->loadTheme('vacancy/myJob',$data);
	}

    function applyjob(){
        $user_id = $this->session->userdata('user_id');
        $job_vacancy_id = $this->input->post('applyjob_id', TRUE);
        $apply_msg = $this->input->post('applymsg_show', TRUE);
        if(!empty($job_vacancy_id) && !empty($user_id)){
            $job_apply_data = array(
                'user_id'=>$user_id,
                'job_vacancy_id'=>$job_vacancy_id,
                'apply_msg'=>$apply_msg
            );
            $job_apply_result = $this->job_apply_model->addJobVacancy($job_apply_data);
            redirect(base_url().'vacancy/listOneJobVacancy/'.$job_vacancy_id);
        }
    }
    
	function applyRecord(){
		$user_id = $this->session->userdata('user_id');
		$data['title']='我的應徵紀錄';
		$data['apply_record']=json_encode($this->job_apply_model->getApplyRecord($user_id)->result());
		$this->theme_model->loadTheme('vacancy/applyRecord',$data);
	}
}



/* End of file vacancy.php */
/* Location: ./application/controllers/vacancy.php */
