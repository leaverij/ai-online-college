<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
		$this->load->model('company_model');
    }

    function index(){
		// $data['title']='企業資料維護';
		$this->user_model->isLogin();
		$this->user_model->isBackend();
		redirect(base_url().'company/listAllCompany');    	
    }

	function listAllCompany(){
		echo '<meta charset="utf-8">';
		$data['title']='企業資料維護';
		//檢查使用者是否登入
		$this->user_model->isLogin();

		$data['getCompany'] = $this->company_model->getCompany()->result();
		$this->theme_model->loadTheme('company/company',$data);
	}    

	function addCompany(){
		$data['title']='企業設定';
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$this->theme_model->loadTheme('company/addCompany',$data);		
	}

	function insertCompany(){
		$company=array(	'company_name'		=>$this->security->xss_clean($this->input->post('company_name')),
						'tax_id_number'		=>$this->security->xss_clean($this->input->post('tax_id_number')),
						'contact_name'		=>$this->security->xss_clean($this->input->post('contact_name')),
						'contact_mail'		=>$this->security->xss_clean($this->input->post('contact_mail')),
						'contact_phone'		=>$this->security->xss_clean($this->input->post('contact_phone')),
						'created_time'		=>$this->theme_model->nowTime(),
		);
		echo('bbbbb');
		if ('insertCompany'==$this->security->xss_clean($this->input->post('insertCompany'))) {
			$job_vacancy_id=$this->company_model->addComapany($company);
			echo('aaaaaaa');
			redirect(base_url().'company/listAllCompany');
		} elseif ('previewCompany'==$this->security->xss_clean($this->input->post('previewCompany'))) {
			$this->session->set_userdata('company',$company);
			echo('bbbbb');
			redirect(base_url().'company/previewCompany');
		}	
	}

	function previewCompany(){
		$data['title']='預覽企業設定';
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$data['getOneCompany']=$this->session->userdata('company');
		$this->theme_model->loadTheme('company/previewCompany',$data);		
	}

	function deleteCompany(){
		$this->user_model->isLogin();
		$deleteCompanyId=$this->security->xss_clean($this->input->post('deleteCompanyId'));
		echo $deleteCompanyId;
		if(!empty($deleteCompanyId)){
			echo 'deleteCompanyId<br>';
			print_r($deleteCompanyId);
			for($i=0;$i<count($deleteCompanyId);$i++){
				$this->company_model->deleteCompany($deleteCompanyId[$i]);
			}
			redirect(base_url().'company/listAllCompany');
		}else{
			redirect(base_url().'company/listAllCompany');
		}
	}

	function editCompany($tax_id_number){
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$data['title']='編輯企業';
		$data['getOneCompany']=$this->company_model->getOneCompany($tax_id_number)->result();
		$this->theme_model->loadTheme('company/editCompany',$data);
	}	
	function updateCompany($tax_id_number){
		$company=array(	'company_name'		=>$this->security->xss_clean($this->input->post('company_name')),
						'tax_id_number'		=>$this->security->xss_clean($this->input->post('tax_id_number')),
						'contact_name'		=>$this->security->xss_clean($this->input->post('contact_name')),
						'contact_mail'		=>$this->security->xss_clean($this->input->post('contact_mail')),
						'contact_phone'		=>$this->security->xss_clean($this->input->post('contact_phone')),
						'created_time'		=>$this->theme_model->nowTime(),
		);
		// $this->company_model->updateCompany($tax_id_number,$company);
		// redirect(base_url().'company/listAllCompany');
		if ('updateCompany'==$this->security->xss_clean($this->input->post('updateCompany'))) {
			$job_vacancy_id=$this->company_model->updateCompany($tax_id_number, $company);
			redirect(base_url().'company/listAllCompany');
		} elseif ('previewCompany'==$this->security->xss_clean($this->input->post('previewCompany'))) {
			$this->session->set_userdata('company',$company);
			redirect(base_url().'company/previewCompany');
		}	
	}	

}


