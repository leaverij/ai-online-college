<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
		$this->load->model('backend_model');
    }

	function index()
	{
		$data['title']='LH後台';
		$this->user_model->isLogin();
		$this->user_model->isBackend();
		//這是一個後台管理的頁面阿～
		$this->theme_model->loadTheme('backend/backend',$data);
	}
	//列出趨勢應用
	function listIndustry()
	{
		$data['title']='趨勢應用';
		$this->user_model->isLogin();
		$this->user_model->isIndustry();
		$data['listAllIndustry'] = $this->backend_model->listAllIndustry();
		//$data['listDomain'] = $this->backend_model->listDomain()->result();
		$data['listJobsCategory'] = $this->backend_model->listJobsCategory()->result();
		$this->theme_model->loadTheme('backend/listIndustry',$data);
	}
	//新增趨勢應用
	function addIndustry()
	{
		$this->user_model->isLogin();
		$this->user_model->isIndustry();
		$jobs_category_temp = $this->input->post('jobs_category_id');
		$jobs_category_id = implode (",", $jobs_category_temp);
		$data = array(
			'industry_title' => $this->input->post('industry_title'), 
			'industry_url' => $this->input->post('industry_url'),
			'industry_source' => $this->input->post('industry_source'),
			'industry_origin_date' => $this->input->post('industry_origin_date'),
			//'domain_id' => $this->input->post('domain_id'),
			'jobs_category_id' => $jobs_category_id,
			'industry_post_date' => $this->theme_model->nowTime(),
			'industry_status' => '1',
			'user_id' =>$this->session->userdata('user_id')
		);
		$this->backend_model->addIndustry($data);
		redirect('backend/listIndustry');
	}
	//修改趨勢應用
	function updateIndustry(){
		$this->user_model->isLogin();
		$this->user_model->isIndustry();
		$industry_id=$this->input->post('industry_id');
		$jobs_category_temp = $this->input->post('jobs_category_id');
		$jobs_category_id = implode (",", $jobs_category_temp);
		$data = array(
			'industry_title' => $this->input->post('industry_title'), 
			'industry_url' => $this->input->post('industry_url'),
			'industry_source' => $this->input->post('industry_source'),
			'industry_origin_date' => $this->input->post('industry_origin_date'),
			//'domain_id' => $this->input->post('domain_id')
			'jobs_category_id' => $jobs_category_id
		);
		$this->backend_model->updateIndustry($industry_id,$data);
		redirect('backend/listIndustry');
	}
	//刪除趨勢應用
	function delIndustry(){
		$this->user_model->isLogin();
		$this->user_model->isIndustry();
		$industry_id=$this->input->post('industry_id');
		$this->backend_model->delIndustry($industry_id);
	}
	
	//列出相關活動
	function listSeminar()
	{
		$data['title']='相關活動';
		$this->user_model->isLogin();
		$this->user_model->isSeminar();
		$data['listAllSeminar'] = $this->backend_model->listAllSeminar();
		//$data['listDomain'] = $this->backend_model->listDomain()->result();
		$data['listJobsCategory'] = $this->backend_model->listJobsCategory()->result();
		$this->theme_model->loadTheme('backend/listSeminar',$data);
	}
	//新增相關活動
	function addSeminar()
	{
		$this->user_model->isLogin();
		$this->user_model->isSeminar();
		$seminar_name = $this->input->post('seminar_type');
		if($seminar_name=='1'){
			$label_name = 'label-success';
		}else if($seminar_name =='2'){
			$label_name = 'label-primary';
		}else if($seminar_name =='3'){
			$label_name = 'label-warning';
		}else if($seminar_name =='4'){
			$label_name = 'label-default';
		}else{
			$label_name = 'label-default';
		}
		$jobs_category_temp = $this->input->post('jobs_category_id');
		$jobs_category_id = implode (",", $jobs_category_temp);
		//$filename = $_FILES['userfile']['name'].$this->theme_model->nowTime();
		//$ext=pathinfo($_FILES['userfile']['name'],PATHINFO_EXTENSION);
		//$config['upload_path'] = './assets/img/seminar';
		//$config['allowed_types'] = 'png|jpg|JPG|jpeg|JPEG';
		//$config['max_size']	= '2000';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';
		//$config['file_name'] = md5($filename).'.'.$ext;
		//$config['overwrite'] = 'TRUE';
		//$this->load->library('upload',$config);
		//if ( ! $this->upload->do_upload())
		//{
			////無上傳相片時
			//$file_size = $this->upload->data();
			//if(! $file_size['file_size']){
				$seminar_start_time = $this->input->post('seminar_start_time');
				$seminar_end_time = $this->input->post('seminar_end_time');
				if($seminar_start_time>$seminar_end_time){
					$seminar_end_time = $seminar_start_time;
				}
				$data = array(
					'seminar_title' => $this->input->post('seminar_title'), 
					'seminar_name' => $seminar_name,
					'label_name' => $label_name,
					'seminar_url' => $this->input->post('seminar_url'),
					'seminar_content' => $this->input->post('seminar_content'),
					'seminar_host' => $this->input->post('seminar_host'),
					'seminar_start_time' => $seminar_start_time,
					'seminar_end_time' => $seminar_end_time,
					'seminar_place' => $this->input->post('seminar_place'),
					//'domain_id' => $this->input->post('domain_id'),
					'jobs_category_id' => $jobs_category_id,
					'seminar_post_date' => $this->theme_model->nowTime(),
					'seminar_status' => '1',
					'user_id' =>$this->session->userdata('user_id')
				);
			$this->backend_model->addSeminar($data);
			redirect('backend/listSeminar');
			//}else{
			////上傳有錯誤時
				//$data['title']='相關活動';
				//$data['error'] = $this->upload->display_errors();
				//$data['listAllSeminar'] = $this->backend_model->listAllSeminar();
				//$this->theme_model->loadTheme('backend/listSeminar',$data);
			//}
		//}else{
			// $file_size = $this->upload->data();
			// $data = array(
					// 'seminar_title' => $this->input->post('seminar_title'), 
					// 'seminar_url' => $this->input->post('seminar_url'),
					// 'seminar_date' => $this->input->post('seminar_date'),
					// 'seminar_place' => $this->input->post('seminar_place'),
					// 'seminar_pic' => md5($filename).'.'.$ext,
					// 'domain_id' => $this->input->post('domain_id'),
					// 'seminar_post_date' => $this->theme_model->nowTime(),
					// 'seminar_status' => '1',
					// 'user_id' =>$this->session->userdata('user_id')
				// );
		//$this->backend_model->addSeminar($data);
		// $fullPath = base_url().'assets/img/seminar/'.md5($filename).'.'.$ext;
		// switch(strtolower($ext)){
			// case "jpg":
				// $src=imagecreatefromjpeg($fullPath);//讀取來源圖檔
				// break;
			// case "jpeg":
				// $src=imagecreatefromjpeg($fullPath);//讀取來源圖檔
				// break;
			// case "gif":
				// $src=imagecreatefromgif($fullPath);//讀取來源圖檔
				// break;
			// case "png":
				// $src=imagecreatefrompng($fullPath);//讀取來源圖檔
				// break;
		// }
		// $src_w = imagesx($src);     //取得來源圖檔長寬
		// $src_h = imagesy($src);
		// $new_w = 128;               //新圖檔長寬
		// $new_h = 128;
		// $thumb = imagecreatetruecolor($new_w, $new_h);    //建立空白縮圖
		// //設定空白縮圖的背景，如不設定，背景預設為黑色
		// $bg = imagecolorallocate($thumb,255,255,255);       //空白縮圖的背景顏色
		// imagefilledrectangle($thumb,0,0,$src_w,$src_h,$bg); //將顏色填入縮圖
		// //執行縮圖
		// imagecopyresampled($thumb, $src, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);
		// switch(strtolower($ext)){
			// case "jpg":
				// imagejpeg($thumb, './assets/img/seminar/'.md5($filename).'.'.$ext,90);
				// break;
			// case "jpeg":
				// imagejpeg($thumb, './assets/img/seminar/'.md5($filename).'.'.$ext,90);
				// break;
			// case "gif":
				// imagegif($thumb, './assets/img/seminar/'.md5($filename).'.'.$ext);
				// break;
			// case "png":
				// imagepng($thumb, './assets/img/seminar/'.md5($filename).'.'.$ext,9);
				// break;
		// }
		// }
		//redirect('backend/listSeminar');
	}
	//修改相關活動
	function updateSeminar(){
		$this->user_model->isLogin();
		$this->user_model->isSeminar();
		$seminar_id=$this->input->post('seminar_id');
		$seminar_name = $this->input->post('seminar_type');
		if($seminar_name=='1'){
			$label_name = 'label-success';
		}else if($seminar_name =='2'){
			$label_name = 'label-primary';
		}else if($seminar_name =='3'){
			$label_name = 'label-warning';
		}else if($seminar_name =='4'){
			$label_name = 'label-default';
		}else{
			$label_name = 'label-default';
		}
		$jobs_category_temp = $this->input->post('jobs_category_id');
		$jobs_category_id = implode (",", $jobs_category_temp);
		// $filename = $_FILES['userfile']['name'].$this->theme_model->nowTime();
		// $ext=pathinfo($_FILES['userfile']['name'],PATHINFO_EXTENSION);
		// $config['upload_path'] = './assets/img/seminar';
		// $config['allowed_types'] = 'png|jpg|JPG|jpeg|JPEG';
		// $config['max_size']	= '2000';
		// //$config['max_width']  = '1024';
		// //$config['max_height']  = '768';
		// $config['file_name'] = md5($filename).'.'.$ext;
		// $config['overwrite'] = 'TRUE';
		// $this->load->library('upload',$config);
		// if ( ! $this->upload->do_upload())
		// {
			// //無上傳相片時
			// $file_size = $this->upload->data();
			// if(! $file_size['file_size']){
				$seminar_start_time = $this->input->post('seminar_start_time');
				$seminar_end_time = $this->input->post('seminar_end_time');
				if($seminar_start_time>$seminar_end_time){
					$seminar_end_time = $seminar_start_time;
				}
				$data = array(
					'seminar_title' => $this->input->post('seminar_title'),
					'seminar_name' => $seminar_name,
					'label_name' => $label_name, 
					'seminar_url' => $this->input->post('seminar_url'),
					'seminar_content' => $this->input->post('seminar_content'),
					'seminar_host' => $this->input->post('seminar_host'),
					'seminar_start_time' => $seminar_start_time,
					'seminar_end_time' => $seminar_end_time,
					'seminar_place' => $this->input->post('seminar_place'),
					//'domain_id' => $this->input->post('domain_id')
					'jobs_category_id' => $jobs_category_id
				);
			$this->backend_model->updateSeminar($seminar_id,$data);
			redirect('backend/listSeminar');
			// }else{
			// //上傳有錯誤時
				// $data['title']='相關活動';
				// $data['error'] = $this->upload->display_errors();
				// $data['listAllSeminar'] = $this->backend_model->listAllSeminar();
				// $this->theme_model->loadTheme('backend/listSeminar',$data);
			// }
		// }else{
			// $file_size = $this->upload->data();
			// $data = array(
					// 'seminar_title' => $this->input->post('seminar_title'), 
					// 'seminar_url' => $this->input->post('seminar_url'),
					// 'seminar_date' => $this->input->post('seminar_date'),
					// 'seminar_place' => $this->input->post('seminar_place'),
					// 'seminar_pic' => md5($filename).'.'.$ext,
					// 'domain_id' => $this->input->post('domain_id')
				// );
		//$this->backend_model->updateSeminar($seminar_id,$data);
		// $fullPath = base_url().'assets/img/seminar/'.md5($filename).'.'.$ext;
		// switch(strtolower($ext)){
			// case "jpg":
				// $src=imagecreatefromjpeg($fullPath);//讀取來源圖檔
				// break;
			// case "jpeg":
				// $src=imagecreatefromjpeg($fullPath);//讀取來源圖檔
				// break;
			// case "gif":
				// $src=imagecreatefromgif($fullPath);//讀取來源圖檔
				// break;
			// case "png":
				// $src=imagecreatefrompng($fullPath);//讀取來源圖檔
				// break;
		// }
		// $src_w = imagesx($src);     //取得來源圖檔長寬
		// $src_h = imagesy($src);
		// $new_w = 128;               //新圖檔長寬
		// $new_h = 128;
		// $thumb = imagecreatetruecolor($new_w, $new_h);    //建立空白縮圖
		// //設定空白縮圖的背景，如不設定，背景預設為黑色
		// $bg = imagecolorallocate($thumb,255,255,255);       //空白縮圖的背景顏色
		// imagefilledrectangle($thumb,0,0,$src_w,$src_h,$bg); //將顏色填入縮圖
		// //執行縮圖
		// imagecopyresampled($thumb, $src, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);
		// switch(strtolower($ext)){
			// case "jpg":
				// imagejpeg($thumb, './assets/img/seminar/'.md5($filename).'.'.$ext,90);
				// break;
			// case "jpeg":
				// imagejpeg($thumb, './assets/img/seminar/'.md5($filename).'.'.$ext,90);
				// break;
			// case "gif":
				// imagegif($thumb, './assets/img/seminar/'.md5($filename).'.'.$ext);
				// break;
			// case "png":
				// imagepng($thumb, './assets/img/seminar/'.md5($filename).'.'.$ext,9);
				// break;
		// }
		// }
		// redirect('backend/listSeminar');
	}
	//刪除相關活動
	function delSeminar(){
		$this->user_model->isLogin();
		$this->user_model->isSeminar();
		$seminar_id=$this->input->post('seminar_id');
		$this->backend_model->delSeminar($seminar_id);
	}
	//列出企業徵才
	function listJobs()
	{
		$data['title']='企業徵才';
		$this->user_model->isLogin();
		$this->user_model->isJobs();
		$data['listAllJobs'] = $this->backend_model->listAllJobs();
		//$data['listDomain'] = $this->backend_model->listDomain()->result();
		$data['listJobsCategory'] = $this->backend_model->listJobsCategory()->result();
		$this->theme_model->loadTheme('backend/listJobs',$data);
	}
	//新增企業徵才
	function addJobs()
	{
		$this->user_model->isLogin();
		$this->user_model->isJobs();
		$jobs_category_temp = $this->input->post('jobs_category_id');
		$jobs_category_id = implode (",", $jobs_category_temp);
		$data = array(
			'jobs_title' => $this->input->post('jobs_title'), 
			'jobs_url' => $this->input->post('jobs_url'),
			'comp_name' => $this->input->post('comp_name'),
			//'domain_id' => $this->input->post('domain_id'),
			'jobs_category_id' => $jobs_category_id,
			'jobs_post_date' => $this->theme_model->nowTime(),
			'jobs_status' => '1',
			'user_id' =>$this->session->userdata('user_id')
		);
		$this->backend_model->addJobs($data);
		redirect('backend/listJobs');
	}
	//修改企業徵才
	function updateJobs(){
		$this->user_model->isLogin();
		$this->user_model->isJobs();
		$jobs_id=$this->input->post('jobs_id');
		$jobs_category_temp = $this->input->post('jobs_category_id');
		$jobs_category_id = implode (",", $jobs_category_temp);
		$data = array(
			'jobs_title' => $this->input->post('jobs_title'), 
			'comp_name' => $this->input->post('comp_name'),
			'jobs_url' => $this->input->post('jobs_url'),
			//'domain_id' => $this->input->post('domain_id')
			'jobs_category_id' => $jobs_category_id
		);
		$this->backend_model->updateJobs($jobs_id,$data);
		redirect('backend/listJobs');
	}
	//刪除企業徵才
	function delJobs(){
		$this->user_model->isLogin();
		$this->user_model->isJobs();
		$jobs_id=$this->input->post('jobs_id');
		$this->backend_model->delJobs($jobs_id);
	}
	//列出國際資源
	function listResource()
	{
		$data['title']='國內外資源';
		$this->user_model->isLogin();
		$this->user_model->isResource();
		$data['listAllResource'] = $this->backend_model->listAllResource();
		//$data['listDomain'] = $this->backend_model->listDomain()->result();
		$data['listJobsCategory'] = $this->backend_model->listJobsCategory()->result();
		$this->theme_model->loadTheme('backend/listResource',$data);
	}
	//新增國際資源
	function addResource()
	{
		$this->user_model->isLogin();
		$this->user_model->isResource();
		$resource_type = $this->input->post('resource_type');
		if($resource_type=='1'){
			$label_name = 'label-success';
		}else if($resource_type =='2'){
			$label_name = 'label-primary';
		}else if($resource_type =='3'){
			$label_name = 'label-warning';
		}else if($resource_type =='4'){
			$label_name = 'label-default';
		}else{
			$label_name = 'label-default';
		}
		$jobs_category_temp = $this->input->post('jobs_category_id');
		$jobs_category_id = implode (",", $jobs_category_temp);
		$data = array(
			'resource_title' => $this->input->post('resource_title'), 
			'resource_url' => $this->input->post('resource_url'),
			'resource_source' => $this->input->post('resource_source'),
			'resource_type' => $resource_type,
			'label_name' => $label_name,
			'resource_origin_date' => $this->input->post('resource_origin_date'),
			//'domain_id' => $this->input->post('domain_id'),
			'jobs_category_id' => $jobs_category_id,
			'resource_post_date' => $this->theme_model->nowTime(),
			'resource_status' => '1',
			'user_id' =>$this->session->userdata('user_id')
		);
		$this->backend_model->addResource($data);
		redirect('backend/listResource');
	}
	//修改國際資源
	function updateResource(){
		$this->user_model->isLogin();
		$this->user_model->isResource();
		$resource_id=$this->input->post('resource_id');
		$resource_type = $this->input->post('resource_type');
		if($resource_type=='1'){
			$label_name = 'label-success';
		}else if($resource_type =='2'){
			$label_name = 'label-primary';
		}else if($resource_type =='3'){
			$label_name = 'label-warning';
		}else if($resource_type =='4'){
			$label_name = 'label-default';
		}else{
			$label_name = 'label-default';
		}
		$jobs_category_temp = $this->input->post('jobs_category_id');
		$jobs_category_id = implode (",", $jobs_category_temp);
		$data = array(
			'resource_title' => $this->input->post('resource_title'), 
			'resource_url' => $this->input->post('resource_url'),
			'resource_source' => $this->input->post('resource_source'),
			'resource_type' => $resource_type,
			'label_name' => $label_name,
			'resource_origin_date' => $this->input->post('resource_origin_date'),
			//'domain_id' => $this->input->post('domain_id')
			'jobs_category_id' => $jobs_category_id
		);
		$this->backend_model->updateResource($resource_id,$data);
		redirect('backend/listResource');
	}
	//刪除國際資源
	function delResource(){
		$this->user_model->isLogin();
		$this->user_model->isResource();
		$resource_id=$this->input->post('resource_id');
		$this->backend_model->delResource($resource_id);
	}
	//更新四種資源的點擊次數
	function updateUserClicks(){
		$type = $this->security->xss_clean($this->input->post("type"));
		$id = $this->security->xss_clean($this->input->post("id"));
		$this->backend_model->updateUserClicks($type,$id);
	}
	//更新modal的點擊次數
	function updateModalClicks(){
		$type = $this->security->xss_clean($this->input->post("type"));
		$id = $this->security->xss_clean($this->input->post("id"));
		$this->backend_model->updateModalClicks($type,$id);
	}
	//取得source以提供出處的auto complete使用
	function getSource(){
		$type = $this->security->xss_clean($this->input->post("type"));
		echo json_encode($this->backend_model->getSource($type)->result());
	}
    //寫入tin_can_api
    function insertStmt(){
        $stmt = $this->security->xss_clean($this->input->post("stmt"));
        $json=json_decode($stmt,true);
        $statementBody =  $this->encode($json);
        $id =  $this->encode($json['id']);
        $actor =  $this->encode($json['actor']);
        $verb =  $this->encode($json['verb']);
        $object =  $this->encode($json['object']);
        $objectType =  $this->encode($json['object']['objectType']);
        $result =  $this->encode($json['result']);
        $context =  $this->encode($json['context']);
        $authority =  $this->encode($json['authority']);
        $verbId =  $this->encode($json['verb']['id']);
        $instructor =  $this->encode($json['context']['instructor']);
        $contextActivities = $this->encode($json['context']['contextActivities']);
        $data = array(
            'statementId' => str_replace ('"'," ",$id),
            'userId' => 'learninghouse',
            'statementBody' =>$statementBody,
            'actor' => $actor, 
            'verb' => $verb,
            'object' => $object,
            'objectType' => str_replace ('"'," ",$objectType),
            'result' => $result, 
            'context' => $context, 
            'authority' => $authority, 
            'verbId' => str_replace ('"'," ",$verbId), 
            'instructor' => $instructor, 
            'contextActivities' => $contextActivities
        );
        $this->backend_model->insertStatement($data);
    }
    function encode($json)
    {
        return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", str_replace('\\/', '/', json_encode($json)));
    }
}

/* End of file backend.php */
/* Location: ./application/controllers/backend.php */
