<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class College extends CI_Controller {
	function __construct()
    {
        parent::__construct();
		$this->load->model('domain_model');
		$this->load->model('tracks_model');
		$this->load->model('backend_model');
    }
	function index()
	{
	}
	
	function more($domain_url)
	{
		// $data['title']='more';
		// //檢查使用者是否登入
        // $this->user_model->isLogin();
		// $row = $this->domain_model->checkDomain($domain_url);
		// if($row){
			// $data['domain_name']=$row->domain_name.'學院';
			// $data['domain_url'] = $domain_url;
			// //趨勢應用
			// $data['listAllIndustryByDomain'] = $this->backend_model->listAllIndustryByDomain($domain_url);
			// //相關活動
			// $data['listAllSeminarByDomain'] = $this->backend_model->listAllSeminarByDomain($domain_url);
			// //企業徵才
			// $data['listAllJobsByDomain'] = $this->backend_model->listAllJobsByDomain($domain_url);
			// //國際資源
			// $data['listAllResourceByDomain'] = $this->backend_model->listAllResourceByDomain($domain_url);
			// $this->theme_model->loadTheme('domain/more',$data);
		// }else{
			// echo 'noData';
		// }
	}
	//更多相關活動
	function moreSeminar($domain_url)
	{
		$data['title']='more';
		//檢查使用者是否登入
        $this->user_model->isLogin();
		$row = $this->domain_model->checkDomain($domain_url);
		if($row){
			//頁碼
			$page =  abs(round($this->input->get('page')));
			$data['domain_name']=$row->domain_name.'學院';
			$data['domain_url'] = $domain_url;
			$data['seminar_name'] = $this->seminar($this->uri->segment(4));
			//點擊搜尋的時候
			if($this->input->post("action")=='search'){
				$search_string = $this->security->xss_clean($this->input->post('search_string'));
				$search_string=mysql_real_escape_string($search_string);
				$this->session->set_userdata('search_string',$search_string);
				$data['search_string'] = $search_string;
				//依照資源類型 影片文件文章其他
				if($this->uri->segment(4) && is_numeric($this->uri->segment(4))){
					$data['listAllSeminarByDomain'] = $this->backend_model->listAllSeminarByDomainAndSeminar($domain_url,$this->uri->segment(4));
				//無分類
				}else{
					$data['listAllSeminarByDomain'] = $this->backend_model->listSearchSeminarByDomain($domain_url);
				}
			}else{
				//pjax 依照資源類型 college/moreresource/mobile/1
				if($this->uri->segment(4) && is_numeric($this->uri->segment(4))){
					$data['search_string'] = $this->session->userdata('search_string');
					$data['listAllSeminarByDomain'] = $this->backend_model->listAllSeminarByDomainAndSeminar($domain_url,$this->uri->segment(4));
				//無分類時  college/moreresource/mobile
				}else{
					if($this->session->userdata('search_string')){
						$data['search_string'] = $this->session->userdata('search_string');
						$data['listAllResourceByDomain'] = $this->backend_model->listSearchSeminarByDomain($domain_url);
					}else{
						//總筆數
						$rows =  $this->backend_model->listAllSeminarCount($domain_url)->row();
						$total_rows = $rows->count;
						//每頁筆數
						$per_page = 10;
						//第一頁
						$data['first'] = '';
						//最後一頁 
						$data['last'] = ceil($total_rows/$per_page);
						//當前頁數
						$data['page'] = $page;
						$start = $per_page*($page-1);
						if($start<0) $start=0;
						//page有值時
						if($page){
							$data['previous'] =$page-1;
							if($total_rows <= $per_page*$page){
								$data['next'] = '';	
							}else{
								$data['next'] = $page+1;
							}
						//page無值時
						}else{
							$data['previous'] = 0;
							if($total_rows <= $per_page*$page){
								$data['next'] = '';	
							}else{
								$data['next'] = $page+2;
							}
						}
						$data['listAllSeminarByDomain'] = $this->backend_model->listAllSeminarByDomain($domain_url,$start,$per_page);
					}
				}
			}
			$this->theme_model->loadTheme('domain/moreSeminar',$data);
		}else{
			echo 'input error';
		}
	}
	//更多趨勢應用
	function moreIndustry($domain_url)
	{
		$data['title']='more';
		//檢查使用者是否登入
        $this->user_model->isLogin();
		$row = $this->domain_model->checkDomain($domain_url);
		if($row){
			//頁碼
			$page =  abs(round($this->input->get('page')));
			$data['domain_name']=$row->domain_name.'學院';
			$data['domain_url'] = $domain_url;
			if($this->input->post("action")=='search'){
				$search_string = $this->security->xss_clean($this->input->post('search_string'));
				$data['listAllIndustryByDomain'] = $this->backend_model->listSearchIndustryByDomain($domain_url,$search_string);
				$data['search_string'] = $search_string;
			}else{
				//總筆數
				$rows =  $this->backend_model->listAllIndustryCount($domain_url)->row();
				$total_rows = $rows->count;
				//每頁筆數
				$per_page = 10;
				//第一頁
				$data['first'] = '';
				//最後一頁 
				$data['last'] = ceil($total_rows/$per_page);
				//當前頁數
				$data['page'] = $page;
				$start = $per_page*($page-1);
				if($start<0) $start=0;
				//page有值時
				if($page){
					$data['previous'] =$page-1;
					if($total_rows <= $per_page*$page){
						$data['next'] = '';	
					}else{
						$data['next'] = $page+1;
					}
				//page無值時
				}else{
					$data['previous'] = 0;
					if($total_rows <= $per_page*$page){
						$data['next'] = '';	
					}else{
						$data['next'] = $page+2;
					}
				}
				$data['listAllIndustryByDomain'] = $this->backend_model->listAllIndustryByDomain($domain_url,$start,$per_page);
			}
			$this->theme_model->loadTheme('domain/moreIndustry',$data);
		}else{
			echo 'noData';
		}
	}
	//更多企業徵才
	function moreJobs($domain_url)
	{
		$data['title']='more';
		//檢查使用者是否登入
        $this->user_model->isLogin();
		$row = $this->domain_model->checkDomain($domain_url);
		if($row){
			//頁碼
			$page =  abs(round($this->input->get('page')));
			$data['domain_name']=$row->domain_name.'學院';
			$data['domain_url'] = $domain_url;
			if($this->input->post("action")=='search'){
				$search_string = $this->security->xss_clean($this->input->post('search_string'));
				$data['listAllJobsByDomain'] = $this->backend_model->listSearchJobsByDomain($domain_url,$search_string);
				$data['search_string'] = $search_string;
			}else{
				//總筆數
				$rows =  $this->backend_model->listAllJobsCount($domain_url)->row();
				$total_rows = $rows->count;
				//每頁筆數
				$per_page = 10;
				//第一頁
				$data['first'] = '';
				//最後一頁 
				$data['last'] = ceil($total_rows/$per_page);
				//當前頁數
				$data['page'] = $page;
				$start = $per_page*($page-1);
				if($start<0) $start=0;
				//page有值時
				if($page){
					$data['previous'] =$page-1;
					if($total_rows <= $per_page*$page){
						$data['next'] = '';	
					}else{
						$data['next'] = $page+1;
					}
				//page無值時
				}else{
					$data['previous'] = 0;
					if($total_rows <= $per_page*$page){
						$data['next'] = '';	
					}else{
						$data['next'] = $page+2;
					}
				}
				$data['listAllJobsByDomain'] = $this->backend_model->listAllJobsByDomain($domain_url,$start,$per_page);
			}
			
			$this->theme_model->loadTheme('domain/moreJobs',$data);
		}else{
			echo 'noData';
		}
	}
	//更多國內外資源
	function moreResource($domain_url)
	{
		$data['title']='more';
		//檢查使用者是否登入
        $this->user_model->isLogin();
		$row = $this->domain_model->checkDomain($domain_url);
		if($row){
			//頁碼
			$page =  abs(round($this->input->get('page')));
			$data['domain_name']=$row->domain_name.'學院';
			$data['domain_url'] = $domain_url;
			$data['resource_type'] = $this->resource($this->uri->segment(4));
			//點擊搜尋表單的時候
			if($this->input->post("action")=='search'){
				$search_string = $this->security->xss_clean($this->input->post('search_string'));
				$search_string=mysql_real_escape_string($search_string);
				$this->session->set_userdata('search_string',$search_string);
				$data['search_string'] = $search_string;
				//依照資源類型 影片文文章其他
				if($this->uri->segment(4) && is_numeric($this->uri->segment(4))){
					$data['listAllResourceByDomain'] = $this->backend_model->listAllResourceByDomainAndResource($domain_url,$this->uri->segment(4));
				//無分類
				}else{
					$data['listAllResourceByDomain'] = $this->backend_model->listSearchResourceByDomain($domain_url);
				}
			}else{
				//透過pjax搜尋時以及點擊國內外資源進來的時候
				//pjax 依照資源類型 college/moreresource/mobile/1
				if($this->uri->segment(4) && is_numeric($this->uri->segment(4))){
					$data['search_string'] = $this->session->userdata('search_string');
					$data['listAllResourceByDomain'] = $this->backend_model->listAllResourceByDomainAndResource($domain_url,$this->uri->segment(4));
				//點擊sidebar的國內外資源
				}else{
					if($this->session->userdata('search_string')){
						$data['search_string'] = $this->session->userdata('search_string');
						$data['listAllResourceByDomain'] = $this->backend_model->listSearchResourceByDomain($domain_url);
					}else{
						//總筆數
						$rows =  $this->backend_model->listAllResourceCount($domain_url)->row();
						$total_rows = $rows->count;
						//每頁筆數
						$per_page = 10;
						//第一頁
						$data['first'] = '';
						//最後一頁 
						$data['last'] = ceil($total_rows/$per_page);
						//當前頁數
						$data['page'] = $page;
						$start = $per_page*($page-1);
						if($start<0) $start=0;
						//page有值時
						if($page){
							$data['previous'] =$page-1;
							if($total_rows <= $per_page*$page){
								$data['next'] = '';	
							}else{
								$data['next'] = $page+1;
							}
						//page無值時
						}else{
							$data['previous'] = 0;
							if($total_rows <= $per_page*$page){
								$data['next'] = '';	
							}else{
								$data['next'] = $page+2;
							}
						}
						$data['listAllResourceByDomain'] = $this->backend_model->listAllResourceByDomain($domain_url,$start,$per_page);
					}
					
				}
			}
			$this->theme_model->loadTheme('domain/moreResource',$data);
		}else{
			echo 'noData';
		}
	}
	function domain($domain_url)
	{
		$row = $this->domain_model->checkDomain($domain_url);
		if($row){
			$data['title']=$row->domain_name.'學院';
			$data['domain_url'] = $domain_url;
			$email = $this->session->userdata('email');
			//pjax 依照程度分類 college/domain/mobile/1
			if($this->uri->segment(4) && is_numeric($this->uri->segment(4))){
				$data['proficiency'] = $this->proficiency($this->uri->segment(4));
				//檢查使用者是否有訂閱學程地圖
				if($this->tracks_model->checkUserTracks($email)){
					$data['getAllTracks']=($this->domain_model->getTracksByDomainAndUserAndProficiency($domain_url,$email,$this->uri->segment(4)));
				}else{
					//未登入狀態
					$data['getAllTracks']=($this->domain_model->getTracksByDomainAndProficiency($domain_url,$this->uri->segment(4)));
				}
			//無分類時  college/domain/mobile
			}else{
				$data['proficiency'] = '依學程深度分類';
				//檢查使用者是否有訂閱學程地圖
				if($this->tracks_model->checkUserTracks($email)){
					$data['getAllTracks']=($this->domain_model->getTracksByDomainAndUser($domain_url,$email));
				}else{
					//未登入狀態
					$data['getAllTracks']=($this->domain_model->getTracksByDomain($domain_url));
				}	
			}
			//趨勢應用
			$data['listIndustryByDomain'] = $this->backend_model->listIndustryByDomain($domain_url);
			//相關活動
			$data['listSeminarByDomain'] = $this->backend_model->listSeminarByDomain($domain_url);
			//企業徵才
			$data['listJobsByDomain'] = $this->backend_model->listJobsByDomain($domain_url);
			//國際資源
			$data['listResourceByDomain'] = $this->backend_model->listResourceByDomain($domain_url);
			//熱門討論
			$data['listPopularDiscussions'] = $this->backend_model->listPopularDiscussions($domain_url);
			//最新討論
			$data['listNewDiscussions'] = $this->backend_model->listNewDiscussions($domain_url);
			//$domain_url底下 發問區的課程分類
			$data['listLibrary_id'] = $this->backend_model->listLibrary_id($domain_url)->result();
			$this->theme_model->loadTheme('domain/oneDomain',$data);
		}else{
			echo 'noData';
		}
		
	}
	//透過ajax送過來，設定search的session
	function setSearchSession(){
		$search_string = $this->input->post("search_string");
		$data = array('search_string'  => $search_string);
		$this->session->set_userdata($data);
		return true;
	}
	//設定session
	function setFlagSession(){
		//$flag = $this->input->post("flag");
		//$this->session->set_userdata('flag', $flag);
	}
	//取消訂閱學程
	function unsubscribe($tracks_url){
		$getJobsCategoryUrl = $this->tracks_model->getJobsCategoryUrl($tracks_url);
		if ($getJobsCategoryUrl->num_rows() > 0){
   			$row = $getJobsCategoryUrl->row(); 
			//取得domain的url
			$this->tracks_model->unsubscribe($tracks_url,$this->session->userdata('user_id'));
			redirect('track/domain/'.$row->jobs_category_url);
		}
	}
	//訂閱學程
	function subscribe($tracks_url){
		$tracks_query= $this->tracks_model->getTracksId($tracks_url)->row();
		$tracks_id = $tracks_query->tracks_id;
		//訂閱前，先把其他學程學習狀態更改為0
		$this->tracks_model->updateTracksLearningStatus($this->session->userdata('user_id'));
		//新增訂閱學程之紀錄
		$userTrackData = array ( 'user_id' => $this->session->userdata('user_id'),
						'learning_status' => '1',
						'tracks_id' =>$tracks_id,
						'tracks_date'=> $this->theme_model->nowTime());
		$this->tracks_model->addUserTracks($userTrackData);
		redirect('track/trackContent/'.$tracks_url);
	}
	function proficiency($val){
		switch ($val){
			case "1":
 				return "學習者";
			break;
			case "2":
 				return "練習者";
			break;
			case "3":
 				return "能力者";
			break;
			case "4":
 				return "專家";
			break;
			default:
  				return "依學程深度分類";
		}
	}
	function resource($type){
		switch ($type){
			case "1":
 				return "影片";
			break;
			case "2":
 				return "文件";
			break;
			case "3":
 				return "文章";
			break;
			case "4":
 				return "其他";
			break;
			default:
  				return "依資源類型";
		}
	}
	function seminar($type){
		switch ($type){
			case "1":
 				return "研討會";
			break;
			case "2":
 				return "競賽";
			break;
			case "3":
 				return "展覽";
			break;
			case "4":
 				return "其他";
			break;
			default:
  				return "依活動類型";
		}
	}
}

/* End of file college.php */
/* Location: ./application/controllers/college.php */