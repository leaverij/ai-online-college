<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {
	
	function __construct()
    {
		parent::__construct();
		$this->load->model('teacher_model');
    }

	function index()
	{
		$data['title']='權限設定';
		//檢查使用者是否登入
        $this->user_model->isBackend();
		$data['getAlluser'] = json_encode($this->user_model->getAllUser()->result(),JSON_UNESCAPED_UNICODE);
		// print_r($data);exit;
        $this->theme_model->loadTheme('setting/permissions',$data);
	}
	//更新後台權限
	function updateBackendFlag(){
		$this->user_model->isBackend();
		$user_id = $this->security->xss_clean($this->input->post('user_id'));
		$backend_flag = $this->security->xss_clean($this->input->post('backend_flag'));
		$backend_flag = ($backend_flag==1)?"1":"0";
		$this->user_model->updateBackendFlag($user_id,$backend_flag);
		// redirect('coupon');
		// echo $backend_flag;
		echo 'true';
	}
	//更新老師權限
	function updateTeacherFlag(){
		$this->user_model->isBackend();
		$user_id = $this->security->xss_clean($this->input->post('user_id'));
		$teacher_flag = $this->security->xss_clean($this->input->post('teacher_flag'));
		$teacher_flag = ($teacher_flag==1)?"1":"0";
		$this->user_model->updateTeacherFlag($user_id,$teacher_flag);
		if($teacher_flag==1){
			// 檢視teacher table是否有資料，如果沒有，新增。
			$hasTeacherData=$this->teacher_model->hasTeacherData($user_id);
			if($hasTeacherData==0){
				$userData = $this->teacher_model->getUserData($user_id)->row();
				$data = array(
					'teacher_id' => $user_id,
					'name'=>$userData->name,
					'email'=>$userData->email,
					'introduction'=>$userData->intro,
					'teacher_pic'=>$userData->img);
				$this->teacher_model->insert($data);
			}
		}
		echo 'true';
	}
	function getData(){
		// $data['title']='權限設定';
		//檢查使用者是否登入
        $this->user_model->isBackend();
        // $data['getAlluser'] = json_encode($this->user_model->getAllUser()->result(),JSON_UNESCAPED_UNICODE);
        // $this->theme_model->loadTheme('setting/permissions',$data);

		// DB table to use
		$table = 'user';
		// Table's primary key
		$primaryKey = 'user_id';
		$columns = array(
			array( 'db' => 'name', 'dt' => 0 ),
			array( 'db' => 'email',  'dt' => 1 ),
			array( 'db' => 'backend_flag', 'dt' => 2 ),
			array( 'db' => 'teacher_flag',  'dt' => 3 ),
			array( 'db' => 'user_id',  'dt' => 4 ),
			array( 'db' => 'enterprise_flag',  'dt' => 5 )
		);
		// SQL server connection information
		$sql_details = array(
			'user' => $this->db->username,
			'pass' => $this->db->password,
			'db'   => $this->db->database,
			'host' => $this->db->hostname
		);
		require(APPPATH.'../application/libraries/ssp.class.php');

		echo json_encode(SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns ));
	}
	//更新企業權限
	function updateEnterpriseFlag(){
		$this->user_model->isBackend();
		$user_id = $this->security->xss_clean($this->input->post('user_id'));
		$enterprise_flag = $this->security->xss_clean($this->input->post('enterprise_flag'));
		$enterprise_flag = ($enterprise_flag==1)?"1":"0";
		// echo $enterprise_flag;exit;
		$this->user_model->updateEnterpriseFlag($user_id,$enterprise_flag);
		echo 'true';
	}

	function userProfile($user_id){
		$data['title'] ='檢視使用者資料';
		// $data['getOneUser'] = $this->user_model->getOneUser($this->session->userdata('email'))->row_array();
		$data['userProfile'] = $this->user_model->getOneUserByUserId($user_id)->row_array();
		$this->theme_model->loadTheme('setting/userProfile',$data);
	}
}

/* End of file setting.php */
/* Location: ./application/controllers/setting.php */
