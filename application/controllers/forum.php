<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forum extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
		$this->load->model('forum_model');
		$this->load->model('user_model');
		$this->load->model('backend_model');
		$this->load->model('notifications_model');
		$this->load->model('forum_subscription_model');
		$this->load->model('forum_assistant_model');
		$this->load->model('mail_model');
		$this->load->model('points_model');
    }

	function index()
	{
		$data['title']='討論區';
		$page =  abs(round($this->input->get('page')));
		$total_rows = $this->forum_model->getForumTotal();
		//每頁筆數
		$per_page = 5; 
		$start = $per_page*($page-1);
		if($start<0) $start=0;
		if($page){
			$data['previous'] =$page-1;
			($total_rows <= $per_page*$page)?$data['next'] = '':$data['next'] = $page+1;
		}else{
			$data['previous'] = 0;
			($total_rows <= $per_page)?$data['next'] = '':$data['next'] = $page+2;
		}
		$data['getTopic']= json_encode($this->forum_model->getForumPagination($start,$per_page)->result());
		$data['sideBar'] = json_encode($this->forum_model->sideBar()->result());
		// $data['courseUrl'] = json_encode($this->forum_model->courseUrl()->result());
		$this->theme_model->loadTheme('forum/listAllForum',$data);
	}
	function add()
	{
		$time = $this->theme_model->nowTime();
		$user_id = $this->session->userdata('user_id');
		//檢查使用者是否登入
		$this->user_model->isLogin();
		//form_topic
		$topicData = array(
			'forum_topic' => $this->security->xss_clean($this->input->post("forum_topic")),
		 	 'library_id' => $this->security->xss_clean($this->input->post("library_id")),
		 	 'course_url' => $this->security->xss_clean($this->input->post("course_url")),
				'user_id' => $user_id,
			'posted_time' => $time
		);
		$forum_topic_id = $this->forum_model->insertTopic($topicData);
		$contentData = array(
		     'topic_flag' => '1',
		  'forum_content' => $this->security->xss_clean($this->input->post("forum_content")),
				'user_id' => $user_id,
	'content_posted_time' => $time,
		 'forum_topic_id' => $forum_topic_id
		);
		$this->forum_model->insertContent($contentData);
		//將除了發問者以外的助教、老師或總監加入該討論區主題訂閱
		$this->notifyForumAssistants($user_id,$this->security->xss_clean($this->input->post("library_id")),$forum_topic_id);
		//通知除了回覆者以外，有訂閱該討論串的用戶
		$this->addNotification($forum_topic_id,2);
		//寄發Email給助教、老師、總監
		$emailData=array(
				'user_id' => $user_id,
				'forum_topic' 		=> $this->security->xss_clean($this->input->post("forum_topic")),
				'forum_content' 	=> $this->security->xss_clean($this->input->post("forum_content")),
				'library_id' 		=> $this->security->xss_clean($this->input->post("library_id")),
				'forum_topic_id' 	=> $forum_topic_id,
				'ask_email'=>$this->session->userdata('email'),
				'ask_alias'=>$this->session->userdata('alias')
		);
		$this->sendEmailToForumAssistants($emailData);
		//將訊息透過Pubnub發送Notification
		//通知助教、老師、總監
		$this->publishToChannel('forumContent_'.$forum_topic_id,json_encode($contentData));
		//若助教、老師、總監登入中，觸發他們更新Notification
		$this->publishToChannel('library_'.$this->security->xss_clean($this->input->post("library_id")),json_encode($contentData));
		//若使用者勾選有新回應時通知我，則將用戶加入討論串訂閱
		$this->addForumSubscription($forum_topic_id);
		if($this->input->post("forum_flag")){
			redirect('college/domain/'.$this->input->post("forum_flag"));
		}else if($this->input->post("course_url")){
			redirect('forum/courseContent/'.$forum_topic_id);
		}else{
			redirect('forum/forumContent/'.$forum_topic_id);
		}
		
	}
	function domain($library)
	{
		$data['title']='討論區';	
		$page =  abs(round($this->input->get('page')));
		$total_rows =  $this->forum_model->getForumDomainTotal($library);
		$per_page = 5; 
		$start = $per_page*($page-1);
		if($start<0) $start=0;
		if($page){
			$data['previous'] =$page-1;
			($total_rows <= $per_page*$page)?$data['next'] = '':$data['next'] = $page+1;
		}else{
			$data['previous'] =$page;
			//page沒顯示跟page=1是同一頁，因此page不存在時，當前頁面應該是1，下一頁應該是2
			($total_rows <= $per_page)?$data['next'] = '':$data['next'] = $page+2;
		}
		
		$data['getTopic']=json_encode($this->forum_model->getForumDomainPagination($library,$start,$per_page)->result());
		$data['sideBar'] = json_encode($this->forum_model->sideBar()->result());
		// $data['courseUrl'] = json_encode($this->forum_model->courseUrl()->result());
		$this->theme_model->loadTheme('forum/listAllForum',$data);
	}
	function course($courseUrl)
	{
		$data['title']='討論區';	
		$page =  abs(round($this->input->get('page')));
		$total_rows =  $this->forum_model->getForumCourseTotal($courseUrl);
		$per_page = 5; 
		$start = $per_page*($page-1);
		if($start<0) $start=0;
		if($page){
			$data['previous'] =$page-1;
			($total_rows <= $per_page*$page)?$data['next'] = '':$data['next'] = $page+1;
		}else{
			$data['previous'] =$page;
			//page沒顯示跟page=1是同一頁，因此page不存在時，當前頁面應該是1，下一頁應該是2
			($total_rows <= $per_page)?$data['next'] = '':$data['next'] = $page+2;
		}
		
		$data['getTopic']=json_encode($this->forum_model->getForumCoursePagination($courseUrl,$start,$per_page)->result());
		// $data['sideBar'] = json_encode($this->forum_model->sideBar()->result());
		// $data['courseUrl'] = json_encode($this->forum_model->courseUrl()->result());
		$course = $this->forum_model->getLibraryIdbyCourseUrl($courseUrl)->row_array();
		$data['library_id'] = $course['library_id'];
		$data['library_url'] = $course['library_url'];
		$data['course_url'] = $courseUrl;
		$this->theme_model->loadTheme('forum/listCourseForum',$data);
	}
	function forumContent($forum_topic_id)
	{
		//驗證$forum_topic_id是否為數字
		if(!is_numeric($forum_topic_id)){
			redirect('forum');
		}
		$data['title']='討論區回覆';
		$data['getForumTopic']=json_encode($this->forum_model->getForumTopic($forum_topic_id)->result());
		$data['getForumContent']=json_encode($this->forum_model->getForumContent($forum_topic_id)->result());
		//取得使用者投過的票
		$user_id = $this->session->userdata('user_id');
		if($user_id){
			$data['getUserTopicVote'] = json_encode($this->forum_model->getUserTopicVote($user_id,$forum_topic_id)->result());
			$data['getUserContentVote'] = json_encode($this->forum_model->getUserContentVote($user_id,$forum_topic_id)->result());
            //新增閱讀討論區文章(xxx於幾點幾分閱讀xxx文章)
            $time = $this->theme_model->nowTime();
            $user_id = $this->session->userdata('user_id');
            $readData = array(
            'forum_topic_id' => $forum_topic_id,
                'user_id' => $user_id,
            'read_time' => $time
            );
            $this->forum_model->insertTopicRead($readData);
		}
		
		//驗證$forum_topic_id是否為有效ID
		//$row = json_decode( $data['getForumTopic'] || $data['getForumContent']);
		$row = json_decode( $data['getForumTopic']);
		if(!$row){
			redirect('forum');
		}
		$data['getForumComment']=json_encode($this->forum_model->getForumComment($forum_topic_id)->result());
		$data['sideBar'] = json_encode($this->forum_model->sideBar()->result());
		$data['courseUrl'] = json_encode($this->forum_model->courseUrl()->result());
		$data['isTopicSubscribed'] = $this->forum_subscription_model->isTopicSubscribedEmail($this->session->userdata('user_id'),$forum_topic_id);
		//update forum的觀看次數
		$this->backend_model->updateUserClicks('forum_topic',$forum_topic_id);
		//$this->load->library('markdown');
		
		//以下facebook
		$this->load->library('facebook');
        $user = $this->facebook->getUser();
		////
		if(!$this->session->userdata('user_id')){
		if ($user) {
		  try {
		    // Proceed knowing you have a logged in user who's authenticated.
		    $user_profile = $this->facebook->api('/me');
		    //set_fb_session
		    $fb_data = array(  			
			'fb_login'  => '1',
			'fb_logout_url' => $this->facebook->getLogoutUrl(array('next' => base_url().'home/logout'))
			);
			$this->session->set_userdata($fb_data);
			
		  } catch (FacebookApiException $e) {
		    error_log($e);
		    $user = null;
		    //unset_fb_session
		    $this->session->unset_userdata('fb_login');
		    $this->session->unset_userdata('fb_logout_url');
		  }
		}
		if ($user):
            //$data['logout_url'] = $this->facebook->getLogoutUrl();
        	$check_email = $this->user_model->checkFbEmail($user_profile['email']);
			//get alias
			$alias = strstr($user_profile['email'], '@', true);
			$i = 10;
			while ($i > 0) {
				$i--;
				$checkalias = $this->user_model->checkAlias($alias);
				if($checkalias=='1'){
					$alias = $alias.rand(1,20);
					$checkalias = $this->user_model->checkAlias($alias);
					if($checkalias=='0'){
						break;
					}
				}
			}
			$now = $this->theme_model->nowTime();
			$row = $check_email->row();
			$count = $row->count;
			$count = $count+1;
			if($check_email->num_rows()==0)://資料庫無資料_新用戶 
				$fb_insert_data = array(	
					'name' => $user_profile['name'], 
					'email' => $user_profile['email'],
					'alias' => $alias,
					'last_login' => $now,
					'register_date' => $now,
					'fb_id' => $user_profile['id'],
					'active_flag'=>'1');
				$this->user_model->fb_insert_new($fb_insert_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				//設定session
				$this->theme_model->set_session($user_profile['email']);
				//提示訊息
				//$this->ym->flash_session("alert-success",'Facebook註冊成功，歡迎您');	
				redirect('forum/forumContent/'.$forum_topic_id);
			elseif($row->email==$user_profile['email']&&$row->fb_id=='')://資料庫有mail，但尚未綁定fb帳號
				$fb_update_data=array(
					'fb_id'=>$user_profile['id'],
					'last_login'=> $now,
					'count' =>$count,
					'active_flag'=>'1');
				$this->user_model->fb_update($user_profile['email'],$fb_update_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				//設定session
				$this->theme_model->set_session($user_profile['email']);
				//提示訊息
				//$this->ym->flash_session("alert-success","成功綁定 email ");	
				redirect('forum/forumContent/'.$forum_topic_id);
			elseif($row->email==$user_profile['email']&&$row->fb_id==$user_profile['id']):
				$fb_update_data=array(
					'last_login'=> $now,
					'count' =>$count);
				$this->user_model->fb_update($user_profile['email'],$fb_update_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				$this->theme_model->set_session($user_profile['email']);
				redirect('forum/forumContent/'.$forum_topic_id);
			endif;
        else:
            $data['login_url'] = $this->facebook->getLoginUrl(array('scope' => 'email'));
        endif;
		}else{
			 $data['login_url'] = $this->facebook->getLoginUrl(array('scope' => 'email'));
		}
		$this->theme_model->loadTheme('forum/forumContent',$data);
	}
	function courseContent($forum_topic_id)
	{
		//驗證$forum_topic_id是否為數字
		if(!is_numeric($forum_topic_id)){
			redirect('forum');
		}
		$data['title']='討論區回覆';
		$data['getForumTopic']=json_encode($this->forum_model->getForumTopic($forum_topic_id)->result());
		$data['getForumContent']=json_encode($this->forum_model->getForumContent($forum_topic_id)->result());
		//取得使用者投過的票
		$user_id = $this->session->userdata('user_id');
		if($user_id){
			$data['getUserTopicVote'] = json_encode($this->forum_model->getUserTopicVote($user_id,$forum_topic_id)->result());
			$data['getUserContentVote'] = json_encode($this->forum_model->getUserContentVote($user_id,$forum_topic_id)->result());
            //新增閱讀討論區文章(xxx於幾點幾分閱讀xxx文章)
            $time = $this->theme_model->nowTime();
            $user_id = $this->session->userdata('user_id');
            $readData = array(
            'forum_topic_id' => $forum_topic_id,
                'user_id' => $user_id,
            'read_time' => $time
            );
            $this->forum_model->insertTopicRead($readData);
		}
		
		//驗證$forum_topic_id是否為有效ID
		//$row = json_decode( $data['getForumTopic'] || $data['getForumContent']);
		$row = json_decode( $data['getForumTopic']);
		
		if(!$row){
			redirect('forum');
		}
		$data['getForumComment']=json_encode($this->forum_model->getForumComment($forum_topic_id)->result());
		$data['sideBar'] = json_encode($this->forum_model->sideBar()->result());
		$data['courseUrl'] = json_encode($this->forum_model->courseUrl()->result());
		$data['isTopicSubscribed'] = $this->forum_subscription_model->isTopicSubscribedEmail($this->session->userdata('user_id'),$forum_topic_id);
		//update forum的觀看次數
		$this->backend_model->updateUserClicks('forum_topic',$forum_topic_id);
		//$this->load->library('markdown');
		
		//以下facebook
		$this->load->library('facebook');
        $user = $this->facebook->getUser();
		////
		if(!$this->session->userdata('user_id')){
		if ($user) {
		  try {
		    // Proceed knowing you have a logged in user who's authenticated.
		    $user_profile = $this->facebook->api('/me');
		    //set_fb_session
		    $fb_data = array(  			
			'fb_login'  => '1',
			'fb_logout_url' => $this->facebook->getLogoutUrl(array('next' => base_url().'home/logout'))
			);
			$this->session->set_userdata($fb_data);
			
		  } catch (FacebookApiException $e) {
		    error_log($e);
		    $user = null;
		    //unset_fb_session
		    $this->session->unset_userdata('fb_login');
		    $this->session->unset_userdata('fb_logout_url');
		  }
		}
		if ($user):
            //$data['logout_url'] = $this->facebook->getLogoutUrl();
        	$check_email = $this->user_model->checkFbEmail($user_profile['email']);
			//get alias
			$alias = strstr($user_profile['email'], '@', true);
			$i = 10;
			while ($i > 0) {
				$i--;
				$checkalias = $this->user_model->checkAlias($alias);
				if($checkalias=='1'){
					$alias = $alias.rand(1,20);
					$checkalias = $this->user_model->checkAlias($alias);
					if($checkalias=='0'){
						break;
					}
				}
			}
			$now = $this->theme_model->nowTime();
			$row = $check_email->row();
			$count = $row->count;
			$count = $count+1;
			if($check_email->num_rows()==0)://資料庫無資料_新用戶 
				$fb_insert_data = array(	
					'name' => $user_profile['name'], 
					'email' => $user_profile['email'],
					'alias' => $alias,
					'last_login' => $now,
					'register_date' => $now,
					'fb_id' => $user_profile['id'],
					'active_flag'=>'1');
				$this->user_model->fb_insert_new($fb_insert_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				//設定session
				$this->theme_model->set_session($user_profile['email']);
				//提示訊息
				//$this->ym->flash_session("alert-success",'Facebook註冊成功，歡迎您');	
				redirect('forum/courseContent/'.$forum_topic_id);
			elseif($row->email==$user_profile['email']&&$row->fb_id=='')://資料庫有mail，但尚未綁定fb帳號
				$fb_update_data=array(
					'fb_id'=>$user_profile['id'],
					'last_login'=> $now,
					'count' =>$count,
					'active_flag'=>'1');
				$this->user_model->fb_update($user_profile['email'],$fb_update_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				//設定session
				$this->theme_model->set_session($user_profile['email']);
				//提示訊息
				//$this->ym->flash_session("alert-success","成功綁定 email ");	
				redirect('forum/courseContent/'.$forum_topic_id);
			elseif($row->email==$user_profile['email']&&$row->fb_id==$user_profile['id']):
				$fb_update_data=array(
					'last_login'=> $now,
					'count' =>$count);
				$this->user_model->fb_update($user_profile['email'],$fb_update_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				$this->theme_model->set_session($user_profile['email']);
				redirect('forum/courseContent/'.$forum_topic_id);
			endif;
        else:
            $data['login_url'] = $this->facebook->getLoginUrl(array('scope' => 'email'));
        endif;
		}else{
			 $data['login_url'] = $this->facebook->getLoginUrl(array('scope' => 'email'));
		}
		$row = json_decode( $data['getForumTopic']);
		$data['library_id'] = $row[0]->library_id;
		$data['course_url'] = $row[0]->course_url;
		$this->theme_model->loadTheme('forum/courseContent',$data);
	}
	//新增討論區的回覆文章
	function addForumComment()
	{
		$time = $this->theme_model->nowTime();
		$user_id = $this->session->userdata('user_id');
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$forum_content_id = $this->security->xss_clean($this->input->post("forum_content_id"));
		$forum_topic_id = $this->security->xss_clean($this->input->post("forum_topic_id"));
		$commentData = array(
			'forum_comment' 		=> $this->security->xss_clean($this->input->post("forum_comment")),
	 	 	'forum_content_id' 		=> $forum_content_id,
			'user_id' 				=> $user_id,
			'comment_posted_time' 	=> $time
		);	
		$this->forum_model->insertComment($commentData);
		//通知除了回覆者以外，有訂閱該討論串的用戶
		$this->addNotification($forum_topic_id,1);
		//寄發Email給助教、老師、總監
		$getOneForumTopic=$this->forum_model->getOneForumTopic($forum_topic_id);
		$forum_topic=$getOneForumTopic->forum_topic;
		$emailData=array(
				'user_id' => $user_id,
				'forum_topic' 		=> $forum_topic,
				'forum_content' 	=> $this->security->xss_clean($this->input->post("forum_content")),
				'library_id' 		=> $this->security->xss_clean($this->input->post("library_id")),
				'forum_topic_id' 	=> $forum_topic_id,
				'ask_email'=>$this->session->userdata('email'),
				'ask_alias'=>$this->session->userdata('alias')
		);
		$this->sendReplyEmailToForumSubscribers($emailData);
		//將訊息透過Pubnub發送Notification
		$this->publishToChannel('forumContent_'.$forum_topic_id,json_encode($commentData));
		//若使用者勾選有新回應時通知我，則將用戶加入討論串訂閱
		$this->addForumSubscription($forum_topic_id);
		$course_url = $this->security->xss_clean($this->input->post("course_url"));
		if($course_url){
			redirect('forum/courseContent/'.$forum_topic_id);
		}else{
			redirect('forum/forumContent/'.$forum_topic_id);
		}
		//送出問題後回到原討論串
	}
	//新增討論區的回答問題
	function addForumContent()
	{
		$time = $this->theme_model->nowTime();
		$user_id = $this->session->userdata('user_id');
		//檢查使用者是否登入
		$this->user_model->isLogin();
		
		$forum_topic_id = $this->security->xss_clean($this->input->post("forum_topic_id"));
		$contentData = array(
			'forum_content' 		=> $this->security->xss_clean($this->input->post("forum_content")),
		 	'forum_topic_id' 		=> $forum_topic_id,
			'user_id' 				=> $user_id,
			'content_posted_time' 	=> $time
		);
		
		$this->forum_model->insertContent($contentData);
		//通知除了回覆者以外，有訂閱該討論串的用戶
		$this->addNotification($forum_topic_id,1);
		//寄發Email給助教、老師、總監
		$getOneForumTopic=$this->forum_model->getOneForumTopic($forum_topic_id);
		$forum_topic=$getOneForumTopic->forum_topic;
		$emailData=array(
				'user_id' => $user_id,
				'forum_topic' 		=> $forum_topic,
				'forum_content' 	=> $this->security->xss_clean($this->input->post("forum_content")),
				'library_id' 		=> $this->security->xss_clean($this->input->post("library_id")),
				'forum_topic_id' 	=> $forum_topic_id,
				'ask_email'=>$this->session->userdata('email'),
				'ask_alias'=>$this->session->userdata('alias')
		);
		$this->sendReplyEmailToForumSubscribers($emailData);
		//將訊息透過Pubnub發送Notification
		$this->publishToChannel('forumContent_'.$forum_topic_id,json_encode($contentData));
		//若使用者勾選有新回應時通知我，則將用戶加入討論串訂閱
		$this->addForumSubscription($forum_topic_id);
		$course_url = $this->security->xss_clean($this->input->post("course_url"));
		if($course_url){
			redirect('forum/courseContent/'.$forum_topic_id);
		}else{
			redirect('forum/forumContent/'.$forum_topic_id);
		}
	}
	//修改討論區的content
	function updateForumContent()
	{
		$user_id = $this->session->userdata('user_id');
		if(!$user_id){
			$this->theme_model->flash_session("alert-danger","請先登入在使用討論區功能");
			$this->session->set_userdata('src', $_SERVER['HTTP_REFERER']);
			die();
		}
		$forum_content = $this->security->xss_clean($this->input->post("forum_content"));
		$forum_content_id = $this->security->xss_clean($this->input->post("forum_content_id"));
		$forum_topic_id = $this->security->xss_clean($this->input->post("forum_topic_id"));
		$contentData = array(
			'forum_content' => $forum_content
		);	
		$this->forum_model->updateContent($user_id,$forum_content_id,$contentData);
		$course_url = $this->security->xss_clean($this->input->post("course_url"));
		if($course_url){
			redirect('forum/courseContent/'.$forum_topic_id);
		}else{
			redirect('forum/forumContent/'.$forum_topic_id);
		}
	}
	//修改討論區的comment
	function updateForumComment()
	{
		$user_id = $this->session->userdata('user_id');
		if(!$user_id){
			$this->theme_model->flash_session("alert-danger","請先登入在使用討論區功能");
			$this->session->set_userdata('src', $_SERVER['HTTP_REFERER']);
			die();
		}
		//$forum_comment = htmlspecialchars($this->input->post("forum_comment"), ENT_COMPAT);
		//$forum_comment = $this->security->xss_clean($this->input->post("forum_comment"));
		$forum_comment = $this->input->post("forum_comment");
		$forum_comment_id = $this->security->xss_clean($this->input->post("forum_comment_id"));
		$forum_topic_id = $this->security->xss_clean($this->input->post("forum_topic_id"));
		$commentData = array(
			'forum_comment' => $forum_comment
		);	
		$this->forum_model->updateComment($user_id,$forum_comment_id,$commentData);
		
		$course_url = $this->security->xss_clean($this->input->post("course_url"));
		if($course_url){
			redirect('forum/courseContent/'.$forum_topic_id);
		}else{
			redirect('forum/forumContent/'.$forum_topic_id);
		}
	}
	//修改討論區的topic and content
	function updateForumTopic()
	{
		$user_id = $this->session->userdata('user_id');
		if(!$user_id){
			$this->theme_model->flash_session("alert-danger","請先登入在使用討論區功能");
			$this->session->set_userdata('src', $_SERVER['HTTP_REFERER']);
			die();
		}
		$forum_topic = $this->security->xss_clean($this->input->post("forum_topic"));
		$forum_topic_id = $this->security->xss_clean($this->input->post("forum_topic_id"));
		$forum_content = $this->security->xss_clean($this->input->post("forum_content"));
		$forum_content_id = $this->security->xss_clean($this->input->post("forum_content_id"));
		$topicData = array(
			'forum_topic' => $forum_topic
		);
		$contentData = array(
			'forum_content' => $forum_content
		);	
		
		$this->forum_model->updateTopic($user_id,$forum_topic_id,$topicData);
		$this->forum_model->updateContent($user_id,$forum_content_id,$contentData);
		$course_url = $this->security->xss_clean($this->input->post("course_url"));
		if($course_url){
			redirect('forum/courseContent/'.$forum_topic_id);
		}else{
			redirect('forum/forumContent/'.$forum_topic_id);
		}
	}
	//通知除了回覆者以外，有訂閱該討論串的用戶
	function addNotification($forum_topic_id,$notification_type){
		$time = $this->theme_model->nowTime();
		$user_id = $this->session->userdata('user_id');
		$topicSubscriber=$this->forum_subscription_model->getForumTopicSubscriber($forum_topic_id)->result();
		$getOneForumTopic=$this->forum_model->getOneForumTopic($forum_topic_id);
		$notification_title=$getOneForumTopic->forum_topic;
		$notification=array();
		$i=0;
		foreach($topicSubscriber as $key=>$value){
			$notification[$i]=array(
					'user_id'				=>$topicSubscriber[$key]->user_id,
					'notification_type'		=>$notification_type,
					'notification_title'	=>$notification_title,
					'notification_content'	=>'',
					'hyperlink'				=>$forum_topic_id,
					'notification_time'		=>$time,
					'notifier_id'			=>$user_id
			);
			if($user_id!=$topicSubscriber[$key]->user_id){
				$this->notifications_model->addNotification($notification[$i]);
			}
			$i++;
		}
	}
	//若使用者勾選有新回應時通知我，則將用戶加入討論串訂閱
	function addForumSubscription($forum_topic_id){
		$time = $this->theme_model->nowTime();
		$user_id = $this->session->userdata('user_id');
		$notify= $this->security->xss_clean($this->input->post("notify-me"));
		if(empty($notify)){
			$notify=0;
		}else{
			$notify=1;
		}
		$forumSubscription=array(
				'user_id' 			=> $user_id,
				'forum_topic_id' 	=> $forum_topic_id,
				'subscription_time' => $time,
				'status' 			=> $notify
		);
		if($this->forum_subscription_model->isTopicSubscribed($user_id,$forum_topic_id)){
			$where=array(	'user_id' 			=> $user_id,
							'forum_topic_id' 	=> $forum_topic_id
			);
			$forumSubscription=array(
					'subscription_time' => $time,
					'status' 			=> $notify
			);
			$this->forum_subscription_model->updateForumSubscription($forumSubscription,$where);
		}else{
			$forumSubscription=array(
					'user_id' 			=> $user_id,
					'forum_topic_id' 	=> $forum_topic_id,
					'subscription_time' => $time,
					'status' 			=> $notify
			);
			$this->forum_subscription_model->addForumSubscription($forumSubscription);
		}
	}
	//移除最佳解答
	function removeBestAnswer(){
		$forum_content_id = $this->security->xss_clean($this->input->post("forum_content_id"));
		$time = $this->theme_model->nowTime();
		$row = $this->forum_model->getForumTopicId($forum_content_id);
		$forum_topic_id = $row['forum_topic_id'];
		//移除content 的最佳解
		$data = array('best_answer' => '0');
		$this->forum_model->updateContentBestAnswer($forum_content_id,$data);
		//移除topic 的最佳解
		$data = array('solved' => '0','solved_time' =>'0');
		$this->forum_model->updateTopicSolved($forum_topic_id,$data);
	}
	//換一個最佳解答
	function changeBestAnswer(){
		$forum_content_id = $this->security->xss_clean($this->input->post("forum_content_id"));
		$time = $this->theme_model->nowTime();
		$row = $this->forum_model->getForumTopicId($forum_content_id);
		$forum_topic_id = $row['forum_topic_id'];
		//取得最佳解的使用者的ID
		$user_id = $row['user_id'];
		//移除所有content 的最佳解
		$data = array('best_answer' => '0');
		$this->forum_model->updateAllContentBestAnswer($forum_topic_id,$data);
		//新增新選到content 的最佳解
		$data = array('best_answer' => '1');
		$this->forum_model->updateContentBestAnswer($forum_content_id,$data);
		//新增topic 的最佳解
		$data = array('solved' => '1','solved_time' => $time);
		//檢查最佳解的使用者是否已取得積分(只能得到一次)
		$checkForumPoint = $this->points_model->checkForumPoint($user_id,$forum_content_id);
		if(!$checkForumPoint){
			//新增積分給提供最佳解的使用者
			$this->points_model->insertPoint($user_id,'forum_best_answer','','','',$forum_content_id);
		}
		$this->forum_model->updateTopicSolved($forum_topic_id,$data);
	}
	//取得討論內容的總票數
	function getVote(){
		$forum_content_id = $this->security->xss_clean($this->input->post("forum_content_id"));
		$row = $this->forum_model->getVote($forum_content_id);
		echo $row['sum'];
	}
	//檢查有無投過票
	function checkVote(){
		$user_id = $this->session->userdata('user_id');
		$forum_content_id = $this->security->xss_clean($this->input->post("forum_content_id"));
		//檢查是否有新增過，如果沒有就新增一個吧
		echo $this->forum_model->checkVote($user_id,$forum_content_id);
	}
	//投票囉
	function doVote(){
		$user_id = $this->session->userdata('user_id');
		if($user_id){
			$forum_content_id = $this->security->xss_clean($this->input->post("forum_content_id"));
			$vote = $this->security->xss_clean($this->input->post("vote"));
			$voteData = array(
				'user_id' 			=> $user_id,
			 	'forum_content_id' 	=> $forum_content_id,
				'vote' 				=> $vote,
				'vote_date'		 	=> $this->theme_model->nowTime()
			);
			$this->forum_model->doVote($voteData);
			$row = $this->forum_model->getVote($forum_content_id);
			$votes = $row['sum'];
			$updateVoteData = array(
		 		'forum_content_id' 	=> $forum_content_id,
				'votes' 				=> $votes
			);
			$this->forum_model->updateVote($forum_content_id,$updateVoteData);
			echo true;
		}else{
			echo false;
		}
		
	}
	//將除了發問者以外的助教、老師或總監加入該討論區主題訂閱
	/*	
	*	$user_id 		發問者的使用者編號
	*	$library_id 	發問者的提問的討論區分類編號
	*	$forum_topic_id 發問者的提問的討論區主題編號
	*/
	function notifyForumAssistants($user_id,$library_id,$forum_topic_id){
		$getForumAssistants=$this->forum_assistant_model->getForumAssistants($user_id,$library_id)->result();
		$time = $this->theme_model->nowTime();
		foreach($getForumAssistants as $key=>$value){
// 			echo $getForumAssistants[$key]->user_id;
			$forumSubscription=array(
					'user_id' 			=> $getForumAssistants[$key]->user_id,
					'forum_topic_id' 	=> $forum_topic_id,
					'subscription_time' => $time,
					'status' 			=> 1
			);
			$this->forum_subscription_model->addForumSubscription($forumSubscription);
		}
	}
	function publishToChannel($channel,$message){
		//讀入Pubnub SDK
		require_once(APPPATH.'../application/libraries/Pubnub.php');
		//初始化Pubnub
		$pubnub=new Pubnub('pub-c-3d1904ba-8858-4977-a188-ccfe444100e8','sub-c-2a5cc3d6-cead-11e3-9782-02ee2ddab7fe');
		//將訊息送至指定的Channel
		$pubnub->publish(array(
				'channel'=>$channel,
				'message'=>$message
		));
	}
	
	function sendEmailToForumAssistants($emailData){
		$getForumAssistants=$this->forum_assistant_model->getForumAssistants($emailData['user_id'],$emailData['library_id'])->result();
		if(!empty($getForumAssistants)){
			foreach($getForumAssistants as $key=>$value){
				$data=array(
					'email'			=>$getForumAssistants[$key]->email,
					'library_name'	=>$getForumAssistants[$key]->library_name,
					'support_type'	=>$getForumAssistants[$key]->support_type,
					'forum_topic'	=>$emailData['forum_topic'],
					'forum_content'	=>$emailData['forum_content'],
					'hyperlink'		=>base_url().'forum/forumContent/'.$emailData['forum_topic_id'],
					'alias'			=>$getForumAssistants[$key]->alias,
					'ask_alias'		=>$emailData['ask_alias']
				);
				//寄信給發問者以外的助教、老師、總監
				if($getForumAssistants[$key]->email!=$emailData['ask_email']){
					$this->mail_model->sendEmailToForumAssistants($data);
				}
			}
		}
	}
	function sendReplyEmailToForumSubscribers($emailData){
		$getForumTopicEmailSubscriber=$this->forum_subscription_model->getForumTopicEmailSubscriber($emailData['forum_topic_id'])->result();
		if(!empty($getForumTopicEmailSubscriber)){
			foreach($getForumTopicEmailSubscriber as $key=>$value){
				$data=array(
					'email'			=>$getForumTopicEmailSubscriber[$key]->email,
					'forum_topic'	=>$emailData['forum_topic'],
					'forum_content'	=>$emailData['forum_content'],
					'hyperlink'		=>base_url().'forum/forumContent/'.$emailData['forum_topic_id'],
					'alias'			=>$getForumTopicEmailSubscriber[$key]->alias,
					'ask_alias'		=>$emailData['ask_alias']
				);
				//寄信給發問者以外的助教、老師、總監
				if($getForumTopicEmailSubscriber[$key]->email!=$emailData['ask_email']){
					$this->mail_model->sendReplyEmailToForumSubscribers($data);
				}
			}
		}
	}
}

/* End of file forum.php */
/* Location: ./application/controllers/forum.php */