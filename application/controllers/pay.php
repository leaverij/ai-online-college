<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pay extends CI_Controller {
	
	function __construct()
    {
		parent::__construct();
		$this->load->model('course_model');
		$this->load->model('pay_model');
		$this->load->model('library_model');
		$this->load->model('tracks_model');
		$this->load->model('coupon_model');
    }

	function index()
	{
		$data['title']='信用卡付費頁面';
		//檢查使用者是否登入
        $this->user_model->isLogin();
		$this->theme_model->loadTheme('pay/payCard',$data);
	}

	//課程立即付款頁面
	function express($course_id){
		$data['title'] = "課程付款";
		$this->user_model->isLogin();
		// 設定課程頁面到付款頁面的session，方便回去課程頁面
		$this->session->set_userdata('course_src', $_SERVER['HTTP_REFERER']);
		$data['course'] = json_encode($this->course_model->getOneCourseByCourseId($course_id)->row_array());
		$this->theme_model->loadTheme('pay/payCourse',$data);
	}

	//購買紀錄
	function paid(){
		$data['title'] = "購買紀錄";
		$user_id = $this->session->userdata('user_id');
		$data['courses'] = $this->course_model->getLearnedCourses($user_id);
		$this->theme_model->loadTheme('pay/payCourseList', $data);
	}

	//導進課程購買頁面
	function payCard(){
		$OrderDetailSN = 'CLD1234';
		$Amount = 1;
		$RETURL = 'https://w3.iiiedu.org.tw/FormCheckResult_r.php';
		//指定國泰世華第二次轉址連結
		$FinalURL = $RETURL;
		$STOREID = '';//'011020292';//廠商代號
		$CUBKEY = '';//'4f4eef379d4c0595db7ae757d4010306';
		$CAVALUE = $STOREID.$OrderDetailSN.$Amount.$CUBKEY; //以MD5演算法加密
		$CAVALUE = md5($CAVALUE);//驗證碼
		//建立XML內容
		$post ="<MERCHANTXML>\n";
		$post.="<CAVALUE>$CAVALUE</CAVALUE>\n";//驗證碼   
		$post.="<ORDERINFO>\n";
		$post.="<STOREID>$STOREID</STOREID>\n";    //廠商代號
		$post.="<ORDERNUMBER>$OrderDetailSN</ORDERNUMBER>\n"; //訂單編號
		$post.="<AMOUNT>$Amount</AMOUNT>\n";   //授權金額
		$post.="</ORDERINFO>\n";
		$post.="</MERCHANTXML>\n";
		echo '
		<form name="frm_card" method="post" action="https://sslpayment.uwccb.com.tw/EPOSService/Payment/OrderInitial.aspx" >
		<input type="hidden" name="strRqXML" value="'.$post.'">
		</form>
		<script>
		document.frm_card.submit();
		</script>
		';
	}

	function payCardResult()
	{
		$data['title']='信用卡付費回傳頁面';
		$OrderDetailSN = $this->input->get('OrderDetailSN');
			   $Amount = $this->input->get('Amount');
			   $Status = $this->input->get('Status');
			   $CardSN = $this->input->get('CardSN');
		$data['result'] = array('OrderDetailSN'=>$OrderDetailSN, 'Amount'=>$Amount, 'Status'=>$Status, 'CardSN'=>$CardSN);
		//create cURL connection
		$curl_connection = curl_init('https://w3.iiiedu.org.tw/DataCheck.php');
		//set options
		curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($curl_connection, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
		curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl_connection, CURLOPT_POST, true); 
		//set data to be posted
		curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $data['result']);
		//perform our request
		$result = curl_exec($curl_connection);
		//show information regarding the request
		//print_r(curl_getinfo($curl_connection));
		//echo curl_errno($curl_connection) . '-' . curl_error($curl_connection);
 
 		$response = curl_exec($curl_connection);
		//echo $response; 
		//close the connection
		curl_close($curl_connection);
 		if($response==='true'){
 			$this->theme_model->loadTheme('pay/payCardResult',$data);
 		}else{
 			echo 'wrong parameters';
 			die();
 		}
	}
	//取得課程價格
	function getPrice(){
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$row = $this->library_model->getCoursePrice($course_id)->row_array();
		echo $row['price'];
	}
	
	
	//取得學程價格//今麻煩 取得該學程使用者尚未上過的課程總價格(優惠以及免費要記得去掉)
	function getTracksPrice(){
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$tracks_id = $this->security->xss_clean($this->input->post('tracks_id'));
		$getTracksPrice = $this->tracks_model->getTracksPrice($tracks_id)->result();
		$total_price=0;
		foreach ($getTracksPrice as $trackPrice){
			if($trackPrice->order_detail_id){
				//已經付款過的課程
			}else{
				if($trackPrice->preferential_price){
					$total_price = $total_price + $trackPrice->preferential_price;
				}else{
					$total_price = $total_price + $trackPrice->course_price;
				}
			}
		}
		echo $total_price;
	}

	
	
	//訂閱課程以及購買課程寫入紀錄用的
	function subscribe(){
		//檢查使用者是否登入
        $this->user_model->isLogin();
		$course_id = $this->security->xss_clean($this->input->post('course_id'));
		$type = $this->security->xss_clean($this->input->post('type'));
		$coupon_code = $this->security->xss_clean($this->input->post('coupon_code'));
		// 後端檢查coupon價格是否符合
		$row = $this->library_model->getCoursePrice($course_id)->row_array();
		// echo $row['price'];
		$coupon_code  = htmlspecialchars($coupon_code, ENT_COMPAT);
		$checkCoupon = $this->coupon_model->checkCoupon($coupon_code)->row_array();
		// echo $checkCoupon['price'];
		if($row['price']==0){
			$checkCoupon['price']=0;
		}
		if($row['price']<=$checkCoupon['price']){
			if(is_numeric($course_id)){
				//寫入order 以及 orderdetail
				$orderData = array( 
					'type' => $type,
					'course_id' => $course_id,
					'user_id' => $this->session->userdata('user_id'),
					'live_flag' => '1');
				$order_id = $this->pay_model->insertOrder($orderData);
				$orderDetailData = array( 
					'course_id' => $course_id,
					'user_id' =>$this->session->userdata('user_id'),
					'live_flag' => '1',
					'order_id' =>$order_id);
				$this->pay_model->insertOrderDetail($orderDetailData);	
				echo 'true';
			}else{
				echo 'false';
			}
		}else{
			echo 'false';
		}
	}
	//購買學程寫入紀錄用的
	function subscribeTracks(){
		//檢查使用者是否登入
        $this->user_model->isLogin();
		$tracks_id = $this->security->xss_clean($this->input->post('tracks_id'));
		$type = $this->security->xss_clean($this->input->post('type'));
		if(is_numeric($tracks_id)){
			//寫入order 以及 orderdetail
			$orderData = array( 'type' => $type,
								'tracks_id' => $tracks_id,
								'user_id' => $this->session->userdata('user_id'),
								'live_flag' => '1');
			$order_id = $this->pay_model->insertOrder($orderData);
			//query學程內的所有課程id(已付款過的不寫入)
			$getCourseData = $this->pay_model->getCourseData($tracks_id)->result();
			foreach($getCourseData as $courseData){
				$orderDetailData = 
					array( 'course_id' => $courseData->course_id,
						   'user_id' =>$this->session->userdata('user_id'),
				    	   'live_flag' => '1',
						   'order_id' =>$order_id);
				$this->pay_model->insertOrderDetail($orderDetailData);	
			}
			echo 'true';
		}else{
			echo 'false';
		}
	}
	//更新訂單資訊
	function updateOrder(){
		//檢查使用者是否登入
        $this->user_model->isLogin();
		$course_id = $this->input->post('course_id');
		$data=array(
			'price'				=>$this->input->post('price'),
			'paid'				=>$this->input->post('paid'),
			'coupon_code'		=>$this->input->post('coupon_code'));
		$this->pay_model->updateOrder($course_id,$data);
	}
}

/* End of file pay.php */
/* Location: ./application/controllers/pay.php */
