<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Competitions extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->model('competition_model');
    }

    //競賽首頁
    function index()
	{
        $data['title']='大擂台';
        $data['topic'] = json_encode($this->competition_model->getHomeCompetitionTopics()->result());
        $this->theme_model->loadTheme('competition/home', $data);
    }

    //建立新競賽
    function create(){
        $this->user_model->isLogin();
        $data['title']='建立競賽';
        $topic = $this->competition_model->getCompetitionTopic()->result();
        $data['topic'] = json_encode($topic);
        $data['category'] = json_encode($this->competition_model->getCompetitionCatgoriesByTopicId($topic[0]->competition_topic_id)->result());
        $this->theme_model->loadTheme('competition/new_competition', $data);
    }

    //競賽管理
    function hosted(){
        $input_para = $this->input->get();
        $qdata=array('is_publish'=>'', 'processing'=>'', 'is_public'=>'', 'competition_category_id'=>'', 'name'=>'');
        if($input_para['publish']!='all'){
            $qdata['is_publish'] = $this->security->xss_clean($input_para['publish']);
        }
        if($input_para['processing']!='all'){
            $qdata['processing'] = $this->security->xss_clean($input_para['processing']);
        }
        if($input_para['public']!='all'){
            $qdata['is_public'] = $this->security->xss_clean($input_para['public']);
        }
        if($input_para['category']!='all'){
            $qdata['competition_category_id'] = $this->security->xss_clean($input_para['category']);
        }
        if(trim($input_para['name'])!=''){
            $qdata['name'] = $this->security->xss_clean($input_para['name']);
        }
        $this->user_model->isLogin();
        $user_id = $this->session->userdata('user_id');
        $data['title']='競賽列表';
        $data['category'] = json_encode($this->competition_model->getCompetitionCategory()->result());
        $data['list'] = $this->competition_model->getHostedList($user_id, $qdata)->result_array();
        $data['selection'] = $qdata;
        $this->theme_model->loadTheme('competition/hosted', $data);
    }

    //編輯競賽基本資訊
    function editCompetitionInfo($competition_id){
        $this->user_model->isLogin();
        $user_id = $this->session->userdata('user_id');
        $data['title']='編輯競賽基本資訊';
        $competition = $this->competition_model->getHostedCompetition($user_id, $competition_id)->row_array();
        $data['competition'] = json_encode($competition);
        $data['topic'] = json_encode($this->competition_model->getCompetitionTopic()->result());
        $data['category'] = json_encode($this->competition_model->getCompetitionCatgoriesByTopicId($competition['competition_topic_id'])->result());
        $this->theme_model->loadTheme('competition/edit_competition_info', $data);
    }

    //編輯競賽細節
    function editCompetitionDetail($competition_id){
        $this->user_model->isLogin();
        $user_id = $this->session->userdata('user_id');
        $data['title']='編輯競賽說明';
        $data['competition'] = json_encode($this->competition_model->getHostedCompetition($user_id, $competition_id)->row_array());
        $data['details'] = json_encode($this->competition_model->getCompetitionDetails($competition_id, $user_id)->result_array());
        $this->theme_model->loadTheme('competition/edit_competition_detail', $data);
    }

    //競賽列表
    function lists($competition_id, $offset){
        $data['title']='競賽列表';
        // $search_string = $this->security->xss_clean($this->input->post('search_string'));
        $data['current_topic'] = json_encode($this->competition_model->getOneCompetitionTopic($competition_id)->result_array());
        $search_string = $this->input->get('search_string');
        $data['topic'] = json_encode($this->competition_model->getCompetitionTopic()->result());
        $data['competition_topic_result'] = json_encode($this->competition_model->getCompetiotionTopicTotal()->result());
        $competition_search_total = $this->competition_model-> getCompetitionTopicTotalBySearchString($competition_id, $search_string)->result();
        $data['competition_topic_total'] = $competition_search_total[0]-> counts;
        $data['per_page'] = 10;
        $data['competition'] = json_encode($this->competition_model->getCompetitionByTopicId($competition_id, $search_string, $data['per_page'], ($offset-$data['per_page']))->result());
        $this->theme_model->loadTheme('competition/competition_list', $data);
    }

    //單一競賽頁面
    function details($competition_id){
        $this->user_model->isLogin();
        $user_id = $this->session->userdata('user_id');
        $competition_result = $this->competition_model->getOneCompetition($competition_id);
        if($competition_result->num_rows()!=0){
            $competition_result = $competition_result->row_array();
            $data['competition'] = json_encode($competition_result);
            $data['title'] = $competition_result['name'];
            $data['overview'] = json_encode($this->competition_model->getOneCompetitionDetails($competition_id)->result_array());
            $this->theme_model->loadTheme('competition/competition_details', $data);
        }else{
            redirect('competitions');
        }
    }

}

?>