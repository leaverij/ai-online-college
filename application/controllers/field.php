<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Field extends CI_Controller {
	function __construct()
    {
        parent::__construct();
		$this->load->model('library_model');
		$this->load->model('course_model');
		$this->load->model('chapter_model');
		$this->load->model('content_model');
		$this->load->model('breadcrumb_model');
		$this->load->model('dashboard_model');
		$this->load->model('user_chapter_model');
		$this->load->model('user_content_model');
		$this->load->model('user_course_model');
		$this->load->model('user_model');
		$this->load->model('teacher_model');
		$this->load->model('forum_model');
		$this->load->model('points_model');
		$this->load->model('jupyter_model');
		$this->load->library('facebook');
		$this->load->library('../controllers/dashboard');
		
    }
	function index()
	{
		redirect("track/domain/ai-hospital");	
		//redirect("track/domain/app-development");
		$data['title']='課程庫';
		//load sidebar 所需的資料
		$data['getAll'] = $this->getAll();
		$data['getAllJobsCategory']=json_encode($this->library_model->getAllJobsCategory()->result());
		$data['func'] = 'library'; 
		$this->theme_model->loadTheme('library/allLibraries',$data);
	}
	
	function domain($library)
	{
		$email = $this->session->userdata('email');
		if($library=='search'){
			//搜尋頁面
			$data['title']= 'search';
			$search_string = $this->security->xss_clean($this->input->post('search_string'));
			$search_string=mysql_real_escape_string($search_string);
			if(!trim($search_string) || !trim(str_replace("　", "", $search_string))){
				redirect('library');
			}
			//load sidebar
			$data['getAll']=$this->getAll();
			$data['getAllJobsCategory']=json_encode($this->library_model->getAllJobsCategory()->result());
			
			$data['searchCourse'] = json_encode($this->library_model->searchCourse($search_string)->result());
			$data['searchJobsCategory'] = json_encode($this->library_model->searchJobsCategory($search_string)->result());
			
			//如果有登入，取得使用者的學程學習資料
			if($email){
				$tracks_id = array();
				$tracksPassAmount = array();
				$getPassAmountBySearchTracks=$this->library_model->getPassAmountBySearchTracks($email);
				foreach ($getPassAmountBySearchTracks->result_array() as $row){
					array_push($tracks_id,$row['tracks_id']);
					array_push($tracksPassAmount,$row['passAmount']);
				}
				$data['tracks_id']= $tracks_id;
				$data['tracksPassAmount']= $tracksPassAmount;
				
				$course_id = array();
				$coursePassAmount = array();
				$getPassAmountBySearchCourse=$this->library_model->getPassAmountBySearchCourse($email);
				foreach ($getPassAmountBySearchCourse->result_array() as $row){
					array_push($course_id,$row['course_id']);
					array_push($coursePassAmount,$row['passAmount']);
				}
				$data['course_id']= $course_id;
				$data['coursePassAmount']= $coursePassAmount;
			}
			//把搜尋紀錄儲存起來
			$search_time = $this->theme_model->nowTime();
			//未登入時，email由guest代替
			if(empty($email)) $email='guest';
			$search_data = array(
				'email' => $email,
				'search_string' => $search_string,
				'search_time' => $search_time);
			//搜尋字串為空時不紀錄
			if(!empty($search_string)) $this->course_model->insertSearchRecord($search_data);
			$data['search_string'] = $search_string;
			$this->theme_model->loadTheme('library/searchResult',$data);
			return;
		}
		$data['title']= $library;
		$breadcrumb_temp_url = array();
		$breadcrumb_name = array();
		$breadcrumb = array();
		$course_url = $this->uri->segment(3);
		$chapter_url = $this->uri->segment(4);
		$content_url = $this->uri->segment(5);
		$data['library'] = $library;	
		$data['course'] = $course_url;
		$data['chapter'] = $chapter_url;
		$data['content'] = $content_url;
		$data['sideBar'] = json_encode($this->forum_model->sideBar()->result());		
		if($content_url){
			$this->user_model->isLogin();
			$data['login_url'] = $this->facebookLogin('library/'.$library.'/'.$course_url.'/'.$chapter_url);
			$data['getOneTeacher']=json_encode($this->teacher_model->getOneTeacher($course_url)->result());
			//取得題號，塞進陣列
			$quizNo = array();
			//取得試卷的題數
			$getQuantity = $this->library_model->getQuantity($content_url)->row_array();
			$quantity = $getQuantity['quantity'];
			$content_type = $getQuantity['content_type'];
			
			if($content_type){
				if($quantity){
					if($content_type=='2'){
						$data['getQuizNo'] = $this->library_model->getQuizCodeNo($content_url,$quantity);
					}else{
						$data['getQuizNo'] = $this->library_model->getQuizNo($content_url,$quantity);
					}
					foreach ($data['getQuizNo']->result_array() as $row)
					{
						array_push($quizNo,$row['question_id']);
					}
				}
			}
			$teachedCourse = $this->course_model->getOneCourse($course_url)->row_array();
			$getCourseOrderDetail = $this->library_model->getCourseOrderDetail($course_url)->row_array();
			$data['getOneContent'] = $this->getOneContent($content_url);
			$data['getNextContent'] = $this->getNextContent($content_url);
			$getOneChapterContent = $this->chapter_model->getOneChapterContent($chapter_url);
           	$data['getOneChapterContent'] = json_encode($getOneChapterContent->result());
           	$data['getChapterPlaylist'] = $this->getChapterPlaylist($chapter_url);
			if(!empty($email)){
				$option=array(	'user.email'			=>$email,
								'course.course_url'		=>$course_url,
								'chapter.chapter_url'	=>$chapter_url,
								'user_chapter_content.status'	=>'1');			
				$data['getUserContent'] = $this->getUserContent($option);
			}
			
			//記錄點擊任務
			$access_chapter_content_log = array(
				'user_mail'=>(empty($email))?'':$this->session->userdata('email'),
				'user_id'=>(empty($email))?0:$this->session->userdata('user_id'),
				'course_id'=>$teachedCourse['course_id'],
				'chapter_id'=>$getOneChapterContent->row_array()['chapter_id'],
				'chapter_content_id'=>$getOneChapterContent->row_array()['chapter_content_id'],
				'chapter_content_type'=>$getOneChapterContent->row_array()['content_type'],
				'access_time'=>$this->theme_model->nowTime(),
				'is_enrolled'=>(empty($email))?'0':(($getCourseOrderDetail && $getCourseOrderDetail['order_detail_id'])?'1':'0')
			);
			$this->dashboard_model->logUserAccessObject($access_chapter_content_log);

			//麵包屑
			function url($v1,$v2){return $v1."/".$v2;}	
			$getLibraryUrl = $this->breadcrumb_model->getLibraryUrl($library)->row_array();
			$this->checkExist($getLibraryUrl);
			array_push($breadcrumb_name, $getLibraryUrl['library_name']);
			array_push($breadcrumb_temp_url, $getLibraryUrl['library_url']);
			$url = array_reduce($breadcrumb_temp_url,"url");
			array_push($breadcrumb_name, $url);
			array_push($breadcrumb,$breadcrumb_name);
			
			$breadcrumb_name = array();
			
			$getCourseUrl = $this->breadcrumb_model->getCourseUrl($course_url)->row_array();
			$this->checkExist($getCourseUrl);
			array_push($breadcrumb_name, $getCourseUrl['course_name']);
			array_push($breadcrumb_temp_url, $getCourseUrl['course_url']);
			$url = array_reduce($breadcrumb_temp_url,"url");
			array_push($breadcrumb_name, $url);
			array_push($breadcrumb,$breadcrumb_name);
			
			$breadcrumb_name = array();
			
			$getChapterUrl = $this->breadcrumb_model->getChapterUrl($chapter_url)->row_array();
			$this->checkExist($getChapterUrl);
			array_push($breadcrumb_name, $getChapterUrl['chapter_name']);
			array_push($breadcrumb_temp_url, $getChapterUrl['chapter_url']);
			$url = array_reduce($breadcrumb_temp_url,"url");
			array_push($breadcrumb_name, $url);
			array_push($breadcrumb,$breadcrumb_name);
			
			$breadcrumb_name = array();
			
			$getContentUrl = $this->content_model->getOneContent($content_url)->row_array();
			$this->checkExist($getContentUrl);
			array_push($breadcrumb_name, $getContentUrl['chapter_content_desc']);
			array_push($breadcrumb_temp_url, $getContentUrl['chapter_content_url']);
			$url = array_reduce($breadcrumb_temp_url,"url");
			array_push($breadcrumb_name, $url);
			array_push($breadcrumb,$breadcrumb_name);
			
			$data['breadcrumb'] = $breadcrumb;
			//麵包屑
			//同一個chapter的下一個Url
			$data['next_content_url'] = $this->content_model->nextContentUrl($content_url);
			$data['last_content_url'] = $this->content_model->lastContentUrl($content_url);

			//list resource
			$data['listResource'] = json_encode($this->chapter_model->listResource($chapter_url)->result());
			
			if($content_type=='0'){
				//影片
				$data['title']= '學習';
				$this->theme_model->loadTheme('chapterContent/oneChapter',$data);
			}elseif($content_type=='1'){
				//測驗
				$data['title']= '測驗';
				$data['getQuestion'] = $this->library_model->getQuestion($content_url,$quizNo[0]);
				$data['current_question_id'] = $quizNo[0];
				$data['quizNo'] = $quizNo;
				$data['chapter_content_url'] = $content_url;
				$this->theme_model->loadTheme('quiz/listQuiz',$data);
			}elseif($content_type=='2'){
				//測驗(填充)
				$data['title']= '填充';
				$data['getQuestion'] = $this->library_model->getQuestion($content_url,$quizNo[0]);
				$data['current_question_id'] = $quizNo[0];
				$data['quizNo'] = $quizNo;
				$data['chapter_content_url'] = $content_url;
				$this->theme_model->loadTheme('quiz/listChallenge',$data);
			}elseif($content_type=='3'){
				//文章(HTML)
				$data['title']= '文章';
				$this->theme_model->loadTheme('chapterContent/articleContent',$data);
			}elseif($content_type=='4'){
				//實作
				$course = json_decode($data['getOneTeacher']);
				$course_id = $course[0]->course_id;
				$chapter = json_decode($data['getOneContent']);
				$chapter_id = $chapter[0]->chapter_id;
				$chapter_content_id = $chapter[0]->chapter_content_id;
				// echo $course_id.'  '. $chapter_id.'  '. $chapter_content_id;exit;
				$data['title']= 'Lab';
				$data['jupyter'] = $this->jupyter_model->getData($course_id, $chapter_id, $chapter_content_id);
				$data['jupyter']['course_id']=$course_id;
				$data['jupyter']['chapter_id']=$chapter_id;
				$data['jupyter']['chapter_content_id']=$chapter_content_id;
				$this->theme_model->loadTheme('chapterContent/labContent',$data);
			}elseif($content_type=='5'){
				//編譯
				$data['title']= '編譯';
				$this->theme_model->loadTheme('chapterContent/compileContent',$data);
			}
		//end of if($content_url)
		}elseif($chapter_url){
			//檢查有無訂閱或購買課程，如果使用者有登入一定要check，如果沒有登入，可以試看，不在此限
			$getCourseOrderDetail = $this->library_model->getCourseOrderDetail($course_url)->row_array();
			if(!empty($getCourseOrderDetail['order_detail_id'])){
				//echo 'you have subscribed the course';
			}else if(!empty($email)){
				redirect('library/'.$library.'/'.$course_url);
			}
			$data['login_url'] = $this->facebookLogin('library/'.$library.'/'.$course_url.'/'.$chapter_url);
			$getOneChapterContent = $this->chapter_model->getOneChapterContent($chapter_url);
			$data['getOneChapterContent'] = json_encode($getOneChapterContent->result());
			$data['getOneTeacher']=json_encode($this->teacher_model->getOneTeacher($course_url)->result());
			$data['listResource'] = json_encode($this->chapter_model->listResource($chapter_url)->result());
			if(!empty($email)){
				$option=array(	'user.email'			=>$email,
								'course.course_url'		=>$course_url,
								'chapter.chapter_url'	=>$chapter_url,
								'user_chapter_content.status'	=>'1');			
				$data['getUserContent'] = $this->getUserContent($option);
				$data['profile'] = $this->user_model->getOneUser($email)->row_array();
			}

			$teachedCourse = $this->course_model->getOneCourse($course_url)->row_array();
			//記錄點擊關卡
			$access_chapter_log = array(
				'user_mail'=>(empty($email))?'':$this->session->userdata('email'),
				'user_id'=>(empty($email))?0:$this->session->userdata('user_id'),
				'course_id'=>$teachedCourse['course_id'],
				'chapter_id'=>$getOneChapterContent->row_array()['chapter_id'],
				'access_time'=>$this->theme_model->nowTime(),
				'is_enrolled'=>(empty($email))?'0':(($getCourseOrderDetail && $getCourseOrderDetail['order_detail_id'])?'1':'0')
			);
			$this->dashboard_model->logUserAccessObject($access_chapter_log);

			//麵包屑
			function url($v1,$v2){return $v1."/".$v2;}
			$getLibraryUrl = $this->breadcrumb_model->getLibraryUrl($library)->row_array();
			$this->checkExist($getLibraryUrl);
			array_push($breadcrumb_name, $getLibraryUrl['library_name']);
			array_push($breadcrumb_temp_url, $getLibraryUrl['library_url']);
			$url = array_reduce($breadcrumb_temp_url,"url");
			array_push($breadcrumb_name, $url);
			array_push($breadcrumb,$breadcrumb_name);
			
			$breadcrumb_name = array();
			
			$getCourseUrl = $this->breadcrumb_model->getCourseUrl($course_url)->row_array();
			$this->checkExist($getCourseUrl);
			array_push($breadcrumb_name, $getCourseUrl['course_name']);
			array_push($breadcrumb_temp_url, $getCourseUrl['course_url']);
			$url = array_reduce($breadcrumb_temp_url,"url");
			array_push($breadcrumb_name, $url);
			array_push($breadcrumb,$breadcrumb_name);
			
			$breadcrumb_name = array();
			
			$getChapterUrl = $this->breadcrumb_model->getChapterUrl($chapter_url)->row_array();
			$this->checkExist($getChapterUrl);
			array_push($breadcrumb_name, $getChapterUrl['chapter_name']);
			array_push($breadcrumb_temp_url, $getChapterUrl['chapter_url']);
			$url = array_reduce($breadcrumb_temp_url,"url");
			array_push($breadcrumb_name, $url);
			array_push($breadcrumb,$breadcrumb_name);

			$data['breadcrumb'] = $breadcrumb;
			//麵包屑
			$this->theme_model->loadTheme('chapter/oneChapter',$data);
		//end of if($chapter_url)
		}elseif($course_url){
			$teachedCourse = $this->course_model->getOneCourse($course_url)->row_array();
			$data['announcements'] = json_encode($this->course_model->getCourseAnnouncements($teachedCourse['course_id'])->result());
			$data['meetings'] = json_encode($this->course_model->getCourseMeetings($teachedCourse['course_id'])->result());
			$data['getOneChapter'] = $this->getOneChapter($course_url);
			$data['getHits'] = $this->course_model->getHits()->result();
			if(!empty($email)){			
				$option=array(	'user.email'			=>$email,
								'course.course_url'		=>$course_url,
								'user_chapter.status'	=>'1');			
				$data['getUserChapter'] = $this->getUserChapter($option); 
				$data['getCourseOrderDetail'] = $this->library_model->getCourseOrderDetail($course_url)->row_array();
			}

			//記錄點擊課程
			$access_course_log = array(
				'user_mail'=>(empty($email))?'':$this->session->userdata('email'),
				'user_id'=>(empty($email))?0:$this->session->userdata('user_id'),
				'course_id'=>$teachedCourse['course_id'],
				'access_time'=>$this->theme_model->nowTime(),
				'is_enrolled'=>(empty($email))?'0':(($data['getCourseOrderDetail'] && $data['getCourseOrderDetail']['order_detail_id'])?'1':'0')
			);
			$this->dashboard_model->logUserAccessObject($access_course_log);

			//使用者的學習進度狀況(給繼續學習按鈕用的)
				$getUserLearningStatus = $this->library_model->getUserLearningStatus($course_url)->result();
				$var=array();
				$chapter_name=array();
				foreach( $getUserLearningStatus as $val){
					array_push($var,$val->pass);
					array_push($chapter_name,$val->chapter_name);
				}
				$data['progress'] = $this->progress($var);
				$data['chapter_name'] = $chapter_name;
				$j=0;
				foreach ($getUserLearningStatus as $key => $getUserLearningStatusVal){
					if($key=='0'){
      					$defaultSegments = array('library',$getUserLearningStatusVal->library_url,$getUserLearningStatusVal->course_url,$getUserLearningStatusVal->chapter_url);
      				}
      				if($getUserLearningStatusVal->uc_status!='1' && $j=='0'){
      					$segments = array('library',$getUserLearningStatusVal->library_url,$getUserLearningStatusVal->course_url,$getUserLearningStatusVal->chapter_url);
      					$j=1;
      				}
      			}
      			if(!empty($segments)){
      				$data['segments'] = $segments;	
      			}elseif(!empty($defaultSegments)){
      				$data['segments'] = $defaultSegments;
      			}
			$data['getOneTeacher']=json_encode($this->teacher_model->getOneTeacher($course_url)->result());
			$data['getEachPreferentialPrice'] = $this->library_model->getEachPreferentialPrice($course_url)->row_array();
			//麵包屑
			function url($v1,$v2){return $v1."/".$v2;}
			$getLibraryUrl = $this->breadcrumb_model->getLibraryUrl($library)->row_array();
			$this->checkExist($getLibraryUrl);
			array_push($breadcrumb_name, $getLibraryUrl['library_name']);
			array_push($breadcrumb_temp_url, $getLibraryUrl['library_url']);
			$url = array_reduce($breadcrumb_temp_url,"url");
			array_push($breadcrumb_name, $url);
			array_push($breadcrumb,$breadcrumb_name);
			
			$breadcrumb_name = array();
			
			$getCourseUrl = $this->breadcrumb_model->getCourseUrl($course_url)->row_array();
			$this->checkExist($getCourseUrl);
			array_push($breadcrumb_name, $getCourseUrl['course_name']);
			array_push($breadcrumb_temp_url, $getCourseUrl['course_url']);
			$url = array_reduce($breadcrumb_temp_url,"url");
			array_push($breadcrumb_name, $url);
			array_push($breadcrumb,$breadcrumb_name);
			
			$data['breadcrumb'] = $breadcrumb;
			//麵包屑
			$data['course_url'] = $course_url;
			$this->theme_model->loadTheme('course/oneCourse',$data);
		//end of if($course_url)
		}else{
			$data['getAllJobsCategory']=json_encode($this->library_model->getAllJobsCategory()->result());
			$data['getAll']=$this->getAll();
			//專案類課程
			//$data['oneLibraryByProject'] = $this->byProject($library);
			//知識類課程
			$data['oneLibraryByKnowledge'] = $this->byKnowledge($library);
			//課程小分類
			$data['getCourseCategory'] = json_encode($this->library_model->getCourseCategory($library)->result());
			//取得優惠價格以及訂閱狀況
			$data['getPreferentialPrice'] = json_encode($this->library_model->getPreferentialPrice($library)->result());
			//if(json_decode($data['oneLibraryByKnowledge'], true)){
				//取得使用者課程進度資料
				//$data=$this->dashboard->getUserCourseProgress($data,'library');
				//取得使用者課程進度資料
				if($email){
					$course_id = array();
					$coursePassAmount = array();
					$getPassAmountBySearchCourse=$this->library_model->getPassAmountBySearchCourse($email);
					foreach ($getPassAmountBySearchCourse->result_array() as $row){
						array_push($course_id,$row['course_id']);
						array_push($coursePassAmount,$row['passAmount']);
					}
					$data['course_id']= $course_id;
					$data['coursePassAmount']= $coursePassAmount;
				}
				$this->theme_model->loadTheme('library/oneLibrary',$data);
			// }else{
				// //提示訊息
				// $this->theme_model->flash_session("alert-danger","目前沒有此領域的課程");
				// redirect('library');
			// }
		}
	}
	
	function getAll()
	{
	 //load sidebar 所需的資料
		$getAll = json_encode($this->library_model->getAll()->result());
		return $getAll;
	}
	//應該是暫時不用了
	function byProject($library)
	{
		//單一課程庫底下的專案技巧課程資料
		$oneLibraryByProject = json_encode($this->library_model->getOneLibrary($library,'1')->result());
		return $oneLibraryByProject;
	}
	
	function byKnowledge($library)
	{
		//單一課程庫底下的知識秘笈課程資料
		$oneLibraryByKnowledge = json_encode($this->library_model->getOneLibrary($library,'2')->result());
		return $oneLibraryByKnowledge;
	}
	 
	function getOneChapter($chapter_url)
	{
		$getOneChapter = json_encode($this->course_model->getOneChapter($chapter_url)->result());
		return $getOneChapter;
	}
    
    function getOneChapterContent($chapter_url)
	{
		$getOneChapterContent = json_encode($this->chapter_model->getOneChapterContent($chapter_url)->result());
		return $getOneChapterContent;
	}
    
    function getOneContent($content_url)
	{
		$getOneContent = json_encode($this->content_model->getOneContent($content_url)->result());
		return $getOneContent;
	}

    function getNextContent($content_url)
	{
		$getNextContent = json_encode($this->content_model->getNextContent($content_url)->result());
		return $getNextContent;
	}
	
	function checkExist($url)
	{
		if(is_array($url) && $url){
			//使用者try網址，錯誤導到library並給提示訊息
		}else{
			//提示訊息
			$this->theme_model->flash_session("alert-danger","目前沒有此領域的課程");	
			redirect('library');	
		}
	}
	function checkAnswer()
	{
		$question_id=$this->input->post("question_id");
		$answer=$this->input->post("answer");
		echo $this->library_model->checkAnswer($question_id,$answer);
	}
	function getQuestion()
	{
		$chapter_content_url=$this->input->post("chapter_content_url");
		$question_id=$this->input->post("question_id");
		echo  json_encode($this->library_model->getQuestion($chapter_content_url,$question_id)->result());
		
	}     
	
	function getUserChapter($option){
		$getUserChapter=json_encode($this->user_chapter_model->getUserChapter($option)->result());
		return $getUserChapter;
	}
	
	function getUserContent($option){
		$getUserContent=json_encode($this->user_content_model->getUserContent($option)->result());
		return $getUserContent;
	}
	
	function getChapterPlaylist($chapter_url){
		$getChapterPlaylist=json_encode($this->content_model->getChapterPlaylist($chapter_url)->result());
		return $getChapterPlaylist;
	}
	//ajax 傳送題目以及選項的陣列過來
	function checkAnswers(){
		$my_array=$this->input->post("my_array");
		//統計答對題數
		$right = 0;
		foreach($my_array as $key=>$val){
			if(($this->library_model->checkAnswer($my_array[$key]['question_id'],$my_array[$key]['answer']))=='true'){
				$right++;
			}
		}
		echo $right;
	}
	
	//js ajax 送過來，設定url的session
	function setUrlSession(){
		$src=$this->input->post("src");
		$newdata = array('src'  => $src);
		$this->session->set_userdata($newdata);
	}
	//這邊echo的資料(主要是affectedRows)不要刪掉，我有拿來做判斷是否完成測驗的依據----change
	function updateUserChapterStatus(){
		$email				=$this->session->userdata('email');
		$chapter_content_url=$this->input->post("chapter_content_url");
		echo 'email					='.$email				.'<br />';
		echo 'chapter_content_url	='.$chapter_content_url	.'<br />';
		if(!empty($email) && !empty($chapter_content_url)){
			$userContentProgress=$this->user_content_model->getUserContentProgress($email,$chapter_content_url);
			$chapterCompleteGoal=$this->content_model->getChapterCompleteGoal($chapter_content_url);
			echo '$userContentProgress = '.$userContentProgress.'<br />';
			echo '$chapterCompleteGoal = '.$chapterCompleteGoal.'<br />';
			if($userContentProgress==$chapterCompleteGoal){
				$isComplete=$this->user_chapter_model->isUserChapterComplete($email,$chapter_content_url);
				echo 'isComplete = '.$isComplete.'<br />';
				if(!$isComplete){
					$affectedRows=$this->user_chapter_model->updateUserChapterStatus($email,$chapter_content_url);
					$getCourseChapterID = $this->points_model->getCourseChapterID($chapter_content_url);
					$this->points_model->insertPoint($this->session->userdata('user_id'),'completed_chapter',$getCourseChapterID->course_id,$getCourseChapterID->chapter_id,'','');
					echo 'affectedRows = '.$affectedRows.'<br />';
					if($affectedRows){
						return true;
					}else{
						return false;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	function updateUserCourseStatus(){
		$email				=$this->session->userdata('email');
		$chapter_content_url=$this->input->post("chapter_content_url");
		echo 'email					='.$email				.'<br />';
		echo 'chapter_content_url	='.$chapter_content_url	.'<br />';
		if(!empty($email) && !empty($chapter_content_url)){
			$userChapterProgress=$this->user_chapter_model->getUserChapterProgress($email,$chapter_content_url);
			$courseCompleteGoal=$this->chapter_model->getCourseCompleteGoal($chapter_content_url);
			echo '$userChapterProgress = '.$userChapterProgress.'<br />';
			echo '$courseCompleteGoal = '.$courseCompleteGoal.'<br />';
			if($userChapterProgress==$courseCompleteGoal){
				$isComplete=$this->user_course_model->isUserCourseComplete($email,$chapter_content_url);
				echo 'isComplete = '.$isComplete.'<br />';
				if(!$isComplete){
					$affectedRows=$this->user_course_model->updateUserCourseStatus($email,$chapter_content_url);
					$getCourseChapterID = $this->points_model->getCourseChapterID($chapter_content_url);
					$this->points_model->insertPoint($this->session->userdata('user_id'),'completed_course',$getCourseChapterID->course_id,'','','');
					echo 'affectedRows = '.$affectedRows.'<br />';
					if($affectedRows){
						return true;
					}else{
						return false;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	function set_flashdata_isQuizDone(){
		$currentMovie=$this->input->post("next_content_order");
		$isQuizDone=$this->input->post("isQuizDone");
		echo 'currentMovie='.$currentMovie;
		echo 'isQuizDone='.$isQuizDone;
		$this->session->set_flashdata('isQuizDone',$isQuizDone);
		$this->session->set_flashdata('currentMovie',$currentMovie);
	}
	function set_flashdata_recordPosition(){
		$currentMovie=$this->input->post("current_content_order");
		$currentChapter=$this->input->post("currentChapter");
		$isQuizDone=$this->input->post("isQuizDone");
		echo 'currentMovie='.$currentMovie.'<br />';
		echo 'currentChapter='.$currentChapter.'<br />';
		echo 'isQuizDone='.$isQuizDone;
		$this->session->set_flashdata('currentMovie',$currentMovie);
		$this->session->set_flashdata('currentChapter',$currentChapter);
		$this->session->set_flashdata('isQuizDone',$isQuizDone);
	}
	//更新教學資源的下載次數
	function updateDownloads(){
		$dl_resource_id = $this->security->xss_clean($this->input->post('dl_resource_id'));
		$this->chapter_model->updateDownloads($dl_resource_id);
	}
	//取得下載資源的網址
	function getDownloadsUrl(){
		$dl_resource_id = $this->security->xss_clean($this->input->post('dl_resource_id'));
		$row = $this->chapter_model->getDownloadsUrl($dl_resource_id);
		echo $row['library_url'].'/'.$row['dl_resource_url'];
	}

	function facebookLogin($url){
		//以下facebook
        $user = $this->facebook->getUser();
		////
		if(!$this->session->userdata('user_id')){
		if ($user) {
		  try {
		    // Proceed knowing you have a logged in user who's authenticated.
		    $user_profile = $this->facebook->api('/me');
		    //set_fb_session
		    $fb_data = array(  			
			'fb_login'  => '1',
			'fb_logout_url' => $this->facebook->getLogoutUrl(array('next' => base_url().'home/logout'))
			);
			$this->session->set_userdata($fb_data);
			
		  } catch (FacebookApiException $e) {
		    error_log($e);
		    $user = null;
		    //unset_fb_session
		    $this->session->unset_userdata('fb_login');
		    $this->session->unset_userdata('fb_logout_url');
		  }
		}
		if ($user):
            //$data['logout_url'] = $this->facebook->getLogoutUrl();
        	$check_email = $this->user_model->checkFbEmail($user_profile['email']);
			//get alias
			$alias = strstr($user_profile['email'], '@', true);
			$i = 10;
			while ($i > 0) {
				$i--;
				$checkalias = $this->user_model->checkAlias($alias);
				if($checkalias=='1'){
					$alias = $alias.rand(1,20);
					$checkalias = $this->user_model->checkAlias($alias);
					if($checkalias=='0'){
						break;
					}
				}
			}
			$now = $this->theme_model->nowTime();
			$row = $check_email->row();
			$count = $row->count;
			$count = $count+1;
			if($check_email->num_rows()==0)://資料庫無資料_新用戶 
				$fb_insert_data = array(	
					'name' => $user_profile['name'], 
					'email' => $user_profile['email'],
					'alias' => $alias,
					'last_login' => $now,
					'register_date' => $now,
					'fb_id' => $user_profile['id'],
					'active_flag'=>'1');
				$this->user_model->fb_insert_new($fb_insert_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				//設定session
				$this->theme_model->set_session($user_profile['email']);
				//提示訊息
				//$this->ym->flash_session("alert-success",'Facebook註冊成功，歡迎您');	
				//redirect('forum/forumContent/'.$forum_topic_id);
				redirect($url);
			elseif($row->email==$user_profile['email']&&$row->fb_id=='')://資料庫有mail，但尚未綁定fb帳號
				$fb_update_data=array(
					'fb_id'=>$user_profile['id'],
					'last_login'=> $now,
					'count' =>$count,
					'active_flag'=>'1');
				$this->user_model->fb_update($user_profile['email'],$fb_update_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				//設定session
				$this->theme_model->set_session($user_profile['email']);
				//提示訊息
				//$this->ym->flash_session("alert-success","成功綁定 email ");	
				redirect($url);
			elseif($row->email==$user_profile['email']&&$row->fb_id==$user_profile['id']):
				$fb_update_data=array(
					'last_login'=> $now,
					'count' =>$count);
				$this->user_model->fb_update($user_profile['email'],$fb_update_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				$this->theme_model->set_session($user_profile['email']);
				redirect($url);
			endif;
        else:
            return $this->facebook->getLoginUrl(array('scope' => 'email'));
        endif;
		}else{
			return $this->facebook->getLoginUrl(array('scope' => 'email'));
		}
	}
	//使用課程狀態參數取得使用者上課進度
	function progress($var){
		$pass = array();
		$key='0';
		for($i=0;$i<count($var);$i++){
			if($var['0']=='0'){
				if($i=='0' && $var[$i]=='0'){
					array_push($pass,'in-progress');
				}elseif($var['0']=='0' && $var[$i]=='0'){
					array_push($pass,'keep-go');
				}elseif($var['0']=='0' && $var[$i]=='1'){
					array_push($pass,'progress-complete-unordered');
				}
			}elseif($var['0']=='1'){
				if($i=='0' && $var[$i]=='1'){
					array_push($pass,'progress-complete');
				}elseif($key=='0' && $var['0']=='1' && $var[$i]=='0' && $var[$i-1]=='1'){
					array_push($pass,'in-progress');
					$key='1';
				}elseif($key=='1' && $var['0']=='1' && $var[$i]=='0'){
					array_push($pass,'keep-go');
				}elseif($key=='1' && $var[$i]=='1'){
					array_push($pass,'progress-complete-unordered');
				}elseif($var[$i]=='1' && $var[$i-1]=='1'){
					array_push($pass,'progress-complete');
				}
			}
		}
		return $pass;
	} 
}

/* End of file field.php */
/* Location: ./application/controllers/field.php */