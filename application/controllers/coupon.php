<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Coupon extends CI_Controller {
	function __construct(){
		parent::__construct();
        $this->load->model('coupon_model');
    }
	
    function index(){
       	$data['title']="Coupon";
		$this->user_model->isCoupon();
		$data['coupon_qry'] = $this->coupon_model->listCoupon();
        $this->theme_model->loadTheme('coupon/listCoupon',$data);
	}
	//新增coupon
	function addCoupon(){
		$this->user_model->isCoupon();
        $gen_number =$this->security->xss_clean($this->input->post('gen_number'));
        $start_time =$this->security->xss_clean($this->input->post('start_time'));
        $end_time   =$this->security->xss_clean($this->input->post('end_time'));
        $comment    =$this->security->xss_clean($this->input->post('comment'));
		$price    =$this->security->xss_clean($this->input->post('price'));
		//如果沒有輸入時間，開始時間是今日，結束時間是七天後
		if(!$start_time){
			$start_time = $this->theme_model->nowTime();
			$end_time = date("Y-m-d",time() + (7*24*60*60));
		}
		//數量忘記輸入，或是格式不正確，或是時間<0，則設定數量為1
		if($gen_number<=0 || !is_numeric($gen_number)){
			$gen_number = '1';
		}
        $random_arr=$this->coupon_model->couponGenerator($gen_number);
		foreach($random_arr as $key=>$value){
			$coupon = array(
				'create_time' => $this->theme_model->nowTime(),
				'start_time'=>$start_time,
				'end_time'=>$end_time,
				'comment'=>$comment,
				'price'=>$price,
				'coupon_code'=>$value);
			$this->coupon_model->addCoupon($coupon);
		}
		redirect('coupon');
	}
	//更新coupon
	function updateCoupon(){
		$this->user_model->isCoupon();
		$coupon_id  =$this->security->xss_clean($this->input->post('coupon_id'));
		$start_time =$this->security->xss_clean($this->input->post('start_time'));
        $end_time   =$this->security->xss_clean($this->input->post('end_time'));
        $comment    =$this->security->xss_clean($this->input->post('comment'));
		$price    	=$this->security->xss_clean($this->input->post('price'));
		$used_flag	=$this->security->xss_clean($this->input->post('used_flag'));
		$data=array(
			'start_time'	=>$start_time,
			'end_time'		=>$end_time,
			'comment'		=>$comment,
			'price'			=>$price,
			'used_flag'		=>$used_flag);
		$this->coupon_model->updateCoupon($coupon_id,$data);
		redirect('coupon');
	}
	//刪除coupon
	function delCoupon(){
		$this->user_model->isCoupon();
        $coupon_id  =$this->security->xss_clean($this->input->post('coupon_id'));
		$this->coupon_model->delCoupon($coupon_id);
	}	
	//驗證coupon
	function checkCoupon(){
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$coupon_code  = htmlspecialchars($this->input->post('coupon_code'), ENT_COMPAT);
		$checkCoupon = $this->coupon_model->checkCoupon($coupon_code)->row_array();
		if(empty($checkCoupon)){
			echo 'none';//找不到
		}else{
			if($checkCoupon['used_flag']=='1'){
				echo 'used';//使用過
			}elseif($checkCoupon['used_flag']=='0'){
				$time = $this->theme_model->nowTime();
				if($time >=$checkCoupon['start_time'] && $time <= $checkCoupon['end_time']){
					echo 'true';
				}else{
					echo 'expired';
				}
			}
		}
	}
	//取得coupon的價格
	function getPrice(){
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$coupon_code  = htmlspecialchars($this->input->post('coupon_code'), ENT_COMPAT);
		$checkCoupon = $this->coupon_model->checkCoupon($coupon_code)->row_array();
		echo $checkCoupon['price'];
	}
	
	//更新已經使用過的coupon 更新時間 email 還有flag
	function usedCoupon(){
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$coupon_code  = htmlspecialchars($this->input->post('coupon_code'), ENT_COMPAT);
		$course_id  =$this->input->post('course_id',true);
		$data=array(
			'used_time'		=>$this->theme_model->nowTime(),
			'user_email'	=>$this->session->userdata('email'),
			'course_id'		=>$course_id,
			'used_flag'		=>'1');
		$this->coupon_model->updateCouponCode($coupon_code,$data);
	}
}

/* End of file coupon.php */
/* Location: ./application/controllers/coupon.php */