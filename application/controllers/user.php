<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct()
    {
        parent::__construct();
		$this->load->model('user_model');
		$this->load->model('content_model');
		$this->load->model('mail_model');
		$this->load->model('points_model');
		$this->load->model('library_model');
		$this->load->model('tracks_model');
		$this->load->model('user_portfolio_model');
        $this->load->model('course_model');
    }


    function index()
    {
        redirect('library');
    }
    function checkCoupon()
    {
        $coupon=$this->security->xss_clean($this->input->post("coupon"));
        echo $this->user_model->checkCoupon($coupon);
    }
    function checkEmail()
    {
        $email=$this->security->xss_clean($this->input->post("email"));
        echo $this->user_model->checkEmail($email);
    }
    function checkAlias()
    {
        $alias=$this->security->xss_clean($this->input->post("alias"));
        echo $this->user_model->checkAlias($alias);
    }
    //編輯使用者頁面：檢查別名是否使用
    function checkEmailAlias()
    {
        $email = $this->session->userdata('email');
        $alias=$this->security->xss_clean($this->input->post("alias"));
        echo $this->user_model->checkEmailAlias($alias,$email);
    }
    //編輯使用者頁面：驗證未通過時，驗證使用者輸入新的mail
    function checkContentEmail()
    {
        $email = $this->session->userdata('email');//old mail-尚未認證
        $newMail=$this->security->xss_clean($this->input->post("email"));//new mail
        echo $this->user_model->checkContentEmail($newMail,$email);
    }
    function addUser()
    {
        $inputName=$this->security->xss_clean($this->input->post("inputName"));
        $inputEmail=$this->security->xss_clean($this->input->post("inputEmail"));
        $checkmail = $this->user_model->checkEmail($inputEmail);
        $activeCode = $this->theme_model->hash_password($this->theme_model->nowTime());
        if($checkmail=='1'){
            $this->theme_model->flash_session("alert-warning","很抱歉，該email已被註冊使用");
            redirect('/home/registration');
        }
        $alias = strstr($inputEmail, '@', true);
        $i = 10;
        while ($i > 0) {
            $i--;
            $checkalias = $this->user_model->checkAlias($alias);
            if($checkalias=='1'){
                $alias = $alias.rand(1,20);
                $checkalias = $this->user_model->checkAlias($alias);
                if($checkalias=='0'){
                    break;
                }
            }
        }
        $inputPassword=$this->security->xss_clean($this->input->post("inputPassword"));
        $password_hashed = $this->theme_model->hash_password($inputPassword);
        //準備資料  user 以及修改coupon 的used_flag，並增加使用該coupon的人的email
        //user
        $data = array( 'name' => $inputName,
                       'email' => $inputEmail,
                       'alias' => $alias,
                       'password' => $password_hashed,
                       'register_date' => $this->theme_model->nowTime(),
                       'last_login' => $this->theme_model->nowTime(),
                       'active_code' => $activeCode,
                       'active_flag' =>'1');
        //coupon
           $data2 = array( 'used_flag' => '1','user_email' => $inputEmail);
        //$agreement=$this->security->xss_clean($this->input->post("agreement"));
        if(!$inputName || !$inputEmail || !$password_hashed ){
            $this->theme_model->flash_session("alert-warning","請確實輸入註冊所需資料");
            redirect('/home/registration');
        }
        //沒有勾選個人資料(前端有過濾了，除非他下console，否則應該不會進來)
		/*
		if($agreement!='1'){
            $this->theme_model->flash_session("alert-warning","請同意本網站之《個人資料蒐集規範》");
            redirect('/home/registration');
		}
		*/
        //沒有選擇付款方式_paid表示優惠卷或信用卡那個欄位
//註解到2014年八月底
        //$paid=$this->security->xss_clean($this->input->post("paid"));
        //if(!$paid){
            //$this->theme_model->flash_session("alert-warning","請使用優惠條碼或信用卡號註冊");
            //redirect('/home');
        //}
        // $coupon=$this->security->xss_clean($this->input->post("coupon"));
        // $credit=$this->security->xss_clean($this->input->post("credit-card"));
        // if($coupon){
            // $row = $this->user_model->getOneCoupon($coupon)->row_array();
            // if(!$row){
                // $this->theme_model->flash_session("alert-warning","請輸入有效的優惠條碼");
                // redirect('/home/registration');
            // }elseif($this->theme_model->nowTime() < $row['start_time']){
                // //帳號啟用時間未到，一樣可以註冊，只是當下無法使用。
                // $this->theme_model->flash_session("alert-warning","您已完成註冊，帳號啟用時間尚未到");
                // $this -> user_model -> addUser($data);
                   // $this -> user_model -> updateCoupon($coupon,$data2);
                // redirect('/home/login');
            // }
        // }elseif($credit){
            // $this->theme_model->flash_session("alert-warning","很抱歉，暫時無提供信用卡付款");
            // redirect('/home/registration');
        // }else{
            // $this->theme_model->flash_session("alert-warning","很抱歉，您輸入的繳款資訊或優惠條碼有誤");
            // redirect('/home/registration');
        // }
           $this -> user_model -> addUser($data);
//註解到2014年八月底
           //$this -> user_model -> updateCoupon($coupon,$data2);
           $this -> theme_model-> set_session($inputEmail);
        //寄認證信件給使用者
        $activeData = array('email' => $inputEmail,'active_code' => $activeCode);
        //$this->mail_model->sendActiveMail($activeData);
           //提示訊息
           $this -> theme_model-> flash_session("alert-success","帳號註冊成功");
           redirect('/home');
           //redirect('page/message/sendActiveMail');
    }
    function updateAccountStatus()
    {
        $data['title']='更新有效時間';
        $data['email'] = $this->session->userdata('temp_email');
        $this->session->unset_userdata('temp_email');
        $inputEmail=$this->security->xss_clean($this->input->post("email"));
        $data2 = array( 'used_flag' => '1','user_email' => $inputEmail);
        if($inputEmail){
            //沒有選擇付款方式_paid表示優惠卷或信用卡那個欄位
            $paid=$this->security->xss_clean($this->input->post("paid"));
            if(!$paid){
                $this->theme_model->flash_session("alert-warning","請使用優惠條碼或信用卡號註冊");
                redirect('/home');
            }
            $coupon=$this->security->xss_clean($this->input->post("coupon"));
            $credit=$this->security->xss_clean($this->input->post("credit-card"));
            if($coupon){
                $row = $this->user_model->getOneCoupon($coupon)->row_array();
                if(!$row){
                    $this->theme_model->flash_session("alert-warning","請輸入有效的優惠條碼");
                    redirect('/home/registration');
                }elseif($this->theme_model->nowTime() < $row['start_time']){
                    //帳號啟用時間未到，一樣可以註冊，只是當下無法使用。
                    $this->theme_model->flash_session("alert-warning","帳號啟用時間尚未到");
                       $this -> user_model -> updateCoupon($coupon,$data2);
                    redirect('/home/login');
                }
            }elseif($credit){
                $this->theme_model->flash_session("alert-warning","很抱歉，暫時無提供信用卡付款");
                redirect('/home/registration');
            }else{
                $this->theme_model->flash_session("alert-warning","很抱歉，您輸入的繳款資訊或優惠條碼有誤");
                redirect('/home/registration');
            }
            $this -> user_model -> updateCoupon($coupon,$data2);
               $this -> theme_model-> set_session($inputEmail);
               //提示訊息
               $this -> theme_model-> flash_session("alert-success","帳號更新成功");
               redirect('/home');
        }
        $this->theme_model->loadTheme('home/updateAccountStatus',$data);
    }
    function profile($alias)
    {
        $data['title']='帳號管理';
        //檢查使用者是否登入
        $this->user_model->isLogin();
        //自己的profile
        //個人總積分
        $user_id = $this->session->userdata('user_id');
        $getTotalPoints = $this->points_model->getTotalPoints($user_id);
        $data['getTotalPoints'] = $getTotalPoints->total;
        $data['getPoints'] = json_encode($this->points_model->getPoints($user_id)->result());
        if($alias==$this->session->userdata('alias')){
            $getOneUser = $this->user_model->getOneUser($this->session->userdata('email'));
            $getUserChapterBadge = $this->user_model->getUserChapterBadge($this->session->userdata('email'));
            $data['profile'] = $getOneUser->row_array();
            $data['getUserChapterBadge'] = json_encode($getUserChapterBadge->result());
            $data['getOneUser'] = $this->user_model->getOneUser($this->session->userdata('email'))->row_array();
            //$data['getUserExperience'] = $this->user_model->getUserExperience($this->session->userdata('email'))->row_array();
            //$data['getUserAward'] = $this->user_model->getUserAward($this->session->userdata('email'))->row_array();
            //$data['getUserExperience'] = $this->user_model->getUserExperience($this->session->userdata('email'))->row_array();
            $this->theme_model->loadTheme('profile/profile',$data);
        }else{
        //別人的profile
        //暫時不開發，可以給除了自己以外的使用者看
        redirect('library');
        }
    }
    function edit()
    {
        $this->user_model->isLogin();
        $data['title']='編輯個人資料';
        $data['error'] = '';
        $data['getOneUser'] = $this->user_model->getOneUser($this->session->userdata('email'))->row_array();
        // $data['getUserAward'] = $this->user_model->getUserAward($this->session->userdata('email'))->row_array();
        // $data['getUserExperience'] = $this->user_model->getUserExperience($this->session->userdata('email'))->row_array();
        // $data['getUserLicense'] = $this->user_model->getUserLicense($this->session->userdata('email'))->row_array();

        $this->theme_model->loadTheme('profile/edit',$data);
    }
    function update()
    {
        $this->user_model->isLogin();
        $getEmailActive = $this->user_model->getEmailActive($this->session->userdata('email'))->row();
        if(($getEmailActive->active_flag)=='0'){
            //email 尚未通過認證才可以修改 也就是等於0的時候
            $email = $this->security->xss_clean($this->input->post("contentEmail"));
            if($email){
                $emailData = array('email' => $email);
                $this->user_model->updateUser($this->session->userdata('email'),$emailData);
                $this ->theme_model-> set_session($email);
            }
        }
        $profile_data = array(
                    'name' => $this->security->xss_clean($this->input->post("name")),
                    'gender' => $this->security->xss_clean($this->input->post("gender")),
                    'mobile' => $this->security->xss_clean($this->input->post("mobile")),
                    'birthday' => $this->security->xss_clean($this->input->post("birthday")),
                    'experience' => preg_replace('/[\n\r\t]/','',nl2br($this->security->xss_clean($this->input->post("experience")))),
                    'education_highest_level' => $this->security->xss_clean($this->input->post("education_highest_level")),
        );
        $this->user_model->updateUser($this->session->userdata('email'),$profile_data);
        $ext=pathinfo($_FILES['userfile']['name'],PATHINFO_EXTENSION);
        $config['upload_path'] = './assets/img/user/origin';
        $config['allowed_types'] = 'png|jpg|JPG|jpeg|JPEG';
        $config['max_size']    = '2000';
        //$config['max_width']  = '1024';
        //$config['max_height']  = '768';
        $config['file_name'] = md5($this->session->userdata('email')).'.'.$ext;
        $config['overwrite'] = 'TRUE';
        $this->load->library('upload',$config);
        //是否提供求才
        $recruiters = $this->security->xss_clean($this->input->post("recruiters"));
        if($recruiters){
            $recruiters='1';
        }else{
            $recruiters='0';
        }
        //這邊字串還要過濾，有空在寫了 囧
        $alias = htmlspecialchars($this->input->post("alias"));
        if ( ! $this->upload->do_upload())
        {
            //無上傳相片時
            $file_size = $this->upload->data();
            if(! $file_size['file_size']){
                //alias在個人資料與履歷編輯的頁面要使用到，若使用者打中文會有error
                //所以鎖起來不讓使用者修改
                $data = array(
//                'name' => $this->security->xss_clean($this->input->post("name"))
//                 		,
//                'intro' => $this->security->xss_clean($this->input->post("myintro")),
//                'address' => $this->security->xss_clean($this->input->post("address")),
//                'mobile' => $this->security->xss_clean($this->input->post("mobile")),
//                'education_highest_level' => $this->security->xss_clean($this->input->post("education_highest_level")),
//                'work_status' => $this->security->xss_clean($this->input->post("work_status")),
//                'work_skills' => $this->security->xss_clean($this->input->post("work_skills")),
//                'recruiters' => $recruiters
            );
//             $this->user_model->updateUser($this->session->userdata('email'),$data);
            $this -> theme_model-> set_session($this->session->userdata('email'));
            redirect('user/profile/'.$this->session->userdata('alias'));
            }else{
            //上傳有錯誤時
            	$data['title']='圖片上傳失敗';
            	$data['errorMessage']=$this->upload->display_errors();
            	$this->theme_model->loadTheme('profile/errorPage',$data);
            }
        }else{
            $file_size = $this->upload->data();
            //alias在個人資料與履歷編輯的頁面要使用到，若使用者打中文會有error
            //所以鎖起來不讓使用者修改
            $data = array(
               'img' => md5($this->session->userdata('email')).'.'.$ext
//             		,
//                'name' => $this->security->xss_clean($this->input->post("name"))
//             		,
               // 'alias' => $alias,
//                'intro' => $this->security->xss_clean($this->input->post("myintro")),
//                'address' => $this->security->xss_clean($this->input->post("address")),
//                'mobile' => $this->security->xss_clean($this->input->post("mobile")),
//                'education_highest_level' => $this->security->xss_clean($this->input->post("education_highest_level")),
//                'work_status' => $this->security->xss_clean($this->input->post("work_status")),
//                'work_skills' => $this->security->xss_clean($this->input->post("work_skills")),
//                'recruiters' => $recruiters
            );
        $this->user_model->updateUser($this->session->userdata('email'),$data);
        $this -> theme_model-> set_session($this->session->userdata('email'));
        $fullPath = base_url().'assets/img/user/origin/'.md5($this->session->userdata('email')).'.'.$ext;
        switch(strtolower($ext)){
            case "jpg":
                $src=imagecreatefromjpeg($fullPath);//讀取來源圖檔
                break;
            case "jpeg":
                $src=imagecreatefromjpeg($fullPath);//讀取來源圖檔
                break;
            case "gif":
                $src=imagecreatefromgif($fullPath);//讀取來源圖檔
                break;
            case "png":
                $src=imagecreatefrompng($fullPath);//讀取來源圖檔
                break;
        }
        $src_w = imagesx($src);     //取得來源圖檔長寬
        $src_h = imagesy($src);
        $new_w = 128;               //新圖檔長寬
        $new_h = 128;
        $thumb = imagecreatetruecolor($new_w, $new_h);    //建立空白縮圖
        //設定空白縮圖的背景，如不設定，背景預設為黑色
        $bg = imagecolorallocate($thumb,255,255,255);       //空白縮圖的背景顏色
        imagefilledrectangle($thumb,0,0,$src_w,$src_h,$bg); //將顏色填入縮圖
        //執行縮圖
        imagecopyresampled($thumb, $src, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);

        switch(strtolower($ext)){
            case "jpg":
                imagejpeg($thumb, './assets/img/user/128x128/'.md5($this->session->userdata('email')).'.'.$ext,90);
                break;
            case "jpeg":
                imagejpeg($thumb, './assets/img/user/128x128/'.md5($this->session->userdata('email')).'.'.$ext,90);
                break;
            case "gif":
                imagegif($thumb, './assets/img/user/128x128/'.md5($this->session->userdata('email')).'.'.$ext);
                break;
            case "png":
                imagepng($thumb, './assets/img/user/128x128/'.md5($this->session->userdata('email')).'.'.$ext,9);
                break;
        }

        //跟原始檔名一樣的話，可以把較大的舊檔覆蓋掉
        redirect('user/profile/'.$this->session->userdata('alias'));
        }
    }
    //當使用者觀看影片超過10秒或完成一題測驗時，該content點擊次數加一
    function updateUserHits()
    {
        $chapter_content_url=$this->input->post("chapter_content_url");
        $this->content_model->updateUserHits($chapter_content_url);
        if($this->session->userdata('email')){
            $this->content_model->updateMemberHits($chapter_content_url);
        }
    }
    //忘記密碼
    function forgetPassword()
    {
        $data['title']='忘記密碼';
        $action=$this->security->xss_clean($this->input->post("action"));
        if($action=='forgetPassword'){
            //檢查email是否有效
            $email = $this->security->xss_clean($this->input->post('inputEmail'));
            $checkmail = $this->user_model->checkEmail($email);
            //如果繞過前端js，這邊在驗證一次，如果查無mail，則導回忘記密碼那頁
            if($checkmail=='0'){
                $this->theme_model->flash_session("alert-warning","查無您的email，請重新輸入!");
                redirect('/user/forgetPassword');
            }
            $time = $this->theme_model->nowTime();
            $code = $this->theme_model->hash_password($time);
            $ip   = $this->theme_model->getIp();
            $data = array(
               'email' => $email,
               'code' => $code,
               'time' => $time,
               'ip' =>$ip,
               'forget_password_used_flag' => '0');
            //把忘記密碼的相關資訊新增到DB
            $this->user_model->addForgetPassword($data);
            //寄信
            //要寄的資訊有email以及驗證碼
            $this->mail_model->sendForgetMail($data);
            //寄出後導至message
            $this->session->set_flashdata('sendMail', $email);
            redirect('page/message/sendMail');
        }

        $this->theme_model->loadTheme('profile/forgetPassword',$data);
    }

    //點選email重設密碼
    function resetPassword(){
        $action=$this->security->xss_clean($this->input->post("action"));
        if($action=='resetPassword'){
            $code = $this->security->xss_clean($this->input->post('code'));
            $checkCode = $this->user_model->checkCode($code)->row_array();
            if($checkCode){
                $inputNewPassword = $this->security->xss_clean($this->input->post('inputNewPassword'));
                $password_hashed = $this->theme_model->hash_password($inputNewPassword);
                $data = array('password' => $password_hashed);
                //update 密碼
                $this -> user_model -> updateUser($checkCode['email'],$data);
                //update forget_password
                $usedFlag = array('forget_password_used_flag' => '1');
                $this -> user_model -> updateForgetPassword($code,$usedFlag);
                redirect('page/message/updatePassword');
            }
        }
        $code = $this->uri->segment(3);
        $checkCode = $this->user_model->checkCode($code)->row_array();
        if($checkCode){
            //驗證碼等資訊正確，導到改密碼頁面
            $data['title']='重設密碼';
            $data['code'] = $code;
            $this->theme_model->loadTheme('profile/resetPassword',$data);
        }else{
            //驗證碼錯誤或時間超過或已經改過密碼了
            redirect('page/message/errorCode');
        }
    }
    //由帳號啟動信點選啟動帳號
    function active(){
        $code = $this->uri->segment(3);
        //由code取得email
        $getEmail = $this->user_model->getEmail($code)->row_array();
        //啟動成功
        if($getEmail){
            if($getEmail['active_flag']=='0'){
                $activeData = array('active_flag'=>'1');
                $this->user_model->updateUser($getEmail['email'],$activeData);
                $this->theme_model->set_session($getEmail['email']);
                redirect('page/message/activeMailSuccess');
            }else{
                //已經啟動過了
                $this->theme_model->set_session($getEmail['email']);
                redirect('page/message/activedMail');
            }
        //啟動失敗
        }else{
            redirect('page/message/activeMailFailure');
        }

    }
    //重寄認證信件
    function resendActiveMail(){
    	$this->user_model->isLogin();
        $activeCode = $this->theme_model->hash_password($this->theme_model->nowTime());
        $email = $this->session->userdata('email');
        //因為重寄認證信，所以要更新active_code
        $codeData = array('active_code' => $activeCode);
        $this->user_model->updateUser($email,$codeData);
        $activeData = array('email' => $email,'active_code' => $activeCode);
        $this->mail_model->sendActiveMail($activeData);
        echo 'sendActiveMail';
    }
    //取得使用者email
    function getUserSession(){
        $email = $this->session->userdata('email');
        echo $email;
    }
    //使用者履歷
    function resume($alias)
    {
        $data['title']='個人履歷';
        //檢查使用者是否登入
        $this->user_model->isLogin();
        //自己的resume
        if($alias==$this->session->userdata('alias')){
            $getOneUser = $this->user_model->getOneUser($this->session->userdata('email'));
            $getUserTracks = $this->user_model->getUserTracks($this->session->userdata('email'));
            $getUserTracksCourse = $this->user_model->getUserTracksCourse($this->session->userdata('email'));
            $data['profile'] = $getOneUser->row_array();
            $data['getUserTracks'] = json_encode($getUserTracks->result());
            $data['getUserTracksCourse'] = json_encode($getUserTracksCourse->result());
            $data['getOneUser'] = $this->user_model->getOneUser($this->session->userdata('email'))->row_array();
            $this->theme_model->loadTheme('profile/resume',$data);
        }else{
        //別人的resume
        //暫時不開發，未來只能給廠商瀏覽
        }
    }
    //勞委會案子
    function webCamPic(){
        $data['title']='個人照片存放區';
        $this->user_model->isLogin();
        $data['getPicture']=json_encode($this->user_model->getPicture($this->session->userdata('email'))->result());
        $this->theme_model->loadTheme('profile/webCamPic',$data);
    }
    function getAll()
    {
     //load sidebar 所需的資料
        $getAll = json_encode($this->library_model->getAll()->result());
        return $getAll;
    }
    //履歷檢視頁
	function portfolio($alias){
		$data['title']='個人履歷';
		$this->user_model->isLogin();
		$getUserByAlias=$this->user_model->getUserByAlias($alias);
        
		if($getUserByAlias->num_rows()>0){
			$email=$getUserByAlias->row()->email;
			$user_id=$getUserByAlias->row()->user_id;
		}
		//查無此人的個人履歷
		if(!$this->user_model->checkAlias($alias)){
			$data['title']='查無此id的個人履歷';
			$data['errorMessage']='查無此id的個人履歷';
			$this->theme_model->loadTheme('portfolio/errorPage',$data);
		//使用者本人的個人履歷
		}elseif($email==$this->session->userdata('email')){
			$data['getOneUser'] = $this->user_model->getOneUser($email)->row_array();
			//個人總積分
			//--------------------------------------------
			$getTotalPoints = $this->points_model->getTotalPoints($user_id);
			$data['getTotalPoints'] = $getTotalPoints->total;
			$data['getPoints'] = json_encode($this->points_model->getPoints($user_id)->result());
			$getOneUser = $this->user_model->getOneUser($email);
			$getUserChapterBadge = $this->user_model->getUserChapterBadge($email);
			$data['profile'] = $getOneUser->row_array();
			$data['getUserChapterBadge'] = json_encode($getUserChapterBadge->result());
			//--------------------------------------------
			$data['getUserExperience'] = json_encode($this->user_portfolio_model->getUserExperience($email)->result());
			$data['getUserAward'] = json_encode($this->user_portfolio_model->getUserAward($email)->result());
			$data['getUserLicense'] = json_encode($this->user_portfolio_model->getUserLicense($email)->result());
			$data['getUserPortfolio'] = json_encode($this->user_portfolio_model->getUserPortfolio($email)->result());
			$data['getUserSkill'] = json_encode($this->user_portfolio_model->getUserSkill($email)->result());
			$data['getUserEducation'] = json_encode($this->user_portfolio_model->getUserEducation($email)->result());
			$data['getUserSociety'] = $this->user_portfolio_model->getUserSociety($email)->row_array();
			$data['getCurrentJob'] = $this->user_portfolio_model->getCurrentJob($email)->row_array();
			$data['getUserTraining'] = json_encode($this->user_portfolio_model->getUserTraining($email)->result());
			$data['getPortfolioSkill'] = json_encode($this->user_portfolio_model->getPortfolioSkill($email)->result());
			$data['getCareerSkill'] = json_encode($this->user_portfolio_model->getCareerSkill($email)->result());
			$data['getSeniority'] = json_encode($this->user_portfolio_model->getSeniority($email)->result());
			$data['alias']=$alias;
			$this->theme_model->loadTheme('portfolio/portfolio',$data);
		//此人的個人履歷未開放
		}elseif(!$this->user_model->isResumeOpen($email)){
			$data['title']='此id的個人履歷未開放';
			$data['errorMessage']='此id的個人履歷未開放';
			$this->theme_model->loadTheme('portfolio/errorPage',$data);
		//其他有開放的個人履歷
		}else{
			$data['getOneUser'] = $this->user_model->getOneUser($email)->row_array();
			//個人總積分
			//--------------------------------------------
			$getTotalPoints = $this->points_model->getTotalPoints($user_id);
			$data['getTotalPoints'] = $getTotalPoints->total;
			$data['getPoints'] = json_encode($this->points_model->getPoints($user_id)->result());
			$getOneUser = $this->user_model->getOneUser($email);
			$getUserChapterBadge = $this->user_model->getUserChapterBadge($email);
			$data['profile'] = $getOneUser->row_array();
			$data['getUserChapterBadge'] = json_encode($getUserChapterBadge->result());
			//--------------------------------------------
			$data['getUserExperience'] = json_encode($this->user_portfolio_model->getUserExperience($email)->result());
			$data['getUserAward'] = json_encode($this->user_portfolio_model->getUserAward($email)->result());
			$data['getUserLicense'] = json_encode($this->user_portfolio_model->getUserLicense($email)->result());
			$data['getUserPortfolio'] = json_encode($this->user_portfolio_model->getUserPortfolio($email)->result());
			$data['getUserSkill'] = json_encode($this->user_portfolio_model->getUserSkill($email)->result());
			$data['getUserEducation'] = json_encode($this->user_portfolio_model->getUserEducation($email)->result());
			$data['getUserSociety'] = $this->user_portfolio_model->getUserSociety($email)->row_array();
			$data['getCurrentJob'] = $this->user_portfolio_model->getCurrentJob($email)->row_array();
			$data['getUserTraining'] = json_encode($this->user_portfolio_model->getUserTraining($email)->result());
			$data['getPortfolioSkill'] = json_encode($this->user_portfolio_model->getPortfolioSkill($email)->result());
			$data['getCareerSkill'] = json_encode($this->user_portfolio_model->getCareerSkill($email)->result());
			$data['alias']=$alias;
			$this->theme_model->loadTheme('portfolio/portfolio',$data);
		}
	}
	//履歷編輯頁
	function editPortfolio()
	{
		$data['title']='編輯個人履歷';
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$data['getOneUser'] = $this->user_model->getOneUser($this->session->userdata('email'))->row_array();
		//個人總積分
		//--------------------------------------------
        $user_id = $this->session->userdata('user_id');
        $getTotalPoints = $this->points_model->getTotalPoints($user_id); 
        $data['getTotalPoints'] = $getTotalPoints->total;
		///
		// by JoeyC
		///
		$data['getCourseScores'] = json_encode($this->points_model->getCourseScores($user_id)->result());
        $data['getAVGCourseScores'] = json_encode($this->points_model->getAVGCourseScores($user_id)->result());
        $data['getChapterScores'] = json_encode($this->points_model->getChapterScores($user_id)->result());
        $data['getLibAnalysis'] = json_encode($this->points_model->getLibAnalysis($user_id)->result());
        ///
		// by JoeyC End
		///
        $data['getPoints'] = json_encode($this->points_model->getPoints($user_id)->result());
        $getOneUser = $this->user_model->getOneUser($this->session->userdata('email'));
        $getUserChapterBadge = $this->user_model->getUserChapterBadge($this->session->userdata('email'));
        $data['profile'] = $getOneUser->row_array();
        $data['getUserChapterBadge'] = json_encode($getUserChapterBadge->result());
		//--------------------------------------------
		$data['getUserExperience'] = json_encode($this->user_portfolio_model->getUserExperience($this->session->userdata('email'))->result());
		$data['getUserAward'] = json_encode($this->user_portfolio_model->getUserAward($this->session->userdata('email'))->result());
		$data['getUserLicense'] = json_encode($this->user_portfolio_model->getUserLicense($this->session->userdata('email'))->result());
		$data['getUserPortfolio'] = json_encode($this->user_portfolio_model->getUserPortfolio($this->session->userdata('email'))->result());
		$data['getUserSkill'] = json_encode($this->user_portfolio_model->getUserSkill($this->session->userdata('email'))->result());
		$data['getUserEducation'] = json_encode($this->user_portfolio_model->getUserEducation($this->session->userdata('email'))->result());
		$data['getUserSociety'] = $this->user_portfolio_model->getUserSociety($this->session->userdata('email'))->row_array();
		$data['getUserTraining'] = json_encode($this->user_portfolio_model->getUserTraining($this->session->userdata('email'))->result());
		$data['getPortfolioSkill'] = json_encode($this->user_portfolio_model->getPortfolioSkill($this->session->userdata('email'))->result());
		$data['getCareerSkill'] = json_encode($this->user_portfolio_model->getCareerSkill($this->session->userdata('email'))->result());
		$data['getJobsCategory'] = json_encode($this->tracks_model->getJobsCategory()->result());
		$data['getSeniority'] = json_encode($this->user_portfolio_model->getSeniority($this->session->userdata('email'))->result());
		$this->theme_model->loadTheme('portfolio/editPortfolio',$data);
	}
	//更新履歷資料
	function updatePortfolio()
	{
		//檢查使用者是否登入
		$this->user_model->isLogin();
		$ext=pathinfo($_FILES['userfile']['name'],PATHINFO_EXTENSION);
		$config['upload_path'] = './assets/img/user/origin';
		$config['allowed_types'] = 'png|jpg|JPG|jpeg|JPEG';
		$config['max_size']	= '2000';
		//$config['max_width']  = '1024';
		//$config['max_height']  = '768';
		$config['file_name'] = md5($this->session->userdata('alias')).'.'.$ext;
		$config['overwrite'] = 'TRUE';
		$this->load->library('upload',$config);
		//是否提供求才
		$recruiters = $this->security->xss_clean($this->input->post("recruiters"));
		if($recruiters){
			$recruiters='1';
		}else{
			$recruiters='0';
		}
		//Youtube影音履歷連結
		$video_resume = $this->security->xss_clean($this->input->post("video_resume"));
		if(!empty($video_resume)){
			if(strpos($video_resume, 'http://www.youtube.com/watch?v=')!==false){
				$video_resume		=str_replace('http://www.youtube.com/watch?v=', 'http://www.youtube.com/embed/', $video_resume);
			}elseif(strpos($video_resume, 'https://www.youtube.com/watch?v=')!==false){
				$video_resume		=str_replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/', $video_resume);
			}else{
				$video_resume = $this->security->xss_clean($this->input->post("video_resume"));
			}
		}else{
			$video_resume		='';
		}
		$jobs_category_id = $this->security->xss_clean($this->input->post("jobs_category_id"));
		$match_jobs_category='';
		if(!empty($jobs_category_id)){
		for($i=0;$i<count($jobs_category_id);$i++){
			if($i+1!=count($jobs_category_id)){
				$match_jobs_category.=($jobs_category_id[$i].',');
			}else{
				$match_jobs_category.=$jobs_category_id[$i];
			}
		}
		}
		//這邊字串還要過濾，有空在寫了 囧
		$alias = htmlspecialchars($this->input->post("alias"));
		if ( ! $this->upload->do_upload())
		{
			//無上傳相片時
			$file_size = $this->upload->data();
			if(! $file_size['file_size']){
				//alias在個人資料與履歷編輯的頁面要使用到，若使用者打中文會有error
				//所以鎖起來不讓使用者修改
				$data = array(
						'name' => $this->security->xss_clean($this->input->post("name")),
						'location' => $this->security->xss_clean($this->input->post("location")),
						'mobile' => $this->security->xss_clean($this->input->post("mobile")),
						'intro' => preg_replace('/[\n\r\t]/','',nl2br($this->security->xss_clean($this->input->post("myintro")))),
						'work_status' => $this->security->xss_clean($this->input->post("work_status")),
						'autobiography' => preg_replace('/[\n\r\t]/','',nl2br($this->security->xss_clean($this->input->post("autobiography")))),
						'contact_email' => $this->security->xss_clean($this->input->post("contact_email")),
						'video_resume' => $video_resume,
						'performance_open_flag' => $this->security->xss_clean($this->input->post("performance_open_flag")),
						'match_jobs_category'	=>$match_jobs_category
				);
				$this->user_model->updateUser($this->session->userdata('email'),$data);
				$this->modifyUserTraining();
				$this->modifyUserExperience();
				$this->modifyUserEducation();
				$this->modifyUserSociety();
				$this->modifyUserSkill();
				$this->modifyUserAward();
				$this->modifyUserLicense();
				$this->modifyUserPortfolio();
				$this->modifyPortfolioSkill();
				$this->modifyCareerSkill();
				$this->modifySeniority();
				$this -> theme_model-> set_session($this->session->userdata('email'));
				redirect('user/portfolio/'.$this->session->userdata('alias'));
			}else{
				//上傳有錯誤時
				$data['title']='編輯個人履歷';
				$data['error'] = $this->upload->display_errors();
				$data['getOneUser'] = $this->user_model->getOneUser($this->session->userdata('email'))->row_array();
				$data['getUserTraining'] = json_encode($this->user_portfolio_model->getUserTraining($this->session->userdata('email'))->result());
				$data['getUserExperience'] = json_encode($this->user_portfolio_model->getUserExperience($this->session->userdata('email'))->result());
				$data['getUserAward'] = json_encode($this->user_portfolio_model->getUserAward($this->session->userdata('email'))->result());
				$data['getUserLicense'] = json_encode($this->user_portfolio_model->getUserLicense($this->session->userdata('email'))->result());
				$data['getUserPortfolio'] = json_encode($this->user_portfolio_model->getUserPortfolio($this->session->userdata('email'))->result());
				$data['getUserSkill'] = json_encode($this->user_portfolio_model->getUserSkill($this->session->userdata('email'))->result());
				$data['getUserEducation'] = json_encode($this->user_portfolio_model->getUserEducation($this->session->userdata('email'))->result());
				$data['getUserSociety'] = $this->user_portfolio_model->getUserSociety($this->session->userdata('email'))->row_array();
				$data['getCareerSkill'] = json_encode($this->user_portfolio_model->getCareerSkill($this->session->userdata('email'))->result());
				$data['getJobsCategory'] = json_encode($this->tracks_model->getJobsCategory()->result());
				$data['getSeniority'] = json_encode($this->user_portfolio_model->getSeniority($this->session->userdata('email'))->result());
				$this->theme_model->loadTheme('portfolio/editPortfolio',$data);
			}
		}else{
			$file_size = $this->upload->data();
			//alias在個人資料與履歷編輯的頁面要使用到，若使用者打中文會有error
			//所以鎖起來不讓使用者修改
			$data = array(
						'resume_img' => md5($this->session->userdata('alias')).'.'.$ext,
						'name' => $this->security->xss_clean($this->input->post("name")),
						'location' => $this->security->xss_clean($this->input->post("location")),
						'mobile' => $this->security->xss_clean($this->input->post("mobile")),
						'intro' => preg_replace('/[\n\r\t]/','',nl2br($this->security->xss_clean($this->input->post("myintro")))),
						'work_status' => $this->security->xss_clean($this->input->post("work_status")),
						'autobiography' => preg_replace('/[\n\r\t]/','',nl2br($this->security->xss_clean($this->input->post("autobiography")))),
						'contact_email' => $this->security->xss_clean($this->input->post("contact_email")),
						'video_resume' => $video_resume,
						'performance_open_flag' => $this->security->xss_clean($this->input->post("performance_open_flag")),
						'match_jobs_category'	=>$match_jobs_category
			);
			$this->user_model->updateUser($this->session->userdata('email'),$data);
			$fullPath = base_url().'assets/img/user/origin/'.md5($this->session->userdata('alias')).'.'.$ext;
			switch(strtolower($ext)){
				case "jpg":
					$src=imagecreatefromjpeg($fullPath);//讀取來源圖檔
					break;
				case "jpeg":
					$src=imagecreatefromjpeg($fullPath);//讀取來源圖檔
					break;
				case "gif":
					$src=imagecreatefromgif($fullPath);//讀取來源圖檔
					break;
				case "png":
					$src=imagecreatefrompng($fullPath);//讀取來源圖檔
					break;
			}
			$src_w = imagesx($src);     //取得來源圖檔長寬
			$src_h = imagesy($src);
			$new_w = 128;               //新圖檔長寬
			$new_h = 128;
			$thumb = imagecreatetruecolor($new_w, $new_h);    //建立空白縮圖
			//設定空白縮圖的背景，如不設定，背景預設為黑色
			$bg = imagecolorallocate($thumb,255,255,255);       //空白縮圖的背景顏色
			imagefilledrectangle($thumb,0,0,$src_w,$src_h,$bg); //將顏色填入縮圖
			//執行縮圖
			imagecopyresampled($thumb, $src, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);
				
			switch(strtolower($ext)){
				case "jpg":
					imagejpeg($thumb, './assets/img/user/128x128/'.md5($this->session->userdata('alias')).'.'.$ext,90);
					break;
				case "jpeg":
					imagejpeg($thumb, './assets/img/user/128x128/'.md5($this->session->userdata('alias')).'.'.$ext,90);
					break;
				case "gif":
					imagegif($thumb, './assets/img/user/128x128/'.md5($this->session->userdata('alias')).'.'.$ext);
					break;
				case "png":
					imagepng($thumb, './assets/img/user/128x128/'.md5($this->session->userdata('alias')).'.'.$ext,9);
					break;
			}
			$this->modifyUserTraining();
			$this->modifyUserExperience();
			$this->modifyUserEducation();
			$this->modifyUserSociety();
			$this->modifyUserSkill();
			$this->modifyUserAward();
			$this->modifyUserLicense();
			$this->modifyUserPortfolio();
			$this->modifyPortfolioSkill();
			$this->modifyCareerSkill();
			$this->modifySeniority();
			$this -> theme_model-> set_session($this->session->userdata('email'));
				
			//跟原始檔名一樣的話，可以把較大的舊檔覆蓋掉
			redirect('user/portfolio/'.$this->session->userdata('alias'));
		}
	}
	//更新履歷經歷資料
	function modifyUserExperience(){
			$this->user_model->isLogin();
			$user_id=$this->session->userdata('user_id');
			$company_name=$this->security->xss_clean($this->input->post("company_name"));
			$job_title=$this->security->xss_clean($this->input->post("job_title"));
			$experience_start=$this->security->xss_clean($this->input->post("experience_start"));
			$experience_start_month=$this->security->xss_clean($this->input->post("experience_start_month"));
			$experience_end=$this->security->xss_clean($this->input->post("experience_end"));
			$experience_end_month=$this->security->xss_clean($this->input->post("experience_end_month"));
			$until_now=$this->security->xss_clean($this->input->post("work_untilNow"));
			// userExperience裡存放使用者在編輯頁面填寫的經歷資料
			// 只在三個欄位都填寫時才更新資料庫(經歷名稱，經歷開始時間與經歷結束時間)
			$edit_userExperience=array();
			//使用者填寫的完整經歷筆數
			$edit_experience_num=0;
			for($i=0;$i<count($company_name);$i++){
				if(!empty($company_name[$i])&&!empty($job_title[$i]) && !empty($experience_start[$i])){
					$edit_userExperience[$i]=array();
					$edit_userExperience[$i]['company_name']	=$company_name[$i];	
					$edit_userExperience[$i]['job_title']		=$job_title[$i];	
					$edit_userExperience[$i]['experience_start']=$experience_start[$i];
					$edit_userExperience[$i]['experience_start_month']=$experience_start_month[$i];
					if(!empty($experience_end[$i])){
						$edit_userExperience[$i]['experience_end']	=$experience_end[$i];
					}else{
						$edit_userExperience[$i]['experience_end']	='0';
					}
					if(!empty($experience_end_month[$i])){
						$edit_userExperience[$i]['experience_end_month']	=$experience_end_month[$i];
					}else{
						$edit_userExperience[$i]['experience_end_month']	='0';
					}
					if($experience_end[$i]==0 && $experience_end_month[$i]==0){
						$edit_userExperience[$i]['until_now']		='0';
					}else{
						$edit_userExperience[$i]['until_now']		='1';
					}
					$edit_userExperience[$i]['experience_order']=$i+1;
					$edit_userExperience[$i]['user_id']			=$user_id;
					$edit_experience_num++;
				}
			}
			//使用者資料庫中存放的經歷資料
			$db_experience_num=$this->user_portfolio_model->getUserExperience($this->session->userdata('email'))->num_rows();
			$db_userExperience=$this->user_portfolio_model->getUserExperience($this->session->userdata('email'))->result_array();

			//資料庫中尚無經歷資料
			if($db_experience_num==0 && $edit_experience_num>0){
				//insert
				for($i=0;$i<count($edit_userExperience);$i++){
					if(!empty($edit_userExperience[$i]['company_name'])&&!empty($edit_userExperience[$i]['job_title'])&&!empty($edit_userExperience[$i]['experience_start'])&&!empty($edit_userExperience[$i]['experience_start_month']))
					$this->user_portfolio_model->addUserExperience($edit_userExperience[$i]);
				}
			//使用者填寫的經歷資料數量跟資料庫數量相同
			}elseif($edit_experience_num==$db_experience_num){
				//update
				$update_times=$edit_experience_num;
				for($i=0;$i<$update_times;$i++){
					if(!empty($edit_userExperience[$i]['company_name'])&&!empty($edit_userExperience[$i]['job_title'])&&!empty($edit_userExperience[$i]['experience_start'])&&!empty($edit_userExperience[$i]['experience_start_month']))
					$this->user_portfolio_model->updateUserExperience($db_userExperience[$i]['user_experience_id'],$edit_userExperience[$i]);
				}
			//使用者填寫的經歷資料數量比資料庫多
			}elseif($edit_experience_num>$db_experience_num){
				//update+add
				$update_times=$db_experience_num;
				$add_times=$edit_experience_num-$db_experience_num;
				for($i=0;$i<$update_times;$i++){
					if(!empty($edit_userExperience[$i]['company_name'])&&!empty($edit_userExperience[$i]['job_title'])&&!empty($edit_userExperience[$i]['experience_start'])&&!empty($edit_userExperience[$i]['experience_start_month']))
					$this->user_portfolio_model->updateUserExperience($db_userExperience[$i]['user_experience_id'],$edit_userExperience[$i]);
				}
				for($i=0;$i<$add_times;$i++){
					$edit_userExperience[$i]['experience_order']	=$i+$update_times+1;
					if(!empty($edit_userExperience[$i]['company_name'])&&!empty($edit_userExperience[$i]['job_title'])&&!empty($edit_userExperience[$i]['experience_start'])&&!empty($edit_userExperience[$i]['experience_start_month']))
					$this->user_portfolio_model->addUserExperience($edit_userExperience[$update_times+$i]);
				}
			//使用者填寫的經歷資料數量比資料庫少
			}elseif($edit_experience_num<$db_experience_num){
				//update+delete
				$update_times=$edit_experience_num;
				$delete_times=$db_experience_num-$edit_experience_num;
				for($i=0;$i<$update_times;$i++){
					if(!empty($edit_userExperience[$i]['company_name'])&&!empty($edit_userExperience[$i]['job_title'])&&!empty($edit_userExperience[$i]['experience_start'])&&!empty($edit_userExperience[$i]['experience_start_month']))
					$this->user_portfolio_model->updateUserExperience($db_userExperience[$i]['user_experience_id'],$edit_userExperience[$i]);
				}
				for($i=0;$i<$delete_times;$i++){
					$this->user_portfolio_model->deleteUserExperience($user_id,$update_times+$i+1);
				}
			}
	}
	//更新履歷社群資料
	function modifyUserSociety(){
		$this->user_model->isLogin();
		$user_id=$this->session->userdata('user_id');
		$data = array(
				'facebook' => $this->security->xss_clean($this->input->post("facebook")),
				'google_plus' => $this->security->xss_clean($this->input->post("google_plus")),
				'twitter' => $this->security->xss_clean($this->input->post("twitter")),
				'linkedin' => $this->security->xss_clean($this->input->post("linkedin")),
				'github' => $this->security->xss_clean($this->input->post("github")),
				'user_id' => $user_id
		);
		//使用者資料庫中存放的學歷資料
		$db_society_num=$this->user_portfolio_model->getUserSociety($this->session->userdata('email'))->num_rows();
		$db_society=$this->user_portfolio_model->getUserSociety($this->session->userdata('email'))->result_array();
		if($db_society_num==0){
			$this->user_portfolio_model->addUserSociety($data);
		}else{
			$this->user_portfolio_model->updateUserSociety($user_id,$data);
		}
	}
	//更新履歷擅長工具資料
	function modifyUserSkill(){
		$this->user_model->isLogin();
		$user_id=$this->session->userdata('user_id');
		$skill_name=$this->security->xss_clean($this->input->post("skill_name"));
		// userSkill裡存放使用者在編輯頁面填寫的擅長工具
		// 只在三個欄位技能名稱有填寫時才更新資料庫
		$edit_userSkill=array();
		//使用者填寫的完整擅長工具筆數
		$edit_skill_num=0;
		for($i=0;$i<count($skill_name);$i++){
			if(!empty($skill_name[$i])){
				$edit_userSkill[$i]=array();
				$edit_userSkill[$i]['skill_name']	=$skill_name[$i];
				$edit_userSkill[$i]['skill_order']=$i+1;
				$edit_userSkill[$i]['user_id']			=$user_id;
				$edit_skill_num++;
			}
		}
		//使用者資料庫中存放的擅長工具資料
		$db_skill_num=$this->user_portfolio_model->getUserSkill($this->session->userdata('email'))->num_rows();
		$db_userSkill=$this->user_portfolio_model->getUserSkill($this->session->userdata('email'))->result_array();
	
		//資料庫中尚無擅長工具資料
		if($db_skill_num==0 && $edit_skill_num>0){
			//insert
			for($i=0;$i<count($edit_userSkill);$i++){
				if(!empty($edit_userSkill[$i]['skill_name']))
				$this->user_portfolio_model->addUserSkill($edit_userSkill[$i]);
			}
			//使用者填寫的擅長工具資料數量跟資料庫數量相同
		}elseif($edit_skill_num==$db_skill_num){
			//update
			$update_times=$edit_skill_num;
			for($i=0;$i<$update_times;$i++){
				if(!empty($edit_userSkill[$i]['skill_name']))
				$this->user_portfolio_model->updateUserSkill($db_userSkill[$i]['user_skill_id'],$edit_userSkill[$i]);
			}
			//使用者填寫的擅長工具資料數量比資料庫多
		}elseif($edit_skill_num>$db_skill_num){
			//update+add
			$update_times=$db_skill_num;
			$add_times=$edit_skill_num-$db_skill_num;
			for($i=0;$i<$update_times;$i++){
				if(!empty($edit_userSkill[$i]['skill_name']))
				$this->user_portfolio_model->updateUserSkill($db_userSkill[$i]['user_skill_id'],$edit_userSkill[$i]);
			}
			for($i=0;$i<$add_times;$i++){
				$edit_userSkill[$i]['skill_order']	=$i+$update_times+1;
				if(!empty($edit_userSkill[$i]['skill_name']))
				$this->user_portfolio_model->addUserSkill($edit_userSkill[$update_times+$i]);
			}
			//使用者填寫的擅長工具資料數量比資料庫少
		}elseif($edit_skill_num<$db_skill_num){
			//update+delete
			$update_times=$edit_skill_num;
			$delete_times=$db_skill_num-$edit_skill_num;
			for($i=0;$i<$update_times;$i++){
				if(!empty($edit_userSkill[$i]['skill_name']))
				$this->user_portfolio_model->updateUserSkill($db_userSkill[$i]['user_skill_id'],$edit_userSkill[$i]);
			}
			for($i=0;$i<$delete_times;$i++){
				$this->user_portfolio_model->deleteUserSkill($user_id,$update_times+$i+1);
			}
		}
	}
	//更新履歷學歷資料
	function modifyUserEducation(){
		$this->user_model->isLogin();
		$user_id=$this->session->userdata('user_id');
		$school_name=$this->security->xss_clean($this->input->post("school_name"));
		$department=$this->security->xss_clean($this->input->post("department"));
		$education_start=$this->security->xss_clean($this->input->post("education_start"));
		$education_start_month=$this->security->xss_clean($this->input->post("education_start_month"));
		$education_end=$this->security->xss_clean($this->input->post("education_end"));
		$education_end_month=$this->security->xss_clean($this->input->post("education_end_month"));
		$until_now=$this->security->xss_clean($this->input->post("education_untilNow"));
		$degree=$this->security->xss_clean($this->input->post("degree"));
		// userEducation裡存放使用者在編輯頁面填寫的學歷資料
		// 只在三個欄位都填寫時才更新資料庫(學歷名稱，學歷開始時間與學歷結束時間)
		$edit_userEducation=array();
		//使用者填寫的完整學歷筆數
		$edit_education_num=0;
		for($i=0;$i<count($school_name);$i++){
			if(!empty($school_name[$i])&&!empty($department[$i]) && !empty($education_start[$i])){
				$edit_userEducation[$i]=array();
				$edit_userEducation[$i]['school_name']	=$school_name[$i];
				$edit_userEducation[$i]['department']		=$department[$i];
				$edit_userEducation[$i]['degree']=$degree[$i];
				$edit_userEducation[$i]['education_start']=$education_start[$i];
				$edit_userEducation[$i]['education_start_month']=$education_start_month[$i];
				$edit_userEducation[$i]['education_end']	=$education_end[$i];
				$edit_userEducation[$i]['education_end_month']	=$education_end_month[$i];
				if($education_end[$i]==0 &&$education_end_month[$i]==0)
				{
					$until_now[$i]='0';
					$edit_userEducation[$i]['until_now']		=$until_now[$i];
				}else{
					$edit_userEducation[$i]['until_now']		=$until_now[$i];
				}
				$edit_userEducation[$i]['education_order']=$i+1;
				$edit_userEducation[$i]['user_id']			=$user_id;
				$edit_education_num++;
			}
		}
		//使用者資料庫中存放的學歷資料
		$db_education_num=$this->user_portfolio_model->getUserEducation($this->session->userdata('email'))->num_rows();
		$db_userEducation=$this->user_portfolio_model->getUserEducation($this->session->userdata('email'))->result_array();
	
		//資料庫中尚無學歷資料
		if($db_education_num==0 && $edit_education_num>0){
			//insert
			for($i=0;$i<count($edit_userEducation);$i++){
				if(!empty($edit_userEducation[$i]['school_name']) && !empty($edit_userEducation[$i]['department']) && !empty($edit_userEducation[$i]['education_start'] )&& !empty($edit_userEducation[$i]['education_start_month'] ))
				$this->user_portfolio_model->addUserEducation($edit_userEducation[$i]);
			}
			//使用者填寫的學歷資料數量跟資料庫數量相同
		}elseif($edit_education_num==$db_education_num){
			//update
			$update_times=$edit_education_num;
			for($i=0;$i<$update_times;$i++){
				if(!empty($edit_userEducation[$i]['school_name']) && !empty($edit_userEducation[$i]['department']) && !empty($edit_userEducation[$i]['education_start'] )&& !empty($edit_userEducation[$i]['education_start_month'] ))
				$this->user_portfolio_model->updateUserEducation($db_userEducation[$i]['user_education_id'],$edit_userEducation[$i]);
			}
			//使用者填寫的學歷資料數量比資料庫多
		}elseif($edit_education_num>$db_education_num){
			//update+add
			$update_times=$db_education_num;
			$add_times=$edit_education_num-$db_education_num;
			for($i=0;$i<$update_times;$i++){
				if(!empty($edit_userEducation[$i]['school_name']) && !empty($edit_userEducation[$i]['department']) && !empty($edit_userEducation[$i]['education_start'] )&& !empty($edit_userEducation[$i]['education_start_month'] ))
				$this->user_portfolio_model->updateUserEducation($db_userEducation[$i]['user_education_id'],$edit_userEducation[$i]);
			}
			for($i=0;$i<$add_times;$i++){
				$edit_userEducation[$i]['education_order']	=$i+$update_times+1;
				if(!empty($edit_userEducation[$i]['school_name']) && !empty($edit_userEducation[$i]['department']) && !empty($edit_userEducation[$i]['education_start'] )&& !empty($edit_userEducation[$i]['education_start_month'] ))
				$this->user_portfolio_model->addUserEducation($edit_userEducation[$update_times+$i]);
			}
			//使用者填寫的學歷資料數量比資料庫少
		}elseif($edit_education_num<$db_education_num){
			//update+delete
			$update_times=$edit_education_num;
			$delete_times=$db_education_num-$edit_education_num;
			for($i=0;$i<$update_times;$i++){
				if(!empty($edit_userEducation[$i]['school_name']) && !empty($edit_userEducation[$i]['department']) && !empty($edit_userEducation[$i]['education_start'] )&& !empty($edit_userEducation[$i]['education_start_month'] ))
				$this->user_portfolio_model->updateUserEducation($db_userEducation[$i]['user_education_id'],$edit_userEducation[$i]);
			}
			for($i=0;$i<$delete_times;$i++){
				$this->user_portfolio_model->deleteUserEducation($user_id,$update_times+$i+1);
			}
		}
	}
	//更新履歷獲獎記錄資料
	function modifyUserAward(){
			$this->user_model->isLogin();
			$user_id=$this->session->userdata('user_id');
			$award_name=$this->security->xss_clean($this->input->post("awards"));
			// userAward裡存放使用者在編輯頁面填寫的獲獎資料
			// 只在欄位都填寫時才更新資料庫(獲獎名稱)
			$edit_userAward=array();
			//使用者填寫的完整獲獎筆數
			$edit_award_num=0;
			for($i=0;$i<count($award_name);$i++){
				if(!empty($award_name[$i])){
					$edit_userAward[$i]=array();
					$edit_userAward[$i]['award_name']	=$award_name[$i];	
					$edit_userAward[$i]['award_order']	=$i+1;
					$edit_userAward[$i]['user_id']		=$user_id;
					$edit_award_num++;
				}
			}
			//使用者資料庫中存放的獲獎資料
			$db_award_num=$this->user_portfolio_model->getUserAward($this->session->userdata('email'))->num_rows();
			$db_userAward=$this->user_portfolio_model->getUserAward($this->session->userdata('email'))->result_array();

			//資料庫中尚無獲獎資料
			if($db_award_num==0 && $edit_award_num>0){
				//insert
				for($i=0;$i<count($edit_userAward);$i++){
					$this->user_portfolio_model->addUserAward($edit_userAward[$i]);
				}
			//使用者填寫的獲獎資料數量跟資料庫數量相同
			}elseif($edit_award_num==$db_award_num){
				//update
				$update_times=$edit_award_num;
				for($i=0;$i<$update_times;$i++){
					$this->user_portfolio_model->updateUserAward($db_userAward[$i]['user_award_id'],$edit_userAward[$i]);
				}
			//使用者填寫的獲獎資料數量比資料庫多
			}elseif($edit_award_num>$db_award_num){
				//update+add
				$update_times=$db_award_num;
				$add_times=$edit_award_num-$db_award_num;
				for($i=0;$i<$update_times;$i++){
					$this->user_portfolio_model->updateUserAward($db_userAward[$i]['user_award_id'],$edit_userAward[$i]);
				}
				for($i=0;$i<$add_times;$i++){
					$edit_userAward[$i]['award_order']	=$i+$update_times+1;
					$this->user_portfolio_model->addUserAward($edit_userAward[$update_times+$i]);
				}
			//使用者填寫的獲獎資料數量比資料庫少
			}elseif($edit_award_num<$db_award_num){
				//update+delete
				$update_times=$edit_award_num;
				$delete_times=$db_award_num-$edit_award_num;
				for($i=0;$i<$update_times;$i++){
					$this->user_portfolio_model->updateUserAward($db_userAward[$i]['user_award_id'],$edit_userAward[$i]);
				}
				for($i=0;$i<$delete_times;$i++){
					$this->user_portfolio_model->deleteUserAward($user_id,$update_times+$i+1);
				}
			}
	}
	//更新履歷證照資料
	function modifyUserLicense(){
			$this->user_model->isLogin();
			$user_id=$this->session->userdata('user_id');
			$license_name=$this->security->xss_clean($this->input->post("licenses"));
			// userLicense裡存放使用者在編輯頁面填寫的證照資料
			// 只在欄位都填寫時才更新資料庫(證照名稱)
			$edit_userLicense=array();
			//使用者填寫的完整證照筆數
			$edit_license_num=0;
			for($i=0;$i<count($license_name);$i++){
				if(!empty($license_name[$i])){
					$edit_userLicense[$i]=array();
					$edit_userLicense[$i]['license_name']	=$license_name[$i];	
					$edit_userLicense[$i]['license_order']	=$i+1;
					$edit_userLicense[$i]['user_id']		=$user_id;
					$edit_license_num++;
				}
			}
			//使用者資料庫中存放的證照資料
			$db_license_num=$this->user_portfolio_model->getUserLicense($this->session->userdata('email'))->num_rows();
			$db_userLicense=$this->user_portfolio_model->getUserLicense($this->session->userdata('email'))->result_array();

			//資料庫中尚無證照資料
			if($db_license_num==0 && $edit_license_num>0){
				//insert
				for($i=0;$i<count($edit_userLicense);$i++){
					$this->user_portfolio_model->addUserLicense($edit_userLicense[$i]);
				}
			//使用者填寫的證照資料數量跟資料庫數量相同
			}elseif($edit_license_num==$db_license_num){
				//update
				$update_times=$edit_license_num;
				for($i=0;$i<$update_times;$i++){
					$this->user_portfolio_model->updateUserLicense($db_userLicense[$i]['user_license_id'],$edit_userLicense[$i]);
				}
			//使用者填寫的經歷資料數量比資料庫多
			}elseif($edit_license_num>$db_license_num){
				//update+add
				$update_times=$db_license_num;
				$add_times=$edit_license_num-$db_license_num;
				for($i=0;$i<$update_times;$i++){
					$this->user_portfolio_model->updateUserLicense($db_userLicense[$i]['user_license_id'],$edit_userLicense[$i]);
				}
				for($i=0;$i<$add_times;$i++){
					$edit_userLicense[$i]['license_order']	=$i+$update_times+1;
					$this->user_portfolio_model->addUserLicense($edit_userLicense[$update_times+$i]);
				}
			//使用者填寫的證照資料數量比資料庫少
			}elseif($edit_license_num<$db_license_num){
				//update+delete
				$update_times=$edit_license_num;
				$delete_times=$db_license_num-$edit_license_num;
				for($i=0;$i<$update_times;$i++){
					$this->user_portfolio_model->updateUserLicense($db_userLicense[$i]['user_license_id'],$edit_userLicense[$i]);
				}
				for($i=0;$i<$delete_times;$i++){
					$this->user_portfolio_model->deleteUserLicense($user_id,$update_times+$i+1);
				}
			}
	}
	//更新履歷成果作品資料
	function modifyUserPortfolio(){
		$this->user_model->isLogin();
		$user_id=$this->session->userdata('user_id');
		$portfolio_name=$this->security->xss_clean($this->input->post("portfolio_name"));
		$portfolio_desc=$this->security->xss_clean($this->input->post("portfolio_desc"));
		$hyperlink=$this->security->xss_clean($this->input->post("hyperlink"));
		$portfolio_type=$this->security->xss_clean($this->input->post("portfolio_type"));
		$uploadImages=array();
		if(!empty($_FILES['portfolio_pic'])){
			$portfolioFolder='./assets/img/user/portfolio/'.$this->session->userdata('alias');
			$uploadImages=$this->upload_files(	$portfolioFolder,
												'',
												'portfolio_pic',
												$_FILES['portfolio_pic']);
		}
			// edit_userPortfolio裡存放使用者在編輯頁面填寫的作品資料
			// 只在欄位都填寫時才更新資料庫(證照名稱)
			$edit_userPortfolio=array();
			//使用者填寫的完整作品筆數
			$edit_portfolio_num=0;
			//使用者資料庫中存放的作品資料
			$db_portfolio_num=$this->user_portfolio_model->getUserPortfolio($this->session->userdata('email'))->num_rows();
			$db_userPortfolio=$this->user_portfolio_model->getUserPortfolio($this->session->userdata('email'))->result_array();
			for($i=0;$i<count($portfolio_name);$i++){
				if(!empty($portfolio_name[$i]) && !empty($portfolio_type[$i])){
					$edit_userPortfolio[$i]=array();
					$edit_userPortfolio[$i]['portfolio_name']	=$portfolio_name[$i];	
					if(!empty($uploadImages[$i])){
						$edit_userPortfolio[$i]['portfolio_pic']	=$uploadImages[$i];
					}else if(!empty($db_userPortfolio[$i]['portfolio_pic'])){
						$edit_userPortfolio[$i]['portfolio_pic']	=$db_userPortfolio[$i]['portfolio_pic'];
					}else{
						$edit_userPortfolio[$i]['portfolio_pic']	='browser.png';
					}
					if(!empty($portfolio_desc[$i])){						
						$edit_userPortfolio[$i]['portfolio_desc']	=preg_replace('/[\n\r\t]/','',nl2br($portfolio_desc[$i]));
					}else{
						$edit_userPortfolio[$i]['portfolio_desc']	='';
					}
					if(!empty($hyperlink[$i])){
						if($portfolio_type[$i]==2 && strpos($hyperlink[$i], 'http://www.youtube.com/watch?v=')!==false){
							$edit_userPortfolio[$i]['hyperlink']		=str_replace('http://www.youtube.com/watch?v=', 'http://www.youtube.com/embed/', $hyperlink[$i]);
						}elseif($portfolio_type[$i]==2 && strpos($hyperlink[$i], 'https://www.youtube.com/watch?v=')!==false){
							$edit_userPortfolio[$i]['hyperlink']		=str_replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/', $hyperlink[$i]);
						}else{
							$edit_userPortfolio[$i]['hyperlink']		=$hyperlink[$i];
						}
					}else{
						$edit_userPortfolio[$i]['hyperlink']		='';
					}
					$edit_userPortfolio[$i]['portfolio_type']	=$portfolio_type[$i];
					$edit_userPortfolio[$i]['portfolio_order']	=$i+1;
					$edit_userPortfolio[$i]['user_id']			=$user_id;
					$edit_portfolio_num++;
				}
			}
				//資料庫中尚無作品資料
				if($db_portfolio_num==0 && $edit_portfolio_num>0){
					//insert
					for($i=0;$i<count($edit_userPortfolio);$i++){
						if(!empty($edit_userPortfolio[$i]['portfolio_name'])&&!empty($edit_userPortfolio[$i]['portfolio_type']))
						$this->user_portfolio_model->addUserPortfolio($edit_userPortfolio[$i]);
					}
				//使用者填寫的作品資料數量跟資料庫數量相同
				}elseif($edit_portfolio_num==$db_portfolio_num){
					//update
					$update_times=$edit_portfolio_num;
					for($i=0;$i<$update_times;$i++){
						if(!empty($edit_userPortfolio[$i]['portfolio_name'])&&!empty($edit_userPortfolio[$i]['portfolio_type']))
						$this->user_portfolio_model->updateUserPortfolio($db_userPortfolio[$i]['user_portfolio_id'],$edit_userPortfolio[$i]);
					}
				//使用者填寫的作品資料數量比資料庫多
				}elseif($edit_portfolio_num>$db_portfolio_num){
					//update+add
					$update_times=$db_portfolio_num;
					$add_times=$edit_portfolio_num-$db_portfolio_num;
					for($i=0;$i<$update_times;$i++){
						if(!empty($edit_userPortfolio[$i]['portfolio_name'])&&!empty($edit_userPortfolio[$i]['portfolio_type']))
						$this->user_portfolio_model->updateUserPortfolio($db_userPortfolio[$i]['user_portfolio_id'],$edit_userPortfolio[$i]);
					}
					for($i=0;$i<$add_times;$i++){
						$edit_userPortfolio[$i]['portfolio_order']	=$i+$update_times+1;
						if(!empty($edit_userPortfolio[$i]['portfolio_name'])&&!empty($edit_userPortfolio[$i]['portfolio_type']))
						$this->user_portfolio_model->addUserPortfolio($edit_userPortfolio[$update_times+$i]);
					}
				//使用者填寫的作品資料數量比資料庫少
				}elseif($edit_portfolio_num<$db_portfolio_num){
					//update+delete
					$update_times=$edit_portfolio_num;
					$delete_times=$db_portfolio_num-$edit_portfolio_num;
					for($i=0;$i<$update_times;$i++){
						if(!empty($edit_userPortfolio[$i]['portfolio_name'])&&!empty($edit_userPortfolio[$i]['portfolio_type']))
						$this->user_portfolio_model->updateUserPortfolio($db_userPortfolio[$i]['user_portfolio_id'],$edit_userPortfolio[$i]);
					}
					for($i=0;$i<$delete_times;$i++){
						$this->user_portfolio_model->deleteUserPortfolio($user_id,$update_times+$i+1);
					}
				}
			// $this->user_portfolio_model->updateUserPortfolio($db_userPortfolio[$i]['user_portfolio_id'],$edit_userPortfolio[$i]);
			$this->modifyPortfolioSkill();
			$this -> theme_model-> set_session($this->session->userdata('email'));
	}
	//更新履歷訓練資料
	function modifyUserTraining(){
		$this->user_model->isLogin();
		$user_id=$this->session->userdata('user_id');
		$training_name=$this->security->xss_clean($this->input->post("training_name"));
		$training_unit=$this->security->xss_clean($this->input->post("training_unit"));
		$training_start=$this->security->xss_clean($this->input->post("training_start"));
		$training_start_month=$this->security->xss_clean($this->input->post("training_start_month"));
		$training_end=$this->security->xss_clean($this->input->post("training_end"));
		$training_end_month=$this->security->xss_clean($this->input->post("training_end_month"));
		// userTraining裡存放使用者在編輯頁面填寫的專業訓練資料
		// 只在三個欄位都填寫時才更新資料庫(訓練名稱，訓練開始時間與訓練結束時間)
		$edit_userTraining=array();
		//使用者填寫的完整經歷筆數
		$edit_training_num=0;
		for($i=0;$i<count($training_name);$i++){
			if(!empty($training_name[$i])&&!empty($training_unit[$i]) && !empty($training_start[$i])){
				$edit_userTraining[$i]=array();
				$edit_userTraining[$i]['training_name']	=$training_name[$i];
				$edit_userTraining[$i]['training_unit']	=$training_unit[$i];
				$edit_userTraining[$i]['training_start']=$training_start[$i];
				$edit_userTraining[$i]['training_start_month']=$training_start_month[$i];
				if(!empty($training_end[$i])){
					$edit_userTraining[$i]['training_end']	=$training_end[$i];
				}else{
					$edit_userTraining[$i]['training_end']	='0';
				}
				if(!empty($training_end_month[$i])){
					$edit_userTraining[$i]['training_end_month']	=$training_end_month[$i];
				}else{
					$edit_userTraining[$i]['training_end_month']	='0';
				}
				if($training_end[$i]==0 && $training_end_month[$i]==0){
					$edit_userTraining[$i]['until_now']		='0';
				}else{
					$edit_userTraining[$i]['until_now']		='1';
				}
				$edit_userTraining[$i]['training_order']=$i+1;
				$edit_userTraining[$i]['user_id']			=$user_id;
				$edit_training_num++;
			}
		}
		//使用者資料庫中存放的訓練資料
		$db_training_num=$this->user_portfolio_model->getUserTraining($this->session->userdata('email'))->num_rows();
		$db_userTraining=$this->user_portfolio_model->getUserTraining($this->session->userdata('email'))->result_array();
	
		//資料庫中尚無訓練資料
		if($db_training_num==0 && $edit_training_num>0){
			//insert
			for($i=0;$i<count($edit_userTraining);$i++){
				if(!empty($edit_userTraining[$i]['training_name'])&&!empty($edit_userTraining[$i]['training_unit'])&&!empty($edit_userTraining[$i]['training_start'])&&!empty($edit_userTraining[$i]['training_start_month']))
					$this->user_portfolio_model->addUserTraining($edit_userTraining[$i]);
			}
			//使用者填寫的訓練資料數量跟資料庫數量相同
		}elseif($edit_training_num==$db_training_num){
			//update
			$update_times=$edit_training_num;
			for($i=0;$i<$update_times;$i++){
				if(!empty($edit_userTraining[$i]['training_name'])&&!empty($edit_userTraining[$i]['training_unit'])&&!empty($edit_userTraining[$i]['training_start'])&&!empty($edit_userTraining[$i]['training_start_month']))
					$this->user_portfolio_model->updateUserTraining($db_userTraining[$i]['user_training_id'],$edit_userTraining[$i]);
			}
			//使用者填寫的訓練資料數量比資料庫多
		}elseif($edit_training_num>$db_training_num){
			//update+add
			$update_times=$db_training_num;
			$add_times=$edit_training_num-$db_training_num;
			for($i=0;$i<$update_times;$i++){
				if(!empty($edit_userTraining[$i]['training_name'])&&!empty($edit_userTraining[$i]['training_unit'])&&!empty($edit_userTraining[$i]['training_start'])&&!empty($edit_userTraining[$i]['training_start_month']))
					$this->user_portfolio_model->updateUserTraining($db_userTraining[$i]['user_training_id'],$edit_userTraining[$i]);
			}
			for($i=0;$i<$add_times;$i++){
				$edit_userTraining[$i]['training_order']	=$i+$update_times+1;
				if(!empty($edit_userTraining[$i]['training_name'])&&!empty($edit_userTraining[$i]['training_unit'])&&!empty($edit_userTraining[$i]['training_start'])&&!empty($edit_userTraining[$i]['training_start_month']))
					$this->user_portfolio_model->addUserTraining($edit_userTraining[$update_times+$i]);
			}
			//使用者填寫的訓練資料數量比資料庫少
		}elseif($edit_training_num<$db_training_num){
			//update+delete
			$update_times=$edit_training_num;
			$delete_times=$db_training_num-$edit_training_num;
			for($i=0;$i<$update_times;$i++){
				if(!empty($edit_userTraining[$i]['training_name'])&&!empty($edit_userTraining[$i]['training_unit'])&&!empty($edit_userTraining[$i]['training_start'])&&!empty($edit_userTraining[$i]['training_start_month']))
					$this->user_portfolio_model->updateUserTraining($db_userTraining[$i]['user_training_id'],$edit_userTraining[$i]);
			}
			for($i=0;$i<$delete_times;$i++){
				$this->user_portfolio_model->deleteUserTraining($user_id,$update_times+$i+1);
			}
		}
	}
	//更新作品使用技術
	function modifyPortfolioSkill() {
		$this->user_model->isLogin();
		$user_id=$this->session->userdata('user_id');
		//每筆作品資料對應使用到的技術，假設作品共有六個，Post讀到的會是六組字串，需個別用explode分割','，才會是各個作品使用到的技術
		$user_skill_id=$this->security->xss_clean($this->input->post("portfolio_skill"));
		//使用者資料庫中存放的作品資料
		$db_portfolio_num=$this->user_portfolio_model->getUserPortfolio($this->session->userdata('email'))->num_rows();
		$db_userPortfolio=$this->user_portfolio_model->getUserPortfolio($this->session->userdata('email'))->result_array();
		//使用者資料庫中存放的作品使用技術
		$db_portfolioSkill		=array();
		for($i=0;$i<$db_portfolio_num;$i++){
			$db_portfolioSkill_num[$i]	=$this->user_portfolio_model->getOnePortfolioSkill($this->session->userdata('email'),$db_userPortfolio[$i]['user_portfolio_id'])->num_rows();
			$db_portfolioSkill[$i]		=$this->user_portfolio_model->getOnePortfolioSkill($this->session->userdata('email'),$db_userPortfolio[$i]['user_portfolio_id'])->result_array();
		}
		//分割後的作品使用技術
		$edit_portfolio_skill=array();
		for($i=0;$i<$db_portfolio_num;$i++){
			$edit_portfolio_skill[$i]=array();
			$edit_portfolio_skill[$i]=explode(',',$user_skill_id[$i]);
		}
		$portfolioSkill=array();
		for($i=0;$i<$db_portfolio_num;$i++){
			$portfolioSkill[$i]=array();
			for($j=0;$j<count($edit_portfolio_skill[$i]);$j++){
				$portfolioSkill[$i][$j]=array();
				$portfolioSkill[$i][$j]['user_portfolio_id']	=$db_userPortfolio[$i]['user_portfolio_id'];
				$portfolioSkill[$i][$j]['user_skill_id']		=$edit_portfolio_skill[$i][$j];
				$portfolioSkill[$i][$j]['user_portfolio_order']	=$j+1;
			}
		}
		for($i=0;$i<$db_portfolio_num;$i++){
			//資料庫中尚無作品使用技術使用技術資料
			if($db_portfolioSkill_num[$i]==0 && count($edit_portfolio_skill[$i])>0){
				//insert
				for($j=0;$j<count($edit_portfolio_skill[$i]);$j++){
					$this->user_portfolio_model->addPortfolioSkill($portfolioSkill[$i][$j]);
				}
			//使用者填寫的作品使用技術資料數量跟資料庫數量相同
			}elseif(count($edit_portfolio_skill[$i])==$db_portfolioSkill_num[$i]){
				//update
				for($j=0;$j<count($edit_portfolio_skill[$i]);$j++){
					$this->user_portfolio_model->updatePortfolioSkill($portfolioSkill[$i][$j],$db_portfolioSkill[$i][$j]['portfolio_skill_id']);
				}
			//使用者填寫的作品使用技術資料數量比資料庫多
			}elseif(count($edit_portfolio_skill[$i])>$db_portfolioSkill_num[$i]){
				//update+add
				$update_times=$db_portfolioSkill_num[$i];
				$add_times=count($edit_portfolio_skill[$i])-$db_portfolioSkill_num[$i];
				for($j=0;$j<$update_times;$j++){
					$this->user_portfolio_model->updatePortfolioSkill($portfolioSkill[$i][$j],$db_portfolioSkill[$i][$j]['portfolio_skill_id']);
				}
				for($j=0;$j<$add_times;$j++){
					$edit_userPortfolio[$i]['portfolio_order']	=$j+$update_times+1;
					$this->user_portfolio_model->addPortfolioSkill($portfolioSkill[$i][$update_times+$j]);
				}
			//使用者填寫的作品使用技術資料數量比資料庫少
			}elseif(count($edit_portfolio_skill[$i])<$db_portfolioSkill_num[$i]){
				//update+delete
				$update_times=count($edit_portfolio_skill[$i]);
				$delete_times=$db_portfolioSkill_num[$i]-count($edit_portfolio_skill[$i]);
				for($j=0;$j<$update_times;$j++){
						$this->user_portfolio_model->updatePortfolioSkill($portfolioSkill[$i][$j],$db_portfolioSkill[$i][$j]['portfolio_skill_id']);
				}
				for($j=0;$i<$delete_times;$j++){
					$this->user_portfolio_model->deletePortfolioSkill($db_userPortfolio[$i]['user_portfolio_id'],$update_times+$j+1);
				}
			}

		}
	}
	//更新履歷專業技能資料
	function modifyCareerSkill(){
		$this->user_model->isLogin();
		$user_id=$this->session->userdata('user_id');
		$career_skill_name=$this->security->xss_clean($this->input->post("career_skill_name"));
		// careerSkill裡存放使用者在編輯頁面填寫的技能資料
		// 只在三個欄位技能名稱有填寫時才更新資料庫
		$edit_careerSkill=array();
		//使用者填寫的完整技能筆數
		$edit_careerSkill_num=0;
		for($i=0;$i<count($career_skill_name);$i++){
			if(!empty($career_skill_name[$i])){
				$edit_careerSkill[$i]=array();
				$edit_careerSkill[$i]['career_skill_name']	=$career_skill_name[$i];
				$edit_careerSkill[$i]['career_skill_order']=$i+1;
				$edit_careerSkill[$i]['user_id']			=$user_id;
				$edit_careerSkill_num++;
			}
		}
		//使用者資料庫中存放的技能資料
		$db_careerSkill_num=$this->user_portfolio_model->getCareerSkill($this->session->userdata('email'))->num_rows();
		$db_careerSkill=$this->user_portfolio_model->getCareerSkill($this->session->userdata('email'))->result_array();
	
		//資料庫中尚無技能資料
		if($db_careerSkill_num==0 && $edit_careerSkill_num>0){
			//insert
			for($i=0;$i<count($edit_careerSkill);$i++){
				if(!empty($edit_careerSkill[$i]['career_skill_name']))
					$this->user_portfolio_model->addCareerSkill($edit_careerSkill[$i]);
			}
			//使用者填寫的技能資料數量跟資料庫數量相同
		}elseif($edit_careerSkill_num==$db_careerSkill_num){
			//update
			$update_times=$edit_careerSkill_num;
			for($i=0;$i<$update_times;$i++){
				if(!empty($edit_careerSkill[$i]['career_skill_name']))
					$this->user_portfolio_model->updateCareerSkill($db_careerSkill[$i]['user_career_skill_id'],$edit_careerSkill[$i]);
			}
			//使用者填寫的技能資料數量比資料庫多
		}elseif($edit_careerSkill_num>$db_careerSkill_num){
			//update+add
			$update_times=$db_careerSkill_num;
			$add_times=$edit_careerSkill_num-$db_careerSkill_num;
			for($i=0;$i<$update_times;$i++){
				if(!empty($edit_careerSkill[$i]['career_skill_name']))
					$this->user_portfolio_model->updateCareerSkill($db_careerSkill[$i]['user_career_skill_id'],$edit_careerSkill[$i]);
			}
			for($i=0;$i<$add_times;$i++){
				$edit_careerSkill[$i]['career_skill_order']	=$i+$update_times+1;
				if(!empty($edit_careerSkill[$i]['career_skill_name']))
					$this->user_portfolio_model->addCareerSkill($edit_careerSkill[$update_times+$i]);
			}
			//使用者填寫的技能資料數量比資料庫少
		}elseif($edit_careerSkill_num<$db_careerSkill_num){
			//update+delete
			$update_times=$edit_careerSkill_num;
			$delete_times=$db_careerSkill_num-$edit_careerSkill_num;
			for($i=0;$i<$update_times;$i++){
				if(!empty($edit_careerSkill[$i]['career_skill_name']))
					$this->user_portfolio_model->updateCareerSkill($db_careerSkill[$i]['user_career_skill_id'],$edit_careerSkill[$i]);
			}
			for($i=0;$i<$delete_times;$i++){
				$this->user_portfolio_model->deleteCareerSkill($user_id,$update_times+$i+1);
			}
		}
	}
	//製作上傳圖片的縮圖
	function createThumbImage($fileName,$portfolioFolder,$ext){
		$origin=$portfolioFolder.'/origin/'.$fileName;
		
		switch(strtolower($ext)){
			case "jpg":
				$src=imagecreatefromjpeg($origin);//讀取來源圖檔
				break;
			case "jpeg":
				$src=imagecreatefromjpeg($origin);//讀取來源圖檔
				break;
			case "gif":
				$src=imagecreatefromgif($origin);//讀取來源圖檔
				break;
			case "png":
				$src=imagecreatefrompng($origin);//讀取來源圖檔
				break;
		}
		$src_w = imagesx($src);     //取得來源圖檔長寬
		$src_h = imagesy($src);
		$new_w = 128;               //新圖檔長寬
		$new_h = 128;
		
		$thumb = imagecreatetruecolor($new_w, $new_h);    //建立空白縮圖
		//設定空白縮圖的背景，如不設定，背景預設為黑色
		$bg = imagecolorallocate($thumb,255,255,255);       //空白縮圖的背景顏色
		imagefilledrectangle($thumb,0,0,$src_w,$src_h,$bg); //將顏色填入縮圖
		//執行縮圖
		imagecopyresampled($thumb, $src, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);
		$thumbFolder=$portfolioFolder.'/'.$new_w.'x'.$new_h.'/';
		$this->createPath($thumbFolder);
		switch(strtolower($ext)){
			case "jpg":
				imagejpeg($thumb,$thumbFolder.$fileName,90);
				break;
			case "jpeg":
				imagejpeg($thumb,$thumbFolder.$fileName,90);
				break;
			case "gif":
				imagegif($thumb,$thumbFolder.$fileName);
				break;
			case "png":
				imagepng($thumb,$thumbFolder.$fileName,9);
				break;
		}
	}
	//上傳圖片
	function upload_files($portfolioFolder, $prefix, $columnName,$files){
		$originPictureFolder=$portfolioFolder.'/origin/';
		$config['upload_path'] = $originPictureFolder;
		$config['allowed_types'] = 'png|jpg|JPG|jpeg|JPEG';
		$config['max_size']	= '2000';
		$config['overwrite'] = 'TRUE';
		$this->load->library('upload', $config);
		$this->createPath($originPictureFolder);
        $images = array();
		$i=0;
        foreach ($_FILES[$columnName]['name'] as $key => $image) {
            $_FILES[$columnName]['name']		= $files['name']		[$key];
            $_FILES[$columnName]['type']		= $files['type']		[$key];
            $_FILES[$columnName]['tmp_name']	= $files['tmp_name']	[$key];
            $_FILES[$columnName]['error']		= $files['error']		[$key];
            $_FILES[$columnName]['size']		= $files['size']		[$key];
            $fileName = $prefix .''. $_FILES[$columnName]['name'];
            $images[$i] = $fileName;
            $config['file_name'] = $fileName;
			$ext=pathinfo($files['name'][$key],PATHINFO_EXTENSION);
			$fullPath[] = $config['upload_path'].$fileName;
            $this->upload->initialize($config);
            if($_FILES[$columnName]['error']){$i++;continue;}
            elseif ($this->upload->do_upload($columnName)) {
				$this->createThumbImage($fileName,$portfolioFolder,$ext);
            } else {
                return $images;
            }
			$i++;
        }
        return $images;
    }
	//判斷路徑的資料夾是否存在，若不存在則建立資料夾
	function createPath($path) {
		if (is_dir($path)) return true;
		$prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
		$return = $this->createPath($prev_path);
		return ($return && is_writable($prev_path)) ? mkdir($path) : false;
	}
	//切換履歷開放設定
	function toggleResumeFlag(){
		$email=$this->session->userdata('email');
		$recruiters=$this->security->xss_clean($this->input->post("recruiters"));
		$this->user_model->toggleResumeFlag($email,$recruiters);
	}
	//修改密碼
    function modifyPassword(){
    	$this->user_model->isLogin();
    	$email=$this->session->userdata('email');
    	$getOneUser = $this->user_model->getOneUser($email);
		$row=$getOneUser->row_array();
		$dbPassword = $row['password'];
		$password=$this->security->xss_clean($this->input->post("oldPassword"));
		$check = $this->theme_model->compare_password($password,$dbPassword);
        if($check){
        	$newPassword = $this->security->xss_clean($this->input->post('newPassword'));
        	$password_hashed = $this->theme_model->hash_password($newPassword);
        	$data = array('password' => $password_hashed);
        	//update 密碼
        	$this -> user_model -> updateUser($email,$data);
        	redirect('page/message/modifyPassword/'.$row['alias']);
		}else{
			//舊密碼驗證錯誤
			redirect('page/message/errorPassword/'.$row['alias']);
		}
		
    }
    //帳號管理修改信箱
    function updateUserEmail(){
    	$this->user_model->isLogin();
    	$email = $this->session->userdata('email');
    	$inputEmail=$this->security->xss_clean($this->input->post('contentEmail'));
    	$checkmail = $this->user_model->checkEmail($inputEmail);
    	if($checkmail=='1'){
    		redirect(base_url().'user/profile/'.$this->session->userdata('alias'));
    	}
    	$alias = strstr($inputEmail, '@', true);
    	$i = 10;
    	while ($i > 0) {
    		$i--;
    		$checkalias = $this->user_model->checkAlias($alias);
    		if($checkalias=='1'){
    			$alias = $alias.rand(1,20);
    			$checkalias = $this->user_model->checkAlias($alias);
    			if($checkalias=='0'){
    				break;
    			}
    		}
    	}
    	$data = array(	'email' => 	$inputEmail,
    					'alias'	=>	$alias);
    	$this->user_model->updateUser($email,$data);
    	$this->session->set_userdata('email',$inputEmail);
    	$this->session->set_userdata('alias',$alias);
    	redirect(base_url().'user/profile/'.$this->session->userdata('alias'));
    }
    //企業徵才修改企業資料
    function updateUserCompany(){
    	$this->user_model->isLogin();
    	$ext=pathinfo($_FILES['userfile']['name'],PATHINFO_EXTENSION);
    	$config['upload_path'] = './assets/img/company/origin';
    	$config['allowed_types'] = 'png|jpg|JPG|jpeg|JPEG';
    	$config['max_size']	= '2000';
    	$config['file_name'] = md5($this->session->userdata('email')).'.'.$ext;
    	$config['overwrite'] = 'TRUE';
    	$this->load->library('upload',$config);
        if ( ! $this->upload->do_upload()){
        	echo '<meta charset="utf-8">';
        	echo $this->upload->display_errors();
        	$data = array(	'company_name' => $this->security->xss_clean($this->input->post('company_name')),
        					'company_website' => $this->security->xss_clean($this->input->post('company_website')),
        	);
        	$this->user_model->updateUser($this->session->userdata('email'),$data);
        	redirect(base_url().'vacancy/listAllJobVacancy');
        }else{
            $file_size = $this->upload->data();
            //alias在個人資料與履歷編輯的頁面要使用到，若使用者打中文會有error
            //所以鎖起來不讓使用者修改
            $data = array(	'company_name' => $this->security->xss_clean($this->input->post('company_name')),
            				'company_website' => $this->security->xss_clean($this->input->post('company_website')),
               				'company_img' => md5($this->session->userdata('email')).'.'.$ext
            );
        $this->user_model->updateUser($this->session->userdata('email'),$data);
        $fullPath = base_url().'assets/img/company/origin/'.md5($this->session->userdata('email')).'.'.$ext;
        switch(strtolower($ext)){
            case "jpg":
                $src=imagecreatefromjpeg($fullPath);//讀取來源圖檔
                break;
            case "jpeg":
                $src=imagecreatefromjpeg($fullPath);//讀取來源圖檔
                break;
            case "gif":
                $src=imagecreatefromgif($fullPath);//讀取來源圖檔
                break;
            case "png":
                $src=imagecreatefrompng($fullPath);//讀取來源圖檔
                break;
        }
        $src_w = imagesx($src);     //取得來源圖檔長寬
        $src_h = imagesy($src);
        $new_w = 128;               //新圖檔長寬
        $new_h = 128;
        //執行縮圖
        $w_percent = $new_w / $src_w;
        $h_percent = $new_h / $src_h;
        $percent=($w_percent > $h_percent) ? $h_percent : $w_percent;
		if ($src_w < $new_w && $src_h < $new_h) {
			//如果都比預計縮圖的小就不用縮圖，另存一份至128x128資料夾中
	        $thumb = imagecreatetruecolor($src_w, $src_h);    //建立空白縮圖
	        //設定空白縮圖的背景，如不設定，背景預設為黑色
	        $bg = imagecolorallocate($thumb,255,255,255);       //空白縮圖的背景顏色
	        imagefilledrectangle($thumb,0,0,$src_w,$src_h,$bg); //將顏色填入縮圖
	        imagecopyresampled($thumb, $src, 0, 0, 0, 0, $src_w, $src_h, $src_w, $src_h);	
		}else{
			$thumb = imagecreatetruecolor($src_w*$percent, $src_h*$percent);    //建立空白縮圖
			//設定空白縮圖的背景，如不設定，背景預設為黑色
			$bg = imagecolorallocate($thumb,255,255,255);       //空白縮圖的背景顏色
			imagefilledrectangle($thumb,0,0,$src_w,$src_h,$bg); //將顏色填入縮圖
			imagecopyresampled($thumb, $src, 0, 0, 0, 0, $src_w*$percent, $src_h*$percent, $src_w, $src_h);
		}
	        switch(strtolower($ext)){
	            case "jpg":
	                imagejpeg($thumb, './assets/img/company/128x128/'.md5($this->session->userdata('email')).'.'.$ext,90);
	                break;
	            case "jpeg":
	                imagejpeg($thumb, './assets/img/company/128x128/'.md5($this->session->userdata('email')).'.'.$ext,90);
	                break;
	            case "gif":
	                imagegif($thumb, './assets/img/company/128x128/'.md5($this->session->userdata('email')).'.'.$ext);
	                break;
	            case "png":
	                imagepng($thumb, './assets/img/company/128x128/'.md5($this->session->userdata('email')).'.'.$ext,9);
	                break;
	        }

        //跟原始檔名一樣的話，可以把較大的舊檔覆蓋掉
        redirect(base_url().'vacancy/listAllJobVacancy');
        }
    }
    //更新履歷年資資料
    function modifySeniority(){
    	$this->user_model->isLogin();
    	$user_id=$this->session->userdata('user_id');
    	$seniority_category=$this->security->xss_clean($this->input->post("seniority_category"));
    	$year=$this->security->xss_clean($this->input->post("year_of_experience"));
//     	print_r($seniority_category);
//     	print_r($year);
    	// seniority裡存放使用者在編輯頁面填寫的年資資料
    	// 只在職務類別與年資欄位名稱有填寫時才更新資料庫
    	$edit_seniority=array();
    	//使用者填寫的完整年資筆數
    	$edit_seniority_num=0;
    	for($i=0;$i<count($seniority_category);$i++){
    		if(!empty($seniority_category[$i])){
    			$edit_seniority[$i]=array();
    			$edit_seniority[$i]['jobs_category_id']	=$seniority_category[$i];
    			$edit_seniority[$i]['year']	=$year[$i];
    			$edit_seniority[$i]['seniority_order']=$i+1;
    			$edit_seniority[$i]['user_id']			=$user_id;
    			$edit_seniority_num++;
    		}
    	}
    	//使用者資料庫中存放的年資資料
    	$db_seniority_num=$this->user_portfolio_model->getSeniority($this->session->userdata('email'))->num_rows();
    	$db_seniority=$this->user_portfolio_model->getSeniority($this->session->userdata('email'))->result_array();
    
    	//資料庫中尚無年資資料
    	if($db_seniority_num==0 && $edit_seniority_num>0){
    		//insert
    		for($i=0;$i<count($edit_seniority);$i++){
    			if(!empty($edit_seniority[$i]['jobs_category_id']))
    				$this->user_portfolio_model->addSeniority($edit_seniority[$i]);
    		}
    		//使用者填寫的年資資料數量跟資料庫數量相同
    	}elseif($edit_seniority_num==$db_seniority_num){
    		//update
    		$update_times=$edit_seniority_num;
    		for($i=0;$i<$update_times;$i++){
    			if(!empty($edit_seniority[$i]['jobs_category_id']))
    				$this->user_portfolio_model->updateSeniority($db_seniority[$i]['seniority_id'],$edit_seniority[$i]);
    		}
    		//使用者填寫的年資資料數量比資料庫多
    	}elseif($edit_seniority_num>$db_seniority_num){
    		//update+add
    		$update_times=$db_seniority_num;
    		$add_times=$edit_seniority_num-$db_seniority_num;
    		for($i=0;$i<$update_times;$i++){
    			if(!empty($edit_seniority[$i]['jobs_category_id']))
    				$this->user_portfolio_model->updateSeniority($db_seniority[$i]['seniority_id'],$edit_seniority[$i]);
    		}
    		for($i=0;$i<$add_times;$i++){
    			$edit_seniority[$i]['seniority_order']	=$i+$update_times+1;
    			if(!empty($edit_seniority[$i]['jobs_category_id']))
    				$this->user_portfolio_model->addSeniority($edit_seniority[$update_times+$i]);
    		}
    		//使用者填寫的年資資料數量比資料庫少
    	}elseif($edit_seniority_num<$db_seniority_num){
    		//update+delete
    		$update_times=$edit_seniority_num;
    		$delete_times=$db_seniority_num-$edit_seniority_num;
    		for($i=0;$i<$update_times;$i++){
    			if(!empty($edit_seniority[$i]['jobs_category_id']))
    				$this->user_portfolio_model->updateSeniority($db_seniority[$i]['seniority_id'],$edit_seniority[$i]);
    		}
    		for($i=0;$i<$delete_times;$i++){
    			$this->user_portfolio_model->deleteSeniority($user_id,$update_times+$i+1);
    		}
    	}
    }
    //能力分析
    function analysis(){
        $data['title']='能力分析';
        $this->user_model->isLogin();
        $user_id=$this->session->userdata('user_id');
        $course_url = $this->uri->segment(3);
        //第二層
        if(!empty($course_url)){
            $data['title']='能力分析-'.$course_url;
            $course_url = htmlspecialchars($course_url, ENT_QUOTES);
            $course_url = $this->security->xss_clean($course_url);
            $data['getUserBuyCourseChapter'] = json_encode($this->user_model->getUserBuyCourseChapter($course_url)->result());
            $result = $this->user_model->getUserBuyCourseChapter($course_url)->row_array();
            $data['result'] = $result;
            $data['getContentAvgScore'] = json_encode($this->points_model->getContentAvgScore($user_id,$course_url)->result());
            //如果$course_url query不到值，先導到/user/analysis吧！
            if(empty ($result)){
                redirect('/user/analysis');
            }
            $this->theme_model->loadTheme('portfolio/analysis_course',$data);
        }else{
        //第一層
            $data['getUserBuyTracks'] = json_encode($this->user_model->getUserBuyTracks()->result());
            $solvePersonal = $this->points_model->getSolvePersonal($user_id)->row_array();
            $solveHighest = $this->points_model->getSolveHighest()->row_array();
            $solveTotal = $this->points_model->getSolveTotal()->row_array();
            $expressionPersonal = $this->points_model->getExpressionPersonal($user_id)->row_array();
            $expressionHighest = $this->points_model->getExpressionHighest()->row_array();
            $expressionTotal = $this->points_model->getExpressionTotal()->row_array();
            $proactivePersonal = $this->points_model->getProactivePersonal($user_id)->row_array();
            $proactiveHighest = $this->points_model->getProactiveHighest()->result_array();
            $proactiveTotal = $this->points_model->getProactiveTotal()->row_array();
            $stack =array();
            foreach ($proactiveHighest as $row)
            {
               array_push($stack, ($row['sum1']+$row['sum2']+$row['sum3']));
            }
            rsort($stack);
            
            //最高$stack[0];
            //問題解決(兩個參數)
            //$para1 = round($solvePersonal['sum'] / $solveTotal['sum'] *100);
            //$para2 = round($solveHighest['sum'] / $solveTotal['sum'] *100);
            //表達互動(兩個參數)
            //$para3 = round($expressionPersonal['sum'] / $expressionTotal['sum'] *100);
            //$para4 = round($expressionHighest['sum'] / $expressionTotal['sum'] *100);
            //$para5 = round(($proactivePersonal['sum1']+$proactivePersonal['sum2']+$proactivePersonal['sum3']) / ($proactiveTotal['sum1']+$proactiveTotal['sum2']+$proactiveTotal['sum3']) *100);
            //echo '問題解決個人'.$solvePersonal['sum'];
            //echo '<p>問題解決最高'.$solveHighest['sum'];
            $radarData[] = $solvePersonal['sum']/$solveHighest['sum'] *100;
            //echo '<p>問題解決總分'.$solveTotal['sum'];
            //echo '<p>表達互動個人'.$expressionPersonal['sum'];
            //echo '<p>表達互動最高'.$expressionHighest['sum'];
            $radarData[] = round($expressionPersonal['sum']/$expressionHighest['sum'] *100);
            //echo '<p>表達互動總分'.$expressionTotal['sum'];
            //echo '<p>積極主動個人'.($proactivePersonal['sum1']+$proactivePersonal['sum2']+$proactivePersonal['sum3']);
            //echo '<p>積極主動最高'.$stack[0];

            $radarData[] = round(($proactivePersonal['sum1']+$proactivePersonal['sum2']+$proactivePersonal['sum3'])/$stack[0] *100);
            $radarData = implode(",", $radarData);
            //echo '<p>積極主動總分'.($proactiveTotal['sum1']+$proactiveTotal['sum2']+$proactiveTotal['sum3']);
            $data['radarData'] = $radarData;
            $data['getUserBuyTracksDetail'] = json_encode($this->user_model->getUserBuyTracksDetail()->result());
            $data['getUserBuyCourse'] = json_encode($this->user_model->getUserBuyCourse()->result());
            //$data['getAllScore'] = json_encode($this->points_model->getAllScore()->result());
            //$data['getMyScore'] = json_encode($this->points_model->getMyScore($user_id)->result());
            $data['getAvgScore'] = json_encode($this->points_model->getAvgScore($user_id)->result());
            $this->theme_model->loadTheme('portfolio/analysis',$data);    
        }
    }
    // 更新課程圖片
    function updateBadges(){
        $this->user_model->isTeacher();
        // use the course_id to get the course_url as the jpeg's name
        $course_id = $this->input->post("course_id",true);
        $data = $this-> course_model->getCourseUrl($course_id)->row();
        $course_url = $data->course_url;

        
        
        if(! $course_url){
            echo "課程名稱有誤";
            die();
        }
        $ext=pathinfo($_FILES['userfile']['name'],PATHINFO_EXTENSION);
        $config['upload_path'] = './assets/img/badges/origin';
        $config['allowed_types'] = 'png|jpg|JPG|jpeg|JPEG';
        $config['max_size']    = '2000';
        //$config['max_width']  = '1024';
        //$config['max_height']  = '768';
        $config['file_name'] = $course_url.'.'.$ext;
        $config['overwrite'] = 'TRUE';
        $this->load->library('upload',$config);
        
        if ( ! $this->upload->do_upload()){
            //無上傳相片時
            $file_size = $this->upload->data();
            if(! $file_size['file_size']){
                redirect('course/teach');
            }else{
            //上傳有錯誤時
                $data['title']='圖片上傳失敗';
                $data['errorMessage']=$this->upload->display_errors();
                $this->theme_model->loadTheme('profile/errorPage',$data);
            }
        }else{
            $file_size = $this->upload->data();
            $data = array('course_pic' => $course_url.'.'.$ext);
            $this->course_model->updateCoursePic($course_id,$data);
            
            $fullPath = base_url().'assets/img/badges/origin/'.$course_url.'.'.$ext;
            switch(strtolower($ext)){
                case "jpg":
                    $src=imagecreatefromjpeg($fullPath);//讀取來源圖檔
                    break;
                case "jpeg":
                    $src=imagecreatefromjpeg($fullPath);//讀取來源圖檔
                    break;
                case "gif":
                    $src=imagecreatefromgif($fullPath);//讀取來源圖檔
                    break;
                case "png":
                    $src=imagecreatefrompng($fullPath);//讀取來源圖檔
                    break;
            }
            $src_w = imagesx($src);     //取得來源圖檔長寬
            $src_h = imagesy($src);
            $new_w = 128;               //新圖檔長寬
            $new_h = 128;
            $thumb = imagecreatetruecolor($new_w, $new_h);    //建立空白縮圖
            //設定空白縮圖的背景，如不設定，背景預設為黑色
            $bg = imagecolorallocate($thumb,255,255,255);       //空白縮圖的背景顏色
            imagefilledrectangle($thumb,0,0,$src_w,$src_h,$bg); //將顏色填入縮圖
            //執行縮圖
            imagecopyresampled($thumb, $src, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);

            switch(strtolower($ext)){
                case "jpg":
                    imagejpeg($thumb, './assets/img/badges/'.$course_url.'.'.$ext,90);
                    break;
                case "jpeg":
                    imagejpeg($thumb, './assets/img/badges/'.$course_url.'.'.$ext,90);
                    break;
                case "gif":
                    imagegif($thumb, './assets/img/badges/'.$course_url.'.'.$ext);
                    break;
                case "png":
                    imagepng($thumb, './assets/img/badges/'.$course_url.'.'.$ext,9);
                    break;
            }
            //跟原始檔名一樣的話，可以把較大的舊檔覆蓋掉
            redirect('course/teach');
        }
    }
    //履歷編輯頁
    function printPortfolio($alias)
    {   
        $data['title']='列印個人履歷';
        //檢查使用者是否登入
        $this->user_model->isLogin();

        $getUserByAlias=$this->user_model->getUserByAlias($alias);
        if($getUserByAlias->num_rows()>0){
            $email=$getUserByAlias->row()->email;
            $user_id=$getUserByAlias->row()->user_id;
        }
        //查無此人的個人履歷
        if(!$this->user_model->checkAlias($alias)){
            $data['title']='查無此id的個人履歷';
            $data['errorMessage']='查無此id的個人履歷';
            $this->theme_model->loadTheme('portfolio/errorPage',$data);
        //使用者本人的個人履歷
        }elseif($email==$this->session->userdata('email')){
        $data['getOneUser'] = $this->user_model->getOneUser($email)->row_array();
        //個人總積分
        $user_id = $this->session->userdata('user_id');
        $getTotalPoints = $this->points_model->getTotalPoints($user_id); 
        $data['getTotalPoints'] = $getTotalPoints->total;        
        $data['getPoints'] = json_encode($this->points_model->getPoints($user_id)->result());
        $getOneUser = $this->user_model->getOneUser($email);
        $getUserChapterBadge = $this->user_model->getUserChapterBadge($email);
        $data['profile'] = $getOneUser->row_array();
        $data['getUserChapterBadge'] = json_encode($getUserChapterBadge->result());
        $data['getUserExperience'] = json_encode($this->user_portfolio_model->getUserExperience($email)->result());
        $data['getUserAward'] = json_encode($this->user_portfolio_model->getUserAward($email)->result());
        $data['getUserLicense'] = json_encode($this->user_portfolio_model->getUserLicense($email)->result());
        $data['getUserPortfolio'] = json_encode($this->user_portfolio_model->getUserPortfolio($email)->result());
        $data['getUserSkill'] = json_encode($this->user_portfolio_model->getUserSkill($email)->result());
        $data['getUserEducation'] = json_encode($this->user_portfolio_model->getUserEducation($email)->result());
        $data['getUserSociety'] = $this->user_portfolio_model->getUserSociety($email)->row_array();
        $data['getUserTraining'] = json_encode($this->user_portfolio_model->getUserTraining($email)->result());
        $data['getPortfolioSkill'] = json_encode($this->user_portfolio_model->getPortfolioSkill($email)->result());
        $data['getCareerSkill'] = json_encode($this->user_portfolio_model->getCareerSkill($email)->result());
        $data['getJobsCategory'] = json_encode($this->tracks_model->getJobsCategory()->result());
        $data['getSeniority'] = json_encode($this->user_portfolio_model->getSeniority($email)->result());
        $this->theme_model->printTheme('portfolio/printPortfolio',$data);
        }
        // elseif(!$this->user_model->isResumeOpen($email)){
        //     $data['title']='此id的個人履歷未開放';
        //     $data['errorMessage']='此id的個人履歷未開放';
        //     $this->theme_model->loadTheme('portfolio/errorPage',$data);
        // //其他有開放的個人履歷
        // }
        else{
        // 使用此功能須符合
        // 1.廠商自己的求職文
        // 2.user有應徵該求職文 
            if($this->user_model->hrCheckUserPortfolio($this->session->userdata('user_id'),$user_id)){

            $data['getOneUser'] = $this->user_model->getOneUser($email)->row_array();
        
            $getTotalPoints = $this->points_model->getTotalPoints($user_id); 
            $data['getTotalPoints'] = $getTotalPoints->total;        
            $data['getPoints'] = json_encode($this->points_model->getPoints($user_id)->result());
            $getOneUser = $this->user_model->getOneUser($email);
            $getUserChapterBadge = $this->user_model->getUserChapterBadge($email);
            $data['profile'] = $getOneUser->row_array();
            $data['getUserChapterBadge'] = json_encode($getUserChapterBadge->result());
            $data['getUserExperience'] = json_encode($this->user_portfolio_model->getUserExperience($email)->result());
            $data['getUserAward'] = json_encode($this->user_portfolio_model->getUserAward($email)->result());
            $data['getUserLicense'] = json_encode($this->user_portfolio_model->getUserLicense($email)->result());
            $data['getUserPortfolio'] = json_encode($this->user_portfolio_model->getUserPortfolio($email)->result());
            $data['getUserSkill'] = json_encode($this->user_portfolio_model->getUserSkill($email)->result());
            $data['getUserEducation'] = json_encode($this->user_portfolio_model->getUserEducation($email)->result());
            $data['getUserSociety'] = $this->user_portfolio_model->getUserSociety($email)->row_array();
            $data['getUserTraining'] = json_encode($this->user_portfolio_model->getUserTraining($email)->result());
            $data['getPortfolioSkill'] = json_encode($this->user_portfolio_model->getPortfolioSkill($email)->result());
            $data['getCareerSkill'] = json_encode($this->user_portfolio_model->getCareerSkill($email)->result());
            $data['getJobsCategory'] = json_encode($this->tracks_model->getJobsCategory()->result());
            $data['getSeniority'] = json_encode($this->user_portfolio_model->getSeniority($email)->result());
            $this->theme_model->printTheme('portfolio/printPortfolio',$data);
            }else{
                $data['title']='此id未應徵該職缺';
                $data['errorMessage']='此id未應徵該職缺';
                $this->theme_model->loadTheme('portfolio/errorPage',$data);
            }
        }
    }

	/**
	 * 語系設定
	 * @param  [type] $lang [description]
	 * @return [type]       [description]
	 */
	public function locale($lang){
		
		$lang = strtolower($lang);
		$locale = array('locale'=>$lang);
		$this->session->set_userdata($locale);

		$first_set_locale = array('first_set_locale'=>"false");
		$this->session->set_userdata($first_set_locale);

		$this->lang->load('aioc',$lang);
		
		//導回上一頁
		$this->load->library("user_agent");
		redirect($this->agent->referrer());	  
	
	}

	//AI計畫能力檢核
	function ai_capability($alias){
		$data['title']='能力檢核';
        //檢查使用者是否登入
        $this->user_model->isLogin();
        //自己的profile
        //個人總積分
		$user_id = $this->session->userdata('user_id');
        if($alias==$this->session->userdata('alias')){
            $data['getOneUser'] = $this->user_model->getOneUser($this->session->userdata('email'))->row_array();
            $this->theme_model->loadTheme('profile/capability',$data);
        }else{
        	//別人的profile
        	redirect('home');
        }
	}

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */