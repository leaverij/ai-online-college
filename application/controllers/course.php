<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Course extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('course_model');
        $this->load->model('library_model');
        $this->load->model('chapter_model');
        $this->load->model('content_model');
    }

    function index()
	{
	}

    //課程建立頁面
    function create(){
        $this->user_model->isLogin();
        $data['title']='建立課程';
        $course_name = $this->input->post('name');
        $course_data = array('course_name'=>$course_name);
        $data['library'] = json_encode($this->library_model->getAllLibrary()->result());
        $data['category'] = json_encode($this->library_model->getAllCourseCategory()->result());
        $this->theme_model->loadTheme('course/new_course',$data);
    }

    //課程列表頁面
    function teach(){
        $this->user_model->isLogin();
        $data['title']='我教授的課程';
        $user_id = $this->session->userdata('user_id');
        $data['courses'] = json_encode($this->course_model->getTeachedCourses($user_id)->result());
        $this->theme_model->loadTheme('course/teached',$data);
    }

    //課程編輯基本資訊頁面
    function editCourseInfo($course_id){
        $this->user_model->isLogin();
        $data['title'] = '課程基本資訊';
        $user_id = $this->session->userdata('user_id');
        $data['course'] = json_encode($this->course_model->getTeachedCourse($user_id, $course_id)->row_array());
        $data['library'] = json_encode($this->library_model->getAllLibrary()->result());
        $data['category'] = json_encode($this->library_model->getAllCourseCategory()->result());
        $this->theme_model->loadTheme('course/edit_course_info', $data);
    }

    //課程編輯章節內容
    function editCourseContent($course_id){
        $this->user_model->isLogin();
        $data['title'] = '課程內容';
        $user_id = $this->session->userdata('user_id');
        $data['course'] = json_encode($this->course_model->getTeachedCourse($user_id, $course_id)->row_array());
        $chapters = $this->chapter_model->teacherGetChapters($course_id, $user_id);
        foreach($chapters->result_array() as $chapter){
            $chapter_id = $chapter['chapter_id'];
            $chapter['chapter_contents'] = $this->content_model->getOneChapterContentByChapterId($chapter_id)->result_array();
        }
        $data['chapters'] = json_encode($chapters->result());
        $this->theme_model->loadTheme('course/edit_course_content', $data);
    }

    //課程編輯公告
    function editCourseAnnouncement($course_id){
        $this->user_model->isLogin();
        $data['title'] = '課程公告';
        $user_id = $this->session->userdata('user_id');
        $data['course'] = json_encode($this->course_model->getTeachedCourse($user_id, $course_id)->row_array());
        $data['library'] = json_encode($this->library_model->getAllLibrary()->result());
        $data['category'] = json_encode($this->library_model->getAllCourseCategory()->result());
        $data['announcements'] = json_encode($this->course_model->getCourseAnnouncementsByTeacherId($course_id, $user_id)->result());
        $this->theme_model->loadTheme('course/edit_course_announcement', $data);
    }

    //課程編輯價格與優惠券
    function editCoursePrice($course_id){
        $this->user_model->isLogin();
        $data['title'] = '課程價格與優惠券';
        $user_id = $this->session->userdata('user_id');
        $data['course'] = json_encode($this->course_model->getTeachedCourse($user_id, $course_id)->row_array());
        $data['library'] = json_encode($this->library_model->getAllLibrary()->result());
        $data['category'] = json_encode($this->library_model->getAllCourseCategory()->result());
        $this->theme_model->loadTheme('course/edit_course_price', $data);
    }

    //課程學生列表
    function editCourseStudent($course_id){
        $this->user_model->isLogin();
        $data['title'] = '學生列表';
        $user_id = $this->session->userdata('user_id');
        $data['course'] = json_encode($this->course_model->getTeachedCourse($user_id, $course_id)->row_array());
        $data['library'] = json_encode($this->library_model->getAllLibrary()->result());
        $data['category'] = json_encode($this->library_model->getAllCourseCategory()->result());
        $data['student_list'] = $this->library_model->getAllCourseStudent($course_id)->result_array();
        $this->theme_model->loadTheme('course/edit_course_student', $data);
    }

    //直播教學
    function editCourseMeeting($course_id){
        $this->user_model->isLogin();
        $data['title'] = '直播教學';
        $user_id = $this->session->userdata('user_id');
        $data['course'] = json_encode($this->course_model->getTeachedCourse($user_id, $course_id)->row_array());
        $data['library'] = json_encode($this->library_model->getAllLibrary()->result());
        $data['category'] = json_encode($this->library_model->getAllCourseCategory()->result());
        $data['meetings'] = json_encode($this->course_model->getCourseMeetingsByTeacherId($course_id, $user_id)->result());
        $this->theme_model->loadTheme('course/edit_course_meeting', $data);
    }

    //學生我的課程頁面
    function learn(){
        $this->user_model->isLogin();
        $data['title']='我學習的課程';
        $user_id = $this->session->userdata('user_id');
        $data['courses'] = $this->course_model->getLearnedCourses($user_id);
        $this->theme_model->loadTheme('course/learn', $data);
    }

    //教師編輯課程內的學生分組
    function editCourseGroup($course_id){
        $this->user_model->isLogin();
        $data['title'] = '學生分組';
        $user_id = $this->session->userdata('user_id');
        $data['course'] = json_encode($this->course_model->getTeachedCourse($user_id, $course_id)->row_array());
        $data['library'] = json_encode($this->library_model->getAllLibrary()->result());
        $data['category'] = json_encode($this->library_model->getAllCourseCategory()->result());
        $data['content_groups'] = json_encode($this->course_model->getCourseStudentGroups($course_id, $user_id));
        $this->theme_model->loadTheme('course/edit_course_group', $data);
    }

    //教師編輯課程內學生分組細節
    function editCourseGroupStudent($course_id, $content_group_id){
        $this->user_model->isLogin();
        $data['title'] = '編輯學生分組';
        $user_id = $this->session->userdata('user_id');
        $data['course'] = json_encode($this->course_model->getTeachedCourse($user_id, $course_id)->row_array());
        $data['student_list'] = $this->course_model->getSingleCourseStudentGroupStatus($content_group_id, $course_id)->result_array();
        $data['content_group'] = json_encode($this->course_model->getSingleCourseStudentGroups($course_id, $user_id, $content_group_id)->result());
        $this->theme_model->loadTheme('course/edit_course_group_student', $data);
    }

}

?>