<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
		$this->load->model('notifications_model');
		$this->load->model('forum_subscription_model');
		$this->load->model('forum_model');
    }

	function index()
	{
		$data['title']='Notification';
		$this->user_model->isLogin();
		$page =  abs(round($this->input->get('page')));
		$total_rows =  $this->notifications_model->getNotification($this->session->userdata('email'))->num_rows();
		//每頁筆數
		$per_page = 5;
		$start = $per_page*($page-1);
		if($start<0) $start=0;
		if($page){
			$data['previous'] =$page-1;
			($total_rows <= $per_page*$page)?$data['next'] = '':$data['next'] = $page+1;
		}else{
			$data['previous'] = 0;
			($total_rows <= $per_page)?$data['next'] = '':$data['next'] = $page+2;
		}
		$data['getNotificationPagination']=json_encode($this->notifications_model->getNotificationPagination($start,$per_page,$this->session->userdata('email'))->result());
		$this->theme_model->loadTheme('notification/listAllNotification',$data);
	}
	//當用戶點擊head.php中的notification鈴鐺下拉式選單，將所有未讀的notification標註為已讀
	function markNotificationReaded(){
// 		$unread=explode(',',$this->security->xss_clean($this->input->post("unread")));
// 		print_r($unread);
// 		for($i=0;$i<count($unread);$i++){
// 			$this->notifications_model->updateNotification($unread[$i]);
// 		}
		$this->notifications_model->updateNotification($this->session->userdata('user_id'));
	}
	
	function subscribed_list(){
		$data['title']='Forum Subscribed List';
		$this->user_model->isLogin();
		$user_id=$this->session->userdata('user_id');
		$page =  abs(round($this->input->get('page')));
		$total_rows =  $this->forum_subscription_model->getUserForumSubscription($this->session->userdata('email'))->num_rows();
		//每頁筆數
		$per_page = 5;
		$start = $per_page*($page-1);
		if($start<0) $start=0;
		if($page){
			$data['previous'] =$page-1;
			($total_rows <= $per_page*$page)?$data['next'] = '':$data['next'] = $page+1;
		}else{
			$data['previous'] = 0;
			($total_rows <= $per_page)?$data['next'] = '':$data['next'] = $page+2;
		}
		$data['getUserForumEmailSubscription']=json_encode($this->forum_subscription_model->getUserForumEmailSubscription($this->session->userdata('email'))->result());
		$data['getSubscriptionPagination']=json_encode($this->forum_subscription_model->getSubscriptionPagination($start,$per_page,$user_id)->result());
		$this->theme_model->loadTheme('notification/ForumSubscribedList',$data);
	}
	
	function refreshUserNotifications(){
		$data['getNotification']=$this->notifications_model->getNotification($this->session->userdata('email'))->result();
		$data['getUnreadNotification']=$this->notifications_model->getUnreadNotification($this->session->userdata('email'));
		$notificationContent='';
		$notificationContent.='<div class="arrow"></div><h4 style="margin:0; padding:10px 0; border-bottom:1px solid #ededed; text-align:center;">通知訊息</h4>';
		if(empty($data['getNotification'])){
			$notificationContent.='<div style="text-align:center; color:#97a5a6; padding:15px;">您目前沒有任何的通知訊息</div>';
		}else{
			$i=0;
			$notificationContent.='<div class="scroll-mask notification-container" style="overflow:hidden;"><ul class="scrollable-menu" style="list-style:none; max-height:500px; margin-right:-17px;">';
			foreach($data['getNotification'] as $key=>$value){
				$notificationContent.='<li class="forum notification" style="list-style: none;">';
				$hyperlink='';
				if($data['getNotification'][$key]->notification_type==1 || $data['getNotification'][$key]->notification_type==2){
					$hyperlink=base_url().'forum/forumContent/'.$data['getNotification'][$key]->hyperlink;
				}
				$notificationContent.='<a href="'.$hyperlink.'" style="display:block; color:#333;">';
				$notificationContent.='<div class="row" style="line-height:15px;overflow:hidden;">';
				$notificationContent.='<div class="col-sm-8"><div class="notif-meta">';
				$notification_type='';
				if($data['getNotification'][$key]->notification_type==1){
					$notification_type='討論區回應';
				}else if($data['getNotification'][$key]->notification_type==2){
					$notification_type='新問題';
				}
				$notificationContent.='<p>'.$notification_type.'</p>';
				$notification_title=$data['getNotification'][$key]->notification_title;
				$notificationContent.='<h4>'.$notification_title.'</h4>';
				$alias=$data['getNotification'][$key]->alias;
				if($data['getNotification'][$key]->notification_type==1 || $data['getNotification'][$key]->notification_type==2){
					$reply_type='經由 ';
				}
				$notificationContent.='<p>';
				if($data['getNotification'][$key]->notification_type==1){
					$notificationContent.=' '.$reply_type.'<strong>'.$alias.'</strong> 回答</p>';
				}else if($data['getNotification'][$key]->notification_type==2){
					$notificationContent.=' '.$reply_type.'<strong>'.$alias.'</strong> 提問</p>';
				}
				$notificationContent.='<p class="time">'.$this->theme_model->calTimeElapsed($data['getNotification'][$key]->notification_time).'</p>';
				$notificationContent.='</div>';
				$notificationContent.='</div>';
				$notificationContent.='<div class="col-sm-4">';
				$img=base_url().'assets/img/user/128x128/'.$data['getNotification'][$key]->img;
				$notificationContent.='<div class="avatar" style="background-image: url('.$img.')">';
				$notificationContent.='<img src="'.$img.'">';
				$notificationContent.='</div>';
				$notificationContent.='</div>';
				$notificationContent.='</div>';
				$notificationContent.='</a>';
				$notificationContent.='</li>';
				if($data['getNotification'][$key]->is_read==0){
					$notification_id=$data['getNotification'][$key]->notification_id;
					$notificationContent.='<input type="hidden" name="unread[]" value="'.$notification_id.'">';
				}
				$i++;
			}
			$notificationContent.='</ul></div>';
		}
		$notificationContent.='<div style="border-top:1px solid #ededed; text-align:center;"><a href="'.base_url().'notifications" style="display:block; padding:10px 0;">檢視所有通知訊息</a></div>';
		echo $notificationContent;
	}
	//討論區訂閱按鈕
	function updateSubscriptionStatus(){
		$user_id=$this->session->userdata('user_id');
		$forum_topic_id=$this->input->post("forum_topic_id");
		$status=$this->input->post("status");
		//若資料表中沒有訂閱資料，須先insert一筆至資料庫中
		if($this->forum_subscription_model->isTopicSubscribed($user_id,$forum_topic_id)<1){
			$forumSubscription=array(
					'user_id'=>$user_id,
					'forum_topic_id'=>$forum_topic_id,
					'subscription_time'=>$this->theme_model->nowTime(),
					'status'=>$status
			);
			$this->forum_subscription_model->addForumSubscription($forumSubscription);
			echo '新增訂閱資料';
		//若資料表中已有訂閱資料，直接更新訂閱狀態
		}else{
			$forumSubscription=array(
					'status'=>$status,
					'subscription_time'=>$this->theme_model->nowTime()
			);
			$where=array(
					'user_id'=>$user_id,
					'forum_topic_id'=>$forum_topic_id
			);
			$this->forum_subscription_model->updateForumSubscription($forumSubscription,$where);
			echo '更新訂閱資料';
		}
	}
}

/* End of file notification.php */
/* Location: ./application/controllers/notification.php */