<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Release extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
		$this->load->model('user_model');
    }

	function index()
	{
		$data['title']='版本更新';
		$this->theme_model->loadTheme('release',$data);
	}
	function record($filename){
		$data['title']='webcam';
		$email=$this->session->userdata('email');
		$user_id = $this->session->userdata('user_id');
		if($this->user_model->isSuper($email)){
			$PATH = './assets/img/user/webcam/';
			if (!is_dir($PATH .$email)) mkdir($PATH .$email,0777);
			$data['email'] = $email;
			$data['filename'] = $filename;
			$pic = array( 
				'user_id' =>$user_id,
				'filename' => $filename, 
				'pic_date' => $this->theme_model->nowTime()
			);
			
			$this->user_model->saveWebCamPic($pic);
			$this->theme_model->loadTheme('record',$data);
		}
		
	}
}

/* End of file release.php */
/* Location: ./application/controllers/release.php */