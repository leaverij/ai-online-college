<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Track extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
		$this->load->model('tracks_model');
		$this->load->model('domain_model');
		$this->load->model('backend_model');
		$this->load->model('library_model');
		$this->load->model('pay_model');
    }

	function index()
	{
		$data['title']='依職務分類';
		$email = $this->session->userdata('email');
		//檢查使用者是否有訂閱學程地圖
		if($this->tracks_model->checkUserTracks($email)){
			$data['getAllTracks']=($this->tracks_model->getUserTracks($email));
		}else{
			$data['getAllTracks']=($this->tracks_model->getAllTracks());
		}
		$this->theme_model->loadTheme('track/listAllTrack',$data);
	}
	
	function trackContent($tracks_url)
	{
		$data['title']='學程';
		//取得線上修學程地圖的人數
		$getTracksPeopleNo = $this->tracks_model->getTracksPeopleNo($tracks_url)->row();
		$data['getTracksPeopleNo'] = $getTracksPeopleNo->count;
		//使用者登入。
		$email = $this->session->userdata('email');
		$user_id = $this->session->userdata('user_id');
		$trackContentQuery = $this->tracks_model->getTracksContent($tracks_url);
		$getTracksContent=json_encode($trackContentQuery->result());
		if(!json_decode($getTracksContent, true)){
		//$tracks_url 有誤時或是無資料時，會導到錯誤頁面
			$getJobsCategoryUrl = $this->tracks_model->getJobsCategoryUrl('ios-app-development')->row();
			redirect('page/message/track/'.$getJobsCategoryUrl->jobs_category_url);
		}
		$getJobsCategoryUrl = $this->tracks_model->getJobsCategoryUrl($tracks_url)->row();
		$data['jobs_category_url'] = $getJobsCategoryUrl->jobs_category_url;
		$categoryName = $this->backend_model->getCategoryName($getJobsCategoryUrl->jobs_category_url)->row();
		$data['getCategoryName'] = $categoryName->jobs_category_name;
		$tracks_query= $this->tracks_model->getTracksId($tracks_url)->row();
		$tracks_id = $tracks_query->tracks_id;
		$data['getTracksPrice'] = $this->tracks_model->getTracksPrice($tracks_id)->result();
		//檢查使用者是否購買學程
		$data['checkBuyTracks'] = $this->pay_model->checkBuyTracks($tracks_url)->row();
		//if($email){
			//$tracks_query= $this->tracks_model->getTracksId($tracks_url)->row();
			//$tracks_id = $tracks_query->tracks_id;
			//新增訂閱學程之紀錄
			//$userTrackData = array ( 'user_id' => $user_id,
							//'learning_status' => '1',
							//'tracks_id' =>$tracks_id,
							//'tracks_date'=> $this->theme_model->nowTime());
			//$this->tracks_model->addUserTracks($userTrackData);
		//}else{
			//沒有登入的狀況，不作任何改變
		//}
		$var=array();
		//為了要對應到tracks裡學習過的chapter所以要取得使用者學習過的chapter
		$data['getUserTracksChapter'] = $this->tracks_model->getUserTracksChapter($user_id,$tracks_url)->result();
		$data['getTrackProgress'] = $this->tracks_model->getTrackProgress($tracks_url,$user_id)->result();
		foreach( $data['getTrackProgress'] as $val){
			array_push($var,$val->course_pass);
		}
		
		$data['progress'] = $this->progress($var);
		$data['getTrackContent'] = json_encode($this->tracks_model->getTrackContent($tracks_url),true);
		$data['getTrackTeacher'] = json_encode($this->tracks_model->getTrackTeacher($tracks_url)->result(),true);
		//檢查使用者是否有挑戰任何學程
		$checkLearningTracks = $this->tracks_model->checkLearningTracks($user_id);
		//檢查是否為正在挑戰的學程
		$checkLearningStatus = $this->tracks_model->checkLearningStatus($tracks_url,$user_id);
		if ($checkLearningTracks->num_rows() > 0){
			$row = $checkLearningStatus->row();
   			if(!empty($row)&&$row->learning_status=='1'){
   				//正在挑戰學程
   				$data['learning_status'] = '1';
   			}else{
   				//有挑戰學程，但不是現在這個
   				$data['learning_status'] = '2';
   			}
		}else{
			//沒有挑戰任何學程
			$data['learning_status'] = '0';
		}		
		$this->theme_model->loadTheme('track/trackContent',$data);
	}
	//職務分類的核心部分
	function domain($jobs_category_url){
		//sidebar資料
		$data['getAllJobsCategory']=json_encode($this->library_model->getAllJobsCategory()->result());
		$data['getAll'] = json_encode($this->library_model->getAll()->result());
		$row = $this->library_model->checkJobsCategory($jobs_category_url);
		if($row){
			$data['title']= $jobs_category_url;
			$data['jobs_category_url'] = $jobs_category_url;
			$email = $this->session->userdata('email');
			//檢查使用者是否有訂閱學程地圖
			//if($this->tracks_model->checkUserTracks($email)){
				//$data['getAllTracks']=($this->domain_model->getTracksByDomainAndUser($domain_url,$email));
			//}
			if($email){
				$tracks_id = array();
				$passAmount = array();
				$getStatusByUser=$this->library_model->getStatusByUser($jobs_category_url,$email);
				foreach ($getStatusByUser->result_array() as $row)
					{
						array_push($tracks_id,$row['tracks_id']);
						array_push($passAmount,$row['passAmount']);
					}
				$data['tracks_id']= $tracks_id;
				$data['passAmount']= $passAmount;
				$data['getSubscribeTrack']=$this->tracks_model->getSubscribeTrack($email)->row();
			}
			//未登入狀態
			$data['getAllTracks']=$this->library_model->getTracksByCategory($jobs_category_url);
			//趨勢應用
			$data['listIndustryByJobsCategory'] = $this->backend_model->listIndustryByJobsCategory($jobs_category_url);
			//相關活動
			$data['listSeminarByJobsCategory'] = $this->backend_model->listSeminarByJobsCategory($jobs_category_url);
			//企業徵才
			$data['listJobsByJobsCategory'] = $this->backend_model->listJobsByJobsCategory($jobs_category_url);
			//國際資源
			$data['listResourceByJobsCategory'] = $this->backend_model->listResourceByJobsCategory($jobs_category_url);
			//熱門討論
			$data['listPopularDiscussions'] = $this->backend_model->listPopularDiscussions($jobs_category_url);
			//最新討論
			$data['listNewDiscussions'] = $this->backend_model->listNewDiscussions($jobs_category_url);
			
			//以下facebook
		$this->load->library('facebook');
        $user = $this->facebook->getUser();
		////
		if(!$this->session->userdata('user_id')){
		if ($user) {
		  try {
		    // Proceed knowing you have a logged in user who's authenticated.
		    $user_profile = $this->facebook->api('/me');
		    //set_fb_session
		    $fb_data = array(  			
			'fb_login'  => '1',
			'fb_logout_url' => $this->facebook->getLogoutUrl(array('next' => base_url().'home/logout'))
			);
			$this->session->set_userdata($fb_data);
			
		  } catch (FacebookApiException $e) {
		    error_log($e);
		    $user = null;
		    //unset_fb_session
		    $this->session->unset_userdata('fb_login');
		    $this->session->unset_userdata('fb_logout_url');
		  }
		}
		if ($user):
            //$data['logout_url'] = $this->facebook->getLogoutUrl();
        	$check_email = $this->user_model->checkFbEmail($user_profile['email']);
			//get alias
			$alias = strstr($user_profile['email'], '@', true);
			$i = 10;
			while ($i > 0) {
				$i--;
				$checkalias = $this->user_model->checkAlias($alias);
				if($checkalias=='1'){
					$alias = $alias.rand(1,20);
					$checkalias = $this->user_model->checkAlias($alias);
					if($checkalias=='0'){
						break;
					}
				}
			}
			$now = $this->theme_model->nowTime();
			$row = $check_email->row();
			$count = $row->count;
			$count = $count+1;
			if($check_email->num_rows()==0)://資料庫無資料_新用戶 
				$fb_insert_data = array(	
					'name' => $user_profile['name'], 
					'email' => $user_profile['email'],
					'alias' => $alias,
					'last_login' => $now,
					'register_date' => $now,
					'fb_id' => $user_profile['id'],
					'active_flag'=>'1');
				$this->user_model->fb_insert_new($fb_insert_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				//設定session
				$this->theme_model->set_session($user_profile['email']);
				//提示訊息
				//$this->ym->flash_session("alert-success",'Facebook註冊成功，歡迎您');	
				redirect('track/domain/'.$jobs_category_url);
			elseif($row->email==$user_profile['email']&&$row->fb_id=='')://資料庫有mail，但尚未綁定fb帳號
				$fb_update_data=array(
					'fb_id'=>$user_profile['id'],
					'last_login'=> $now,
					'count' =>$count,
					'active_flag'=>'1');
				$this->user_model->fb_update($user_profile['email'],$fb_update_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				//設定session
				$this->theme_model->set_session($user_profile['email']);
				//提示訊息
				//$this->ym->flash_session("alert-success","成功綁定 email ");	
				redirect('track/domain/'.$jobs_category_url);
			elseif($row->email==$user_profile['email']&&$row->fb_id==$user_profile['id']):
				$fb_update_data=array(
					'last_login'=> $now,
					'count' =>$count);
				$this->user_model->fb_update($user_profile['email'],$fb_update_data);
				//存取FB照片
				$this->user_model->fb_pic_save($user_profile['id']);
				$this->theme_model->set_session($user_profile['email']);
				redirect('track/domain/'.$jobs_category_url);
			endif;
        else:
            $data['login_url'] = $this->facebook->getLoginUrl(array('scope' => 'email'));
        endif;
		}
			
			$this->theme_model->loadTheme('track/listJobsCategory',$data);
		}else{
			echo 'noData';
		}
	}
	//更多趨勢應用
	function industry($jobs_category_url)
	{
		$data['title']='more';
		//檢查使用者是否登入
        $this->user_model->isLogin();
		$row = $this->library_model->checkJobsCategory($jobs_category_url);
		if($row){
			//頁碼
			$page =  abs(round($this->input->get('page')));
			$data['jobs_category_url'] = $jobs_category_url;
			$categoryName = $this->backend_model->getCategoryName($jobs_category_url)->row();
			$data['getCategoryName'] = $categoryName->jobs_category_name;
			//使用者做搜尋的動作時
			if($this->input->post("action")=='search'){
				$search_string = $this->security->xss_clean($this->input->post('search_string'));
				$data['listAllIndustryByCategory'] = $this->backend_model->listIndustryBySearchCategory($jobs_category_url,$search_string);
				$data['search_string'] = $search_string;
			}else{
				//總筆數
				$rows =  $this->backend_model->listAllIndustryCount($jobs_category_url)->row();
				$total_rows = $rows->count;
				//每頁筆數
				$per_page = 10;
				//第一頁
				$data['first'] = '';
				//最後一頁 
				$data['last'] = ceil($total_rows/$per_page);
				//當前頁數
				$data['page'] = $page;
				$start = $per_page*($page-1);
				if($start<0) $start=0;
				//page有值時
				if($page){
					$data['previous'] =$page-1;
					if($total_rows <= $per_page*$page){
						$data['next'] = '';	
					}else{
						$data['next'] = $page+1;
					}
				//page無值時
				}else{
					$data['previous'] = 0;
					if($total_rows <= $per_page*$page){
						$data['next'] = '';	
					}else{
						$data['next'] = $page+2;
					}
				}
				$data['listAllIndustryByCategory'] = $this->backend_model->listAllIndustryByCategory($jobs_category_url,$start,$per_page);
			}
			$this->theme_model->loadTheme('track/moreIndustry',$data);
		}else{
			echo 'noData';
		}
	}
	//更多企業徵才
	function jobs($jobs_category_url){
		$data['title']='more';
		//檢查使用者是否登入
        $this->user_model->isLogin();
		$row = $this->library_model->checkJobsCategory($jobs_category_url);
		if($row){
			//頁碼
			$page =  abs(round($this->input->get('page')));
			$data['jobs_category_url'] = $jobs_category_url;
			$categoryName = $this->backend_model->getCategoryName($jobs_category_url)->row();
			$data['getCategoryName'] = $categoryName->jobs_category_name;
			//使用者做搜尋的動作時
			if($this->input->post("action")=='search'){
				$search_string = $this->security->xss_clean($this->input->post('search_string'));
				$data['listAllJobsByCategory'] = $this->backend_model->listJobsBySearchCategory($jobs_category_url,$search_string);
				$data['search_string'] = $search_string;
			}else{
				//總筆數
				$rows =  $this->backend_model->listAllJobsCount($jobs_category_url)->row();
				$total_rows = $rows->count;
				//每頁筆數
				$per_page = 10;
				//第一頁
				$data['first'] = '';
				//最後一頁 
				$data['last'] = ceil($total_rows/$per_page);
				//當前頁數
				$data['page'] = $page;
				$start = $per_page*($page-1);
				if($start<0) $start=0;
				//page有值時
				if($page){
					$data['previous'] =$page-1;
					if($total_rows <= $per_page*$page){
						$data['next'] = '';	
					}else{
						$data['next'] = $page+1;
					}
				//page無值時
				}else{
					$data['previous'] = 0;
					if($total_rows <= $per_page*$page){
						$data['next'] = '';	
					}else{
						$data['next'] = $page+2;
					}
				}
				$data['listAllJobsByCategory'] = $this->backend_model->listAllJobsByCategory($jobs_category_url,$start,$per_page);
			}
			
			$this->theme_model->loadTheme('track/moreJobs',$data);
		}else{
			echo 'noData';
		}
	}
	//更多相關活動
	function seminar($jobs_category_url)
	{
		$data['title']='more';
		//檢查使用者是否登入
        $this->user_model->isLogin();
		$row = $this->library_model->checkJobsCategory($jobs_category_url);
		if($row){
			//頁碼
			$page =  abs(round($this->input->get('page')));
			$data['jobs_category_url'] = $jobs_category_url;
			$categoryName = $this->backend_model->getCategoryName($jobs_category_url)->row();
			$data['getCategoryName'] = $categoryName->jobs_category_name;
			$data['seminar_name'] = $this->seminarType($this->uri->segment(4));
			//使用者做搜尋的動作時
			if($this->input->post("action")=='search'){
				$search_string = $this->security->xss_clean($this->input->post('search_string'));
				$search_string=mysql_real_escape_string($search_string);
				$this->session->set_userdata('search_string',$search_string);
				$data['search_string'] = $search_string;
				//依照資源類型 影片文件文章其他
				if($this->uri->segment(4) && is_numeric($this->uri->segment(4))){
					//$data['listAllSeminarByDomain'] = $this->backend_model->listAllSeminarByDomainAndSeminar($domain_url,$this->uri->segment(4));
					$data['listAllSeminarByCategory'] = $this->backend_model->listAllSeminarByCategoryAndSeminar($jobs_category_url,$this->uri->segment(4));
				//無分類
				}else{
					//$data['listAllSeminarByDomain'] = $this->backend_model->listSearchSeminarByDomain($domain_url);
					$data['listAllSeminarByCategory'] = $this->backend_model->listSearchSeminarByCategory($jobs_category_url);
				}
			}else{
				//pjax 依照資源類型 college/moreresource/mobile/1
				if($this->uri->segment(4) && is_numeric($this->uri->segment(4))){
					$data['search_string'] = $this->session->userdata('search_string');
					$data['listAllSeminarByCategory'] = $this->backend_model->listAllSeminarByCategoryAndSeminar($jobs_category_url,$this->uri->segment(4));
				//無分類時  college/moreresource/mobile
				}else{
					if($this->session->userdata('search_string')){
						$data['search_string'] = $this->session->userdata('search_string');
						//$data['listAllResourceByDomain'] = $this->backend_model->listSearchSeminarByDomain($domain_url);
						$data['listAllSeminarByCategory'] = $this->backend_model->listSearchSeminarByCategory($jobs_category_url);
					}else{
						//總筆數
						$rows =  $this->backend_model->listAllSeminarCount($jobs_category_url)->row();
						$total_rows = $rows->count;
						//每頁筆數
						$per_page = 10;
						//第一頁
						$data['first'] = '';
						//最後一頁 
						$data['last'] = ceil($total_rows/$per_page);
						//當前頁數
						$data['page'] = $page;
						$start = $per_page*($page-1);
						if($start<0) $start=0;
						//page有值時
						if($page){
							$data['previous'] =$page-1;
							if($total_rows <= $per_page*$page){
								$data['next'] = '';	
							}else{
								$data['next'] = $page+1;
							}
						//page無值時
						}else{
							$data['previous'] = 0;
							if($total_rows <= $per_page*$page){
								$data['next'] = '';	
							}else{
								$data['next'] = $page+2;
							}
						}
						$data['listAllSeminarByCategory'] = $this->backend_model->listAllSeminarByCategory($jobs_category_url,$start,$per_page);
					}
				}
			}
			$this->theme_model->loadTheme('track/moreSeminar',$data);
		}else{
			echo 'input error';
		}
	}
	//更多國內外資源
	function resource($jobs_category_url)
	{
		$data['title']='more';
		//檢查使用者是否登入
        $this->user_model->isLogin();
		$row = $this->library_model->checkJobsCategory($jobs_category_url);
		if($row){
			//頁碼
			$page =  abs(round($this->input->get('page')));
			$data['jobs_category_url'] = $jobs_category_url;
			$categoryName = $this->backend_model->getCategoryName($jobs_category_url)->row();
			$data['getCategoryName'] = $categoryName->jobs_category_name;
			$data['resource_type'] = $this->resourceType($this->uri->segment(4));
			//點擊搜尋表單的時候
			if($this->input->post("action")=='search'){
				$search_string = $this->security->xss_clean($this->input->post('search_string'));
				$search_string=mysql_real_escape_string($search_string);
				$this->session->set_userdata('search_string',$search_string);
				$data['search_string'] = $search_string;
				//依照資源類型 影片文文章其他
				if($this->uri->segment(4) && is_numeric($this->uri->segment(4))){
					//$data['listAllResourceByDomain'] = $this->backend_model->listAllResourceByDomainAndResource($domain_url,$this->uri->segment(4));
					$data['listAllResourceByDomain'] = $this->backend_model->listAllResourceByCategoryAndResource($jobs_category_url,$this->uri->segment(4));
				//無分類
				}else{
					//$data['listAllResourceByDomain'] = $this->backend_model->listSearchResourceByDomain($domain_url);
					$data['listAllResourceByDomain'] = $this->backend_model->listSearchResourceByCategory($jobs_category_url);
				}
			}else{
				//透過pjax搜尋時以及點擊國內外資源進來的時候
				//pjax 依照資源類型 college/moreresource/mobile/1
				if($this->uri->segment(4) && is_numeric($this->uri->segment(4))){
					$data['search_string'] = $this->session->userdata('search_string');
					//$data['listAllResourceByDomain'] = $this->backend_model->listAllResourceByDomainAndResource($domain_url,$this->uri->segment(4));
					$data['listAllResourceByDomain'] = $this->backend_model->listAllResourceByCategoryAndResource($jobs_category_url,$this->uri->segment(4));
				//點擊sidebar的國內外資源
				}else{
					if($this->session->userdata('search_string')){
						$data['search_string'] = $this->session->userdata('search_string');
						//$data['listAllResourceByDomain'] = $this->backend_model->listSearchResourceByDomain($domain_url);
						$data['listAllResourceByDomain'] = $this->backend_model->listSearchResourceByCategory($jobs_category_url);
					}else{
						//總筆數
						$rows =  $this->backend_model->listAllResourceCount($jobs_category_url)->row();
						$total_rows = $rows->count;
						//每頁筆數
						$per_page = 10;
						//第一頁
						$data['first'] = '';
						//最後一頁 
						$data['last'] = ceil($total_rows/$per_page);
						//當前頁數
						$data['page'] = $page;
						$start = $per_page*($page-1);
						if($start<0) $start=0;
						//page有值時
						if($page){
							$data['previous'] =$page-1;
							if($total_rows <= $per_page*$page){
								$data['next'] = '';	
							}else{
								$data['next'] = $page+1;
							}
						//page無值時
						}else{
							$data['previous'] = 0;
							if($total_rows <= $per_page*$page){
								$data['next'] = '';	
							}else{
								$data['next'] = $page+2;
							}
						}
						//$data['listAllResourceByDomain'] = $this->backend_model->listAllResourceByDomain($domain_url,$start,$per_page);
						$data['listAllResourceByDomain'] = $this->backend_model->listAllResourceByCategory($jobs_category_url,$start,$per_page);
					}
					
				}
			}
			$this->theme_model->loadTheme('track/moreResource',$data);
		}else{
			echo 'noData';
		}
	}
	function seminarType($type){
		switch ($type){
			case "1": return "研討會";break;
			case "2": return "競賽";	break;
			case "3": return "展覽";	break;
			case "4": return "其他";	break;
			default:  return "依活動類型";
		}
	}
	function resourceType($type){
		switch ($type){
			case "1": return "影片"; break;
			case "2": return "文件"; break;
			case "3": return "文章"; break;
			case "4": return "其他"; break;
			default:  return "依資源類型";
		}
	}
	
	//使用課程狀態參數取得使用者上課進度
	function progress($var){
		$pass = array();
		$key='0';
		for($i=0;$i<count($var);$i++){
			if($var['0']=='0'){
				if($i=='0' && $var[$i]=='0'){
					array_push($pass,'in-progress');
				}elseif($var['0']=='0' && $var[$i]=='0'){
					array_push($pass,'keep-go');
				}elseif($var['0']=='0' && $var[$i]=='1'){
					array_push($pass,'progress-complete-unordered');
				}
			}elseif($var['0']=='1'){
				if($i=='0' && $var[$i]=='1'){
					array_push($pass,'progress-complete');
				}elseif($key=='0' && $var['0']=='1' && $var[$i]=='0' && $var[$i-1]=='1'){
					array_push($pass,'in-progress');
					$key='1';
				}elseif($key=='1' && $var['0']=='1' && $var[$i]=='0'){
					array_push($pass,'keep-go');
				}elseif($key=='1' && $var[$i]=='1'){
					array_push($pass,'progress-complete-unordered');
				}elseif($var[$i]=='1' && $var[$i-1]=='1'){
					array_push($pass,'progress-complete');
				}
			}
		}
		return $pass;
	}
}

/* End of file track.php */
/* Location: ./application/controllers/track.php */
