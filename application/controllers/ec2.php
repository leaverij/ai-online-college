<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EC2 extends CI_Controller {
	
	function __construct()
    {
		parent::__construct();
		$this->load->model('jupyter_model');
		$this->load->model('content_model');
		$this->load->library('ec2_create');
    }

	// 新建instance
	function createInstance(){
		$course_id = $this->input->get('courseId', TRUE);
		$chapter_id = $this->input->get('chapterId', TRUE);
		$chapter_content_id = $this->input->get('chapterContentId', TRUE);
		$content = $this->content_model->getContentById($chapter_id, $chapter_content_id);
		$s3_url = null;
		$lab_duration = 60;
		if(!empty($content)){
			if($content['content']!=''){
				$s3_url = $content['content'];
			}
			if($content['time_spent']){
				$lab_duration = $content['time_spent'];
			}
		}
		$result = $this->ec2_create->createInstance($chapter_id, $chapter_content_id, $s3_url,$lab_duration);
		$instanceid = $result['id'];
		$dnsName = $result['dns'];
		$result = $this->jupyter_model->insertData($instanceid, $dnsName, $course_id, $chapter_id, $chapter_content_id);
		if($result===true){
			$return = array('status'=>'success', 'message'=>'LAB 建立完成', 'url'=>$dnsName, 'id'=>$instanceid);
			echo json_encode($return);
		}else{
			$return = array('status'=>'failure', 'message'=>'LAB 建立失敗');
			echo json_encode($return);
		}
	}

	// 取得instance的dns位置
	function getInstanceDns(){
		$result = $this->jupyter_model->getData();
		print_r($result);
	}

	// 管理instance(開/關)
	function ManageInstance(){
		$instanceid = $this->input->get('instanceid', TRUE);
		$action = $this->input->get('action', TRUE);
		if(empty($instanceid) || empty($action)){
			echo json_encode(array('status'=>'failure', 'message'=>'Required missing!'));
		}else{
			if($action!='START' && $action!='STOP'){
				echo json_encode(array('status'=>'failure', 'message'=>'Status wrong!'));
			}else{
				$result = $this->ec2_create->ManageInstance($instanceid, $action);
				if($action=='START' && $result['status']=='success'){
					$dns = $result['dns'];
					$this->jupyter_model->updatedns($instanceid, $dns);
					$this->jupyter_model->updatestatus($instanceid, 1);
				}else if($action=='STOP' && $result['status']=='success'){
					$this->jupyter_model->updatestatus($instanceid, 0);
					$this->jupyter_model->updateperiod($instanceid);
				}
				echo json_encode($result);
			}
		}
	}
}