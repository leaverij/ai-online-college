<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chapter extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('course_model');
        $this->load->model('chapter_model');
        $this->load->model('content_model');
    }

    function index()
	{
    }
    
    //編輯課程章節內容
    function editChapterContent($course_id, $target_chapter_id){
        $data['title'] = '編輯課程章節內容';
        $user_id = $this->session->userdata('user_id');
        $data['course'] = json_encode($this->course_model->getTeachedCourse($user_id, $course_id)->row_array());
        $chapters = $this->chapter_model->teacherGetChapters($course_id, $user_id);
        $target_chapter = null;
        foreach($chapters->result_array() as $chapter){
            $chapter_id = $chapter['chapter_id'];
            $chapter['chapter_contents'] = $this->content_model->getOneChapterContentByChapterId($chapter_id)->result_array();
            if($target_chapter_id==$chapter_id){
                $target_chapter = $chapter;
            }
        }
        $data['chapters'] = json_encode($chapters->result());
        $data['chapter'] = json_encode($target_chapter);
        $data['content_groups'] = json_encode($this->course_model->getCourseStudentGroups($course_id, $user_id));
        $this->theme_model->loadTheme('chapter/editChapterContent', $data);
    }

}

?>