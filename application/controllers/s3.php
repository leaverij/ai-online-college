<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class S3 extends CI_Controller {
	
	function __construct()
    {
		parent::__construct();
		$this->load->model('content_model');
	}
	
	function index(){
		echo "s3 controller";
	}

	function uploadFiles(){
		$this->load->library('s3_upload');
		$course_id = $this->input->post('courseId', TRUE);
		$chapter_id = $this->input->post('chapterId', TRUE);
		$fileUUID = $this->input->post('fileUUID', TRUE);
		$teacher_id = $this->input->post('teacherId', TRUE);
		$file_name = $this->input->post('fileName', TRUE);
		$fileExt = $this->input->post('fileExt', TRUE);
		$file_type = $this->input->post('fileType', TRUE);
		$fileUUIDName = $fileUUID.'.'.$fileExt;
		$pathToFile = $_FILES['file']['tmp_name'];
		$s3_url = $this->s3_upload->uploadS3($pathToFile, $fileUUID, $file_name, $course_id, $chapter_id, $file_type);
		if($s3_url){
			$return = array(
				'status'=>'success', 
				'message'=>'文件上傳完成', 
				's3_url'=>$s3_url,
				's3_uuid'=>$fileUUIDName,
				's3_name'=>$file_name
			);
			echo json_encode($return);
		}else{
			$return = array('status'=>'failure', 'message'=>'文件以上傳失敗');
			echo json_encode($return);
		}
	}

	function getBuckets(){
		$this->load->library('s3_get');
		$this->s3_get->getBuckets();
	}

	function getBucketObjects($bucket){
		$this->load->library('s3_get');
		$this->s3_get->getBucketObjects($bucket);
	}

	function getBucketFile($bucket){
		$this->load->library('s3_get');
		$file = $this->input->get("file");
		$this->s3_get->getBucketLogFile($bucket, $file);
	}

}
/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */