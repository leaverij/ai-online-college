<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Player_test extends CI_Controller {
	
	function __construct()
    {
        parent::__construct();
    }

	function index()
	{
		$data['title']='Flowplayer Test';
		$this->theme_model->loadTheme('fp_test',$data);
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */