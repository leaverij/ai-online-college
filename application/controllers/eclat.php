<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eclat extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('eclat_model');
    }

    function index(){
        $data['title'] = "儒鴻轉檔";
        $data['files'] = json_encode($this->eclat_model->listEclatFiles()->result());
        $this->load->view('eclat/index', $data);
    }

    function uploadFiles(){
        $fileName=$this->security->xss_clean($this->input->post('fileName'));
        $uploadDir = APPPATH."../upload/";
        $uploadFile = $uploadDir.$fileName;
        if(!file_exists(dirname($uploadFile))) mkdir(dirname($uploadFile), 0777, true);
        $new_eclat_id;
        $upload_date = mdate("%Y-%m-%d %H:%i:%s",time());
        if(move_uploaded_file($_FILES['file']['tmp_name'], iconv("utf-8", "big5", $uploadFile))){
            $eclat_data = array(
                'encode_name'=>md5($fileName),
                'name'=>$fileName,
                'upload_date'=>$upload_date
            );
            $new_eclat_id = $this->eclat_model->insertEclatRecord($eclat_data);
        }
        $return = array(
            'success'=>true,
            'id'=>$new_eclat_id,
            'name'=>$fileName,
            'upload_date'=>$upload_date
        );    
        header('Content-Type: application/json');
        echo json_encode($return);
    }

    function transFile(){
        //$uploadFile = $uploadDir.$fileName;
        $fileName=$this->security->xss_clean($this->input->post('name'));
        //執行python來轉檔
        /*
        $result = system("cd /opt/lampp/htdocs/ai-online-college/upload;/usr/bin/python3 tran.py LF70051.txt /opt/lampp/htdocs/ai-online-college/assets/eclat/lebron 2>&1");
        var_dump($result);
        */
        $command = "cd /opt/lampp/htdocs/ai-online-college/upload;/usr/bin/python3 tran.py ".$fileName.".txt /opt/lampp/htdocs/ai-online-college/assets/eclat/".$fileName;
        system($command);
        //var_dump($result);
        header('Content-Type: text/plain');
    }

}