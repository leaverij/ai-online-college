<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['protocol'] 	= 'smtp';
$config['smtp_port'] 	= '25';
$config['smtp_host'] 	= 'mta3.iii.org.tw';
$config['smtp_timeout'] = '5';
$config['mailtype'] 	= 'html';
$config['newline'] 		= "rn";
$config['crlf'] 		= "rn";