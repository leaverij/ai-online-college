<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';
$route['library'] = "field";
$route['library/(:any)'] = "field/domain/$1";
//$route['domain/(:any)/more-seminar'] = "college/moreSeminar/$1";
// $route['domain/(:any)/more-industry'] = "college/moreIndustry/$1";
// $route['domain/(:any)/more-resource'] = "college/moreResource/$1";
// $route['domain/(:any)/more-jobs'] = "college/moreJobs/$1";
//$route['college/(:any)'] = "college/domain/$1";
$route['profile/(:any)'] = "user/profile/$1";
$route['resume/(:any)'] = "user/resume/$1";
/* End of file routes.php */
/* Location: ./application/config/routes.php */

/* 課程公告 */
$route['course/edit/(:num)/announcement'] = "course/editCourseAnnouncement/$1";
/* 課程編輯 */
$route['course/edit/(:num)/info'] = "course/editCourseInfo/$1";
$route['course/edit/(:num)/content'] = "course/editCourseContent/$1";
/* 章節編輯 */
$route['course/edit/(:num)/content/chapter/(:num)'] = "chapter/editChapterContent/$1/$2";
/* 課程價格與優惠券 */
$route['course/edit/(:num)/price'] = "course/editCoursePrice/$1";
/* 學生列表 */
$route['course/edit/(:num)/student'] = "course/editCourseStudent/$1";
/* 直播教學 */
$route['course/edit/(:num)/meeting'] = "course/editCourseMeeting/$1";
/* datasets下的檔案內容 */
$route['(:any)/datasets/(:any)/(:any)'] = "datasets/singleFile/$1/$2/$3";
/* datasets */
$route['(:any)/datasets/(:any)'] = "datasets/listAll/$1/$2";
/* 學生分組 */
$route['course/edit/(:num)/group']  = "course/editCourseGroup/$1";

/* 競賽 */
/* 基本資訊 */
$route['competition/edit/(:num)/info'] = "competitions/editCompetitionInfo/$1";
/* 說明 */
$route['competition/edit/(:num)/detail'] = "competitions/editCompetitionDetail/$1";

