<?php
class User_chapter_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

	/*	$user_chapter=array('user_course_id'=>$user_course_id,
	 *						'chapter_id'	=>$chapter_id);
	 */
	function addUserChapter($user_chapter){
		$sql="insert into user_chapter (user_course_id,chapter_id,create_time) values (?,?,?)";
		$user_chapter['create_time']=$this->theme_model->nowTime();
		$query=$this->db->query($sql,$user_chapter);
		return $this->db->affected_rows();
	}
	
	/*	$option=array(	'user.email'			=>$email,
	 *					'course.course_url'		=>$course_url,
	 *					'user_chapter.status'	=>'1');			//0:Not Finished 1:Finished
	 */
	function getUserChapter($option){
		$this->db->select('*')
		->from('user_chapter')
		->join('user_course','user_chapter.user_course_id=user_course.user_course_id','left')
		->join('course','user_course.course_id=course.course_id','left')
		->join('user','user_course.user_id=user.user_id','left')
		->join('chapter','user_chapter.chapter_id=chapter.chapter_id','left')
		->where($option);
		$query=$this->db->get();
		return $query;
	}
	
	//判斷user是否曾看過章節下的影片
	function isChapterSubscribed($user_chapter){
		$query=$this->db->get_where('user_chapter',$user_chapter);
		return $query->num_rows();
	}
	
	//回傳使用者指定課程的徽章數量
	/*	$option=array(	'user.email'			=>$email,
	 *					'course.course_url'		=>$course_url,
	 *					'user_chapter.status'	=>'1');			//0:Not Finished 1:Finished
	 */
	function getUserCourseBadges($option){
		$this->db->select('*')
		->from('user_chapter')
		->join('user_course','user_chapter.user_course_id=user_course.user_course_id','left')
		->join('user','user_course.user_id=user.user_id','left')
		->join('course','user_course.course_id=course.course_id','left')
		->where($option);
		$query=$this->db->get();
		return $query->num_rows();
	}
	
	function getUserChapterId($user_chapter){
		$sql='select user_chapter_id from user_chapter where user_course_id=? and chapter_id=?';
		$query=$this->db->query($sql,$user_chapter);
		return $query->result();
	}
	
	//修改使用者關卡進度的狀態
	function updateUserChapterStatus($email,$chapter_content_url){
		$accomplish_time=$this->theme_model->nowTime();
		$sql="	update user_chapter 
				set status=1 ,
				accomplish_time = '$accomplish_time'
				where user_course_id = (	select user_course_id 
											from user_course 
											where user_id =(	select user_id 
																from user 
																where email = '$email')
											and course_id =(	select course_id 
																from chapter 
																where chapter_id = (	select chapter_id from chapter_content where chapter_content_url = '$chapter_content_url'))) 
				and chapter_id = (	select chapter_id from chapter_content where chapter_content_url='$chapter_content_url')";
		$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
	
	//檢查使用者是否已完成關卡
	function isUserChapterComplete($email,$chapter_content_url){
		$sql="select * 
		from user_chapter 
		where user_course_id = (	select user_course_id 
									from user_course 
									where user_id=(	select user_id 
													from user 
													where email='$email') 
									and course_id=(	select course_id 
													from chapter 
													where chapter_id=(	select chapter_id 
																		from chapter_content 
																		where chapter_content_url='$chapter_content_url'))) 
		and chapter_id = (	select chapter_id 
							from chapter_content 
							where chapter_content_url='$chapter_content_url')
		and status=1";
		$query=$this->db->query($sql);
		return $query->num_rows();
	}
	
	function getUserChapterProgress($email,$chapter_content_url){
		$sql = "select * 
				from user_chapter 
				where user_course_id = (	select user_course_id 
											from user_course 
											where user_id = (	select user_id 
																from user 
																where email = '$email') 
											and course_id = (	select course_id 
																from chapter 
																where chapter_id=(	select chapter_id 
																					from chapter_content 
																					where chapter_content_url = '$chapter_content_url')))
				and status=1";
		$query=$this->db->query($sql);
		return $query->num_rows();
	}
}