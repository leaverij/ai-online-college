<?php
class User_course_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

	/*	$user_course=array(	'user_id'	=>$user_id,
	 *						'course_id'	=>course_id);
	 */
	function addUserCourse($user_course){
		$sql="insert into user_course (user_id,course_id,create_time) values (?,?,?)";
		$user_course['create_time']=$this->theme_model->nowTime();
		$query=$this->db->query($sql,$user_course);
		return $this->db->affected_rows();
	}
	
	/*	$option=array(	'user.email'		=>$email,
	 *					'course.course_type'=>'1',		//0:Not set 1:Technique 2:Knowledge
	 *					'user_course.status'=>'0');		//0:Not Finished 1:Finished
	 */
	function getUserCourse($option){
		$sql="	select * from user 
				left join user_course on user.user_id=user_course.user_id
				left join course on course.course_id=user_course.course_id
				left join library on course.library_id=library.library_id
				where user.email=? and course.course_type=? and user_course.status=?";
		$query=$this->db->query($sql,$option);
		return $query;
	}
	
	//判斷user是否曾看過課程下的影片
	//$user_course=array('user_id'->$user_id,'course_id'->course_id);
	function isCourseSubscribed($user_course){
		$query=$this->db->get_where('user_course',$user_course);
		return $query->num_rows();
	}
	
	function getUserCourseId($user_course){
		$sql="select user_course_id from user_course where user_id=? and course_id=?";
		$query=$this->db->query($sql,$user_course);
		return $query->result();
	}
	
	function updateUserCourseStatus($email,$chapter_content_url){
		$accomplish_time = $this->theme_model->nowTime();
		$sql="update user_course 
		set status = 1 ,
		accomplish_time = '$accomplish_time'
		where user_id = (	select user_id 
							from user 
							where email = '$email') 
							and course_id = (	select course_id 
												from chapter 
												where chapter_id = (	select chapter_id 
																		from chapter_content 
																		where chapter_content_url = '$chapter_content_url'))";
		$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
	
	function isUserCourseComplete($email,$chapter_content_url){
		$sql="	select * from user_course 
				where status=1 
				and user_id = (	select user_id from user 
									where email='$email') 
									and course_id = (	select course_id 
														from chapter 
														where chapter_id = (	select chapter_id 
																				from chapter_content 
																				where chapter_content_url = '$chapter_content_url'))";
		$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
	
	function getUserAwardVideo($email){
		$sql="	select * from chapter_content
				left join chapter on chapter_content.chapter_id = chapter.chapter_id
				left join course on chapter.course_id = course.course_id
				left join library on course.library_id = library.library_id
				left join user_course on course.course_id=user_course.course_id
				left join user on user.user_id=user_course.user_id
				where course.course_id in (	select course.course_id from user 
											left join user_course on user.user_id=user_course.user_id
											left join course on course.course_id=user_course.course_id
											left join library on course.library_id=library.library_id
											where user.email='$email' and user_course.status=1) 
				and chapter_content.award_video='1'
				and user.email='$email'
				order by user_course.accomplish_time";
		$query=$this->db->query($sql);
		return $query;
	}
}