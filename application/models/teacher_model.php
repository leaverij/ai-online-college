<?php
class Teacher_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	
	function getOneTeacher($course_url){
		$sql="select * from teacher left join course on teacher.teacher_id=course.teacher_id where course_url='$course_url'";
    	$query=$this->db->query($sql);
    	return $query;
	}
    
    function hasTeacherData($user_id)
    {
        $sql="select * from teacher where teacher_id=$user_id";
        $query=$this->db->query($sql);
        return $query->num_rows()==0?"0":"1";       
    }
    function insert($data){
        $query = $this->db->insert('teacher', $data); 
        return $this->db->affected_rows();
    }
    function getUserData($user_id){
        $sql="select * from user where user_id=$user_id";
        $query=$this->db->query($sql);
        return $query;
    }
}