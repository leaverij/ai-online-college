<?php
class Domain_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	//檢查是否有$domain_url的學院
	function checkDomain($domain_url){
		$sql = "select * from domain where domain_url='$domain_url'";
		$query=$this->db->query($sql);
    	return $query->row();	
	}
	//取得單一學院底下的學程資訊(未登入)
	function getTracksByDomain($domain_url)
	{
		$sql = "select * from tracks,domain
				where domain.domain_id = tracks.domain_id
				and domain.domain_url='$domain_url'";
    	$query=$this->db->query($sql);
    	return $query;
	}
	//熟練度分類ex:學習者、練習者、能力者、專家
	function getTracksByDomainAndProficiency($domain_url,$proficiency)
	{
		$sql = "select * from tracks,domain
				where domain.domain_id = tracks.domain_id
				and domain.domain_url='$domain_url'
				and tracks.proficiency='$proficiency'";
    	$query=$this->db->query($sql);
    	return $query;	
	}
	//取得單一學院底下的學程資訊(登入)
	function getTracksByDomainAndUser($domain_url,$email)
	{
		$sql = "select * from domain,tracks left join
					(select name,email,user_tracks.tracks_date,tracks_id,user_tracks.learning_status
        			from user_tracks,user
        			where user.user_id = user_tracks.user_id
        			and user_tracks.learning_status='1'
        			and user.email='$email' order by user_tracks.tracks_date desc LIMIT 0,1) b
				on (tracks.tracks_id = b.tracks_id )
				where domain.domain_id = tracks.domain_id
				and domain.domain_url='$domain_url'
				order by b.tracks_date desc,tracks.tracks_id asc";
    	$query=$this->db->query($sql);
    	return $query;
	}
	//熟練度分類ex:學習者、練習者、能力者、專家
	function getTracksByDomainAndUserAndProficiency($domain_url,$email,$proficiency)
	{
		$sql = "select * from domain,tracks left join
					(select name,email,user_tracks.tracks_date,tracks_id,user_tracks.learning_status
        			from user_tracks,user
        			where user.user_id = user_tracks.user_id
        			and user_tracks.learning_status='1'
        			and user.email='$email' order by user_tracks.tracks_date desc LIMIT 0,1) b
				on (tracks.tracks_id = b.tracks_id )
				where domain.domain_id = tracks.domain_id
				and domain.domain_url='$domain_url'
				and tracks.proficiency='$proficiency'
				order by b.tracks_date desc,tracks.tracks_id asc";
    	$query=$this->db->query($sql);
    	return $query;
	}
}