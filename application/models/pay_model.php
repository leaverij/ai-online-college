<?php
class Pay_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
	}
	//新增訂單
	function insertOrder($data)
	{
		$this->db->insert('course_order', $data);
		return $this->db->insert_id(); 
	}
	//新增訂單明細
	function insertOrderDetail($data){
		$this->db->insert('order_detail', $data);
	}
	//更新訂單
	function updateOrder($course_id,$data){
		$this -> db -> where('course_id', $course_id);
		$this -> db -> update('course_order', $data);
	}
	
	//query學程內的所有課程id(已付款過的不寫入)
	function getCourseData($tracks_id){
		$user_id = $this->session->userdata('user_id');
		$sql = "select * from tracks,tracks_course,course
				where tracks.tracks_id = '$tracks_id'
				and tracks.tracks_id = tracks_course.tracks_id
				and tracks_course.course_id = course.course_id
				and course.course_id not in (select order_detail.course_id from order_detail where order_detail.user_id ='$user_id' and order_detail.live_flag ='1')";
		$query = $this->db->query($sql);
    	return $query;
	}
	//檢查使用者是否購買學程
	function checkBuyTracks($tracks_url){
		$user_id = $this->session->userdata('user_id');
		$sql = "select * from course_order,tracks
				where course_order.type='tracks'
				and course_order.user_id ='$user_id'
				and tracks.tracks_id = course_order.tracks_id
				and tracks.tracks_url ='$tracks_url'";
		$query = $this->db->query($sql);
    	return $query;
	}
}