<?php
class Jupyter_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	
	function insertData($id, $dns, $course_id, $chapter_id, $chapter_content_id){
		$user = '1';
		$now = date('Y-m-d H:i:s');
		$sql = "
			INSERT INTO jupyter
			(user_id, course_id, chapter_id, chapter_content_id, instanceID, instanceURL, startTime, status, restartTime)
			VALUES
			(?, ?, ?, ?, ?, ?, ?, 1, ?)
			ON DUPLICATE KEY UPDATE chapter_id=?, chapter_content_id=?, instanceID=?, instanceURL=?, startTime=?, restartTime=?
		";
		$query=$this->db->query($sql, array($user, $course_id, $chapter_id, $chapter_content_id, $id, $dns, $now, $now, $chapter_id, $chapter_content_id, $id, $dns, $now, $now));
		return $query;
	}

	function getData($course_id, $chapter_id, $chapter_content_id){
		$user = '1';
		$now = date('Y-m-d H:i:s');
		$sql = "
			SELECT  *, period + TIME_TO_SEC(TIMEDIFF(?, restartTime)) as periodtime
			FROM 	jupyter
			WHERE 	user_id = ?
			AND 	course_id = ?
			AND 	chapter_id = ?
			AND 	chapter_content_id = ?
		";
		$query=$this->db->query($sql, array($now, $user, $course_id, $chapter_id, $chapter_content_id));
		$row = $query->row_array(); 
		return $row;
	}
	
	function updatedns($instanceid, $dns){
		$user = '1';
		$now = date('Y-m-d H:i:s');
		if(!empty($instanceid)&&!empty($dns)){
			$sql = "
				UPDATE 	jupyter
				SET 	instanceURL = ?,
						restartTime = ?
				WHERE 	instanceID = ?
				AND 	user_id = ?
			";
			$query=$this->db->query($sql, array($dns, $now, $instanceid, $user));
			return $query;
		}
	}

	function updatestatus($instanceid, $status){
		$user = '1';
		if(!empty($instanceid)){
			$sql = "
				UPDATE 	jupyter
				SET 	status = ?
				WHERE 	instanceID = ?
				AND 	user_id = ?
			";
			$query=$this->db->query($sql, array($status, $instanceid, $user));
			return $query;
		}
	}

	function updateperiod($instanceid){
		$user = '1';
		$now = date('Y-m-d H:i:s');
		if(!empty($instanceid)){
			$sql = "
				UPDATE 	jupyter
				SET 	period = period + TIME_TO_SEC(TIMEDIFF(?, restartTime))
				WHERE 	instanceID = ?
				AND 	user_id = ?
			";
			$query=$this->db->query($sql, array($now, $instanceid, $user));
			return $query;
		}
	}
}