<?php
class Backend_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
	}
//list
	/*-----------列出相關活動　-----------*/
	function listAllSeminar()
	{
		$sql = "select * from seminar,user
				where seminar.user_id = user.user_id
				order by seminar_post_date desc";
    	$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------列出趨勢應用　-----------*/
	function listAllIndustry()
	{
		$sql = "select * from industry,user
				where user.user_id = industry.user_id
				order by industry_post_date desc";
    	$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------列出國內外資源-----------*/
	function listAllResource()
	{
		$sql = "select * from resource,user
				where user.user_id = resource.user_id
				order by resource_post_date desc";
    	$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------列出企業徵才　-----------*/
	function listAllJobs()
	{
		$sql = "select * from jobs,user
				where user.user_id = jobs.user_id
				order by jobs_post_date desc";
    	$query=$this->db->query($sql);
    	return $query;
	}
//del	
	/*-----------刪除相關活動　-----------*/
	function delSeminar($seminar_id)
	{
		$this->db->delete('seminar', array('seminar_id' => $seminar_id)); 
	}
	/*-----------刪除趨勢應用　-----------*/
	function delIndustry($industry_id)
	{
		$this->db->delete('industry', array('industry_id' => $industry_id)); 
	}
	/*-----------刪除國內外資源-----------*/
	function delResource($resource_id)
	{
		$this->db->delete('resource', array('resource_id' => $resource_id)); 
	}
	/*-----------刪除企業徵才　-----------*/
	function delJobs($jobs_id)
	{
		$this->db->delete('jobs', array('jobs_id' => $jobs_id)); 
	}
//update	
	/*-----------修改相關活動　-----------*/
	function updateSeminar($seminar_id,$data)
	{
		$this -> db -> where('seminar_id', $seminar_id);
		$this -> db -> update('seminar', $data);
	}
	/*-----------修改趨勢應用　-----------*/
	function updateIndustry($industry_id,$data)
	{
		$this -> db -> where('industry_id', $industry_id);
		$this -> db -> update('industry', $data);
	}
	/*-----------修改國內外資源-----------*/
	function updateResource($resource_id,$data)
	{
		$this -> db -> where('resource_id', $resource_id);
		$this -> db -> update('resource', $data);
	}
	/*-----------修改企業徵才　-----------*/
	function updateJobs($jobs_id,$data)
	{
		$this -> db -> where('jobs_id', $jobs_id);
		$this -> db -> update('jobs', $data);
	}
//add	
	/*-----------新增相關活動　-----------*/
	function addSeminar($data)
	{
		$this->db->insert('seminar', $data); 
	}
	/*-----------新增趨勢應用　-----------*/
	function addIndustry($data)
	{
		$this->db->insert('industry', $data); 
	}
	/*-----------新增國內外資源-----------*/
	function addResource($data)
	{
		$this->db->insert('resource', $data); 
	}
	/*-----------新增企業徵才　-----------*/
	function addJobs($data)
	{
		$this->db->insert('jobs', $data); 
	}
	
	/*-----------職務分類顯示頁__相關活動　-----------*/
	function listSeminarByJobsCategory($jobs_category_url)
	{
		$sql = "select * from seminar,jobs_category
				where find_in_set(jobs_category.jobs_category_id,seminar.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				order by seminar_post_date desc limit 0,5;";
		$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------職務分類顯示頁__趨勢應用　-----------*/
	function listIndustryByJobsCategory($jobs_category_url)
	{
		$sql = "select * from industry,jobs_category
				where find_in_set(jobs_category.jobs_category_id,industry.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				order by industry_post_date desc limit 0,5;";
		$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------職務分類顯示頁__國內外資源-----------*/
	function listResourceByJobsCategory($jobs_category_url)
	{
		$sql = "select * from resource,jobs_category
				where find_in_set(jobs_category.jobs_category_id,resource.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				order by resource_post_date desc limit 0,5;";
		$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------職務分類顯示頁__企業徵才　-----------*/
	function listJobsByJobsCategory($jobs_category_url)
	{
		$sql = "select * from jobs,jobs_category
				where find_in_set(jobs_category.jobs_category_id,jobs.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				order by jobs_post_date desc limit 0,5;";
		$query=$this->db->query($sql);
    	return $query;
	}
	
	/*-----------相關活動__資料總筆數　-----------*/
	function listAllSeminarCount($jobs_category_url)
	{
		$sql = "select count(*) count
				from seminar,jobs_category
				where find_in_set(jobs_category.jobs_category_id,seminar.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				order by seminar_post_date desc";
		$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------趨勢應用__資料總筆數　-----------*/
	function listAllIndustryCount($jobs_category_url)
	{
		$sql = "select count(*) count
				from industry,jobs_category
				where find_in_set(jobs_category.jobs_category_id,industry.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				order by industry_post_date desc";
		$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------國內外資源__資料總筆數-----------*/
	function listAllResourceCount($jobs_category_url)
	{
		$sql = "select count(*) count
				from resource,jobs_category
				where find_in_set(jobs_category.jobs_category_id,resource.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				order by resource_post_date desc";
		$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------企業徵才__資料總筆數　-----------*/
	function listAllJobsCount($jobs_category_url)
	{
		$sql = "select count(*) count
				from jobs,jobs_category
				where find_in_set(jobs_category.jobs_category_id,jobs.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				order by jobs_post_date desc";
		$query=$this->db->query($sql);
    	return $query;
	}
	
	/*-----------list更多__相關活動　-----------*/
	function listAllSeminarByCategory($jobs_category_url,$start,$per_page)
	{
		$sql = "select * from seminar,jobs_category
				where find_in_set(jobs_category.jobs_category_id,seminar.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				order by seminar_post_date desc limit $start,$per_page;";
		$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------list更多__趨勢應用　-----------*/
	function listAllIndustryByCategory($jobs_category_url,$start,$per_page)
	{
		$sql = "select * from industry,jobs_category
				where find_in_set(jobs_category.jobs_category_id,industry.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				order by industry_post_date desc limit $start,$per_page;";
		$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------list更多__國內外資源-----------*/
	function listAllResourceByCategory($jobs_category_url,$start,$per_page)
	{
		$sql = "select * from resource,jobs_category
				where find_in_set(jobs_category.jobs_category_id,resource.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				order by resource_post_date desc limit $start,$per_page";
		$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------list更多__企業徵才　-----------*/
	function listAllJobsByCategory($jobs_category_url,$start,$per_page)
	{
		$sql = "select *
				from jobs,jobs_category
				where find_in_set(jobs_category.jobs_category_id,jobs.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				order by jobs_post_date desc limit $start,$per_page";
		$query=$this->db->query($sql);
    	return $query;
	}
	
	/*-----------搜尋相關活動　-----------*/
	function listSearchSeminarByCategory($jobs_category_url)
	{
		$search_string = $this->security->xss_clean($this->session->userdata('search_string'));
		$this->session->unset_userdata('search_string');
		$sql = "select * from seminar,jobs_category
				where find_in_set(jobs_category.jobs_category_id,seminar.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				and (seminar.seminar_title like '%$search_string%' or seminar.seminar_content like '%$search_string%')
				order by seminar_post_date desc";
		$query=$this->db->query($sql);
    	return $query;
	}	
	/*-----------搜尋趨勢應用　-----------*/
	function listIndustryBySearchCategory($jobs_category_url,$search_string)
	{
		$sql = "select * from industry,jobs_category
				where find_in_set(jobs_category.jobs_category_id,industry.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				and industry_title like '%$search_string%'
				order by industry_post_date desc";
		$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------搜尋國內外資源-----------*/
	function listSearchResourceByCategory($jobs_category_url)
	{
		$search_string = $this->security->xss_clean($this->session->userdata('search_string'));
		$this->session->unset_userdata('search_string');
		$sql = "select * from resource,jobs_category
				where find_in_set(jobs_category.jobs_category_id,resource.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				and resource_title like '%$search_string%' 
				order by resource_post_date desc";
		$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------搜尋企業徵才　-----------*/
	function listJobsBySearchCategory($jobs_category_url,$search_string)
	{
		$sql = "select * from jobs,jobs_category
				where find_in_set(jobs_category.jobs_category_id,jobs.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				and (Jobs_title like '%$search_string%' or comp_name like '%$search_string%')
				order by jobs_post_date desc";
		$query=$this->db->query($sql);
    	return $query;
	}
	
	/*-----------依來源分類__相關活動　-----------*/
	/*-----------依來源分類__趨勢應用　-----------*/
	function listAllSeminarByCategoryAndSeminar($jobs_category_url,$seminar_name)
	{
		$search_string = $this->security->xss_clean($this->session->userdata('search_string'));
		$this->session->unset_userdata('search_string');
		$sql = "select * from seminar,jobs_category
				where find_in_set(jobs_category.jobs_category_id,seminar.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				and seminar.seminar_name = '$seminar_name'
				and (seminar.seminar_title like '%$search_string%' or seminar.seminar_content like '%$search_string%')
				order by seminar_post_date desc";
		$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------依來源分類__國內外資源-----------*/
	function listAllResourceByCategoryAndResource($jobs_category_url,$resource_type)
	{
		$search_string = $this->security->xss_clean($this->session->userdata('search_string'));
		$this->session->unset_userdata('search_string');
		$sql = "select * from resource,jobs_category
				where find_in_set(jobs_category.jobs_category_id,resource.jobs_category_id)
				and jobs_category.jobs_category_url = '$jobs_category_url'
				and resource.resource_type = '$resource_type'
				and resource_title like '%$search_string%'
				order by resource_post_date desc";
		$query=$this->db->query($sql);
    	return $query;
	}
	/*-----------依來源分類__企業徵才　-----------*/
			
	//後台輸入各式資源，列出職務分類以及ID
	function listJobsCategory()
	{
		$sql = "select jobs_category_id, jobs_category_name from jobs_category where status ='1';";
		$query=$this->db->query($sql);
    	return $query;
	}
	//列出熱門討論
	function listPopularDiscussions($jobs_category_url){
		$sql = "select * from jobs_category,jobs_category_library,library,forum_topic,user,(select forum_topic_id,count(*) count from forum_content group by forum_topic_id)b
				where jobs_category.jobs_category_id =jobs_category_library.jobs_category_id
				and jobs_category_library.library_id = library.library_id
				and jobs_category.jobs_category_url='$jobs_category_url'
				and library.library_id = forum_topic.library_id
				and user.user_id = forum_topic.user_id
				and forum_topic.forum_topic_id = b.forum_topic_id
				order by b.count desc limit 0,5";
		$query=$this->db->query($sql);
    	return $query;
	}
	//列出最新討論
	function listNewDiscussions($jobs_category_url){
		$sql = "select * from jobs_category,jobs_category_library,library,forum_topic,user,(select forum_topic_id,count(*) count from forum_content group by forum_topic_id)b
				where jobs_category.jobs_category_id =jobs_category_library.jobs_category_id
				and jobs_category_library.library_id = library.library_id
				and jobs_category.jobs_category_url='$jobs_category_url'
				and library.library_id = forum_topic.library_id
				and user.user_id = forum_topic.user_id
				and forum_topic.forum_topic_id = b.forum_topic_id
				order by forum_topic.posted_time desc limit 0,5";
		$query=$this->db->query($sql);
    	return $query;
	}
	//紀錄資源的點擊次數(還有討論區)
	function updateUserClicks($type,$id){
		if($type=='resource'||$type=='jobs'||$type=='seminar'||$type=='industry'||$type=='forum_topic'){
			$db = $type.'_id'; 
		$sql="	update $type 
				set clicks = clicks + 1 
				where $db = '$id'";
    	$query=$this->db->query($sql);
		return $query;
		}
	}
	//紀錄modal的點擊次數
	function updateModalClicks($type,$id){
		if($type=='seminar'){
			$db = $type.'_id'; 
		$sql="	update $type 
				set modal_clicks = modal_clicks + 1 
				where $db = '$id'";
    	$query=$this->db->query($sql);
		return $query;
		}
	}
	//後台輸入用的，取得趨勢應用的出處
	function getSource($type){
		$table = $type.'_source'; 
		$sql="	select distinct $table from $type WHERE $table != ''";
    	$query=$this->db->query($sql);
		return $query;
	}
	//使用jobs_category_url取得jobs_category_name
	function getCategoryName($jobs_category_url){
		$sql = "select jobs_category_name from jobs_category where status ='1' and jobs_category_url = '$jobs_category_url'";
		$query=$this->db->query($sql);
    	return $query;
	}
	
	/*----------- 寫入tin-can　-----------*/
    function insertStatement($data)
    {
        $this->db->insert('statement', $data); 
    }
}