<?php
class Job_apply_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }

	function addJobVacancy($job_apply_data){
		$query=$this->db->insert('job_apply',$job_apply_data);
		return $query;
    }
    
    function getJobVacancy($job_vacancy_id, $user_id){
        $this->db->where('job_vacancy_id', $job_vacancy_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('job_apply');
		return $query;
	}

    function getAllJobList($qdata){
        $querystring = '';
        if(!empty($qdata['job_category'])){
            $querystring .= ' AND jobs_category_id = '.$qdata['job_category'];
        }
        if(!empty($qdata['degree_limit'])){
            $querystring .= ' AND degree_limit = '.$qdata['degree_limit'];
        }
        if($qdata['working_experience']=='0' || !empty($qdata['working_experience'])){
            $querystring .= ' AND working_experience = '.$qdata['working_experience'];
        }
        if(!empty($qdata['job_name'])){
            $querystring .= " AND job_name like '%".$qdata['job_name']."%'";  
        }   
        $sql = "
            SELECT job_vacancy.*, user.company_name
            FROM job_vacancy 
            INNER JOIN user ON job_vacancy.user_id = user.user_id 
            WHERE vacancy_open_flag = '1'
            $querystring
            ";
        $query=$this->db->query($sql);
        return $query;      
    }
    
    function getAllJobCategory(){
        $sql = "select * from jobs_category where status='1'";
        $query=$this->db->query($sql);
        return $query;      
    }

    function getApplyRecord($user_id){
        $sql = "
            SELECT job_apply.apply_date, job_vacancy.job_name, user.company_name 
            FROM job_vacancy, job_apply, user 
            WHERE job_apply.user_id = '$user_id'
            AND job_vacancy.job_vacancy_id = job_apply.job_vacancy_id 
            AND job_vacancy.user_id = user.user_id";
        $query=$this->db->query($sql);
        return $query;         
    }    
}