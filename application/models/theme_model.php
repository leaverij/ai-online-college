<?php
class Theme_model extends CI_Model {

	var $view   = '';
	var $data   = '';
   
    function __construct()
    {
        parent::__construct();
        $this->load->model('notifications_model');
        $this->load->model('forum_subscription_model');
		$this->load->model('forum_assistant_model');
		
		$langArr = array("zh-tw", "zh-cn","english");
        //load 語系
        if($this->session->userdata('locale')!=null &&  $this->session->userdata('locale')!=""){           
            $lang = $this->session->userdata('locale');
            $lang = strtolower($lang);
            if(!in_array($lang, $langArr)){
                $lang = "zh-tw";
            }  
        }else{
            
            //$lang = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
            preg_match('/^([a-z\-]+)/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $matches);  

            $lang = $matches[1];  
            $lang = strtolower($lang);
             // echo $lang;
            if(!in_array($lang, $langArr)){
                $lang = "zh-tw";
            }
            $locale = array('locale'=>$lang);
            $this->session->set_userdata($locale);
            $first_set_locale = array('first_set_locale'=>"true");
            $this->session->set_userdata($first_set_locale);
            
        }
        $this->lang->load('aioc',$lang);
    }
	
	function loadTheme($view,$data)
    {		
    	// $data['getDropdownNotification']=json_encode($this->notifications_model->getDropdownNotification($this->session->userdata('email'))->result());
    	$data['getUnreadNotification']=$this->notifications_model->getUnreadNotification($this->session->userdata('email'));
    	$data['getUserForumSubscription']=json_encode($this->forum_subscription_model->getUserForumSubscription($this->session->userdata('email'))->result());
    	if($this->forum_assistant_model->getUserSupportLibraryId($this->session->userdata('user_id'))->num_rows()>0){
	    	$data['getUserSupportLibraryId']=json_encode($this->forum_assistant_model->getUserSupportLibraryId($this->session->userdata('user_id'))->result());
		}
    	$this->load->view('head',$data);
        $this->load->view($view,$data);
        $this->load->view('foot');       		
    }
	function nowTime()
	{
		$date = mdate("%Y-%m-%d %H:%i:%s",time());
		return $date;
	}
	function flash_session($type,$msg)
	{
		//4種type: alert-success alert-info alert-warning alert-danger
		$value=array("type"=>$type,"msg"=>$msg);
		$this->session->set_flashdata('alert', $value);
	}
	//設到session的資料如果有增加,controller的home/logout 也要增加	
	function set_session($email){
		$check = $this->db->query('select * from user where email ="'.$email.'" ');
		$row=$check->row_array();
		$this->load->library('os_info');
		$os = $this->os_info->getOS();
		$browser = $this->os_info->getBrowser();
		$this -> user_model -> insertLoginHistory(array('user_id' => $row['user_id'], 'os' => $os, 'browser' => $browser));
		if($check->num_rows()!=0){				
			$name = $row['name'];
			$email = $row['email'];	
			$alias = $row['alias'];
			$user_id = $row['user_id'];
			$userdata = array(  			
				'name'  => $name,
				'email'=>$email,
				'alias'=>$alias,
				'user_id'=>$user_id
				);
			$this->session->set_userdata($userdata);
		}
	}
	function hash_password($password, $salt=false){
  		if (!$salt) {
			$salt = md5(mt_rand());
  		}
		return $salt . md5($salt . $password);
	}
		
    function compare_password($password, $db_password){
  		$salt = substr( $db_password, 0, 32 );
		return $db_password === $this->hash_password($password, $salt);
	}
	
	function calTimeElapsed($targetTime){
		$time = strtotime($targetTime);
		return $this->humanTiming($time).'前';
	}
	
	function  humanTiming ($time){
		$time = time() - $time; // to get the time since that moment
		$tokens = array (
			31536000 => '年',
			2592000 => '月',
			604800 => '週',
			86400 => '天',
			3600 => '小時',
			60 => '分鐘',
			1 => '秒'
		);
		if(floor($time)==0)
			return '不久';
		else{
			foreach ($tokens as $unit => $text) {
				if ($time < $unit) continue;
				$numberOfUnits = floor($time / $unit);
				return $numberOfUnits.' '.$text;
			}
		}
	}
	
	function getIp(){
		if(!empty($_SERVER['HTTP_CLIENT_IP'])){
   			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
   			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
   			$ip= $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	function printTheme($view,$data)
    {		
    	// $this->load->view('head',$data);
        $this->load->view($view,$data);
        // $this->load->view('foot');       		
    }
}