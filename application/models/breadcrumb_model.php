<?php
class Breadcrumb_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	//第一層 領域(不過沒用到)
	function getLibraryUrl($library_url)
	{
		$sql = "select library_name,library_url
				from library
				where library_url='$library_url'";
		$query = $this->db->query($sql);
		return $query;
	}
	//第二層 (課程)
	function getCourseUrl($course_url)
	{
		$sql = "select distinct course_name,course_url from course left join chapter
				on course.course_id = chapter.course_id
				where course.course_url = '$course_url'";
		$query = $this->db->query($sql);
		return $query;
	}
	//第三層 (章節)
	function getChapterUrl($chapter_url)
	{
		$sql = "select distinct chapter_name,chapter_url from chapter left join chapter_content
				on chapter.chapter_id = chapter_content.chapter_id
				where chapter.chapter_url ='$chapter_url'";
		$query = $this->db->query($sql);
		return $query;
	}
	//第四層 (影片)
	function getContentUrl($content_url)
	{
		//content_model 裡面有 getOneContent 可以用
	}
}