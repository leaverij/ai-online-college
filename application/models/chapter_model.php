<?php
class Chapter_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	//列出單一關卡下的內容(也就是chapter_content)
	function getOneChapterContent($chapter_url)
	{
    	$sql="	select a.*,b.*,c.quantity from chapter a left join chapter_content b
				on a.chapter_id = b.chapter_id
				left join quiz_detail c
				on b.chapter_content_id = c.chapter_content_id
				where a.chapter_url = '$chapter_url' and content_open_flag = '1'
				order by b.content_order";
    	$query=$this->db->query($sql,array($chapter_url));
		return $query;
	}

	// function getChapterByChapterUrl($chapter_url){
		// $query=$this->db->get_where('chapter',array('chapter_url'=>$chapter_url));
		// return $query;
	// }

	//回傳課程指定下的徽章數量
	function getCourseBadges($course_url){
		$this->db->select('*')
		->from('chapter')
		->join('course','chapter.course_id=course.course_id','left')
		->where('course_url',$course_url);
		$query=$this->db->get();
		return $query->num_rows();
	}
	
	function getCourseCompleteGoal($chapter_content_url){
		$sql="	select * from chapter 
				where course_id = (	select course_id 
									from chapter 
									where chapter_id = (	select chapter_id 
															from chapter_content 
															where chapter_content_url='$chapter_content_url'))";
		$query=$this->db->query($sql);
		return $query->num_rows();
	}
	//列出單一關卡下的資源
	function listResource($chapter_url){
		$sql="	select * from chapter,dl_resource,course,library
				where chapter.chapter_id = dl_resource.chapter_id
				and chapter.chapter_url = '$chapter_url'
				and library.library_id = course.library_id
				and course.course_id = chapter.chapter_id";
    	$query=$this->db->query($sql);
		return $query;
	}
	//修改教學資源的下載次數
	function updateDownloads($dl_resource_id)
	{
		$sql="	update dl_resource 
				set downloads = downloads + 1 
				where dl_resource_id = '$dl_resource_id'";
    	$query=$this->db->query($sql);
		return $query;
	}
	//取得下載資源的網址
	function getDownloadsUrl($dl_resource_id){
		$sql = "select library_url,dl_resource_url from library,course,chapter,dl_resource
				where library.library_id = course.library_id
				and course.course_id = chapter.course_id
				and dl_resource.chapter_id = chapter.chapter_id
				and dl_resource.dl_resource_id = '$dl_resource_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}

	//教師取得課程所有章節
	function teacherGetChapters($course_id, $teacher_id){
		$sql = "select * from chapter ch 
				left join course co on ch.course_id=co.course_id 
				where co.course_id=$course_id and co.teacher_id=$teacher_id and ch.status='1' order by chapter_order asc";
		$query=$this->db->query($sql);
		return $query;		
	}

	//更新關卡名稱
	function updateChapterName($chapter_id,$chapter_name){
		$sql="	update chapter 
				set chapter_name = '$chapter_name' 
				where chapter_id = $chapter_id";
    	$query=$this->db->query($sql);
		return $query;
	}

	//建立新關卡
    function createNewChapter($data){
		$this -> db -> insert('chapter', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
	}

	//刪除關卡
	function removeChapter($chapter_id){
		$sql = "update chapter set status='0' where chapter_id=$chapter_id";
		$query=$this->db->query($sql);
		return $query;
	}

	//取得使用者chapter紀錄
	function getUserChapterCount($chapter_id){
		$sql = "
			SELECT 	sum(status) as ucfinish, count(status) as ucjoin
			FROM 	user_chapter
			WHERE 	chapter_id = ?
		";
		$query=$this->db->query($sql, array($chapter_id));
		return $query;
	}

	//更新關卡順序
	function updateChapterOrder($chapter_id, $chapter_order){
		$sql = "update chapter set chapter_order=$chapter_order where chapter_id=$chapter_id";
		$query=$this->db->query($sql);
		return $query;
	}


}