<?php
class Mail_model extends CI_Model {
	var $message_head   = '<html><head></head><body>';
    var $message_foot   = '</body></html>';

    function __construct()
    {
        parent::__construct();
		$this->load->library('email');	
		$this->load->model('user_model');
		
    }
	//寄送忘記密碼信件
	function sendForgetMail($data)
	{
		$email = $data['email'];
		$alias = $this->user_model->getAlias($email)->row_array();
		$message = $this->message_head;
		$message.= $alias['alias'].'您好：<br>';
		$message.= '我們收到您的重設密碼需求，如果您忘記密碼，欲重新設定密碼，請至下方的網址設定：<br>';
		$message.= '<a href="'.base_url().'user/resetPassword/'.$data['code'].'">重新設定</a><br>';
		$message.= '（本連結將在一小時後自動失效）';
		$message.= $this->message_foot;
		
		$this->email->from('noreply@aioc.iiiedu.org.tw', 'AI Online College 團隊');
		$this->email->to($email);
		$this->email->subject('[重設密碼]重設您的 AI Online College 密碼!');
		$this->email->message($message);
		$this->email->send();
		//echo $this->email->print_debugger();
		$recordMail = array('addressee'=>$email ,'email_time' => $this->theme_model->nowTime());
		$this->recordMail($recordMail);
	}
	//寄送啟用帳號認證信件
	function sendActiveMail($data)
	{
		$email = $data['email'];
		$message = $this->message_head;
		$message.= $email.' 您好，歡迎加入 AI Online College：<br><br>';
		$message.= '請按以下連結以完成註冊啟用程序:<br>';
		$message.= '<a href="'.base_url().'user/active/'.$data['active_code'].'">開始啟用</a><br><br>';
		$message.= 'AI Online College 團隊敬上';
		$message.= $this->message_foot;
		
		$this->email->from('noreply@aioc.iiiedu.org.tw', 'AI Online College 團隊');
		$this->email->to($email);
		$this->email->subject('[帳號啟動]AI Online College啟用帳號認證信!');
		$this->email->message($message);
		$this->email->send();
		$recordMail = array('addressee'=>$email ,'email_time' => $this->theme_model->nowTime());
		$this->recordMail($recordMail);
	}
	//批次寄送啟用帳號認證信件_ for 2014大專以上就業人口意向調查
	function sendBatchMail($data,$inputPassword)
	{
		$email = $data['email'];
		$message = $this->message_head;
		$message.= '您好：<br>';
		$message.= '此信件為註冊 AI Online College 後之啟用通知，<br>';
		$message.= '您在本網站上註冊的帳號為'.$email.'<br>';
		$message.= '預設密碼為'.$inputPassword.'<br>';
		$message.= '請按以下連結以完成註冊啟用程序<br>';
		$message.= '<a href="'.base_url().'user/active/'.$data['active_code'].'">開始啟用</a><br>';
		$message.= '完成啟用後，您將可隨時觀看「AI Online College」的精彩課程及各式學習資源，<br>';
		$message.= 'AI Online College 感謝您的支持!<br>';
		$message.= '貼心提醒: 啟用後,您可至個人資料區變更會員密碼<br>';
		$message.= $this->message_foot;
		
		$this->email->from('noreply@aioc.iiiedu.org.tw', 'AIOC 團隊');
		$this->email->to($email);
		$this->email->subject('[帳號啟動]AI Online College 啟用帳號認證信!');
		$this->email->message($message);
		$this->email->send();
		$recordMail = array('addressee'=>$email ,'email_time' => $this->theme_model->nowTime());
		$this->recordMail($recordMail);
	}
	//儲存寄信資訊
	function recordMail($data)
	{
		$this -> db -> insert('email_record', $data);
	}
	
	function sendEmailToForumAssistants($data){
		$email = $data['email'];
		$message = $this->message_head;
		if($data['support_type']==0){
			$message.= $data['alias'].' 助教 您好：<br><br>';
		}else if($data['support_type']==1){
			$message.= $data['alias'].' 講師 您好：<br><br>';
		}else if($data['support_type']==2){
			$message.= $data['alias'].' 總監 您好：<br><br>';			
		}		
		$message.= '學員在 AI Online College 「'.$data['library_name'].'」 分類下提問了新問題<br>';
		$message.= '以下是學員提問的問題：<br>';
		$message.= '提問者：'.$data['ask_alias'].'<br>';
		$message.= '討論區主題：'.$data['forum_topic'].'<br>';
		$message.= '討論區內容：'.$data['forum_content'].'<br><br>';
		$message.= '請盡快解答提問者的疑惑<br><br>';
		$message.= '<a href="'.$data['hyperlink'].'">前往討論串</a><br>';
		$message.= $this->message_foot;
		$this->email->from('noreply@aioc.iiiedu.org.tw', 'AI Online College 團隊');
		$this->email->to($email);
		$this->email->subject('[提問通知]AI Online College 討論區提問通知!');
		$this->email->message($message);
		$this->email->send();
		$recordMail = array('addressee'=>$email ,'email_time' => $this->theme_model->nowTime());
		$this->recordMail($recordMail);
	}
	function sendReplyEmailToForumSubscribers($data){
		$email = $data['email'];
		$message = $this->message_head;
		$message.= $data['alias'].' 您好：<br><br>';
		$message.= '學員在 AI Online College 討論區中回應了您的問題<br>';
		$message.= '以下是學員回應的內容：<br>';
		$message.= '回應者：'.$data['ask_alias'].'<br>';
		$message.= '討論區主題：'.$data['forum_topic'].'<br>';
		$message.= '討論區內容：'.$data['forum_content'].'<br><br>';
		$message.= '您可至討論區中評分，標註合適的答案給其他人參考<br><br>';
		$message.= '<a href="'.$data['hyperlink'].'">前往討論串</a><br>';
		$message.= $this->message_foot;
		$this->email->from('noreply@aioc.iiiedu.org.tw', 'AI Online College 團隊');
		$this->email->to($email);
		$this->email->subject('[回應通知]AI Online College 討論區回應通知!');
		$this->email->message($message);
		$this->email->send();
		$recordMail = array('addressee'=>$email ,'email_time' => $this->theme_model->nowTime());
		$this->recordMail($recordMail);
	}
}