<?php
class Coupon_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
    
	function couponGenerator($count) {
        $couponArray=array();
        for($i=0;$i<$count;$i++){
            array_push($couponArray,$this->randomString());
        }
        return $couponArray;
	}
	
	function randomString() {
		$random=random_string('alnum',16);
		return 	$random;
		//$random=random_string('alnum',4).'-'.random_string('alnum',4).'-'.random_string('alnum',4).'-'.random_string('alnum',4);
		//return 	trim($random);
	}
    
	function addCoupon($coupon){
		// $sql = "insert into coupon (coupon_code,start_time,end_time,comment) values (?,?,?,?)";
		// $query = $this->db->query($sql,array($coupon_code,$start_time,$end_time,$comment));
		$query = $this->db->insert('coupon', $coupon); 
		return $this->db->affected_rows();
	}
	
	function updateCoupon($coupon_id,$data){
		// $sql = "update coupon set start_time=?,end_time=?,comment=?,used_flag=? where coupon_code=?";
		// $query = $this->db->query($sql,array($start_time,$end_time,$comment,$used_flag,$coupon_code));
		//$query = $this->db->update('coupon', $coupon, $where);
		//return $this->db->affected_rows();
		$this -> db -> where('coupon_id', $coupon_id);
		$this -> db -> update('coupon', $data);
	}
	function updateCouponCode($coupon_code,$data){
		// $sql = "update coupon set start_time=?,end_time=?,comment=?,used_flag=? where coupon_code=?";
		// $query = $this->db->query($sql,array($start_time,$end_time,$comment,$used_flag,$coupon_code));
		//$query = $this->db->update('coupon', $coupon, $where);
		//return $this->db->affected_rows();
		$this -> db -> where('coupon_code', $coupon_code);
		$this -> db -> update('coupon', $data);
	}
	
	function delCoupon($coupon_id){
		// $sql = "delete from coupon where coupon_id=? and coupon_code=?";
		// $query = $this->db->query($sql,array($coupon_id,$coupon_code));
		//$query = $this->db->delete('coupon',$coupon);
		//return $this->db->affected_rows();
		$this->db->delete('coupon', array('coupon_id' => $coupon_id)); 
	}
	
	function listCoupon(){
		$sql = "select * from coupon order by create_time desc";
		$query = $this->db->query($sql);
		return $query;
	}
	
	function listOneCoupon($coupon){
		// $sql = "select * from coupon where coupon_id=? and coupon_code=?";
		// $query = $this->db->query($sql,array($coupon_id,$coupon_code));
		$query = $this->db->get_where('coupon',$coupon);
		
		return $query->result();
	}
	//驗證coupon是否有效
	function checkCoupon($coupon_code){
		$sql = "select * from coupon
				where coupon_code = '$coupon_code'";
		$query = $this->db->query($sql);
		return $query;
	}
}