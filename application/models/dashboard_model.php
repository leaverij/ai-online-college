<?php
class Dashboard_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    //取得學程數
    function getAllTrackCounts(){
        $sql = "select count(*) as track_counts from tracks";
        $query=$this->db->query($sql);
        return $query;
    }

    //取得課程數
    function getAllCourseCounts(){
        $sql = "select count(*) as course_counts from course";
        $query=$this->db->query($sql);
        return $query;
    }

    //取得教師數
    function getAllTeacherCounts(){
        $sql = "select count(*) as teacher_counts from teacher";
        $query=$this->db->query($sql);
        return $query;
    }

    //取得學生數
    function getAllStudentCounts(){
        $sql = "select count(*) as student_counts from user";
        $query=$this->db->query($sql);
        return $query;
    }

    //  取得註冊人數（註冊率）
    function getRegisterUsage($date_from, $date_end){
        $sql = "
            SELECT  count(user_id) as count, date_format(register_date, '%c') as month
            FROM    user
            WHERE   date_format(register_date, '%Y-%m-%d') between ? and ?
            GROUP BY date_format(register_date, '%Y-%c')
        ";
        $query=$this->db->query($sql, array($date_from, $date_end));
        $result = $query->result_array();
        return $result;
    }

    //  取得登入紀錄（活躍率）
    function getLoginUsage($date_from, $date_end){
        $sql = "
            SELECT  count(user_id) as count, date_format(login_time, '%c') as month
            FROM    login_history
            WHERE   date_format(login_time, '%Y-%m-%d') between ? and ?
            GROUP BY date_format(login_time, '%Y-%c')
        ";
        $query=$this->db->query($sql, array($date_from, $date_end));
        $result = $query->result_array();
        return $result;
    }

    //  取得裝置比例
    function getDeviceRatio($date_from, $date_end){
        $sql = "
            SELECT  count(user_id) as count, os
            FROM    login_history
            WHERE   date_format(login_time, '%Y-%m-%d') between ? and ?
            AND     os != ''
            GROUP BY os
        ";
        $query=$this->db->query($sql, array($date_from, $date_end));
        $result = $query->result_array();
        return $result;
    }

    //  取得領域學程完成比例
    function getCompleteRatio($date_from, $date_end){
        $sql = "
            SELECT  domain.domain_name as y, (sum(user_course.status) / count(user_course.status))*100 as a
            FROM    domain
            INNER JOIN tracks ON domain.domain_id = tracks.domain_id
            INNER JOIN tracks_course ON tracks_course.tracks_id = tracks.tracks_id
            INNER JOIN user_course ON user_course.course_id = tracks_course.course_id
            WHERE date_format(user_course.create_time, '%Y-%m-%d') between ? and ?
            GROUP BY domain.domain_id
            ORDER BY count(user_course.status) / sum(user_course.status) DESC
            LIMIT 5
        ";
        $query=$this->db->query($sql, array($date_from, $date_end));
        $result = $query->result_array();
        return $result;
    }

    //取得教師績效
    function getTeacherPerformance($date_from, $date_end){
        $sql = "
            SELECT  teacher_id, name
            FROM    teacher
            WHERE   1
        ";
        $query=$this->db->query($sql);
        $result = $query->result_array();
        $return = array();;
        foreach($result as $index=>$teacher){
            $return[$index]['name'] = $teacher['name'];
            // 課程數
            $course_count_sql = "
                SELECT  count(course_id) as course_count
                FROM    course
                WHERE   teacher_id = ?
            ";
            $query=$this->db->query($course_count_sql, array($teacher['teacher_id']));
            $course_count = $query->row_array();
            $return[$index]['course_count'] = (int)$course_count['course_count'];
            // 	平均註冊人數
            $return[$index]['av_reg_count'] = 0;
            // 平均完課率
            $return[$index]['av_end_count'] = 0;
            // 吸睛分數
            $return[$index]['gorgeous_num'] = 0;
            // 互動分數
            $return[$index]['interaction_num'] = 0;
            
            if($course_count['course_count']!=0){
                // 取得教師所有課程
                $course_sql = "
                    SELECT  course_id
                    FROM    course
                    WHERE   teacher_id = ?
                ";
                $query=$this->db->query($course_sql, array($teacher['teacher_id']));
                $course = $query->result_array();

                $temp_course = array(
                    'reg_count'=>0,
                    'end_count'=>0,
                    'gorgeous_num' =>0
                );
                foreach($course as $course_row){
                    // 取得課程人數
                    $user_course_sql = "
                        SELECT  count(user_id) as reg_count, sum(status) as end_count
                        FROM    user_course
                        WHERE   course_id = ?
                    ";
                    $query=$this->db->query($user_course_sql, array($course_row['course_id']));
                    $user_course = $query->row_array();

                    // 總註冊人數
                    $temp_course['reg_count'] = $temp_course['reg_count'] + $user_course['reg_count'];
                    // 總完課人數
                    $temp_course['end_count'] = $temp_course['end_count'] + $user_course['end_count'];

                    // 吸睛分數
                    $hit_sql ="
                        SELECT sum(hits) as gorgeous_num
                        FROM chapter_content
                        INNER JOIN chapter ON chapter.chapter_id = chapter_content.chapter_id
                        WHERE chapter.course_id = ?
                    ";
                    $query=$this->db->query($hit_sql, array($course_row['course_id']));
                    $hit = $query->row_array();
                    $temp_course['gorgeous_num'] = $temp_course['gorgeous_num'] + $hit['gorgeous_num'];
                }
                if($temp_course['reg_count']!=0){
                    // 	平均註冊人數
                    $return[$index]['av_reg_count'] = (int)($temp_course['reg_count'] / $course_count['course_count']);
                    // 平均完課率
                    $return[$index]['av_end_count'] = (int)(round(($temp_course['end_count'] / $temp_course['reg_count']), 2) * 100);
                    // 吸睛分數
                    $return[$index]['gorgeous_num'] = (int)$temp_course['gorgeous_num'];
                }
            }
        }
        return $return;
    }

    //教師取得所有課程數
    function getAllCourseCountsByTeacher($teacher_id){
        $sql = "select count(*) as course_counts from course where teacher_id=$teacher_id";
        $query=$this->db->query($sql);
        return $query;
    }

    //教師取得所有學生數
    function getAllStudentCountsByTeacher($teacher_id){
        $sql = "select count(distinct(uc.user_id)) as student_counts from user_course uc
                left join course c on uc.course_id=c.course_id
                where c.teacher_id=$teacher_id";
        $query=$this->db->query($sql);
        return $query;
    }

    //教師取得 Top 10 課程學生人數
    function getTopCourseStudentCountsByTeacher($teacher_id, $top){
        $sql = "select c.course_id,c.course_name,count(uc.user_id) as user_counts from course c 
                left join user_course uc on c.course_id=uc.course_id
                where c.teacher_id=$teacher_id
                group by c.course_id
                order by user_counts desc
                limit $top";
        $query=$this->db->query($sql);
        return $query;
    }

    //教師取得課程概況
    function getTeachedCourseSummary($date_from, $date_end, $teacher_id){
        $return = array();
        $sql = "
            SELECT  course_id, course_name, course_url
            FROM    course
            WHERE   teacher_id=$teacher_id
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        foreach($result as $index=>$course){
            $return[$index]['id'] = $course['course_id'];
            $return[$index]['name'] = $course['course_name'];
            // 課程修課人數, 完課人數, 完課率
            $user_count_sql = "
                SELECT  count(user_id) as user_counts, sum(status) as end_user_counts
                FROM    user_course
                WHERE   course_id = ?
            ";
            $query = $this->db->query($user_count_sql, array($course['course_id']));
            $user_count_query = $query->row_array();
            $return[$index]['user_counts'] = (int)$user_count_query['user_counts'];
            $return[$index]['end_user_counts'] = (int)$user_count_query['end_user_counts'];
            $return[$index]['end_ratio'] = 0;
            if($user_count_query['user_counts']!=0){
                $return[$index]['end_ratio'] = (int)(round(($user_count_query['end_user_counts'] / $user_count_query['user_counts']), 2) * 100);
            }
            //課程討論數
            $discussion_sql = "select count(*) as discussion_counts from forum_topic where course_url=?";
            $discussion_query = $this->db->query($discussion_sql, array($course['course_url']))->row_array();
            $return[$index]['discussion_counts'] = (int)$discussion_query['discussion_counts'];
        }
        return $return;
    }

    //Log 使用者點擊學程,課程,關卡,任務
    function logUserAccessObject($data){
        $this -> db -> insert('access_log', $data);
    }

}