<?php
class User_portfolio_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	//集合了個人履歷中相關的Modal，包含
	/*
	 * UserPortfolio
	 * UserAward
	 * CareerSkill
	 * UserEducation
	 * UserExperience
	 * UserLicense
	 * UserSkill
	 * UserSociety
	 * UserTraining
	 * PortfolioSkill
	 * Seniority
	 */
	function getUserPortfolio($email){
		$sql = "select user_portfolio.* from user_portfolio
				left join user on user_portfolio.user_id=user.user_id
				where user.email ='$email' order by portfolio_order";
    	$query=$this->db->query($sql);
		return $query;
	}
	
	function addUserPortfolio($user_portfolio){
		$sql="insert into user_portfolio (portfolio_name,portfolio_pic,portfolio_desc,hyperlink,portfolio_type,portfolio_order,user_id) values (?,?,?,?,?,?,?)";
		$query=$this->db->query($sql,$user_portfolio);
		return $this->db->affected_rows();
	}
	
	function updateUserPortfolio($user_portfolio_id,$user_portfolio){
		$sql = "update user_portfolio set portfolio_name=?,portfolio_pic=?,portfolio_desc=?,hyperlink=?,portfolio_type=?,portfolio_order=? where user_portfolio_id='$user_portfolio_id'";
    	$query=$this->db->query($sql,$user_portfolio);
		return $this->db->affected_rows();
	}

	function deleteUserPortfolio($user_id,$portfolio_order){
		$sql = "delete from user_portfolio where user_id ='$user_id' and portfolio_order='$portfolio_order'";
    	$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
	function getUserAward($email){
		$sql = "select user_award.* from user_award
		left join user on user_award.user_id=user.user_id
		where user.email ='$email' order by award_order";
		$query=$this->db->query($sql);
		return $query;
	}
	
	function addUserAward($user_award){
		$sql="insert into user_award (award_name,award_order,user_id) values (?,?,?)";
		$query=$this->db->query($sql,$user_award);
		return $this->db->affected_rows();
	}
	
	function updateUserAward($user_award_id,$user_award){
		$sql = "update user_award set award_name=?,award_order=? where user_award_id='$user_award_id'";
		$query=$this->db->query($sql,$user_award);
		return $this->db->affected_rows();
	}
	
	function deleteUserAward($user_id,$award_order){
		$sql = "delete from user_award where user_id ='$user_id' and award_order='$award_order'";
		$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}

	function getCareerSkill($email){
		$sql = "select user_career_skill.* from user_career_skill
		left join user on user_career_skill.user_id=user.user_id
		where user.email ='$email' order by career_skill_order";
		$query=$this->db->query($sql);
		return $query;
	}
	
	function addCareerSkill($user_career_skill){
		$sql="insert into user_career_skill (career_skill_name,career_skill_order,user_id) values (?,?,?)";
		$query=$this->db->query($sql,$user_career_skill);
		return $this->db->affected_rows();
	}
	
	function updateCareerSkill($user_career_skill_id,$user_career_skill){
		$sql = "update user_career_skill set career_skill_name=?,career_skill_order=? where user_career_skill_id='$user_career_skill_id'";
		$query=$this->db->query($sql,$user_career_skill);
		return $this->db->affected_rows();
	}
	
	function deleteCareerSkill($user_id,$career_skill_order){
		$sql = "delete from user_career_skill where user_id ='$user_id' and career_skill_order='$career_skill_order'";
		$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
	function getUserEducation($email){
		$sql = "select user_education.* from user_education
		left join user on user_education.user_id=user.user_id
		where user.email ='$email' order by education_order";
		$query=$this->db->query($sql);
		return $query;
	}
	
	function addUserEducation($user_education){
		$sql="insert into user_education (school_name,department,degree,education_start,education_start_month,education_end,education_end_month,until_now,education_order,user_id) values (?,?,?,?,?,?,?,?,?,?)";
		$query=$this->db->query($sql,$user_education);
		return $this->db->affected_rows();
	}
	
	function updateUserEducation($user_education_id,$user_education){
		$sql = "update user_education set school_name=? ,department=?,degree=?,education_start=?,education_start_month=?,education_end=?,education_end_month=?,until_now=?,education_order=? where user_education_id='$user_education_id'";
		$query=$this->db->query($sql,$user_education);
		return $this->db->affected_rows();
	}
	
	function deleteUserEducation($user_id,$education_order){
		$sql = "delete from user_education where user_id ='$user_id' and education_order='$education_order'";
		$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
	
	function getHighestDegree($email){
		$sql = "select user_education.* from user_education
		left join user on user_education.user_id=user.user_id
		where user.email ='$email' order by degree desc,education_start desc limit 1";
		$query=$this->db->query($sql);
		return $query;
	}
	function getUserExperience($email){
		$sql = "select user_experience.* from user_experience
		left join user on user_experience.user_id=user.user_id
		where user.email ='$email' order by experience_order";
		$query=$this->db->query($sql);
		return $query;
	}
	
	function addUserExperience($user_experience){
		$sql="insert into user_experience (company_name,job_title,experience_start,experience_start_month,experience_end,experience_end_month,until_now,experience_order,user_id) values (?,?,?,?,?,?,?,?,?)";
		$query=$this->db->query($sql,$user_experience);
		return $this->db->affected_rows();
	}
	
	function updateUserExperience($user_experience_id,$user_experience){
		$sql = "update user_experience set company_name=? ,job_title=?, experience_start=?, experience_start_month=?,experience_end=?,experience_end_month=?,until_now=?,experience_order=? where user_experience_id='$user_experience_id'";
		$query=$this->db->query($sql,$user_experience);
		return $this->db->affected_rows();
	}
	
	function deleteUserExperience($user_id,$experience_order){
		$sql = "delete from user_experience where user_id ='$user_id' and experience_order='$experience_order'";
		$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
	
	function getCurrentJob($email){
		$sql = "select user_experience.* from user_experience
		left join user on user_experience.user_id=user.user_id
		where user.email ='$email'and user_experience.experience_end='0' and user_experience.experience_end_month='0' order by experience_start desc limit 1";
		$query=$this->db->query($sql);
		return $query;
	}
	function getUserLicense($email){
		$sql = "select user_license.* from user_license
		left join user on user_license.user_id=user.user_id
		where user.email ='$email' order by license_order";
		$query=$this->db->query($sql);
		return $query;
	}
	
	function addUserLicense($user_license){
		$sql="insert into user_license (license_name,license_order,user_id) values (?,?,?)";
		$query=$this->db->query($sql,$user_license);
		return $this->db->affected_rows();
	}
	
	function updateUserLicense($user_license_id,$user_license){
		$sql = "update user_license set license_name=?,license_order=? where user_license_id='$user_license_id'";
		$query=$this->db->query($sql,$user_license);
		return $this->db->affected_rows();
	}
	
	function deleteUserLicense($user_id,$license_order){
		$sql = "delete from user_license where user_id ='$user_id' and license_order='$license_order'";
		$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
	function getUserSkill($email){
		$sql = "select user_skill.* from user_skill
		left join user on user_skill.user_id=user.user_id
		where user.email ='$email' order by skill_order";
		$query=$this->db->query($sql);
		return $query;
	}
	
	function addUserSkill($user_skill){
		$sql="insert into user_skill (skill_name,skill_order,user_id) values (?,?,?)";
		$query=$this->db->query($sql,$user_skill);
		return $this->db->affected_rows();
	}
	
	function updateUserSkill($user_skill_id,$user_skill){
		$sql = "update user_skill set skill_name=?,skill_order=? where user_skill_id='$user_skill_id'";
		$query=$this->db->query($sql,$user_skill);
		return $this->db->affected_rows();
	}
	
	function deleteUserSkill($user_id,$skill_order){
		$sql = "delete from user_skill where user_id ='$user_id' and skill_order='$skill_order'";
		$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
	function getUserSociety($email){
		$sql = "select user_society.* from user_society
		left join user on user_society.user_id=user.user_id
		where user.email ='$email'";
		$query=$this->db->query($sql);
		return $query;
	}
	
	function addUserSociety($user_society){
		$sql="insert into user_society (facebook,google_plus,twitter,linkedin,github,user_id) values (?,?,?,?,?,?)";
		$query=$this->db->query($sql,$user_society);
		return $this->db->affected_rows();
	}
	
	function updateUserSociety($user_id,$user_society){
		$sql = "update user_society set facebook=? ,google_plus=?,twitter=?,linkedin=?,github=? where user_id='$user_id'";
		$query=$this->db->query($sql,$user_society);
		return $this->db->affected_rows();
	}
	
	function deleteUserSociety($user_id){
		$sql = "delete from user_society where user_id ='$user_id'";
		$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
	function getUserTraining($email){
		$sql = "select user_training.* from user_training
		left join user on user_training.user_id=user.user_id
		where user.email ='$email' order by training_order";
		$query=$this->db->query($sql);
		return $query;
	}
	
	function addUserTraining($user_training){
		$sql="insert into user_training (training_name,training_unit,training_start,training_start_month,training_end,training_end_month,until_now,training_order,user_id) values (?,?,?,?,?,?,?,?,?)";
		$query=$this->db->query($sql,$user_training);
		return $this->db->affected_rows();
	}
	
	function updateUserTraining($user_training_id,$user_training){
		$sql = "update user_training set training_name=? ,training_unit=?, training_start=?, training_start_month=?,training_end=?,training_end_month=?,until_now=?,training_order=? where user_training_id='$user_training_id'";
		$query=$this->db->query($sql,$user_training);
		return $this->db->affected_rows();
	}
	
	function deleteUserTraining($user_id,$training_order){
		$sql = "delete from user_training where user_id ='$user_id' and training_order='$training_order'";
		$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
	function getPortfolioSkill($email){
		$sql = "select portfolio_skill.* from portfolio_skill
		left join user_portfolio on user_portfolio.user_portfolio_id=portfolio_skill.user_portfolio_id
		left join user on user_portfolio.user_id=user.user_id
		where user.email ='$email' order by user_portfolio_id,portfolio_skill_order";
		$query=$this->db->query($sql);
		return $query;
	}
	
	function getOnePortfolioSkill($email,$user_portfolio_id){
		$sql = "select portfolio_skill.* from portfolio_skill
		left join user_portfolio on user_portfolio.user_portfolio_id=portfolio_skill.user_portfolio_id
		left join user on user_portfolio.user_id=user.user_id where user.email ='$email'
		and portfolio_skill.user_portfolio_id='$user_portfolio_id' order by user_portfolio_id,portfolio_skill_order";
		$query=$this->db->query($sql);
		return $query;
	}
	
	function addPortfolioSkill($portfolio_skill){
		$sql="insert into portfolio_skill (user_portfolio_id,user_skill_id,portfolio_skill_order) values (?,?,?)";
		$query=$this->db->query($sql,$portfolio_skill);
		return $this->db->affected_rows();
	}
	
	function updatePortfolioSkill($portfolio_skill,$portfolio_skill_id){
		$sql = "update portfolio_skill set user_portfolio_id=?,user_skill_id=?,portfolio_skill_order=? where portfolio_skill_id='$portfolio_skill_id'";
		$query=$this->db->query($sql,$portfolio_skill);
		return $this->db->affected_rows();
	}
	
	function deletePortfolioSkill($user_portfolio_id,$portfolio_skill_order){
		$sql = "delete from portfolio_skill where  user_portfolio_id='$user_portfolio_id' and portfolio_skill_order='$portfolio_skill_order'";
		$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
	
	function getSeniority($email){
		$sql = "select seniority.* from seniority
		left join user on seniority.user_id=user.user_id
		where user.email ='$email' order by seniority_id,seniority_order";
		$query=$this->db->query($sql);
		return $query;
	}
	function addSeniority($seniority){
		$query=$this->db->insert('seniority',$seniority);
		return $this->db->insert_id();
	}
	
	function updateSeniority($seniority_id,$data){
		$this -> db -> where('seniority_id', $seniority_id);
		$this -> db -> update('seniority', $data);
	}
	function deleteSeniority($user_id,$seniority_order){
		$sql = "delete from seniority where user_id='$user_id' and seniority_order='$seniority_order'";
		$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
}