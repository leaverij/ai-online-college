<?php
class Job_vacancy_model extends CI_Model {
    function __construct()
    {
        parent::__construct();
		
    }
	function getJobVacancy($user_id){
		$sql = "select job_vacancy.*,jobs_category_name from job_vacancy left join jobs_category on jobs_category.jobs_category_id = job_vacancy.jobs_category_id where user_id='$user_id'";
    	$query=$this->db->query($sql);
		return $query;
	}
	function getOneJobVacancy($job_vacancy_id){
		$sql = "select job_vacancy.*,user.company_img,user.company_name,user.company_website from job_vacancy left join user on user.user_id=job_vacancy.user_id where job_vacancy_id='$job_vacancy_id'";
		$query=$this->db->query($sql);
		return $query;
	}
	function addJobVacancy($job_vacancy){
		$query=$this->db->insert('job_vacancy',$job_vacancy);
		return $this->db->insert_id();
	}
	
	function updateJobVacancy($job_vacancy_id,$data){
		$this -> db -> where('job_vacancy_id', $job_vacancy_id);
		$this -> db -> update('job_vacancy', $data);
	}

	function deleteJobVacancy($user_id,$job_vacancy_id){
		$sql = "delete from job_vacancy where user_id ='$user_id' and job_vacancy_id='$job_vacancy_id'";
    	$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}
	
	function compositeQueryJobVacancy($factor_array){
		$sql = "select user.name,user.email,user.alias,user.img,user.resume_img,user.intro,s1.highest_degree,user.autobiography,user.location,user.match_jobs_category from user 
				left join user_skill on user.user_id=user_skill.user_id 
				left join user_license on user.user_id=user_license.user_id 
				left join seniority on user.user_id=seniority.user_id 
				left join	(	select user_education.user_id,max(user_education.degree) as highest_degree 
								from user_education 
								left join user on user.user_id=user_education.user_id
								group by user_education.user_id
							) as s1 on s1.user_id=user.user_id".$this->get_WhereCondition($factor_array)." group by user.user_id";
		// echo $sql.'<br>';
		$query=$this->db->query($sql);
		return $query;
	}
	
	function getVacancyMatchFactor($job_vacancy_id){
		$sql = "select job_vacancy_id,job_name,job_vacancy.jobs_category_id,match_county,match_district,degree_limit,allowed_condition,department_type_id,skill,license,additional_demand,jobs_category.jobs_category_name,working_experience from job_vacancy left join jobs_category on jobs_category.jobs_category_id = job_vacancy.jobs_category_id where job_vacancy_id='$job_vacancy_id'";
		$query=$this->db->query($sql);
		// echo $sql.'<br>';
		return $query;		
	}
	function get_WhereCondition($factor_array){
		$sql='';
		$count=0;
		foreach($factor_array['0'] as $key=>$value){
			// 			echo $key.' = '.$value.'<br>';
			if($value!=null){
// 				if($key!='job_vacancy_id'&&$key!='job_name'&&$key!='allowed_condition'&&$key!='department_type_id'&&$key!='additional_demand'&&$key!='skill'&&$key!='working_experience'&&$key!='jobs_category_name'){
				if($key=='jobs_category_id'||$key=='degree_limit'||$key=='skill'||$key=='license'||$key=='working_experience'){
					$count++;
					if(is_array($value)){
						$aCondition = $this->get_aCondition($key, $value);
					}elseif(is_string($value)){
						$aCondition = $this->get_aCondition($key, trim($value));
					}
					if ($count == 1){
						$sql.=' where ' . $aCondition;
					}else{
						$sql.=' and ' . $aCondition;
					}
				}
			}
		}
		if($count==0){
			$sql.=' where recruiters =1';
		}else{
			$sql.=' and recruiters =1';
		}
		return $sql;
	}
	
	function get_aCondition($columnName,$value){
		$aCondition='';
		if(	'match_county'==$columnName||'match_district'==($columnName)){
			$aCondition='location like \'%'.$value.'%\' ';
		}elseif('degree_limit'==$columnName){
			$aCondition='s1.highest_degree >= '.$value;
		}elseif('working_experience'==$columnName){
			$aCondition='seniority.year >='.$value;
		}elseif('allowed_condition'==$columnName){
			if(is_array($value)){
				$array_count=0;
				foreach($value as $key1=>$value1){
					if($array_count==0){
						$aCondition=$columnName.' like \'%'.$value.'%\'';
					}elseif($array_count+1<=count($value)){
						$aCondition=$columnName.'or like \'%'.$value.'%\'';
					}
					$array_count++;
				}
			}else{
				$aCondition=$columnName.' like \'%'.$value.'%\'';
			}
		}elseif('jobs_category_id'==$columnName){
			$aCondition='find_in_set('.$value.',match_jobs_category)>0 and seniority.jobs_category_id= '.$value;
		}elseif('skill'==$columnName){
			$temp='';
			$array=explode(',',$value);
			for($i=0;$i<count($array);$i++){
				if($i+1==count($array)){
					$temp.=('\''.$array[$i].'\'');
				}else{
					$temp.=('\''.$array[$i].'\',');
				}
			}
			$aCondition='lower(user_skill.skill_name) in ('.strtolower($temp).')';
		}elseif('license'==$columnName){
			$temp='';
			$array=explode(',',$value);
			for($i=0;$i<count($array);$i++){
				if($i+1==count($array)){
					$temp.=('\''.$array[$i].'\'');
				}else{
					$temp.=('\''.$array[$i].'\',');
				}
			}
			$aCondition='lower(user_license.license_name) in ('.strtolower($temp).')';
		}else{
			$aCondition=$columnName.' = '.$value;
		}
		return $aCondition;
	}

	function getApplyPerson($job_vacancy_id){
		$sql = "SELECT * FROM job_apply 
		INNER JOIN user ON  job_apply.user_id = user.user_id
		WHERE job_apply.job_vacancy_id = '$job_vacancy_id'";
		$query=$this->db->query($sql);
		return $query;		
	}
}