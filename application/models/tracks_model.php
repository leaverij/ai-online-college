<?php
class Tracks_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	//檢查使用者是否有學程地圖
	function checkUserTracks($email){
		$sql = "select * from user_tracks,user
				where user.user_id = user_tracks.user_id
				and user.email = '$email'";
    	$query=$this->db->query($sql);
    	return $query->num_rows()==0?"0":"1";	
	}
	//取得學程地圖的學程資訊
	function getAllTracks()
	{
		$sql="select * from tracks";
    	$query=$this->db->query($sql);
    	return $query;
	}
	//取得使用者學程地圖的學程資訊
	function getUserTracks($email)
	{
		$sql = "select * from tracks left join
					(select name,email,user_tracks.tracks_date,tracks_id
        			from user_tracks,user
        			where user.user_id = user_tracks.user_id
        			and user_tracks.learning_status='1'
        			and user.email='$email' order by user_tracks.tracks_date desc LIMIT 0,1) b
				on (tracks.tracks_id = b.tracks_id )
				order by b.tracks_date desc,tracks.tracks_id asc";
    	$query=$this->db->query($sql);
    	return $query;
	}
	//用tracks_url 取得tracks_id
	function getTracksId($tracks_url){
		$sql = "select * from tracks where tracks_url ='$tracks_url'";
		$query=$this->db->query($sql);
    	return $query;
	}
	//新增UserTracks使用者訂閱紀錄
	function addUserTracks($data)
	{
		$this -> db -> insert('user_tracks', $data);
	}
	//取得學程地圖內的各課程與描述
	function getTracksContent($tracks_url)
	{
		$sql = "select * from tracks,tracks_course,course
				where tracks.tracks_id = tracks_course.tracks_id
				and tracks_course.course_id = course.course_id
				and tracks.tracks_url = '$tracks_url'";
		$query=$this->db->query($sql);
    	return $query;
	}
	//正在上學程地圖的人數
	function getTracksPeopleNo($tracks_url){
		$sql = "select count(pno.peopleNo) count
				from (select count(*) peopleNo
						from tracks,user_tracks
						where tracks.tracks_id = user_tracks.tracks_id
						and tracks.tracks_url='$tracks_url'
						and user_tracks.learning_status = '1'
        				group by user_tracks.user_id,user_tracks.tracks_id) pno";
		$query=$this->db->query($sql);
    	return $query;
	}
	//取得學程地圖Tracks裡的課程與章節
	// function getTracksCourseChapter($tracks_url){
		// $sql = "select * from tracks,tracks_course,library,course,chapter
				// where tracks.tracks_id = tracks_course.tracks_id
				// and tracks_course.course_id = course.course_id
				// and library.library_id = course.library_id
				// and course.course_id = chapter.course_id
				// and tracks.tracks_url = '$tracks_url'";
		// $query=$this->db->query($sql);
    	// return $query;
	// }
	//為了要對應tracks裡學習過的chapter所以要取得使用者學習過的chapter
	function getUserTracksChapter($user_id,$tracks_url){
		$sql = "select *,user_course.status course_pass from user_course,user_chapter,tracks,tracks_course
				where user_course.user_course_id = user_chapter.user_course_id
				and tracks.tracks_id = tracks_course.tracks_id
				and user_course.course_id = tracks_course.course_id
				and user_chapter.status='1'
				and user_course.user_id='$user_id'
				and tracks.tracks_url = '$tracks_url'";
		$query = $this->db->query($sql);
    	return $query;
	}
	//trackcontent那一頁的資訊
	//包含track名稱、描述、課程名稱、以及各課程的章節
	function getTrackContent($tracks_url){
		$sql = "select *,course.course_id courseKey from tracks,tracks_course,library,course left join chapter
        		on course.course_id = chapter.course_id
        		where tracks.tracks_id = tracks_course.tracks_id
				and tracks_course.course_id = course.course_id
				and library.library_id = course.library_id
				and tracks.tracks_url = '$tracks_url'";
		$query = $this->db->query($sql);
    	return $query->result();
	}
	//檢查是否為正在挑戰的學程
	function checkLearningStatus($tracks_url,$user_id){
		$sql = "select * from user_tracks,tracks
				where tracks.tracks_id = user_tracks.tracks_id
				and tracks.tracks_url = '$tracks_url'
				and user_tracks.user_id='$user_id'
				order by user_tracks.learning_status desc";
		$query = $this->db->query($sql);
    	return $query;
	}
	//檢查使用者是否有挑戰任何學程
	function checkLearningTracks($user_id){
		$sql = "select * from user_tracks
				where user_tracks.learning_status = '1'
				and user_tracks.user_id='$user_id'";
		$query = $this->db->query($sql);
    	return $query;
	}
	//取得domain的url
	// function getDomainUrl($tracks_url){
		// $sql = "select * from domain,tracks
				// where domain.domain_id = tracks.domain_id
				// and tracks.tracks_url ='$tracks_url'";
		// $query = $this->db->query($sql);
    	// return $query;
	// }
	//取得domain的url
	function getJobsCategoryUrl($tracks_url){
		$sql = "select * from tracks,jobs_category_tracks,jobs_category
				where tracks.tracks_id = jobs_category_tracks.tracks_id
				and jobs_category_tracks.jobs_category_id = jobs_category.jobs_category_id
				and tracks.tracks_url ='$tracks_url'";
		$query = $this->db->query($sql);
    	return $query;
	}
	//取消當下訂閱的學程
	function unsubscribe($tracks_url,$user_id){
		$sql = "UPDATE user_tracks t1
				inner join tracks t2
             	ON t1.tracks_id = t2.tracks_id
				SET t1.learning_status = '0'
				WHERE t2.tracks_url = '$tracks_url'
				and user_id='$user_id'";
		$query = $this->db->query($sql);
	}
	//取消全部訂閱的學程
	function updateTracksLearningStatus($user_id){
		$data = array('learning_status'=>'0');
		$this -> db -> where('user_id', $user_id);
		$this -> db -> update('user_tracks', $data);
	}
	//取得學程的授課講師資訊
	function getTrackTeacher($tracks_url){
		$sql = "select * from tracks,tracks_teacher,teacher
				where tracks.tracks_id = tracks_teacher.tracks_id
				and teacher.teacher_id = tracks_teacher.teacher_id
				and tracks.tracks_url = '$tracks_url'";
		$query = $this->db->query($sql);
    	return $query;
	}
	//取得學程進度資訊(progress bar)
	function getTrackProgress($tracks_url,$user_id){
		$sql = "select *,course.course_id courseKey ,ifnull(user_course.status,'0') course_pass
				from tracks,tracks_course,library,course left join user_course
        		on course.course_id = user_course.course_id and user_course.user_id='$user_id'
        		where tracks.tracks_id = tracks_course.tracks_id
				and tracks_course.course_id = course.course_id
				and library.library_id = course.library_id
				and tracks.tracks_url = '$tracks_url'";
		$query = $this->db->query($sql);
    	return $query;
	}
	//使用職務分類的url取得使用者是否有訂閱學程的資料
	function getSubscribeTrack($email){
		$sql = "select * from jobs_category,jobs_category_tracks,user_tracks,`user`
				where jobs_category.jobs_category_id = jobs_category_tracks.jobs_category_id
				and user_tracks.tracks_id = jobs_category_tracks.tracks_id
				and user_tracks.user_id = user.user_id
				and user_tracks.learning_status='1'
				and user.email='$email'";
		$query = $this->db->query($sql);
    	return $query;
	}
	//取得職務分類資料
	function getJobsCategory(){
		$sql = "select * from jobs_category where status='1'";
		$query = $this->db->query($sql);
		return $query;		
	}
	//登入後取得學程價格資訊
	function getTracksPrice($tracks_id){
		$user_id = $this->session->userdata('user_id');
		$sql = "select * from tracks,tracks_course,course left join preferential
				on course.course_id = preferential.course_id and current_date() between start_time and end_time
				left join order_detail
				on course.course_id = order_detail.course_id and order_detail.user_id = '$user_id'
				and order_detail.live_flag='1'
				where tracks.tracks_id = '$tracks_id'
				and tracks.tracks_id = tracks_course.tracks_id
				and tracks_course.course_id = course.course_id";
		$query = $this->db->query($sql);
    	return $query;
	}
}