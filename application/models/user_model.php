<?php
class User_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		$email = $this->session->userdata('email');
    }
	//網站會員人數
	function amount()
	{
		$sql="select format(count(*),0) amount from user";
    	$query=$this->db->query($sql);
    	return $query;
	}

	//驗證coupon是否有效
	function checkCoupon($coupon)
    {
    	$time = $this->theme_model->nowTime();
    	$sql="select * from coupon where coupon_code='$coupon' and used_flag='0' and end_time >= '$time'";
    	$query=$this->db->query($sql);
    	return $query->num_rows()==0?"0":"1";		
    }
	//檢查email是否可以使用
	function checkEmail($email)
    {
    	$sql="select * from user where email='$email'";
    	$query=$this->db->query($sql);
    	return $query->num_rows()==0?"0":"1";		
    }
	//檢查fb_email是否可以使用
	function checkFbEmail($email)
    {
    	$sql="select * from user where email='$email'";
    	$query=$this->db->query($sql);
    	return $query;	
    }
	//檢查alias別名是否可以使用
	function checkAlias($alias)
    {
    	$sql="select * from user where alias='$alias'";
    	$query=$this->db->query($sql);
    	return $query->num_rows()==0?"0":"1";		
    }
	//更新個人資料時，檢查alias別名是否可以使用
	function checkEmailAlias($alias,$email)
    {
    	$sql="select * from user where alias='$alias' and email!='$email'";
    	$query=$this->db->query($sql);
    	return $query->num_rows()==0?"0":"1";		
    }
    //更新個人資料時，檢查email是否可以使用
    function checkContentEmail($newMail,$email)
    {
    	$sql="select * from user where email='$newMail' and email!='$email'";
    	$query=$this->db->query($sql);
    	return $query->num_rows()==0?"0":"1";	
    }
	//列出one_coupon相關欄位
    function getOneCoupon($coupon)
	{
		$time = $this->theme_model->nowTime();
    	$sql="select * from coupon where coupon_code='$coupon' and used_flag='0' and end_time >= '$time'";
    	$query=$this->db->query($sql);
		return $query;
	}
	//修改coupon
	function updateCoupon($coupon,$data)
	{
		$this -> db -> where('coupon_code', $coupon);
		$this -> db -> update('coupon', $data);
	}
	//新增user
	function addUser($data)
	{
		$this -> db -> insert('user', $data);
	}
	//更新user
	function updateUser($email,$data)
	{
		$this -> db -> where('email', $email);
		$this -> db -> update('user', $data);
	}
	//列出one_user相關欄位
    function getOneUser($email)
	{
		$sql = "select * from user 
				where user.email='$email'";
		$query = $this->db->query($sql);
		return $query;
	}
    function getOneUserByUserId($user_id)
	{
		$sql = "select * from user 
				where user.user_id='$user_id'";
		$query = $this->db->query($sql);
		return $query;
	}	
	//檢查帳號有效時間
	function checkCouponTime($email)
	{
		$sql = "select * from coupon
				where user_email='$email'
				and now() between start_time and end_time";
		$query = $this->db->query($sql);
		return $query;
	}
	//檢查coupon開始時間小於今日
	function checkCouponLtTime($email)
	{
		$sql = "select * from coupon
				where user_email='$email'
				and now() < start_time";
		$query = $this->db->query($sql);
		return $query;
	}
	//user 得到的chapter徽章以及chapter資訊
	function getUserChapterBadge($email)
	{
		$sql = "select * from user,user_course,user_chapter,chapter,course,library
				where user_course.user_course_id=user_chapter.user_course_id
				and user_chapter.chapter_id=chapter.chapter_id
				and user_course.user_id=user.user_id
				and user_chapter.status=1
        		and chapter.course_id = course.course_id
        		and library.library_id = course.library_id
				and user.email='$email'";
		$query = $this->db->query($sql);
		return $query;
	}
	//facebook照片insert
	function fb_pic_save($fid)
	{
		//設定facebook 使用者圖片
		if($fid!=''){
		$img = file_get_contents('http://graph.facebook.com/'.$fid.'/picture?type=large');
		$file = dirname(__file__).'/../../assets/img/user/128x128/'.$fid.'.jpg';
		file_put_contents($file, $img);	
		$update_data = array('img' => $fid.'.jpg');
		$this->db->where('fb_id', $fid);
		$this->db->update('user', $update_data);
		}else return 0;
	}
	//非會員第一次使用facebook登入
	function fb_insert_new($data)
	{
		$this -> db -> insert('user', $data);
	}
	//會員第一次使用facebook登入
	function fb_update($email,$data)
	{
		$this -> db -> where('email', $email);
		$this -> db -> update('user', $data);
	}
	//忘記密碼：使用者點選忘記密碼時，取得email等相關資訊然後塞進資料庫
	function addForgetPassword($data)
	{
		$this -> db -> insert('forget_password', $data);
	} 
	//輸入email，取得別名
	function getAlias($email)
	{
		$sql = "select alias from user where email='$email'";
		$query = $this->db->query($sql);
		return $query;
	}
	//檢查忘記密碼傳過來的驗證碼
	function checkCode($code)
	{
		$sql = "select * from forget_password
				where code='$code'
				and forget_password_used_flag='0'
				and NOW() <= DATE_ADD(`time`, INTERVAL 1 HOUR)";
		$query = $this->db->query($sql);
		return $query;
	}
	//更新密碼的同時，update forget_password的資料
	function updateForgetPassword($code,$data)
	{
		$this -> db -> where('code', $code);
		$this -> db -> update('forget_password', $data);
	}
	//使用者點擊email的啟動連結，回傳emal
	function getEmail($activeCode)
	{
		$sql = "select email,active_flag from user where active_code='$activeCode'";
		$query = $this->db->query($sql);
		return $query;
	}
	//使用email去取得帳號是否啟動
	function getEmailActive($email)
	{
		$sql = "select email,active_flag from user where email='$email'";
		$query = $this->db->query($sql);
		return $query;
	}
	//使用email取得使用者照片
	function getImg($email)
	{
		$sql = "select img from user where email='$email'";
		$query = $this->db->query($sql)->row_array();
		return $query['img'];
	}
	//取得使用者在學習的學程
	function getUserTracks($email){
		$sql = "select tracks.tracks_id,tracks.tracks_name,tracks.tracks_desc,tracks.tracks_url,tracks.pic
				from user,user_tracks,tracks
				where user.user_id =user_tracks.user_id
				and user_tracks.tracks_id = tracks.tracks_id
				and user.email = '$email'";
		$query = $this->db->query($sql);
		return $query;
	}
	//取得使用者再學習的學程裡面的課程
	function getUserTracksCourse($email){
		$sql = "select tracks.tracks_id,tracks_name,tracks_course.course_id,course.course_name,course.course_desc,course.course_pic
				from user,user_tracks,tracks,tracks_course left join course on tracks_course.course_id = course.course_id
				where user.user_id =user_tracks.user_id
				and user_tracks.tracks_id = tracks.tracks_id
				and tracks.tracks_id = tracks_course.tracks_id
				and user.email = '$email'
				order by user_tracks.status desc";
		$query = $this->db->query($sql);
		return $query;
	}
	//檢查使用者是否登入
	//有登入則繼續操作系統，未登入導引到登入頁面
	function isLogin()
	{
		if($this->session->userdata('email')){
			return true;
		}else{
			$this->theme_model->flash_session("alert-danger","您尚未登入AIOnlineCollege！");
			$this->session->set_userdata('src', $_SERVER['HTTP_REFERER']);
			redirect('home/login');	
		}
	}
	//檢查user是否為super user//勞委會拉~~~~
	function isSuper($email){
		$check = $this->db->query("select * from user where email='$email' and permissions_flag = 2");
		if($check->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	//紀錄webcam拍下的照片檔案名稱
	function saveWebCamPic($data)
	{
		$this -> db -> insert('web_cam_pic', $data);
	} 
	function getPicture($email){
		$sql = "select * from user,web_cam_pic
				where user.user_id = web_cam_pic.user_id
				and user.email ='$email'";
    	$query=$this->db->query($sql);
		return $query;
	}
	//check使用者有無在研討會寫入的權限
	function isSeminar(){
		$email = $this->session->userdata('email');
		$check = $this->db->query("select * from user where email='$email' and seminar_flag = 1");
		if($check->num_rows()>0){
			return true;
		}else{
			redirect('page/message/permission');
			return false;
		}
	}
	//check使用者有無在產業新聞寫入的權限
	function isIndustry(){
		$email = $this->session->userdata('email');
		$check = $this->db->query("select * from user where email='$email' and industry_flag = 1");
		if($check->num_rows()>0){
			return true;
		}else{
			redirect('page/message/permission');
			return false;
		}
	}
	//check使用者有無在企業徵才寫入的權限
	function isJobs(){
		$email = $this->session->userdata('email');
		$check = $this->db->query("select * from user where email='$email' and jobs_flag = 1");
		if($check->num_rows()>0){
			return true;
		}else{
			redirect('page/message/permission');
			return false;
		}
	}
	//check使用者有無在國際資源寫入的權限
	function isResource(){
		$email = $this->session->userdata('email');
		$check = $this->db->query("select * from user where email='$email' and resource_flag = 1");
		if($check->num_rows()>0){
			return true;
		}else{
			redirect('page/message/permission');
			return false;
		}
	}
	//check使用者是否有檢視後台的權限
	function isBackend(){
		$email = $this->session->userdata('email');
		$check = $this->db->query("select * from user where email='$email' and backend_flag = 1");
		if($check->num_rows()>0){
			return true;
		}else{
			redirect('page/message/permission');
			return false;
		}
	}
	//check使用者後台權限(將後台入口顯示在nav bar)
	function checkBackend(){
		$email = $this->session->userdata('email');
		$check = $this->db->query("select * from user where email='$email' and backend_flag = 1");
		if($check->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	function getUserByAlias($alias){
		$sql = "select * from user
				where user.alias ='$alias'";
    	$query=$this->db->query($sql);
		return $query;
	}
	function isResumeOpen($email){
    	$sql="select recruiters from user where email='$email' and recruiters=1";
    	$query=$this->db->query($sql);
    	return $query->num_rows()==0?"0":"1";	
	}
	function toggleResumeFlag($email,$recruiters){
		$sql="update user set recruiters='$recruiters' where email='$email'";
		$query=$this->db->query($sql);
		return $query;
	}
	//驗證舊密碼
	function checkPassword($email,$password){
		$sql="select * from user where email='$email' and password='$password'";
    	$query=$this->db->query($sql);
    	return $query->num_rows()==0?"0":"1";
	}
	//check使用者是否有批次寄信的權限
	function isBacthEmail(){
		$email = $this->session->userdata('email');
		$check = $this->db->query("select * from user where email='$email' and sendemail_flag = 1");
		if($check->num_rows()>0){
			return true;
		}else{
			redirect('page/message/permission');
			return false;
		}
	}
	//check使用者是否有批次寄信的權限(顯示在nav bar)
	function checkBacthEmail(){
		$email = $this->session->userdata('email');
		$check = $this->db->query("select * from user where email='$email' and sendemail_flag = 1");
		if($check->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	//check使用者是否有新增coupon的權限
	function isCoupon(){
		$email = $this->session->userdata('email');
		$check = $this->db->query("select * from user where email='$email' and coupon_flag = 1");
		if($check->num_rows()>0){
			return true;
		}else{
			redirect('page/message/permission');
			return false;
		}
	}
	//紀錄使用者登入資料
	function insertLoginHistory($data)
	{
		$this->db->insert('login_history', $data); 
	}
	
	function checkJob(){
		$email = $this->session->userdata('email');
		$check = $this->db->query("select * from user where email='$email' and jobs_flag = 1");
		if($check->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	//取得使用者購買的學程
	function getUserBuyTracks(){
	    $user_id = $this->session->userdata('user_id');
	    $sql = "select * from tracks,course_order where tracks.tracks_id = course_order.tracks_id and course_order.user_id = '$user_id' and course_order.tracks_id != 0";
        $query=$this->db->query($sql);
        return $query;
	}
    //取得使用者購買的學程細節
    function getUserBuyTracksDetail(){
        $user_id = $this->session->userdata('user_id');
        $sql = "select tracks.tracks_id,tracks.tracks_name,course.course_id,course.course_url,course.course_name,course.course_category_id,course_category.category_color
                from tracks,course_order left join tracks_course
                on course_order.tracks_id = tracks_course.tracks_id ,course,course_category
                where tracks.tracks_id = course_order.tracks_id
                and tracks_course.course_id = course.course_id
                and course.course_category_id = course_category.course_category_id
                and course.sync_course = 0
                and course_order.user_id = '$user_id'
                and course_order.tracks_id != 0";
        $query=$this->db->query($sql);
        return $query;
    }
    //取得使用者購買的課程(如有購買學程，這邊就不會顯示學程內的課程)
    function getUserBuyCourse(){
        $user_id = $this->session->userdata('user_id');
        $sql = "select * from course_order left join order_detail on course_order.order_id = order_detail.order_id ,course
                where course_order.course_id=course.course_id
                and course.course_id not in (select course.course_id
                from tracks,course_order left join tracks_course
                on course_order.tracks_id = tracks_course.tracks_id ,course
                where tracks.tracks_id = course_order.tracks_id
                and tracks_course.course_id = course.course_id
                and course_order.user_id = '$user_id'
                and course_order.tracks_id != 0)
                and course_order.user_id = '$user_id'
                and course_order.course_id != 0";
        $query=$this->db->query($sql);
        return $query;
    }
    //取得使用者購買的課程底下的關卡資訊
    function getUserBuyCourseChapter($course_url){
        $user_id = $this->session->userdata('user_id');
        $sql = "select * from order_detail,`user`,course,chapter
                where order_detail.user_id ='$user_id'
                and order_detail.user_id = user.user_id
                and order_detail.course_id = course.course_id
                and course.course_id = chapter.course_id
                and course.course_url = '$course_url'";
        $query=$this->db->query($sql);
        return $query;
	}
	
	//檢查是否是教師
	function check_teacher_flag($user_mail)
	{
		$this->db->where('email',$user_mail);
		$row=$this->db->get('user')->row();
		return $row->teacher_flag;
	}
	//取得使用者在學習的學程
	function getAllUser(){
		$sql = "select user_id,name,email,alias,backend_flag,teacher_flag,enterprise_flag from user order by user_id";
		$query = $this->db->query($sql);
		return $query;
	}
	// 更新後台權限
	function updateBackendFlag($user_id,$backend_flag){
		$sql="update user set backend_flag='$backend_flag' where user_id='$user_id'";
		$query=$this->db->query($sql);
		return $query;
	}
	// 更新老師權限
	function updateTeacherFlag($user_id,$teacher_flag){
		$sql="update user set teacher_flag='$teacher_flag' where user_id='$user_id'";
		$query=$this->db->query($sql);
		// var_dump($this->db->last_query());die;
		return $query;
	}
	//check使用者是否老師的權限
	function isTeacher(){
		$email = $this->session->userdata('email');
		$check = $this->db->query("select * from user where email='$email' and teacher_flag = 1");
		if($check->num_rows()>0){
			return true;
		}else{
			redirect('page/message/permission');
			return false;
		}
	}
	// user_id 是廠商,$user2_id是應徵者
	function hrCheckUserPortfolio($user_id,$user2_id)
    {
    	$sql="select job_apply.* from job_vacancy,job_apply where job_vacancy.user_id='$user_id' and job_vacancy.job_vacancy_id=job_apply.job_vacancy_id and job_apply.user_id='$user2_id'";
    	$query=$this->db->query($sql);
    	return $query->num_rows()==0?"0":"1";		
    }

	// 更新企業權限
	function updateEnterpriseFlag($user_id,$enterprise_flag){
		$sql="update user set enterprise_flag='$enterprise_flag' where user_id='$user_id'";
		$query=$this->db->query($sql);
		// var_dump($this->db->last_query());exit;
		return $query;
	}
}