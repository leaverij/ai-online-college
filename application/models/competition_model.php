<?php

class Competition_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }

    //取得競賽主題清單
    function getCompetitionTopic(){
        $sql = "select * from competition_topic where is_active=true order by name asc";
		$query = $this->db->query($sql);
    	return $query;
    }

    //取得競賽首頁主題
    function getHomeCompetitionTopics(){
        $sql = "select * from competition_topic where is_active=true order by name asc limit 4";
		$query = $this->db->query($sql);
    	return $query;
    }

    //取得競賽類別清單
    function getCompetitionCategory(){
        $sql = "select * from competition_category where is_active=true order by name asc";
		$query = $this->db->query($sql);
    	return $query;
    }

    //取得競賽單一類別
    function getOneCompetitionCategory($category_id){
        $sql = "select * from competition_category where competition_category_id=$category_id";
		$query = $this->db->query($sql);
    	return $query;
    }

    //取得所有競賽類別By競賽主題
    function getCompetitionCatgoriesByTopicId($topic_id){
        $sql = "select * from competition_category where competition_topic_id=$topic_id and is_active=true order by name asc";
		$query = $this->db->query($sql);
    	return $query;
    }

    //建立新競賽
    function createNewCompetition($data){
        $this -> db -> insert('competition', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
    }

    //取得單一競賽
    function getHostedCompetition($creator_id, $competition_id){
		$sql = "select * from competition where creator_id=? and competition_id=?";       
		$query=$this->db->query($sql, array($creator_id, $competition_id));
		return $query;
    }

    //取得所有競賽By競賽主題
    function getCompetitionByTopicId($competition_topic_id, $search_string, $per_page, $offset){
        $sql = "select c.*,cc.name as category_name,ct.name as topic_name from competition c,competition_topic ct,competition_category cc
                where c.competition_topic_id=ct.competition_topic_id and c.competition_category_id=cc.competition_category_id and c.competition_topic_id=$competition_topic_id  and c.name like '%$search_string%' limit $per_page offset $offset";
        $query=$this->db->query($sql);
        return $query;
    }

    //取得所有競賽主題數量
    function getCompetiotionTopicTotal(){
        $sql = "select ct.competition_topic_id,ct.name,sum(c.competition_id is not null) as counts from competition_topic ct
                left join competition c on ct.competition_topic_id=c.competition_topic_id
                group by ct.competition_topic_id";
        $query=$this->db->query($sql);
        return $query;                
    }

    //取得所有競賽主題為搜尋字串的數量
    function getCompetitionTopicTotalBySearchString($competition_topic_id, $search_string){
        $sql = "select competition_topic_id, name, count(competition_topic_id and name) as counts from competition where name like '%$search_string%' and competition_topic_id='$competition_topic_id'";
        $query=$this->db->query($sql);
        return $query;
    }

    //更新競賽基本資訊
	function updateCompetitionInfo($competition_id, $creator_id, $competition_name, $competition_subtitle, $competition_category, $competition_publish, $competition_starttime, $competition_endtime, $competition_deadline, $competition_topic){
		$sql="update competition set name='$competition_name', subtitle='$competition_subtitle', competition_category_id=$competition_category, competition_topic_id=$competition_topic, is_publish=$competition_publish, start_time='$competition_starttime', end_time='$competition_endtime', deadline='$competition_deadline' where competition_id=$competition_id and creator_id=$creator_id ";
        $this->db->query($sql);
    }
    
    //教師取得競賽細節
    function getCompetitionDetails($competition_id, $creator_id){
        $sql = "select * from competition_detail where competition_id=$competition_id and creator_id=$creator_id and is_delete=false order by create_time asc";
        $query=$this->db->query($sql);
		return $query;
    }

    //教師取得單一競賽細節
    function getOneCompetitionDetail($competition_id, $competition_detail_id){
        $sql = "select * from competition_detail where competition_detail_id=$competition_detail_id and competition_id=$competition_id";
        $query=$this->db->query($sql);
		return $query;
    }

    //建立新競賽細節
    function createNewCompetitionDetail($data){
        $this -> db -> insert('competition_detail', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
    }

    //更新競賽細節
    function updateCompetitionDetail($competition_detail_title, $competition_detail_content, $competition_detail_publish, $competition_detail_id, $competition_id, $creator_id){
        $sql = "update competition_detail set title='$competition_detail_title', content='$competition_detail_content', is_publish=$competition_detail_publish where competition_detail_id=$competition_detail_id and competition_id=$competition_id and creator_id=$creator_id";
        $this->db->query($sql);
    }

    //取得單一競賽
    function getHostedList($creator_id, $qdata){
        $querystring = '';
        $nowdate = date('Y-m-d H:i:s');
        if(!empty($qdata['is_publish'])){
            $querystring .= ' AND competition.is_publish = '.$qdata['is_publish'];
        }
        if(!empty($qdata['is_public'])){
            $querystring .= ' AND competition.is_public = '.$qdata['is_public'];
        }
        if(!empty($qdata['competition_category_id'])){
            $querystring .= ' AND competition.competition_category_id = '.$qdata['competition_category_id'];
        }
        if(!empty($qdata['name'])){
            $querystring .= " AND competition.name = '".$qdata['name']."'";
        }
        if(!empty($qdata['processing'])){
            if($qdata['processing']=='0'){
                $querystring .= " AND competition.start_time > '$nowdate'";
            }else if($qdata['processing']=='1'){
                $querystring .= " AND competition.start_time <= '$nowdate' AND competition.end_time >= '$nowdate'";
            }else{
                $querystring .= " AND competition.end_time < '$nowdate'";
            }
        }
        $sql = "
            SELECT  competition.*, competition_category.name as category_name
            FROM    competition
            INNER JOIN competition_category ON competition.competition_category_id = competition_category.competition_category_id
            WHERE   competition.creator_id = ?
            AND     competition.is_delete = 0
            $querystring
        ";
		$query=$this->db->query($sql, array($creator_id));
		return $query;
    }
    
    //取得單一競賽
    function getOneCompetition($competition_id){
        $sql = "select 
                    competition.*,
                    competition_category.name as category_name,
                    competition_topic.name as topic_name
                from competition 
                left join competition_category on competition.competition_category_id=competition_category.competition_category_id 
                left join competition_topic on competition.competition_topic_id=competition_topic.competition_topic_id 
                where competition.competition_id=$competition_id";
        $query=$this->db->query($sql);
        return $query;
    }

    //取得單一競賽細節
    function getOneCompetitionDetails($competition_id){
        $sql = "select * from competition_detail 
                where competition_id=$competition_id and is_delete=false and is_publish=true";
        $query=$this->db->query($sql);
        return $query;
    }

    //取得單一競賽主題
    function getOneCompetitionTopic($competition_topic_id){
        $sql = "select * from competition_topic where competition_topic_id=$competition_topic_id";
        $query = $this->db->query($sql);
        return $query;
    }

}