<?php
class Forum_assistant_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	//取得發問者以外指定討論區分類的助教、老師或總監的資料
    function getForumAssistants($user_id,$library_id){
    	$sql = "select forum_assistant.user_id,user.email,user.active_flag,user.alias,forum_assistant.support_type,library.library_name from forum_assistant
    	left join user on user.user_id=forum_assistant.user_id
    	left join library on forum_assistant.library_id=library.library_id
    	where forum_assistant.user_id!='$user_id' and forum_assistant.library_id='$library_id'";
    	$query=$this->db->query($sql);
    	return $query;
    }
    //取得使用者需支援的討論區分類編號
    function getUserSupportLibraryId($user_id){
    	$sql = "select library_id from forum_assistant
    	where user_id='$user_id'";
    	$query=$this->db->query($sql);
    	return $query;
    }
}