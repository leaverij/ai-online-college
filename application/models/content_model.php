<?php
class Content_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
	}
	
	//取讀一個任務
	function getOneContent($content_url)
	{
    	$sql="select * from chapter_content where chapter_content_url = ?";
    	$query=$this->db->query($sql,array($content_url));
		return $query;
	}

	//取得一個關卡的任務 BY 關卡ID
	function getOneChapterContentByChapterId($chapter_id){
		$sql = "select * from chapter_content where chapter_id=$chapter_id and content_open_flag='1' order by content_order asc";
		$query=$this->db->query($sql);
		return $query;
	}
	
	function getNextContent($content_url)
	{
    			$sql="	select * from chapter_content
				where content_order =(
  					select min(content_order)
  					from chapter_content
  					where content_order > (
    					select content_order
    					from chapter_content
    					where chapter_content_url = '$content_url'))
    				and chapter_id = (
      					select chapter_id
      					from chapter_content
      					where chapter_content_url = '$content_url')";
    	$query=$this->db->query($sql);
		return $query;
	}
	
	// function getChapterContentByContentUrl($content_url){
		// $query=$this->db->get_where('chapter_content',array('chapter_content_url'=>$content_url));
		// return $query;
	// }
	
	function getChapterPlaylist($chapter_url){
		$this->db->select('*')
		->from('chapter_content')
		->join('chapter','chapter_content.chapter_id=chapter.chapter_id','left')
		->where('chapter_url',$chapter_url)
		->where('content_type',0)
		->order_by('chapter_content_id');
		$query=$this->db->get();
		return $query;
	}
	//取得下一個關卡資訊
	function nextContentUrl($content_url){
		$sql="	select * from chapter_content
				where content_order =(
  					select min(content_order)
  					from chapter_content
  					where content_order > (
    					select content_order
    					from chapter_content
    					where chapter_content_url = '$content_url'))
    				and chapter_id = (
      					select chapter_id
      					from chapter_content
      					where chapter_content_url = '$content_url')";
    	$query=$this->db->query($sql);
		return $query;
	}
	//取得最後一個關卡資訊
	function lastContentUrl($chapter_content_url){
		$sql="	select *
				from chapter_content
				where chapter_id = (select chapter_id
    					from chapter_content
    					where chapter_content_url = '$chapter_content_url')
 				order by chapter_content_id desc limit 1";
    	$query=$this->db->query($sql);
		return $query;
	}
	//取得關卡下的測驗與挑戰任務數量(過關條件)
	function getChapterCompleteGoal($chapter_content_url){
		$sql="	select * from chapter_content 
				where chapter_id=(	select chapter_id 
									from chapter_content 
									where chapter_content_url='$chapter_content_url') 
				and content_type!=0";
		$query=$this->db->query($sql);
		return $query->num_rows();
	}
	//更新[一般使用者]影片、測驗、實作的作答或觀看次數
	function updateUserHits($chapter_content_url){
		$sql="	update chapter_content 
				set hits = hits + 1 
				where chapter_content_url = '$chapter_content_url'";
    	$query=$this->db->query($sql);
		return $query;
	}
	//更新[會員]影片、測驗、實作的作答或觀看次數
	function updateMemberHits($chapter_content_url){
		$sql="	update chapter_content 
				set user_hits = user_hits + 1 
				where chapter_content_url = '$chapter_content_url'";
    	$query=$this->db->query($sql);
		return $query;
	}
	
	// 更新aws_s3_url連結
	function updates3url($s3_url, $chapter_content_id){
		$sql="	update 	chapter_content 
				set 	aws_s3_url = ?
				where 	chapter_content_id = ?
				";
    	$query=$this->db->query($sql, array($s3_url, $chapter_content_id));
		return $query;
	}

	// 使用chapter_id及chapter_content_id取得Content資訊
	function getContentById($chapter_id, $chapter_content_id)
	{
		$sql="
			select 	* 
			from 	chapter_content 
			where 	chapter_id = ?
			AND 	chapter_content_id = ?
		";
		$query=$this->db->query($sql,array($chapter_id, $chapter_content_id));
		$result = $query->row_array();
		return $result;
	}

	//更新任務名稱
	function updateChapterContentName($chapter_content_id, $chapter_content_name){
		$sql = "update chapter_content set chapter_content_desc='$chapter_content_name' where chapter_content_id=$chapter_content_id";
		$query = $this->db->query($sql);
		return $query;
	}

	//刪除任務
	function removeChapterContent($chapter_content_id){
		$sql = "update chapter_content set content_open_flag='0' where chapter_content_id=$chapter_content_id";
		$query=$this->db->query($sql);
		return $query;
	}

	//取得任務內容
	function getChapterContentByChapterContentId($chapter_content_id){
		$sql = "select * from chapter_content where chapter_content_id=$chapter_content_id";
		$query = $this->db->query($sql);
		return $query;
	}
	//取得測驗卷內容
	function getQuiz($chapter_content_id){
		$sql = "select quiz_detail_desc,question_content,origin_answer from quiz_detail,question where chapter_content_id=$chapter_content_id and quiz_detail.quiz_detail_id=question.quiz_detail_id";
		$query = $this->db->query($sql);
		return $query;
	}	

	//取得目前任務總數
	function getTotalChapterContentCounts($chapter_id){
		$sql = "select count(*) as counts from chapter_content where chapter_id=$chapter_id";
		$query = $this->db->query($sql);
		return $query;
	}

	//新增一個任務
	function createNewChapterContent($data){
		$this -> db -> insert('chapter_content', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
	}

	//更新任務內容
    function updateChapterContent($chapter_content_id, $chapter_content){
		$sql = "update chapter_content set content='$chapter_content' where chapter_content_id=$chapter_content_id";
		$query = $this->db->query($sql);
		return $query;
	}

	//更新任務內容說明
    function updateChapterContentDesc($chapter_content_id, $chapter_content_desc){
		$sql = "update chapter_content set content_desc='$chapter_content_desc' where chapter_content_id=$chapter_content_id";
		$query = $this->db->query($sql);
		return $query;
	}
	//新增一個quiz_detail
	function createNewQuizDetail($data){
		$this -> db -> insert('quiz_detail', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
	}
	//新增一個question
	function createNewQuestion($data){
		$this -> db -> insert('question', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
	}
	//新增一個options
	function createNewOptions($data){
		$this -> db -> insert('options', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
	}

	//更新編譯任務內容
	function updateCompileContent($chapter_content_id, $chapter_content_desc, $content,$right_code_content,$hint){
		$sql = "update chapter_content set content_desc='$chapter_content_desc',content='$content',right_code_content='$right_code_content',hint='$hint' where chapter_content_id=$chapter_content_id";
		$query = $this->db->query($sql);
		return $query;
	}

	//更新任務順序
	function updateChapterContentOrder($chapter_content_id, $chapter_content_order){
		$sql = "update chapter_content set content_order=$chapter_content_order where chapter_content_id=$chapter_content_id";
		$query=$this->db->query($sql);
		return $query;
	}

	//更新團體實作任務
	function updateLabContentGroup($chapter_content_id, $content_group_id, $is_active){
		$sql = "
			INSERT INTO lab_content_group (chapter_content_id, content_group_id) VALUES ($chapter_content_id, $content_group_id)
			ON DUPLICATE KEY UPDATE is_active = $is_active
		";
		$query=$this->db->query($sql);
	}

	//取得任務下的團體實作群組
	function getChapterContentLabGroup($course_id, $chapter_content_id){
		$sql = "
			select 
				content_group.content_group_id,	
				content_group.name,
				lab_content_group.is_active
			from content_group
			left join lab_content_group on content_group.content_group_id=lab_content_group.content_group_id and lab_content_group.chapter_content_id=$chapter_content_id
			where content_group.course_id=$course_id and content_group.is_active=true";
		$query=$this->db->query($sql);
		return $query;
	}

}