<?php
class Forum_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	//取得討論區內的文章總數
	function getForumTotal(){
		$this->db->where('course_url', null);
		$this->db->or_where('course_url', '');
		$this->db->from('forum_topic');
		return $this->db->count_all_results();
	}
	//分頁模式下的討論區
	function getForumPagination($start,$per_page)
	{
		$sql = "select * from (select distinct topic.forum_topic,topic.user_id,topic.course_url,topic.library_id,library_name,topic.solved,count(*)-1 answers,user.alias,user.img,topic.forum_topic_id,
				CASE
				WHEN posted_time between date_sub(now(), INTERVAL 60 minute) and now() THEN concat(floor(TIMESTAMPDIFF(MINUTE,posted_time,now())), ' 分鐘前')
				WHEN posted_time between date_sub(now(), INTERVAL 24 hour)   and now() THEN concat(floor(TIMESTAMPDIFF(HOUR,posted_time,now())), ' 小時前')
				ELSE concat(floor(TIMESTAMPDIFF(DAY,posted_time,now())), ' 天前')
				END posted_time
				from user,library,forum_topic topic left join forum_content content
				on topic.forum_topic_id = content.forum_topic_id
				
				where user.user_id = topic.user_id
        		and library.library_id = topic.library_id
				group by forum_topic_id
        		order by topic.posted_time desc)temp where temp.course_url is null or temp.course_url=''
        		limit $start,$per_page";
    	$query=$this->db->query($sql);
    	return $query;
	}
	//各分類下的討論區文章總數
	function getForumDomainTotal($library_url){
		$sql = "select * from forum_topic,library
				where library.library_id = forum_topic.library_id
				and library.library_url='$library_url'
				and (forum_topic.course_url is null or forum_topic.course_url='')";
		$query=$this->db->query($sql)->num_rows();;
    	return $query;
	}
	//各課程底下的討論區文章總數
	function getForumCourseTotal($course_url){
		$sql = "select * from forum_topic,course
				where course.course_url = forum_topic.course_url
				and course.course_url='$course_url'";
		$query=$this->db->query($sql)->num_rows();;
    	return $query;
	}
	
	//各個分類下的討論區題目
	function getForumDomainPagination($library_url,$start,$per_page)
	{
		$sql = "select * from (select distinct topic.forum_topic,topic.user_id,topic.library_id,topic.course_url,library_name,topic.solved,count(*)-1 answers,user.alias,user.img,topic.forum_topic_id,
				CASE
				WHEN posted_time between date_sub(now(), INTERVAL 60 minute) and now() THEN concat(floor(TIMESTAMPDIFF(MINUTE,posted_time,now())), ' 分鐘前')
				WHEN posted_time between date_sub(now(), INTERVAL 24 hour)   and now() THEN concat(floor(TIMESTAMPDIFF(HOUR,posted_time,now())), ' 小時前')
				ELSE concat(floor(TIMESTAMPDIFF(DAY,posted_time,now())), ' 天前')
				END posted_time
				from user,library,forum_topic topic left join forum_content content
				on topic.forum_topic_id = content.forum_topic_id
				where user.user_id = topic.user_id
        		and library.library_id = topic.library_id
        		and library.library_url='$library_url'
				group by forum_topic_id
        		order by topic.posted_time desc)temp where temp.course_url is null or temp.course_url=''
        		limit $start,$per_page";
    	$query=$this->db->query($sql);
    	return $query;
	}
	//各個課程下的討論區題目
	function getForumCoursePagination($course_url,$start,$per_page)
	{
		$sql = "select distinct topic.forum_topic,topic.user_id,topic.posted_time,topic.course_url,course_name,topic.solved,count(*)-1 answers,user.alias,user.img,topic.forum_topic_id,
				CASE
				WHEN posted_time between date_sub(now(), INTERVAL 60 minute) and now() THEN concat(floor(TIMESTAMPDIFF(MINUTE,posted_time,now())), ' 分鐘前')
				WHEN posted_time between date_sub(now(), INTERVAL 24 hour)   and now() THEN concat(floor(TIMESTAMPDIFF(HOUR,posted_time,now())), ' 小時前')
				ELSE concat(floor(TIMESTAMPDIFF(DAY,posted_time,now())), ' 天前')
				END posted_time
				from user,course,forum_topic topic left join forum_content content
				on topic.forum_topic_id = content.forum_topic_id
				where user.user_id = topic.user_id
        		and course.course_url = topic.course_url
        		and course.course_url='$course_url'
				group by forum_topic_id
        		order by topic.posted_time desc
        		limit $start,$per_page";
    	$query=$this->db->query($sql);
    	return $query;
	}
	//使用coursel_url取得library_id
	function getLibraryIdbyCourseUrl($course_url){
		$sql = "select library.library_id,library.library_url from library,course where library.library_id=course.library_id and course.course_url='$course_url'";
    	$query=$this->db->query($sql);
    	return $query;
	}
	//討論區的library類別(因為原來課程頁面的library那邊太多資訊，所以重寫一個給討論區用)
	function sideBar(){
		$sql = "select * from library order by `order`";
    	$query=$this->db->query($sql);
    	return $query;
	}
	//討論區的課程類別
	function courseUrl(){
		$sql = "select course_url,course_name,course_id from course";
    	$query=$this->db->query($sql);
    	return $query;
	}
	//新增問題標題
	function insertTopic($data)
	{
		$this -> db -> insert('forum_topic', $data);
		return $this->db->insert_id();
	}
	//新增問題或是回答問題的內容
	function insertContent($data)
	{
		$this -> db -> insert('forum_content', $data);
	}
	//新增討論區回覆
	function insertComment($data)
	{
		$this -> db -> insert('forum_comment', $data);
	}
	//討論區的內容(只有發問者的內容)
	function getForumTopic($forum_topic_id)
	{
		$sql = "select library_name,user.alias,user.user_id,user.img,user.email,forum_topic.forum_topic_id,forum_topic.user_id topic_user,forum_topic.course_url,forum_topic.forum_topic,forum_topic.library_id,forum_content.forum_content,forum_content.votes,forum_content.best_answer,forum_content.forum_content_id,
				CASE
				WHEN content_posted_time between date_sub(now(), INTERVAL 60 minute) and now() THEN concat(floor(TIMESTAMPDIFF(MINUTE,content_posted_time,now())), ' 分鐘前')
				WHEN content_posted_time between date_sub(now(), INTERVAL 24 hour)   and now() THEN concat(floor(TIMESTAMPDIFF(HOUR,content_posted_time,now())), ' 小時前')
				ELSE concat(floor(TIMESTAMPDIFF(DAY,content_posted_time,now())), ' 天前')
				END content_posted_time
				from user,library,forum_topic,forum_content
				where forum_topic.forum_topic_id = forum_content.forum_topic_id
				and forum_topic.posted_time = forum_content.content_posted_time
				and user.user_id = forum_content.user_id
				and library.library_id = forum_topic.library_id
				and forum_topic.forum_topic_id = $forum_topic_id";
		$query=$this->db->query($sql);
    	return $query;
	}
	//討論區的內容(不含發問者的內容)
	function getForumContent($forum_topic_id)
	{
		$sql = "select library_name,user.alias,user.user_id,user.img,forum_topic.forum_topic_id,forum_topic.user_id topic_user,forum_topic.forum_topic,forum_topic.solved_time,forum_topic.library_id,forum_content.forum_content,forum_content.votes,forum_content.best_answer,forum_content.forum_content_id,
				CASE
				WHEN content_posted_time between date_sub(now(), INTERVAL 60 minute) and now() THEN concat(floor(TIMESTAMPDIFF(MINUTE,content_posted_time,now())), ' 分鐘前')
				WHEN content_posted_time between date_sub(now(), INTERVAL 24 hour)   and now() THEN concat(floor(TIMESTAMPDIFF(HOUR,content_posted_time,now())), ' 小時前')
				ELSE concat(floor(TIMESTAMPDIFF(DAY,content_posted_time,now())), ' 天前')
				END content_posted_time
				from user,library,forum_topic,forum_content
				where forum_topic.forum_topic_id = forum_content.forum_topic_id
				and forum_topic.posted_time < forum_content.content_posted_time
				and user.user_id = forum_content.user_id
				and library.library_id = forum_topic.library_id
				and forum_topic.forum_topic_id = $forum_topic_id
				order by forum_content.best_answer desc,forum_content.votes desc";
		$query=$this->db->query($sql);
    	return $query;
	}
	//取得討論區的回覆
	function getForumComment($forum_topic_id)
	{
		$sql = "select forum_content.forum_content_id,user.img,user.alias,forum_comment.forum_comment,forum_comment.user_id,forum_comment.forum_comment_id,
				CASE
				WHEN comment_posted_time between date_sub(now(), INTERVAL 60 minute) and now() THEN concat(floor(TIMESTAMPDIFF(MINUTE,comment_posted_time,now())), ' 分鐘前')
				WHEN comment_posted_time between date_sub(now(), INTERVAL 24 hour)   and now() THEN concat(floor(TIMESTAMPDIFF(HOUR,comment_posted_time,now())), ' 小時前')
				ELSE concat(floor(TIMESTAMPDIFF(DAY,comment_posted_time,now())), ' 天前')
				END comment_posted_time 
				from user,forum_content left join forum_comment
				on forum_content.forum_content_id = forum_comment.forum_content_id
				where forum_content.forum_topic_id = $forum_topic_id
				and user.user_id = forum_comment.user_id";
		$query=$this->db->query($sql);
    	return $query;
	}
	//修改討論區的content
	function updateContent($user_id,$forum_content_id,$data)
	{
		$this -> db -> where('user_id', $user_id);
		$this -> db -> where('forum_content_id', $forum_content_id);
		$this -> db -> update('forum_content', $data);
	}
	//修改討論區的comment
	function updateComment($user_id,$forum_comment_id,$data)
	{
		$this -> db -> where('user_id', $user_id);
		$this -> db -> where('forum_comment_id', $forum_comment_id);
		$this -> db -> update('forum_comment', $data);
	}
	//修改討論區的topic
	function updateTopic($user_id,$forum_topic_id,$data)
	{
		$this -> db -> where('user_id', $user_id);
		$this -> db -> where('forum_topic_id', $forum_topic_id);
		$this -> db -> update('forum_topic', $data);
	}
	//取得指定討論串的主題作為Notification的Title
	function getOneForumTopic($forum_topic_id){
		$sql = "select forum_topic from forum_topic
				where forum_topic_id = $forum_topic_id";
		$query=$this->db->query($sql);
		return $query->row();
	}
	//取得forum_topic_id
	function getForumTopicId($forum_content_id){
		$sql = "select * from forum_content
				where forum_content.forum_content_id = '$forum_content_id'";
		$query=$this->db->query($sql);
		return $query->row_array(); 
	}
	//修改topic的最佳解
	function updateTopicSolved($forum_topic_id,$data)
	{
		$this -> db -> where('forum_topic_id', $forum_topic_id);
		$this -> db -> update('forum_topic', $data);
	}
	//修改content的最佳解
	function updateContentBestAnswer($forum_content_id,$data)
	{
		$this -> db -> where('forum_content_id', $forum_content_id);
		$this -> db -> update('forum_content', $data);
	}
	//修改所有的bestAnswer為0
	function updateAllContentBestAnswer($forum_topic_id,$data)
	{
		$this -> db -> where('forum_topic_id', $forum_topic_id);
		$this -> db -> update('forum_content', $data);
	}
	//取得討論內容的總票數
	function getVote($forum_content_id){
		$sql = "select *,ifnull(sum(vote),0) `sum`
				from forum_vote_record
				where forum_content_id = '$forum_content_id'";
		$query=$this->db->query($sql);
		return $query->row_array();
	}
	//檢查是否投過票了
	function checkVote($user_id,$forum_content_id)
	{
		$sql = "select * from forum_vote_record
				where user_id = '$user_id'
				and forum_content_id = '$forum_content_id'";
		$query=$this->db->query($sql);
		return $query->num_rows()==0?"0":"1";	
	}
	//投票囉
	function doVote($data)
	{
		$this -> db -> insert('forum_vote_record', $data);	
	}
	//更新forum_content的vote總票數欄位
	function updateVote($forum_content_id,$data)
	{
		$this -> db -> where('forum_content_id', $forum_content_id);
		$this -> db -> update('forum_content', $data);
	}
	//取得使用者投過的票(討論區發問文章)
	function getUserTopicVote($user_id,$forum_topic_id)
	{
		$sql = "select *,forum_content.forum_content_id from forum_topic,forum_content left join forum_vote_record
				on forum_content.forum_content_id = forum_vote_record.forum_content_id and forum_vote_record.user_id = '$user_id'
				where  forum_topic.forum_topic_id = '$forum_topic_id'
				and forum_topic.forum_topic_id = forum_content.forum_topic_id
				and forum_topic.posted_time = forum_content.content_posted_time";
		$query=$this->db->query($sql);
		return $query;
	}
	//取得使用者投過的票(討論區回覆文章)
	function getUserContentVote($user_id,$forum_topic_id)
	{
		$sql = "select *,forum_content.forum_content_id from forum_topic,forum_content left join forum_vote_record
				on forum_content.forum_content_id = forum_vote_record.forum_content_id and forum_vote_record.user_id = '$user_id'
				where  forum_topic.forum_topic_id = '$forum_topic_id'
				and forum_topic.forum_topic_id = forum_content.forum_topic_id
				and forum_topic.posted_time < forum_content.content_posted_time";
		$query=$this->db->query($sql);
		return $query;
	}
	//新增閱讀討論區文章歷史紀錄
    function insertTopicRead($data)
    {
        $this -> db -> insert('forum_topic_read', $data);
    }
	
}