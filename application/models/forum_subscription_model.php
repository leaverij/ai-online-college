<?php
class Forum_subscription_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	//列出用戶有訂閱(訂閱狀態0或1),發送Notification用的討論區主題
	function getUserForumSubscription($email){
		$sql = "select forum_subscription.*,forum_topic.forum_topic_id,forum_topic,posted_time,solved,user.img,user.alias from forum_subscription
				left join forum_topic on forum_subscription.forum_topic_id=forum_topic.forum_topic_id
				left join user on user.user_id=forum_subscription.user_id
				where user.email ='$email' order by subscription_time";
    	$query=$this->db->query($sql);
		return $query;
	}
	//列出用戶訂閱狀態為1,要發送Email的討論區主題
	function getUserForumEmailSubscription($email){
		$sql = "select forum_subscription.*,forum_topic.forum_topic_id,forum_topic,posted_time,solved,user.img,user.alias from forum_subscription
				left join forum_topic on forum_subscription.forum_topic_id=forum_topic.forum_topic_id
				left join user on user.user_id=forum_subscription.user_id
				where user.email ='$email'and forum_subscription.status=1 order by subscription_time";
    	$query=$this->db->query($sql);
		return $query;
	}
	//分頁模式下的主題訂閱列表
	function getSubscriptionPagination($start,$per_page,$user_id)
	{
		$sql = "select distinct topic.forum_topic,topic.user_id,topic.posted_time,topic.library_id,library_name,count(*)-1 answers,user.alias,user.img,topic.forum_topic_id,
				CASE
				WHEN posted_time between date_sub(now(), INTERVAL 60 minute) and now() THEN concat(floor(TIMESTAMPDIFF(MINUTE,posted_time,now())), ' 分鐘前')
				WHEN posted_time between date_sub(now(), INTERVAL 24 hour)   and now() THEN concat(floor(TIMESTAMPDIFF(HOUR,posted_time,now())), ' 小時前')
				ELSE concat(floor(TIMESTAMPDIFF(DAY,posted_time,now())), ' 天前')
				END posted_time
				from user,library,forum_topic topic left join forum_content content
				on topic.forum_topic_id = content.forum_topic_id
				where user.user_id = topic.user_id
        		and library.library_id = topic.library_id
        		and topic.forum_topic_id in(select forum_topic_id from forum_subscription where user_id = '$user_id' and status=1)
				group by forum_topic_id
        		order by topic.posted_time desc
				limit $start,$per_page";
		$query=$this->db->query($sql);
		return $query;
	}
	//列出有訂閱指定討論區主題的用戶
	function getForumTopicSubscriber($forum_topic_id){
		$sql = "select forum_subscription.*,user.email,user.alias,user.active_flag from forum_subscription
		left join user on forum_subscription.user_id=user.user_id
		where forum_subscription.forum_topic_id ='$forum_topic_id' 
		order by subscription_time";
		$query=$this->db->query($sql);
		return $query;
	}
	//列出有訂閱指定討論區主題的用戶
	function getForumTopicEmailSubscriber($forum_topic_id){
		$sql = "select forum_subscription.*,user.email,user.alias,user.active_flag from forum_subscription
		left join user on forum_subscription.user_id=user.user_id
		where forum_subscription.forum_topic_id ='$forum_topic_id' and forum_subscription.status=1 
		order by subscription_time";
		$query=$this->db->query($sql);
		return $query;
	}
	//檢查用戶是否已訂閱指定的討論區主題
	function isTopicSubscribed($user_id,$forum_topic_id){
		$sql = "select * from forum_subscription where user_id ='$user_id' and forum_topic_id='$forum_topic_id'";
		$query=$this->db->query($sql);
		return $query->num_rows();
	}
	//檢查用戶指定的討論區主題是否寄發Email
	function isTopicSubscribedEmail($user_id,$forum_topic_id){
		$sql = "select * from forum_subscription where user_id ='$user_id' and forum_topic_id='$forum_topic_id' and status=1";
		$query=$this->db->query($sql);
		return $query->num_rows();
	}
	//新增討論區主題訂閱
	function addForumSubscription($forumSubscription){
// 		$sql="insert into forum_subscription (user_id,forum_topic_id,subscription_time,status) values (?,?,?,?)";
// 		$query=$this->db->query($sql,$forumSubscription);
		$query=$this->db->insert('forum_subscription',$forumSubscription);
		return $this->db->insert_id();
	}
	function updateForumSubscription($forumSubscription,$where){
		$query=$this->db->update('forum_subscription', $forumSubscription, $where);
		return $this->db->affected_rows();
	}
}