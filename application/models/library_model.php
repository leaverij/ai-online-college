<?php
class Library_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	//列出All Library相關欄位
    function getAll()
	{
    	$sql="	select *,sum(course.course_type='1')project,sum(course.course_type='2') deep_dives ,count(course.course_type)total
				from library left JOIN (select * from course where course.status=1) course
				on library.library_id = course.library_id and course.sync_course = '0'
				where library.status = '1'
				group by library.library_id
				order by library.order";
    	$query=$this->db->query($sql);
		return $query;
	}
	//列出單一課程分類下的課程
	function getOneLibrary($library_url,$type)
	{
    	$sql="	select *,course.course_id course_id,count(chapter.course_id) total
				from library left join course
				on library.library_id = course.library_id and course.course_type='2' and course.sync_course = '0' and course.status=1
        		left join chapter
        		on course.course_id = chapter.course_id
        		where library.library_url = '$library_url'
				group by chapter.course_id,course.course_id
				order by course.create_time desc";
    	$query=$this->db->query($sql);
		return $query;
	}
	//列出特定試卷編號的題目 ajax(單選)
	function getQuestion($chapter_content_url,$question_id)
	{
		$sql="	select *,question.question_id question_id
				from chapter
       		 	left join chapter_content
        		on chapter.chapter_id = chapter_content.chapter_id
				left join (quiz_detail,question)
				on chapter_content.chapter_content_id = quiz_detail.chapter_content_id
				left join options
				on question.question_id = options.question_id
				where chapter_content.chapter_content_url = '$chapter_content_url'
				and question.question_id = '$question_id'
				and quiz_detail.quiz_detail_id = question.quiz_detail_id
				order by rand()";
		$query=$this->db->query($sql);
		return $query;
	}
	//列出特定試卷編號的題目 ajax(填充用)
	function getFillInBlankQuestion($chapter_content_url,$question_id)
	{
		$sql="	select *
				from chapter_content
				left join (quiz_detail,question)
				on chapter_content.chapter_content_id = quiz_detail.chapter_content_id
				where chapter_content.chapter_content_url = '$chapter_content_url'
				and question.question_id = '$question_id'
				and quiz_detail.quiz_detail_id = question.quiz_detail_id";
		$query=$this->db->query($sql);
		return $query;
	}
	
	//列出試卷題號
	function getQuizNo($chapter_content_url,$quantity)
	{
		$sql="	select question.question_id
				from chapter_content
				left join (quiz_detail,question)
				on chapter_content.chapter_content_id = quiz_detail.chapter_content_id
				where chapter_content.chapter_content_url = '$chapter_content_url'
				and quiz_detail.quiz_detail_id = question.quiz_detail_id
				order by rand() limit $quantity";
		$query=$this->db->query($sql);
		return $query;
	}
	//列出試卷題號
	function getQuizCodeNo($chapter_content_url,$quantity)
	{
		$sql="	select question.question_id
				from chapter_content
				left join (quiz_detail,question)
				on chapter_content.chapter_content_id = quiz_detail.chapter_content_id
				where chapter_content.chapter_content_url = '$chapter_content_url'
				and quiz_detail.quiz_detail_id = question.quiz_detail_id
				order by question.question_id asc limit $quantity";
		$query=$this->db->query($sql);
		return $query;
	}
	
	//check answer
	function checkAnswer($question_id,$answer)
	{
		$sql="select * from question where question_id='$question_id' and correct_answer='$answer'";
    	$query=$this->db->query($sql);
    	return $query->num_rows()==0?"false":"true";	
	}
	
	//列出quiz_detail 考卷的題數
	function getQuantity($chapter_content_url)
	{
		$sql="	select quantity,chapter_content.content_type
				from chapter_content
				left join quiz_detail
				on chapter_content.chapter_content_id = quiz_detail.chapter_content_id
				where chapter_content.chapter_content_url = '$chapter_content_url'";
		$query=$this->db->query($sql);
		return $query;
	}
	//搜尋一般課程
	function searchCourse($search_string)
	{
		$sql="	select *,count(chapter.course_id) total
				from library,course left join chapter
				on course.course_id = chapter.course_id
				where library.library_id = course.library_id
				and course.course_type='2'
				and course.sync_course = '0'
				and (course.course_name like '%$search_string%'or course.course_desc like '%$search_string%')
				group by chapter.course_id,course.course_id
				order by library.library_id;";
		$query=$this->db->query($sql);
		return $query;
	}
	//搜尋依職務分類的學程
	function searchJobsCategory($search_string)
	{
		$sql = "select *,count(tracks_course.tracks_id) total
				from jobs_category,jobs_category_tracks,tracks
				left join (tracks_course,course)
				on tracks.tracks_id=tracks_course.tracks_id and tracks_course.course_id = course.course_id 
				where jobs_category.jobs_category_id =jobs_category_tracks.jobs_category_id
				and jobs_category_tracks.tracks_id = tracks.tracks_id
        		and (tracks.tracks_name like '%$search_string%' or tracks.tracks_desc like '%$search_string%')
				group by tracks.tracks_id";
		$query=$this->db->query($sql);
		return $query;
	}
	//以下為依職務分類的函數
	//取得所有有效的職務類別
	function getAllJobsCategory(){
		$sql="	select * from jobs_category
				where status = '1'
				order by jobs_category_order"; 
		$query=$this->db->query($sql);
		return $query;
	}
	//檢查是否有$jobsCategory這個分類
	function checkJobsCategory($jobs_category_url){
		$sql = "select * from jobs_category where jobs_category_url='$jobs_category_url' and status='1'";
		$query=$this->db->query($sql);
    	return $query->row();	
	}
	//取得單一職務分類底下的學程資訊(未登入)
	function getTracksByCategory($jobs_category_url)
	{
		$sql = "select *,count(tracks_course.tracks_id) total
				from jobs_category,jobs_category_tracks,tracks
				left join (tracks_course,course)
				on tracks.tracks_id=tracks_course.tracks_id and tracks_course.course_id = course.course_id
				where jobs_category.jobs_category_id =jobs_category_tracks.jobs_category_id
				and jobs_category_tracks.tracks_id = tracks.tracks_id
				and jobs_category.jobs_category_url='$jobs_category_url'
				group by tracks.tracks_id";
    	$query=$this->db->query($sql);
    	return $query;
	}
	//登入時，職務分類下各學程的學習狀況
	function getStatusByUser($jobs_category_url,$email){
		$sql = "select *,count(tracks.tracks_id) total,count(user_course.status) passAmount from jobs_category,jobs_category_tracks,tracks,tracks_course,course left join (user_course,`user`)
				on course.course_id = user_course.course_id and user_course.user_id = user.user_id and user_course.status=1 and user.email='$email'
				where jobs_category.jobs_category_id =jobs_category_tracks.jobs_category_id
				and jobs_category_tracks.tracks_id = tracks.tracks_id
				and jobs_category.jobs_category_url='$jobs_category_url'
				and tracks.tracks_id = tracks_course.tracks_id
				and tracks_course.course_id = course.course_id
				group by tracks.tracks_id";
    	$query=$this->db->query($sql);
    	return $query;
	}
	//搜尋用的，登入時，職務分類下的學習狀況
	function getPassAmountBySearchTracks($email){
		$sql = "select *,count(tracks.tracks_id) total,count(user_course.status) passAmount from jobs_category,jobs_category_tracks,tracks,tracks_course,course left join (user_course,`user`)
				on course.course_id = user_course.course_id and user_course.user_id = user.user_id and user_course.status=1 and user.email='$email'
				where jobs_category.jobs_category_id =jobs_category_tracks.jobs_category_id
				and jobs_category_tracks.tracks_id = tracks.tracks_id
				and tracks.tracks_id = tracks_course.tracks_id
				and tracks_course.course_id = course.course_id
				group by tracks.tracks_id";
    	$query=$this->db->query($sql);
    	return $query;
	}
	//搜尋用的，登入時，課程分類下的學習狀況
	function getPassAmountBySearchCourse($email){
		$sql = "select *,count(user_course.course_id)passAmount
				from user_course,user_chapter,user
				where user_course.user_course_id = user_chapter.user_course_id
				and user_chapter.status = '1'
				and user_course.user_id = user.user_id
				and user.email='$email'
				group by user_course.course_id";
    	$query=$this->db->query($sql);
    	return $query;
	}
	
	//取得課程分類中的小分類 ex ios android
	function getCourseCategory($library_url){
		$sql = "select course_category.*
				from library,course,course_category
				where course.library_id = library.library_id
				and library.library_url='$library_url'
				and course.sync_course='0'
				and course.course_type='2'
				and course_category.course_category_id = course.course_category_id
				group by course_category.course_category_name
				order by course_category.course_category_id";
		$query=$this->db->query($sql);
    	return $query;
	}
	
	//取得課程分類下優惠價格等資訊
	function getPreferentialPrice($library_url){
		$user_id = $this->session->userdata('user_id');
		$sql = "select distinct *,order_detail.*,course.course_id course_id,order_detail.live_flag
				from library,course 
				left join order_detail
					on course.course_id = order_detail.course_id and order_detail.live_flag='1' and order_detail.user_id='$user_id'
				left join preferential
					on course.course_id = preferential.course_id and current_date() between start_time and end_time
				where library.library_id = course.library_id
				and course.course_type='2'
				and course.sync_course = '0'
				and library.library_url = '$library_url'
				group by course.course_id";
		$query=$this->db->query($sql);
    	return $query;
	}
	
	//取得單一課程下的價格資訊
	function getEachPreferentialPrice($course_url){
		$user_id = $this->session->userdata('user_id');
		$sql = "select distinct *,order_detail.*,course.course_id course_id,order_detail.live_flag
				from library,course
				left join order_detail
					on course.course_id = order_detail.course_id and order_detail.live_flag='1' and order_detail.user_id='$user_id'
				left join preferential
					on course.course_id = preferential.course_id and current_date() between start_time and end_time
				left join course_category
          			on course_category.course_category_id = course.course_category_id
				where library.library_id = course.library_id
				and course.course_type='2'
				and course.sync_course = '0'
				and course.course_url = '$course_url'
				group by course.course_id";
		$query=$this->db->query($sql);
    	return $query;
	}
	//取得關卡頁面，使用者是否購買課程的資訊
	function getCourseOrderDetail($course_url){
		$user_id = $this->session->userdata('user_id');
		$sql = "select * from course,order_detail,user
				where course.course_id = order_detail.course_id 
				and order_detail.user_id = user.user_id 
				and order_detail.live_flag = '1' 
				and course.course_url = '$course_url' 
				and user.user_id = '$user_id'";
		$query=$this->db->query($sql);
    	return $query;
	}
	//取得使用者的學習進度狀況(給繼續學習按鈕用的)
	function getUserLearningStatus($course_url){
		$user_id = $this->session->userdata('user_id');
		$sql = "select *,ifnull(uc.uc_status,'0') pass from library,course,chapter
				left join (	select user_chapter.status uc_status,user_chapter.chapter_id
          					from user_course,user_chapter
          					where user_course.user_course_id = user_chapter.user_course_id
          					and user_course.user_id = '$user_id') uc 
          		on chapter.chapter_id = uc.chapter_id
				where course_url = '$course_url'
				and course.course_id = chapter.course_id
				and library.library_id = course.library_id";
		$query=$this->db->query($sql);
    	return $query;
	}
	//取得課程價錢(js連到這邊取值)
	function getCoursePrice($course_id){
		$sql = "select ifnull(preferential.preferential_price,course.course_price) price 
				from course left join preferential 
				on course.course_id = preferential.course_id and current_date() between start_time and end_time
				where course.course_id ='$course_id'";
		$query=$this->db->query($sql);
    	return $query;
	}

	//取得所有體驗單元
	function getLabChapterContent(){
		$sql = "select * from chapter_content where content_type=4 and content_open_flag=1 and is_gym=true order by chapter_content_desc asc";
		$query=$this->db->query($sql);
		return $query;
	}

	//取得單一體驗單元
	function getSingleChapterContent($chapter_content_id){
		$sql = "select * from chapter_content where chapter_content_id='$chapter_content_id'";
		$query=$this->db->query($sql);
    	return $query;
	}

	//取得所有library
	function getAllLibrary(){
		$sql = "select * from library order by create_time desc";
		$query=$this->db->query($sql);
    	return $query;
	}

	//取得所有course category
	function getAllCourseCategory(){
		$sql = "select * from course_category";
		$query=$this->db->query($sql);
    	return $query;
	}

	// 取得課程所有學生清單
	function getAllCourseStudent($course_id){
		$sql = "
		SELECT 	
			course.course_id,
			course.course_name,
			user.name,
			user.email,
			course_order.order_time,
			user.last_login,
			course_order.user_id,
			user_course.status
		FROM 	course_order
		INNER JOIN course ON course.course_id = course_order.course_id
		INNER JOIN user on course_order.user_id = user.user_id
		LEFT JOIN user_course on user_course.user_id=course_order.user_id and user_course.course_id=?
		WHERE 	course_order.course_id = ?
		order by course_order.order_time asc
		";
		$query=$this->db->query($sql, array($course_id, $course_id));
    	return $query;
	}


}