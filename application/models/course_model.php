<?php
class Course_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	//列出單一課程下的關卡(也就是chapter)
	function getOneChapter($course_url)
	{
    	$sql="	select * from course_category,course left join chapter
				on course.course_id = chapter.course_id
				where course.course_url = '$course_url'
       	 		and course.course_category_id = course_category.course_category_id order by chapter.chapter_order asc";
    	$query=$this->db->query($sql);
		return $query;
	}
	
	function getPartialCourse()
	{
		$sql = "select * from course limit 0,6";
		$query = $this->db->query($sql);
		return $query;
		
	}
	
	// function getCourseByCourseUrl($course_url){
		// $query=$this->db->get_where('course',array('course_url'=>$course_url));
		// return $query;
	// }
	
	//提供content_url參數 可以得到course以及chapter的id 和url
	function getCourseChapterContentId($chapter_content_url)
	{
    	$sql="	select course.course_id,course.course_url,chapter.chapter_id,chapter.chapter_url,chapter_content.chapter_content_id,chapter_content.chapter_content_url,chapter_content.hits
				from chapter_content,chapter,course
				where course.course_id = chapter.course_id
				and chapter_content.chapter_id=chapter.chapter_id
				and chapter_content.chapter_content_url='$chapter_content_url'";
    	$query=$this->db->query($sql);
		return $query;
	}
	
	//紀錄使用者搜尋的字串
	function insertSearchRecord($data)
	{
		$this -> db -> insert('search_record', $data);
	}
	//取得觀看次數
	function getHits()
	{
		$sql ="	select *,sum(hits) hits
				from chapter_content,chapter
				where chapter.chapter_id = chapter_content.chapter_id
				group by chapter_content.chapter_id";
		$query=$this->db->query($sql);
		return $query;
	}

	//建立新課程
	function createCourse($data){
		$this -> db -> insert('course', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
	}

	//更新課程基本資訊
	function updateCourseInfo($course_id,$teacher_id,$course_name,$course_library,$course_category,$status){
		$sql="update course set course_name='$course_name', library_id=$course_library, course_category_id=$course_category,status='$status' where course_id=$course_id and teacher_id=$teacher_id ";
    	$this->db->query($sql);
	}

	//教師取得所有課程
	function getTeachedCourses($teacher_id){
		$sql = "select * from course where teacher_id=$teacher_id order by create_time desc";
		$query=$this->db->query($sql);
		return $query;
	}
	
	//教師取得單一課程
	function getTeachedCourse($teacher_id, $course_id){
		$sql = "select * from course where teacher_id=$teacher_id and course_id=$course_id";
		$query=$this->db->query($sql);
		return $query;
	}

	//取得單一課程
	function getOneCourse($course_url){
		$sql = "select * from course where course_url='$course_url'";
		$query=$this->db->query($sql);
		return $query;
	}

	//取得單一課程 By 課程ID
	function getOneCourseByCourseId($course_id){
		$sql = "select * from course 
				left join teacher on course.teacher_id=teacher.teacher_id
				where course.course_id=$course_id";
		$query=$this->db->query($sql);
		return $query;
	}

	//更新課程價格
	function updateCoursePrice($course_id, $course_price){
		$sql="update course set course_price=$course_price where course_id=$course_id";
    	$this->db->query($sql);
	}

	//取得課程公告BY教師ID
	function getCourseAnnouncementsByTeacherId($course_id, $teacher_id){
		$sql = "select * from course_announcement where course_id=$course_id and teacher_id=$teacher_id and is_delete='0'";
		$query=$this->db->query($sql);
		return $query;
	}

	//取得課程公告
	function getCourseAnnouncements($course_id){
		$sql = "select * from course_announcement left join teacher on course_announcement.teacher_id=teacher.teacher_id where course_announcement.course_id=$course_id and course_announcement.is_delete='0' order by course_announcement.announce_date desc";
		$query=$this->db->query($sql);
		return $query;
	}

	//建立新課程公告
	function createCourseAnnouncement($data){
		$this -> db -> insert('course_announcement', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
	}

	//更新課程公告
	function updateCourseAnnouncement($announcement_id, $title, $content){
		$sql = "update course_announcement set title='$title',content='$content' where id=$announcement_id";
		$this->db->query($sql);
	}

	//取得單一課程公告
	function getSingleCourseAnnouncement($announcement_id){
		$sql = "select * from course_announcement where id=$announcement_id";
		$query=$this->db->query($sql);
		return $query;
	}

	//移除課程公告
	function removeAnnouncement($announcement_id){
		$sql = "update course_announcement set is_delete='1' where id=$announcement_id";
		$this->db->query($sql);
	}

	//取得直播教學列表ByTeacherId
	function getCourseMeetingsByTeacherId($course_id, $teacher_id){
		$sql = "select * from meeting where course_id=$course_id and teacher_id=$teacher_id and is_delete='0' order by create_date asc";
		$query=$this->db->query($sql);
		return $query;
	}

	//取得直播教學列表
	function getCourseMeetings($course_id){
		$sql = "select * from meeting where course_id=$course_id and is_delete='0' order by create_date asc";
		$query=$this->db->query($sql);
		return $query;
	}

	//建立直播教學
	function createCourseMeeting($data){
		$this -> db -> insert('meeting', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
	}

	//更新直播教學
	function updateCourseMeeting($course_id, $teacher_id, $topic, $description, $date, $meeting_id){
		$sql = "update meeting set topic='$topic',description='$description',start_time='$date' where course_id=$course_id and teacher_id=$teacher_id and id=$meeting_id";
		$this->db->query($sql);
	}

	//取得單一直播資訊
	function getSingleCourseMeeting($meeting_id){
		$sql = "select * from meeting where id=$meeting_id";
		$query=$this->db->query($sql);
		return $query;
	}

	//移除課程直播
	function removeMeeting($meeting_id){
		$sql = "update meeting set is_delete='1' where id=$meeting_id";
		$this->db->query($sql);
	}

	//學生取得我的課程
	function getLearnedCourses($user_id){
		$return = array();
		$sql = "select 
					course.course_id,
					course.course_name,
					course.course_url,
					course.course_pic,
					teacher.name as teacher_name,
					domain_library.domain_id,
					course_order.order_time,
					course_order.price,
					course_order.paid
				from course 
				left join course_order on course.course_id=course_order.course_id
				left join teacher on course.teacher_id=teacher.teacher_id
				left join domain_library on course.library_id=domain_library.library_id
				where course_order.user_id=$user_id
				order by course_order.order_time asc";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		foreach($result as $index=>$course){
			$return[$index]['course_id'] = $course['course_id'];
			$return[$index]['course_name'] = $course['course_name'];
			$return[$index]['course_pic'] = $course['course_pic'];
			$return[$index]['course_url'] = $course['course_url'];
			$return[$index]['teacher_name'] = $course['teacher_name'];
			$return[$index]['domain_id'] = $course['domain_id'];
			$return[$index]['order_time'] = $course['order_time'];
			$return[$index]['price'] = $course['price'];
			$return[$index]['paid'] = $course['paid'];
			$domain_sql = "select domain_url from domain where domain_id=?";
			$domain_query = $this->db->query($domain_sql, array($course['domain_id']))->row_array();
			$return[$index]['domain_url'] = 'ai';//$domain_query['domain_url'];
		}
		return $return;
	}

	//Zoom回傳更新直播
	function updateCourseMeetingZoom(
		$meeting_uuid,
		$meeting_room_id,
		$start_url,
		$join_url,
		$meeting_id
	){
		$sql = "update meeting set uuid='$meeting_uuid',meeting_id='$meeting_room_id',start_url='$start_url',join_url='$join_url'
				where id=$meeting_id";
		$this->db->query($sql);
	}

	function getStudentByCourseid($id){
		$sql="
			SELECT  user.user_id as user_id, user.name as user_name
			FROM 	course_order
			INNER JOIN user ON user.user_id = course_order.user_id
			WHERE 	course_id = ?
			ORDER BY order_time ASC
		";
    	$query=$this->db->query($sql,array($id));
		return $query;
	}
	//取得課程網址（用作課程圖片檔名）
	function getCourseUrl($course_id){
		$sql = "select * from course where course_id='$course_id'";
		$query=$this->db->query($sql);
		return $query;
	}
	function updateCoursePic($course_id,$data){
		$this -> db -> where('course_id', $course_id);
		$this -> db -> update('course', $data);
	}

	//建立課程學生分組
	function createNewCourseGroup($data){
		$this -> db -> insert('content_group', $data);
		$insert_id = $this->db->insert_id();
   		return  $insert_id;
	}

	//取得課程學生分組
	function getCourseStudentGroups($course_id, $teacher_id){
		$return = array();
		$sql = "select * from content_group where course_id=$course_id and teacher_id=$teacher_id and is_active=true order by create_time desc";
		$query=$this->db->query($sql);
		$result = $query->result_array();
		foreach($result as $index=>$group){
			$return[$index]['content_group_id'] = $group['content_group_id'];
			$return[$index]['name'] = $group['name'];
			$return[$index]['create_time'] = $group['create_time'];
			$return[$index]['is_valid'] = $group['is_active'];
			$return[$index]['teacher_id'] = $group['teacher_id'];
			$return[$index]['course_id'] = $group['course_id'];
			$student_count_sql = "select count(*) as counts from group_member where content_group_id=?";
			$student_count_query = $this->db->query($student_count_sql, array($group['content_group_id']))->row();
			$return[$index]['counts'] = $student_count_query->counts;
		}
		return $return;
	}

	//取得課程單一學生分組
	function getSingleCourseStudentGroups($course_id, $teacher_id, $content_group_id){
		$sql = "select * from content_group where course_id=$course_id and teacher_id=$teacher_id and content_group_id=$content_group_id";
		$query=$this->db->query($sql);
		return $query;
	}

	//將學生加入課程群組
	function addCourseGroupStudent($data){
		$this -> db -> insert('group_member', $data);
	}

	//將學生移除課程群組
	function removeCourseGroupStudent($user_id, $content_group_id){
		$sql = "delete from group_member where user_id=$user_id and content_group_id=$content_group_id";
		$this->db->query($sql);
	}

	//取得學生分組狀態
	function getSingleCourseStudentGroupStatus($content_group_id, $course_id){
		$sql = "
			SELECT 
				user.user_id,
				user.name,
				user.email,
				course_order.order_time,
				user.last_login,
				group_member.member_id
			FROM course_order
			INNER JOIN user ON course_order.user_id=user.user_id
			LEFT JOIN group_member on course_order.user_id=group_member.user_id and group_member.content_group_id=?
			WHERE course_order.course_id=?
			ORDER BY course_order.order_time asc
		";
		$query=$this->db->query($sql, array($content_group_id, $course_id));
		return $query;
	}

	//取得課程分組學生人數
	function getCourseGroupStudentCounts($content_group_id){
		$sql = "select count(*) as counts from group_member where content_group_id=$content_group_id";
		$query=$this->db->query($sql);
        return $query;
	}

	//教師刪除課程分組
	function removeCourseGroup($content_group_id, $teacher_id, $course_id){
		$sql = "update content_group set is_active=false where course_id=$course_id and teacher_id=$teacher_id and content_group_id=$content_group_id";
		$this->db->query($sql);
	}

}