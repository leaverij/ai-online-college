<?php
class User_content_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
	/*	$user_chapter_content	=array(	'user_chapter_id'	=>$user_chapter_id,
	 *									'chapter_content_id'=>$content_id);
	 */						
	function addUserContent($user_content){
		$sql="insert into user_chapter_content (user_chapter_id,chapter_content_id,accomplish_time,status) values (?,?,?,?)";
		$user_content['accomplish_time']=$this->theme_model->nowTime();
		$user_content['status']=1;
		$query=$this->db->query($sql, $user_content);
		return $this->db->affected_rows();
	}
	
	/*	$option=array(	'user.email'			=>$email,
	 *					'course.course_url'		=>$course_url,
	 *					'chapter.chapter_url'	=>$chapter_url,
	 *					'user_chapter_content.status'	=>'1');			//0:Not Finished 1:Finished
	 */
	function getUserContent($option){
		$this->db->select('chapter_content.chapter_content_id,chapter_content.chapter_content_desc,chapter_content.content_type,user_chapter_content.status')
		->from('user_chapter_content')
		->join('user_chapter','user_chapter_content.user_chapter_id=user_chapter.user_chapter_id','left')
		->join('user_course','user_chapter.user_course_id=user_course.user_course_id','left')
		->join('user','user_course.user_id=user.user_id','left')
		->join('course','user_course.course_id=course.course_id','left')
		->join('chapter','user_chapter.chapter_id=chapter.chapter_id','left')
		->join('chapter_content','user_chapter_content.chapter_content_id=chapter_content.chapter_content_id','left')
		->where($option);
		$this->db->distinct();
		$query=$this->db->get();
		return $query;
	}
	
	//取得使用者目前關卡中，完成的測驗或挑戰的任務數量(不重複)
	function getUserContentProgress($email,$chapter_content_url){
		$sql="	select *
				from user_chapter_content
				where chapter_content_id in (	select chapter_content_id 
												from chapter_content 
												where content_type!=0)
				and user_chapter_id=(	select user_chapter_id 
										from user_chapter 
										where user_course_id = (	select user_course_id 
																	from user_course 
																	where user_id =(	select user_id 
																						from user 
																						where email='$email') 
																	and course_id =(	select course_id 
																						from chapter 
																						where chapter_id =(	select chapter_id 
																											from chapter_content 
																											where chapter_content_url='$chapter_content_url'))) 
										and chapter_id=(	select chapter_id 
															from chapter_content 
															where chapter_content_url = '$chapter_content_url')) 
				and status=1
				group by chapter_content_id;";
		$query=$this->db->query($sql);
		return $query->num_rows();
	}
	
	function getUserActivity($email){
		$sql="	select 	user_chapter.user_chapter_id,user.email,user.alias,user.img,
						library.library_url,library.library_name,
						course.course_url,
						chapter.chapter_url,chapter.chapter_pic,chapter.chapter_name,
						chapter_content_url,chapter_content.chapter_content_desc,chapter_content.content_type,
						user_chapter_content.accomplish_time,chapter_content.content_order,chapter_content.duration
				from user_chapter_content
				left join user_chapter on user_chapter_content.user_chapter_id=user_chapter.user_chapter_id
				left join user_course on user_chapter.user_course_id=user_course.user_course_id
				left join user on user_course.user_id=user.user_id
				left join chapter_content on user_chapter_content.chapter_content_id=chapter_content.chapter_content_id
				left join chapter on chapter.chapter_id=chapter_content.chapter_id
				left join course on chapter.course_id=course.course_id
				left join library on course.library_id=library.library_id
				where email='$email' and
				chapter_content.award_video='0'
				order by accomplish_time desc limit 8";
		$query=$this->db->query($sql);
		return $query;
	}
}