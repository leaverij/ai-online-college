<?php
class Points_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	//新增積分
	function insertPoint($user_id,$points_source,$course_id,$chapter_id,$content_id,$forum_content_id)
	{
		//points_source來源決定分數多寡
		//completed_video、completed_quiz、completed_chapter、completed_course、completed_track、forum_best_answer   
		//取得積分 請include   $this->load->model('points_model'); $this->points_model->insertPoint($user_id,'forum',$forum_topic_id);
		//暫時不加//看影片              		completed_video
		//做測驗               		completed_quiz
		//完成一個關卡(badge)  completed_chapter
		//完成一個課程         		completed_course
		//完成一個學程地圖     	completed_track
		//問題為最佳解			forum_best_answer
		//暫時不加//發問一個問題         		forum_question
		//暫時不加//回應一個問題         		forum_response
		switch($points_source){
			//暫時未做//case "completed_video": 	$points='1';	break;
			case "completed_quiz": 		$points='6';	break;
			case "completed_code": 		$points='10';	break;
			case "completed_chapter":   $points='30';	break;
			case "completed_course": 	$points='60';	break;
			//暫時未做//case "completed_track": 	$points='120';	break;
			case "forum_best_answer": 	$points='12';	break;
			default:
       			echo "積分來源有誤";
				redirect('library');die();
		}
		$data = array(	'user_id'  =>$user_id,         
						'points_source'  =>$points_source,
						'course_id'  =>$course_id,
						'chapter_id' =>$chapter_id,
						'content_id' =>$content_id,
						'forum_content_id' =>$forum_content_id,
						'points'  =>$points,          
						'points_get_time'  =>$this->theme_model->nowTime());
		$this -> db -> insert('points', $data);
	}
	//使用chapter_content_url取得chapter_id以及course_id
	function getCourseChapterID($chapter_contnt_url){
		$sql = "select course.course_id,chapter.chapter_id,chapter_content.chapter_content_id from course,chapter,chapter_content
				where course.course_id = chapter.course_id and chapter.chapter_id = chapter_content.chapter_id
				and chapter_content.chapter_content_url='$chapter_contnt_url'";
		$query=$this->db->query($sql);
    	return $query->row();	
	}
	//取得領域內的積分(不含討論區塊內的積分) 這邊以領域為主要分類
	function getPoints($user_id){
		$sql = "select library.library_color,library.library_id,library_name,library_url,ifnull(sum(po3.sum),0) sum
				from library left join (course,(select po2.user_id,po2.course_id ,sum(po2.points)sum  
												from (select * from (select * 
																	from points order by points_get_time desc)po
												where user_id='$user_id' and course_id != '0'
												group by content_id ,chapter_id,course_id,user_id  
												order by points_id asc)po2
				group by po2.course_id)po3)
				on (library.library_id = course.library_id and course.course_id = po3.course_id)
				where library.status='1'
				group by library_id;";
		$query=$this->db->query($sql);
    	return $query;
	}
	function getTotalPoints($user_id){
		$sql = "select po2.user_id,sum(po2.points)total from (select * from (select * from points order by points_get_time desc)po
				where user_id='$user_id'
				group by content_id ,chapter_id,course_id,user_id,forum_content_id  order by points_id asc)po2";
		$query=$this->db->query($sql);
    	return $query->row();
	}
	//檢查使用者是否已經取得積分(forum_content_id)
	function checkForumPoint($user_id,$forum_content_id){
    	$sql="select * from points where user_id = '$user_id' and forum_content_id = '$forum_content_id'";
    	$query=$this->db->query($sql);
    	return $query->num_rows()==0?"0":"1";
	}
	///
	// by JoeyC
	///
function getCSAnalysis($user_id){
        $sql = "select course_id, chapter_id, AVG(points) as avg from (select * from (select * from points order by points_get_time desc)po 
                where user_id='$user_id' and course_id != '0')po2 where chapter_id!='0' group by 
                course_id, chapter_id, user_id order by course_id";
        $query=$this->db->query($sql);
        return $query;
    }
    function getCourseScores($user_id){
        $sql = "select library.library_id, course.course_id, po3.avg from library left join
                (course, (select course_id, chapter_id, AVG(points) as avg from 
                (select * from (select * from points order by points_get_time desc)po where user_id='$user_id' and course_id != '0')po2 where chapter_id!='0' group by course_id)po3)
                on (library.library_id = course.library_id and course.course_id = po3.course_id) where library.status=1 group by library.library_id, po3.course_id";
        
        $query=$this->db->query($sql);
        return $query;
    }
    function getAVGCourseScores($user_id){
        $sql = "select library.library_id, course.course_id, po3.avg from library left join
                (course, (select course_id, chapter_id, AVG(points) as avg from 
                (select * from (select * from points order by points_get_time desc)po where course_id != '0')po2 where chapter_id!='0' group by course_id)po3)
                on (library.library_id = course.library_id and course.course_id = po3.course_id) where library.status=1 group by library.library_id ,po3.course_id";
        
        $query=$this->db->query($sql);
        return $query;
    }
    function getChapterScores($user_id){
        $sql = "select library.library_id, course.course_id, po3.chapter_id, po3.points from library left join
                (course, (select course_id, chapter_id, points from 
                (select * from (select * from points order by points_get_time desc)po where user_id='$user_id' and course_id != '0')po2 where chapter_id!='0' group by course_id, chapter_id)po3)
                on (library.library_id = course.library_id and course.course_id = po3.course_id) where library.status=1 group by po3.course_id, po3.chapter_id";
        
        $query=$this->db->query($sql);
        return $query;
    }
    function getLibAnalysis($user_id){
        $sql = "select library.library_color,library.library_id,library_name, ifnull(AVG(po3.avg),0) as avg
                from library left join (course,(select course_id, chapter_id, AVG(points) as avg from  (select * from (select * from points order by points_get_time desc)po 
                where user_id='$user_id' and course_id != '0')po2 where chapter_id!='0' group by 
                course_id, user_id order by course_id)po3) on (library.library_id = course.library_id and course.course_id = po3.course_id)
                where library.status=1
                group by library_id";
        $query=$this->db->query($sql);
        return $query;
    }
	///
	// by JoeyC End
	///
	
	//軟性能力之問題解決
	//個人分數
	function getSolvePersonal($user_id){
        $sql = "select sum(best_answer)*5 + sum(votes) as `sum` from forum_content where user_id=$user_id";
        $query=$this->db->query($sql);
        return $query;
    }
    //最高分
    function getSolveHighest(){
        $sql = "select (sum(votes)+sum(best_answer)*5) `sum` from forum_content group by user_id order by `sum` desc limit 0,1";
        $query=$this->db->query($sql);
        return $query;
    }
    //總分
    function getSolveTotal(){
        $sql = "select (select sum(votes) as votes from forum_content)+(select sum(best_answer)*5 as best_answer from forum_content) as `sum`";
        $query=$this->db->query($sql);
        return $query;
    }
    //軟性能力之表達互動
    //個人分數
    function getExpressionPersonal($user_id){
        $sql = "select ((select count(*)*10 `sum` from forum_topic where user_id =$user_id)+
                (select ((select count(*) from forum_content where topic_flag =0 and user_id =$user_id)+(select count(*) from forum_comment where user_id =$user_id))*5 `sum`)+
                (select count(*) `sum` from forum_vote_record where user_id =$user_id)+
                (select count(*)*2 `sum` from forum_topic where user_id =$user_id and solved=1)+
                (select count(distinct forum_topic_id) `sum` from forum_topic_read where user_id =$user_id))`sum`";
        $query=$this->db->query($sql);
        return $query;
    }
    //最高分
    function getExpressionHighest(){
        $sql = "select ((select count(*)*10 `sum` from forum_topic group by user_id order by `sum` desc limit 0,1)+
                (select sum(a.sum+b.sum)*5 `sum` from (
                select user_id,count(*) `sum` from forum_content where forum_content.topic_flag =0 group by forum_content.user_id order by `sum` desc)a left join (
                select user_id,count(*) `sum` from forum_comment group by forum_comment.user_id order by `sum` desc) b on a.user_id = b.user_id group by a.user_id order by `sum` desc limit 0,1)+
                (select count(*) `sum` from forum_vote_record group by user_id order by `sum` desc limit 0,1)+
                (select count(*)*2 `sum` from forum_topic where solved=1 group by user_id order by `sum` desc limit 0,1)+
                (select count(distinct forum_topic_id) `sum` from forum_topic_read group by user_id order by `sum` desc limit 0,1))`sum`";
        $query=$this->db->query($sql);
        return $query;
    }
    //總分
    function getExpressionTotal(){
        $sql = "select ((select count(*)*10 `sum` from forum_topic)+
                (select ((select count(*) from forum_content where topic_flag =0)+
                (select count(*) from forum_comment))*5 `sum`)+
                (select count(*) `sum` from forum_vote_record)+
                (select count(*)*2 `sum` from forum_topic where solved=1)+
                (select count(distinct forum_topic_id,user_id) `sum` from forum_topic_read))`sum`";
        $query=$this->db->query($sql);
        return $query;
    }
    //軟性能力之積極主動
    //個人分數
    function getProactivePersonal($user_id){
        $sql = "select sum(case
                    when user_course.status = 1 then user_chapter.status
                    when user_course.status = 0 then 0
                end) `sum1`,sum(case
                    when user_chapter.status = 1 then (user_chapter_content.status)/2
                    when user_chapter.status = 0 then 0
                end) `sum2`,sum(user_chapter_content.status) `sum3`
                from user_course,user_chapter,user_chapter_content
                where user_course.user_course_id = user_chapter.user_course_id
                and user_chapter.user_chapter_id = user_chapter_content.user_chapter_id
                and user_course.user_id =$user_id";
        $query=$this->db->query($sql);
        return $query;
    }
    //最高分
    function getProactiveHighest(){
        $sql = "select sum(case
                  when user_course.status = 1 then user_chapter.status
                  when user_course.status = 0 then 0
                end) `sum1`,sum(case
                  when user_chapter.status = 1 then (user_chapter_content.status)/2
                  when user_chapter.status = 0 then 0
                end) `sum2`,sum(user_chapter_content.status) `sum3`
                from user_course,user_chapter,user_chapter_content
                where user_course.user_course_id = user_chapter.user_course_id
                and user_chapter.user_chapter_id = user_chapter_content.user_chapter_id group by user_id";
        $query=$this->db->query($sql);
        return $query;
    }
    //總分
    function getProactiveTotal(){
        $sql = "select sum(case
                  when user_course.status = 1 then user_chapter.status
                  when user_course.status = 0 then 0
                end) `sum1`,sum(case
                  when user_chapter.status = 1 then (user_chapter_content.status)/2
                  when user_chapter.status = 0 then 0
                end) `sum2`,sum(user_chapter_content.status) `sum3`
                from user_course,user_chapter,user_chapter_content
                where user_course.user_course_id = user_chapter.user_course_id
                and user_chapter.user_chapter_id = user_chapter_content.user_chapter_id";
        $query=$this->db->query($sql);
        return $query;
    }
    //各個課程關卡任務學習分數
    // function getMyScore($user_id){
        // $sql = "select a.course_id,a.sum+b.sum+c.sum sum 
                // from (
                    // ( select user_course.course_id course_id,if(user_course.status=1,'60','0')sum
                      // from user_course
                      // where user_course.user_id=$user_id  order by course_id)a,
                    // ( select course_id,sum(chapter_score)sum 
                      // from (  select user_course.course_id course_id,user_course.status course_status,user_chapter.chapter_id,user_chapter.status chapter_status ,
                              // if(user_course.status=1,'60','0')course_score,if(user_chapter.status=1,'30','0')chapter_score
                              // from user_course , user_chapter
                              // where user_course.user_course_id = user_chapter.user_course_id
                              // and user_course.user_id=$user_id  order by course_id,chapter_id) a 
                      // group by a.course_id)b,
                    // ( select course_id,sum(content_score)sum 
                      // from (  select user_course.course_id course_id,user_course.status course_status,user_chapter.chapter_id,user_chapter.status chapter_status ,user_chapter_content.chapter_content_id,
                              // if(user_course.status=1,'60','0')course_score,
                              // if(user_chapter.status=1,'30','0')chapter_score,
                              // case when chapter_content.content_type=0 then '1'when chapter_content.content_type=1 then '6' else'10' end content_score
                              // from user_course , user_chapter ,user_chapter_content,chapter_content
                              // where user_course.user_course_id = user_chapter.user_course_id
                              // and user_chapter.user_chapter_id = user_chapter_content.user_chapter_id
                              // and user_chapter_content.chapter_content_id = chapter_content.chapter_content_id
                              // and user_course.user_id=$user_id group by user_chapter_content.chapter_content_id order by course_id,chapter_id)a 
                      // group by a.course_id)c)
                // where a.course_id = b.course_id and b.course_id = c.course_id";
        // $query=$this->db->query($sql);
        // return $query;
    // } 
    // function getAllScore(){
        // $sql = "select a.course_id,a.sum+b.sum+c.sum sum
                // from
                // ((  select course.course_id,course.status*60 sum
                    // from course
                    // where course.status=1 and sync_course = 0)a,
                 // (  select a.course_id,(sum(a.status)*30)sum 
                    // from (
                        // select course.course_id,chapter.status `status`
                        // from course,chapter
                        // where course.status=1 and sync_course = 0
                        // and course.course_id = chapter.course_id
                        // and chapter.status=1)a group by a.course_id)b,
                 // (  select course_id,sum(sum)sum 
                    // from (
                        // select course.course_id,case when chapter_content.content_type=0 then '1'when chapter_content.content_type=1 then '6' else'10' end sum
                        // from course,chapter,chapter_content
                        // where course.status=1 and sync_course = 0
                        // and course.course_id = chapter.course_id
                        // and chapter.status=1
                        // and chapter.chapter_id = chapter_content.chapter_id
                        // and chapter_content.award_video = 0)a 
                    // group by a.course_id)c)
                // where a.course_id = b.course_id
                // and b.course_id = c.course_id";
        // $query=$this->db->query($sql);
        // return $query;
    // }
    //第一層學程及個別課程 的相關數據
    function getAvgScore($user_id){
        $sql = "select *,case when `avg` >=80 then 'levelA' when `avg` >=60 then 'levelB' else 'levelC' end class  from (select a.course_id,round(ifnull (b.sum,'0')/a.sum*100)`avg`
                from (select a.course_id,a.sum+b.sum+c.sum sum
                from
                ((select course.course_id,course.status*60 sum
                from course
                where course.status=1 and sync_course = 0)a,(select a.course_id,(sum(a.status)*30)sum from (select course.course_id,chapter.status `status`
                from course,chapter
                where course.status=1 and sync_course = 0
                and course.course_id = chapter.course_id
                and chapter.status=1)a group by a.course_id)b,(select course_id,sum(sum)sum from (select course.course_id,case when chapter_content.content_type=0 then '1'when chapter_content.content_type=1 then '6' else'10' end sum
                from course,chapter,chapter_content
                where course.status=1 and sync_course = 0
                and course.course_id = chapter.course_id
                and chapter.status=1
                and chapter.chapter_id = chapter_content.chapter_id
                and chapter_content.award_video = 0)a group by a.course_id)c)
                where a.course_id = b.course_id
                and b.course_id = c.course_id)a left join (select a.course_id,a.sum+b.sum+c.sum sum from ((select user_course.course_id course_id,if(user_course.status=1,'60','0')sum
                from user_course
                where user_course.user_id=$user_id  order by course_id)a,(select course_id,sum(chapter_score)sum from (select user_course.course_id course_id,user_course.status course_status,user_chapter.chapter_id,
                user_chapter.status chapter_status ,
                if(user_course.status=1,'60','0')course_score,
                if(user_chapter.status=1,'30','0')chapter_score
                from user_course , user_chapter
                where user_course.user_course_id = user_chapter.user_course_id
                and user_course.user_id=$user_id  order by course_id,chapter_id) a group by a.course_id)b,(select course_id,sum(content_score)sum from (select user_course.course_id course_id,user_course.status course_status,user_chapter.chapter_id,user_chapter.status chapter_status ,user_chapter_content.chapter_content_id,
                if(user_course.status=1,'60','0')course_score,
                if(user_chapter.status=1,'30','0')chapter_score,
                case when chapter_content.content_type=0 then '1'when chapter_content.content_type=1 then '6' else'10' end content_score
                from user_course , user_chapter ,user_chapter_content,chapter_content
                where user_course.user_course_id = user_chapter.user_course_id
                and user_chapter.user_chapter_id = user_chapter_content.user_chapter_id
                and user_chapter_content.chapter_content_id = chapter_content.chapter_content_id
                and user_course.user_id=$user_id group by user_chapter_content.chapter_content_id order by course_id,chapter_id)a group by a.course_id)c)
                where a.course_id = b.course_id and b.course_id = c.course_id)b on a.course_id = b.course_id)temp";
        $query=$this->db->query($sql);
        return $query;
    }
    //第二層關卡的相關數據
    function getContentAvgScore($user_id,$course_url){
        $sql = "select a.chapter_id,a.sum `sum`,b.sum `sum1`
from
(select a.chapter_id,ifnull(sum(a.`sum`+a.`sum1`),0) sum from (
select chapter.chapter_id,sum(a.chapter_score)/count(*) `sum`,sum(a.content_score)`sum1` from course,chapter
  left join (select course.course_name,user_course.course_id,user_chapter.chapter_id,
            if(user_chapter.status=1,'30','0')chapter_score,
            case when chapter_content.content_type=0 then '1'
                 when chapter_content.content_type=1 then '6'
                  else'10'
            end content_score
from user_course , user_chapter ,user_chapter_content,chapter_content,course
where user_course.user_course_id = user_chapter.user_course_id
and user_chapter.user_chapter_id = user_chapter_content.user_chapter_id
and user_chapter_content.chapter_content_id = chapter_content.chapter_content_id
and user_course.user_id=$user_id
and course.course_id = user_course.course_id
and course.course_url = '$course_url'
group by user_chapter_content.chapter_content_id)a on chapter.chapter_id = a.chapter_id
where course.course_id = chapter.course_id
and course.course_url = '$course_url'
group by a.chapter_id
order by chapter.chapter_id)a group by a.chapter_id)a,
(select a.chapter_id,sum(a.sum)+30 `sum`  from (select course.course_id,chapter.chapter_id,case when chapter_content.content_type=0 then '1'when chapter_content.content_type=1 then '6' else'10' end sum
from course,chapter,chapter_content
where course.course_url = '$course_url'
and course.status=1 and sync_course = 0 and chapter_content.award_video = 0
and course.course_id = chapter.course_id
and chapter.chapter_id = chapter_content.chapter_id)a group by a.chapter_id)b where a.chapter_id = b.chapter_id";
        $query=$this->db->query($sql);
        return $query;
    }
    
    
    
    
    
    
}