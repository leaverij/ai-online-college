<?php
class Accesslog_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
	}

	function getaccessLogByChapterId($id){
		$sql="
			SELECT  user_id, user_name, chapter_content_id, count(id) as click_num
			FROM 	access_log
			WHERE 	chapter_id = ?
			AND 	is_enrolled = 1
			AND 	chapter_content_id is not null
			GROUP BY user_id, chapter_content_id
			ORDER BY user_id
		";
    	$query=$this->db->query($sql,array($id));
		return $query;
	}

}