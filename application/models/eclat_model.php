<?php
class Eclat_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }

    //新增儒鴻上傳檔案
    function insertEclatRecord($data)
	{
        $this -> db -> insert('eclat', $data);
        $insert_id = $this->db->insert_id();
   		return  $insert_id;
    }
    
    //取得儒鴻上傳檔案列表
    function listEclatFiles(){
        $sql = "select * from eclat order by upload_date desc";
        $query = $this->db->query($sql);
    	return $query;
    }

}