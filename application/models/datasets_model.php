<?php
class Datasets_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
    
	function readAll($user_id){
		$sql = "select * from datasets where user_id=$user_id and delete_flag=0";
		$query = $this->db->query($sql)->result();
		return $query;
	}

	function create($datasets){
		$query = $this->db->insert('datasets', $datasets); 
		return $this->db->affected_rows();
	}
	
	function update($user_id,$datasets_name,$data){
		$this -> db -> where('user_id', $user_id);
		$this -> db -> where('datasets_name', $datasets_name);
		$this -> db -> update('datasets', $data);
	}
	
	
	function check_datasets_name($datasets_name,$user_id)
    {
    	$sql="select * from datasets where datasets_name='$datasets_name' and user_id='$user_id' and delete_flag=0";
    	$query=$this->db->query($sql);
    	return $query->num_rows()==0?"0":"1";		
    }
    function listPublic($user_id){
		$sql = "select datasets.*,user.alias from datasets,user where datasets.user_id!=$user_id and datasets.delete_flag=0 and datasets.open =1 and datasets.user_id=user.user_id";
		$query = $this->db->query($sql)->result();
		return $query;
	}
	// 檢查$user_alias的$datasets_name是否為不公開的dataset
	function is_private_datasets($user_alias,$datasets_name)
    {
    	$sql="select datasets.*,user.alias from datasets,user where datasets.datasets_name='$datasets_name' and datasets.delete_flag=0 and datasets.open =0 and datasets.user_id=user.user_id and user.alias='$user_alias'";
    	$query=$this->db->query($sql);
    	return $query->num_rows()==0?"0":"1";		
    }
}