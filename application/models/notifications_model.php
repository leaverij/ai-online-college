<?php
class Notifications_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
		
    }
	//取得指定使用者的通知訊息
    function getNotification($email){
    	$sql = "select notification.*,b.alias,b.img from notification
    	left join user a on a.user_id=notification.user_id
    	left join user b on b.user_id=notification.notifier_id
    	where a.email ='$email' order by notification_time desc";
    	$query=$this->db->query($sql);
    	return $query;
    }
	//取得指定使用者的通知訊息
    function getDropdownNotification($email){
    	$sql = "select notification.*,b.alias,b.img from notification
    	left join user a on a.user_id=notification.user_id
    	left join user b on b.user_id=notification.notifier_id
    	where a.email ='$email' order by notification_time desc
    	limit 0,10";
    	$query=$this->db->query($sql);
    	return $query;
    }
    //新增指定使用者擁有的的通知訊息
    function addNotification($notification){
    	$sql="insert into notification (user_id,notification_type,notification_title,notification_content,hyperlink,notification_time,notifier_id) values (?,?,?,?,?,?,?)";
    	$query=$this->db->query($sql,$notification);
    	return $this->db->affected_rows();
    }
    
    //取得指定使用者的未讀訊息數量
    function getUnreadNotification($email){
    	$sql = "select notification.* from notification
    	left join user on user.user_id=notification.user_id
    	where user.email ='$email' and is_read=0 order by notification_time";
    	$query=$this->db->query($sql);
    	return $query->num_rows();
    }
    
    function updateNotification($user_id){
    	$sql = "update notification set is_read=1 where user_id='$user_id'";
    	$query=$this->db->query($sql);
    	return $this->db->affected_rows();
    }
    //取得指定使用者的通知訊息
    function getNotificationPagination($start,$per_page,$email){
    	$sql = "select notification.*,b.alias,b.img from notification
    	left join user a on a.user_id=notification.user_id
    	left join user b on b.user_id=notification.notifier_id
    	where a.email ='$email' order by notification_time desc
    	limit $start,$per_page";
    	$query=$this->db->query($sql);
    	return $query;
    }
}