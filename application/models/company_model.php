<?php
class Company_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
	}


	function getCompany(){
		$sql="SELECT * FROM company";
    	$query=$this->db->query($sql);
		return $query;		
	}

	function addComapany($company){
		$query=$this->db->insert('company',$company);
		return $this->db->insert_id();
	}

	function deleteCompany($tax_id_number){
		$sql = "delete from company where tax_id_number ='$tax_id_number'";
    	$query=$this->db->query($sql);
		return $this->db->affected_rows();
	}

	function getOneCompany($tax_id_number){
		$sql = "select * from company where tax_id_number ='$tax_id_number'";
		$query=$this->db->query($sql);
		return $query;
	}
	function updateCompany($tax_id_number,$company){
		$this -> db -> where('tax_id_number', $tax_id_number);
		$this -> db -> update('company', $company);
	}
}