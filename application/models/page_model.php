<?php
class Page_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
	}
	//列出團隊資料
	function listTeam()
	{
		$sql = "select * from lh_team";
    	$query=$this->db->query($sql);
    	return $query;
	}
	
	//取得全部AI準備度測驗
	function listAllAIAssessment(){
		$sql = "select * from ai_assessment where is_open=true order by create_date asc";
		$query=$this->db->query($sql);
    	return $query;
	}

	//取得單一AI準備度測驗 BY URL
	function listAllAIAssessmentByUrl($assessment_url){
		$sql = "select * from ai_assessment where url='$assessment_url'";
		$query=$this->db->query($sql);
    	return $query;
	}

	//取得單一AI準備度測驗 BY ID
	function listAllAIAssessmentById($assessment_id){
		$sql = "select * from ai_assessment where id='$assessment_id'";
		$query=$this->db->query($sql);
    	return $query;
	}

	//使用者開始AI準備度測驗
	function insertAIAssessmentUser($data){
		$this -> db -> insert('ai_assessment_access', $data);
	}

	//檢查使用者是否開始AI準備度測驗
	function checkUserStartAIAssessment($userId, $assessment_id){
		$sql = "select * from ai_assessment_access where user_id='$userId' and assessment_id=$assessment_id";
		$query=$this->db->query($sql);
    	return $query;
	}

	//使用者回答AI準備度測驗問題
	function userAnswerAIAssessmentQuestion($data){
		$this -> db -> insert('ai_assessment_answer', $data);
	}

	//使用者繳交AI準備度測驗
	function userSubmitAIAssessment($data){
		$this -> db -> insert('ai_assessment_result', $data);
	}

	//取得AI準備度測驗分數:問題定義
	function getAIAssessmentScoreByUserIdDef($assessment_id, $user_id){
		$sql = "select count(*)*9 as score
		from (select * from ai_assessment_answer where assessment_id=$assessment_id and user_id='$user_id' and is_correct='1' order by answer_date desc limit 11) r";
		$query = $this->db->query($sql);
    	return $query;
	}

	//取得AI準備度測驗能力分布:問題定義
	function getAIAssessmentCapabilityDef($assessment_id, $user_id){
		$sql = "select r.sub_ability_name as name,count(*)*9 as y from 
		(select * from ai_assessment_answer where assessment_id=$assessment_id and user_id='$user_id' and is_correct='1' order by answer_date desc limit 11) r
		group by r.sub_ability_name order by y desc";
		$query = $this->db->query($sql);
    	return $query;
	}

	//取得AI準備度測驗分數:問題解決
	function getAIAssessmentScoreByUserId($assessment_id, $user_id){
		$sql = "select sum(r.answer_score) as score from (select * from ai_assessment_answer where assessment_id=$assessment_id and user_id='$user_id' order by answer_date desc limit 16) r";
		$query = $this->db->query($sql);
    	return $query;
	}

	//取得AI準備度測驗能力分布:問題解決
	function getAIAssessmentCapability($assessment_id, $user_id){
		$sql = "select r.sub_ability_name as name,sum(r.answer_score) as y from 
		(select * from ai_assessment_answer where assessment_id=$assessment_id and user_id='$user_id' order by answer_date desc limit 16) r
		group by r.sub_ability_name order by y desc";
		$query = $this->db->query($sql);
    	return $query;
	}

	//取得使用者繳交AI準備度測驗繳交結果
	function getUserAIAssessmentSubmitResult($assessment_id, $user_id){
		$sql = "select * from ai_assessment_result where assessment_id=$assessment_id and user_id='$user_id' limit 1";
		$query = $this->db->query($sql);
    	return $query;
	}

}