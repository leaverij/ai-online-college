/*
SQLyog Community Edition- MySQL GUI v7.1 Beta2
MySQL - 5.6.16-log : Database - aioc
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`aioc` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `aioc`;

/*Table structure for table `competition` */

DROP TABLE IF EXISTS `competition`;

CREATE TABLE `competition` (
  `competition_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL COMMENT '競賽名稱',
  `subtitle` varchar(256) DEFAULT NULL COMMENT '競賽子標題',
  `creator_id` int(11) DEFAULT NULL COMMENT '建立者ID',
  `create_time` datetime DEFAULT NULL COMMENT '建立時間',
  `is_publish` tinyint(1) DEFAULT '0' COMMENT '是否上架',
  `organization` int(11) DEFAULT NULL COMMENT '主辦公司ID',
  `start_time` datetime DEFAULT NULL COMMENT '開始時間',
  `end_time` datetime DEFAULT NULL COMMENT '結束時間',
  `deadline` datetime DEFAULT NULL COMMENT '繳交期限',
  `is_public` tinyint(1) DEFAULT '1' COMMENT '是否公開',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '使否刪除',
  `competition_category_id` int(11) DEFAULT NULL COMMENT '類型',
  `competition_image` varchar(256) DEFAULT NULL COMMENT '競賽圖片',
  `description` text COMMENT '競賽說明',
  `views` int(11) DEFAULT '0' COMMENT '瀏覽次數',
  PRIMARY KEY (`competition_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `competition` */

/*Table structure for table `competition_category` */

DROP TABLE IF EXISTS `competition_category`;

CREATE TABLE `competition_category` (
  `competition_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL COMMENT '類別名稱',
  `description` text COMMENT '類別描述',
  `creator_id` int(11) DEFAULT NULL COMMENT '建立者ID',
  `create_time` datetime DEFAULT NULL COMMENT '建立時間',
  `is_active` tinyint(1) DEFAULT '0' COMMENT '是否可用',
  PRIMARY KEY (`competition_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `competition_category` */

insert  into `competition_category`(`competition_category_id`,`name`,`description`,`creator_id`,`create_time`,`is_active`) values (1,'旅遊','旅遊資料分析',1,NULL,1),(2,'保險','保險資料預測',1,NULL,1),(3,'紡織','紡織資料檢核',1,NULL,1),(4,'醫療','醫療資料預測',1,NULL,1),(5,'交通','交通資料預測',1,NULL,1),(6,'教育','教育資料預測',1,NULL,1);

/*Table structure for table `competition_detail` */

DROP TABLE IF EXISTS `competition_detail`;

CREATE TABLE `competition_detail` (
  `competition_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `competition_id` int(11) DEFAULT NULL COMMENT '競賽ID',
  `creator_id` int(11) DEFAULT NULL COMMENT '建立者ID',
  `title` varchar(32) DEFAULT NULL COMMENT '細節標題',
  `content` text COMMENT '細節內容',
  `create_time` datetime DEFAULT NULL COMMENT '建立時間',
  `is_publish` tinyint(1) DEFAULT '0' COMMENT '是否公布',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否刪除',
  PRIMARY KEY (`competition_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `competition_detail` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
