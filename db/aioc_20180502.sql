ALTER TABLE `jupyter` ADD `status` VARCHAR(1) NOT NULL COMMENT '虛擬機目前狀態, 1:open 0:close' AFTER `instanceURL`, 
ADD `period` INT(10) NOT NULL DEFAULT '0' COMMENT '使用累積時間(秒)' AFTER `status`;
ALTER TABLE `jupyter` ADD `restartTime` DATETIME NOT NULL COMMENT 'instance重起時間' AFTER `startTime`;