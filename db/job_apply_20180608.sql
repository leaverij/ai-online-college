-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2018-06-08 09:04:14
-- 伺服器版本: 10.1.21-MariaDB
-- PHP 版本： 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `aioc`
--

-- --------------------------------------------------------

--
-- 資料表結構 `job_apply`
--

CREATE TABLE `job_apply` (
  `apply_id` int(11) NOT NULL COMMENT 'job apply id',
  `user_id` int(11) NOT NULL COMMENT '使用者id',
  `job_vacancy_id` int(11) NOT NULL COMMENT '工作職缺id',
  `apply_msg` text NOT NULL COMMENT '應徵自我推薦',
  `apply_date` datetime NOT NULL COMMENT '應徵申請時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='使用者對應工作職缺表';

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `job_apply`
--
ALTER TABLE `job_apply`
  ADD PRIMARY KEY (`apply_id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `job_apply`
--
ALTER TABLE `job_apply`
  MODIFY `apply_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'job apply id';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
