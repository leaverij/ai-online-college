-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- 主機: localhost
-- 產生時間： 2018 年 04 月 19 日 12:06
-- 伺服器版本: 10.1.28-MariaDB
-- PHP 版本： 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `aioc`
--

-- --------------------------------------------------------

--
-- 資料表結構 `access_log`
--

CREATE TABLE `access_log` (
  `id` int(11) NOT NULL COMMENT '流水號',
  `user_id` int(11) DEFAULT NULL COMMENT '使用者ID',
  `track_id` int(11) DEFAULT NULL COMMENT '學程ID',
  `course_id` int(11) DEFAULT NULL COMMENT '課程ID',
  `chapter_id` int(11) DEFAULT NULL COMMENT '關卡ID',
  `chapter_content_id` int(11) DEFAULT NULL COMMENT '任務ID',
  `chapter_content_type` int(11) DEFAULT NULL COMMENT '任務型態,0:影片,1:測驗,2:填充,3:文章,4:實做,5:編譯',
  `access_time` datetime DEFAULT NULL COMMENT '點擊時間',
  `is_enrolled` tinyint(1) DEFAULT '0' COMMENT '使用者是否註冊該課程',
  `user_mail` varchar(256) DEFAULT NULL COMMENT '使用者Email',
  `user_name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `ai_assessment`
--

CREATE TABLE `ai_assessment` (
  `id` int(11) NOT NULL COMMENT '流水號',
  `name` varchar(256) DEFAULT NULL COMMENT '測驗名稱',
  `url` varchar(256) DEFAULT NULL COMMENT '測驗路徑',
  `is_open` tinyint(1) DEFAULT NULL COMMENT '是否開放',
  `creator_id` int(11) DEFAULT NULL COMMENT '建立者',
  `create_date` datetime DEFAULT NULL COMMENT '建立日期',
  `description` text COMMENT '測驗說明'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `ai_assessment_access`
--

CREATE TABLE `ai_assessment_access` (
  `id` int(11) NOT NULL,
  `user_id` varchar(32) DEFAULT NULL COMMENT '使用者ID',
  `assessment_id` int(11) DEFAULT NULL COMMENT '測驗ID',
  `start_date` datetime DEFAULT NULL COMMENT '開始日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `ai_assessment_answer`
--

CREATE TABLE `ai_assessment_answer` (
  `id` int(11) NOT NULL,
  `assessment_id` int(11) DEFAULT NULL COMMENT '測驗ID',
  `question_id` int(11) DEFAULT NULL COMMENT '問題ID',
  `user_id` varchar(32) DEFAULT NULL COMMENT '使用者ID',
  `answer` text COMMENT '使用者答案',
  `answer_date` datetime DEFAULT NULL COMMENT '回答時間',
  `answer_encode` text COMMENT '使用者答案編碼',
  `is_correct` tinyint(1) DEFAULT NULL,
  `answer_score` int(11) NOT NULL COMMENT '選項分數',
  `sub_ability_name` varchar(256) NOT NULL COMMENT '子能力'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `ai_assessment_result`
--

CREATE TABLE `ai_assessment_result` (
  `id` int(11) NOT NULL,
  `assessment_id` int(11) DEFAULT NULL,
  `user_id` varchar(32) DEFAULT NULL,
  `submit_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `chapter`
--

CREATE TABLE `chapter` (
  `chapter_id` int(11) NOT NULL COMMENT '關卡ID',
  `chapter_name` varchar(50) NOT NULL COMMENT '關卡名稱',
  `chapter_pic` varchar(50) NOT NULL DEFAULT 'LH-default-stage.png' COMMENT '關卡徽章圖',
  `chapter_diff` tinyint(1) DEFAULT '0' COMMENT '關卡難易度 0:beginner;1:intermediate;2:advanced',
  `chapter_desc` varchar(256) DEFAULT NULL COMMENT '關卡描述',
  `chapter_url` varchar(50) DEFAULT NULL COMMENT '關卡URL',
  `syllabus` varchar(256) DEFAULT NULL COMMENT '教學大綱',
  `create_time` datetime DEFAULT NULL COMMENT '建立時間',
  `status` tinyint(1) DEFAULT '1' COMMENT '狀態 0:Cancel;1:Available',
  `course_id` int(11) NOT NULL COMMENT '課程ID',
  `chapter_order` int(11) NOT NULL DEFAULT '0' COMMENT '關卡順序'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `chapter_content`
--

CREATE TABLE `chapter_content` (
  `chapter_content_id` int(11) NOT NULL,
  `chapter_content_desc` varchar(256) NOT NULL COMMENT 'content的描述',
  `chapter_content_url` varchar(64) NOT NULL COMMENT 'content的url',
  `chapter_id` int(11) NOT NULL,
  `question` longtext NOT NULL,
  `content` longtext NOT NULL,
  `subtitle` varchar(20) NOT NULL,
  `content_type` int(11) NOT NULL COMMENT '0:影片1:測驗2:實作',
  `apiquiz` int(11) DEFAULT '0',
  `preview` tinyint(4) NOT NULL COMMENT '0:Enable1:Disable',
  `hits` int(11) NOT NULL DEFAULT '0' COMMENT '一班使用者點擊數',
  `user_hits` int(11) NOT NULL DEFAULT '0' COMMENT '會員點擊數',
  `duration` varchar(10) NOT NULL COMMENT '影片長度',
  `snapshot` varchar(32) NOT NULL,
  `content_open_flag` int(2) NOT NULL DEFAULT '0' COMMENT 'content_開關 0:未開放 ; 1:開放',
  `content_order` int(32) NOT NULL COMMENT '任務順序',
  `award_video` tinyint(4) NOT NULL,
  `content_desc` text NOT NULL COMMENT '關卡說明',
  `aws_s3_uuid` varchar(48) NOT NULL COMMENT '檔案上傳S3 UUID',
  `aws_s3_name` varchar(128) NOT NULL COMMENT '檔案上傳S3 名稱',
  `is_gym` tinyint(1) NOT NULL,
  `create_date` datetime NOT NULL COMMENT '任務建立時間',
  `time_spent` int(11) NOT NULL COMMENT 'LAB時間',
  `lab_type` int(11) NOT NULL DEFAULT '0' COMMENT 'lab類型',
  `video_duration` int(11) NOT NULL DEFAULT '0' COMMENT '影片長度(秒)',
  `right_code_content` longtext,
  `hint` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `content_group`
--

CREATE TABLE `content_group` (
  `content_group_id` int(11) NOT NULL COMMENT '分組流水號',
  `name` varchar(32) DEFAULT NULL COMMENT '分組名稱',
  `create_time` datetime DEFAULT NULL COMMENT '分組建立時間',
  `is_active` tinyint(1) DEFAULT '1' COMMENT '分組是否可用',
  `teacher_id` int(11) DEFAULT NULL COMMENT '分組建立人',
  `course_id` int(11) DEFAULT NULL COMMENT '分組所屬課程'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `coupon`
--

CREATE TABLE `coupon` (
  `coupon_id` int(11) NOT NULL COMMENT '優惠卷ID',
  `coupon_code` varchar(19) NOT NULL COMMENT '優惠卷序號',
  `create_time` datetime NOT NULL COMMENT '建立時間',
  `price` int(11) NOT NULL COMMENT '價格',
  `start_time` datetime DEFAULT NULL COMMENT '帳號開始時間',
  `end_time` datetime DEFAULT NULL COMMENT '帳號結束時間',
  `comment` varchar(20) NOT NULL,
  `user_email` varchar(50) NOT NULL COMMENT '使用者mail',
  `used_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '帳號狀態 0:未使用;1:已使用',
  `used_time` datetime NOT NULL COMMENT '使用時間',
  `course_id` int(11) DEFAULT NULL COMMENT '被哪個課程註冊'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `course`
--

CREATE TABLE `course` (
  `course_id` int(11) NOT NULL COMMENT '課程ID',
  `course_name` varchar(50) NOT NULL COMMENT '課程名稱',
  `course_pic` varchar(50) NOT NULL DEFAULT 'LH-default-course.png' COMMENT '課程圖片',
  `course_type` tinyint(1) DEFAULT '2' COMMENT '課程類型 0:尚未設定;1:專案技巧;2:知識秘笈',
  `course_desc` varchar(256) DEFAULT NULL COMMENT '課程簡介',
  `course_price` int(11) NOT NULL COMMENT '課程價格，0表示免費課程',
  `course_url` varchar(50) NOT NULL COMMENT '課程URL',
  `learning_target` varchar(256) DEFAULT NULL COMMENT '學習目標',
  `suitable_for` varchar(256) DEFAULT NULL COMMENT '適用對象',
  `prior_knowledge` varchar(256) DEFAULT NULL COMMENT '先備知識',
  `references` varchar(256) DEFAULT NULL COMMENT '參考資料',
  `create_time` datetime DEFAULT NULL COMMENT '建立時間',
  `status` tinyint(1) DEFAULT '0' COMMENT '狀態 0:Cancel;1:Available',
  `sync_course` tinyint(1) NOT NULL COMMENT '同步課程 1:是;2:否',
  `teacher_id` int(11) NOT NULL COMMENT '課程講師',
  `library_id` int(11) NOT NULL COMMENT '知識庫ID',
  `course_category_id` int(11) NOT NULL COMMENT '課程分類ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `course_announcement`
--

CREATE TABLE `course_announcement` (
  `id` int(11) NOT NULL COMMENT '流水號',
  `title` varchar(256) DEFAULT NULL COMMENT '公告標題',
  `content` text COMMENT '公告內容',
  `teacher_id` int(11) DEFAULT NULL COMMENT '教師ID',
  `course_id` int(11) DEFAULT NULL COMMENT '課程ID',
  `create_date` datetime DEFAULT NULL COMMENT '公告建立日期',
  `announce_date` datetime DEFAULT NULL COMMENT '公告日期',
  `is_delete` tinyint(1) DEFAULT NULL COMMENT '是否刪除',
  `view_counts` int(11) DEFAULT '0' COMMENT '瀏覽次數'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `course_category`
--

CREATE TABLE `course_category` (
  `course_category_id` int(11) NOT NULL COMMENT '課程分類ID',
  `course_category_name` varchar(50) NOT NULL COMMENT '課程分類名稱',
  `category_color` varchar(7) NOT NULL DEFAULT '#b7c0c7' COMMENT '課程分類顏色'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `course_order`
--

CREATE TABLE `course_order` (
  `order_id` int(11) NOT NULL COMMENT '訂單ID',
  `type` varchar(20) NOT NULL COMMENT '商品類型 course(課程) or tracks(學程)',
  `course_id` int(11) NOT NULL COMMENT '課程ID',
  `tracks_id` int(11) NOT NULL DEFAULT '0' COMMENT '學程ID',
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `order_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '訂單時間',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '價格',
  `paid` int(11) NOT NULL DEFAULT '0' COMMENT '已付價格',
  `coupon_code` varchar(16) NOT NULL COMMENT '優惠券碼',
  `comment` varchar(60) NOT NULL COMMENT '註解',
  `live_flag` tinyint(1) DEFAULT '0' COMMENT '使用狀態 0:無效;1:有效'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `dl_resource`
--

CREATE TABLE `dl_resource` (
  `dl_resource_id` int(11) NOT NULL COMMENT '資源ID',
  `dl_resource_url` varchar(100) NOT NULL COMMENT '資源下載連結',
  `chapter_id` int(11) NOT NULL COMMENT '關卡ID',
  `downloads` int(11) NOT NULL COMMENT '下載次數'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `domain`
--

CREATE TABLE `domain` (
  `domain_id` int(11) NOT NULL COMMENT '領域分類ID',
  `domain_name` varchar(50) NOT NULL COMMENT '領域分類名稱',
  `domain_desc` text COMMENT '領域分類簡介',
  `domain_url` varchar(50) NOT NULL COMMENT '領域分類URL',
  `domain_pic` varchar(50) NOT NULL COMMENT '領域分類圖片',
  `domain_order` int(11) NOT NULL COMMENT '領域分類排序',
  `status` tinyint(1) DEFAULT '1' COMMENT '狀態 0:Cancel;1:Available'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `domain_library`
--

CREATE TABLE `domain_library` (
  `domain_library_id` int(11) NOT NULL COMMENT '學院學程ID',
  `domain_id` varchar(50) NOT NULL COMMENT '學院ID',
  `library_id` varchar(50) NOT NULL COMMENT '知識庫ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `eclat`
--

CREATE TABLE `eclat` (
  `id` int(11) NOT NULL,
  `encode_name` varchar(256) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `upload_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `email_record`
--

CREATE TABLE `email_record` (
  `email_record_id` int(11) NOT NULL COMMENT '信件紀錄ID',
  `addressee` varchar(50) DEFAULT NULL COMMENT '收件人',
  `email_time` datetime NOT NULL COMMENT '寄件時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='信件紀錄';

-- --------------------------------------------------------

--
-- 資料表結構 `forget_password`
--

CREATE TABLE `forget_password` (
  `forget_password_id` int(11) NOT NULL COMMENT '忘記密碼ID',
  `email` varchar(50) NOT NULL COMMENT '信箱',
  `code` varchar(64) NOT NULL COMMENT '驗證碼',
  `time` datetime NOT NULL COMMENT '申請時間',
  `ip` varchar(32) NOT NULL COMMENT 'IP位置',
  `forget_password_used_flag` int(3) NOT NULL DEFAULT '0' COMMENT '是否使用了 0:未使用;1:已使用'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `forum_assistant`
--

CREATE TABLE `forum_assistant` (
  `forum_assistant_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `library_id` int(11) NOT NULL,
  `support_type` int(11) NOT NULL COMMENT '0:助教 1:老師 2:總監'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `forum_comment`
--

CREATE TABLE `forum_comment` (
  `forum_comment_id` int(11) NOT NULL COMMENT '討論區回覆ID',
  `forum_comment` text COMMENT '回覆內容',
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `comment_posted_time` datetime DEFAULT NULL COMMENT '回覆時間',
  `forum_content_id` int(11) NOT NULL COMMENT '討論區內容ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='討論區_回覆';

-- --------------------------------------------------------

--
-- 資料表結構 `forum_content`
--

CREATE TABLE `forum_content` (
  `forum_content_id` int(11) NOT NULL COMMENT '討論區內容ID',
  `topic_flag` int(2) NOT NULL COMMENT '題目flag 1:題目 2:回文',
  `forum_content` text COMMENT '內容',
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `content_posted_time` datetime DEFAULT NULL COMMENT '發表時間',
  `votes` int(11) DEFAULT '0' COMMENT '評比',
  `best_answer` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:最佳解答',
  `forum_topic_id` int(11) NOT NULL COMMENT '討論區主題ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='討論區_內容';

-- --------------------------------------------------------

--
-- 資料表結構 `forum_subscription`
--

CREATE TABLE `forum_subscription` (
  `forum_subscription_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `forum_topic_id` int(11) NOT NULL,
  `subscription_time` datetime NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `forum_topic`
--

CREATE TABLE `forum_topic` (
  `forum_topic_id` int(11) NOT NULL COMMENT '討論區主題ID',
  `forum_topic` text COMMENT '主題',
  `library_id` int(11) NOT NULL COMMENT '(知識庫)分類ID',
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `posted_time` datetime DEFAULT NULL COMMENT '發表時間',
  `clicks` int(11) NOT NULL DEFAULT '0' COMMENT '點擊次數',
  `solved` int(11) NOT NULL COMMENT '問題是否解決 0:尚未解決;1:已經解決',
  `solved_time` datetime NOT NULL COMMENT '最佳解成立時間',
  `course_url` varchar(50) DEFAULT NULL COMMENT '課程網址'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='討論區主題';

-- --------------------------------------------------------

--
-- 資料表結構 `forum_topic_read`
--

CREATE TABLE `forum_topic_read` (
  `forum_topic_read_id` int(11) NOT NULL COMMENT '閱讀文章ID',
  `forum_topic_id` int(11) NOT NULL COMMENT '文章ID',
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `read_time` datetime NOT NULL COMMENT '閱讀時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `forum_vote_record`
--

CREATE TABLE `forum_vote_record` (
  `forum_vote_record_id` int(11) NOT NULL COMMENT '討論區投票紀錄ID',
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `forum_content_id` int(11) NOT NULL COMMENT '討論區內容_ID',
  `vote` int(2) NOT NULL COMMENT '投票',
  `vote_date` datetime DEFAULT NULL COMMENT '投票時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `group_member`
--

CREATE TABLE `group_member` (
  `member_id` int(11) NOT NULL COMMENT '分組成員ID',
  `user_id` int(11) DEFAULT NULL COMMENT '使用者ID',
  `content_group_id` int(11) DEFAULT NULL COMMENT '分組ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `industry`
--

CREATE TABLE `industry` (
  `industry_id` int(11) NOT NULL COMMENT '產業新聞ID',
  `industry_title` varchar(50) NOT NULL COMMENT '產業新聞標題',
  `industry_url` varchar(500) NOT NULL COMMENT '產業新聞URL',
  `industry_source` varchar(100) NOT NULL COMMENT '資料出處',
  `industry_origin_date` datetime NOT NULL COMMENT '資料原始時間',
  `domain_id` int(11) NOT NULL COMMENT '領域分類ID',
  `jobs_category_id` varchar(20) NOT NULL COMMENT '職務分類ID(type:array)',
  `industry_post_date` datetime NOT NULL COMMENT '發表時間',
  `industry_status` tinyint(1) DEFAULT '1' COMMENT '狀態 0:Cancel;1:Available',
  `clicks` int(11) NOT NULL DEFAULT '0' COMMENT '點擊次數',
  `user_id` int(11) NOT NULL COMMENT 'post user_id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `jobs`
--

CREATE TABLE `jobs` (
  `jobs_id` int(11) NOT NULL COMMENT '人才需求ID',
  `comp_name` varchar(50) NOT NULL COMMENT '人才需求廠商名稱',
  `jobs_title` varchar(50) NOT NULL COMMENT '人才需求標題',
  `jobs_url` varchar(500) NOT NULL COMMENT '人才需求URL',
  `domain_id` int(11) NOT NULL COMMENT '領域分類ID',
  `jobs_category_id` varchar(20) NOT NULL COMMENT '職務分類ID(type:array)',
  `jobs_post_date` datetime NOT NULL COMMENT '發表時間',
  `jobs_status` tinyint(1) DEFAULT '1' COMMENT '狀態 0:Cancel;1:Available',
  `clicks` int(11) NOT NULL DEFAULT '0' COMMENT '點擊次數',
  `user_id` int(11) NOT NULL COMMENT 'post user_id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `jobs_category`
--

CREATE TABLE `jobs_category` (
  `jobs_category_id` int(11) NOT NULL COMMENT '職務分類ID',
  `jobs_category_name` varchar(50) NOT NULL COMMENT '職務分類名稱',
  `jobs_category_desc` text NOT NULL COMMENT '職務分類描述',
  `jobs_category_url` varchar(50) NOT NULL COMMENT '職務分類URL',
  `jobs_category_order` int(3) NOT NULL COMMENT '職務分類排序',
  `status` tinyint(1) NOT NULL COMMENT '狀態 0:取消;1:正常'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `jobs_category_library`
--

CREATE TABLE `jobs_category_library` (
  `jobs_category_library_id` int(11) NOT NULL COMMENT '職業分類對應知識庫ID',
  `jobs_category_id` varchar(50) NOT NULL COMMENT '職業分類ID',
  `library_id` varchar(50) NOT NULL COMMENT '知識庫ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `jobs_category_tracks`
--

CREATE TABLE `jobs_category_tracks` (
  `jobs_category_tracks_id` int(11) NOT NULL COMMENT '職務分類學程ID',
  `jobs_category_id` int(11) NOT NULL COMMENT '職務分類ID',
  `tracks_id` int(11) NOT NULL COMMENT '學程ID',
  `order` int(3) NOT NULL COMMENT '職務分類排序'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `job_vacancy`
--

CREATE TABLE `job_vacancy` (
  `job_vacancy_id` int(11) NOT NULL,
  `job_name` varchar(100) NOT NULL,
  `jobs_category_id` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `county` varchar(20) NOT NULL,
  `district` varchar(20) NOT NULL,
  `working_time` varchar(30) NOT NULL,
  `job_content` varchar(4000) NOT NULL,
  `contact` varchar(30) NOT NULL,
  `contact_email` varchar(200) NOT NULL,
  `by_phone` varchar(20) NOT NULL,
  `by_fax` varchar(20) NOT NULL,
  `by_letter` varchar(100) NOT NULL,
  `by_self` varchar(100) NOT NULL,
  `by_others` varchar(100) NOT NULL,
  `match_county` varchar(20) NOT NULL,
  `match_district` varchar(20) NOT NULL,
  `degree_limit` int(11) NOT NULL,
  `working_experience` int(11) NOT NULL,
  `allowed_condition` varchar(120) NOT NULL,
  `department_type_id` int(11) NOT NULL,
  `skill` varchar(100) NOT NULL,
  `license` varchar(100) NOT NULL,
  `additional_demand` varchar(4000) NOT NULL,
  `posted_time` datetime NOT NULL,
  `lasted_update_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'posted_user_id',
  `vacancy_open_flag` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `jupyter`
--

CREATE TABLE `jupyter` (
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `course_id` int(11) NOT NULL COMMENT '關卡ID',
  `chapter_id` int(11) NOT NULL COMMENT 'chapter的id',
  `chapter_content_id` int(11) NOT NULL COMMENT 'chapter_content的id',
  `instanceID` varchar(50) NOT NULL COMMENT 'EC2 Instance ID',
  `instanceURL` text NOT NULL COMMENT 'EC2 Instance URL',
  `startTime` datetime NOT NULL COMMENT '開始時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `lab_content_group`
--

CREATE TABLE `lab_content_group` (
  `chapter_content_id` int(11) NOT NULL COMMENT '任務ID',
  `content_group_id` int(11) NOT NULL COMMENT '分組ID',
  `is_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有用'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `lh_team`
--

CREATE TABLE `lh_team` (
  `lh_team_id` int(11) NOT NULL COMMENT '團隊ID',
  `name` varchar(10) NOT NULL COMMENT '名字',
  `role` varchar(10) NOT NULL COMMENT '腳色',
  `desc` varchar(150) NOT NULL COMMENT '描述',
  `photo` varchar(20) NOT NULL COMMENT '照片檔名'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `library`
--

CREATE TABLE `library` (
  `library_id` int(11) NOT NULL COMMENT '知識庫ID',
  `library_color` varchar(6) NOT NULL DEFAULT '111111',
  `library_name` varchar(30) NOT NULL COMMENT '知識庫名稱',
  `library_url` varchar(32) NOT NULL COMMENT '網址列文字敘述',
  `library_desc` varchar(256) NOT NULL COMMENT '知識庫敘述',
  `pic` varchar(50) NOT NULL COMMENT '課程領域圖片',
  `create_time` datetime DEFAULT NULL COMMENT '建立時間',
  `status` tinyint(1) DEFAULT '1' COMMENT '狀態 0:Cancel;1:Available',
  `order` int(11) NOT NULL COMMENT '順序'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='知識庫';

-- --------------------------------------------------------

--
-- 資料表結構 `login_history`
--

CREATE TABLE `login_history` (
  `login_history_id` int(11) NOT NULL COMMENT '課程分類ID',
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `os` varchar(100) NOT NULL COMMENT '登入者作業系統',
  `browser` varchar(100) NOT NULL COMMENT '登入者瀏覽器',
  `login_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '登入時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `meeting`
--

CREATE TABLE `meeting` (
  `id` int(11) NOT NULL,
  `topic` varchar(256) DEFAULT NULL COMMENT '會議名稱',
  `type` int(11) DEFAULT '1' COMMENT '會議類型',
  `start_time` datetime DEFAULT NULL COMMENT '會議日期',
  `course_id` int(11) DEFAULT NULL COMMENT '課程ID',
  `teacher_id` int(11) DEFAULT NULL COMMENT '教師ID',
  `description` text COMMENT '會議說明',
  `create_date` datetime DEFAULT NULL COMMENT '會議建立日期',
  `duration` int(11) DEFAULT '30' COMMENT '會議時間長度(分)',
  `timezone` varchar(30) DEFAULT NULL COMMENT '時區',
  `password` varchar(10) DEFAULT NULL COMMENT '會議密碼',
  `join_first` tinyint(1) DEFAULT '0' COMMENT '主持會議前是否可先加入會議',
  `start_type` varchar(30) DEFAULT 'video' COMMENT '會議開啟時預設類型',
  `host_video` tinyint(1) DEFAULT '1' COMMENT '主持者加入會議時是否開啟視訊',
  `participants_video` tinyint(1) DEFAULT '1' COMMENT '與會者加入會議時是否開啟視訊',
  `audio_option` varchar(30) DEFAULT 'both' COMMENT '會議音訊',
  `check_value` varchar(255) DEFAULT NULL COMMENT '檢查碼',
  `uuid` varchar(24) DEFAULT NULL COMMENT '會議序號',
  `meeting_id` varchar(10) DEFAULT NULL COMMENT '會議室ID',
  `start_url` text COMMENT '開啟會議URL',
  `join_url` varchar(255) DEFAULT NULL COMMENT '加入會議URL',
  `host_id` varchar(30) DEFAULT NULL COMMENT '主持人ID',
  `status` int(11) DEFAULT '0' COMMENT '0:未完成,1:已完成',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否刪除'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `notification`
--

CREATE TABLE `notification` (
  `notification_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `notification_type` int(11) NOT NULL COMMENT '1:Forum Reply',
  `notification_title` varchar(100) NOT NULL COMMENT '標題',
  `notification_content` varchar(200) DEFAULT NULL COMMENT '依據Notification不同 1:無content',
  `hyperlink` varchar(1024) NOT NULL COMMENT '依據Notification不同 1:forum_topic_id',
  `notification_time` datetime NOT NULL,
  `notifier_id` int(11) NOT NULL COMMENT '通知來源',
  `is_read` tinyint(4) NOT NULL DEFAULT '0' COMMENT '通知是否已被讀取'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `options`
--

CREATE TABLE `options` (
  `options_id` int(11) NOT NULL COMMENT '選項ID',
  `options_content` varchar(256) NOT NULL COMMENT '選項內容',
  `answer` varchar(256) DEFAULT NULL COMMENT '答案',
  `question_id` int(11) NOT NULL COMMENT '題目ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='選項';

-- --------------------------------------------------------

--
-- 資料表結構 `order_detail`
--

CREATE TABLE `order_detail` (
  `order_detail_id` int(11) NOT NULL COMMENT '訂單明細ID',
  `course_id` int(11) NOT NULL COMMENT '課程ID',
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `live_flag` tinyint(1) DEFAULT '0' COMMENT '使用狀態 0:無效;1:有效',
  `order_id` int(11) NOT NULL COMMENT '訂單ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `points`
--

CREATE TABLE `points` (
  `points_id` int(11) NOT NULL COMMENT '積分ID',
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `points_source` varchar(36) NOT NULL COMMENT '積分來源',
  `course_id` int(11) DEFAULT NULL COMMENT '積分來源ID',
  `chapter_id` int(11) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `forum_content_id` int(11) DEFAULT NULL,
  `points` int(11) NOT NULL COMMENT '積分',
  `points_get_time` datetime NOT NULL COMMENT '積分取得時間'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `portfolio_skill`
--

CREATE TABLE `portfolio_skill` (
  `portfolio_skill_id` int(11) NOT NULL,
  `user_portfolio_id` int(11) NOT NULL,
  `user_skill_id` int(11) NOT NULL,
  `portfolio_skill_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `preferential`
--

CREATE TABLE `preferential` (
  `preferential_id` int(11) NOT NULL COMMENT '優惠價格ID',
  `start_time` datetime DEFAULT NULL COMMENT '優惠開始時間',
  `end_time` datetime DEFAULT NULL COMMENT '優惠結束時間',
  `course_id` int(11) NOT NULL COMMENT '課程ID',
  `preferential_price` int(11) NOT NULL COMMENT '優惠價'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `question`
--

CREATE TABLE `question` (
  `question_id` int(11) NOT NULL COMMENT '題目ID',
  `question_content` text NOT NULL COMMENT '題目內容',
  `question_type` tinyint(1) DEFAULT '0' COMMENT '課程類型 0:單選題;1:複選題;2:是非題;3:填空題',
  `question_hint` varchar(256) NOT NULL COMMENT '答案候選區',
  `correct_answer` varchar(64) DEFAULT NULL COMMENT '正確答案',
  `origin_answer` varchar(512) NOT NULL COMMENT '未加密原始檔案',
  `score` tinyint(1) DEFAULT '1' COMMENT '計分用',
  `quiz_detail_id` int(11) NOT NULL COMMENT '關卡內容ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='題目';

-- --------------------------------------------------------

--
-- 資料表結構 `quiz_detail`
--

CREATE TABLE `quiz_detail` (
  `quiz_detail_id` int(11) NOT NULL COMMENT '測驗ID',
  `quiz_detail_desc` varchar(256) NOT NULL COMMENT '關卡測驗描述',
  `quiz_detail_url` varchar(32) NOT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '建立時間',
  `chapter_content_id` int(11) NOT NULL COMMENT '關卡內容ID',
  `pass_conditions` int(12) NOT NULL COMMENT '過關條件',
  `quantity` int(16) NOT NULL COMMENT '題目數量'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='關卡內容(測驗)';

-- --------------------------------------------------------

--
-- 資料表結構 `resource`
--

CREATE TABLE `resource` (
  `resource_id` int(11) NOT NULL COMMENT '國際資源ID',
  `resource_type` tinyint(1) NOT NULL COMMENT '資源類型 1:影片;2:文件;3:文章;4:其他',
  `label_name` varchar(20) NOT NULL COMMENT '標籤名稱(標籤顏色)',
  `resource_title` varchar(50) NOT NULL COMMENT '國際資源標題',
  `resource_url` varchar(500) NOT NULL COMMENT '國際資源URL',
  `resource_source` varchar(100) NOT NULL COMMENT '資料出處',
  `resource_origin_date` datetime NOT NULL COMMENT '資料原始時間',
  `domain_id` int(11) NOT NULL COMMENT '領域分類ID',
  `jobs_category_id` varchar(20) NOT NULL COMMENT '職務分類ID(type:array)',
  `resource_post_date` datetime NOT NULL COMMENT '發表時間',
  `resource_status` tinyint(1) DEFAULT '1' COMMENT '狀態 0:Cancel;1:Available',
  `clicks` int(11) NOT NULL DEFAULT '0' COMMENT '點擊次數',
  `user_id` int(11) NOT NULL COMMENT 'post user_id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `search_record`
--

CREATE TABLE `search_record` (
  `search_record_id` int(11) NOT NULL COMMENT '搜尋紀錄ID',
  `email` varchar(50) NOT NULL COMMENT '使用者ID',
  `search_string` varchar(256) NOT NULL COMMENT '搜尋字串',
  `search_time` datetime NOT NULL COMMENT '搜尋日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `seminar`
--

CREATE TABLE `seminar` (
  `seminar_id` int(11) NOT NULL COMMENT '研討會訊息ID',
  `seminar_name` varchar(10) NOT NULL COMMENT '1:研討會;2:競賽;3:展覽;4:其他',
  `seminar_host` varchar(30) NOT NULL COMMENT '主辦單位',
  `seminar_title` varchar(50) NOT NULL COMMENT '研討會訊息標題',
  `seminar_content` text NOT NULL COMMENT '研討會內容(摘要)',
  `seminar_url` varchar(500) NOT NULL COMMENT '研討會訊息URL',
  `seminar_pic` varchar(50) NOT NULL COMMENT '研討會圖片',
  `seminar_date` datetime NOT NULL COMMENT '研討會時間',
  `seminar_start_time` datetime NOT NULL COMMENT '開始時間',
  `seminar_end_time` datetime NOT NULL COMMENT '結束時間',
  `seminar_place` varchar(50) NOT NULL COMMENT '研討會地點',
  `domain_id` int(11) NOT NULL COMMENT '領域分類ID',
  `jobs_category_id` varchar(20) NOT NULL COMMENT '職務分類ID(type:array)',
  `seminar_post_date` datetime NOT NULL COMMENT '發表時間',
  `seminar_status` tinyint(1) DEFAULT '1' COMMENT '狀態 0:Cancel;1:Available',
  `label_name` varchar(20) NOT NULL COMMENT '標籤名稱(標籤顏色)',
  `clicks` int(11) NOT NULL DEFAULT '0' COMMENT '點擊次數',
  `modal_clicks` int(11) NOT NULL DEFAULT '0' COMMENT 'modal的點擊次數',
  `user_id` int(11) NOT NULL COMMENT 'post user_id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `seniority`
--

CREATE TABLE `seniority` (
  `seniority_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `jobs_category_id` int(11) NOT NULL,
  `year` int(11) NOT NULL COMMENT '-1:無經驗0:未滿1年1:1年以上2:2年以上3:3年以上4:4年以上5:5年以上6:6年以上7:7年以上8:8年以上9:9年以上10:10年以上',
  `seniority_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `statement`
--

CREATE TABLE `statement` (
  `statementId` varchar(40) NOT NULL,
  `userId` varchar(40) NOT NULL,
  `statementBody` text,
  `actor` text,
  `verb` text,
  `objectType` varchar(20) DEFAULT NULL,
  `object` text,
  `result` text,
  `context` text,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `stored` timestamp NULL DEFAULT NULL,
  `authority` text,
  `version` varchar(10) DEFAULT NULL,
  `attachments` text,
  `isVoided` tinyint(1) DEFAULT '0',
  `verbId` text,
  `activityId` text,
  `registration` varchar(40) DEFAULT NULL,
  `instructor` text,
  `team` text,
  `contextActivities` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `teacher`
--

CREATE TABLE `teacher` (
  `teacher_id` int(11) NOT NULL,
  `name` varchar(10) NOT NULL,
  `email` varchar(32) NOT NULL,
  `introduction` varchar(256) NOT NULL,
  `teacher_pic` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `tracks`
--

CREATE TABLE `tracks` (
  `tracks_id` int(11) NOT NULL COMMENT '學程ID',
  `tracks_name` varchar(32) NOT NULL COMMENT '學程名稱',
  `tracks_desc` varchar(256) NOT NULL COMMENT '學程描述',
  `proficiency` tinyint(1) NOT NULL DEFAULT '1' COMMENT '熟練度 1:學習者;2:練習者;3:能力者;4:專家;',
  `will_learn` varchar(512) NOT NULL COMMENT '你將學會什麼',
  `tracks_url` varchar(50) NOT NULL COMMENT '學程網址列',
  `pic` varchar(50) NOT NULL COMMENT '學程圖片',
  `create_time` datetime NOT NULL COMMENT '建立時間',
  `status` tinyint(1) NOT NULL COMMENT '狀態 0:Cancel;1:Available',
  `order` int(11) NOT NULL COMMENT '排序',
  `domain_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `tracks_course`
--

CREATE TABLE `tracks_course` (
  `tracks_course_id` int(11) NOT NULL COMMENT '學程_課程ID',
  `tracks_id` int(11) NOT NULL COMMENT '學程ID',
  `course_id` int(11) NOT NULL COMMENT '課程ID',
  `order` int(11) NOT NULL COMMENT '排序'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `tracks_teacher`
--

CREATE TABLE `tracks_teacher` (
  `tracks_teacher_id` int(11) NOT NULL COMMENT '學程講師ID',
  `tracks_id` varchar(50) NOT NULL COMMENT '學程ID',
  `teacher_id` varchar(50) NOT NULL COMMENT '講師ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `fb_id` varchar(50) DEFAULT NULL,
  `name` varchar(15) NOT NULL COMMENT '使用者名字',
  `email` varchar(50) NOT NULL COMMENT '信箱',
  `alias` varchar(50) NOT NULL COMMENT '別名',
  `password` varchar(64) NOT NULL COMMENT '密碼',
  `img` varchar(50) DEFAULT 'default_user.png' COMMENT '相片',
  `resume_img` varchar(50) DEFAULT 'default_user.png	' COMMENT '履歷大頭照',
  `company_img` varchar(100) DEFAULT NULL COMMENT '企業徵才代表公司的logo',
  `intro` varchar(256) DEFAULT NULL COMMENT '個人簡介',
  `address` varchar(50) DEFAULT NULL COMMENT '地址',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手機',
  `recruiters` int(1) NOT NULL DEFAULT '0' COMMENT '提供履歷供求才 0:不提供;1:提供',
  `education_highest_level` int(11) NOT NULL DEFAULT '0' COMMENT '最高學歷 1:高中職;2:大學;3:研究所;4:博士',
  `work_status` int(11) NOT NULL DEFAULT '0' COMMENT '就業狀態 1:工作中;2:無工作',
  `work_skills` varchar(256) DEFAULT NULL COMMENT '擅長工具',
  `company_name` varchar(50) DEFAULT NULL COMMENT '企業徵才代表的公司名稱',
  `company_website` varchar(100) DEFAULT NULL COMMENT '公司網址',
  `video_resume` varchar(100) DEFAULT NULL,
  `demo_website` varchar(100) DEFAULT NULL,
  `location` varchar(20) DEFAULT NULL,
  `autobiography` varchar(4000) DEFAULT NULL COMMENT '自傳',
  `contact_email` varchar(50) DEFAULT NULL COMMENT '連絡信箱',
  `match_jobs_category` varchar(100) DEFAULT NULL COMMENT '有興趣的職務分類',
  `performance_open_flag` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否開放學習表現供求才者查看 0:不公開 1:公開',
  `register_date` datetime DEFAULT NULL COMMENT '註冊時間',
  `status` tinyint(1) DEFAULT '1' COMMENT '帳號狀態 0:Cancel;1:Available;2:Pausing',
  `account_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '帳號等級 0:Gold;1:Silver',
  `last_login` datetime DEFAULT NULL COMMENT '上次登入時間',
  `count` int(11) NOT NULL DEFAULT '1' COMMENT '登入次數',
  `leader_email` varchar(50) DEFAULT NULL COMMENT 'leader',
  `start_time` datetime DEFAULT NULL COMMENT '帳號開始時間',
  `end_time` datetime DEFAULT NULL COMMENT '帳號結束時間',
  `permissions_flag` tinyint(1) DEFAULT '0' COMMENT '權限 0:一般使用者;1:老師;2:SUPERUSER',
  `sendemail_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '批次寄信的權限',
  `backend_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '後台進入權限',
  `coupon_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'coupon權限',
  `industry_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '趨勢案例寫入權限',
  `seminar_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '相關活動寫入權限',
  `jobs_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '人才需求寫入權限',
  `resource_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '國際資源寫入權限',
  `active_code` varchar(64) NOT NULL COMMENT '帳號啟動碼',
  `active_flag` int(11) NOT NULL DEFAULT '0' COMMENT '帳號啟動 0:未啟動;1:已啟動',
  `teacher_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否是教師'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_award`
--

CREATE TABLE `user_award` (
  `user_award_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `award_name` varchar(20) NOT NULL,
  `award_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_career_skill`
--

CREATE TABLE `user_career_skill` (
  `user_career_skill_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `career_skill_name` varchar(20) NOT NULL,
  `career_skill_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_chapter`
--

CREATE TABLE `user_chapter` (
  `user_chapter_id` int(11) NOT NULL,
  `user_course_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL COMMENT '關卡開始時間',
  `accomplish_time` datetime NOT NULL COMMENT '關卡完成時間',
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_chapter_content`
--

CREATE TABLE `user_chapter_content` (
  `user_content_id` int(11) NOT NULL,
  `user_chapter_id` int(11) NOT NULL,
  `chapter_content_id` int(11) NOT NULL,
  `accomplish_time` datetime NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_course`
--

CREATE TABLE `user_course` (
  `user_course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL COMMENT '課程開始時間',
  `accomplish_time` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_education`
--

CREATE TABLE `user_education` (
  `user_education_id` int(11) NOT NULL,
  `school_name` varchar(20) NOT NULL,
  `department` varchar(20) NOT NULL,
  `degree` int(11) NOT NULL COMMENT '最高學歷 1:高中職;2:大學;3:研究所;4:博士',
  `education_start` smallint(4) NOT NULL,
  `education_start_month` tinyint(2) NOT NULL,
  `education_end` smallint(4) NOT NULL,
  `education_end_month` tinyint(2) NOT NULL,
  `until_now` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:畢業 1:肄業 2:在學中',
  `education_order` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_experience`
--

CREATE TABLE `user_experience` (
  `user_experience_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_name` varchar(20) NOT NULL,
  `job_title` varchar(20) NOT NULL,
  `experience_start` smallint(4) NOT NULL,
  `experience_start_month` tinyint(2) NOT NULL,
  `experience_end` smallint(4) NOT NULL,
  `experience_end_month` tinyint(2) NOT NULL,
  `until_now` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:未在職 1:在職中',
  `experience_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_license`
--

CREATE TABLE `user_license` (
  `user_license_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `license_name` varchar(20) NOT NULL,
  `license_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_portfolio`
--

CREATE TABLE `user_portfolio` (
  `user_portfolio_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `portfolio_name` varchar(50) NOT NULL,
  `portfolio_pic` varchar(100) NOT NULL,
  `portfolio_desc` varchar(1000) NOT NULL,
  `hyperlink` varchar(1024) NOT NULL,
  `portfolio_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:未選擇 1:圖片 2:Youtube影片 3:SlideShare 4:外連網址',
  `hits` int(11) NOT NULL DEFAULT '0',
  `portfolio_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_skill`
--

CREATE TABLE `user_skill` (
  `user_skill_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `skill_name` varchar(20) NOT NULL,
  `skill_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_society`
--

CREATE TABLE `user_society` (
  `user_society_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `facebook` varchar(1024) NOT NULL,
  `google_plus` varchar(1024) NOT NULL,
  `twitter` varchar(1024) NOT NULL,
  `linkedin` varchar(1024) NOT NULL,
  `github` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_tracks`
--

CREATE TABLE `user_tracks` (
  `user_tracks_id` int(11) NOT NULL COMMENT '使用者訂閱學程ID',
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `tracks_id` int(11) NOT NULL COMMENT '學程ID',
  `tracks_date` datetime NOT NULL,
  `learning_status` tinyint(1) NOT NULL COMMENT '學習狀態 0:未學習;1:學習中'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `user_training`
--

CREATE TABLE `user_training` (
  `user_training_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `training_name` varchar(20) NOT NULL,
  `training_unit` varchar(20) NOT NULL,
  `training_start` smallint(4) NOT NULL,
  `training_start_month` tinyint(2) NOT NULL,
  `training_end` smallint(4) NOT NULL,
  `training_end_month` tinyint(2) NOT NULL,
  `until_now` tinyint(1) NOT NULL DEFAULT '0',
  `training_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `web_cam_pic`
--

CREATE TABLE `web_cam_pic` (
  `web_cam_pic_id` int(11) NOT NULL COMMENT '使用者照片ID',
  `user_id` int(11) NOT NULL COMMENT '使用者ID',
  `filename` varchar(50) NOT NULL COMMENT '檔案名稱',
  `pic_date` datetime NOT NULL COMMENT '檔案建立日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `access_log`
--
ALTER TABLE `access_log`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `ai_assessment`
--
ALTER TABLE `ai_assessment`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `ai_assessment_access`
--
ALTER TABLE `ai_assessment_access`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `ai_assessment_answer`
--
ALTER TABLE `ai_assessment_answer`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `ai_assessment_result`
--
ALTER TABLE `ai_assessment_result`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `chapter`
--
ALTER TABLE `chapter`
  ADD PRIMARY KEY (`chapter_id`),
  ADD UNIQUE KEY `chapter_url` (`chapter_url`),
  ADD KEY `course_id` (`course_id`);

--
-- 資料表索引 `chapter_content`
--
ALTER TABLE `chapter_content`
  ADD PRIMARY KEY (`chapter_content_id`),
  ADD UNIQUE KEY `chapter_content_url` (`chapter_content_url`),
  ADD KEY `chapter_id` (`chapter_id`);

--
-- 資料表索引 `content_group`
--
ALTER TABLE `content_group`
  ADD PRIMARY KEY (`content_group_id`);

--
-- 資料表索引 `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`coupon_id`),
  ADD UNIQUE KEY `coupon_code` (`coupon_code`);

--
-- 資料表索引 `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`course_id`),
  ADD KEY `library_id` (`library_id`),
  ADD KEY `course_category_id` (`course_category_id`);

--
-- 資料表索引 `course_announcement`
--
ALTER TABLE `course_announcement`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `course_category`
--
ALTER TABLE `course_category`
  ADD PRIMARY KEY (`course_category_id`);

--
-- 資料表索引 `course_order`
--
ALTER TABLE `course_order`
  ADD PRIMARY KEY (`order_id`);

--
-- 資料表索引 `dl_resource`
--
ALTER TABLE `dl_resource`
  ADD PRIMARY KEY (`dl_resource_id`),
  ADD KEY `chapter_id` (`chapter_id`);

--
-- 資料表索引 `domain`
--
ALTER TABLE `domain`
  ADD PRIMARY KEY (`domain_id`);

--
-- 資料表索引 `domain_library`
--
ALTER TABLE `domain_library`
  ADD PRIMARY KEY (`domain_library_id`);

--
-- 資料表索引 `eclat`
--
ALTER TABLE `eclat`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `email_record`
--
ALTER TABLE `email_record`
  ADD PRIMARY KEY (`email_record_id`);

--
-- 資料表索引 `forget_password`
--
ALTER TABLE `forget_password`
  ADD PRIMARY KEY (`forget_password_id`);

--
-- 資料表索引 `forum_assistant`
--
ALTER TABLE `forum_assistant`
  ADD PRIMARY KEY (`forum_assistant_id`);

--
-- 資料表索引 `forum_comment`
--
ALTER TABLE `forum_comment`
  ADD PRIMARY KEY (`forum_comment_id`),
  ADD KEY `forum_content_id` (`forum_content_id`);

--
-- 資料表索引 `forum_content`
--
ALTER TABLE `forum_content`
  ADD PRIMARY KEY (`forum_content_id`),
  ADD KEY `forum_topic_id` (`forum_topic_id`);

--
-- 資料表索引 `forum_subscription`
--
ALTER TABLE `forum_subscription`
  ADD PRIMARY KEY (`forum_subscription_id`),
  ADD KEY `forum_topic_id` (`forum_topic_id`),
  ADD KEY `forum_topic_id_2` (`forum_topic_id`);

--
-- 資料表索引 `forum_topic`
--
ALTER TABLE `forum_topic`
  ADD PRIMARY KEY (`forum_topic_id`),
  ADD KEY `library_id` (`library_id`);

--
-- 資料表索引 `forum_topic_read`
--
ALTER TABLE `forum_topic_read`
  ADD PRIMARY KEY (`forum_topic_read_id`);

--
-- 資料表索引 `forum_vote_record`
--
ALTER TABLE `forum_vote_record`
  ADD PRIMARY KEY (`forum_vote_record_id`);

--
-- 資料表索引 `group_member`
--
ALTER TABLE `group_member`
  ADD PRIMARY KEY (`member_id`);

--
-- 資料表索引 `industry`
--
ALTER TABLE `industry`
  ADD PRIMARY KEY (`industry_id`);

--
-- 資料表索引 `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`jobs_id`);

--
-- 資料表索引 `jobs_category`
--
ALTER TABLE `jobs_category`
  ADD PRIMARY KEY (`jobs_category_id`);

--
-- 資料表索引 `jobs_category_library`
--
ALTER TABLE `jobs_category_library`
  ADD PRIMARY KEY (`jobs_category_library_id`);

--
-- 資料表索引 `jobs_category_tracks`
--
ALTER TABLE `jobs_category_tracks`
  ADD PRIMARY KEY (`jobs_category_tracks_id`);

--
-- 資料表索引 `job_vacancy`
--
ALTER TABLE `job_vacancy`
  ADD PRIMARY KEY (`job_vacancy_id`);

--
-- 資料表索引 `jupyter`
--
ALTER TABLE `jupyter`
  ADD PRIMARY KEY (`user_id`,`course_id`);

--
-- 資料表索引 `lab_content_group`
--
ALTER TABLE `lab_content_group`
  ADD PRIMARY KEY (`chapter_content_id`,`content_group_id`);

--
-- 資料表索引 `lh_team`
--
ALTER TABLE `lh_team`
  ADD PRIMARY KEY (`lh_team_id`);

--
-- 資料表索引 `library`
--
ALTER TABLE `library`
  ADD PRIMARY KEY (`library_id`),
  ADD UNIQUE KEY `library_url` (`library_url`);

--
-- 資料表索引 `login_history`
--
ALTER TABLE `login_history`
  ADD PRIMARY KEY (`login_history_id`),
  ADD KEY `os` (`os`),
  ADD KEY `browser` (`browser`);

--
-- 資料表索引 `meeting`
--
ALTER TABLE `meeting`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`notification_id`),
  ADD KEY `user_id` (`user_id`);

--
-- 資料表索引 `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`options_id`),
  ADD KEY `question_id` (`question_id`);

--
-- 資料表索引 `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`order_detail_id`),
  ADD KEY `order_id` (`order_id`);

--
-- 資料表索引 `points`
--
ALTER TABLE `points`
  ADD PRIMARY KEY (`points_id`);

--
-- 資料表索引 `portfolio_skill`
--
ALTER TABLE `portfolio_skill`
  ADD PRIMARY KEY (`portfolio_skill_id`);

--
-- 資料表索引 `preferential`
--
ALTER TABLE `preferential`
  ADD PRIMARY KEY (`preferential_id`);

--
-- 資料表索引 `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`question_id`),
  ADD KEY `quiz_detail_id` (`quiz_detail_id`);

--
-- 資料表索引 `quiz_detail`
--
ALTER TABLE `quiz_detail`
  ADD PRIMARY KEY (`quiz_detail_id`),
  ADD KEY `chapter_content_id` (`chapter_content_id`);

--
-- 資料表索引 `resource`
--
ALTER TABLE `resource`
  ADD PRIMARY KEY (`resource_id`);

--
-- 資料表索引 `search_record`
--
ALTER TABLE `search_record`
  ADD PRIMARY KEY (`search_record_id`);

--
-- 資料表索引 `seminar`
--
ALTER TABLE `seminar`
  ADD PRIMARY KEY (`seminar_id`);

--
-- 資料表索引 `seniority`
--
ALTER TABLE `seniority`
  ADD PRIMARY KEY (`seniority_id`);

--
-- 資料表索引 `statement`
--
ALTER TABLE `statement`
  ADD PRIMARY KEY (`statementId`,`userId`),
  ADD KEY `NewIndex1` (`userId`),
  ADD KEY `stored` (`stored`);

--
-- 資料表索引 `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`teacher_id`);

--
-- 資料表索引 `tracks`
--
ALTER TABLE `tracks`
  ADD PRIMARY KEY (`tracks_id`),
  ADD UNIQUE KEY `tracks_url` (`tracks_url`),
  ADD KEY `domain_id` (`domain_id`);

--
-- 資料表索引 `tracks_course`
--
ALTER TABLE `tracks_course`
  ADD PRIMARY KEY (`tracks_course_id`),
  ADD KEY `tracks_id` (`tracks_id`),
  ADD KEY `course_id` (`course_id`);

--
-- 資料表索引 `tracks_teacher`
--
ALTER TABLE `tracks_teacher`
  ADD PRIMARY KEY (`tracks_teacher_id`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- 資料表索引 `user_award`
--
ALTER TABLE `user_award`
  ADD PRIMARY KEY (`user_award_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`);

--
-- 資料表索引 `user_career_skill`
--
ALTER TABLE `user_career_skill`
  ADD PRIMARY KEY (`user_career_skill_id`);

--
-- 資料表索引 `user_chapter`
--
ALTER TABLE `user_chapter`
  ADD PRIMARY KEY (`user_chapter_id`),
  ADD KEY `user_id` (`chapter_id`),
  ADD KEY `chapter_id` (`chapter_id`),
  ADD KEY `user_course_id` (`user_course_id`);

--
-- 資料表索引 `user_chapter_content`
--
ALTER TABLE `user_chapter_content`
  ADD PRIMARY KEY (`user_content_id`),
  ADD KEY `user_id` (`chapter_content_id`),
  ADD KEY `chapter_content_id` (`chapter_content_id`),
  ADD KEY `user_chapter_id` (`user_chapter_id`);

--
-- 資料表索引 `user_course`
--
ALTER TABLE `user_course`
  ADD PRIMARY KEY (`user_course_id`),
  ADD KEY `user_id` (`user_id`,`course_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `course_id` (`course_id`),
  ADD KEY `course_id_2` (`course_id`),
  ADD KEY `user_id_3` (`user_id`),
  ADD KEY `course_id_3` (`course_id`);

--
-- 資料表索引 `user_education`
--
ALTER TABLE `user_education`
  ADD PRIMARY KEY (`user_education_id`),
  ADD KEY `user_id` (`user_id`);

--
-- 資料表索引 `user_experience`
--
ALTER TABLE `user_experience`
  ADD PRIMARY KEY (`user_experience_id`),
  ADD KEY `user_id` (`user_id`);

--
-- 資料表索引 `user_license`
--
ALTER TABLE `user_license`
  ADD PRIMARY KEY (`user_license_id`),
  ADD KEY `user_id` (`user_id`);

--
-- 資料表索引 `user_portfolio`
--
ALTER TABLE `user_portfolio`
  ADD PRIMARY KEY (`user_portfolio_id`),
  ADD KEY `user_id` (`user_id`);

--
-- 資料表索引 `user_skill`
--
ALTER TABLE `user_skill`
  ADD PRIMARY KEY (`user_skill_id`),
  ADD KEY `user_id` (`user_id`);

--
-- 資料表索引 `user_society`
--
ALTER TABLE `user_society`
  ADD PRIMARY KEY (`user_society_id`),
  ADD KEY `user_id` (`user_id`);

--
-- 資料表索引 `user_tracks`
--
ALTER TABLE `user_tracks`
  ADD PRIMARY KEY (`user_tracks_id`),
  ADD KEY `tracks_id` (`tracks_id`),
  ADD KEY `user_id` (`user_id`);

--
-- 資料表索引 `user_training`
--
ALTER TABLE `user_training`
  ADD PRIMARY KEY (`user_training_id`);

--
-- 資料表索引 `web_cam_pic`
--
ALTER TABLE `web_cam_pic`
  ADD PRIMARY KEY (`web_cam_pic_id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `access_log`
--
ALTER TABLE `access_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=820;

--
-- 使用資料表 AUTO_INCREMENT `ai_assessment`
--
ALTER TABLE `ai_assessment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=3;

--
-- 使用資料表 AUTO_INCREMENT `ai_assessment_access`
--
ALTER TABLE `ai_assessment_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- 使用資料表 AUTO_INCREMENT `ai_assessment_answer`
--
ALTER TABLE `ai_assessment_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- 使用資料表 AUTO_INCREMENT `ai_assessment_result`
--
ALTER TABLE `ai_assessment_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- 使用資料表 AUTO_INCREMENT `chapter`
--
ALTER TABLE `chapter`
  MODIFY `chapter_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '關卡ID', AUTO_INCREMENT=157;

--
-- 使用資料表 AUTO_INCREMENT `chapter_content`
--
ALTER TABLE `chapter_content`
  MODIFY `chapter_content_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=933;

--
-- 使用資料表 AUTO_INCREMENT `content_group`
--
ALTER TABLE `content_group`
  MODIFY `content_group_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分組流水號', AUTO_INCREMENT=10;

--
-- 使用資料表 AUTO_INCREMENT `coupon`
--
ALTER TABLE `coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '優惠卷ID', AUTO_INCREMENT=78;

--
-- 使用資料表 AUTO_INCREMENT `course`
--
ALTER TABLE `course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '課程ID', AUTO_INCREMENT=105;

--
-- 使用資料表 AUTO_INCREMENT `course_announcement`
--
ALTER TABLE `course_announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水號', AUTO_INCREMENT=2;

--
-- 使用資料表 AUTO_INCREMENT `course_category`
--
ALTER TABLE `course_category`
  MODIFY `course_category_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '課程分類ID', AUTO_INCREMENT=19;

--
-- 使用資料表 AUTO_INCREMENT `course_order`
--
ALTER TABLE `course_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '訂單ID', AUTO_INCREMENT=303;

--
-- 使用資料表 AUTO_INCREMENT `dl_resource`
--
ALTER TABLE `dl_resource`
  MODIFY `dl_resource_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '資源ID', AUTO_INCREMENT=8;

--
-- 使用資料表 AUTO_INCREMENT `domain`
--
ALTER TABLE `domain`
  MODIFY `domain_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '領域分類ID', AUTO_INCREMENT=5;

--
-- 使用資料表 AUTO_INCREMENT `domain_library`
--
ALTER TABLE `domain_library`
  MODIFY `domain_library_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '學院學程ID', AUTO_INCREMENT=19;

--
-- 使用資料表 AUTO_INCREMENT `eclat`
--
ALTER TABLE `eclat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- 使用資料表 AUTO_INCREMENT `email_record`
--
ALTER TABLE `email_record`
  MODIFY `email_record_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '信件紀錄ID', AUTO_INCREMENT=2976;

--
-- 使用資料表 AUTO_INCREMENT `forget_password`
--
ALTER TABLE `forget_password`
  MODIFY `forget_password_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '忘記密碼ID', AUTO_INCREMENT=64;

--
-- 使用資料表 AUTO_INCREMENT `forum_assistant`
--
ALTER TABLE `forum_assistant`
  MODIFY `forum_assistant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- 使用資料表 AUTO_INCREMENT `forum_comment`
--
ALTER TABLE `forum_comment`
  MODIFY `forum_comment_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '討論區回覆ID', AUTO_INCREMENT=26;

--
-- 使用資料表 AUTO_INCREMENT `forum_content`
--
ALTER TABLE `forum_content`
  MODIFY `forum_content_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '討論區內容ID', AUTO_INCREMENT=73;

--
-- 使用資料表 AUTO_INCREMENT `forum_subscription`
--
ALTER TABLE `forum_subscription`
  MODIFY `forum_subscription_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- 使用資料表 AUTO_INCREMENT `forum_topic`
--
ALTER TABLE `forum_topic`
  MODIFY `forum_topic_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '討論區主題ID', AUTO_INCREMENT=36;

--
-- 使用資料表 AUTO_INCREMENT `forum_topic_read`
--
ALTER TABLE `forum_topic_read`
  MODIFY `forum_topic_read_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '閱讀文章ID', AUTO_INCREMENT=32;

--
-- 使用資料表 AUTO_INCREMENT `forum_vote_record`
--
ALTER TABLE `forum_vote_record`
  MODIFY `forum_vote_record_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '討論區投票紀錄ID', AUTO_INCREMENT=26;

--
-- 使用資料表 AUTO_INCREMENT `group_member`
--
ALTER TABLE `group_member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分組成員ID', AUTO_INCREMENT=13;

--
-- 使用資料表 AUTO_INCREMENT `industry`
--
ALTER TABLE `industry`
  MODIFY `industry_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '產業新聞ID', AUTO_INCREMENT=572;

--
-- 使用資料表 AUTO_INCREMENT `jobs`
--
ALTER TABLE `jobs`
  MODIFY `jobs_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '人才需求ID', AUTO_INCREMENT=316;

--
-- 使用資料表 AUTO_INCREMENT `jobs_category`
--
ALTER TABLE `jobs_category`
  MODIFY `jobs_category_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '職務分類ID', AUTO_INCREMENT=9;

--
-- 使用資料表 AUTO_INCREMENT `jobs_category_library`
--
ALTER TABLE `jobs_category_library`
  MODIFY `jobs_category_library_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '職業分類對應知識庫ID', AUTO_INCREMENT=23;

--
-- 使用資料表 AUTO_INCREMENT `jobs_category_tracks`
--
ALTER TABLE `jobs_category_tracks`
  MODIFY `jobs_category_tracks_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '職務分類學程ID', AUTO_INCREMENT=15;

--
-- 使用資料表 AUTO_INCREMENT `job_vacancy`
--
ALTER TABLE `job_vacancy`
  MODIFY `job_vacancy_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- 使用資料表 AUTO_INCREMENT `lh_team`
--
ALTER TABLE `lh_team`
  MODIFY `lh_team_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '團隊ID', AUTO_INCREMENT=13;

--
-- 使用資料表 AUTO_INCREMENT `library`
--
ALTER TABLE `library`
  MODIFY `library_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '知識庫ID', AUTO_INCREMENT=16;

--
-- 使用資料表 AUTO_INCREMENT `login_history`
--
ALTER TABLE `login_history`
  MODIFY `login_history_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '課程分類ID', AUTO_INCREMENT=1574;

--
-- 使用資料表 AUTO_INCREMENT `meeting`
--
ALTER TABLE `meeting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用資料表 AUTO_INCREMENT `notification`
--
ALTER TABLE `notification`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- 使用資料表 AUTO_INCREMENT `options`
--
ALTER TABLE `options`
  MODIFY `options_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '選項ID', AUTO_INCREMENT=1212;

--
-- 使用資料表 AUTO_INCREMENT `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `order_detail_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '訂單明細ID', AUTO_INCREMENT=314;

--
-- 使用資料表 AUTO_INCREMENT `points`
--
ALTER TABLE `points`
  MODIFY `points_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '積分ID', AUTO_INCREMENT=972;

--
-- 使用資料表 AUTO_INCREMENT `portfolio_skill`
--
ALTER TABLE `portfolio_skill`
  MODIFY `portfolio_skill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- 使用資料表 AUTO_INCREMENT `preferential`
--
ALTER TABLE `preferential`
  MODIFY `preferential_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '優惠價格ID';

--
-- 使用資料表 AUTO_INCREMENT `question`
--
ALTER TABLE `question`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '題目ID', AUTO_INCREMENT=468;

--
-- 使用資料表 AUTO_INCREMENT `quiz_detail`
--
ALTER TABLE `quiz_detail`
  MODIFY `quiz_detail_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '測驗ID', AUTO_INCREMENT=116;

--
-- 使用資料表 AUTO_INCREMENT `resource`
--
ALTER TABLE `resource`
  MODIFY `resource_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '國際資源ID', AUTO_INCREMENT=139;

--
-- 使用資料表 AUTO_INCREMENT `search_record`
--
ALTER TABLE `search_record`
  MODIFY `search_record_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '搜尋紀錄ID', AUTO_INCREMENT=134;

--
-- 使用資料表 AUTO_INCREMENT `seminar`
--
ALTER TABLE `seminar`
  MODIFY `seminar_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '研討會訊息ID', AUTO_INCREMENT=57;

--
-- 使用資料表 AUTO_INCREMENT `seniority`
--
ALTER TABLE `seniority`
  MODIFY `seniority_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- 使用資料表 AUTO_INCREMENT `teacher`
--
ALTER TABLE `teacher`
  MODIFY `teacher_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- 使用資料表 AUTO_INCREMENT `tracks`
--
ALTER TABLE `tracks`
  MODIFY `tracks_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '學程ID', AUTO_INCREMENT=17;

--
-- 使用資料表 AUTO_INCREMENT `tracks_course`
--
ALTER TABLE `tracks_course`
  MODIFY `tracks_course_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '學程_課程ID', AUTO_INCREMENT=77;

--
-- 使用資料表 AUTO_INCREMENT `tracks_teacher`
--
ALTER TABLE `tracks_teacher`
  MODIFY `tracks_teacher_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '學程講師ID', AUTO_INCREMENT=7;

--
-- 使用資料表 AUTO_INCREMENT `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '使用者ID', AUTO_INCREMENT=3286;

--
-- 使用資料表 AUTO_INCREMENT `user_award`
--
ALTER TABLE `user_award`
  MODIFY `user_award_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用資料表 AUTO_INCREMENT `user_career_skill`
--
ALTER TABLE `user_career_skill`
  MODIFY `user_career_skill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- 使用資料表 AUTO_INCREMENT `user_chapter`
--
ALTER TABLE `user_chapter`
  MODIFY `user_chapter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1882;

--
-- 使用資料表 AUTO_INCREMENT `user_chapter_content`
--
ALTER TABLE `user_chapter_content`
  MODIFY `user_content_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9228;

--
-- 使用資料表 AUTO_INCREMENT `user_course`
--
ALTER TABLE `user_course`
  MODIFY `user_course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1163;

--
-- 使用資料表 AUTO_INCREMENT `user_education`
--
ALTER TABLE `user_education`
  MODIFY `user_education_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- 使用資料表 AUTO_INCREMENT `user_experience`
--
ALTER TABLE `user_experience`
  MODIFY `user_experience_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- 使用資料表 AUTO_INCREMENT `user_license`
--
ALTER TABLE `user_license`
  MODIFY `user_license_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用資料表 AUTO_INCREMENT `user_portfolio`
--
ALTER TABLE `user_portfolio`
  MODIFY `user_portfolio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- 使用資料表 AUTO_INCREMENT `user_skill`
--
ALTER TABLE `user_skill`
  MODIFY `user_skill_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- 使用資料表 AUTO_INCREMENT `user_society`
--
ALTER TABLE `user_society`
  MODIFY `user_society_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- 使用資料表 AUTO_INCREMENT `user_tracks`
--
ALTER TABLE `user_tracks`
  MODIFY `user_tracks_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '使用者訂閱學程ID', AUTO_INCREMENT=86;

--
-- 使用資料表 AUTO_INCREMENT `user_training`
--
ALTER TABLE `user_training`
  MODIFY `user_training_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表 AUTO_INCREMENT `web_cam_pic`
--
ALTER TABLE `web_cam_pic`
  MODIFY `web_cam_pic_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '使用者照片ID', AUTO_INCREMENT=49;

--
-- 已匯出資料表的限制(Constraint)
--

--
-- 資料表的 Constraints `chapter`
--
ALTER TABLE `chapter`
  ADD CONSTRAINT `chapter_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`) ON DELETE CASCADE;

--
-- 資料表的 Constraints `chapter_content`
--
ALTER TABLE `chapter_content`
  ADD CONSTRAINT `chapter_content_ibfk_1` FOREIGN KEY (`chapter_id`) REFERENCES `chapter` (`chapter_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 資料表的 Constraints `course`
--
ALTER TABLE `course`
  ADD CONSTRAINT `course_ibfk_1` FOREIGN KEY (`library_id`) REFERENCES `library` (`library_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `course_ibfk_2` FOREIGN KEY (`course_category_id`) REFERENCES `course_category` (`course_category_id`);

--
-- 資料表的 Constraints `dl_resource`
--
ALTER TABLE `dl_resource`
  ADD CONSTRAINT `dl_resource_ibfk_1` FOREIGN KEY (`chapter_id`) REFERENCES `chapter` (`chapter_id`) ON DELETE CASCADE;

--
-- 資料表的 Constraints `forum_comment`
--
ALTER TABLE `forum_comment`
  ADD CONSTRAINT `forum_comment_ibfk_1` FOREIGN KEY (`forum_content_id`) REFERENCES `forum_content` (`forum_content_id`) ON DELETE CASCADE;

--
-- 資料表的 Constraints `forum_content`
--
ALTER TABLE `forum_content`
  ADD CONSTRAINT `forum_content_ibfk_1` FOREIGN KEY (`forum_topic_id`) REFERENCES `forum_topic` (`forum_topic_id`) ON DELETE CASCADE;

--
-- 資料表的 Constraints `forum_subscription`
--
ALTER TABLE `forum_subscription`
  ADD CONSTRAINT `forum_subscription_ibfk_1` FOREIGN KEY (`forum_topic_id`) REFERENCES `forum_topic` (`forum_topic_id`) ON DELETE CASCADE;

--
-- 資料表的 Constraints `forum_topic`
--
ALTER TABLE `forum_topic`
  ADD CONSTRAINT `forum_topic_ibfk_2` FOREIGN KEY (`library_id`) REFERENCES `library` (`library_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 資料表的 Constraints `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE;

--
-- 資料表的 Constraints `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`question_id`) ON DELETE CASCADE;

--
-- 資料表的 Constraints `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `order_detail_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `course_order` (`order_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 資料表的 Constraints `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_ibfk_1` FOREIGN KEY (`quiz_detail_id`) REFERENCES `quiz_detail` (`quiz_detail_id`) ON DELETE CASCADE;

--
-- 資料表的 Constraints `quiz_detail`
--
ALTER TABLE `quiz_detail`
  ADD CONSTRAINT `quiz_detail_ibfk_1` FOREIGN KEY (`chapter_content_id`) REFERENCES `chapter_content` (`chapter_content_id`) ON DELETE CASCADE;

--
-- 資料表的 Constraints `tracks_course`
--
ALTER TABLE `tracks_course`
  ADD CONSTRAINT `tracks_course_ibfk_1` FOREIGN KEY (`tracks_id`) REFERENCES `tracks` (`tracks_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tracks_course_ibfk_2` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`) ON DELETE CASCADE;

--
-- 資料表的 Constraints `user_award`
--
ALTER TABLE `user_award`
  ADD CONSTRAINT `user_award_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 資料表的 Constraints `user_chapter`
--
ALTER TABLE `user_chapter`
  ADD CONSTRAINT `user_chapter_ibfk_3` FOREIGN KEY (`chapter_id`) REFERENCES `chapter` (`chapter_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_chapter_ibfk_5` FOREIGN KEY (`user_course_id`) REFERENCES `user_course` (`user_course_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 資料表的 Constraints `user_chapter_content`
--
ALTER TABLE `user_chapter_content`
  ADD CONSTRAINT `user_chapter_content_ibfk_6` FOREIGN KEY (`chapter_content_id`) REFERENCES `chapter_content` (`chapter_content_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_chapter_content_ibfk_7` FOREIGN KEY (`user_chapter_id`) REFERENCES `user_chapter` (`user_chapter_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 資料表的 Constraints `user_course`
--
ALTER TABLE `user_course`
  ADD CONSTRAINT `user_course_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_course_ibfk_4` FOREIGN KEY (`course_id`) REFERENCES `course` (`course_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 資料表的 Constraints `user_education`
--
ALTER TABLE `user_education`
  ADD CONSTRAINT `user_education_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 資料表的 Constraints `user_experience`
--
ALTER TABLE `user_experience`
  ADD CONSTRAINT `user_experience_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 資料表的 Constraints `user_license`
--
ALTER TABLE `user_license`
  ADD CONSTRAINT `user_license_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 資料表的 Constraints `user_portfolio`
--
ALTER TABLE `user_portfolio`
  ADD CONSTRAINT `user_portfolio_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 資料表的 Constraints `user_skill`
--
ALTER TABLE `user_skill`
  ADD CONSTRAINT `user_skill_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 資料表的 Constraints `user_society`
--
ALTER TABLE `user_society`
  ADD CONSTRAINT `user_society_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- 資料表的 Constraints `user_tracks`
--
ALTER TABLE `user_tracks`
  ADD CONSTRAINT `user_tracks_ibfk_1` FOREIGN KEY (`tracks_id`) REFERENCES `tracks` (`tracks_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_tracks_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
