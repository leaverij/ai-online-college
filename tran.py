# -*- coding: utf-8 -*-
"""
Created on Wed Dec 20 15:57:44 2017

@author: Apple

版本5b: COLOR 用SIZE遇到XXL會有誤判的問題, 不好處理,還是使用COLOR+SIZE方法
"""

from datetime import datetime
from datetime import timedelta
import csv
import json
import codecs
import os
import sys  #從命令列傳入參數


# 設定執行程式所在路徑
root =os.getcwd() + '\\'
#root = 'E:\\po\\'


# 切換執行目錄
os.chdir(root)



#從命令列傳入參數檔案名稱與輸出的目錄名稱
#arg = 'LT70191_1Part2.txt'
arg = sys.argv[1]

#輸出的目錄名稱或目的檔案名
#outdir = 'LT70191_1Part2'
outdir = sys.argv[2]

# 檔案路徑及檔案名
a = root + arg


# 建立目錄
#dir = 'LF70051Part2'
if os.path.exists(outdir):
    pass
else:
    os.mkdir(outdir)

#取出來源檔案, 存入list
infile = open(a,'r' , encoding = 'UTF-8') 
text = infile.readlines()

#關閉來源檔案
infile.close()
#print(text)
#print(len(text))

#合併輸出
filename = outdir + '.csv'
f = codecs.open(filename, 'w', 'utf_8_sig')   #分割輸出

#處理各別PO
def CSVTable(po_id,raw_start, raw_end,content,po_num):
    # 產生系統導入表格 csv格式檔案
    print(po_id,raw_start, raw_end, po_num)
    
    #filename = po_id +'.csv'   #分割輸出
    #print(filename)
       
    # Output 目錄root
    #os.chdir("C:\\Users\\Apple\\case1\\"+dir)
    #os.chdir(root + outdir)    #分割輸出
    
    #f = codecs.open(filename, 'w', 'utf_8_sig')   #分割輸出
    
    # content
    matrix=[]   #json results
    
    CPO = {'CPO':''} #留白
    Country = {'*Country':'US'} 
    Ship_mode = {'Ship Mode':'SEA'} 
    Comment = {'Comment':''} #留白
    MAXLINE=len(content)
    Carton_qty = 0
    
    for i in range(MAXLINE):
    
        if('FIRST COST' in content[i]):
            text_list = content[i].split()
            Price={'Price':text_list[3]}
            
        elif('Page: 1' in content[i]):
            text_list = content[i].split()
            PO = {'PO':text_list[2]}
    
    
        elif('In Store Date:' in content[i]):
            text_list = content[i].split()
            instore_date = {'instore_date':text_list[7]}
    
        # Destination List
        elif('SAVANNAH' in content[i]) or \
            ('SUFFOLK' in content[i]) :  #ETD & Export_Date 
            text_list = content[i].split()
            ETD = {'ETD':text_list[-9]}
            ETD2 = text_list[-9]   #倒數

    
        elif('Qty (ea):' in content[i]):
            text_list = content[i].split()
            qty_len = len(text_list)
            # 欄位長度不同
            if qty_len == 12:
                Carton = {'Carton':text_list[2]}
                Carton_qty = int(text_list[2])
                poly = {'poly':text_list[3]}
            else:
                Carton = {'Carton':text_list[3]}
                Carton_qty = int(text_list[3])
                poly = {'poly':text_list[4]}                
    
        elif('Cubic Meter:' in content[i]):
            text_list = content[i].split()
            # 需加if判斷欄位 len(x[i]): 或者取最後一碼
            xx = len(text_list)-1
            retail_price = {'retail_price':text_list[xx]}
            
            
        elif('Cartons per Line:' in content[i]):
            text_list = content[i].split()
            catorn_qty = {'catorn_qty':text_list[4]}
            per_line_qty = int(text_list[4])

        elif('per Line:' in content[i]):
            text_list = content[i].split()
            catorn_qty = {'catorn_qty':text_list[4]}
            per_line_qty = int(text_list[4])

    
        #Assortment number-1
        elif('Assortment Id:' in content[i]):
            text_list = content[i].split()
            Assortment_number = {'Assortment_number':text_list[2]}
    
        #Assortment number-2
        elif('Item Id:' in content[i]):
            text_list = content[i].split()
            Assortment_number = {'Assortment_number':text_list[2]}
            
        #TARIFF
        elif('WOMEN' in content[i]) or \
            ('BRASSIERES' in content[i]) or \
            ('TROUS' in content[i]):

            text_list = content[i].split()
            TARIFF = {'TARIFF':text_list[0]}
    
        #執行到此行表示一個輪迴,寫入資料
        elif('Vendor Stock:' in content[i]):
            text_list = content[i].split()
            Style = {'Style':text_list[2]}

    
        #CCustNo
        elif('Replenish to Sales' in content[i]):
            text_list = content[i].split()
            CCustNo = {'CCustNo':'REPL'}   
            VendorType = 'REPL'
    
        #CCustNo
        elif('Initial Set' in content[i]):
            text_list = content[i].split()
            CCustNo = {'CCustNo':'IS'}  
            VendorType = 'IS'
            
        #CCustNo  有New Store, New Stores 取最小長度
        elif('New Store' in content[i]):
            text_list = content[i].split()
            CCustNo = {'CCustNo':'IS'} 
            VendorType = 'IS'
    
        #COLOR COLOR 判斷  by SIZE ,XXL會有其它資料, 以長度進行區分判斷 
     

        #COLOR 判斷  by SIZE & COLOR, 需維護Color LIST
        elif ('SMALL' in content[i]) or \
            ('MEDIUM' in content[i]) or \
            ('LARGE' in content[i]) or \
            ('XLARGE' in content[i])or \
            ('SUFFOLK' in content[i]) or \
            ('BLKSOT' in content[i]) or \
            ('HDSHBS' in content[i]) or \
            ('BKCOBK' in content[i]) or \
            ('BKSAWT' in content[i]) or \
            ('BKSCBC' in content[i])or \
            ('CHGYHT' in content[i])or \
            ('INDESS' in content[i]):
                     
            text_list = content[i].split()
            
            #日期轉換與計算
            #text = '02/15/2018'
            y = datetime.strptime(ETD2, '%m/%d/%Y')
            
            #日期計算
            aDay = timedelta(days=7)
            EDate = y - aDay
            
            # PO Type 不同時計算數量的方法也會不同
            if VendorType == 'REPL':
               VendorTypeQty = {'QTY':int(Carton_qty) * per_line_qty}
            else:
               VendorTypeQty = {'QTY':int(text_list[2]) * per_line_qty}
            
            #datetime轉string
            EDateStr = str(EDate.strftime("%m/%d/%Y"))
            ExportDate = {'Export Date':EDateStr}
    
            matrix.append([Style,PO,CPO,            
                           ExportDate,Country,Ship_mode,
                           {'Article_No':text_list[0]},
                            {'Size':text_list[1]},
                             VendorTypeQty,
                            Price,CCustNo,Comment,
                            retail_price,catorn_qty,
                            Assortment_number,
                            {'item_number':text_list[3]},
                            {'UPC':text_list[5]},
                            instore_date,ETD,Carton,poly,TARIFF                              
                          ])    
    
    
    #print(matrix)  #結果1-字典檢查
    #print(len(matrix))  #結果1-字典檢查
    #Unit data 合併DICT 變成單一筆
    unitMerged=[]
    y={}
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            y={**y,**matrix[i][j]} 
        
        unitMerged.append(y) 
    
    #print(unitMerged)   # 合併字典檢查
    
    #csv dtata stream
    writer=csv.writer(f)
    count = 0 #分割輸出檔案
    for item in unitMerged:
        
        # Write CSV Header
        #if count == 0:
        if( (po_num == 0) and (count == 0)):
            header = item.keys()
            writer.writerow(header)
            count += 1
            
        # Write CSV content    
        writer.writerow([item['Style'], 
                         item['PO'],
                         item['CPO'],
                         item['Export Date'],
                         item['*Country'],
                         item['Ship Mode'],
                         item['Article_No'],
                         item['Size'],
                         item['QTY'],
                         item['Price'],
                         item['CCustNo'],
                         item['Comment'],
                         item['retail_price'],
                         item['catorn_qty'],
                         item['Assortment_number'],
                         item['item_number'],
                         item['UPC'],
                         item['instore_date'],
                         item['ETD'],
                         item['Carton'],
                         item['poly'],
                         item['TARIFF']
                         ])    
    
    #f.close()    #分割輸出檔案
    # 返回程式所在目錄
    #os.chdir("C:\\Users\\Apple\\case1")
    #os.chdir(root)  #分割輸出檔案



# 計算PO行號
po_row=[]
po_id=[]
MAXLINE = len(text)
#print(MAXLINE)
for i in range(MAXLINE):
    if ('Purchase Order:' in text[i]):
        text_list = text[i].split()
        #print(text_list)
        if (text_list[-1] == '1') and (text_list[-2] == 'Page:') :
                po_row.append(i)
                po_id.append(text_list[2])

po_row.append(i)    #最後一列             
#print(po_row)
#print(po_id)

#單筆PO測試
#CSVTable(po_id[0],po_row[0],po_row[0+1],text[po_row[0]:po_row[0+1]])


# 多筆批次處理 
PO_size = len(po_id)
for i in range(PO_size):
    CSVTable(po_id[i],po_row[i],po_row[i+1],text[po_row[i]:po_row[i+1]],i)
    #print(text)




#關閉檔案
f.close()

print('File generated successfully. ',root+filename)



